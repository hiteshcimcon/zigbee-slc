//
// network-creator.h
//
// Author: Andrew Keesler <andrew.keesler@silabs.com>
//
// Copyright 2014 Silicon Laboratories, Inc.                               *80*
//

#ifndef __NETWORK_CREATOR_H__
#define __NETWORK_CREATOR_H__

// -----------------------------------------------------------------------------
// Constants

// TODO: remove this when done with development

extern PGM int8u emAfNetworkCreatorPluginName[];

// -----------------------------------------------------------------------------
// API

/** @brief Commands the network creator to form a network
 *  with the following qualities.
 *
 *  @param desiredRadioTxPower The desired radio power to use
 *  on this network.
 *
 *  @returns Status of the commencement of the network creator
 *  process.
 */
EmberStatus emberAfPluginNetworkCreatorForm(int8s desiredRadioTxPower);

/** @brief Stops the network creator form process.
 */
void emberAfPluginNetworkCreatorStop(void);

// -----------------------------------------------------------------------------
// Callbacks

boolean emberAfPluginNetworkCreatorShouldFormNetworkCallback(int8u channel,
                                                             int8s channelRssi,
                                                             int8u numNetworks);

void emberAfPluginNetworkCreatorCompleteCallback(EmberNetworkParameters *network);

#endif /* __NETWORK_CREATOR_H__ */
