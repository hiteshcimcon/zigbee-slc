//
// network-creator-cli.c
//
// Author: Andrew Keesler <andrew.keesler@silabs.com>
//
// Copyright 2014 Silicon Laboratories, Inc.                               *80*
//

#include "app/framework/include/af.h"

#include "network-creator.h"

#if defined(EMBER_AF_GENERATE_CLI) || defined(EMBER_AF_API_COMMAND_INTERPRETER2)

// -----------------------------------------------------------------------------
// CLI Command Definitions

// plugin network-creator form  <power:1>
void emberAfPluginNetworkCreatorFormCommand()
{
  EmberStatus status;
  EmberPanId desiredPanId = (EmberPanId)emberUnsignedCommandArgument(0);
  int8s desiredRadioTxPower = (int8s)emberUnsignedCommandArgument(1);

  status = emberAfPluginNetworkCreatorForm(desiredRadioTxPower);

  emberAfCorePrintln("%p: Form: 0x%X",
                     emAfNetworkCreatorPluginName,
                     status);
}

#endif /* 
          defined(EMBER_AF_GENERATE_CLI)
          || defined(EMBER_AF_API_COMMAND_INTERPRETER2)
       */

