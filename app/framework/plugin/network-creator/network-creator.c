//
// network-creator.c
//
// Author: Andrew Keesler <andrew.keesler@silabs.com>
//
// Network creation process as specified by the Base Device Behavior
// specification.
//
// Copyright 2014 Silicon Laboratories, Inc.                               *80*

#include "app/framework/include/af.h"


#include "network-creator.h"
#include "network-creator-state.h"

#define EM_AF_PLUGIN_NETWORK_CREATOR_DEBUG

#if defined(EM_AF_PLUGIN_NETWORK_CREATOR_DEBUG) || defined(EMBER_TEST)
  #define debugPrintln(...) emberAfCorePrintln(__VA_ARGS__)
  #define EMBER_TEST_STATIC
#elif !defined(EMBER_TEST)
  #define debugPrintln(...)
  #define EMBER_TEST_STATIC static
  #if !defined(EMBER_AF_HAS_COORDINATOR_NETWORK)
    #error "The network creator plugin is only for Zigbee Coordinators."
  #endif
#endif

// -----------------------------------------------------------------------------
// Constants

PGM int8u emAfNetworkCreatorPluginName[] = "Network Creator";

// -----------------------------------------------------------------------------
// Globals

// This is what the Base Device Behavior spec says?
#define SCAN_DURATION 0x04

// This is what the Base Device Behavior spec says?
#define PRIMARY_CHANNEL_MASK                              \
  (BIT32(11) | BIT32(15) | BIT32(20) | BIT32(25))
#define SECONDARY_CHANNEL_MASK                            \
  (EMBER_ALL_802_15_4_CHANNELS_MASK ^ PRIMARY_CHANNEL_MASK)

static int8s userDesiredRadioTxPower = 0;

static int8s currentChannelRssi = 0;
static int8u currentChannelNetworks = 0;

EMBER_TEST_STATIC int8u emAfPluginNetworkCreatorStateMask;

/* state machine */
static EmberStatus stateMachineRun(void);
static EmberStatus startScan(void);
static void noteScan(boolean isComplete, void* info);
static boolean conditionallyFormNetwork(void);
static EmberStatus setupSecurity(void);
static EmberStatus gotoNextChannel(void);
static void cleanupAndStop(EmberStatus status);

// -----------------------------------------------------------------------------
// Public API Definitions

EmberStatus emberAfPluginNetworkCreatorForm(int8s desiredRadioTxPower)
{
  if ((emAfPluginNetworkCreatorStateMaskReadState()
       != EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_NONE)
      || (emberAfNetworkState() != EMBER_NO_NETWORK)) {
    debugPrintln("%p: Cannot start. Mask: 0x%X",
                 emAfNetworkCreatorPluginName,
                 emAfPluginNetworkCreatorStateMask);
    return EMBER_INVALID_CALL;
  }

  emAfPluginNetworkCreatorStateMaskInitialize();
  emAfPluginNetworkCreatorStateMaskSetState(EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_SCAN);

  userDesiredRadioTxPower = desiredRadioTxPower;
  currentChannelRssi = currentChannelNetworks = 0;

  return stateMachineRun();
}

void emberAfPluginNetworkCreatorStop()
{
  cleanupAndStop(EMBER_ERR_FATAL);
}

void emberAfNetworkFoundCallback(EmberZigbeeNetwork *networkFound,
                                 int8u lqi,
                                 int8s rssi)
{
  debugPrintln("Found network!");
  debugPrintln("  PanId: 0x%2X, Channel: %d, PJoin: %p",
                     networkFound->panId,
                     networkFound->channel,
                     (networkFound->allowingJoin ? "YES" : "NO"));
  debugPrintln("  lqi:  %d", lqi);
  debugPrintln("  rssi: %d", rssi);

  noteScan(FALSE,           // we don't know if scan has completed yet
           (void *)&(networkFound->panId));  // hand the rssi over
}

void emberAfEnergyScanResultCallback(int8u channel, int8s rssi)
{
  debugPrintln("Energy scan results.");
  debugPrintln(  "%p: Channel: %d. Rssi: %d",
                   emAfNetworkCreatorPluginName,
                   channel,
                   rssi);

  noteScan(FALSE,           // we don't know if scan has completed yet
           (void *)&rssi);  // hand the rssi over
}

void emberAfScanCompleteCallback(int8u channel, EmberStatus status)
{
  debugPrintln("%p: Scan complete. Channel: %d. Status: 0x%X",
               emAfNetworkCreatorPluginName,
               channel,
               status);

  noteScan(TRUE,              // now we know scan has completed
           (void *)&status);  // hand the status over
  stateMachineRun();
}

void emberAfScanErrorCallback(EmberStatus status)
{
  debugPrintln("%p: Scan error. Status: 0x%X",
               emAfNetworkCreatorPluginName,
               status);  

  noteScan(TRUE,              // now we know scan has completed
           (void *)&status);  // hand the status over
}

// -----------------------------------------------------------------------------
// Private API Definitions

static EmberStatus stateMachineRun(void)
{
  EmberStatus status;
  boolean networkCreatorIsDone = FALSE;
  
  debugPrintln("%p: State machine run. mask: 0x%X",
               emAfNetworkCreatorPluginName,
               emAfPluginNetworkCreatorStateMask);

  if (emAfPluginNetworkCreatorStateMaskReadState()
      & EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_SCAN) {
    status = startScan();
  } else if (emAfPluginNetworkCreatorStateMaskReadState()
             & EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_ASKING_USER) {
    status = (conditionallyFormNetwork() ? EMBER_SUCCESS : gotoNextChannel());
  } else {
    status = EMBER_ERR_FATAL;
  }

  if (status != EMBER_SUCCESS || networkCreatorIsDone) {
    cleanupAndStop(status);
  }

  debugPrintln("%p: State machine return. mask: 0x%X",
               emAfNetworkCreatorPluginName,
               emAfPluginNetworkCreatorStateMask);

  return status;
}

static EmberStatus startScan(void)
{
  EmberStatus status;

  status = emberStartScan((emAfPluginNetworkCreatorStateMaskScantypeIsActive()
                           ? EMBER_ACTIVE_SCAN
                           : EMBER_ENERGY_SCAN),
                          BIT32(emAfPluginNetworkCreatorStateMaskGetCurrentChannel()),
                          SCAN_DURATION);

  debugPrintln("%p: Scan. Channel: %d, Status: 0x%X",
               emAfNetworkCreatorPluginName,
               emAfPluginNetworkCreatorStateMaskGetCurrentChannel(),
               status);

  return status;
}

static void noteScan(boolean isComplete, void *info)
{
  if (!isComplete && info) {

    if (emAfPluginNetworkCreatorStateMaskScantypeIsActive()) {
      currentChannelNetworks ++;
    } else {
      currentChannelRssi = *((int8s *)info);
    }

  } else {

    EmberStatus status = (*(EmberStatus *)info);

    if (status != EMBER_SUCCESS) {
      cleanupAndStop(status);
    } else if (emAfPluginNetworkCreatorStateMaskScantypeIsActive()) {
      // Then you are done with this mask!
      emAfPluginNetworkCreatorStateMaskSetState(EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_ASKING_USER);
    }

    emAfPluginNetworkCreatorStateMaskToggleScantype();

  }
}

static EmberStatus conditionallyFormNetwork()
{
  EmberStatus status;
  EmberNetworkParameters networkParameters;

  if (!emberAfPluginNetworkCreatorShouldFormNetworkCallback(emAfPluginNetworkCreatorStateMaskGetCurrentChannel(),
                                                            currentChannelRssi,
                                                            currentChannelNetworks)) {
    return FALSE;
  }

  networkParameters.panId = halCommonGetRandom() & 0xFFFF;
  networkParameters.radioTxPower = userDesiredRadioTxPower;
  networkParameters.radioChannel = emAfPluginNetworkCreatorStateMaskGetCurrentChannel();

  // TODO: is this where the BDB security plugins help out?
  status = setupSecurity();
  if (status != EMBER_SUCCESS) {
    cleanupAndStop(status);
  }
    
  status = emberFormNetwork(&networkParameters);
  if (status != EMBER_SUCCESS) {
    cleanupAndStop(status);
#ifdef EM_AF_PLUGIN_NETWORK_CREATOR_DEBUG
    return FALSE;
#endif
  } else {
    emberAfPluginNetworkCreatorCompleteCallback(&networkParameters);
#ifdef EM_AF_PLUGIN_NETWORK_CREATOR_DEBUG
    return TRUE;
#endif
  }

  debugPrintln("%p: Form. panId: 0x%2X, power: %d, channel: %d, status: 0x%X",
               emAfNetworkCreatorPluginName,
               networkParameters.panId,
               networkParameters.radioTxPower,
               networkParameters.radioChannel,
               status);

#ifndef EM_AF_PLUGIN_NETWORK_CREATOR_DEBUG
    return (status == EMBER_SUCCESS ? TRUE : FALSE);
#endif
  
}

// This security stuff is copied from Rob's network-steering plugin, since he
// knows what he is doing and I don't.
static const EmberKeyData defaultLinkKey = {
  { 0x5A, 0x69, 0x67, 0x42, 0x65, 0x65, 0x41, 0x6C,
    0x6C, 0x69, 0x61, 0x6E, 0x63, 0x65, 0x30, 0x39
  },
};
static EmberStatus setupSecurity()
{
  EmberStatus status;
  EmberInitialSecurityState securityState;

  MEMSET(&securityState, 0, sizeof(EmberInitialSecurityState));
  MEMCOPY(emberKeyContents(&(securityState.preconfiguredKey)),
          emberKeyContents(&defaultLinkKey),
          EMBER_ENCRYPTION_KEY_SIZE);

  securityState.bitmask = (EMBER_HAVE_PRECONFIGURED_KEY
                           | EMBER_REQUIRE_ENCRYPTED_KEY
                           | EMBER_NO_FRAME_COUNTER_RESET);

  status = emberSetInitialSecurityState(&securityState);

  debugPrintln("%p: Set initial security state. mask: 0x%2X, status: 0x%X",
               emAfNetworkCreatorPluginName,
               securityState.bitmask,
               status);

  return status;
}

// TODO: this function could be nicer
static EmberStatus gotoNextChannel()
{
  int8u nextChannel = emAfPluginNetworkCreatorStateMaskGetCurrentChannel() + 1;
  int32u currentChannelMask = (emAfPluginNetworkCreatorStateMaskChannelsAreSecondary()
                               ? SECONDARY_CHANNEL_MASK
                               : PRIMARY_CHANNEL_MASK);
  debugPrintln("currentChannel: %d, currentChannelMask: 0x%4X",
               nextChannel-1,
               currentChannelMask);
  
  if (nextChannel > EMBER_MAX_802_15_4_CHANNEL_NUMBER) {
    if (!emAfPluginNetworkCreatorStateMaskChannelsAreSecondary()) {
      // Then you are on the primary channel mask, so move to secondary.
      emAfPluginNetworkCreatorStateMaskToggleChannelMask();
      emAfPluginNetworkCreatorStateMaskSetCurrentChannel(EMBER_MIN_802_15_4_CHANNEL_NUMBER);
      return EMBER_SUCCESS;
    } else {
      // Then you have run out of channels on the secondary channel mask - done!
      return EMBER_PHY_INVALID_CHANNEL;
    }
  }

  // Find next 802.15.4 channel that is in the current channel mask.
  while ((nextChannel <= EMBER_MAX_802_15_4_CHANNEL_NUMBER)
          && (!READBIT(currentChannelMask, nextChannel))) {
    nextChannel ++;        
  }

  if (nextChannel <= EMBER_MAX_802_15_4_CHANNEL_NUMBER) {
    emAfPluginNetworkCreatorStateMaskSetCurrentChannel(nextChannel);
    debugPrintln("now the current channel: %d",
                 emAfPluginNetworkCreatorStateMaskGetCurrentChannel());
    return EMBER_SUCCESS;
  } else {
    return gotoNextChannel();
  }
}

static void cleanupAndStop(EmberStatus status)
{
  debugPrintln("%p: Stop. Status: 0x%X. Mask: 0x%X",
               emAfNetworkCreatorPluginName,
               status,
               emAfPluginNetworkCreatorStateMask);
  
  emAfPluginNetworkCreatorStateMaskSetState(EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_NONE);
}
