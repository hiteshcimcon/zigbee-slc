//
// network-creator-state.h
//
// Andrew Keesler
//
// August 28, 2014
//
// Internal header for state mask.

#ifndef __NETWORK_CREATOR_STATE_H__
#define __NETWORK_CREATOR_STATE_H__

extern int8u emAfPluginNetworkCreatorStateMask;
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_MASK     (BIT(0) | BIT(1))
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_OFFSET   (0)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_NONE                     \
  (0 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_OFFSET)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_SCAN                     \
  (1 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_OFFSET)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_ASKING_USER              \
  (2 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_OFFSET)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_FORMING                  \
  (3 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_OFFSET)

#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_MASK           (BIT(2))
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_OFFSET         (2)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_PRIMARY        \
  (0 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_OFFSET)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_SECONDARY      \
  (1 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_OFFSET)

#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_MASK           (BIT(3))
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_OFFSET         (3)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_ENERGY         \
  (0 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_OFFSET)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_ACTIVE         \
  (1 << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_OFFSET)

#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_MASK    \
  (BIT(4) | BIT(5) | BIT(6) | BIT(7))
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_OFFSET  (4)
#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_INITIAL (0)

#define EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_INITIAL                      \
  (EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_SCAN                        \
   | EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_PRIMARY                \
   | EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_ENERGY                 \
   | EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_INITIAL)

// -----------------------------------------------------------------------------
// Private API Declarations

/* state mask */
#define emAfPluginNetworkCreatorStateMaskInitialize()                        \
  (emAfPluginNetworkCreatorStateMask                                         \
   = EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_INITIAL)

#define emAfPluginNetworkCreatorStateMaskReadState()                         \
  (emAfPluginNetworkCreatorStateMask                                         \
   & EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_MASK)
#define emAfPluginNetworkCreatorStateMaskSetState(value)                     \
  emAfPluginNetworkCreatorStateMask                                          \
  = ((emAfPluginNetworkCreatorStateMask                                      \
      & ~EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_STATE_MASK)                 \
     | (value))

#define emAfPluginNetworkCreatorStateMaskChannelsAreSecondary()              \
  (emAfPluginNetworkCreatorStateMask                                         \
   & EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_MASK)

#define emAfPluginNetworkCreatorStateMaskToggleChannelMask()                    \
  (emAfPluginNetworkCreatorStateMask                                         \
   ^= EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CHANNELS_MASK)

#define emAfPluginNetworkCreatorStateMaskScantypeIsActive()                  \
  (emAfPluginNetworkCreatorStateMask                                         \
   & EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_MASK)
#define emAfPluginNetworkCreatorStateMaskToggleScantype()                    \
  (emAfPluginNetworkCreatorStateMask                                         \
   ^= EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_SCANTYPE_MASK)

#define emAfPluginNetworkCreatorStateMaskGetCurrentChannel()                 \
  (((emAfPluginNetworkCreatorStateMask                                       \
     & EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_MASK)         \
    >> EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_OFFSET)       \
   + EMBER_MIN_802_15_4_CHANNEL_NUMBER)
#define channelToUpper4BitsOfByte(channel)                                   \
  (((int8u)(channel) - EMBER_MIN_802_15_4_CHANNEL_NUMBER)                    \
   << EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_OFFSET)
#define emAfPluginNetworkCreatorStateMaskSetCurrentChannel(channel)          \
  emAfPluginNetworkCreatorStateMask                                          \
  = ((emAfPluginNetworkCreatorStateMask                                      \
      & ~EM_AF_PLUGIN_NETWORK_CREATOR_STATE_MASK_CURRENT_CHANNEL_MASK)       \
     | channelToUpper4BitsOfByte(channel));

#endif /* __NETWORK_CREATOR_STATE_H__ */
