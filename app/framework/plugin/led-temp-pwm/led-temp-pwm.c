// *******************************************************************
// * led-temp-pwm.c
// *
// * Implements the color control server for color temperature bulbs.  Note: 
// * this is HA certifable and has passed HA certification for one customer
// * project.  
// *
// * Copyright 2015 by Silicon Laboratories. All rights reserved.           *80*
// *******************************************************************

#include "app/framework/include/af.h"
#include "app/framework/util/attribute-storage.h"

#include "hal/micro/cortexm3/diagnostic.h"
#include "app/framework/plugin/bulb-pwm-configuration/bulb-config.h"

#ifdef EMBER_AF_PLUGIN_SCENES
  #include "app/framework/plugin/scenes/scenes.h"
#endif //EMBER_AF_PLUGIN_SCENES

#ifdef EMBER_AF_PLUGIN_ON_OFF
  #include "app/framework/plugin/on-off/on-off.h"
#endif //EMBER_AF_PLUGIN_ON_OFF

#ifdef EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER
  #include "app/framework/plugin/zll-level-control-server/zll-level-control-server.h"
#endif //EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER

enum {
  EMBER_ZCL_COLOR_TEMP_MOVE_MODE_STOP = 0x00,
  EMBER_ZCL_COLOR_TEMP_MOVE_MODE_UP   = 0x01,
  EMBER_ZCL_COLOR_TEMP_MOVE_MODE_DOWN = 0x03,
}; 

// ----- declarations for the color transition events --------
#define colorTempTransitionEventControl  emberAfPluginLedTempPwmTransitionEventControl

EmberEventControl colorTempTransitionEventControl;

#define UPDATE_TIME_MS 100
typedef struct {
  int16u initialTemp;
  int16u currentTemp;
  int16u finalTemp;
  int16u stepsRemaining;
  int16u stepsTotal;
  int16u endpoint;
} ColorTransitionState;

static ColorTransitionState colorTransitionState;

// ---------- Hardware values required for computing drive levels ----------
static int16u minColor, maxColor;
static int16u minPwmDrive, maxPwmDrive;

#define MIN_COLOR_DEFAULT 155
#define MAX_COLOR_DEFAULT 360

static int8u currentEndpoint( void )
{
  int8u fixedEndpoints[] = FIXED_ENDPOINT_ARRAY;

  return(fixedEndpoints[0]);
}

static void driveWRGB( int16u white, int16u red, int16u green, int16u blue )
{
  emberAfPluginBulbConfigDriveWRGB(white, red, green, blue);
}


static void updateDriveLevel( int8u endpoint);
void emberAfPluginLedTempPwmInitCallback( void )
{
  int8u endpoint = currentEndpoint();
  EmberAfStatus status;

  minPwmDrive = emberAfPluginBulbConfigMinDriveValue();
  maxPwmDrive = emberAfPluginBulbConfigMaxDriveValue();

  status = emberAfReadServerAttribute(endpoint,
                                      ZCL_COLOR_CONTROL_CLUSTER_ID,
                                      ZCL_COLOR_CONTROL_COLOR_TEMP_PHYSICAL_MIN_ATTRIBUTE_ID,
                                      (int8u *)&minColor,
                                      sizeof(minColor));

  if(status != EMBER_ZCL_STATUS_SUCCESS) {
    emberSerialPrintf(APP_SERIAL, "Color Temp:  no color temp physical min attribute.\r\n");
    minColor = MIN_COLOR_DEFAULT;
  }

  status = emberAfReadServerAttribute(endpoint,
                                      ZCL_COLOR_CONTROL_CLUSTER_ID,
                                      ZCL_COLOR_CONTROL_COLOR_TEMP_PHYSICAL_MAX_ATTRIBUTE_ID,
                                      (int8u *)&maxColor,
                                      sizeof(maxColor));

  if(status != EMBER_ZCL_STATUS_SUCCESS) {
    emberSerialPrintf(APP_SERIAL, "Color Temp:  no color temp physical max attribute.\r\n");
    maxColor = MAX_COLOR_DEFAULT;
  }
  
  emberSerialPrintf(APP_SERIAL, "Color Temp Init %d %d %d %d\r\n", minPwmDrive, maxPwmDrive, minColor, maxColor);

  updateDriveLevel(endpoint);
}

static void computeRgbFromColorTemp( int8u endpoint )
{
  int16u currentTemp;
  int8u onOff, currentLevel;

  //emberSerialPrintf(APP_SERIAL, "COMPUTE RGB\r\n");
  int32u R32, W32;
  int16u rDrive, wDrive;

  // during framework init, this funciton sometimes is called before we set up
  // the values for max/min color temperautre.  
  if(maxColor == 0 ||
    minColor == 0)
    return;

  emberAfReadServerAttribute(endpoint,
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMPERATURE_ATTRIBUTE_ID,
                             (int8u *)&currentTemp,
                             sizeof(currentTemp));
  
  emberAfReadServerAttribute(endpoint,
                             ZCL_LEVEL_CONTROL_CLUSTER_ID,
                             ZCL_CURRENT_LEVEL_ATTRIBUTE_ID,
                             (int8u *)&currentLevel,
                             sizeof(currentLevel));

  emberAfReadServerAttribute(endpoint,
                             ZCL_ON_OFF_CLUSTER_ID,
                             ZCL_ON_OFF_ATTRIBUTE_ID,
                             (int8u *)&onOff,
                             sizeof(onOff));

  if(onOff == 0 || currentLevel == 0) {
    driveWRGB(0,0,0,0);
    
    return;
  }

  //bounds checking of the attribute temp. 
  if(currentTemp > maxColor)
    currentTemp = maxColor;
  else if(currentTemp < minColor)
    currentTemp = minColor;
     
  // First, compute the R and W transfer
  W32 = maxPwmDrive - (minPwmDrive *6);
  W32 *= (currentTemp - minColor);
  W32 /= (maxColor - minColor);
  W32 += (minPwmDrive *6);

  R32 = maxPwmDrive - (minPwmDrive *6);
  R32 *= (maxColor - currentTemp);
  R32 /= (maxColor - minColor);
  R32 += (minPwmDrive *6);

  // Handle level
  R32 *= (currentLevel - 1);
  R32 /= 253;
  if(R32 < minPwmDrive)
    R32 = 0;

  W32 *= (currentLevel - 1);
  W32 /= 253;
  if(W32 < minPwmDrive)
    W32 = 0;

  // convert to int16u and drive the PWMs.
  rDrive = (int16u) R32;
  wDrive = (int16u) W32;
  
  //emberAfAppPrintln("Setting level now: %d", rDrive);

  driveWRGB(wDrive, rDrive, 0,0);

}

/** @brief Move To Color
 *
 * 
 *
 * @param colorX   Ver.: always
 * @param colorY   Ver.: always
 * @param transitionTime   Ver.: always
 */
boolean emberAfColorControlClusterMoveToColorCallback(int16u colorX,
                                                      int16u colorY,
                                                      int16u transitionTime)
{
  return TRUE;
}

/** @brief Server Attribute Changedyes.
 *
 * Level Control cluster, Server Attribute Changed
 *
 * @param endpoint Endpoint that is being initialized  Ver.: always
 * @param attributeId Attribute that changed  Ver.: always
 */
void emberAfLevelControlClusterServerAttributeChangedCallback(int8u endpoint,
                                                              EmberAfAttributeId attributeId)
{
  computeRgbFromColorTemp( endpoint );
}

void emberAfOnOffClusterServerAttributeChangedCallback(int8u endpoint, 
                                                       EmberAfAttributeId attributeId)
{
  emberAfLevelControlClusterServerAttributeChangedCallback( endpoint, attributeId );
}

/** @brief Color Control Cluster Server Attribute Changed
 *
 * Server Attribute Changed
 *
 * @param endpoint Endpoint that is being initialized  Ver.: always
 * @param attributeId Attribute that changed  Ver.: always
 */
void emberAfColorControlClusterServerAttributeChangedCallback(int8u endpoint,
                                                              EmberAfAttributeId attributeId)
{
  emberAfLevelControlClusterServerAttributeChangedCallback( endpoint, attributeId );
}

/** @brief Move To Color Temperature
 *
 * 
 *
 * @param colorTemperature   Ver.: always
 * @param transitionTime   Ver.: always
 */

static void kickOffTemperatureTransition( int16u currentTemp, 
                                          int16u newTemp, 
                                          int16u transitionTime,
                                          int8u endpoint);

boolean emberAfColorControlClusterMoveToColorTemperatureCallback(int16u colorTemperature,
                                                                 int16u transitionTime)
{
  int8u endpoint = emberAfCurrentEndpoint();
  int16u currentTemp;

  emberAfReadServerAttribute(endpoint,
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMPERATURE_ATTRIBUTE_ID,
                             (int8u *)&currentTemp,
                             sizeof(currentTemp));

  kickOffTemperatureTransition( currentTemp, 
                                colorTemperature, 
                                transitionTime, 
                                endpoint);
 
  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);

  return TRUE;
}

/** @brief Move To Hue And Saturation
 *
 * 
 *
 * @param hue   Ver.: always
 * @param saturation   Ver.: always
 * @param transitionTime   Ver.: always
 */
boolean emberAfColorControlClusterMoveToHueAndSaturationCallback(int8u hue,
                                                                 int8u saturation,
                                                                 int16u transitionTime)
{
  return TRUE;
}

static void updateDriveLevel( int8u endpoint )
{
  emberAfLevelControlClusterServerAttributeChangedCallback( endpoint, 0x0000 );
}

void emberAfPluginLedTempPwmTransitionEventHandler( void )
{
  int16u currentTemp;
  ColorTransitionState *p = &colorTransitionState;
  int32u currentTemp32u;

  (p->stepsRemaining)--;

  if(p->finalTemp == p->currentTemp) {
    currentTemp = p->currentTemp;
  } else if(p->finalTemp > p->currentTemp) {
    currentTemp32u  = ((int32u) (p->finalTemp - p->initialTemp));
    currentTemp32u *= ((int32u) (p->stepsRemaining));
    currentTemp32u /= ((int32u) (p->stepsTotal));
    currentTemp = p->finalTemp - ((int16u) (currentTemp32u));
  } else {
    currentTemp32u  = ((int32u) (p->initialTemp - p->finalTemp));
    currentTemp32u *= ((int32u) (p->stepsRemaining));
    currentTemp32u /= ((int32u) (p->stepsTotal));
    currentTemp = p->finalTemp + ((int16u) (currentTemp32u));
  }

  // need to write colorX and colorY here.  Note:  I am ignoring transition 
  // time for now.  
  emberAfWriteAttribute(colorTransitionState.endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_COLOR_TEMPERATURE_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&currentTemp,
                        ZCL_INT16U_ATTRIBUTE_TYPE);

  //computeRgbFromColorTemp(colorTransitionState.endpoint);
  
  p->currentTemp = currentTemp;

  // Write steps remaining here.  Note:  I am asuming 1/10 of a second per
  // step.
  emberAfWriteAttribute(colorTransitionState.endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_REMAINING_TIME_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&(p->stepsRemaining),
                        ZCL_INT16U_ATTRIBUTE_TYPE);


  if(p->stepsRemaining > 0) {
    emberEventControlSetDelayMS( colorTempTransitionEventControl, 
                                 UPDATE_TIME_MS );
  } else {
    emberEventControlSetInactive( colorTempTransitionEventControl );
  }
}

static void stopColorTempTransition( void )
{
  int16u stepsRemaining = 0;
  
  emberAfWriteAttribute(colorTransitionState.endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_REMAINING_TIME_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&stepsRemaining,
                        ZCL_INT16U_ATTRIBUTE_TYPE);

  emberEventControlSetInactive( colorTempTransitionEventControl );
}

static void kickOffTemperatureTransition( int16u currentTemp, 
                                          int16u newTemp, 
                                          int16u transitionTime,
                                          int8u endpoint) {
  int32u stepsRemaining32u;
  int16u stepsRemaining, frameworkMin, frameworkMax;

  // first, make sure we are transitioning into a valid temperature range.
  emberAfReadServerAttribute(currentEndpoint(),
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMP_PHYSICAL_MIN_ATTRIBUTE_ID,
                             (int8u *) &frameworkMin,
                             sizeof(frameworkMin));

  emberAfReadServerAttribute(currentEndpoint(),
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMP_PHYSICAL_MAX_ATTRIBUTE_ID,
                             (int8u *) &frameworkMax,
                             sizeof(frameworkMax));

  if(newTemp > frameworkMax) 
    newTemp = frameworkMax;
  if(newTemp < frameworkMin)
    newTemp = frameworkMin;

  emberSerialPrintf(APP_SERIAL, "kick:  %d %d %d %x\r\n", currentTemp, newTemp, transitionTime, endpoint);

  stepsRemaining32u = (int32u) transitionTime;
  stepsRemaining32u *= 100; // convert to mS
  stepsRemaining32u /= (int32u) UPDATE_TIME_MS;
  stepsRemaining32u++;

  if(stepsRemaining32u > 0xffff)
    stepsRemaining32u--;

  stepsRemaining = (int16u) stepsRemaining32u;
  // making an immediate jump, so there is N+1 steps.

  colorTransitionState.initialTemp = currentTemp;
  colorTransitionState.currentTemp = currentTemp;
  colorTransitionState.finalTemp = newTemp;
  colorTransitionState.stepsRemaining = stepsRemaining;
  colorTransitionState.stepsTotal = stepsRemaining;
  colorTransitionState.endpoint = endpoint;

  emberEventControlSetActive( colorTempTransitionEventControl );
}

// new functions for HA compliance
// Note:  need to clean things up a bit.
static int16u checkMinMaxValues( int16u *commandMin, int16u *commandMax)
{
  int16u frameworkMin, frameworkMax, currentTemp;

  emberAfReadServerAttribute(currentEndpoint(),
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMP_PHYSICAL_MIN_ATTRIBUTE_ID,
                             (int8u *) &frameworkMin,
                             sizeof(frameworkMin));

  emberAfReadServerAttribute(currentEndpoint(),
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMP_PHYSICAL_MAX_ATTRIBUTE_ID,
                             (int8u *) &frameworkMax,
                             sizeof(frameworkMax));

  emberAfReadServerAttribute(currentEndpoint(),
                             ZCL_COLOR_CONTROL_CLUSTER_ID,
                             ZCL_COLOR_CONTROL_COLOR_TEMPERATURE_ATTRIBUTE_ID,
                             (int8u *)&currentTemp,
                             sizeof(currentTemp));

  // first, check to see if either is outside bounds
  if(*commandMin < frameworkMin) {
    *commandMin = frameworkMin;
  }

  if(*commandMax > frameworkMax ||
     *commandMax == 0) {
    *commandMax = frameworkMax;
  }

  // next, check to make sure the max is above the current temp, and min is
  // below the current temp.
  if(*commandMin > currentTemp) {
    *commandMin = currentTemp;
  }

  if(*commandMax < currentTemp) {
    *commandMax = currentTemp;
  }

  return currentTemp;
  
}

boolean emberAfColorControlClusterMoveColorTemperatureCallback(int8u moveMode,
                                                               int16u rate,
                                                               int16u colorTemperatureMinimum,
                                                               int16u colorTemperatureMaximum)
{
  int16u currentTemp =
    checkMinMaxValues( &colorTemperatureMinimum, &colorTemperatureMaximum);
  int16u transitTime;

  switch(moveMode) {
  case EMBER_ZCL_COLOR_TEMP_MOVE_MODE_STOP:
    stopColorTempTransition();
    break;
  case EMBER_ZCL_COLOR_TEMP_MOVE_MODE_UP:
    transitTime = colorTemperatureMaximum - currentTemp;
    transitTime *= (1000 / UPDATE_TIME_MS);
    transitTime /= rate;

    kickOffTemperatureTransition( currentTemp,
                                  colorTemperatureMaximum,
                                  transitTime,
                                  currentEndpoint() );
    break;
  case EMBER_ZCL_COLOR_TEMP_MOVE_MODE_DOWN:
    transitTime = currentTemp - colorTemperatureMinimum;
    transitTime *= (1000 / UPDATE_TIME_MS);
    transitTime /= rate;

    kickOffTemperatureTransition( currentTemp,
                                  colorTemperatureMinimum,
                                  transitTime,
                                  currentEndpoint() );
    break;
  } 

  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);

  return TRUE;
}

boolean emberAfColorControlClusterStepColorTemperatueCallback(int8u stepMode,
                                                              int16u stepSize,
                                                              int16u transitionTime,
                                                              int16u colorTemperatureMinimum,
                                                              int16u colorTemperatureMaximum)
{
  int16u currentTemp =
    checkMinMaxValues( &colorTemperatureMinimum, &colorTemperatureMaximum);
  int16u newTemp;

  switch(stepMode) {
  case EMBER_ZCL_COLOR_TEMP_MOVE_MODE_STOP:
    stopColorTempTransition();
    break;
  case EMBER_ZCL_COLOR_TEMP_MOVE_MODE_UP:

    newTemp = currentTemp + stepSize;

    if(newTemp > colorTemperatureMaximum)
      newTemp = colorTemperatureMaximum;

    kickOffTemperatureTransition( currentTemp, 
                                  newTemp, 
                                  transitionTime,
                                  currentEndpoint() );
    break;
  case EMBER_ZCL_COLOR_TEMP_MOVE_MODE_DOWN:

    newTemp = currentTemp - stepSize;

    if(newTemp < colorTemperatureMinimum ||
      stepSize > currentTemp )
      newTemp = colorTemperatureMinimum;

    kickOffTemperatureTransition( currentTemp, 
                                  newTemp, 
                                  transitionTime,
                                  currentEndpoint() );
    break;
  } 

  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);

  return TRUE;
}

boolean emberAfColorControlClusterStopMoveStepCallback(void)
{
  stopColorTempTransition();

  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);

  return TRUE;
}

// **********************************************
// LED Output Blinking State

void emberAfPluginBulbPwmConfigurationBlinkStopCallback( int8u endpoint )
{
  updateDriveLevel( endpoint );
}

