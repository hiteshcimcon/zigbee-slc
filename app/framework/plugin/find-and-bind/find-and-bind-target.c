//
// find-and-bind-target.c
//
// Author: Andrew Keesler <andrew.keesler@silabs.com>
//
// Target functionality as described in the Base Device Behavior
// spec.
//
// Copyright 2014 Silicon Laboratories, Inc.                               *80*

#include "app/framework/include/af.h"

#include "find-and-bind.h"

#include ATTRIBUTE_STORAGE_CONFIGURATION //TODO: for FIXED_ENDPOINT_COUNT?

#define EM_PLUGIN_FIND_AND_BIND_TARGET_DEBUG

#ifdef  EM_PLUGIN_FIND_AND_BIND_TARGET_DEBUG
#define emberAfPluginFindAndBindGetTargetEndpointsCallback(endpoints) \
  endpoints[0] = 1;
#endif  /* EM_PLUGIN_FIND_AND_BIND_TARGET_DEBUG */

// -----------------------------------------------------------------------------
// Constants

// This comes from the same spec variable as
// ::EMBER_AF_PLUGIN_NETWORK_STEERING_PERMIT_JOIN_MIN_DURATION
// from network-steering.c.
#define BDB_MIN_COMMISSION_TIME 180

// -----------------------------------------------------------------------------
// Private API Prototypes

static EmberStatus writeIdentifyTime(void);

// -----------------------------------------------------------------------------
// Public API

EmberStatus emberAfPluginFindAndBindTarget(void)
{
  return writeIdentifyTime();
}

// -----------------------------------------------------------------------------
// Private API

static EmberStatus writeIdentifyTime(void)
{
  int8u i;
  int8u identifyTime = BDB_MIN_COMMISSION_TIME;
  int8u targetEndpoints[FIXED_ENDPOINT_COUNT];
  EmberStatus status;  

  emberAfPluginFindAndBindGetTargetEndpointsCallback(targetEndpoints);
                                         
  for (i = 0; i < FIXED_ENDPOINT_COUNT; i ++) {
    if (emberAfContainsCluster(targetEndpoints[i], ZCL_IDENTIFY_CLUSTER_ID)) {
      status = emberAfWriteAttribute(targetEndpoints[i],
                                     ZCL_IDENTIFY_CLUSTER_ID,
                                     ZCL_IDENTIFY_TIME_ATTRIBUTE_ID,
                                     CLUSTER_MASK_SERVER,
                                     (int8u *)&identifyTime,
                                     ZCL_INT16U_ATTRIBUTE_TYPE);
    }
    if (status != EMBER_SUCCESS) {
      targetEndpoints[i] = EMBER_ZDP_INVALID_ENDPOINT;
    }
  }

  // debug
  emberAfCorePrint("%p: Endpoints set to identify: ", emAfFindAndBindPluginName);
  for (i = 0; i < FIXED_ENDPOINT_COUNT; i ++) {
    if (targetEndpoints[i] != EMBER_ZDP_INVALID_ENDPOINT)
      emberAfCorePrint("%d ", targetEndpoints[i]);
  }
  emberAfCorePrintln("");

  return status;
}

