//
// find-and-bind.h
//
// Author: Andrew Keesler <andrew.keesler@silabs.com>
//
// Copyright 2014 Silicon Laboratories, Inc.                               *80*

#ifndef __FIND_AND_BIND_H__
#define __FIND_AND_BIND_H__

// -----------------------------------------------------------------------------
// Constants

extern PGM int8u emAfFindAndBindPluginName[];

// -----------------------------------------------------------------------------
// API

/* target */
EmberStatus emberAfPluginFindAndBindTarget(void);

void emberAfPluginFindAndBingGetTargetEndpointsCallback(int8u *endpoints);

/* initiator */
EmberStatus emberAfPluginFindAndBindInitiator(void);

int8u emberAfPluginFindAndBindGetInitiatorEndpointCallback(void);
boolean emberAfPluginFindAndBindFoundBindTargetCallback(EmberNodeId nodeId,
                                                        int8u endpoint,
                                                        EmberEUI64 ieeeAddr,
                                                        int16u clusterId);
void emberAfPluginFindAndBindInitiatorCompleteCallback(EmberStatus status);

// -----------------------------------------------------------------------------
// Declarations

extern EmberEventControl emberAfPluginFindAndBindCheckTargetResponsesEventControl;
void emberAfPluginFindAndBindCheckTargetResponsesEventHandler();

#endif /* __FIND_AND_BIND_H__ */
