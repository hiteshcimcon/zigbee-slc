// *******************************************************************
// * led-dim-pwm.c
// *
// *
// * Copyright 2015 by Silicon Laboratories. All rights reserved.           *80*
// *******************************************************************

#include "app/framework/include/af.h"
#include "app/framework/util/attribute-storage.h"

#ifdef EMBER_AF_PLUGIN_SCENES
  #include "app/framework/plugin/scenes/scenes.h"
#endif //EMBER_AF_PLUGIN_SCENES

#ifdef EMBER_AF_PLUGIN_ON_OFF
  #include "app/framework/plugin/on-off/on-off.h"
#endif //EMBER_AF_PLUGIN_ON_OFF

#ifdef EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER
  #include "app/framework/plugin/zll-level-control-server/zll-level-control-server.h"
#endif //EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER

#include "app/framework/plugin/bulb-pwm-configuration/bulb-config.h"

#include "led-dim-pwm-transform.h"

static int8u minLevel;
static int8u maxLevel;
static int16u minPwmDrive, maxPwmDrive;

// Use precalculated values for the PWM based on a 1 kHz frequency to achieve
// the proper perceived LED output.  
int16u pwmValues[] = { PWM_VALUES };

static void updateDriveLevel( int8u endpoint);
void emberAfPluginLedDimPwmInitCallback( void )
{
  int8u fixedEndpoints[] = FIXED_ENDPOINT_ARRAY;

  halInternalPowerUpBoard(); // redoing this here just in case.

  minPwmDrive = emberAfPluginBulbConfigMinDriveValue();
  maxPwmDrive = emberAfPluginBulbConfigMaxDriveValue();

  // Set the min and max levels
#ifdef EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER
  minLevel = EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER_MINIMUM_LEVEL;
  maxLevel = EMBER_AF_PLUGIN_ZLL_LEVEL_CONTROL_SERVER_MAXIMUM_LEVEL;
#else
  minLevel = EMBER_AF_PLUGIN_LEVEL_CONTROL_MINIMUM_LEVEL;
  maxLevel = EMBER_AF_PLUGIN_LEVEL_CONTROL_MAXIMUM_LEVEL;
#endif

  updateDriveLevel(fixedEndpoints[0]);
}

static void pwmSetValue( int16u value )
{
  emberAfPluginBulbConfigDrivePwm(value);
}

// update drive level based on linear power delivered to the light
static int16u updateDriveLevelLumens( int8u endpoint)
{
  int32u driveScratchpad;
  int8u currentLevel, mappedLevel;
  int16u newDrive;
  
  emberAfReadServerAttribute(endpoint,
                             ZCL_LEVEL_CONTROL_CLUSTER_ID,
                             ZCL_CURRENT_LEVEL_ATTRIBUTE_ID,
                             (int8u *)&currentLevel,
                             sizeof(currentLevel));

  // First handle the degenerate case.  
  if(currentLevel == 0)
    return 0;

  // first, map the drive level into the size of the table
  // We have a 255 entry table that goes from 0 to 6000.
  // use 32 bit math to avoid losing information.
  driveScratchpad = currentLevel - minLevel;
  driveScratchpad *= PWM_VALUES_LENGTH;
  driveScratchpad /= (maxLevel - minLevel);
  mappedLevel = (int8u) driveScratchpad;

  driveScratchpad = (int32u) pwmValues[ mappedLevel ];

  // newDrive now is mapped to 0..6000.  We need to remap it 
  // to WHITE_MIMIMUM_ON_VALUE..WHITE_MAXIMUM_ON_VALUE
  // use 32 bit math to avoid losing information.
  driveScratchpad = driveScratchpad * (maxPwmDrive-minPwmDrive);
  driveScratchpad = driveScratchpad / 6000;
  driveScratchpad += minPwmDrive;
  newDrive = (int16u) driveScratchpad;

  return newDrive;
}  

static void updateDriveLevel( int8u endpoint)
{
  boolean isOn;

  // updateDriveLevel is called before maxLevel has been initialzied.  So if
  // maxLevel is zero, we need to set the PWM to 0 for now and exit.  This 
  // will be called again after maxLevel has been initialzied.  
  if(maxLevel == 0) {

    pwmSetValue( 0 );
    return;
  }

  emberAfReadServerAttribute(endpoint,
                             ZCL_ON_OFF_CLUSTER_ID,
                             ZCL_ON_OFF_ATTRIBUTE_ID,
                             (int8u *)&isOn,
                             sizeof(boolean));

  if(isOn) {
    pwmSetValue( updateDriveLevelLumens( endpoint ) );
  } else {
    pwmSetValue(0);
  }
}

/** @brief Server Attribute Changedyes.
 *
 * Level Control cluster, Server Attribute Changed
 *
 * @param endpoint Endpoint that is being initialized  Ver.: always
 * @param attributeId Attribute that changed  Ver.: always
 */
void emberAfLevelControlClusterServerAttributeChangedCallback(int8u endpoint,
                                                              EmberAfAttributeId attributeId)
{
  updateDriveLevel( endpoint );
}

void emberAfOnOffClusterServerAttributeChangedCallback(int8u endpoint, 
                                                       EmberAfAttributeId attributeId)
{
  updateDriveLevel( endpoint );
}

void emAfLedDimPwmTestCommand(void)
{
  int16u pwmValue = (int16u)emberUnsignedCommandArgument(0);

  if(pwmValue > 0 && pwmValue < minPwmDrive)
    pwmValue = minPwmDrive;

  if(pwmValue > maxPwmDrive)
    pwmValue = maxPwmDrive;

  emberAfAppPrintln("White PWM Value:  %2x",pwmValue);

  pwmSetValue( pwmValue );

}

// **********************************************
// LED Output Blinking State

void emberAfPluginBulbPwmConfigurationBlinkStopCallback( int8u endpoint )
{
  updateDriveLevel( endpoint );
}

