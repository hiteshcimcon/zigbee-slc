// *****************************************************************************
// * ias-zone-server-cli.c
// *
// * This is the source for the command line interface used for the ias zone
// * server plugin.
// *
// * Copyright 2015 Silicon Laboratories, Inc.                              *80*
// *****************************************************************************

#include "af.h"
#include "ias-zone-server.h"

//-----------------------------------------------------------------------------
// Globals

#define RESERVED_END 0x7FFF
#define MANUFACTURER_SPECIFIC_START 0x8000
#define MANUFACTURER_SPECIFIC_END   0xFFFE

void emberAfPluginIasZoneServerInfoCommand(void);
void emberAfPluginIasZoneServerChangeStatusCommand(void);

static const char* infoArguments[] = {
  "endpoint",
  NULL,
};

static const char* changeStatusArguments[] = {
  "new-status",
  "time-since-occurred-seconds",
  "endpoint",
  NULL,
};

EmberCommandEntry emberAfPluginIasZoneServerCommands[] = {
  emberCommandEntryActionWithDetails("info",
                                     emberAfPluginIasZoneServerInfoCommand,
                                     "",
                                     "Print IAS Zone information",
                                     infoArguments),
  emberCommandEntryActionWithDetails("change-status", 
                                     emberAfPluginIasZoneServerChangeStatusCommand, 
                                     "vu", 
                                     "Change the current Zone Status",
                                     changeStatusArguments),
  emberCommandEntryTerminator(),
};

typedef struct {
  int16u zoneType;
  const char* zoneTypeString;
} ZoneTypeToStringMap;

// These functions and variables are only used to generate strings used with
// emberAfIasZoneClusterPrintln calls, so if EMBER_AF_PRINT_IAS_ZONE_CLUSTER is
// not defined and they are compiled in, these declarations generate "function
// declared but never referenced" warnings
#if defined(EMBER_AF_PRINT_ENABLE) && defined(EMBER_AF_PRINT_IAS_ZONE_CLUSTER)
static ZoneTypeToStringMap zoneTypeToStringMap[] = {
  { 0x0000, "Standard CIE" },
  { 0x000d, "Motion Sensor" },
  { 0x0015, "Contact Switch" },
  { 0x0028, "Fire Sensor" },
  { 0x002a, "Water Sensor" },
  { 0x002b, "Gas Sensor" },
  { 0x002c, "Peersonal Emergency Device" },
  { 0x002d, "Vibration / Movement Sensor" },
  { 0x010f, "Remote Control" },
  { 0x0115, "Key Fob" },
  { 0x021d, "Keypad" },
  { 0x0225, "Standard Warning Device" },
  { 0xFFFF, NULL } // terminator
};

static const char manufacturerSpecificString[] = "Manufacturer Specific";
static const char invalidZoneTypeString[] = "Invalid";
static const char reservedString[] = "Reserved";

static const char notEnrolledString[] = "NOT Enrolled";
static const char enrolledString[] = "Enrolled";
static const char unknownZoneStateString[] = "Unknown";

//-----------------------------------------------------------------------------
// Functions

static const char* getZoneTypeString(int16u type)
{
  int16u i = 0;
  while (zoneTypeToStringMap[i].zoneTypeString != NULL) {
    if (zoneTypeToStringMap[i].zoneType == type)
    return zoneTypeToStringMap[i].zoneTypeString;
    i++;
  }

  if (type <= RESERVED_END) {
    return reservedString;
  }

  if (type >= MANUFACTURER_SPECIFIC_START
      && type <= MANUFACTURER_SPECIFIC_END) {
    return manufacturerSpecificString;
  }

  return invalidZoneTypeString;
}

static const char* getZoneStateString(int8u zoneState)
{
  switch (zoneState) {
    case EMBER_ZCL_IAS_ZONE_STATE_ENROLLED:
      return enrolledString;
    case EMBER_ZCL_IAS_ZONE_STATE_NOT_ENROLLED:
      return notEnrolledString;
  }
  return unknownZoneStateString;
}
#endif //defined(EMBER_AF_PRINT_ENABLE) 
       //&& defined(EMBER_AF_PRINT_IAS_ZONE_CLUSTER)

static void getAttributes(int8u*  returnCieAddress, 
                          int16u* returnZoneStatus,
                          int16u* returnZoneType,
                          int8u*  returnZoneState,
                          int8u   endpoint)
{
  EMBER_TEST_ASSERT(endpoint != EM_AF_UNKNOWN_ENDPOINT);

  emberAfReadServerAttribute(endpoint,
                             ZCL_IAS_ZONE_CLUSTER_ID,
                             ZCL_IAS_CIE_ADDRESS_ATTRIBUTE_ID,
                             returnCieAddress,
                             EUI64_SIZE);

  emberAfReadServerAttribute(endpoint,
                             ZCL_IAS_ZONE_CLUSTER_ID,
                             ZCL_ZONE_STATUS_ATTRIBUTE_ID,
                             (int8u*)returnZoneStatus,
                             2);   // int16u size  

  emberAfReadServerAttribute(endpoint,
                             ZCL_IAS_ZONE_CLUSTER_ID,
                             ZCL_ZONE_TYPE_ATTRIBUTE_ID,
                             (int8u*)returnZoneType,
                             2);  // int16u size   

  emberAfReadServerAttribute(endpoint,
                             ZCL_IAS_ZONE_CLUSTER_ID,
                             ZCL_ZONE_STATE_ATTRIBUTE_ID,
                             (int8u*)returnZoneState,
                             1);  // int8u size

}

void emberAfPluginIasZoneServerInfoCommand(void)
{
  int8u cieAddress[EUI64_SIZE];
  int16u zoneStatus;
  int16u zoneType;
  int8u zoneState;
  int8u endpoint = (int8u)emberUnsignedCommandArgument(0);
  
  getAttributes(cieAddress,
                &zoneStatus,
                &zoneType,
                &zoneState,
                endpoint);
  emberAfIasZoneClusterPrint("CIE Address: ");
  emberAfPrintBigEndianEui64(cieAddress);
  emberAfIasZoneClusterPrintln("");
  emberAfIasZoneClusterPrintln("Zone Type:   0x%2X (%p)", 
                               zoneType, 
                               getZoneTypeString(zoneType));
  emberAfIasZoneClusterPrintln("Zone State:  0x%X   (%p)",
                               zoneState,
                               getZoneStateString(zoneState));
  emberAfIasZoneClusterPrintln("Zone Status: 0x%2X", 
                               zoneStatus);
  emberAfIasZoneClusterPrintln("Zone ID:     0x%2X", 
                               emberAfPluginIasZoneServerGetZoneId(endpoint));
}

void emberAfPluginIasZoneServerChangeStatusCommand(void)
{
  int16u newStatus = (int16u)emberUnsignedCommandArgument(0);
  int8u  timeSinceOccurredSeconds = (int8u)emberUnsignedCommandArgument(1);
  int8u  endpoint = (int8u)emberUnsignedCommandArgument(2);
  emberAfPluginIasZoneServerUpdateZoneStatus(endpoint,
                                             newStatus,
                                             timeSinceOccurredSeconds << 2);
}
