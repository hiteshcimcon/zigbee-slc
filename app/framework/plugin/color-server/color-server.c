// *******************************************************************
// * color-server.c
// *
// * Demonstration of a simple implementation of a color control server.  Note:
// * because this does not implement transitions, it is not HA certifiable.  
// *
// * Copyright 2015 by Silicon Laboratories. All rights reserved.           *80*
// *******************************************************************

#include "app/framework/include/af.h"
#include "app/framework/util/attribute-storage.h"
#include "app/framework/plugin/led-rgb-pwm/led-rgb-pwm.h"

void emberAfPluginColorServerInitCallback( void )
{
  // Placeholder.
}

/** @brief Move To Color
 *
 * 
 *
 * @param colorX   Ver.: always
 * @param colorY   Ver.: always
 * @param transitionTime   Ver.: always
 */
boolean emberAfColorControlClusterMoveToColorCallback(int16u colorX,
                                                      int16u colorY,
                                                      int16u transitionTime)
{
  int8u endpoint = emberAfCurrentEndpoint();
  int8u colorMode = 0x01;

  // need to write colorX and colorY here.  Note:  I am ignoring transition 
  // time for now.  
  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_CURRENT_X_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&colorX,
                        ZCL_INT16U_ATTRIBUTE_TYPE);

  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_CURRENT_Y_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&colorY,
                        ZCL_INT16U_ATTRIBUTE_TYPE);

  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_COLOR_MODE_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&colorMode,
                        ZCL_INT8U_ATTRIBUTE_TYPE);

  // now we need to update the 
  emberAfLedRgbPwmComputeRgbFromXy( endpoint );

  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);
  return TRUE;
}

/** @brief Move To Color Temperature
 *
 * 
 *
 * @param colorTemperature   Ver.: always
 * @param transitionTime   Ver.: always
 */
boolean emberAfColorControlClusterMoveToColorTemperatureCallback(int16u colorTemperature,
                                                                 int16u transitionTime)
{
  int8u endpoint = emberAfCurrentEndpoint();
  int8u colorMode = 0x02;

  // need to write colorX and colorY here.  Note:  I am ignoring transition 
  // time for now.  
  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_COLOR_TEMPERATURE_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&colorTemperature,
                        ZCL_INT16U_ATTRIBUTE_TYPE);
  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_COLOR_MODE_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&colorMode,
                        ZCL_INT8U_ATTRIBUTE_TYPE);

  emberAfLedRgbPwmComputeRgbFromColorTemp(endpoint);
 
  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);
  return TRUE;
}

/** @brief Move To Hue And Saturation
 *
 * 
 *
 * @param hue   Ver.: always
 * @param saturation   Ver.: always
 * @param transitionTime   Ver.: always
 */
boolean emberAfColorControlClusterMoveToHueAndSaturationCallback(int8u hue,
                                                                 int8u saturation,
                                                                 int16u transitionTime)
{
  int8u endpoint = emberAfCurrentEndpoint();
  int8u colorMode = 0x00;

  // limit checking:  hue and saturation are 0..254
  if(hue == 255 || saturation == 255) {
    emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_MALFORMED_COMMAND);
    return TRUE;
  }
  
  // need to write colorX and colorY here.  Note:  I am ignoring transition 
  // time for now.  
  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_CURRENT_HUE_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&hue,
                        ZCL_INT8U_ATTRIBUTE_TYPE);
  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_CURRENT_SATURATION_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&saturation,
                        ZCL_INT8U_ATTRIBUTE_TYPE);
  emberAfWriteAttribute(endpoint,
                        ZCL_COLOR_CONTROL_CLUSTER_ID,
                        ZCL_COLOR_CONTROL_COLOR_MODE_ATTRIBUTE_ID,
                        CLUSTER_MASK_SERVER,
                        (int8u *)&colorMode,
                        ZCL_INT8U_ATTRIBUTE_TYPE);

  emberAfLedRgbPwmComputeRgbFromHSV(endpoint);

  emberAfSendImmediateDefaultResponse(EMBER_ZCL_STATUS_SUCCESS);
  return TRUE;
}
