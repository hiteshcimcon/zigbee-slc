// *******************************************************************
// * led-rgb-pwm.h
// *
// *
// * Copyright 2015 Silicon Laboratories, Inc.                              *80*
// *******************************************************************
#ifndef __LED_RGB_PWM_H__
#define __LED_RGB_PWM_H__

void emberAfLedRgbPwmComputeRgbFromXy( int8u endpoint );
void emberAfLedRgbPwmComputeRgbFromColorTemp( int8u endpoint );
void emberAfLedRgbComputeRgbFromHSV( int8u endpoint );

#endif