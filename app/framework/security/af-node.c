// *******************************************************************
// * af-node.c
// *
// * Security code for a normal (non-Trust Center) node.
// *
// * Copyright 2009 by Ember Corporation. All rights reserved.              *80*
// *******************************************************************

#include "app/framework/include/af.h"
#define USE_REAL_SECURITY_PROTOTYPES
#include "app/framework/security/af-security.h"

//------------------------------------------------------------------------------
extern unsigned char Link_Key[];
EmberStatus zaNodeSecurityInit(void)
{
  EmberInitialSecurityState state;
  EmberExtendedSecurityBitmask newExtended;
  EmberExtendedSecurityBitmask oldExtended;
  EmberStatus status;
    unsigned char i=0;
  const EmberAfSecurityProfileData *data = emAfGetCurrentSecurityProfileData();

  if (data == NULL) {
    return EMBER_ERR_FATAL;
  }

  MEMSET(&state, 0, sizeof(EmberInitialSecurityState));
  for(i=0;i<16;i++)
  {
    state.preconfiguredKey.contents[i]=Link_Key[i];
  }
  //state.bitmask = data->nodeBitmask;
  state.bitmask =
(EMBER_STANDARD_SECURITY_MODE
| EMBER_HAVE_PRECONFIGURED_KEY
| EMBER_REQUIRE_ENCRYPTED_KEY);
  newExtended = data->nodeExtendedBitmask;
  MEMCOPY(emberKeyContents(&state.preconfiguredKey),
          emberKeyContents(&state.preconfiguredKey),
          EMBER_ENCRYPTION_KEY_SIZE);

#if defined ZA_CLI_FULL
  // This function will only be used if the full CLI is enabled,
  // and a value has been previously set via the "changekey" command.
  getLinkKeyFromCli(&(state.preconfiguredKey));
#endif

  emberGetExtendedSecurityBitmask(&oldExtended);
  if (oldExtended & EMBER_EXT_NO_FRAME_COUNTER_RESET){
    newExtended |= EMBER_EXT_NO_FRAME_COUNTER_RESET;
  }
  emberAfSecurityInitCallback(&state, &newExtended, FALSE); // trust center?

  emberAfSecurityPrintln("set state to: 0x%2x", state.bitmask);
  status = emberSetInitialSecurityState(&state);
  if (status != EMBER_SUCCESS) {
    emberAfSecurityPrintln("security init node: 0x%x", status);
    return status;
  }

  // Don't need to check on the status here, emberSetExtendedSecurityBitmask
  // always returns EMBER_SUCCESS.
  emberAfSecurityPrintln("set extended security to: 0x%2x", newExtended);
  emberSetExtendedSecurityBitmask(newExtended);

  emAfClearLinkKeyTable();

  return EMBER_SUCCESS;
}
