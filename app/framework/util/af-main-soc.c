// *******************************************************************
// * af-main-soc.c
// *
// *
// * Copyright 2007 by Ember Corporation. All rights reserved.              *80*
// *******************************************************************

#include "app/framework/include/af.h"
#include "app/framework/util/attribute-storage.h"
#include "app/util/serial/command-interpreter2.h"

// ZDO
#include "app/util/zigbee-framework/zigbee-device-common.h"
#include "app/util/zigbee-framework/zigbee-device-library.h"

#include "app/util/counters/counters.h"
#include "app/util/security/security.h"
#include "app/util/common/form-and-join.h"

#include "app/framework/util/service-discovery.h"
#include "app/framework/util/af-main.h"
#include "app/framework/util/util.h"

#include "app/framework/security/af-security.h"

#include "app/framework/plugin/partner-link-key-exchange/partner-link-key-exchange.h"
#include "app/framework/plugin/fragmentation/fragmentation.h"
#include "SLC/Gps.h"
#if defined(__ICCARM__)
  #define EM35X_SERIES
#endif

#if defined(EM35X_SERIES)
#include "hal/micro/cortexm3/diagnostic.h"
#endif

// *****************************************************************************
// Globals

// APP_SERIAL is set in the project files
int8u serialPort = APP_SERIAL;

#if (EMBER_AF_BAUD_RATE == 300)
  #define BAUD_RATE BAUD_300
#elif (EMBER_AF_BAUD_RATE == 600)
  #define BAUD_RATE BAUD_600
#elif (EMBER_AF_BAUD_RATE == 900)
  #define BAUD_RATE BAUD_900
#elif (EMBER_AF_BAUD_RATE == 1200)
  #define BAUD_RATE BAUD_1200
#elif (EMBER_AF_BAUD_RATE == 2400)
  #define BAUD_RATE BAUD_2400
#elif (EMBER_AF_BAUD_RATE == 4800)
  #define BAUD_RATE BAUD_4800
#elif (EMBER_AF_BAUD_RATE == 9600)
  #define BAUD_RATE BAUD_9600
#elif (EMBER_AF_BAUD_RATE == 14400)
  #define BAUD_RATE BAUD_14400
#elif (EMBER_AF_BAUD_RATE == 19200)
  #define BAUD_RATE BAUD_19200
#elif (EMBER_AF_BAUD_RATE == 28800)
  #define BAUD_RATE BAUD_28800
#elif (EMBER_AF_BAUD_RATE == 38400)
  #define BAUD_RATE BAUD_38400
#elif (EMBER_AF_BAUD_RATE == 50000)
  #define BAUD_RATE BAUD_50000
#elif (EMBER_AF_BAUD_RATE == 57600)
  #define BAUD_RATE BAUD_57600
#elif (EMBER_AF_BAUD_RATE == 76800)
  #define BAUD_RATE BAUD_76800
#elif (EMBER_AF_BAUD_RATE == 100000)
  #define BAUD_RATE BAUD_100000
#elif (EMBER_AF_BAUD_RATE == 115200)
  #define BAUD_RATE BAUD_115200
#elif (EMBER_AF_BAUD_RATE == 230400)
  #define BAUD_RATE BAUD_230400
#elif (EMBER_AF_BAUD_RATE == 460800)
  #define BAUD_RATE BAUD_460800
#else
  #error EMBER_AF_BAUD_RATE set to an invalid baud rate
#endif

#if defined(MAIN_FUNCTION_HAS_STANDARD_ARGUMENTS)
  #define APP_FRAMEWORK_MAIN_ARGUMENTS argc, argv
#else
  #define APP_FRAMEWORK_MAIN_ARGUMENTS 0, NULL
#endif

// *****************************************************************************
// Forward declarations.

#if defined(EMBER_TEST) && defined(EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM)
  void emAfSetupFakeEepromForSimulation(void);
  #define SETUP_FAKE_EEPROM_FOR_SIMULATION() emAfSetupFakeEepromForSimulation()
#else
  #define SETUP_FAKE_EEPROM_FOR_SIMULATION()
#endif

#if defined(ZA_CLI_MINIMAL) || defined(ZA_CLI_FULL)
  #define COMMAND_READER_INIT() emberCommandReaderInit()
#else
  #define COMMAND_READER_INIT()
#endif

// *****************************************************************************
// Functions
////////////////////SLC///////////////////////
#include "SLC/DS1339.h"
#include "SLC/AMR_CS5463.h"
#include "SLC/AMR_RTC.h"
#include "SLC/MSPI.h"
#include "SLC/protocol.h"
#include "SLC/Application_logic.h"


/***************** Global variable decleration *************/
unsigned char Cali_Cnt =0;
unsigned char Calibration_timer =0;
unsigned char mfg_lib_flag =0;
unsigned char search_ntw=1,Ember_network_up_send_frame=0;
EmberEUI64 SLC_eui;
unsigned char SLC_reset_info,SLC_extreset_info;
unsigned char app_beacon_timer = 0;
unsigned short int SLC_Start_netwrok_Search_Timer = 0;
unsigned char SLC_Start_netwrok_Search = 1;
unsigned char SLC_Start_netwrok_Search_Timeout = 1;
unsigned char SLC_intermetent_netwrok_Search_Timeout = 0;
unsigned char SLC_Start_netwrok_Search_counter;
unsigned char SETMAC[27],mac=0,EM_TEST[10],mfgtest =0,chan=0;;
unsigned char convert_1=0,convert_2=0,mfg_test_variable =0;
unsigned char MFG_CUSTOM_EUI_64[8],Serial_command_access =0;
unsigned int  External_PA =0xFFFF;
//unsigned char GPS_buff[30]; //100
unsigned char Enable_Security =0;
/***************** Global variable decleration *************/

/***************** Extern variable decleration *************/
extern unsigned char Ember_Test_Response;
extern int8s Slc_db;
extern unsigned char boot_flag,one_sec,one_sec_Ntw_Search;
extern unsigned char receive_mac[],receive_flag,write_attr,Single_time_run_cmd;
extern unsigned int PanId,scann_chan;
extern unsigned long int channel_set;
extern unsigned char ExtPanId_set[],Valid_DCU_Flag;
extern unsigned char Link_Key[];
extern unsigned char Read_buff_success;
extern unsigned int Read_buff_Loc;
extern unsigned char Serial_Read_buff_sam[];
unsigned char call_old_function = 0;
/***************** Extern variable decleration *************/

unsigned char otaClientStart=0;

int emberAfMain(MAIN_FUNCTION_PARAMETERS)
{
  EmberStatus status;
  
  EmberEUI64 SLC_myEui64;
  unsigned char Main_Display[35];
  EmberNetworkParameters networkParams1;
  EmberNodeType nodeTypeResult1 = 0xFF;
  unsigned int i = 0;

#if (defined (ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
  emberAfPrintAllOff();
#endif
  SLC_reset_info = halGetResetInfo();
  SLC_extreset_info =halGetExtendedResetInfo();

  SETUP_FAKE_EEPROM_FOR_SIMULATION();

  INTERRUPTS_ON();  // Safe to enable interrupts at this point
  {
    int returnCode;
    if (emberAfMainStartCallback(&returnCode, APP_FRAMEWORK_MAIN_ARGUMENTS)) {
      return returnCode;
    }
  }


  // Initialize the Ember Stack.
  status = emberInit();

  //emberSerialInit(RS485_SERIAL, BAUD_RATE, PARITY_NONE, 1);
  emberSerialInit(APP_SERIAL, GPS_BAUD_RATE2, PARITY_NONE, 1);
 // send_data_on_uart((char *)"HITESHPPPPPP",11);
  #if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
  emberAfCorePrintln("Reset info: 0x%x %x",SLC_reset_info,SLC_extreset_info);    // Print Last reset causes.
#endif
//  emberAfCorePrintln("Reset info: 0x%x (%p)", 
//                     halGetResetInfo(),
//                     halGetResetString());

#if defined(EM35X_SERIES)
  emberAfCorePrintln("Extended Reset info: 0x%2X (%p)",
                     halGetExtendedResetInfo(),
                     halGetExtendedResetString());

  if (halResetWasCrash()) {
    halPrintCrashSummary(serialPort);
    halPrintCrashDetails(serialPort);
    halPrintCrashData(serialPort);
  }

#endif


  if (status != EMBER_SUCCESS) {
    emberAfCorePrintln("%pemberInit 0x%x", "ERROR: ", status);

    // The app can choose what to do here.  If the app is running
    // another device then it could stay running and report the
    // error visually for example. This app asserts.
    assert(FALSE);
  } else {
    emberAfDebugPrintln("init pass");
  }

  // This will initialize the stack of networks maintained by the framework,
  // including setting the default network.
  emAfInitializeNetworkIndexStack();

  // Initialize messageSentCallbacks table
  emAfInitializeMessageSentCallbackArray();

  emberAfEndpointConfigure();
  emberAfMainInitCallback();

  emberAfInit();

  // The address cache needs to be initialized and used with the source routing
  // code for the trust center to operate properly.
  securityAddressCacheInit(EMBER_AF_PLUGIN_ADDRESS_TABLE_SIZE,                     // offset
                           EMBER_AF_PLUGIN_ADDRESS_TABLE_TRUST_CENTER_CACHE_SIZE); // size

  EM_AF_NETWORK_INIT();

  COMMAND_READER_INIT();
  halCommonGetToken(&Enable_Security,TOKEN_Enable_Security);
  if(Enable_Security > 1)
  {
    Enable_Security = 0;
  }

  if(Enable_Security == 1)
  {
    emDefaultSecurityLevel=5;
  }
  else
  {
    emDefaultSecurityLevel=0;
  }

  // Set the manufacturing code. This is defined by ZigBee document 053874r10
  // Ember's ID is 0x1002 and is the default, but this can be overridden in App Builder.
  emberSetManufacturerCode(EMBER_AF_MANUFACTURER_CODE);

  emberSetMaximumIncomingTransferSize(EMBER_AF_INCOMING_BUFFER_LENGTH);
  emberSetMaximumOutgoingTransferSize(EMBER_AF_MAXIMUM_SEND_PAYLOAD_LENGTH);
  emberSetTxPowerMode(EMBER_AF_TX_POWER_MODE);
///////////////////////
  halStackIndicatePresence();
  //\********* reade network related tokens ************/
  halCommonGetToken(&Single_time_run_cmd,TOKEN_Single_time_run_cmd);
  halCommonGetToken(&scann_chan,TOKEN_scann_chan);
  halCommonGetToken(&PanId,TOKEN_PanId);
  halCommonGetToken(&ExtPanId_set,TOKEN_ExtPanId_set);
  halCommonGetToken(&Valid_DCU_Flag,TOKEN_Valid_DCU_Flag);

//  Valid_DCU_Flag = 0;
//  halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
/********* reade network related tokens ************/

  if(scann_chan == 0)
  {
//    scann_chan  =0x7fff;
        scann_chan  =0x7ffe; //changed from version 2.0.31
  }
  channel_set=scann_chan<<11;

  emRadioSetEdCcaThreshold(-63);      // set CCA Threshold to -63dbm because we use LNA with 12dbm gain

  emberAfGetEui64(SLC_myEui64);       // Read MAC address of chip


#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
  sprintf(Main_Display,"MAC Add = %02x%02x%02x%02x%02x%02x%02x%02x",SLC_myEui64[7],SLC_myEui64[6],SLC_myEui64[5],SLC_myEui64[4],SLC_myEui64[3],SLC_myEui64[2],SLC_myEui64[1],SLC_myEui64[0]);		
  emberAfGuaranteedPrint("\r\n%p", Main_Display);
#endif

  app_beacon_timer = SLC_myEui64[0]%20;     // make (beacon timer) based on Last Byte of MAC address it provide random delay on all SLCs in network for network join attampts.
Time_Slice =SLC_myEui64[0];
  Temp_Time_Slice = Time_Slice;

#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
  sprintf(Main_Display,"app_beacon_timer = %u",app_beacon_timer);
  emberAfGuaranteedPrint("\r\n%p", Main_Display);
#endif

//#if defined(DALI_SUPPORT)
  if(dali_support == 1)
  {
  halSetLed(EN_DALI);
  halSetLed(TX_DALI);
  }
//#endif

  if(!Single_time_run_cmd)        // if SLC start first time then up network with out any delay with all channel scan and extended PAN ID = 0
  {
//      zaNodeSecurityInit();       // Init network security with defined link key.
      emberAfStartSearchForJoinableNetwork(); // search for joinable network.
      emberPermitJoining(0xFF);         // Set Permit joining value to 0xFF for always joining enable
      //emberAfGuaranteedPrint("\r\n%p>", "Single_time_run_cmd=0");
  }
  else
  {
      //emberAfGuaranteedPrint("\r\n%p>", "Single_time_run_cmd=1");
  }
  //////////////////////
  
  while(TRUE) {
    
    /////////////////
    
    if((Id_frame_received)&&(otaClientStart == 1))
    {
      otaClientStart =0;
      emberAfPluginOtaClientStackStatusCallback();}

   SLC_main();              // call for all SLC related functions from here.
    if(((search_ntw)&&(one_sec_Ntw_Search >=
      (9 + app_beacon_timer))&&(Single_time_run_cmd))||
      ((!Single_time_run_cmd)&&(one_sec_Ntw_Search >=
      (9 + app_beacon_timer))))        // if SLC not in network then search netwrok from this routin.  //V1.0.18.2
    {
      one_sec_Ntw_Search =0;

      if(SLC_Start_netwrok_Search == 1)
      {
              halResetWatchdog();   // Periodically reset the watchdog.
              if(Enable_Security == 1)
              {
                //emberAfGuaranteedPrint("\r\n%p>","Enable_seccccccccccc");
                emDefaultSecurityLevel=5;
                zaNodeSecurityInit();

              }
              else
              {
                //  emberAfGuaranteedPrint("\r\n%p>","DisaBLE_seccccccccccc");
                   emDefaultSecurityLevel=0;
              }
              emberAfStartSearchForJoinableNetwork();      // start search with joinable network
             //emberPermitJoining(0xFF);// emAfPermitJoin(0xFF);    // Set Permit joining value to 0xFF for always joining enable
      }
    }
    /////////////////
    halResetWatchdog();   // Periodically reset the watchdog.
    emberTick();          // Allow the stack to run.
   emberPermitJoining(0xFF);// emAfPermitJoin(0xFF);  //add logic to make mnetwork in multihope with trust center _5sept2012
    // Allow the ZCL clusters to run. This should go immediately after emberTick
    emberAfTick();

   // emberSerialBufferTick();

    emberAfRunEvents();

//#if (defined(ISLC_3100_V7_7) || defined(ISLC_3100_V7_9))    
    
    if(emberProcessCommandString_RS485(NULL,RS485_SERIAL))
    {
      if(RS485_Read_buff_success==2)
      {
/////////////////Calibration////////////////////////
        if((RS485_Serial_Read_buff[0] == 0x8E) && (RS485_Serial_Read_buff[1] == 0x4C) && (RS485_Serial_Read_buff[4] == 0x43))
        {

            if(RS485_Serial_Read_buff[5] == 0x92)
            {
                GPS_Read_Enable =0;
                Voltage_calibration = 1;
                calibmode = 1;
                pulses_r = 0;
                Cali_Cnt =0;
            }
            else if(RS485_Serial_Read_buff[5] == 0x93)
            {
                Current_calibration = 1;
                GPS_Read_Enable =0;
            }
            else if(RS485_Serial_Read_buff[5] == 0x94)
            {
                KW_Calibration = 1;
                GPS_Read_Enable =0;
            }
            else if(RS485_Serial_Read_buff[5] == 0x95)
            {
                Read_Em_Calibration = 1;
                GPS_Read_Enable =0;
            }
            else if(RS485_Serial_Read_buff[5] == 0x96)
            {
                kwh =0;
                GPS_Read_Enable =0;
              // pulseCounter.long_data = kwh;
                Read_KWH_Value =1;
                pulses_r =0;
                halCommonSetToken(TOKEN_pulses_r,&pulses_r);
            }
            /////////////////
            else if(RS485_Serial_Read_buff[5] == 0x97)
            {

                GPS_Read_Enable =0;
                KW_Calibration_120=1;
               // pulses_r =0;
                calibmode =1;

            }
            else if(RS485_Serial_Read_buff[5] == 0x98)
            {
                scann_chan =0x7FFF;
                
            //                        channel_set=scann_chan<<11;
                halCommonSetToken(TOKEN_scann_chan,&scann_chan);    // Set channel to NVM.
                
                Single_time_run_cmd =1; //after calibration and power cycle slc not move on channel 11 because this flag is 0 and it scann without channel 11  
                halCommonSetToken(TOKEN_Single_time_run_cmd,&Single_time_run_cmd);
                
                Cali_buff[0]=0x8E;
                Cali_buff[1]=0x4C;
                Cali_buff[2]=0x00;
                Cali_buff[3]=0x00;
                Cali_buff[4]=0x00;
                Cali_buff[5]=0x00;
                Cali_buff[6]=0x98;
                delay(1000);
        #if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))         
                send_data_on_RS485((char *)Cali_buff,7);
        #else
                send_data_on_uart((char *)Cali_buff,7);
        #endif                     
            }
  //////////////////////////////////////////////////////////////////////////          
       }
        RS485_Read_buff_success=0;
        RS485_Read_buff_Loc = 0;
        for(i=0;i<B_SIZE_SERIAL_RF;i++)
        {
           RS485_Serial_Read_buff[i]=0;
        }
        //send_data_on_RS485("RX485 Received\r\n",strlen("RX485 Received\r\n"));

      } 
    }
//#endif    
    
//#if defined(ZA_CLI_MINIMAL) || defined(ZA_CLI_FULL)
     if(call_old_function == 0)
    {
        if(emberProcessCommandString_new(NULL,APP_SERIAL))
        {
            if(Read_buff_success == 2)
            {

        #if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)|| defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
               if(GPS_Read_Enable == 1)
               {
                    Parse_GPS_Query(Serial_Read_buff_sam,Read_buff_Loc);
                    check_Gps_Statistics();
               } 
//        /////////////////Calibration////////////////////////
//                if((Serial_Read_buff_sam[0] == 0x8E) && (Serial_Read_buff_sam[1] == 0x4C) && (Serial_Read_buff_sam[4] == 0x43))
//                {
//
//                    if(Serial_Read_buff_sam[5] == 0x92)
//                    {
//                        GPS_Read_Enable =0;
//                        Voltage_calibration = 1;
//                        calibmode = 1;
//                        pulses_r = 0;
//                        Cali_Cnt =0;
//                    }
//                    else if(Serial_Read_buff_sam[5] == 0x93)
//                    {
//                        Current_calibration = 1;
//                        GPS_Read_Enable =0;
//                    }
//                    else if(Serial_Read_buff_sam[5] == 0x94)
//                    {
//                        KW_Calibration = 1;
//                        GPS_Read_Enable =0;
//                    }
//                    else if(Serial_Read_buff_sam[5] == 0x95)
//                    {
//                        Read_Em_Calibration = 1;
//                        GPS_Read_Enable =0;
//                    }
//                    else if(Serial_Read_buff_sam[5] == 0x96)
//                    {
//                        kwh =0;
//                        GPS_Read_Enable =0;
//                      // pulseCounter.long_data = kwh;
//                        Read_KWH_Value =1;
//                        pulses_r =0;
//                        halCommonSetToken(TOKEN_pulses_r,&pulses_r);
//                    }
//                    /////////////////
//                    else if(Serial_Read_buff_sam[5] == 0x97)
//                    {
//
//                        GPS_Read_Enable =0;
//                        KW_Calibration_120=1;
//                       // pulses_r =0;
//                        calibmode =1;
//
//                    }
//                    else if(Serial_Read_buff_sam[5] == 0x98)
//                    {
//                        scann_chan =0x7FFF;
//                        
//                    //                        channel_set=scann_chan<<11;
//                        halCommonSetToken(TOKEN_scann_chan,&scann_chan);    // Set channel to NVM.
//                        
//                        Single_time_run_cmd =1; //after calibration and power cycle slc not move on channel 11 because this flag is 0 and it scann without channel 11  
//                        halCommonSetToken(TOKEN_Single_time_run_cmd,&Single_time_run_cmd);
//                        
//                        Cali_buff[0]=0x8E;
//                        Cali_buff[1]=0x4C;
//                        Cali_buff[2]=0x00;
//                        Cali_buff[3]=0x00;
//                        Cali_buff[4]=0x00;
//                        Cali_buff[5]=0x00;
//                        Cali_buff[6]=0x98;
//                        delay(1000);
//                        send_data_on_uart((char *)Cali_buff,7);
//                    }

                    /////////////////

                 }
        /////////////////////////////////////////////////////
        #endif
                if((Serial_Read_buff_sam[0] == '*') && (Serial_Read_buff_sam[1] == 'b') && (Serial_Read_buff_sam[2] == 'o') && (Serial_Read_buff_sam[3] == 'o') && (Serial_Read_buff_sam[4] == 't'))
                {
                    boot_flag = 2;
                }
//                else if((Serial_Read_buff_sam[0] == 'i') && (Serial_Read_buff_sam[1] == 'n') && (Serial_Read_buff_sam[2] == 'f') && (Serial_Read_buff_sam[3] == 'o'))
//                {
//                    emberAfPrintAllOn();
//                    emAfCliInfoCommand();
//                    call_old_function =1;
//                    mac = 0;
////                    printKeyInfo();
////                    emberAfGetNetworkParameters(&nodeTypeResult1, &networkParams1);
////                    emberAfGuaranteedPrint("\r\n Channel = %u",networkParams1.radioChannel);
////                    emberAfGuaranteedPrint("\r\n Extended PANid = 0x%x%x%x%x%x%x%x%x",networkParams1.extendedPanId[0],networkParams1.extendedPanId[1],networkParams1.extendedPanId[2],networkParams1.extendedPanId[3],networkParams1.extendedPanId[4],networkParams1.extendedPanId[5],networkParams1.extendedPanId[6],networkParams1.extendedPanId[7]);
////                    emberAfGuaranteedPrint("\r\n Short PANid = 0x%2x",networkParams1.panId);
////                    emberAfGuaranteedPrint("\r\n MAC Add = 0x%x%x%x%x%x%x%x%x",SLC_myEui64[7],SLC_myEui64[6],SLC_myEui64[5],SLC_myEui64[4],SLC_myEui64[3],SLC_myEui64[2],SLC_myEui64[1],SLC_myEui64[0]);
////                    emberAfGuaranteedPrint("\r\n Valid DCU = %u",Valid_DCU_Flag);
////                    emberAfGuaranteedPrint("\r\n Enable_Security = %u",Enable_Security);
//                }
                else if((Serial_Read_buff_sam[0] == 'n') && (Serial_Read_buff_sam[1] == 'l') && (Serial_Read_buff_sam[2] == 'c'))
                {
                    scann_chan =0x7FFF;
                    channel_set=scann_chan<<11;
                    halCommonSetToken(TOKEN_scann_chan,&scann_chan);    // Set channel to NVM.

                    Id_frame_received = 0;                // clear ID frame received flag to search new DCU in to new channel.
                    RF_communication_check_Counter = 0;   // reset Id frame timer.
                    Valid_DCU_Flag = 0;                    // clear valid DCU flage to search DCU.
                    halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);

                    networkLeaveCommand();
                }

                for(i=0;i<B_SIZE_SERIAL_RF;i++)
                {
                    Serial_Read_buff_sam[i]=0;
                }
                Read_buff_success = 0;
                Read_buff_Loc = 0;
            }
//            if(boot_flag == 2) //Enter in bootmode
//            {
//                boot_flag=0;
//                //emberSerialPrintf(APP_SERIAL, "starting bootloader...\r\n");
//                sprintf(Main_Display,"starting bootloader...\r\n");
//                send_data_on_uart(Main_Display,strlen(Main_Display));
//                emberSerialWaitSend(APP_SERIAL);
//                //halLaunchStandaloneBootloader(STANDALONE_BOOTLOADER_NORMAL_MODE);
//            }
//            else{
//            //emberAfGuaranteedPrint("%p>", ZA_PROMPT);
//            }

        }
    
//    else
//    {
//      if(emberProcessCommandInput(APP_SERIAL))
//      {
//         if(boot_flag == 2) //Enter in bootmode
//          {
//              boot_flag=0;
//              emberSerialPrintf(APP_SERIAL, "starting bootloader...\r\n");
//              emberSerialWaitSend(APP_SERIAL);
//              //halLaunchStandaloneBootloader(STANDALONE_BOOTLOADER_NORMAL_MODE);
//          }
//          else
//          {
//            emberAfGuaranteedPrint("%p>", ZA_PROMPT);
//          }
//      }
//
//    }
//    if (emberProcessCommandInput(APP_SERIAL)) {
//      emberAfGuaranteedPrint("%p>", ZA_PROMPT);
//    }
//#endif

#if defined(EMBER_TEST)
    if (1) {
      // Simulation only
      int32u timeToNextEventMax = emberMsToNextStackEvent();
      timeToNextEventMax = emberAfMsToNextEvent(timeToNextEventMax);
      simulatedTimePassesMs(timeToNextEventMax);
    }
#endif


    // After each interation through the main loop, our network index stack
    // should be empty and we should be on the default network index again.
    emAfAssertNetworkIndexStackIsEmpty();
  }
  return 0;
}

void emberAfGetMfgString(int8u* returnData)
{
  halCommonGetMfgToken(returnData, TOKEN_MFG_STRING);
}

EmberNodeId emberAfGetNodeId(void)
{
  return emberGetNodeId();
}

EmberPanId emberAfGetPanId(void)
{
  return emberGetPanId();
}

EmberNetworkStatus emberAfNetworkState(void)
{
  return emberNetworkState();
}

int8u emberAfGetBindingIndex(void)
{
  return emberGetBindingIndex();
}

int8u emberAfGetStackProfile(void)
{
  return EMBER_STACK_PROFILE;
}

int8u emberAfGetAddressIndex(void)
{
  EmberNodeId nodeId = emberGetSender();
  int8u i;
  for (i = 0; i < EMBER_AF_PLUGIN_ADDRESS_TABLE_SIZE; i++) {
    if (emberGetAddressTableRemoteNodeId(i) == nodeId) {
      return i;
    }
  }
  return EMBER_NULL_ADDRESS_TABLE_INDEX;
}

// ******************************************************************
// binding
// ******************************************************************
EmberStatus emberAfSendEndDeviceBind(int8u endpoint)
{
  EmberStatus status;
  EmberApsOption options = ((EMBER_AF_DEFAULT_APS_OPTIONS
                             | EMBER_APS_OPTION_SOURCE_EUI64)
                            & ~EMBER_APS_OPTION_RETRY);

  status = emberAfPushEndpointNetworkIndex(endpoint);
  if (status != EMBER_SUCCESS) {
    return status;
  }

  emberAfZdoPrintln("send %x %2x", endpoint, options);
  status = emberEndDeviceBindRequest(endpoint, options);
  emberAfZdoPrintln("done: %x.", status);
  emberAfZdoFlush();

  emberAfPopNetworkIndex();
  return status;
}

EmberStatus emberRemoteSetBindingHandler(EmberBindingTableEntry *entry)
{
  EmberStatus status = EMBER_TABLE_FULL;
  EmberBindingTableEntry candidate;
  int8u i;

  emberAfPushCallbackNetworkIndex();

  // If we receive a bind request for the Key Establishment cluster and we are
  // not the trust center, then we are doing partner link key exchange.  We
  // don't actually have to create a binding.
  if (emberAfGetNodeId() != EMBER_TRUST_CENTER_NODE_ID
      && entry->clusterId == ZCL_KEY_ESTABLISHMENT_CLUSTER_ID) {
    status = emberAfPartnerLinkKeyExchangeRequestCallback(entry->identifier);
    goto kickout;
  }


  // ask the application if current binding request is allowed or not
  status = emberAfRemoteSetBindingPermissionCallback(entry);
  if (status == EMBER_SUCCESS){
    // For all other requests, we search the binding table for an unused entry
    // and store the new entry there if we find one.
    for (i = 0; i < EMBER_BINDING_TABLE_SIZE; i++) {
      if (emberGetBinding(i, &candidate) == EMBER_SUCCESS
          && candidate.type == EMBER_UNUSED_BINDING) {
        status = emberSetBinding(i, entry);
        goto kickout;
      }
    }
    // If we get here, we didn't find an empty table slot, so table is full.
    status = EMBER_TABLE_FULL;
  }


kickout:
  emberAfPopNetworkIndex();
  return status;
}

EmberStatus emberRemoteDeleteBindingHandler(int8u index)
{
  EmberStatus status;
  emberAfPushCallbackNetworkIndex();

  // ask the application if current binding request is allowed or not
  status = emberAfRemoteDeleteBindingPermissionCallback(index);
  if (status == EMBER_SUCCESS){
    status = emberDeleteBinding(index);
    emberAfZdoPrintln("delete binding: %x %x", index, status);
  }

  emberAfPopNetworkIndex();
  return status;
}

// ******************************************************************
// setup endpoints and clusters for responding to ZDO requests
// ******************************************************************
int8u emberGetEndpoint(int8u index)
{
  int8u endpoint = EMBER_AF_INVALID_ENDPOINT;
  if (emberAfGetEndpointByIndexCallback(index, &endpoint)) {
    return endpoint;
  }
  return (((emberAfNetworkIndexFromEndpointIndex(index)
            == emberGetCallbackNetwork())
           && emberAfEndpointIndexIsEnabled(index))
          ? emberAfEndpointFromIndex(index)
          : 0xFF);
}

// must return the endpoint desc of the endpoint specified
boolean emberGetEndpointDescription(int8u endpoint,
                                    EmberEndpointDescription *result)
{
  if (emberAfGetEndpointDescriptionCallback(endpoint, result)) {
    return TRUE;
  }
  int8u endpointIndex = emberAfIndexFromEndpoint(endpoint);
  if (endpointIndex == 0xFF
      || (emberAfNetworkIndexFromEndpointIndex(endpointIndex)
          != emberGetCallbackNetwork())) {
    return FALSE;
  }
  result->profileId          = emberAfProfileIdFromIndex(endpointIndex);
  result->deviceId           = emberAfDeviceIdFromIndex(endpointIndex);
  result->deviceVersion      = emberAfDeviceVersionFromIndex(endpointIndex);
  result->inputClusterCount  = emberAfClusterCount(endpoint, TRUE);
  result->outputClusterCount = emberAfClusterCount(endpoint, FALSE);
  return TRUE;
}

// must return the clusterId at listIndex in the list specified for the
// endpoint specified
int16u emberGetEndpointCluster(int8u endpoint,
                               EmberClusterListId listId,
                               int8u listIndex)
{
  EmberAfCluster *cluster = NULL;
  int8u endpointIndex = emberAfIndexFromEndpoint(endpoint);
  if (endpointIndex == 0xFF
      || (emberAfNetworkIndexFromEndpointIndex(endpointIndex)
          != emberGetCallbackNetwork())) {
    return 0xFFFF;
  } else if (listId == EMBER_INPUT_CLUSTER_LIST) {
    cluster = emberAfGetNthCluster(endpoint, listIndex, TRUE);
  } else if (listId == EMBER_OUTPUT_CLUSTER_LIST) {
    cluster = emberAfGetNthCluster(endpoint, listIndex, FALSE);
  }
  return (cluster == NULL ? 0xFFFF : cluster->clusterId);
}


// *******************************************************************
// Handlers required to use the Ember Stack.

// Called when the stack status changes, usually as a result of an
// attempt to form, join, or leave a network.
void emberStackStatusHandler(EmberStatus status)
{
  emberAfPushCallbackNetworkIndex();
  emAfStackStatusHandler(status);
  emberAfPopNetworkIndex();
}

// Copy the message buffer into a RAM buffer.
//   If message is too large, 0 is returned and no copying is done.
//   Otherwise data is copied, and length of copied data is returned.
int8u emAfCopyMessageIntoRamBuffer(EmberMessageBuffer message,
                                   int8u *buffer,
                                   int16u bufLen)
{
  int8u length = emberMessageBufferLength(message);
  if (bufLen < length) {
    emberAfAppPrintln("%pmsg too big (%d > %d)", 
                      "ERROR: ", 
                      length, 
                      bufLen);
    return 0;
  }
  emberCopyFromLinkedBuffers(message, 0, buffer, length); // no offset
  return length;
}

void emberIncomingMessageHandler(EmberIncomingMessageType type,
                                 EmberApsFrame *apsFrame,
                                 EmberMessageBuffer message)
{
  int8u lastHopLqi;
  int8s lastHopRssi;
  int16u messageLength;
  int8u messageContents[EMBER_AF_MAXIMUM_APS_PAYLOAD_LENGTH];

  emberAfPushCallbackNetworkIndex();

  messageLength = emAfCopyMessageIntoRamBuffer(message,
                                               messageContents,
                                               EMBER_AF_MAXIMUM_APS_PAYLOAD_LENGTH);
  if (messageLength == 0) {
    goto kickout;
  }

  emberGetLastHopLqi(&lastHopLqi);
  emberGetLastHopRssi(&lastHopRssi);

  emAfIncomingMessageHandler(type,
                             apsFrame,
                             lastHopLqi,
                             lastHopRssi,
                             messageLength,
                             messageContents);

kickout:
  emberAfPopNetworkIndex();
}


// Called when a message we sent is acked by the destination or when an
// ack fails to arrive after several retransmissions.
void emberMessageSentHandler(EmberOutgoingMessageType type,
                             int16u indexOrDestination,
                             EmberApsFrame *apsFrame,
                             EmberMessageBuffer message,
                             EmberStatus status)
{
  int8u messageContents[EMBER_AF_MAXIMUM_APS_PAYLOAD_LENGTH];
  int8u messageLength;
  emberAfPushCallbackNetworkIndex();


#ifdef EMBER_AF_PLUGIN_FRAGMENTATION
  if (emAfFragmentationMessageSent(apsFrame, status)) {
    goto kickout;
  }
#endif //EMBER_AF_PLUGIN_FRAGMENTATION
  
  messageLength = emAfCopyMessageIntoRamBuffer(message,
                                               messageContents,
                                               EMBER_AF_MAXIMUM_APS_PAYLOAD_LENGTH);
  if (messageLength == 0) {
    // Message too long.  Error printed by above function.
    goto kickout;
  }

  emAfMessageSentHandler(type,
                         indexOrDestination,
                         apsFrame,
                         status,
                         messageLength,
                         messageContents,
                         message);

kickout:
  emberAfPopNetworkIndex();
}

EmberStatus emAfSend(EmberOutgoingMessageType type,
                     int16u indexOrDestination,
                     EmberApsFrame *apsFrame,
                     int8u messageLength,
                     int8u *message,
                     int8u *messageTag)
{
  EmberMessageBuffer payload = emberFillLinkedBuffers(message, messageLength);
  if (payload == EMBER_NULL_MESSAGE_BUFFER) {
    return EMBER_NO_BUFFERS;
  } else {
    EmberStatus status;

    *messageTag = payload;

    switch (type) {
    case EMBER_OUTGOING_DIRECT:
    case EMBER_OUTGOING_VIA_ADDRESS_TABLE:
    case EMBER_OUTGOING_VIA_BINDING:
      status = emberSendUnicast(type, indexOrDestination, apsFrame, payload);
      break;
    case EMBER_OUTGOING_MULTICAST:
      status = emberSendMulticast(apsFrame,
                                  ZA_MAX_HOPS, // radius
                                  ZA_MAX_HOPS, // nonmember radius
                                  payload);
      break;
    case EMBER_OUTGOING_BROADCAST:
      status = emberSendBroadcast(indexOrDestination,
                                  apsFrame,
                                  ZA_MAX_HOPS, // radius
                                  payload);
      break;
    default:
      status = EMBER_BAD_ARGUMENT;
      break;
    }

    emberReleaseMessageBuffer(payload);

    return status;
  }
}

void emberAfGetEui64(EmberEUI64 returnEui64)
{
  MEMCOPY(returnEui64, emberGetEui64(), EUI64_SIZE);
}

EmberStatus emberAfGetNetworkParameters(EmberNodeType* nodeType, 
                                        EmberNetworkParameters* parameters)
{
  emberGetNetworkParameters(parameters);
  return emberGetNodeType(nodeType);
}

EmberStatus emberAfGetNodeType(EmberNodeType *nodeType)
{
  return emberGetNodeType(nodeType);
}

int8u emberAfGetSecurityLevel(void)
{
  return EMBER_SECURITY_LEVEL;
}

int8u emberAfGetKeyTableSize(void)
{
  return EMBER_KEY_TABLE_SIZE;
}

int8u emberAfGetBindingTableSize(void)
{
  return EMBER_BINDING_TABLE_SIZE;
}

int8u emberAfGetAddressTableSize(void)
{
  return EMBER_ADDRESS_TABLE_SIZE;
}

int8u emberAfGetChildTableSize(void)
{
  return EMBER_CHILD_TABLE_SIZE;
}

int8u emberAfGetNeighborTableSize(void)
{
  return EMBER_NEIGHBOR_TABLE_SIZE;
}

int8u emberAfGetRouteTableSize(void)
{
  return EMBER_ROUTE_TABLE_SIZE;
}

int8u emberAfGetSleepyMulticastConfig(void)
{
  return EMBER_SEND_MULTICASTS_TO_SLEEPY_ADDRESS;
}

EmberStatus emberAfGetChildData(int8u index,
                                EmberNodeId *childId,
                                EmberEUI64 childEui64,
                                EmberNodeType *childType)
{
  *childId = emberChildId(index);
  return emberGetChildData(index,
                           childEui64,
                           childType);
}

int8u emAfGetPacketBufferFreeCount(void)
{
  return emberPacketBufferFreeCount();
}

int8u emAfGetPacketBufferTotalCount(void)
{
  return EMBER_PACKET_BUFFER_COUNT;
}

void emAfCliVersionCommand(void)
{
  emAfParseAndPrintVersion(emberVersion);
}

void emberNetworkFoundHandler(EmberZigbeeNetwork *networkFound)
{
  int8u lqi;
  int8s rssi;
  emberAfPushCallbackNetworkIndex();
  emberGetLastHopLqi(&lqi);
  emberGetLastHopRssi(&rssi);
  emberAfNetworkFoundCallback(networkFound, lqi, rssi);
  emberAfPopNetworkIndex();
}

void emberScanCompleteHandler(int8u channel, EmberStatus status)
{
  emberAfPushCallbackNetworkIndex();
  emberAfScanCompleteCallback(channel, status);
  emberAfPopNetworkIndex();
}

void emberEnergyScanResultHandler(int8u channel, int8s rssi)
{
  emberAfPushCallbackNetworkIndex();
  emberAfEnergyScanResultCallback(channel, rssi);
  emberAfPopNetworkIndex();
}

void emAfPrintEzspEndpointFlags(int8u endpoint)
{
  // Not applicable for SOC
}

void send_data_frame(unsigned int received_short_id,unsigned char *msg,unsigned char length,unsigned char only_DCU)
{
  unsigned char status;
  EmberApsFrame globalApsFrame;

  /************** set cluster and profile according to DIGI Module *********/
  globalApsFrame.sourceEndpoint = 0x01;	
  globalApsFrame.destinationEndpoint = 0xE8;	
  globalApsFrame.options = EMBER_AF_DEFAULT_APS_OPTIONS;
  globalApsFrame.clusterId = 0x0011;
  globalApsFrame.profileId = 0xC105;

		if(Ember_network_up_send_frame)   // send message if network join successfully
  {
    if(only_DCU == 2)               // check if message need to be unicast or broadcast
    {

      status = emberAfSendBroadcast(0xFFFF,
                                    &globalApsFrame,
                                    length,
                                    msg);
    }
    else
    {
		    status = emberAfSendUnicast(EMBER_OUTGOING_DIRECT,
                                  received_short_id,
                                  &globalApsFrame,
                                  length,
                                  msg);
    }
  }
}

///////////////////////
unsigned char ascii_to_hex(unsigned char temp_s)
 {

        if(temp_s >= 48 && temp_s < 58)
        {
                temp_s = temp_s - 48;
                return temp_s;
        }
        else if(temp_s >= 65 && temp_s < 71)
        {
                temp_s = temp_s - 55;	
                return temp_s;
        }
        else if(temp_s >= 97 && temp_s < 103)
        {
                temp_s = temp_s - 87;
                return temp_s;	
        }
	return 0;		
 }



void Ember_Stack_run_Fun(void)
{
    halResetWatchdog();   // Periodically reset the watchdog.
    emberTick();          // Allow the stack to run.
    emberPermitJoining(0xFF);//emAfPermitJoin(0xFF);  //add logic to make mnetwork in multihope with trust center _5sept2012
    emberAfTick();
   // appTick();
    emberFormAndJoinTick();
    emberAfRunEvents();
}

void send_data_on_uart(unsigned char *data,unsigned int len)
{
    unsigned int i;

    for(i=0;i<len;i++)
    {
      emberSerialWaitSend(APP_SERIAL);
      emberSerialWriteByte(APP_SERIAL,data[i]);
    }
}

//void AUTO_DETECT_AO_DALI_FUN(void);
//void AUTO_DETECT_AO_DALI_FUN(void)
//{
//    if((energy_meter_ok == 0)&&(set_do_flag == 1))
//    {
//      if(error_condition.bits.b0 == 1)         // Lamp ON
//      {
//        /*Put ADC read function*/
//
//        
//      }
//  
//    }
//}