// Copyright 2014 Silicon Laboratories, Inc.

#include PLATFORM_HEADER
#include EMBER_AF_API_EMBER_TYPES

PGM int8u emTaskCount = EMBER_TASK_COUNT;
EmberTaskControl emTasks[EMBER_TASK_COUNT];
