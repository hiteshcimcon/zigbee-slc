// Copyright 2014 Silicon Laboratories, Inc.

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include "default-actions.h"
#include EMBER_AF_API_INFRARED_LED
#include EMBER_AF_API_STACK
#include EMBER_AF_API_RF4CE_PROFILE
#include EMBER_AF_API_RF4CE_ZRC20
#include EMBER_AF_API_RF4CE_ZRC20_ACTION_MAPPING_CLIENT


static const int8u sirdTv_1[] =
{
  // Header
  0x03, 0x01, 0x04, 0x00, 0x22, 0x00, 0x00, 0x00, 0xd2, 0x00,
  // Timers
  0x8c, 0x00, 0x7d, 0x00,
  0x8c, 0x00, 0xa4, 0x01,
  0x71, 0x04, 0x65, 0x04,
  0x8c, 0x00, 0xe6, 0x2d,
  // Repeat frame
  0x21, 0x11, 0x00, 0x00, 0x01, 0x11, 0x00, 0x00, 0x00, 0x01,
  0x00, 0x00, 0x01, 0x10, 0x11, 0x11, 0x13
};

static const int8u uirdTv_2[] =
{
    // Header
    0x84, 0x03, 0x00, 0x22, 0x00, 0xd2, 0x00,
    // Timers
    4,
    (int8u)((565 / 4) & 0xff),
    (int8u)((565 / 4) >> 8),
    (int8u)((1125 / 4) & 0xff),
    (int8u)((1125 / 4) >> 8),
    (int8u)((565 / 4) & 0xff),
    (int8u)((565 / 4) >> 8),
    (int8u)((2250 / 4) & 0xff),
    (int8u)((2250 / 4) >> 8),
    (int8u)((4500 / 4) & 0xff),
    (int8u)((4500 / 4) >> 8),
    (int8u)((9000 / 4) & 0xff),
    (int8u)((9000 / 4) >> 8),
    (int8u)((565 / 4) & 0xff),
    (int8u)((565 / 4) >> 8),
    (int8u)((47000 / 4) & 0xff),
    (int8u)((47000 / 4) >> 8),
    // Repeat pattern
    0x21, 0x11, 0x00, 0x00, 0x01, 0x11, 0x00, 0x00, 0x01, 0x01,
    0x00, 0x00, 0x00, 0x10, 0x11, 0x11, 0x13
};

static const int8u uirdMute[] =
{
  0x6F, 0x13, 0x25, 0xCE, 0x01, 0xFC, 0xD0, 0x45, 0xDA, 0x53,
  0xB3, 0x6D, 0xDA, 0x61, 0x68, 0x6C, 0x68, 0x56, 0x3D, 0x06,
  0x2D, 0x61, 0x24, 0x8B, 0x21, 0x00, 0x63, 0x11, 0x10, 0x01,
  0x71, 0x7D, 0x01, 0x00, 0x01, 0x7C, 0x6C, 0x00, 0x00, 0x53,
  0x61, 0x00, 0x69, 0x53, 0x6C, 0x00, 0x6C, 0x69, 0x53, 0x61,
  0x69, 0x61
};

const DefaultActionsIr_t defaultActionsIr[ 49] =
{
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 0: REWIND
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 1: STOP
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 2: RED (A)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 3: GUIDE
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 4: ROOT MENU (MENU)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 5: SETUP ENU (MENU)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 6: NO CODE (SET)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 7: NO CODE (VOICE)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 8: GREEN (B)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  // 9: ---
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //10: LEFT
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //11: ---
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //12: UP
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //13: NO CODE (TV)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //14: PLAY
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //15: YELLOW (C)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //16: DOWN
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //17: OK (SELECT)
  {HAL_INFRARED_LED_DB_FORMAT_SIRD, sirdTv_1, sizeof(sirdTv_1)},  //18: RIGHT
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //19: ---
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //20: NO CODE (STB)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //21: FSTFRWD
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //22: RECORD
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //23: BLUE (D)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //24: EXIT
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //25: DISPLAY INFORMATION (INFO)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //26: INPUT SELECT (INPUT)
  {HAL_INFRARED_LED_DB_FORMAT_UIRD, uirdTv_2, sizeof(uirdTv_2)},  //27: POWER  
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //28: MEDIA CONTEXT-SENSITIVE MENU (LIST)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //29: 7
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //30: 4
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //31: 1
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //32: VOL_DWN
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //33: VOL_UP
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //34: BACKWARD (REPLAY)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //35: 0
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //36: 8
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //37: 5
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //38: 2
  {HAL_INFRARED_LED_DB_FORMAT_UIRD_ENCRYPTED, uirdMute, sizeof(uirdMute)},   //39: MUTE
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //40: PREVIOUS CHANNEL (LAST)
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //41: PAUSE
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //42: ENTER
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //43: 9
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //44: 6
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //45: 3
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //46: CHL_DWN
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //47: CHL_UP
  {HAL_INFRARED_LED_DB_FORMAT_UDEFINED, NULL, 0},  //48: FORWARD (ADVANCE)
};


const DefaultActionsRf_t defaultActionsRf[ 49] =
{
  {0x48, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 0: REWIND
  {0x45, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 1: STOP
  {0x72, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 2: RED (A)
  {0x53, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 3: GUIDE
  {0x09, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 4: ROOT MENU (MENU)
  {0x0A, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 5: SETUP MENU (SETUP)
  {0xFF, 0x00                                            }, // 6: NO CODE (SET)
  {0xFF, 0x00                                            }, // 7: NO CODE (VOICE)
  {0x73, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, // 8: GREEN (B)
  {0xFF, 0x00                                            }, // 9: ---
  {0x03, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //10: LEFT
  {0xFF, 0x00                                            }, //11: ---
  {0x01, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //12: UP
  {0xFF, 0x00                                            }, //13: NO CODE (TV)
  {0x44, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //14: PLAY
  {0x74, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //15: YELLOW (C)
  {0x02, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //16: DOWN
  {0x00, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //17: OK (SELECT)
  {0x04, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //18: RIGHT
  {0xFF, 0x00                                            }, //19: ---
  {0xFF, 0x00                                            }, //20: NO CODE (STB)
  {0x49, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //21: FSTFRWD
  {0x47, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //22: RECORD
  {0x71, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //23: BLUE (D)
  {0x0D, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //24: EXIT
  {0x35, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //25: DISPLAY INFORMATION (INFO)
  {0x34, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //26: INPUT SELECT (INPUT)
  {0x40, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //27: POWER
  {0x11, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //28: MEDIA CONTEXT-SENSITIVE MENU (LIST)
  {0x27, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //29: 7
  {0x24, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //30: 4
  {0x21, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //31: 1
  {0x42, 0x00                                            }, //32: VOL_DWN
  {0x41, 0x00                                            }, //33: VOL_UP
  {0x4C, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //34: BACKWARD (REPLAY)
  {0x20, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //35: 0
  {0x28, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //36: 8
  {0x25, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //37: 5
  {0x22, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //38: 2
  {0x43, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //39: MUTE
  {0x32, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //40: PREVIOUS CHANNEL(LAST)
  {0x46, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //41: PAUSE
  {0x2B, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //42: ENTER
  {0x29, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //43: 9
  {0x26, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //44: 6
  {0x23, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //45: 3
  {0x31, 0x00                                            }, //46: CHL_DWN
  {0x30, 0x00                                            }, //47: CHL_UP
  {0x4B, RF4CE_ZRC_ACTION_MAPPING_RF_CONFIG_ATOMIC_ACTION}, //48: FORWARD (ADVANCE)
};
