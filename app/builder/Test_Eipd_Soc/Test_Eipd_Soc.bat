@echo off
REM THIS IS A GENERATED FILE: PLEASE DO NOT EDIT
REM Post Build processing for IAR Workbench.
SET TARGET_BPATH=%*%
SET PROJECT_DIR=%1%\..\..\..
echo " "
echo "This converts S37 to Ember Bootload File format if a bootloader has been selected in AppBuilder"
echo " "
@echo on
cmd /C ""%ISA3_UTILS_DIR%\em3xx_convert.exe"  "%TARGET_BPATH%.s37" "%TARGET_BPATH%.ebl" > "%TARGET_BPATH%-em3xx-convert-output.txt""
@echo off
type "%TARGET_BPATH%-em3xx-convert-output.txt"

echo " "
echo "This creates a ZigBee OTA file if the "OTA Client Policy Plugin" has been enabled.
echo "It uses the parameters defined there.  "
echo " "
@echo on
cmd /C ""C:/Users/hiteshpatel.EXCHANGE/Ember/EmberZNet5.4.5-GA/tool/image-builder/image-builder-windows.exe" --create "%TARGET_BPATH%.ota" --version 20202 --manuf-id 0x1002 --image-type 0 --tag-id 0x0000 --tag-file "%TARGET_BPATH%.ebl" --string "EBL Test_Eipd_Soc"" > "%TARGET_BPATH%-image-builder-output.txt"
@echo off
type "%TARGET_BPATH%-image-builder-output.txt"
