//

// This callback file is created for your convenience. You may add application
// code to this file. If you regenerate this file over a previous version, the
// previous version will be overwritten and any code you have added will be
// lost.

#include "app/framework/include/af.h"
#include "SLC/protocol.h"

/** @brief Read Attributes Response
 *
 * This function is called by the application framework when a Read Attributes
 * Response command is received from an external device.  The application should
 * return TRUE if the message was processed or FALSE if it was not.
 *
 * @param clusterId The cluster identifier of this response.  Ver.: always
 * @param buffer Buffer containing the list of read attribute status records. 
 * Ver.: always
 * @param bufLen The length in bytes of the list.  Ver.: always
 */
boolean emberAfReadAttributesResponseCallback(EmberAfClusterId clusterId,
                                              int8u *buffer,
                                              int16u bufLen)
{
  return FALSE;
}

/** @brief Default Response
 *
 * This function is called by the application framework when a Default Response
 * command is received from an external device.  The application should return
 * TRUE if the message was processed or FALSE if it was not.
 *
 * @param clusterId The cluster identifier of this response.  Ver.: always
 * @param commandId The command identifier to which this is a response.  Ver.:
 * always
 * @param status Specifies either SUCCESS or the nature of the error that was
 * detected in the received command.  Ver.: always
 */
boolean emberAfDefaultResponseCallback(EmberAfClusterId clusterId,
                                       int8u commandId,
                                       EmberAfStatus status)
{
  return FALSE;
}

/** @brief Pre Message Received
 *
 * This callback is the first in the Application Framework's message processing
 * chain. The Application Framework calls it when a message has been received
 * over the air but has not yet been parsed by the ZCL command-handling code. If
 * you wish to parse some messages that are completely outside the ZCL
 * specification or are not handled by the Application Framework's command
 * handling code, you should intercept them for parsing in this callback. 
     
 *   This callback returns a Boolean value indicating whether or not the message
 * has been handled. If the callback returns a value of TRUE, then the
 * Application Framework assumes that the message has been handled and it does
 * nothing else with it. If the callback returns a value of FALSE, then the
 * application framework continues to process the message as it would with any
 * incoming message.
        Note: 	This callback receives a pointer to an
 * incoming message struct. This struct allows the application framework to
 * provide a unified interface between both Host devices, which receive their
 * message through the ezspIncomingMessageHandler, and SoC devices, which
 * receive their message through emberIncomingMessageHandler.
 *
 * @param incomingMessage   Ver.: always
 */
unsigned int received_short_id=0;
extern int8s Slc_db;
boolean emberAfPreMessageReceivedCallback(EmberAfIncomingMessage* incomingMessage)
{
  	unsigned char status;
        static unsigned char receivecount=0;
	
	EmberApsFrame      globalApsFrame;
	///parse received frame here
//	emberAfAppPrint("pre message received=%s         \n",(char*)(incomingMessage->message));
//	emberAfCorePrintln("pre message received");
	//emberAfCorePrintln("%x:%x:%x:%x:%x:%x",(incomingMessage->message[0]),(incomingMessage->message[1]),(incomingMessage->message[2]),(incomingMessage->message[3]),(incomingMessage->message[4]),(incomingMessage->message[5]),(incomingMessage->message[6]));
	/////////////////////////
        checkfream(incomingMessage->message);

        received_short_id=incomingMessage->source;
        Slc_db=incomingMessage->lastHopRssi;
        //emberAfAppPrint("SLC_DB****=%d",Slc_db);

        ////////////////////////


//	return TRUE;
	
  return FALSE;
}

/** @brief Message Sent
 *
 * This function is called by the application framework from the message sent
 * handler, when it is informed by the stack regarding the message sent status.
 * All of the values passed to the emberMessageSentHandler are passed on to this
 * callback. This provides an opportunity for the application to verify that its
 * message has been sent successfully and take the appropriate action. This
 * callback should return a boolean value of TRUE or FALSE. A value of TRUE
 * indicates that the message sent notification has been handled and should not
 * be handled by the application framework.
 *
 * @param type   Ver.: always
 * @param indexOrDestination   Ver.: always
 * @param apsFrame   Ver.: always
 * @param msgLen   Ver.: always
 * @param message   Ver.: always
 * @param status   Ver.: always
 */
boolean emberAfMessageSentCallback(EmberOutgoingMessageType type,
                                   int16u indexOrDestination,
                                   EmberApsFrame* apsFrame,
                                   int16u msgLen,
                                   int8u* message,
                                   EmberStatus status)
{
      if(status==EMBER_SUCCESS)
	{
		//emberAfAppPrint("message sent=%s status=%d \n",message,status);
          if(Send_To_DCU == 2)
          {
            Send_To_DCU =0;
            Send_Data_Success =1;
            Network_Leave_Hr_Cnt =0;
            Network_Scan_Cnt =0;
          }
          else if(LastGasp_unicast_status==2)
          {
            LastGasp_unicast_status=0;
          }
          emberAfCoreFlush();
          return TRUE;
	}
	else
	{
          if(LastGasp_unicast_status==2)
          {
            LastGasp_unicast_status=2;
          }
	//	emberAfAppPrint("message send failed status=%d\n",status,msgLen);
  		emberAfCoreFlush();
  		return FALSE;
	}
  //return FALSE;
}

/** @brief Main Init
 *
 * This function is called from the application's main function. It gives the
 * application a chance to do any initialization required at system startup. Any
 * code that you would normally put into the top of the application's main()
 * routine should be put into this function. This is called before the clusters,
 * plugins, and the network are initialized so some functionality is not yet
 * available.
        Note: No callback in the Application Framework is
 * associated with resource cleanup. If you are implementing your application on
 * a Unix host where resource cleanup is a consideration, we expect that you
 * will use the standard Posix system calls, including the use of atexit() and
 * handlers for signals such as SIGTERM, SIGINT, SIGCHLD, SIGPIPE and so on. If
 * you use the signal() function to register your signal handler, please mind
 * the returned value which may be an Application Framework function. If the
 * return value is non-null, please make sure that you call the returned
 * function from your handler to avoid negating the resource cleanup of the
 * Application Framework itself.
 *
 */
void emberAfMainInitCallback(void)
{
}


