///*
// * File: adc.c
// * Description: EM3XX-specific ADC HAL functions
// *
// *
// * Copyright 2008 by Ember Corporation. All rights reserved.                *80*
// */
//#include PLATFORM_HEADER
//#include "stack/include/error.h"
//#include "hal/micro/micro-common.h"
//#include "hal/micro/cortexm3/micro-common.h"
//#include "micro/adc.h"
//
//#if (NUM_ADC_USERS > 8)
//  #error NUM_ADC_USERS must not be greater than 8, or int8u variables in adc.c must be changed
//#endif
//
//static int16u adcData;             // conversion result written by DMA
//static int8u adcPendingRequests;   // bitmap of pending requests
//static volatile int8u adcPendingConversion; // id of pending conversion
//static int8u adcReadingValid;      // bitmap of valid adcReadings
//static int16u adcReadings[NUM_ADC_USERS];
//static int16u adcConfig[NUM_ADC_USERS];
//static boolean adcCalibrated;
//static int16s Nvss;
//static int16s Nvdd;
//static int16u adcStaticConfig;
//
//void halAdcSetClock(boolean slow)
//{
//  if (slow) {
//    adcStaticConfig |= ADC_1MHZCLK_MASK;
//  } else {
//    adcStaticConfig &= ~ADC_1MHZCLK_MASK;
//  }
//}
//
//boolean halAdcGetClock(void)
//{
//  return (boolean)(adcStaticConfig & ADC_1MHZCLK_MASK);
//}
//
//
//// Define a channel field that combines ADC_MUXP and ADC_MUXN
//#define ADC_CHAN        (ADC_MUXP | ADC_MUXN)
//#define ADC_CHAN_BIT    ADC_MUXN_BIT
//
//void halAdcIsr(void)
//{
//  int8u i;
//  int8u conversion = adcPendingConversion; //fix 'volatile' warning; costs no flash
//
//  // make sure data is ready and the desired conversion is valid
//  if ( (INT_ADCFLAG & INT_ADCULDFULL)
//        && (conversion < NUM_ADC_USERS) ) {
//    adcReadings[conversion] = adcData;
//    adcReadingValid |= BIT(conversion); // mark the reading as valid
//    // setup the next conversion if any
//    if (adcPendingRequests) {
//      for (i = 0; i < NUM_ADC_USERS; i++) {
//        if (BIT(i) & adcPendingRequests) {
//          adcPendingConversion = i;     // set pending conversion
//          adcPendingRequests ^= BIT(i); //clear request: conversion is starting
//          ADC_CFG = adcConfig[i];
//          break; //conversion started, so we're done here (only one at a time)
//        }
//      }
//    } else {                                // no conversion to do
//      ADC_CFG = 0;                          // disable adc
//      adcPendingConversion = NUM_ADC_USERS; //nothing pending, so go "idle"
//    }
//  }
//  INT_ADCFLAG = 0xFFFF;
//  asm("DMB");
//}
//
//// An internal support routine called from functions below.
//// Returns the user number of the started conversion, or NUM_ADC_USERS
//// otherwise.
//ADCUser startNextConversion(void)
//{
//  int8u i;
//
//  ATOMIC (
//    // start the next requested conversion if any
//    if (adcPendingRequests && !(ADC_CFG & ADC_ENABLE)) {
//      for (i = 0; i < NUM_ADC_USERS; i++) {
//        if ( BIT(i) & adcPendingRequests) {
//          adcPendingConversion = i;     // set pending conversion
//          adcPendingRequests ^= BIT(i); // clear request
//          ADC_CFG = adcConfig[i];       // set the configuration to desired
//          INT_ADCFLAG = 0xFFFF;
//          INT_CFGSET = INT_ADC;
//          break;
//        }
//      }
//    } else {
//      i = NUM_ADC_USERS;
//    }
//  )
//  return i;
//}
//
//void halInternalInitAdc(void)
//{
//  // reset the state variables
//  adcPendingRequests = 0;
//  adcPendingConversion = NUM_ADC_USERS;
//  adcCalibrated = FALSE;
//  adcStaticConfig = ADC_1MHZCLK | ADC_ENABLE; // init config: 1MHz, low voltage
//
//  // set all adcReadings as invalid
//  adcReadingValid = 0;
//
//  // turn off the ADC
//  ADC_CFG = 0;                   // disable ADC, turn off HV buffers
//  ADC_OFFSET = ADC_OFFSET_RESET;
//  ADC_GAIN = ADC_GAIN_RESET;
//  ADC_DMACFG = ADC_DMARST;
//  ADC_DMABEG = (int32u)&adcData;
//  ADC_DMASIZE = 1;
//  ADC_DMACFG = (ADC_DMAAUTOWRAP | ADC_DMALOAD);
//
//  // clear the ADC interrupts and enable
//  INT_ADCCFG = INT_ADCULDFULL;
//  INT_ADCFLAG = 0xFFFF;
//  INT_CFGSET = INT_ADC;
//
//  emberCalibrateVref();
//}
//
//EmberStatus halStartAdcConversion(ADCUser id,
//                                  ADCReferenceType reference,
//                                  ADCChannelType channel,
//                                  ADCRateType rate)
//{
//
//   if(reference != ADC_REF_INT)
//    return EMBER_ERR_FATAL;
//
//  // save the chosen configuration for this user
//  adcConfig[id] = ( ((rate << ADC_PERIOD_BIT) & ADC_PERIOD)
//                  | ((channel << ADC_CHAN_BIT) & ADC_CHAN)
//                  | adcStaticConfig);
//
//  // if the user already has a pending request, overwrite params
//  if (adcPendingRequests & BIT(id)) {
//    return EMBER_ADC_CONVERSION_DEFERRED;
//  }
//
//  ATOMIC (
//    // otherwise, queue the transaction
//    adcPendingRequests |= BIT(id);
//    // try and start the conversion if there is not one happening
//    adcReadingValid &= ~BIT(id);
//  )
//  if (startNextConversion() == id)
//    return EMBER_ADC_CONVERSION_BUSY;
//  else
//    return EMBER_ADC_CONVERSION_DEFERRED;
//}
//
//EmberStatus halRequestAdcData(ADCUser id, int16u *value)
//{
//  //Both the ADC interrupt and the global interrupt need to be enabled,
//  //otherwise the ADC ISR cannot be serviced.
//  boolean intsAreOff = ( INTERRUPTS_ARE_OFF()
//                        || !(INT_CFGSET & INT_ADC)
//                        || !(INT_ADCCFG & INT_ADCULDFULL) );
//  EmberStatus stat;
//
//  ATOMIC (
//    // If interupts are disabled but the flag is set,
//    // manually run the isr...
//    //FIXME -= is this valid???
//    if( intsAreOff
//      && ( (INT_CFGSET & INT_ADC) && (INT_ADCCFG & INT_ADCULDFULL) )) {
//      halAdcIsr();
//    }
//
//    // check if we are done
//    if (BIT(id) & adcReadingValid) {
//      *value = adcReadings[id];
//      adcReadingValid ^= BIT(id);
//      stat = EMBER_ADC_CONVERSION_DONE;
//    } else if (adcPendingRequests & BIT(id)) {
//      stat = EMBER_ADC_CONVERSION_DEFERRED;
//    } else if (adcPendingConversion == id) {
//      stat = EMBER_ADC_CONVERSION_BUSY;
//    } else {
//      stat = EMBER_ADC_NO_CONVERSION_PENDING;
//    }
//  )
//  return stat;
//}
//
//EmberStatus halReadAdcBlocking(ADCUser id, int16u *value)
//{
//  EmberStatus stat;
//
//  do {
//    stat = halRequestAdcData(id, value);
//    if (stat == EMBER_ADC_NO_CONVERSION_PENDING)
//      break;
//  } while(stat != EMBER_ADC_CONVERSION_DONE);
//  return stat;
//}
//
//EmberStatus halAdcCalibrate(ADCUser id)
//{
//  EmberStatus stat;
//
//  halStartAdcConversion(id,
//                        ADC_REF_INT,
//                        ADC_SOURCE_GND_VREF2,
//                        ADC_CONVERSION_TIME_US_4096);
//  stat = halReadAdcBlocking(id, (int16u *)(&Nvss));
//  if (stat == EMBER_ADC_CONVERSION_DONE) {
//    halStartAdcConversion(id,
//                          ADC_REF_INT,
//                          ADC_SOURCE_VREG2_VREF2,
//                          ADC_CONVERSION_TIME_US_4096);
//    stat = halReadAdcBlocking(id, (int16u *)(&Nvdd));
//  }
//  if (stat == EMBER_ADC_CONVERSION_DONE) {
//    Nvdd -= Nvss;
//    adcCalibrated = TRUE;
//  } else {
//    adcCalibrated = FALSE;
//    stat = EMBER_ERR_FATAL;
//  }
//  return stat;
//}
//
//// Use the ratio of the sample reading to the of VDD_PADSA/2, a 'known'
//// value (nominally 900mV in normal mode, but slightly higher in boost mode)
//// to convert to 100uV units.
//// FIXME: support external Vref
////        use #define of Vref, ignore VDD_PADSA
//
//
//
//
//int16s halConvertValueToVolts(int16u value)
//{
//  int32s N;
//  int16s V;
//  int32s nvalue;
//
//  if (!adcCalibrated) {
//    halAdcCalibrate(ADC_USER_LQI);
//  }
//  if (adcCalibrated) {
//    assert(Nvdd);
//    nvalue = value - Nvss;
//    // Convert input value (minus ground) to a fraction of VDD/2.
//    N = ((nvalue << 16) + Nvdd/2) / Nvdd;
//    // Calculate voltage with: V = (N * Vreg/2) / (2^16)
//    // Mutiplying by Vreg/2*10 makes the result units of 100 uVolts
//    // (in fixed point E-4 which allows for 13.5 bits vs millivolts
//    // which is only 10.2 bits).
//    V = (int16s)((N*((int32s)halInternalGetVreg())*5) >> 16);
//    if (V > 12000) {
//      V = 12000;
//    }
//  } else {
//     V = -32768;
//  }
//  return V;
//}



/*
 * File: adc.c
 * Description: EM3XX-specific ADC HAL functions
 *
 *
 * Copyright 2008 by Ember Corporation. All rights reserved.                *80*
 */
#include PLATFORM_HEADER
#include "stack/include/error.h"
#include "hal/micro/micro-common.h"
#include "hal/micro/cortexm3/micro-common.h"
#include "micro/adc.h"
#include "app/framework/include/af.h"
//#include "SLC/Application_logic.h"
#if (NUM_ADC_USERS > 8)
  #error NUM_ADC_USERS must not be greater than 8, or int8u variables in adc.c must be changed
#endif
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
    extern unsigned char Photo_feedback_pin;
#endif
////////Analog Photosensor///////////
unsigned char Detect_Off_Condition=0,Make_Lamp_Off = 0;
float Photosensor_Set_FC =2.5,Photosensor_Set_FC_Lamp_OFF=3.7;
float photosensor=3.2,photosensor_average =0.0; //set 3.2 value default to prevent malfunction since first time it may get 0 unless finsih average value

float Photosensor_FootCandle=0.0,Photosensor_FootCandle_CHeck =0.0;
void Photocell_ON_OFF_Decision(void);
void Negetive_Foot_Candle_Ratio(void);
unsigned char chr_i=0,Pin_Detect_Dark =0;
/////////////////////
float ADC_Result=0.0;
float PA4_Analog_Value =0.0;
extern unsigned int analog_input_scaling_high_Value;
extern unsigned int analog_input_scaling_low_Value;

unsigned char Auto_Dimmer_Detect_analog_input_scaling_high_Value=22;
unsigned char Auto_Dimmer_Detect_analog_input_scaling_low_Value=0;

//unsigned int lamp_lumen;
void halAdcIsr(void)
{
//  int8u i;
//  int8u conversion = adcPendingConversion; //fix 'volatile' warning; costs no flash
//
//  // make sure data is ready and the desired conversion is valid
//  if ( (INT_ADCFLAG & INT_ADCULDFULL)
//        && (conversion < NUM_ADC_USERS) ) {
//    adcReadings[conversion] = adcData;
//    adcReadingValid |= BIT(conversion); // mark the reading as valid
//    // setup the next conversion if any
//    if (adcPendingRequests) {
//      for (i = 0; i < NUM_ADC_USERS; i++) {
//        if (BIT(i) & adcPendingRequests) {
//          adcPendingConversion = i;     // set pending conversion
//          adcPendingRequests ^= BIT(i); //clear request: conversion is starting
//          ADC_CFG = adcConfig[i];
//          break; //conversion started, so we're done here (only one at a time)
//        }
//      }
//    } else {                                // no conversion to do
//      ADC_CFG = 0;                          // disable adc
//      adcPendingConversion = NUM_ADC_USERS; //nothing pending, so go "idle"
//    }
//  }
//  INT_ADCFLAG = 0xFFFF;
//  asm("DMB");
}
EmberStatus halStartAdcConversion(ADCUser id,
                                  ADCReferenceType reference,
                                  ADCChannelType channel,
                                  ADCRateType rate)
{

}
EmberStatus halReadAdcBlocking(ADCUser id, int16u *value)
{

}

void halInternalInitAdc(void)
{
  // configure PB5 as analog
  SET_REG_FIELD(GPIO_PBCFGH, PB5_CFG, GPIOCFG_ANALOG);
    ADC_CFG = 0x0000E049; // 4D= 0.6  55= 1.2 v //45 channel N=0x8
  // configure interrupts for polling
//  ADC_OFFSET =30208;//[1.2 v ref 30208] 0x3AEA;//0x3AEA(15082);//0x3B79;//ADC_OFFSET_RESET;
   // ADC_OFFSET = 15078;//0x3AEA(15082);//0x3B79;//ADC_OFFSET_RESET;
 // ADC_GAIN = 35483;// [1.2 v ref 35483]//35490;//ADC_GAIN_RESET;
   //  ADC_GAIN = 35476;//ADC_GAIN_RESET;
//  INT_CFGCLR = INT_ADC;
    INT_CFGCLR = 0;
  INT_ADCCFG = INT_ADCULDFULL | INT_ADCDATA;
  // start the ADC
  ADC_CFG |= ADC_ENABLE;
  //emberCalibrateVref();
}

int16s adcMeasure(void)
{
  // clear interrupt flags
  INT_ADCFLAG = 0xFFFF;
  while (!(INT_ADCFLAG & INT_ADCDATA)) { // wait for INT_ADCDATA
  }
  return (int16s)ADC_DATA;
}
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
   unsigned char Photo_feedback_pin =0;//1
#endif

unsigned char one_time_access=1;
//float Photosensor= 0.0;
void ADC_Process(void)
{
//unsigned char Display[30];
  short int dif = 0;            // scaleHi = 50000,scaleLo = 0,
  float ADC_Value=0;
  int16s ngnd,ADC;
  int16s vdd;
  int16s nvreg;
  float Photosensor_On_Off_Decision;
  float temp_sam;
#ifndef ISLC_3100_V9_2 //need to add this EM and ADC pin shuffel for this hardware 27Jan2017
 if(one_time_access)
  {
    one_time_access=0;
    SET_REG_FIELD(GPIO_PBCFGH, PB5_CFG, GPIOCFG_ANALOG);
    ADC_CFG = 0x0000E049; // 4D= 0.6  55= 1.2 v //45 channel N=0x8
    INT_ADCCFG = 0;//INT_ADCULDFULL | INT_ADCDATA;
  // start the ADC
    INT_CFGCLR = 0;
 ADC_CFG |= ADC_ENABLE;
//      ///////////////////////
  ADC_OFFSET = ADC_OFFSET_RESET;
  ADC_GAIN = ADC_GAIN_RESET;
//////  // read factory trimmed vdd value from token
////// // halCommonGetToken(&vdd, TOKEN_MFG_1V8_REG_VOLTAGE);
//////  // take ground reading (ngnd)
  SET_REG_FIELD(ADC_CFG, ADC_MUXP, 0x8);
  SET_REG_FIELD(ADC_CFG, ADC_MUXN, 0x9);
  ngnd = adcMeasure();
////
////
////  // multiply ngnd by -2 and write to offset register
  ADC_OFFSET = (-2 * ngnd);
  ADC_OFFSET = ADC_OFFSET ;
//  sprintf(DIS,"ADC_OFFSET=%u",ADC_OFFSET );
//  emberAfGuaranteedPrint("%p>\r\n",DIS );
////  // read 1.8V regulator/2 (nvreg)
  SET_REG_FIELD(ADC_CFG, ADC_MUXP, 0xA);
//
  nvreg = adcMeasure();
////  nvreg = nvreg - ngnd;
////
//  sprintf(DIS,"nvreg=%u",nvreg);
//  emberAfGuaranteedPrint("%p>",DIS );
////  // adjust gain so 1200mV produces 16384 at adc raw output vdd=17930
////  // (32768 corresponds to a gain value of 1.0)
  ADC_GAIN = ((int16u)(32768L * (16384L ) / nvreg));
//ADC_GAIN=  ADC_GAIN+(200);
////
//  sprintf(DIS,"ADC_GAIN=%u ",ADC_GAIN);
//  emberAfGuaranteedPrint("%p>",DIS );
    ///////////////////////////////////////
////SET_REG_FIELD(ADC_CFG, ADC_MUXP, 0x8);
// // SET_REG_FIELD(ADC_CFG, ADC_MUXN, 0x8);
 SET_REG_FIELD(GPIO_PBCFGH, PB5_CFG, GPIOCFG_ANALOG);
 ADC_CFG = 0x0000E049; // 1110 0000 0100 1001// 4D= 0.6  55= 1.2 v //45 channel N=0x8
}
 SET_REG_FIELD(GPIO_PBCFGH, PB5_CFG, GPIOCFG_ANALOG);
 ADC_CFG = 0x0000E049; // 1110 0000 0100 1001// 4D= 0.6  55= 1.2 v //45 channel N=0x8

    ADC=adcMeasure();
  ADC=ADC;
  if(ADC<0)
    ADC=0;
//  ADC_Value=(((float)ADC*1.2)/16384);
//  sprintf(Display,"\r\n ADC1.2=%5.3f",ADC_Value);
//  emberAfGuaranteedPrint("%p>",Display );

//  ADC_Value=(((float)ADC*1.2)/16384)/0.3599;
  ADC_Value=(((float)ADC*1.2)/16384)/0.3606;
  Analog_Result = ADC_Value;

//  sprintf(Display,"[ADC=%5.3f]",ADC_Value);
  //emberAfGuaranteedPrint("%p",Display );

////////////////////////////
        dif = analog_input_scaling_high_Value - analog_input_scaling_low_Value;
	ADC_Value = ADC_Value * ((float)(dif/3.3));
	
	lamp_lumen = (unsigned int)ADC_Value;
//////////////////////////////
        //////////////////////////////////////
#endif
#ifdef ISLC3300_T8_V2
 SET_REG_FIELD(GPIO_PCCFGL, PA4_CFG, GPIOCFG_ANALOG);
 ADC_CFG = 0x0000E24D; //1110 0010 0100 1101// 4D= 0.6  55= 1.2 v //45 channel N=0x8

  ADC=adcMeasure();
  ADC=ADC;
  if(ADC<0)
    ADC=0;

    ADC_Value=(((float)ADC*1.2)/16384)/0.3606;
    PA4_Analog_Value = ADC_Value;
//  ADC_Value=(((float)ADC*1.2)/16384)/0.1162;
//    sprintf(Display,"\r\n ANALOG_PA4=%5.3f",ADC_Value);
//    emberAfGuaranteedPrint("%p>",Display);
#endif
////////////////////////////////////////////////////
//#ifdef ISLC_3100_V7_7
// SET_REG_FIELD(GPIO_PCCFGL, PB5_CFG, GPIOCFG_ANALOG);
// ADC_CFG = 0x0000E04D;  //1110 0000 0100 1101// ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB5 | Bits : 14bit
//                       
//  ADC=adcMeasure();
//  ADC=ADC;
//  if(ADC<0)
//    ADC=0;
//
//    PA4_Analog_Value=(((float)ADC*1.2)/16384)/0.3606;
//    
////***************** START ADC_2 PB7 DALI/10_DETECT *********************************
//
////    1110 0|001 0|100 1|101   E14D  ==> ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB7 | Bits : 14bit
////    1110 0|000 1|100 1|101   E0CD  ==> ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB6 | Bits : 14bit
    
//  ADC_CFG = 0x0000E0CD; // ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB7 | Bits : 14bit
//  ADC=adcMeasure();
//  ADC=ADC;
//  if(ADC<0)
//          ADC=0;
//  daliAoADC_FLOAT=(((float)ADC*1.2)/16384)/0.3606;
//
//#endif
//***************** END ADC_2 PB7 DALI/10_DETECT *********************************** 
////////////////////////////////////////////////
    
    
//***************** START ADC_2 PB6 DALI/10_DETECT ***********************************     
//    if(dali_support==2)
//    {
//      //1110 0|001 0|100 1|101   E14D  ==> ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB7 | Bits : 14bit
//      //1110 0|000 1|100 1|101   E0CD  ==> ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB6 | Bits : 14bit 
//      ADC_CFG = 0x0000E0CD; // ADC : emable | Clock : 1Mhz | ADC_Mn : VREF/2(0.6V) | ADC_Mp : PB7 | Bits : 14bit
//      ADC=adcMeasure();
//      ADC=ADC;
//      if(ADC<0)ADC=0;  
//      daliAoADC_FLOAT=(((float)ADC*1.2)/16384)/0.3606; 
//      dif = Auto_Dimmer_Detect_analog_input_scaling_high_Value - Auto_Dimmer_Detect_analog_input_scaling_low_Value;
//      daliAoADC_FLOAT = daliAoADC_FLOAT * ((float)(dif/3.3));      
//    }
//***************** END ADC_2 PB6 DALI/10_DETECT ***********************************     
    
///////////////ANALOG SENSOR///////////
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))

 SET_REG_FIELD(GPIO_PCCFGL, PC1_CFG, GPIOCFG_ANALOG);
 ADC_CFG = 0x0000E1CD; //1110 0001 1100 1101// 4D= 0.6  55= 1.2 v //45 channel N=0x8

  ADC=adcMeasure();
  ADC=ADC;
  if(ADC<0)
    ADC=0;


    //  ADC_Value=((((float)ADC*1.2)/16384));
        ADC_Value=(((float)ADC*1.2)/16384)/0.3589;
    temp_sam=ADC_Value;
//    if(Photosensor <0.02)
//    {
//        Photo_feedback_pin =1;
//    }
//    else
//    {
//        Photo_feedback_pin =0;
//    }
    //////////////////////
    photosensor_average =photosensor_average + temp_sam;
    chr_i++;
    if(chr_i>15)
    {

            photosensor = photosensor_average/chr_i;
            photosensor_average =0;
            chr_i =0;
    }

    	Photocell_ON_OFF_Decision();
	Photosensor_FootCandle_CHeck =Photosensor_FootCandle;
	Photosensor_FootCandle =Photosensor_FootCandle;
	Photosensor_On_Off_Decision = Photosensor_Set_FC;
	if(Photosensor_FootCandle <= Photosensor_On_Off_Decision)
	{
            Pin_Detect_Dark =1;//Photo_feedback_pin = 1;
		
	}
		
	Photosensor_On_Off_Decision = Photosensor_Set_FC_Lamp_OFF;
	Negetive_Foot_Candle_Ratio();
	if(Photosensor_FootCandle >= Photosensor_On_Off_Decision)
	{
            if(Make_Lamp_Off == 1)
            {
                    Pin_Detect_Dark =0;//Photo_feedback_pin = 0;
                    Make_Lamp_Off =0;
            }
	}
        if(Pin_Detect_Dark == 1)
        {
                Photo_feedback_pin = 1;
        }	
        else
        {
                Photo_feedback_pin = 0;
        }

    /////////////////////
#endif
///////////////////////////////////////

 //  ngnd = adcMeasure();
 //  sprintf(Display," ROW_DATA=%d",ngnd);
  // emberAfGuaranteedPrint("%p>",Display );

}


/////////////////////////////////////

void Photocell_ON_OFF_Decision(void)
{
  float MV_ARRAY[15]={0.0514,0.076,0.189,0.247,0.286,0.47,0.74,1.073,1.634,2.245,2.88};
    float FC_ARRAY[15]={0.5,1.2,2.5,3,3.75,5.5,7.5,11,15,18,25};

float f_temp=0.0,FC_temp=0.0,photosensor_temp =0.0;
	char v=0;
	for(v=0;v<11;v++)
	{
		if((photosensor <= MV_ARRAY[v+1]) && (photosensor >= MV_ARRAY[v+0]))
		{
                    f_temp =(MV_ARRAY[v+1] - MV_ARRAY[v+0]);
                    FC_temp =FC_ARRAY[v+1]-FC_ARRAY[v+0];

                    FC_temp =	(FC_temp/f_temp);
                    photosensor_temp= photosensor -MV_ARRAY[v+0];
                    //photosensor= photosensor*1000;
                    Photosensor_FootCandle =(FC_temp * photosensor_temp) + FC_ARRAY[v+0];
		}
	}
	if(photosensor <0.0514)
		Photosensor_FootCandle =0;
	if(photosensor >2.88)
		Photosensor_FootCandle =25;	
			
}
void Negetive_Foot_Candle_Ratio(void)
{
	unsigned long f_local =0;
	f_local =(unsigned long) Photosensor_FootCandle;
	
	if(Photosensor_Set_FC_Lamp_OFF < Photosensor_Set_FC)
	{
		if(f_local <= Photosensor_Set_FC_Lamp_OFF)
		{
			Make_Lamp_Off = 1;
			Detect_Off_Condition =1;
		}
		if((f_local<=Photosensor_Set_FC)&&(f_local > Photosensor_Set_FC_Lamp_OFF)&&(Detect_Off_Condition == 1))	
		{
			Make_Lamp_Off = 1;
		}
		if( f_local> Photosensor_Set_FC)
		{
			Make_Lamp_Off =1;
			Detect_Off_Condition =0;	
		}
	}
	else
	{
		Make_Lamp_Off =1;	
	}
}	




//void Photocell_ON_OFF_Decision(void)
//{
//	float f_temp=0.0;
//	
//	if((photosensor < 0.076)&&(photosensor > 0.0514))
//	{
//		f_temp =(0.076 - 0.0514);
//		
//		f_temp =	f_temp/(1.2-0.5);
//		Photosensor_FootCandle =0.5 + f_temp;
//	}	
//}
////////////////////////////////////
