// *****************************************************************************
// * battery-monitor.c
// *
// * Code to implement a battery monitor.
// *
// * This code will read the battery voltage during a transmission (in theory
// * when we are burning the most current), and update the battery voltage
// * attribute in the power configuration cluster.
// *
// *
// * Copyright 2015 Silicon Laboratories, Inc.                              *80*
// *****************************************************************************

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include "stack/include/ember-types.h"
#include "stack/include/event.h"
#include "hal/hal.h"
#include "hal/micro/battery-monitor.h"
#include "hal/micro/generic-interrupt-control.h"
#include "hal/micro/gpio-sensor.h"

// Shorter macros for plugin options
#define FIFO_SIZE EMBER_AF_PLUGIN_BATTERY_MONITOR_SAMPLE_FIFO_SIZE
#define MS_BETWEEN_BATTERY_CHECK \
          (EMBER_AF_PLUGIN_BATTERY_MONITOR_MONITOR_TIMEOUT_M * 60 * 1000)

#define MAX_INT_MINUS_DELTA 0xe0000000
          
//------------------------------------------------------------------------------
// Forward Declaration
static int16u filterVoltageSample(int16u sample);
static void batteryMonitorIsr(void);

//------------------------------------------------------------------------------
// Globals

EmberEventControl emberAfPluginBatteryMonitorReadADCEventControl;

// structure used to store irq configuration from GIC plugin
static HalGenericInterruptControlIrqCfg *irqConfig;

// count used to track when the last measurement occurred
// Ticks start at 0.  We use this value to limit how frequently we make
// measurements in an effort to conserve battery power.  By setting this to an
// arbitrary value close to MAX_INT, we are going to make sure we make a
// battery measurement on the first transmission.
static int32u lastBatteryMeasureTick = MAX_INT_MINUS_DELTA;

// sample FIFO access variables
static int8u samplePtr = 0;
static int16u voltageFifo[FIFO_SIZE];
static boolean fifoInitialized = FALSE;

// Remember the last reported voltage value from callback, which will be the
// return value if anyone needs to manually poll for data
static int16u lastReportedVoltageMilliV;

//------------------------------------------------------------------------------
// Implementation of public functions

void emberAfPluginBatteryMonitorInitCallback(void)
{
  halBatteryMonitorInitialize();
}

void halBatteryMonitorInitialize(void)
{
  // Set up the generic interrupt controller to handle changes on the gpio
  // sensor
  irqConfig = halGenericInterruptControlIrqCfgInitialize(
    BATTERY_MONITOR_TX_ACTIVE_PIN,
    BATTERY_MONITOR_TX_ACTIVE_PORT,
    BATTERY_MONITOR_IRQ);
  halGenericInterruptControlIrqIsrAssignFxn(irqConfig, batteryMonitorIsr);
  halGenericInterruptControlIrqEnable(irqConfig);
}

int16u halGetBatteryVoltageMilliV(void)
{
  return lastReportedVoltageMilliV;
}

// This event will be triggered by the TX Active ISR.  It will sample the ADC
// during a radio transmission and notify any interested parties of a new valid
// battery voltage level via the emberAfPluginBatteryMonitorDataCallback
void emberAfPluginBatteryMonitorReadADCEventHandler(void)
{
  emberEventControlSetInactive(emberAfPluginBatteryMonitorReadADCEventControl);

  int16u voltageMilliV;
  int32u currentMsTick = halCommonGetInt32uMillisecondTick();
  int32u timeSinceLastMeasureMS = currentMsTick - lastBatteryMeasureTick;

  if (timeSinceLastMeasureMS >= MS_BETWEEN_BATTERY_CHECK) {
    // The most common and shortest (other than the ACK) transmission is the
    // data poll.  It takes 512 uS for a data poll.  Therefore, the conversion
    // needs to be faster than 256.
    // However, I think this is ignored, and halMeasureVdd uses 256 regardless
    // of what value we give it.
    voltageMilliV = halMeasureVdd(ADC_CONVERSION_TIME_US_256);

    // filter the voltage
    voltageMilliV = filterVoltageSample(voltageMilliV);

    emberAfPluginBatteryMonitorDataCallback(voltageMilliV);
    lastReportedVoltageMilliV = voltageMilliV;
    lastBatteryMeasureTick = currentMsTick;
  }
}

//------------------------------------------------------------------------------
// Implementation of private functions

static int16u filterVoltageSample(int16u sample)
{
  int16u voltageSum;
  int8u i;

  if (fifoInitialized) {
    voltageFifo[samplePtr++] = sample;

    if (samplePtr >= FIFO_SIZE) {
      samplePtr = 0;
    }
    voltageSum = 0;
    for (i=0; i<FIFO_SIZE; i++) {
      voltageSum += voltageFifo[i];
    }
    sample = voltageSum / FIFO_SIZE;
  } else {
    for (i=0; i<FIFO_SIZE; i++) {
      voltageFifo[i] = sample;
    }
    fifoInitialized = TRUE;
  }

  return sample;
}

//!!!CAUTION!!!!
// This function runs in interrupt context.  Take proper care when modifying or
// adding functionality.
static void batteryMonitorIsr(void)
{
  INT_MISS = irqConfig->irqMissBit;
  INT_GPIOFLAG = irqConfig->irqFlagBit;

  if (halGenericInterruptControlIrqReadGpio(irqConfig))
  {
    emberEventControlSetActive(emberAfPluginBatteryMonitorReadADCEventControl);
  }
}
