// *****************************************************************************
// * led-blink.h
// *
// * API to allow for basic control of activity LEDs, including on/off
// * functionality and blink pattern creation.
// *
// * Copyright 2015 Silicon Laboratories, Inc.                              *80*
// *****************************************************************************

#include PLATFORM_HEADER
#include "stack/include/ember-types.h"
#include "stack/include/event.h"
#include "hal/hal.h"
#include "hal/micro/micro.h"
#include "hal/micro/led-blink.h"

//------------------------------------------------------------------------------
// Private plugin macros

// Register macros helpful for GPIO interaction
#define GPIO_PxCLR_BASE (GPIO_PACLR_ADDR)
#define GPIO_PxSET_BASE (GPIO_PASET_ADDR)
#define GPIO_PxOUT_BASE (GPIO_PAOUT_ADDR)
// Each port is offset from the previous port by the same amount
#define GPIO_Px_OFFSET  (GPIO_PBCFGL_ADDR-GPIO_PACFGL_ADDR)

// Length restriction for LED pattern
#define MAX_BLINK_PATTERN_LENGTH  20


//------------------------------------------------------------------------------
// Plugin events
EmberEventControl emberAfPluginLedBlinkLedEventFunctionEventControl;

//------------------------------------------------------------------------------
// Plugin private types and enums
enum {
  LED_ON            = 0x00,
  LED_OFF           = 0x01,
  LED_BLINKING_ON   = 0x02,
  LED_BLINKING_OFF  = 0x03,
  LED_BLINK_PATTERN = 0x04,
};

typedef int8u gpioBlinkState;

//------------------------------------------------------------------------------
// Forward declaration of private plugin functions
static void turnLedOn( int8u led );
static void turnLedOff( int8u led );
static void setBit(int8u *data, int8u bit);
static void clearBit(int8u *data, int8u bit);

//------------------------------------------------------------------------------
// Plugin private global variables
static gpioBlinkState ledEventState = LED_ON;
static int8u ledBlinkCount = 0x00;
static int16u ledBlinkTime;
static int8u activeLed = BOARDLED1;
static int16u blinkPattern[MAX_BLINK_PATTERN_LENGTH];
static int8u blinkPatternLength;
static int8u blinkPatternPointer;

//------------------------------------------------------------------------------
// Plugin registered callback implementations

//------------------------------------------------------------------------------
// Plugin event handlers
void emberAfPluginLedBlinkLedEventFunctionEventHandler( void )
{
  switch(ledEventState) {
  case LED_ON:
    // was on.  this must be time to turn it off.
    turnLedOff(activeLed);
    emberEventControlSetInactive(emberAfPluginLedBlinkLedEventFunctionEventControl);
    break;

  case LED_OFF:
    // was on.  this must be time to turn it off.
    turnLedOn(activeLed);
    emberEventControlSetInactive(emberAfPluginLedBlinkLedEventFunctionEventControl);
    break;

  case LED_BLINKING_ON:
    turnLedOff(activeLed);
    if (ledBlinkCount > 0) {
      if (ledBlinkCount != 255) { // blink forever if count is 255
        ledBlinkCount --;
      }
      if (ledBlinkCount > 0) {
        ledEventState = LED_BLINKING_OFF;
        emberEventControlSetDelayMS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                                    ledBlinkTime);

      } else {
        ledEventState = LED_OFF;
        emberEventControlSetInactive(emberAfPluginLedBlinkLedEventFunctionEventControl);
      }
    } else {
      ledEventState = LED_BLINKING_OFF;
      emberEventControlSetDelayMS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                                  ledBlinkTime);
    }
    break;
  case LED_BLINKING_OFF:
    turnLedOn(activeLed);
    ledEventState = LED_BLINKING_ON;
    emberEventControlSetDelayMS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                                ledBlinkTime);
    break;
  case LED_BLINK_PATTERN:
    if (ledBlinkCount == 0) {
      turnLedOff(activeLed);

      ledEventState = LED_OFF;
      emberEventControlSetInactive(emberAfPluginLedBlinkLedEventFunctionEventControl);

      break;
    }

    if (blinkPatternPointer %2 == 1) {
      turnLedOff(activeLed);
    } else {
      turnLedOn(activeLed);
    }

    emberEventControlSetDelayMS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                                blinkPattern[blinkPatternPointer]);

    blinkPatternPointer ++;

    if (blinkPatternPointer >= blinkPatternLength) {
      blinkPatternPointer = 0;
      if (ledBlinkCount != 255) { // blink forever if count is 255
        ledBlinkCount --;
      }
    }

  default:
    break;
  }
}

//------------------------------------------------------------------------------
// Plugin public API function implementations

void halLedBlinkLedOn( int8u time )
{
  turnLedOn(activeLed);
  ledEventState = LED_ON;

  if (time > 0) {
    emberEventControlSetDelayQS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                                ((int16u) time) * 4);
  } else {
    emberEventControlSetInactive(emberAfPluginLedBlinkLedEventFunctionEventControl);
  }
}

void halLedBlinkLedOff( int8u time )
{
  turnLedOff(activeLed);
  ledEventState = LED_OFF;

  if (time > 0) {
    emberEventControlSetDelayQS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                                ((int16u) time) * 4);
  } else {
    emberEventControlSetInactive(emberAfPluginLedBlinkLedEventFunctionEventControl);
  }
}

void halLedBlinkBlink( int8u count, int16u blinkTime)
{
  ledBlinkTime = blinkTime;
  turnLedOff(activeLed);
  ledEventState = LED_BLINKING_OFF;
  emberEventControlSetDelayMS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                              ledBlinkTime);
  ledBlinkCount = count;
}

void halLedBlinkPattern( int8u count, int8u length, int16u *pattern)
{
  int8u i;

  if (length < 2) {
    return;
  }

  turnLedOn(activeLed);

  ledEventState = LED_BLINK_PATTERN;

  if (length > MAX_BLINK_PATTERN_LENGTH) {
    length = MAX_BLINK_PATTERN_LENGTH;
  }

  blinkPatternLength = length;
  ledBlinkCount = count;

  for(i=0; i<blinkPatternLength; i++) {
    blinkPattern[i] = pattern[i];
  }

  emberEventControlSetDelayMS(emberAfPluginLedBlinkLedEventFunctionEventControl,
                              blinkPattern[0]);

  blinkPatternPointer = 1;
}

void halLedBlinkSetActivityLed(int8u led)
{
  activeLed = led;
}

//------------------------------------------------------------------------------
// Plugin private function implementations

// *****************************************************************************
// function to set the GPIO and maintain the state during sleep.
// Port is 0 for port a, 1 for port b, and 2 for port c.
void halLedBlinkSleepySetGpio( int8u port, int8u pin )
{
  assert( port < 3 );
  assert( pin < 8 );

  *((volatile int32u *)(GPIO_PxSET_BASE+(GPIO_Px_OFFSET*(port)))) = BIT(pin);

  // Make sure this stays set during the next power cycle
  setBit(&(gpioOutPowerUp[port]), pin);
  setBit(&(gpioOutPowerDown[port]), pin);
}

// *****************************************************************************
// function to clear the GPIO and maintain the state during sleep.
// Port is 0 for port a, 1 for port b, and 2 for port c.
void halLedBlinkSleepyClearGpio( int8u port, int8u pin )
{
  assert( port < 3 );
  assert( pin < 8 );

  *((volatile int32u *)(GPIO_PxCLR_BASE+(GPIO_Px_OFFSET*(port)))) = BIT(pin);

  // Make sure this stays clear during the next power cycle
  clearBit(&(gpioOutPowerUp[port]), pin);
  clearBit(&(gpioOutPowerDown[port]), pin);
}

// *****************************************************************************
// Helper function to set a bit
static void setBit(int8u *data, int8u bit)
{
  int8u mask = 0x01 << bit;

  *data = *data | mask;
}

// *****************************************************************************
// Helper function to clear a bit
static void clearBit(int8u *data, int8u bit)
{
  int8u mask = ~(0x01 << bit);

  *data = *data & mask;
}

// *****************************************************************************
// Drive the LED for a GPIO high and update sleepy state
static void turnLedOn( int8u led )
{
  int8u port = (led) >> 3;
  int8u pin = (led) & 0x07;

  halLedBlinkSleepyClearGpio( port, pin );
}

// *****************************************************************************
// Drive the LED for a GPIO low and update sleepy state
static void turnLedOff( int8u led )
{

  int8u port = (led) >> 3;
  int8u pin = (led) & 0x07;

  halLedBlinkSleepySetGpio( port, pin );
}
