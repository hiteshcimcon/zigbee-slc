///** @file hal/micro/cortexm3/buzzer.c
// *  @brief  Sample API functions for using a timer to play tunes on a buzzer.
// *
// * <!-- Copyright 2015 Silicon Laboratories, Inc.                        *80*-->
// */
//
//#include PLATFORM_HEADER
//#include "stack/include/ember.h"
//#include "hal/hal.h"
//
//// Tune state
//int8u PGM *currentTune = NULL;
//int8u tunePos = 0;
//int16u currentDuration = 0;
//boolean tuneDone=TRUE;
//
//// Keep the defaults if not defined on the board file. Default is TMR1
//#ifndef BUZZER_CLK
//#define BUZZER_CLK TIM1_OR
//#endif
//
//#ifndef BUZZER_PSC
//#define BUZZER_PSC TIM1_PSC
//#endif
//
//#ifndef BUZZER_UPDATE
//#define BUZZER_UPDATE TIM1_EGR
//#endif
//
//#ifndef BUZZER_OUTPUT_CFG
//#define BUZZER_OUTPUT_CFG TIM1_CCMR1
//#endif
//
//#ifndef BUZZER_OUTPUT_MODE
//#define BUZZER_OUTPUT_MODE (0x3 << TIM_OC2M_BIT)
//#endif
//
//#ifndef BUZZER_OUTPUT_ENABLE
//#define BUZZER_OUTPUT_ENABLE TIM1_CCER
//#endif
//
//#ifndef BUZZER_OUTPUT_ENABLE_CHANNEL
//#define BUZZER_OUTPUT_ENABLE_CHANNEL TIM_CC2E
//#endif
//
//#ifndef BUZZER_TOP
//#define BUZZER_TOP TIM1_ARR
//#endif
//
//#ifndef BUZZER_CNT
//#define BUZZER_CNT TIM1_CNT
//#endif
//
//#ifndef BUZZER_INT_MASK
//#define BUZZER_INT_MASK INT_TIM1CFG
//#endif
//
//#ifndef BUZZER_INT
//#define BUZZER_INT INT_TIM1FLAG
//#endif
//
//#ifndef BUZZER_BIT
//#define BUZZER_BIT INT_TIMUIF
//#endif
//
//#ifndef BUZZER_ENABLE
//#define BUZZER_ENABLE TIM1_CR1
//#endif
//
//#ifndef BUZZER_TEMPO
//#define BUZZER_TEMPO 200
//#endif
//
//// EO defaults
//
//static void endTune(void)
//{
//    // Also useful for "cleaning out the timer."
//    BUZZER_INT_MASK &= ~BUZZER_BIT; //disable the Timer, CNT ?= TOP interrupt
//    INT_CFGSET &= ~INT_TIM1; //stop the interrupts
//    BUZZER_OUTPUT_ENABLE  = 0; //disable output
//    BUZZER_ENABLE = 0;; //disable timer
//    tuneDone = TRUE;
//    BUZZER_TOP = 0;
//    BUZZER_CNT = 0;
//}
//
//static void setUpNextNoteOrStop(void)
//{
//  if (currentTune[tunePos + 1]) {
//    if (currentTune[tunePos]) {
//      // generate a note
//      BUZZER_TOP = currentTune[tunePos]*13; //magical conversion
//      BUZZER_CNT = 0; //force the counter back to zero to prevent missing BUZZER_TOP
//      BUZZER_OUTPUT_ENABLE = BUZZER_OUTPUT_ENABLE_CHANNEL; //enable channel output
//      // work some magic to determine the duration based upon the frequency
//      // of the note we are currently playing.
//      currentDuration = (((int16u)BUZZER_TEMPO
//                          * (int16u)currentTune[tunePos + 1])
//                         / (currentTune[tunePos] / 15));
//    } else {
//      // generate a pause
//      BUZZER_TOP = 403; //simulated a note (NOTE_B4*13), but output is disabled
//      BUZZER_CNT = 0; //force the counter back to zero to prevent missing BUZZER_TOP
//      BUZZER_OUTPUT_ENABLE = 0; //Output waveform disabled for silence
//      currentDuration = (((int16u)BUZZER_TEMPO
//                          * (int16u)currentTune[tunePos + 1])
//                         / (31 / 15));
//    }
//    tunePos += 2;
//  } else {
//    endTune();
//  }
//}
//
//void halPlayTune_P(int8u PGM *tune, boolean bkg)
//{
//  //Start from a fully disabled timer.  (Tune's cannot be chained with
//  //this play tune functionality.)
//  endTune();
//
//  //According to emulator.h, buzzer is on pin 15 which is mapped
//  //to channel 2 of TMR1
//  BUZZER_CLK = 0; //use 12MHz clock
//  BUZZER_PSC = 5; //2^5=32 -> 12MHz/32 = 375kHz = 2.6us tick
//  BUZZER_UPDATE = 1; //trigger update event to load new prescaler value
//  BUZZER_OUTPUT_CFG  = 0; //start from a zeroed configuration
//  //Output waveform: toggle on CNT reaching TOP
//  BUZZER_OUTPUT_CFG |= BUZZER_OUTPUT_MODE;
//
//  currentTune = tune;
//  tunePos = 0;
//  tuneDone = FALSE;
//
//  ATOMIC(
//    BUZZER_INT_MASK = BUZZER_BIT; //enable the Timer 1, CNT ?= TOP interrupt
//    INT_CFGSET |= INT_TIM1; //enable top level timer interrupts
//    BUZZER_ENABLE |= TIM_CEN; //enable counting
//    setUpNextNoteOrStop();
//  )
//  while (!bkg && !tuneDone) {
//    halResetWatchdog();
//  }
//}
//
//void halTimer1Isr(void)
//{
//  if (currentDuration-- == 0) {
//    setUpNextNoteOrStop();
//  }
//  //clear interrupt
//  BUZZER_INT = 0xFFFFFFFF;
//}
//
//int8u PGM hereIamTune[] = {
//  NOTE_B4,  1,
//  0,        1,
//  NOTE_B4,  1,
//  0,        1,
//  NOTE_B4,  1,
//  0,        1,
//  NOTE_B5,  5,
//  0,        0
//};
//
//void halStackIndicatePresence(void)
//{
//  halPlayTune_P(hereIamTune, TRUE);
//}
/** @file hal/micro/cortexm3/buzzer.c
*  @brief  Sample API functions for using a timer to play tunes on a buzzer.
*
* <!-- Author(s): Areeya Vimayangkoon -->
* <!-- Copyright 2007 by Ember Corporation. All rights reserved.       *80*-->
*/

#include PLATFORM_HEADER
#include "stack/include/ember.h"
#include "hal/hal.h"
#include "SLC/generic.h"
#include "SLC/protocol.h"
#include "SLC/Application_logic.h"
#include "SLC/SLC_defination.h"
// Tune state
volatile unsigned int Tilt_Timeout_Cnt =0;
volatile unsigned char Lamp_ON_Flag =0,Lamp_OFF_Flag=0;
#define TE 39/2          // half bit time = 417 usec
#define MIN_TE TE - 4    // minimum half bit time
#define MAX_TE TE + 4    // maximum half bit time
#define MIN_2TE 2*TE - 4 // minimum full bit time
#define MAX_2TE 2*TE + 4 // maximum full bit time
unsigned long low_time;      // captured puls low time
unsigned long high_time;     // captured puls high time
 unsigned short int Network_Leave_Hr_Cnt =0;
unsigned char count11,answer1,cnt1,cnt2,cnt3,recieved=0x00,cnt4,s,answer2=0;
//unsigned long Sandip1[10],k=0,Sandip2[10];
unsigned int dali_dim_cnt=0,send_data_cnt=0;
volatile unsigned char Read_Data_send_cnt=0;
////////////////////////////////////
unsigned char SEARCH_DALI_HIGH,SEARCH_DALI_MID,SEARCH_DALI_LOW,compare,address1,compare1,actual_address =3,address2;
unsigned char DALI_ADD[4];
////////////////////////////////////
int8u PGM *currentTune = NULL;
int8u tunePos = 0;
int16u currentDuration = 0;
boolean tuneDone=TRUE;
extern unsigned int Tilt_counter;
extern unsigned char Time_Out_Check,Time_Out_Flag;
// Keep the defaults if not defined on the board file. Default is TMR1
#ifndef BUZZER_CLK
#define BUZZER_CLK TIM1_OR
#endif

#ifndef BUZZER_PSC
#define BUZZER_PSC TIM1_PSC
#endif

#ifndef BUZZER_UPDATE
#define BUZZER_UPDATE TIM1_EGR
#endif

#ifndef BUZZER_OUTPUT_CFG
#define BUZZER_OUTPUT_CFG TIM1_CCMR1
#endif

#ifndef BUZZER_OUTPUT_MODE
#define BUZZER_OUTPUT_MODE (0x3 << TIM_OC2M_BIT)
#endif

#ifndef BUZZER_OUTPUT_ENABLE
#define BUZZER_OUTPUT_ENABLE TIM1_CCER
#endif

#ifndef BUZZER_TOP
#define BUZZER_TOP TIM1_ARR
#endif

#ifndef BUZZER_COMPARE
#define BUZZER_COMPARE TIM1_CCR2
#endif

#ifndef BUZZER_CNT
#define BUZZER_CNT TIM1_CNT
#endif

#ifndef BUZZER_INT_MASK
#define BUZZER_INT_MASK INT_TIM1CFG
#endif

#ifndef BUZZER_INT
#define BUZZER_INT INT_TIM1FLAG
#endif

#ifndef BUZZER_BIT
#define BUZZER_BIT INT_TIMUIF
#endif

#ifndef BUZZER_ENABLE
#define BUZZER_ENABLE TIM1_CR1
#endif

#ifndef BUZZER_TEMPO
#define BUZZER_TEMPO 200
#endif

#ifndef BUZZER_OUTPUT_ENABLE_CHANNEL
#define BUZZER_OUTPUT_ENABLE_CHANNEL TIM_CC2E
#endif

//#if defined(DALI_SUPPORT)

#define DALI_DIM_INIT_VAL 255

//void scenes_write(unsigned char scenes_num, unsigned char scenes_value);
void scenes_read(unsigned char scenes_num);

////////////////////////////////////////////////////////////////////
//void scenes_write2(unsigned char bankno,unsigned char bankoffset,unsigned char *scenes_value, unsigned char len);
void scenes_read_3(unsigned char scenes_num,unsigned char dali_add);
void Read_Dali_Ballast_Information(unsigned char driverno,unsigned char dali_add);
void scenes_write3(unsigned char scenes_num, unsigned char scenes_value,unsigned char dali_add);
void search_address(void);
void assigning_address(void);
unsigned char search_compare(unsigned char low);
void intialize_randomize(void);
///////////////////////////////////////////////////////////////////


void DALI_ON(unsigned char);
void DALI_Send(void);
void MAKE_DEFAULT(void);
void UNICAST_SHORTADDRESS(void);
static void DALI_Shift_Bit(BYTE val);
static void DALI_Decode(void);

static unsigned char value;                                  // used for dali send bit
static unsigned char previous;                                  // used for dali send bit
static unsigned char position;                                         // keeps track of sending bit position
unsigned short int forward = 0xff00;
static unsigned int frame;
unsigned char answer = 0;                                     		// DALI slave answer
unsigned char f_dalitx = 0;
unsigned char f_dalirx = 0;
static unsigned char f_repeat=0;                                         // flag command shall be repeated
static unsigned char f_busy;
unsigned int ONOFF_CNT=0;
unsigned int SSN_CNT =0;
extern unsigned char dali_delay;
//#endif
unsigned char DALI_Ballast_Info[4][16];

// EO defaults

/////////////////////PWM///////////////////////

#define   PWM_CLOCK           TIM2_OR
#define   PWM_PSC             TIM2_PSC
#define   PWM_UPDATE          TIM2_EGR
#define   PWM_OUTPUT_CFG      TIM2_CCMR2
#define   PWM_OUTPUT_ENABLE   TIM2_CCER

#define   PWM_CNT             TIM2_CNT

#define   PWM_INT_MASK        INT_TIM2CFG



#define   PWM_INT             INT_TIM2FLAG



#define   PWM_BIT             INT_TIMUIF


#define   PWM_ENABLE          TIM2_CR1

/////////////////////////////////////////////
///////////////////////////////////////////////
//void halPlayTune_P(int8u PGM *tune, boolean bkg)
//{
//  //Start from a fully disabled timer.  (Tune's cannot be chained with
//  //this play tune functionality.)
//  endTune();
//
//  //According to emulator.h, buzzer is on pin 15 which is mapped
//  //to channel 2 of TMR1
//  BUZZER_CLK = 0; //use 12MHz clock
//  BUZZER_PSC = 5; //2^5=32 -> 12MHz/32 = 375kHz = 2.6us tick
//  BUZZER_UPDATE = 1; //trigger update event to load new prescaler value
//  BUZZER_OUTPUT_CFG  = 0; //start from a zeroed configuration
//  //Output waveform: toggle on CNT reaching TOP
//  BUZZER_OUTPUT_CFG |= BUZZER_OUTPUT_MODE;
//
//  currentTune = tune;
//  tunePos = 0;
//  tuneDone = FALSE;
//
//  ATOMIC(
//    BUZZER_INT_MASK = BUZZER_BIT; //enable the Timer 1, CNT ?= TOP interrupt
//    INT_CFGSET |= INT_TIM1; //enable top level timer interrupts
//    BUZZER_ENABLE |= TIM_CEN; //enable counting
//    setUpNextNoteOrStop();
//  )
//  while (!bkg && !tuneDone) {
//    halResetWatchdog();
//  }
//}
////////////////////////////////////////////////

void halPlayTune_P(int8u PGM *tune, boolean bkg)
{

 if(Default_Para_Set != 0)
 //if(Set_default_perameter == 1)
 {
//#if defined(DALI_SUPPORT)
   if((dali_support == 1 )||(dali_support == 2))
   {
      TIM2_OR  =0;
      TIM2_PSC=8;
      TIM2_EGR =1;
      TIM2_CCMR1 = 0;
      TIM2_CCMR1 |= (0x3 << TIM_OC2M_BIT);
      TIM2_ARR = 0x0013;
      TIM2_CCR2 = TIM2_ARR;

      TIM2_CNT=0;
      TIM2_CR1 = 0x00000000u; //disable counting; //enable counting

      ATOMIC(
             INT_TIM2CFG = INT_TIMUIF; //enable the Timer 1, CNT ?= TOP interrupt
             INT_CFGSET |= INT_TIM2; //enable top level timer interrupts
             TIM2_CCER = TIM_CC2E; //enable output on channel 2
             TIM2_CR1 = 0x00000000u; //disable counting; //enable counting
             )

    }
  //#else
    else
    {
      PWM_CLOCK=0x00000040; //PB3
      PWM_PSC = 0;//2^5=32 -> 12MHz/32 = 375kHz = 2.6us tick
      PWM_UPDATE = 1;
      PWM_OUTPUT_CFG = 0x0000006C;//00 00 [0111 1100] 00

      PWM_FREQ = 29980;    //5K =74 PWM_PSC =5  //5K =2398 PWM_PSC =0 //400hz =29980  PWM_PSC =0
      DUTY_CYCLE =29980;
      //  PWM_FREQ = 2398;    //5K =74 PWM_PSC =5  //5K =2398 PWM_PSC =0 //400hz =29980  PWM_PSC =0
      //DUTY_CYCLE =2398;
      PWM_CNT = 0;
      // PWM_ENABLE = 1;

      ATOMIC(
             PWM_INT_MASK = PWM_BIT; //enable the Timer 2, CNT ?= TOP interrupt
             INT_CFGSET |= 0x00000002; //enable top level timer interrupts
             PWM_OUTPUT_ENABLE = 0x00000100;//TIM_CC3E;; //enable output on channel 2
             PWM_ENABLE = 0x00000001; //enable counting
             )
    }
  //#endif
  }
  else
  {
      //According to emulator.h, buzzer is on pin 15 which is mapped
      //to channel 2 of TMR1
    BUZZER_CLK = 0; //use 12MHz clock
    BUZZER_PSC = 0; //2^5=32 -> 12MHz/32 = 375kHz = 2.6us tick
    BUZZER_UPDATE = 1; //trigger update event to load new prescaler value
    BUZZER_OUTPUT_CFG  = 0; //start from a zeroed configuration
    //Output waveform. Toggle on CNT = COMPARE which is configured to be
    //the same as TOP.
    BUZZER_OUTPUT_CFG |= BUZZER_OUTPUT_MODE;

    currentTune = tune;
    //  BUZZER_TOP = 365;//tune[0]*13; //magical conversion to match our tick period
    BUZZER_TOP = 0x2EDF;//tune[0]*13; //magical conversion to match our tick period (interrupt at 0.0009973333 sec)
    BUZZER_COMPARE = BUZZER_TOP; //set both compare and reload to be the same
    BUZZER_CNT = 0; //force the counter back to zero to prevent missing BUZZER_TOP

    // Magic duration calculation based on frequency
    // currentDuration = ((int16u)BUZZER_TEMPO*(int16u)tune[1])/(tune[0]/15);
    // tunePos = 2;  // First note is already set up
    // tuneDone = FALSE;
    /*ATOMIC(
           BUZZER_INT_MASK = BUZZER_BIT; //enable the Timer 1, CNT ?= TOP interrupt
           INT_CFGSET |= INT_TIM1; //enable top level timer interrupts
           BUZZER_OUTPUT_ENABLE = TIM_CC2E; //enable output on channel 2
           BUZZER_ENABLE |= TIM_CEN; //enable counting
           )
      while( (!bkg) && (!tuneDone)) {
        halResetWatchdog();
      }*/
    ATOMIC(
      BUZZER_INT_MASK = BUZZER_BIT; //enable the Timer 1, CNT ?= TOP interrupt
      INT_CFGSET |= INT_TIM1; //enable top level timer interrupts
      BUZZER_ENABLE |= TIM_CEN; //enable counting
      BUZZER_OUTPUT_ENABLE = BUZZER_OUTPUT_ENABLE_CHANNEL;
      //setUpNextNoteOrStop();
    )
    while (!bkg && !tuneDone) {
      halResetWatchdog();
    }
  }
}
unsigned char Photocell_Off_Condition=0;
unsigned int photocell_off_Cnt=0;
unsigned int energy_meter_cnt ;
extern unsigned char half_sec;
extern unsigned int RF_communication_check_Counter;
extern unsigned char cheak_emt_timer_sec ;
extern unsigned char Time_Out;
extern unsigned int lamp_off_on_timer;
extern unsigned char one_sec,Stop_EM_Access,one_sec_Ntw_Search;
extern unsigned int rf_event_send_counter;
extern unsigned char test_query_1_send_timer;
extern unsigned char energy_meter_ok;
extern unsigned char energy_time;
extern unsigned int Lamp_off_on_Time;
extern unsigned int Sec_Counter;
extern unsigned char SLC_New_Manula_Mode_En ;
unsigned char sec_flag=0;
extern unsigned char chk_interlock;
extern unsigned int check_current_counter;
extern unsigned int check_current_firsttime_counter;
extern unsigned char check_cycling_current_counter;
extern unsigned char check_half_current_counter;
extern unsigned char check_counter;
extern unsigned int minute_timer;
extern unsigned char cheak_emt_timer;
extern unsigned char cycling_lamp_fault;
extern unsigned char save_run_hour;
extern  BYTE_VAL  error_condition,error_condition1,test_result,day_sch,test_result2;
extern unsigned char change_Link_key_timer,Photo_steady_cnt ;
extern unsigned char change_Link_key;
unsigned char event_counter=0;
extern unsigned char photo_cell_timer;
extern unsigned char photo_cell_toggel_timer;

unsigned char power_on_count = 0;

extern unsigned int Photocell_int_counter;
extern unsigned char Photo_feedback;
extern unsigned char Start_dim_flag;
extern unsigned int Photocell_int_timer ;
extern unsigned char one_min,do_toggel;
extern unsigned char do_min_timer,run_m_sch;
extern unsigned char do_on_timer;
unsigned char Frame_send_timer=0,Stop_EM_Access_timer =0,Gateway_Rsp_Cnt = 0;
unsigned char Photocell_steady =0;
extern unsigned long Network_Scan_Cnt,Network_Scan_Cnt_Time_out;
extern unsigned int Time_change_dueto_DST_Timer ;
extern unsigned char mfg_test_variable;
extern unsigned int Photocell_Detect_Time;
extern unsigned char check_reset_need_condition,check_reset_need_condition_timer;
extern unsigned short int SLC_Start_netwrok_Search_Timer;
extern unsigned char SLC_Start_netwrok_Search;
extern unsigned char SLC_Start_netwrok_Search_Timeout;
extern unsigned char SLC_intermetent_netwrok_Search_Timeout;
extern unsigned char day_burner_timer;
unsigned char Read_RTC = 0;
unsigned char Commissioning_timer = 0;
extern unsigned char Read_buff_success;
extern unsigned int Read_buff_Time_Out;
extern unsigned char RS485_Read_buff_success;
extern unsigned int RS485_Read_buff_Time_Out;
unsigned int ms_timer = 0;
extern unsigned int RTC_power_on_Logic_Timer;
extern unsigned char check_RTC_logic_cycle;
volatile unsigned int Event_Send_Cnt=0;
#ifdef NEW_LAMP_FAULT_LOGIC
extern unsigned char Lock_cyclic_check;
extern unsigned int Lock_cyclic_Counter;
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

void halTimer2Isr(void)
{
//#if defined(DALI_SUPPORT)
  if((dali_support == 1 )||(dali_support == 2))
  {
 if(INT_TIM2FLAG & 0x00000001)
  {
      if (value)
        halClearLed(TX_DALI);          //DALIT=1;
      else if(value ==0x00)
        halSetLed(TX_DALI);            //DALIT=0;

      if (position ==  0)                                  // 0TE second half of start bit = 1
      {
        value =  1;
      }
      else if (position <  33)                             // 1TE - 32TE, so address + command
      {

        value = (forward >> (( 32 - position)/2)) & 1;
        if (position &  1)
          value = !value;                            // invert if first half of data bit
      }
      else if (position ==  33)                            // 33TE start of stop bit (4TE)
      {                                                    // and start of min settling time (7TE)
        value =  1;
      }
      else if (position ==  44)                            // 44TE, end stop bits + settling time
      {                                                    // receive slave answer, timeout
        //PR2 = 0x019A;								 // this is for with PLL enable =  9174;
        //init_capture();   //look
        //T0CCR =  0x0007;                              // enable rx, capture on both edges
        //TIM2_CCMR1 = 0X00003100;

    TIM2_CCMR1= 0x00003100;
    TIM2_CCMR2= 0x00003100;
    TIM2_CCER  |= 0x00003010;
    TIM2_EGR   |= 0x00000014;
    INT_TIM2CFG = 0x00000015;       //enable the Timer 1, CNT ?= TOP interrupt

    TIM2_ARR = 0x01ad;
    //TIM2_CNT =0;
    //TIM2_CCR2 = TIM2_ARR;

      }
      else if (position ==  45)                            // end of transfer
      {

        //T2CONbits.TON = 0;


        TIM2_CR1 = 0x00000000u; //disable counting
         answer2 = (unsigned char)frame;
        //T0TCR =  2;                                   // stop and reset timer
        if (frame  & 0x100)                             // backward frame (answer) completed?
        {
          answer = (unsigned char)frame;                // OK ! save answer
          answer2 = (unsigned char)frame;                // OK ! save answer
          f_dalirx =  1;                                // and set flag to signal application
        }
        frame    =  0;                                  // reset receive frame
        f_busy =  0;                                    // end of transmission
        //if (f_repeat)
        //	f_dalitx =  1;                                // yes, set flag to signal application

      }
      PWM_INT = 0xFFFFFFFF;
      position++;
      answer = position ;
  }
  else if((INT_TIM2FLAG & 0x00000004)||(INT_TIM2FLAG & 0x00000010))   // INT_TIMUIF; //enable the Timer 1, CNT ?= TOP interrupt
    {
      if(INT_TIM2FLAG & 0x00000004)
        {

            if (frame != 0) // not first pulse ?
              {
             // Sandip1[cnt1]=(unsigned long)TIM2_CCR2;
              low_time = (unsigned long) TIM2_CCR2; // rising, so capture low time
              TIM2_CNT =0;
              //TIM2_CCR2 =0;
              cnt1++;
              DALI_Decode(); // decode received bit
              }
            else
              {
                TIM2_CNT =0;
              cnt2++;
              previous = 1; // first pulse, so shift 1
              DALI_Shift_Bit(1);
              }
            INT_TIM2FLAG |=0x00000004;
         }
       else if(INT_TIM2FLAG & 0x00000010)
         {

            //Sandip2[cnt3]=(unsigned long)TIM2_CCR4; // SAN
            high_time=(unsigned long)TIM2_CCR4;
            TIM2_CNT =0;
            //TIM2_CCR4 =0;
            cnt3++;
            INT_TIM2FLAG |=0x00000010;
          //TIM2_CCR4 =0;
         }
          count11 ++;
          answer = count11;
          answer1= frame;

    }
  }
//#endif

  INT_TIM2MISS |=0X00000014;
  PWM_INT |= 0xFFFFFFFF;
}

void halTimer1Isr(void)
{

  if(Read_buff_success == 1)
  {
    Read_buff_Time_Out++;
  }
  if(RS485_Read_buff_success==1)  // maulin
  {
    RS485_Read_buff_Time_Out++;
  }
    
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_6_0216)||defined(ISLC_3100_480V_Rev3))
  if(TILT_DI1 == 1) //reverse logic
  {
    Tilt_Cnt =0;
    //Tilt_counter=0;
  }
#endif
energy_meter_cnt++;
  Tilt_Cnt++;
  ms_timer++;
  //temp_ms_timer++;
  if(automode == PANIC_MODE)
  {
        Panic_Lamp_On_Cnt++ ;
        if(Panic_Lamp_On_Cnt<= Lamp_On_Time)
        {
            Lamp_ON_Flag =1;
            Lamp_OFF_Flag =0;
        }
        else if(Panic_Lamp_On_Cnt<= (Lamp_On_Time*2))
        {
          Lamp_ON_Flag =0;
          Lamp_OFF_Flag =1;

        }
        if(Panic_Lamp_On_Cnt>= (Lamp_On_Time*2))
        {
          Lamp_ON_Flag =0;
          Lamp_OFF_Flag =0;
          Panic_Lamp_On_Cnt =0;
        }
         // Panic_Lamp_OFF_Cnt++;

  }
 //////////////LasTGasP///////////////
	if(Last_Gsp_Enable == 1)
	{
		LastGasp_Timer++; 
		if(LastGasp_Timer<=30) //no need to reset this timer since hardware must be restart befor overflow the variable.
		{
                  Last_Gasp_Detect =0;
//		//	CN_counter =0;
//			if(Last_Gasp_Detect == 1)
//			{
//				Last_Gasp_Detect =0;
//				if(Lamp_status_On_LastGasp == 1)
//				{
//					Lamp_status_On_LastGasp =0;
//					LAmp_previous_status_On_Aftr_LastGasp = 1;				
//				//	Set_Do(1);
//				}	
//			}	
		}
		else
		{
			
                  if(Last_Gasp_Detect==1)
                  {
                      if((LastGasp_Timer>=700) &&(LastGasp_unicast_status==2))
                      {
                          Send_LastGasp=1;
                      //LastGasp_unicast_status=1;
                      }
                  }
                  else
                  {
                      Last_Gasp_Detect =1;
                      halResetWatchdog();
                      // calculate_kwh_LampOFF();             // store last reading of KWH in to NV
                      //Calculate_burnhour_Lampoff();        // store last reading of Burn_Hour in to NV   
                      Send_LastGasp=1;
                      LastGasp_unicast_status=1;
                  }
		}
	}	
//////////////////////////////////// 
  
  
  if(ms_timer >= 500)
  {
    ms_timer = 0;

    half_sec++;					// increment Half_sec counter
    Motion_half_sec = 1;
    Read_RTC = 1;

    if(half_sec >= 2)
    {
      if((dali_support == 1 )||(dali_support == 2))
      {
//#if defined(DALI_SUPPORT)
      dali_delay++;							//SAN
//#endif
      }
      CheckAoDimmerForDetectionTimmer++;
     if(automode == PANIC_MODE)
     {
        Panic_Cnt++;
     }
	 dali_dim_cnt++;
     Event_Send_Cnt++;
      Increment_Local_Timer();
      half_sec = 0;
      RF_communication_check_Counter++;
      one_sec = 1;
      run_m_sch =1;
      Tilt_Timeout_Cnt++;
      one_sec_Ntw_Search++;
      Dimming_start_timer++;
      if(Stop_EM_Access)
      {
        Stop_EM_Access_timer++;
      }
      rf_event_send_counter++;
      lamp_off_on_timer++;
      Time_Out++;
      if(Time_Out >Time_Out_Check)
      {
         Time_Out_Flag =0;
      }
      test_query_1_send_timer++;
      cheak_emt_timer_sec++;
      /////////////added on 27 june
      Get_Sch_timer++;				// V6.1.10
      Get_Master_Sch_timer++;			// V6.1.10
      dim_inc_timer++;
      Motion_detect_sec++;
      motion_broadcast_timer++;
      day_burner_timer++;
      //////////////////////
      ///////// idimmer /////////		
      //Send_dimming_cmd++;
      Read_Data_send_cnt ++;
      read_dim_val_timer++;
      ///////// idimmer /////////

      send_event_delay_timer++;             // uncomment for auto event with 10 sec delay.
      if((send_event_with_delay == 1) && (send_event_delay_timer > 5))   // delay for send data direct delay
      {
        send_event_delay_timer = 0;	
        send_data_direct = 1;
        send_event_with_delay = 0;
      }

      /////////////
      //	if(energy_meter_ok == 0)
      //	{
      energy_time++;
      //	}
      if(lamp_off_on_timer >= Lamp_off_on_Time)
      {
        lamp_off_on_timer = Lamp_off_on_Time;
      }
      ///////////////
      if(Dimming_start_timer >= dim_applay_time)
      {
        Start_dim_flag = 1;
      }
      ////////////////
      Sec_Counter++;
      sec_flag = 1;
      if(chk_interlock == 1)														 // if lamp on increment timer related current interlock
      {
        check_current_counter++;
        check_current_firsttime_counter++;
        SSN_CNT++;
        if(SSN_CNT>3600)
        {
                SSN_CNT =0;
                Lamp_Balast_fault_Remove_Cnt++;
        }        
#ifdef NEW_LAMP_FAULT_LOGIC
        //check_cycling_current_counter++;
        //check_half_current_counter++;
#elif defined(OLD_LAMP_FAULT_LOGIC)
        check_cycling_current_counter++;
        check_half_current_counter++;
#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif
      }
      Relay_weld_timer++;
      check_counter++;
      //////////////////////////////////////////////
      if(Gateway_Rsp_Cnt >= 1) //After single response to gateway do not give response atleast 30 second
      {
        Gateway_Rsp_Cnt++;
        if(Gateway_Rsp_Cnt>=120)
        {
          Gateway_Rsp_Cnt =0;
        }
      }
      ///////////////
      if(change_Link_key == 1)
      {
        change_Link_key_timer++;
      }
      /////////////////////////new menual mix mode///////////////////////
      if((SLC_New_Manual_Mode_Timer > 0) && (SLC_New_Manula_Mode_En == 1))
      {
        SLC_New_Manual_Mode_counter++;
      }
      Network_Scan_Cnt++;
      ////////////////////////////////////////////////
      if(SLC_Start_netwrok_Search == 0)
      {
        SLC_Start_netwrok_Search_Timer++;
        if(SLC_Start_netwrok_Search_Timer >= (SLC_intermetent_netwrok_Search_Timeout * 60))
        {
          SLC_Start_netwrok_Search_Timer = 0;
          SLC_Start_netwrok_Search = 1;
        }
      }

      Commissioning_timer++;

#ifdef NEW_LAMP_FAULT_LOGIC
      one_sec_cyclic = 1;
      if(Lock_cyclic_check == 1)
      {
        Lock_cyclic_Counter++;
        //if(Lock_cyclic_Counter >= 1200)
        //if(Lock_cyclic_Counter >= 120)
        if(Lock_cyclic_Counter >= 1200)
        {
          Lock_cyclic_check = 0;
        }
      }
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

      RTC_power_on_Logic_Timer++;
      if(RTC_power_on_Logic_Timer >= 60)     // for testing
      {
        RTC_power_on_Logic_Timer = 0;
        check_RTC_logic_cycle = 1;
      }

      if(Sec_Counter > 59)
      {
        Sec_Counter = 0;
        //              Send_Gps_Statistic = 1;
        minute_timer++;															// increment minute counter
        cheak_emt_timer++;
        ////////////////
        Frame_send_timer=1;
        do_min_timer++;
        if(do_min_timer >= do_on_timer)
        {
          do_min_timer = 0;
          one_min = 1;
        }
        /////////////////
#ifdef NEW_LAMP_FAULT_LOGIC
        //     if(minute_timer == 30)														
        //     {
        //       cycling_lamp_fault = 0;												// reset cyclic lamp fault trip at every 30 minute.
        //     }
#elif defined(OLD_LAMP_FAULT_LOGIC)
        if(minute_timer == 30)														
        {
          cycling_lamp_fault = 0;												// reset cyclic lamp fault trip at every 30 minute.
        }
#else
#error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif



        if(error_condition.bits.b0 == 1)										// check lamp on DI for increment run hour.
        {
          lamp_burn_hour++;
          if((lamp_burn_hour%180) == 0)
          {
            save_run_hour = 1;													// if Lamp on set Lamp burn hour inc flag
          }
        }

        if(minute_timer > 59)
        {
          Network_Leave_Hr_Cnt++;
          minute_timer = 0;
          event_counter = 0;													// reset event counter at every hour
          photo_cell_timer++;					
          photo_cell_toggel_timer++;
          power_on_count++;													// increment power on counter at every hour
        }
        if((Time_change_dueto_DST == 2) )
        {
          Time_change_dueto_DST_Timer++;
          if(Time_change_dueto_DST_Timer > (SLC_DST_Time_Zone_Diff+5))
          {
            Time_change_dueto_DST = 0;	
            Time_change_dueto_DST_Timer = 0;
          }
        }
        if(check_reset_need_condition == 1)
        {
          check_reset_need_condition_timer++;
        }
        mfg_test_variable++;
      }


/*
      Photocell_int_timer++;
      //            if((Photocell_int_timer%5) == 0)  // use for GPS testing lat/long value send at every 5 sec.
      //            {
      //                send_get_mode_replay = 1;
      //            }

      if(Photocell_int_timer >= Photocell_Detect_Time)
      {
        Photocell_int_timer = 0;
        Photo_steady_cnt =0;
        #ifndef ISLC3300_T8_V2
        if(Photo_feedback_pin == 1)
        {
          Photo_feedback = 0;
          //emberAfGuaranteedPrint("\r\n%p", "Night time");
        }
        else
        {
          Photo_feedback = 1;
          //emberAfGuaranteedPrint("\r\n%p", "Day time");
        }
        #endif
      }
*/
////////////////////////////////////////////////////////
    #ifndef ISLC3300_T8_V2
      Photocell_int_timer++;
        //if(Photocell_int_timer >= Photocell_unsteady_timeout_Val)   // read photocell at every 15 sec.
        if(Photocell_int_timer >= 1)   // read photocell at every 15 sec.
        {


            Photocell_int_timer = 0;
            Photo_steady_cnt =0;

            if(Photo_feedback_pin == 1)
            {
                Photo_feedback = 0;
                Photocell_Off_Condition =0;
                photocell_off_Cnt =0;
            }
        }
        if(Photo_feedback_pin == 1) //off time
        {
            photocell_off_Cnt =0;
            Photocell_Off_Condition =0;
        }
        else
        {
            Photocell_Off_Condition =1;
        }
        if(Photocell_Off_Condition == 1)
        {
            photocell_off_Cnt++;

            //if(photocell_off_Cnt > Photocell_Off_Time)
            if(photocell_off_Cnt > Photocell_Detect_Time)
            {
                photocell_off_Cnt =0;
                Photo_feedback = 1;
                Photocell_Off_Condition =0;
                Photocell_int_timer = 0;

            }
        }
    #endif
////////////////////////////////////////////////////////
      //		if(Motion_detect_sec > 60)
      //		{
      motion_dimming_timer++;
      //emberAfGuaranteedPrint("%p", "\r\nmotion_dimming_timer =");
      //emberAfGuaranteedPrint("%u",motion_dimming_timer );
      motion_intersection_dimming_timer++;
      //			   Motion_detect_sec = 0;
      //		}

      if((Motion_Continiue == 0) && (Motion_detected == 1))
      {
        Motion_continue_timer++;
      }
      else
      {
        Motion_continue_timer = 0;
      }

      if(Motion_Received_broadcast == 1)
      {
        Motion_Received_broadcast_counter++;
        if(Motion_Received_broadcast_counter > Motion_Broadcast_Timeout)
        {
          Motion_Received_broadcast_counter = 0;	
          Motion_Received_broadcast = 0;
        }	
      }

      if(Motion_countr >= Motion_pulse_rate)
      {
        if(Motion_detected == 0)
        {
          Motion_detected = 1;
          Motion_intersection_detected = 0;
          if(Temp_Slc_Multi_group == 1)
          {
            Motion_Received_broadcast = 0;
            motion_dimming_Broadcast_send = 0;
          }
          if(Motion_Received_broadcast == 0)
          {
            motion_dimming_timer = 0;
            Motion_detect_sec = 0;
          }
        }			
      }
      Motion_countr = 0;
    }
  }
BUZZER_INT = 0xFFFFFFFF;

}

int8u PGM hereIamTune[] = {
  NOTE_B4,  1,
  0,        1,
  NOTE_B4,  1,
  0,        1,
  NOTE_B4,  1,
  0,        1,
  NOTE_B5,  5,
  0,        0
};

void halStackIndicatePresence(void)
{
  halPlayTune_P(hereIamTune,TRUE);
}

//#if defined(DALI_SUPPORT)
void DALI_ON_CMD(unsigned char cmd)
{
  if(cmd==1)
  {
    forward    = (0x07 << 8) | 0x05;
    f_dalitx =  1;
  }
  else if(cmd ==0)
  {
    forward    = (0x07 << 8) | 0x00;
    f_dalitx =  1;
  }

}


void DALI_Send(void)
{
  unsigned char m;
  if (f_repeat)                                            // repeat last command ?
       {
           //f_repeat =  0;
       }
       else if ((forward  & 0xE100) == 0xA100  || (forward  & 0xE100) == 0xC100)
       {
            //if ((forward &  0xFF00) == INITIALISE  || forward == RANDOMISE)
                 //f_repeat =  1;                                 // special command repeat < 100 ms
       }
       else if ((forward  & 0x1FF) >= 0x120 && (forward  & 0x1FF) <= 0x180)
       {
            //f_repeat =  1;                                      // config. command repeat < 100 ms
       }
       while (f_busy) ;                                         // Wait until dali port is idle

       frame      =  0;
       value      =  0;                                         // first half of start bit = 0
       position =  0;

        TIM2_CCMR1 =0x00000000;
        TIM2_CCMR2 =0x00000000;
        TIM2_CCER  =0x00000000;
        TIM2_EGR   |=0x00000000;

        recieved =0x00;
        TIM2_ARR =0x0013;
        TIM2_CR1 |=0x00000001u; //disable counting; //enable counting

       //TIM2_CCR2 = TIM2_ARR;
        cnt1=0; cnt2=0; cnt3=0; answer=0; answer1=0;count11=0,cnt4=0,s=0;
/*
        for(m=0;m<10;m++)
        {
          Sandip1[m]=0;
         Sandip2[m]=0;
        }
*/


       //InitTimer1	();
       //IC3CON =  0x0000;               // Turn off Input Capture 3 Module
       //IEC2bits.IC3IE  =  0;                                    // Enable IC1 interrupts


}

void MAKE_DEFAULT(void)                             // send shortaddress cmd && Provide FAD TIME using uni and Broadcat
{
  unsigned char j;
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
  forward    = (0xA3 << 8) | 0x07;
  f_dalitx =  1;
  if (f_dalitx)
  {
    f_dalitx =  0;                                // clear DALI send flag
    f_dalirx =  0;                                // clear DALI receive (answer) flag
    DALI_Send();                                  // DALI send data to slave(s)
  }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
  f_dalitx =  1;
  forward    = (0xFF << 8) | 0x80;
  if (f_dalitx)
  {
    f_dalitx =  0;                                // clear DALI send flag
    f_dalirx =  0;                                // clear DALI receive (answer) flag
    DALI_Send();                                  // DALI send data to slave(s)
  }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
  f_dalitx =  1;
  forward    = (0xFF << 8) | 0x80;
  if (f_dalitx)
  {
    f_dalitx =  0;                                // clear DALI send flag
    f_dalirx =  0;                                // clear DALI receive (answer) flag
    DALI_Send();                                  // DALI send data to slave(s)
  }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
}


void DALI_DIMMING_CMD(unsigned char dim)
{

  //forward    = (0x06 << 8) | dim;
  forward    = (0xFE << 8) | dim;
  f_dalitx =  1;
  if (f_dalitx ==1)
  {
    f_dalitx =  0;                                // clear DALI send flag
    f_dalirx =  0;                                // clear DALI receive (answer) flag
    DALI_Send();                                  // DALI send data to slave(s)
  }
//  emberAfGuaranteedPrint("%p","\r\n DALI DIMMING CMD SEND");
//  emberSerialWaitSend(APP_SERIAL);
}

void UNICAST_SHORTADDRESS(void)       // send shortaddress cmd && Provide FAD TIME using uni and Broadcat
{
//  unsigned char i ;
//  forward    = (0xA3 << 8) | 0xFF;
//  f_dalitx =  1;
//  if (f_dalitx)
//  {
//    f_dalitx =  0;                                // clear DALI send flag
//    f_dalirx =  0;                                // clear DALI receive (answer) flag
//    DALI_Send();                                  // DALI send data to slave(s)
//  }
//
//  delay(60060);     // 55 msec
//  delay(60060);     // 55 msec
//
//  f_dalitx =  1;
//  forward    = (0xFF << 8) | 0x80;
//  if (f_dalitx)
//  {
//    f_dalitx =  0;                                // clear DALI send flag
//    f_dalirx =  0;                                // clear DALI receive (answer) flag
//    DALI_Send();                                  // DALI send data to slave(s)
//  }
//  delay(60060);     // 55 msec
//  delay(60060);     // 55 msec
//  f_dalitx =  1;
//  forward    = (0xFF << 8) | 0x80;
//  if (f_dalitx)
//  {
//    f_dalitx =  0;                                // clear DALI send flag
//    f_dalirx =  0;                                // clear DALI receive (answer) flag
//    DALI_Send();                                  // DALI send data to slave(s)
//  }
//  for (i=0;i<5;i++)
//  {
//    delay(60060);     // 55 msec
//    delay(60060);     // 55 msec
//  }
}
/* 0 for Logarithmic 1 for Linear dimming type*/
void set_Linear_Logarithmic_dimming(unsigned char Curv_type)
{
  unsigned char j;
   	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	///////////DTR//////////////////////////////
//	forward    = (0xA3 << 8) |  0x00;
	forward    = (0xA3 << 8) |  Curv_type;
	f_dalitx =  1;
	 if (f_dalitx)
	 {
	      f_dalitx =  0;                                // clear DALI send flag
	      f_dalirx =  0;                                // clear DALI receive (answer) flag
	      DALI_Send();                                  // DALI send data to slave(s)
	 }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        
	forward    = (0xC1 << 8) |  0x06;
        //forward    = (0xC1 << 8) |  0xFE;
	f_dalitx =  1;
	 if (f_dalitx)
	 {
	      f_dalitx =  0;                                // clear DALI send flag
	      f_dalirx =  0;                                // clear DALI receive (answer) flag
	      DALI_Send();                                  // DALI send data to slave(s)
	 }
	
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	
	////////////////
		forward    = (0xFF << 8) |  0xE3;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	/////////////////
	forward    = (0xFF << 8) |  0xE3;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
		delay(60060);     // 55 msec
		delay(60060);     // 55 msec
}
///////////////////////
static void DALI_Shift_Bit(BYTE val)
{
  if (frame & 0x100)          // frame full ?
  frame = 0;                  // yes, ERROR
  else
  frame = (frame << 1) | val; // shift bit
}


static void DALI_Decode(void)
{
    BYTE action;

    action = previous << 2;
    if ((high_time > MIN_2TE) && (high_time < MAX_2TE))
    action = action | 1; // high_time = long
    else if (!((high_time > MIN_TE) && (high_time < MAX_TE)))
    {
    frame= 0; // DALI ERROR
    return;
    }

    if ((low_time > MIN_2TE) && (low_time < MAX_2TE))

    action = action | 2; // low_time = long
    else if (!((low_time > MIN_TE) && (low_time < MAX_TE)))
    {
    frame = 0; // DALI ERROR
    return;
    }

    switch (action)
    {
    case 0:
      DALI_Shift_Bit(0); // short low, short high, shift 0
    break;

    case 1:
      frame= 0; // short low, long high, ERROR
    break;

    case 2:
      DALI_Shift_Bit(0); // long low, short high, shift 0,1
      DALI_Shift_Bit(1);
      previous = 1; // new half bit is 1
     break;

    case 3:
      frame= 0; // long low, long high, ERROR
     break;

    case 4:
      DALI_Shift_Bit(1); // short low, short high, shift 1
    break;

    case 5:
      DALI_Shift_Bit(0); // short low, long high, shift 0
      previous = 0; // new half bit is 0

    break;

    case 6:
      frame = 0; // long low, short high, ERROR
    break;

    case 7:
      DALI_Shift_Bit(0); // long low, long high, shift 0,1
      DALI_Shift_Bit(1);
     default: break; // invalid

    }

}



//void scenes_write(unsigned char scenes_num, unsigned char scenes_value)
//{
	
//	forward    = (0xA3 << 8) | scenes_value;
//	f_dalitx =  1;
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
//	
//	delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//        delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//
//
//	f_dalitx =  1;
//	forward    = (0x07 << 8) | (0x40+scenes_num);
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
//	
//	delay(60060);     // 55 msec
//        delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//        delay(60060);     // 55 msec
//
//	f_dalitx =  1;
//	forward    = (0x07 << 8) | (0x40+scenes_num);
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
// 	delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//        delay(60060);     // 55 msec
//	delay(60060);     // 55 msec

//}
/*
void scenes_read(unsigned char scenes_num)
{

forward    = (0x07 << 8) | 0xb0 + scenes_num;
f_dalitx =  1;
 if (f_dalitx)
 {
      f_dalitx =  0;                                // clear DALI send flag
      f_dalirx =  0;                                // clear DALI receive (answer) flag
      DALI_Send();                                  // DALI send data to slave(s)
 }

delay(60060);     // 55 msec
delay(60060);     // 55 msec

}

void Read_Dali_Ballast_Information(void)
{
unsigned char i;

	for(i=0;i<16;i++)
	{
	scenes_read(i);

        delay(60060);     // 55 msec
        delay(60060);     // 55 msec
        delay(60060);     // 55 msec
        delay(60060);     // 55 msec
        delay(60060);     // 55 msec
        delay(60060);     // 55 msec

	DALI_Ballast_Info[i]= answer1;
	}

}
*/


//void scenes_write2(unsigned char bankno,unsigned char bankoffset,unsigned char *scenes_value, unsigned char len)
//{
//	
//unsigned char i;
//	
////////////////DTR1//////////////////////
//forward    = (0xC3 << 8) |  bankno;
//f_dalitx =  1;
// if (f_dalitx)
// {
//      f_dalitx =  0;                                // clear DALI send flag
//      f_dalirx =  0;                                // clear DALI receive (answer) flag
//      DALI_Send();                                  // DALI send data to slave(s)
// }
//
//delay(60060);     // 55 msec
//delay(60060);     // 55 msec
//	
//	forward    = (0xA3 << 8) | 2;
//
//	f_dalitx =  1;
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
//	
//delay(60060);     // 55 msec
//
//
//	forward    = (0xFF << 8) |  0x81;
//	f_dalitx =  1;
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
//	
//	
//	delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//
//forward    = (0xFF << 8) |  0x81;
//	f_dalitx =  1;
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
//	
//	delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//
//forward    = (0xC7 << 8) |  0x55;
//f_dalitx =  1;
// if (f_dalitx)
// {
//      f_dalitx =  0;                                // clear DALI send flag
//      f_dalirx =  0;                                // clear DALI receive (answer) flag
//      DALI_Send();                                  // DALI send data to slave(s)
// }
//
//delay(60060);     // 55 msec
//delay(60060);     // 55 msec
///////////////////DTR 7//////////////
//forward    = (0xA3 << 8) |  bankoffset;
//f_dalitx =  1;
// if (f_dalitx)
// {
//      f_dalitx =  0;                                // clear DALI send flag
//      f_dalirx =  0;                                // clear DALI receive (answer) flag
//      DALI_Send();                                  // DALI send data to slave(s)
// }
//
//delay(60060);     // 55 msec
//delay(60060);     // 55 msec
///////////////////75//////////////
//
//for(i=0;i<len;i++)
//	{
//	forward    = (0xC7 << 8) |   scenes_value [i];
//	f_dalitx =  1;
//	 if (f_dalitx)
//	 {
//	      f_dalitx =  0;                                // clear DALI send flag
//	      f_dalirx =  0;                                // clear DALI receive (answer) flag
//	      DALI_Send();                                  // DALI send data to slave(s)
//	 }
//	
//	delay(60060);     // 55 msec
//	delay(60060);     // 55 msec
//	}
//
//}





 void intialize_randomize(void)
 {

 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
 	forward    = (0xA3 << 8) |  0xff;
	f_dalitx =  1;
	 if (f_dalitx)
	 {
	      f_dalitx =  0;                                // clear DALI send flag
	      f_dalirx =  0;                                // clear DALI receive (answer) flag
	      DALI_Send();                                  // DALI send data to slave(s)
	 }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	////////////////
		forward    = (0xFF << 8) |  0x80;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	/////////////////
	forward    = (0xFF << 8) |  0x80;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
		
			/////////////////
	forward    = (0xA5 << 8) |  0x00;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
		
			/////////////////
 	forward    = (0xA5 << 8) |  0x00;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
		
		forward    = (0xA7 << 8) |  0x00;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
		
			/////////////////
		forward    = (0xA7 << 8) |  0x00;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
		
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
		
}

unsigned char search_compare(unsigned char low)
 {

 	//unsigned int highbyte,loop_one=0x01;
          unsigned char highbyte,loop_one=0x01;

	 for(highbyte=255;highbyte>0;highbyte--)
	 {
                if(cheak_emt_timer_sec >120)
                  return 0;
	 	if(loop_one ==0x01)
	 	highbyte = highbyte - 20;
	 	
	 	
	 	forward    = (low << 8) | highbyte ;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
		
		forward    = (0xa9 << 8) | 0X00 ;
		f_dalitx =  1;
		 if (f_dalitx)
		 {
		      f_dalitx =  0;                                // clear DALI send flag
		      f_dalirx =  0;                                // clear DALI receive (answer) flag
		      DALI_Send();                                  // DALI send data to slave(s)
		 }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec


		delay(60060);     // 55 msec
		delay(60060);     // 55 msec
		delay(60060);     // 55 msec
		delay(60060);     // 55 msec
		compare=answer1;

		
		if(compare == 0x00)
		{	
			
			forward    = (low << 8) | (highbyte +1) ;
			f_dalitx =  1;
			 if (f_dalitx)
			 {
			      f_dalitx =  0;                                // clear DALI send flag
			      f_dalirx =  0;                                // clear DALI receive (answer) flag
			      DALI_Send();                                  // DALI send data to slave(s)
			 }

 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec			
			forward    = (0xa9 << 8) | 0X00 ;
			f_dalitx =  1;
			 if (f_dalitx)
			 {
			      f_dalitx =  0;                                // clear DALI send flag
			      f_dalirx =  0;                                // clear DALI receive (answer) flag
			      DALI_Send();                                  // DALI send data to slave(s)
			 }
	

                          	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			compare1=answer1;
				
				if(compare1 ==0xff)
				{
				compare1=answer1;
				return (highbyte);
				}
				else if(compare1 ==0x00)
				{
				highbyte = highbyte+11;
				loop_one =0;
				}
		}
		
		
	 }


 }



void assigning_address(void)
{


	SEARCH_DALI_HIGH = 0xff;
	SEARCH_DALI_MID = 0xff;
	SEARCH_DALI_LOW = 0xff;
	
	forward    = (0xb1 << 8) | 0xff ;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
					
	forward    = (0xb3 << 8) | 0xff ;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec	
	
	forward    = (0xb5 << 8) | 0xff ;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec

	SEARCH_DALI_HIGH = search_compare(0xb1);
	
	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        //delay(60060);     // 55 msec
 	//delay(60060);     // 55 msec
	
	SEARCH_DALI_MID = search_compare(0xb3);
	
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        //delay(60060);     // 55 msec
 	//delay(60060);     // 55 msec
	
	SEARCH_DALI_LOW = search_compare(0xb5);
	
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec	
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec	
	actual_address = actual_address + 2;
	forward    = (0xb7 << 8) | actual_address ;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec	
	forward    = (0xbb << 8) | 0x00;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
	delay(60060);     // 55 msec
	delay(60060);     // 55 msec
	delay(60060);     // 55 msec
	delay(60060);     // 55 msec
	address1 = answer1 ;
	address2 = address1-1;
	forward    = (address2 << 8) | 0x00;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec	
	forward    = (0xab << 8) | 0x00;
	f_dalitx =  1;
	if (f_dalitx)
	{
	     f_dalitx =  0;                                // clear DALI send flag
	     f_dalirx =  0;                                // clear DALI receive (answer) flag
	     DALI_Send();                                  // DALI send data to slave(s)
	}
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec

}

void search_address(void)
{
	unsigned char i ,j =0;
	
	 for(i=0;i<130;i++)
	 {
		if((i%2)>0x00)
		{
		 	forward    = (i << 8) | 0x91;
		 	
			f_dalitx =  1;
			 if (f_dalitx)
			 {
			      f_dalitx =  0;                                // clear DALI send flag
			      f_dalirx =  0;                                // clear DALI receive (answer) flag
			      DALI_Send();                                  // DALI send data to slave(s)
			 }


 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			delay(60060);     // 55 msec
			compare=answer1;
			if(compare == 0xff)
			{	
				DALI_ADD[j]= i;
				j++;
			
				forward    = (i << 8) | 0x00;
				f_dalitx =  1;
				 if (f_dalitx)
				 {
				      f_dalitx =  0;                                // clear DALI send flag
				      f_dalirx =  0;                                // clear DALI receive (answer) flag
				      DALI_Send();                                  // DALI send data to slave(s)
				 }
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
				
				forward    = (0xab << 8) | 0x00;
				f_dalitx =  1;
				if (f_dalitx)
				{
				 f_dalitx =  0;                                // clear DALI send flag
				 f_dalirx =  0;                                // clear DALI receive (answer) flag
				 DALI_Send();                                  // DALI send data to slave(s)
				}
 	delay(60060);     // 55 msec
 	delay(60060);     // 55 msec
        delay(60060);     // 55 msec
 	delay(60060);     // 55 msec

			
			}
		
		}
		
	}

}


void scenes_write3(unsigned char scenes_num, unsigned char scenes_value,unsigned char dali_add)
{
	
	forward    = (0xA3 << 8) | scenes_value;
	f_dalitx =  1;
	 if (f_dalitx)
	 {
	      f_dalitx =  0;                                // clear DALI send flag
	      f_dalirx =  0;                                // clear DALI receive (answer) flag
	      DALI_Send();                                  // DALI send data to slave(s)
	 }
	
	delay(60060);     // 55 msec
	delay(60060);     // 55 msec


	f_dalitx =  1;
	forward    = (dali_add << 8) | (0x40+scenes_num);
	 if (f_dalitx)
	 {
	      f_dalitx =  0;                                // clear DALI send flag
	      f_dalirx =  0;                                // clear DALI receive (answer) flag
	      DALI_Send();                                  // DALI send data to slave(s)
	 }
	
	delay(60060);     // 55 msec
	f_dalitx =  1;
	forward    = (dali_add << 8) | (0x40+scenes_num);
	 if (f_dalitx)
	 {
	      f_dalitx =  0;                                // clear DALI send flag
	      f_dalirx =  0;                                // clear DALI receive (answer) flag
	      DALI_Send();                                  // DALI send data to slave(s)
	 }
 	delay(60060);     // 55 msec
	delay(60060);     // 55 msec
}

void scenes_read_3(unsigned char scenes_num,unsigned char dali_add)
{

    forward    = (dali_add << 8) | 0xb0 + scenes_num;
    f_dalitx =  1;
     if (f_dalitx)
     {
          f_dalitx =  0;                                // clear DALI send flag
          f_dalirx =  0;                                // clear DALI receive (answer) flag
          DALI_Send();                                  // DALI send data to slave(s)
     }

    delay(60060);     // 55 msec
    delay(60060);     // 55 msec
    delay(60060);     // 55 msec
    delay(60060);     // 55 msec
}

void Read_Dali_Ballast_Information(unsigned char driverno,unsigned char dali_add)
{
        unsigned char i;

	for(i=0;i<16;i++)
	{
	scenes_read_3(i,dali_add);
	DALI_Ballast_Info[driverno][i]= answer1;
	}

}

/////////////////////////
//#endif

