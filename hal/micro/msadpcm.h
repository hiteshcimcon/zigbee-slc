// Copyright 2014 Silicon Laboratories, Inc.
//-----------------------------------------------------------------------------
#ifndef __MSADPCM_H__
#define __MSADPCM_H__

//-----------------------------------------------------------------------------
// #defines
//-----------------------------------------------------------------------------
typedef struct HalMsadpcmState {
  int8u predictor;            // Index into msadpcmAdaptCoeffx tables
  int16u delta;               // Quantization scale
  int16s sample1;             // Second sample of block (full PCM format)
  int16s sample2;             // First sample of block (full PCM format)

                              // Temp variables
  int32s coeff1;              // Promoted msadpcmAdaptCoeff1[State->predictor]
  int32s coeff2;              // Promoted msadpcmAdaptCoeff2[State->predictor]
} HalMsadpcmState;

// PACKED SIZE
#define MSADCPM_STATE_SIZE (sizeof(int8u)+sizeof(int16u)+2*sizeof(int16s))
#define MSADCPM_SAMPLES_IN_HEADER 2

//-----------------------------------------------------------------------------
// Global API Function Prototypes
//-----------------------------------------------------------------------------
// Decode/Encode a single sample and update state
int16s halInternalMsadpcmDecode(int8u compressedCode,
                                HalMsadpcmState *state);
int8u halInternalMsadpcmEncode(int16s realSample,
                               HalMsadpcmState *state);
void halInternalMsadpcmResetState(HalMsadpcmState *state,
                                  int8u predictor,
                                  int8u resetDelta,
                                  const int16s *samples,
                                  int8u numsamples);
void halInternalMsadpcmSaveState(int8u *stateDst,
                                 const HalMsadpcmState *stateSrc);
void halInternalMsadpcmRestoreState(HalMsadpcmState *stateDst,
                                    const int8u *stateSrc);
#endif // __MSADPCM_H__
