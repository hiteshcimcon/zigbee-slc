// Copyright 2014 Silicon Laboratories, Inc.
#ifndef __I2C_DRIVER_H__
#define __I2C_DRIVER_H__

// Return codes for read/write transaction error handling
#define I2C_DRIVER_ERR_NONE         0x00
#define I2C_DRIVER_ERR_TIMEOUT      0x01
#define I2C_DRIVER_ERR_ADDR_NAK     0x02
#define I2C_DRIVER_ERR_DATA_NAK     0x03

/** @brief Initializes the I2C plugin. The application framework will generally
 * initialize this plugin automatically. Customers who do not use the framework
 * must ensure the plugin is initialized by calling this function.
 */
void halI2cInitialize(void);

/** @brief Write bytes
 *
 * This function writes a set of bytes to an I2C slave, returning immediately
 * after the last write is done.
 *
 * @param address Address of the I2C slave
 *
 * @param buffer  Pointer to the buffer holding the data
 *
 * @param count   Number of bytes to be written
 *
 * @return 0 if I2C operation was successful. Error code if unsuccesful
 */
int8u halI2cWriteBytes(int8u address,
                       const int8u *buffer,
                       int8u count);

/** @brief Write bytes with delay
 *
 * This function writes a set of bytes to an I2C slave, returning after a delay
 * after the last write is done.
 *
 * @param address Address of the I2C slave
 *
 * @param buffer  Pointer to the buffer holding the data
 *
 * @param count   Number of bytes to be written
 *
 * @param delay   Number of ms to wait before returning after last write
 *
 * @return 0 if I2C operation was successful. Error code if unsuccesful
 */
int8u halI2cWriteBytesDelay(int8u address,
                         const int8u *buffer,
                         int8u count,
                         int8u delay);

/** @brief Read bytes
 *
 * This function reads a set of bytes from an I2C slave, returning immediately
 * after the last read is done.
 *
 * @param address Address of the I2C slave
 *
 * @param buffer  Pointer to the buffer to hold the incoming data
 *
 * @param count   Number of bytes to be read
 *
 * @return 0 if I2C operation was successful. Error code if unsuccesful
 */
int8u halI2cReadBytes(int8u address,
                      int8u *buffer,
                      int8u count);

#endif // __I2C_DRIVER_H__
