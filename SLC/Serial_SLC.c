
///***********************************************************************
//	PC COMMUNICATION PROTOCOL FILE FOR GETTING AMR DATA
//	AUTHOR : RAMANDEEP SINGH
//***********************************************************************/



# include <stdio.h>
# include <ctype.h>
# include <math.h>
# include <string.h>
# include <stdlib.h>

#include "SLC/serial_SLC.h"
//#include "uart.h"
#include "protocol.h"

extern unsigned char Time_Out;

void ConfigIntUART2(unsigned int config)
{
    /* clear IF flags */
//    IFS0bits.U2RXIF = 0;
//    IFS0bits.U2TXIF = 0;

    /* set priority */
//    IPC7bits.U2RXIP = 0x0007 & config;
//    IPC7bits.U2TXIP = (0x0070 & config) >> 4;

    /* enable/disable interrupt */
//    IEC1bits.U2RXIE = (0x0008 & config) >> 3;
//    IEC1bits.U2TXIE = (0x0080 & config) >> 7;
}

void Configure_UART2(void)
{
//	unsigned int i=0;
//    unsigned int U1MODEvalue;
//    unsigned int U1STAvalue;
//
//    IFS0bits.U1RXIF = 0;
//    IFS0bits.U1TXIF = 0;
//
//    IEC0bits.U1RXIE = 1;
//    IEC0bits.U1TXIE = 0;
//
////    U1MODEvalue = 0b1000001000001000;		//for high baud rate BRGH=1,rts,cts enabled
//    U1MODEvalue = 0b1000000000000000;		//for high baud rate BRGH=1,rts,cts enabled
//    U1STAvalue =  0b0000010000000000;
//
//	U1BRG  = (BAUD_9600);     /* baud rate */
//    U1MODE = U1MODEvalue;  /* operation settings */
//    U1STA = U1STAvalue;   /* TX & RX interrupt modes */
//
//	for(i=0;i<B_SIZE_SERIAL_RF;i++) Serial_RF_Buff[i]=0;
}


/*****************************************************************************/
/*		WriteData_UART2(): Writes data to the serial port for transmitting   */
/*		This function writes one by one data byte to the serial port.
		The data buffer and length of buffer is passed as parameters.		 */
/*****************************************************************************/
void WriteData_UART2(unsigned char *dataPtr,unsigned int len)
{
//	int i=0;
//	for(i=0;i<len;i++)
//	{
//		Time_Out = 0;
//		while(!U1STAbits.TRMT)
//		{
//			if(Time_Out > 5)
//			{ 
//				break;
//			}
//		}
//		U1TXREG = dataPtr[i];   
//	}
}

void WriteDebugMsg(unsigned char *dataPtr)
{
//	int i=0;
//	
//	while(dataPtr[i] != '\0')
//	{
//		while(!U2STAbits.TRMT);
//		U2TXREG = dataPtr[i];  
//		i++; 
//	}
}

//void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void)
//{
//   	IFS0bits.U1RXIF = 0;
//
//    while(U1STAbits.URXDA)
//    {
//		data_rec_RF=1;	//query received
//		Serial_RF_Buff[temp_rx_RF] = U1RXREG;
//		temp_rx_RF++;
//		if(temp_rx_RF >= B_SIZE_SERIAL_RF)
//		{
//			temp_rx_RF = 0;
//		}
//    }
//}





