

#include <math.h>

//#include "Compiler.h"
//#include "stack/include/ember.h"


#include "generic.h"
#include"DS1339.h"
#include "SLC/MSPI.h"
#include "Application_logic.h"
#include "AMR_cs5463.h"
#include "protocol.h"
#include "Gps.h"
#include "hal/micro/buzzer.h"

void Fault_Remove_Logic(void);
unsigned int Lamp_Balast_fault_Remove_Time=0;
unsigned  int Lamp_Balast_fault_Remove_Cnt =0; 
//unsigned char No_check_Lamp_After_This_Event=0;
unsigned char Ballast_Fault_Remove_Retry_Cnt_3 =0;
unsigned char Lamp_Fault_Remove_Retry_Cnt_3 =0;
unsigned char Ballast_Fault_Occured =0;
unsigned char Lamp_Fault_Occured =0;
//unsigned long int Open_circuit=0,Lamp_Failure = 0;

unsigned char panic_dim=0;
unsigned char Dimming_Jump_Set =0,pre_dim_val=0;;
float Dimming_Jump=0,old_dim_val_f=0;
unsigned char temp_dim_inc_val=0,Mix_Mode_Motion_Dimming_Set=0,Mix_Mode_Motion_Dimming_Value=0;

unsigned char CURV_TYPE=1; //default should be lenear
struct Schedule sSch[1][10],Astro_sSch[2],DLH_sSch[2];		// V6.1.10
struct DimSchedule Master_sSch[1][10];
struct Mix_DimSch Mix_sSch[9];
struct Slc_M_Sch Slc_Mix_Mode_schedule[1];
struct Slc_Compress_M_Sch Slc_Comp_Mix_Mode_schedule[1];
struct Stree_light_Lat Lat;
struct Stree_light_Log Log;
struct Stree_light_UTC UTC;

unsigned char Start_To_Cnt_Kwh;
unsigned char automode = 0;
unsigned int Sec_Counter = 0;
unsigned char Photo_Cell_Ok = 1;
unsigned char Set_Do_On = 0;
unsigned char Cloudy_Flag = 0;
long sunriseTime,sunsetTime;
float Latitude,longitude;
float Dimvalue;              // SAN
float utcOffset;

unsigned char sun_StartHour;
unsigned char sun_StartMin;
unsigned char sun_StopHour;
unsigned char sun_StopMin;
unsigned char sun_Flag;
BYTE_VAL  error_condition,error_condition1,test_result,day_sch,test_result2,day_sch_R;
unsigned char Stop_EM_Access =0;
unsigned char chk_interlock;
unsigned char photo_cell_toggel = 0;
unsigned char photo_cell_toggel_counter = 0;
unsigned char photo_cell_Logic_toggel_counter = 0;
unsigned char photo_cell_timer;
unsigned char photo_cell_toggel_timer;
unsigned char check_counter;
unsigned char lamp_lock = 0;
unsigned int check_current_counter = 0;

unsigned int check_current_firsttime_counter = 0;
unsigned char check_cycling_current_counter = 0;
unsigned char cycling_lamp_fault = 0;
unsigned char detect_lamp_current = 0;
float lamp_current;
unsigned char lamp_on_first=0;
unsigned char lamp_current_low = 0;
unsigned char check_half_current_counter=0;
unsigned char half_current_counter=0;
unsigned char lamp_off_to_on = 0;
unsigned int lamp_off_on_timer=0,Dimming_start_timer=0;					// SAN
unsigned char low_current_counter=0;
unsigned char detect_lamp_current_first_time_on = 0;
unsigned char low_voltage_generate=0;
unsigned int Vol_hi;
unsigned int Vol_low;
unsigned int Curr_Steady_Time;
unsigned int Per_Val_Current;
unsigned int Lamp_Fault_Time;
unsigned int Lamp_off_on_Time;
unsigned int Lamp_faulty_retrieve_Count;
float Per_Val;
unsigned char energy_time = 0;
unsigned char energy_meter_ok = 0;
float current_creep_limit = 0.0;
unsigned int Lamp_lock_condition = 0;
unsigned char set_do_flag =0;
unsigned char Master_Sch_Match = 0;					
unsigned char Photo_feedback=1;						
unsigned char ballast_type = 0;
unsigned char adaptive_light_dimming = 0;
unsigned int duty_cycle;
unsigned char old_dim_val=100;
unsigned char dim_inc_timer=0;
unsigned char dim_inc_val = 5;					
unsigned int dim_applay_time = 900;				
unsigned int desir_lamp_lumen = 40000;
unsigned char lumen_tollarence = 1;
unsigned int analog_input_scaling_high_Value = 0;
unsigned int analog_input_scaling_low_Value = 0;
unsigned char Motion_pre_val = 1;
unsigned char Motion_countr = 0;
unsigned char Motion_detected = 0;
unsigned int motion_dimming_timer = 0;
unsigned int motion_intersection_dimming_timer = 0;
unsigned int Motion_dimming_time = 0;
unsigned char Motion_dimming_percentage = 0;
unsigned char motion_dimming_Broadcast_send = 0;
unsigned char motion_dimming_send = 0;
unsigned char Motion_detected_broadcast = 0;
unsigned char Motion_normal_dimming_percentage = 0;
unsigned char Motion_group_id = 0;
unsigned char Motion_pulse_rate = 0;
unsigned char motion_broadcast_timer = 0;
unsigned char Motion_half_sec = 0;
unsigned char Motion_Continiue = 0;
unsigned char Motion_detect_sec=0;
unsigned char Motion_Detect_Timeout=0;
unsigned char Motion_Broadcast_Timeout=0;
unsigned char Motion_Sensor_Type = 0;
unsigned char Motion_continue_timer = 0;
unsigned char Motion_Received_broadcast = 0;
unsigned char Motion_Received_broadcast_counter = 0;
unsigned char Motion_Rebroadcast_timeout = 59;
unsigned char Motion_intersection_detected = 0;
unsigned char Relay_weld_timer = 0;
unsigned char run_Mix_Mode_sch = 0;
unsigned char Civil_Twilight = 0;
unsigned char Schedule_offset = 0;
unsigned char Photocell_steady_timeout_Val = 18;
unsigned char photo_cell_toggel_counter_Val = 10;
unsigned char photo_cell_toggel_timer_Val = 18;
unsigned char Photo_cell_Frequency = 30;
unsigned int Photocell_unsteady_timeout_Val = 5;// 180;
unsigned int lamp_lumen = 0;
unsigned char day_burner_timer = 0;
unsigned char RTC_faulty_detected = 1;      // initialy RTC OK
unsigned char previous_mode = 0;
//struct RTC_Fault_Local RTC_Fault_Logic_data[2];
unsigned char RTC_Fault_Logic_Result = 0;
unsigned char RTC_New_fault_logic_Enable = 1;
unsigned int RTC_drift = 0;
unsigned char Photo_Cell_error = 0;

extern ALLTYPES pulseCounter;
extern unsigned char temp_slc_event,temp_slc_event1;
extern unsigned char Start_dim_flag;
extern BYTE_VAL  GPS_error_condition;

#ifdef NEW_LAMP_FAULT_LOGIC

  float KW_Reference = 0.0;
  float current_Threshold = 0.3;
  float KW_Threshold = 70.0;
  float KW_average = 0.0;
  unsigned char Lock_cyclic_check = 0;
  unsigned int Lock_cyclic_Counter = 0;
  unsigned char one_sec_cyclic = 0;
  unsigned char KW_average_counter = 0;

#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

//#if defined(DALI_SUPPORT)
extern unsigned char f_dalitx;
extern unsigned char f_dalirx;
unsigned char default_dali =0,dim_cycle;
extern unsigned char dali_delay,dimm_cmd_rec;					//SAN
//#endif 
//+++++++++++++++++RS485 And DI2+++++++++++++++++++++++++

unsigned char RS485_0_Or_DI2_1=0;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**********************************************************/
/*
Function: Operate Lamp ON/OFF control by Scheduling
when in Auto mode.
Returns 0

Modified By : Samir Malvi
Date of Modification : 19/10/2010
*/
/**********************************************************/

//unsigned int temp_ms_timer;
void CheckSchedule(void)
{
  unsigned char n,m,i;
  unsigned char scF[20];	

  for(n=0;n<10;n++)                // check all configured schedules.
  {
    scF[n] = 0;
    if(sSch[0][n].bSchFlag)			 // check Schedule enable flag, if enable then chekc schedule time.
    {
      if((Time.Hour >= sSch[0][n].cStartHour) && (Time.Hour <= sSch[0][n].cStopHour))   // if RTC hour is in between start and stop hour then run schedule.
        scF[n] = 1;
      if((Time.Hour == sSch[0][n].cStartHour) && (Time.Min < sSch[0][n].cStartMin))	    // if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.  								
        scF[n] = 0;
      if((Time.Hour == sSch[0][n].cStopHour) && (Time.Min >= sSch[0][n].cStopMin))		    // if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  								
        scF[n] = 0;
      if(((Time.Hour == sSch[0][n].cStopHour) && (sSch[0][n].cStopHour == 23)) && ((Time.Min == sSch[0][n].cStopMin) && (sSch[0][n].cStopMin == 59)))		// if RTC time is 23:59 then start schedule.
        scF[n] = 1;
    }
  }

  if((scF[0] == 1)||(scF[1] == 1)||(scF[2] == 1)||(scF[3] == 1)||(scF[4] == 1)||(scF[5] == 1)||(scF[6] == 1)||(scF[7] == 1)||(scF[8] == 1)||(scF[9] == 1))
  {
    if(adaptive_light_dimming == 2)  // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
    {
      Mix_MOD_Motion_Based_dimming();
    }
    else
    {
      Set_Do(1);          // do lamp ON.
    }
  }
  else
  {
    Set_Do(0);            // do lamp OFF.
  }
}

/*************************************************************************
Function Name: Check_MASTER_Schedule
input: none.
Output: none.
Discription: This function is use for checking dimming schedule.
*************************************************************************/
void Check_MASTER_Schedule(void)						// Master sch 15/04/11
{
  unsigned char n,m,i;
  unsigned char scF[20];	

  for(n=0;n<10;n++)
  {
    scF[n] = 0;
    if(Master_sSch[0][n].bSchFlag)
    {
      if((Time.Hour >= Master_sSch[0][n].cStartHour) && (Time.Hour <= Master_sSch[0][n].cStopHour)) // if RTC hour is in between start and stop hour then run schedule.
        scF[n] = 1;
      if((Time.Hour == Master_sSch[0][n].cStartHour) && (Time.Min < Master_sSch[0][n].cStartMin))					// if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.  								
        scF[n] = 0;
      if((Time.Hour == Master_sSch[0][n].cStopHour) && (Time.Min >= Master_sSch[0][n].cStopMin))					// if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  								
        scF[n] = 0;
      if(((Time.Hour == Master_sSch[0][n].cStopHour) && (Master_sSch[0][n].cStopHour == 23)) && ((Time.Min == Master_sSch[0][n].cStopMin) && (Master_sSch[0][n].cStopMin == 59)))    // if RTC time is 23:59 then start schedule.
        scF[n] = 1;
    }
  }
  /////////////////////////// for normal lamp dimming schedule ///////////////////////
  if(RTC_faulty_detected == 1)
  {
    if(ballast_type == 0)			// normal magnetic ballast.
    {		
      if((scF[0] == 1)||(scF[1] == 1)||(scF[2] == 1)||(scF[3] == 1)||(scF[4] == 1)||(scF[5] == 1)||(scF[6] == 1)||(scF[7] == 1)||(scF[8] == 1)||(scF[9] == 1))
      {
        Set_Do(0);                // if dimming schedule match for Magnetic ballast then do lamp OFF.
        Master_Sch_Match = 1;			
      }
      else
      {
        Master_Sch_Match = 0;			  // if dimming schedule not match for Magnetic ballast then reset flag so mode logic will do lamp ON/OFF.
      }
    }
    /////////////////////////// for electronic ballast lamp dimming schedule ///////////////////////
    else if(ballast_type == 1)		// dimming based on electronic ballast
    {	
      if(scF[0] == 1)
      {						
        dimming_applied(Master_sSch[0][0].dimvalue);  // set dimming persent according to schedule configured.
      }
      else if(scF[1] == 1)
      {						
        dimming_applied(Master_sSch[0][1].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[2] == 1)
      {						
        dimming_applied(Master_sSch[0][2].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[3] == 1)
      {						
        dimming_applied(Master_sSch[0][3].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[4] == 1)
      {						
        dimming_applied(Master_sSch[0][4].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[5] == 1)
      {						
        dimming_applied(Master_sSch[0][5].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[6] == 1)
      {						
        dimming_applied(Master_sSch[0][6].dimvalue);	     // set dimming persent according to schedule configured.
      }
      else if(scF[7] == 1)
      {						
        dimming_applied(Master_sSch[0][7].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[8] == 1)
      {						
        dimming_applied(Master_sSch[0][8].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if(scF[9] == 1)
      {						
        dimming_applied(Master_sSch[0][9].dimvalue);      // set dimming persent according to schedule configured.
      }
      else if ((scF[0] == 0)&&(scF[1] == 0)&&(scF[2] == 0)&&(scF[3] == 0)&&(scF[4] == 0)&&(scF[5] == 0)&&(scF[6] == 0)&&(scF[7] == 0)&&(scF[8] == 0)&&(scF[9] == 0))
      {
        dimming_applied(0);      // if schedule not match then make lamp full bright.
      }
    }
    else
    {
    }		
  }
  else
  {
    dimming_applied(0);
  }
}

/*************************************************************************
Function Name: CheckSchedule_sun
input: none.
Output: unsigned char result.
Discription: This function is use for checking Astro based schedule.
*************************************************************************/
unsigned char CheckSchedule_sun(void)        // check astro schedule
{
  unsigned char n,m,i;
  unsigned char scF[20];	

  for(n=0;n<2;n++)
  {
    scF[n] = 0;
    if(Astro_sSch[n].bSchFlag)
    {
      if((Time.Hour >= Astro_sSch[n].cStartHour) && (Time.Hour <= Astro_sSch[n].cStopHour)) // if RTC hour is in between start and stop hour then run schedule.
        scF[n] = 1;
      if((Time.Hour == Astro_sSch[n].cStartHour) && (Time.Min < Astro_sSch[n].cStartMin))       // if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.  								
        scF[n] = 0;
      if((Time.Hour == Astro_sSch[n].cStopHour) && (Time.Min > Astro_sSch[n].cStopMin))         // if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  								
        scF[n] = 0;
    }
  }

  if((scF[0] == 1) || (scF[1] == 1))
  {
        return 1;
  }
  else
  {
        return 0;
  }
//  if((scF[0] == 1) || (scF[1] == 1))
//  {
//    //		 Set_Do(1);
//    if(adaptive_light_dimming == 2)    // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
//    {
//      Mix_MOD_Motion_Based_dimming();
//    }
//    else
//    {
//      Set_Do(1);
//    }
//  }
//  else
//  {
//    Set_Do(0);           // if schedule not match then off lamp.
//  }
}

/*************************************************************************
Function Name: CheckSchedule_DLH Day light harvesting
input: none.
Output: none.
Discription: This function is use for checking Astro based schedule with day light harvesting.
*************************************************************************/
void CheckSchedule_DLH(void)
{
  unsigned char n,m,i;
  unsigned char scF[20];	

  for(n=0;n<2;n++)
  {
    scF[n] = 0;
    if(DLH_sSch[n].bSchFlag)
    {
      if((Time.Hour >= DLH_sSch[n].cStartHour) && (Time.Hour <= DLH_sSch[n].cStopHour)) // if RTC hour is in between start and stop hour then run schedule.
        scF[n] = 1;
      if((Time.Hour == DLH_sSch[n].cStartHour) && (Time.Min < DLH_sSch[n].cStartMin))   // if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.  								
        scF[n] = 0;
      if((Time.Hour == DLH_sSch[n].cStopHour) && (Time.Min > DLH_sSch[n].cStopMin))     // if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  								
        scF[n] = 0;
    }
  }
  if((scF[0] == 1) || (scF[1] == 1))
  {
    //		 Set_Do(1);
    if(adaptive_light_dimming == 2)   // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
    {
      Mix_MOD_Motion_Based_dimming();
    }
    else
    {
      Set_Do(1);
    }
  }
  else
  {
    Set_Do(0);                  // if schedule not match then off lamp.
  }
}

/*************************************************************************
Function Name: Set_Do
input: lamp action 0 = off, 1 = On.
Output: none.
Discription: This function is use turn lamp on or off.
*************************************************************************/
//unsigned char Display_1[100];
void Set_Do(unsigned char i)
{
  unsigned char dali_detect_count=0;
 // unsigned char Display_1[100],ADC_loop;
  unsigned char j=0,k=0,serach=0;
  if(i == 0)          // off Lamp
  {
    chk_interlock = 0;          // if lamp off then there is no need to check interlock.
    lamp_on_first = 0;		        // set variable for ON to OFF transection
    if(lamp_off_to_on == 0)									// check for on to off transection and make 60 sec delay between off to on.
    {
      lamp_off_to_on = 1;
      lamp_off_on_timer = 0;
      Relay_weld_timer = 0;             // reset Relay weld timer.
      halClearLed(DO_1);                // Set pin status to 1
      halClearLed(DO_2);                // Set pin status to 1
  

      if(energy_meter_ok == 1)							// energy meter faulty so display lamp off when command fire for off
      {
        error_condition.bits.b0 = 0;
      }

      set_do_flag = 0;               // set variable to know other function like Lamp OFF fire.
//#ifndef ISLC_10
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
      dimming_applied(100);          // Set dimming value to 0v
      Start_dim_flag =0;	            // set flage to start dimming after lamp ON.
      old_dim_val = 100;
      old_dim_val = 100 - old_dim_val; // set old dim value to 0 for step increment dimming.

//#if (defined(DALI_SUPPORT))
if(dali_support == 1)
{
}
//#else
else
{
      duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));
      DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;
}
//#endif

//      if(idimmer_en == 1)              // if Idimmer enable then send message for lamp OFF to Idimmer.
//      {
//        //     			idimmer_dimming_applied(old_dim_val);
//        send_idimmer_Val_frame = 1;
//        idimmer_dimval = old_dim_val;
//      }
#endif


#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
      emberAfGuaranteedPrint("%p", "  Set_Do off");
#endif

      check_day_burnar_flag = 1;           // check day burner after lamp off transection.
      day_burner_timer = 0;                // reset day burner timer.
      calculate_kwh_LampOFF();             // store last reading of KWH in to NV
      Calculate_burnhour_Lampoff();        // store last reading of Burn_Hour in to NV
    }				
  }
  else
  {
    if(lamp_lock == 0)          // check Lamp lock if set to 0 then do not lamp ON.
    {
      if((lamp_off_to_on == 1) && (lamp_off_on_timer < Lamp_off_on_Time))			// do not on lamp if time is less then lamp_off_On time delay sec.
      {

      }
      else
      {
        lamp_off_to_on = 0;   // set variable for off to On transection

        chk_interlock = 1;      // start checking of all interlocks for current.
        if(lamp_on_first == 0)  // if Lamp OFF then make lamp ON else bypass below routine.
        {
          Stop_EM_Access = 1;   // at lamp on stop em reading if required at preset no need.
          detect_lamp_current_first_time_on = 0;  // reset all trip related parameters.
          check_current_firsttime_counter = 0;
          lamp_on_first = 1;
          lamp_current_low = 0;
          check_current_counter = 0;
          check_half_current_counter=0;
          halClearLed(DO_1);                    // Set pin status to 0
          halSetLed(DO_2);                      // Set pin status to 0
          set_do_flag = 1;													         // set variable to know other function like Lamp ON fire.

          if(energy_meter_ok == 1)											// energy meter faulty so display lamp ON when command fire for ON
          {
            error_condition.bits.b0 = 1;
          }
          check_day_burnar_flag = 1;        // check day burner after lamp off transection.
          day_burner_timer = 0;             // reset day burner timer.
//#ifndef ISLC_10
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
          Dimming_start_timer =0;			            // reset dimming start timer.									
          Start_dim_flag =0;			                 // reset dimming start flag to provide delay.								
//#if (defined(DALI_SUPPORT))
if(dali_support == 1)
{
          if(!default_dali)
          {
            default_dali =1;
            for(j=0;j<150;j++)delay(60060);     // 55 msec
            halClearLed(EN_DALI); // Enable
//            UNICAST_SHORTADDRESS();
            MAKE_DEFAULT();
////            halSetLed(EN_DALI);
//            delay(60060);     // 55 msec
//	           delay(60060);     // 55 msec
//            delay(60060);     // 55 msec
////            emberAfGuaranteedPrint("\r\n%p", "Dali En High");
//            set_Linear_Logarithmic_dimming(CURV_TYPE);
//            delay(60060);     // 55 msec
//            dimming_applied(0);                   // Set dimming percentage to full bright.
/////////////////////////////////////////////////////////

             for(j=0;j<24;j++)delay(60060);     // 55 msec
             for(j=0;j<24;j++)delay(60060);     // 55 msec
            delay(60060);     // 55 msec
            delay(60060);     // 55 msec
            set_Linear_Logarithmic_dimming(CURV_TYPE);
            delay(60060);     // 55 msec
            dimming_applied(0);                   // Set dimming percentage to full bright.
/////////////////////////////////////////////////////////

          }
          else
          {
            if(Cmd_Rcv_To_Change_Curv_Ype == 1)
            {
              Cmd_Rcv_To_Change_Curv_Ype =0;
                set_Linear_Logarithmic_dimming(CURV_TYPE);
                dali_delay =0;
            }
            else if(Cmd_Rcv_To_Change_Curv_Ype == 2)
            {
                  Cmd_Rcv_To_Change_Curv_Ype =0;
                  Multiple_Dali_Driver_Handle();
            }
          }
          dimming_applied(0);                   // Set dimming percentage to full bright.

}
//#else
else
{

          dimming_applied(0);                   // Set dimming percentage to full bright.
          old_dim_val = 0;
          old_dim_val = 100 - old_dim_val; // set old dim value to 0 for step increment dimming.

          duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));
          DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;
}
//#endif
//          if(idimmer_en == 1)              // if Idimmer enable then send message to Idimmer for Lamp ON.
//          {
//            //						idimmer_dimming_applied(old_dim_val);
//            send_idimmer_Val_frame = 1;
//            idimmer_dimval = old_dim_val;
//          }
#endif


#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
          emberAfGuaranteedPrint("%p", "  Set_Do on");
#endif
        }				
      }			
    }
  }	
}

/*************************************************************************
Function Name: check_logic
input: None.
Output: none.
Discription: This function is use to turn lamp on/off according to
             configured SLC Mode.
*************************************************************************/
void check_logic(void)
{
  unsigned char temp = 0;
  unsigned char Display[50];
  if((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0)) 	// check if voltage trip occurs and lamp lock required then not check any logic mode. 		
  {
    if(adaptive_light_dimming != 1)              // if dimming mode is daylight harvesting then not check any logic mode.
    {

      if(automode == 0)               // manual mode
      {
        if(Set_Do_On == 1)                      // lamp on command received from application then do lamp on.
        {
          if(adaptive_light_dimming == 2)       // if dimming mode is motion based dimming then check motion first.
          {
            Mix_MOD_Motion_Based_dimming();
          }
          else
          {
            Set_Do(1);
          }
        }
        else                        // if lamp off command received from application then make lamp off.
        {
          Set_Do(0);
        }
      }
      else if(automode == 2)				//schedual control mode.	
      {
        if(RTC_faulty_detected == 1)
        {
          CheckSchedule();               // check schedule if match thne make lamp On.
        }
        else
        {
          previous_mode = automode;  // store mode value before chage.
          halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
          automode = 1;								
          halCommonSetToken(TOKEN_automode, &automode);
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
          sprintf(Display,"\r\nPrevious mode change by schedule function");
          emberAfGuaranteedPrint(" %p", Display);
          emberSerialWaitSend(APP_SERIAL);
#endif
        }
      }
      else if(automode == 1)				//photo control mode.
      {		
        #if (defined(ISLC3300_T8_V2)||defined(ISLC_T8_V4))
           automode = 8;								
           halCommonSetToken(TOKEN_automode, &automode);
        #else
          	
            if(Photo_Cell_Ok == 1)      // if photocell trip not set then check photocell status
            {
              if(Photo_feedback == 0)						// photocell status for Night.
              {
                if(adaptive_light_dimming == 2)  // if dimming mode is motion based dimming then check motion first.
                {
                  Mix_MOD_Motion_Based_dimming();
                }
                else
                {
                  Set_Do(1);
                }
              }
              else                              // photocell status for Day.
              {
                Set_Do(0);
              }
            }
            else               // if photocell fault then switch mode to Astro with photocell override.
            {

              if((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable != 1))  // if RTC OK then change mode to astro from photocell.
              {
                      automode = 4;								
                      halCommonSetToken(TOKEN_automode, &automode);
                      previous_mode = automode;  // store mode value before chage.
                      halCommonSetToken(TOKEN_previous_mode, &previous_mode);

            #if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
                      sprintf(Display,"\r\nPrevious mode change by photocell function");
                      emberAfGuaranteedPrint(" %p", Display);
                      emberSerialWaitSend(APP_SERIAL);
            #endif

                      GetSCHTimeFrom_LAT_LOG();       // Calculate Astro schedule from lat/long.
                      Astro_sSch[0].cStartHour = sunsetTime/3600;				
                      Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
                      Astro_sSch[0].cStopHour  = 23;							
                      Astro_sSch[0].cStopMin   = 59;							
                      Astro_sSch[0].bSchFlag   = 1;							

                      Astro_sSch[1].cStartHour = 00;							
                      Astro_sSch[1].cStartMin  = 00;							
                      Astro_sSch[1].cStopHour  = sunriseTime/3600;			
                      Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
                      Astro_sSch[1].bSchFlag   = 1;							
                      halCommonSetToken(TOKEN_schedule_Astro_sSch,&Astro_sSch[0]);
              }
              else
              {
                    automode = 8;								
                    halCommonSetToken(TOKEN_automode, &automode);
              }
            }
         #endif
      }
      else if(automode == 3)			//Astro timer mode.
      {
        if((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable == 0))
        {
          temp = CheckSchedule_sun();    //  check Astro schedule if match then make Lamp On.
          if(temp == 1)
          {
            if(adaptive_light_dimming == 2)    // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
            {
              Mix_MOD_Motion_Based_dimming();
            }
            else
            {
              Set_Do(1);
            }
          }
          else
          {
            Set_Do(0);           // if schedule not match then off lamp.
          }
        }
        else
        {
          previous_mode = automode;  // store mode value before chage.
          halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
          automode = 1;								
          halCommonSetToken(TOKEN_automode, &automode);
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
          sprintf(Display,"\r\nPrevious mode change by Astro function");
          emberAfGuaranteedPrint(" %p", Display);
          emberSerialWaitSend(APP_SERIAL);
#endif
        }
      }
      else if(automode == 4)			//Astro timer with photo control over ride Mode.
      {
        if(RTC_New_fault_logic_Enable == 1)
        {
          if((RTC_faulty_detected == 0) && (Photo_Cell_Ok == 0))
          {
            previous_mode = automode;  // store mode value before chage.
            halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
            automode = 8;								
            halCommonSetToken(TOKEN_automode, &automode);
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
          sprintf(Display,"\r\nPrevious mode change by Astro-photo function");
          emberAfGuaranteedPrint(" %p", Display);
          emberSerialWaitSend(APP_SERIAL);
#endif
          }
          else
          {
            temp = CheckSchedule_sun();          //  check Astro schedule if match then make Lamp On.

            if(((temp == 1) && (RTC_faulty_detected == 1)) || (Cloudy_Flag == 1) || ((Photo_feedback == 0) && (Photo_Cell_Ok == 1)))
            {
              if(adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
              {
                Mix_MOD_Motion_Based_dimming();
              }
              else
              {
                Set_Do(1);
              }
            }
            else
            {
              Set_Do(0);
            }
          }
        }
        else
        {
          temp = CheckSchedule_sun();          //  check Astro schedule if match then make Lamp On.
          if((temp == 1) || (Cloudy_Flag == 1))
          {
            if(adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
            {
              Mix_MOD_Motion_Based_dimming();
            }
            else
            {
              Set_Do(1);
            }
          }
          else
          {
            Set_Do(0);
          }
        }
      }
      else if(automode == 5)			// civil twilight mode
      {
        if((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable == 0))
        {
          temp = CheckSchedule_sun();    //  check Astro schedule if match then make Lamp On.
          if(temp == 1)
          {
            //		 Set_Do(1);
            if(adaptive_light_dimming == 2)    // if dimming mode is motion based dimming then do lamp off/on according to motion sensor input.
            {
              Mix_MOD_Motion_Based_dimming();
            }
            else
            {
              Set_Do(1);
            }
          }
          else
          {
            Set_Do(0);           // if schedule not match then off lamp.
          }
        }
        else
        {
          previous_mode = automode;  // store mode value before chage.
          halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
          automode = 1;								
          halCommonSetToken(TOKEN_automode, &automode);
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
          sprintf(Display,"\r\nPrevious mode change by Civiltwilight function");
          emberAfGuaranteedPrint(" %p", Display);
          emberSerialWaitSend(APP_SERIAL);
#endif
        }
      }
      else if(automode == 6)			// civil twilight with photo cell override mode
      {
        if(RTC_New_fault_logic_Enable == 1)
        {
          if((RTC_faulty_detected == 0) && (Photo_Cell_Ok == 0))
          {
            previous_mode = automode;  // store mode value before chage.
            halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
            automode = 8;								
            halCommonSetToken(TOKEN_automode, &automode);
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
            sprintf(Display,"\r\nPrevious mode change by Civiltwilight-photo function");
            emberAfGuaranteedPrint(" %p", Display);
            emberSerialWaitSend(APP_SERIAL);
#endif
          }
          else
          {
            temp = CheckSchedule_sun();          //  check Astro schedule if match then make Lamp On.

            if(((temp == 1) && (RTC_faulty_detected == 1)) || (Cloudy_Flag == 1) || ((Photo_feedback == 0) && (Photo_Cell_Ok == 1)))
            {
              if(adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
              {
                Mix_MOD_Motion_Based_dimming();
              }
              else
              {
                Set_Do(1);
              }
            }
            else
            {
              Set_Do(0);
            }
          }
        }
        else
        {
          temp = CheckSchedule_sun();          //  check Astro schedule if match then make Lamp On.
          if((temp == 1) || (Cloudy_Flag == 1))
          {
            if(adaptive_light_dimming == 2) // if dimming mode is motion based dimming then check motion first.
            {
              Mix_MOD_Motion_Based_dimming();
            }
            else
            {
              Set_Do(1);
            }
          }
          else
          {
            Set_Do(0);
          }
        }	
      }
      else if(automode == 8)        // fail safe mode.
      {
        if(adaptive_light_dimming == 2)       // if dimming mode is motion based dimming then check motion first.
        {
          Mix_MOD_Motion_Based_dimming();
        }
        else
        {
          Set_Do(1);
        }
      }
      else
      {
      }		
    }
    else
    {
      if((RTC_New_fault_logic_Enable == 1) && (RTC_faulty_detected == 0))
      {
          if(automode != 8)
          {
            //previous_mode = automode;  // store mode value before chage.
            //halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
            automode = 8;								
            halCommonSetToken(TOKEN_automode, &automode);
          }
          Set_Do(1);
      }
      else
      {
        CheckSchedule_DLH(); //  check Day light Harvesting schedule if match then make Lamp On.
        //Set_Do(1); //for testing
      }
      //CheckSchedule_DLH(); //  check Day light Harvesting schedule if match then make Lamp On.
    }	
  }
}

/*************************************************************************
Function Name: GetSCHTimeFrom_LAT_LOG
input: None.
Output: none.
Discription: generate sunrise and sunset time from lat/long variables.
*************************************************************************/
void GetSCHTimeFrom_LAT_LOG(void)
{
  double Lat_deg,Lat_min,Lat_Sec,Log_deg,Log_min,Log_Sec;

  sunriseTime = Calculate(SUNRISE);       // calculate time for sunrise from lat,long,timezone,date.
  sunriseTime = sunriseTime - Sunrise_delay;  // add sunrise delay to computed time.
  sunsetTime = Calculate(SUNSET);          // calculate time for sunset from lat,long,timezone,date.
  sunsetTime = sunsetTime + Sunset_delay;       // add sunset delay to computed time.
}

/*************************************************************************
Function Name: Day_light_harves_time
input: None.
Output: none.
Discription: generate day light harvesting sunrise and sunset time from lat/long variables.
*************************************************************************/
void Day_light_harves_time(void)
{
  Day_light_harvesting_start_time = Calculate(SUNSET);       // calculate time for sunset from lat,long,timezone,date.
  Day_light_harvesting_start_time = Day_light_harvesting_start_time - Day_light_harvesting_start_offset;   // add start offset to computed time.

  Day_light_harvesting_stop_time = Calculate(SUNRISE);       // calculate time for sunrise from lat,long,timezone,date.
  Day_light_harvesting_stop_time = Day_light_harvesting_stop_time + Day_light_harvesting_stop_offset;      // add stop offset to computed time.
}	


/*************************************************************************
Function Name: Calculate
input: direction for calculation 0 = sunrise, 1 = Sunset.
Output: none.
Discription: this function is use for calculation of astro timing.
*************************************************************************/
long int Calculate(unsigned char direction)
{
  /* doy (N) */
  int i = 0;
  double lngHour = 0.0;
  double t = 0.0;
  double M = 0.0;
  double L = 0.0;
  double RA = 0.0;
  double Lquadrant = 0.0;
  double RAquadrant = 0.0;
  double sinDec = 0.0;
  double cosDec = 0.0;
  double cosH = 0.0;
  double H = 0.0;
  double T = 0.0 ;
  double zenith = 0.0;
  double UT = 0.0 ;
  int N = 0;		
  long temp_hour = 0,temp_min = 0;	
  double temp_utcoffset = 0.0;																			//06/07/09


  if((automode == 5) || (automode == 6) || (Civil_Twilight == 1))       // civil twilight mode
  {
    zenith = 96000;	
  }
  else
  {
    zenith = 90500;	
  }		
////////////////
  if((automode == 3) || (automode == 4))
  {
    zenith = 90500;
  }
////////////////

  for(i=1;i<Date.Month;i++)
  {
    if(i==1 || i==3 ||i==5 ||i==7 ||i==8 ||i==10 ||i==12 )
    {
      N=N+31;
    }
    else if(i==4 || i==6 ||i==9 ||i==11 )
    {
      N=N+30;
    }		

    if(i==2)
    {
      if(Date.Year%4==0)
        N=N+29;
      else
        N=N+28;

    }
  }
  N=N+Date.Date;
  lngHour = (double)longitude / 15.0;																	/* appr. time (t) */

  if (direction == SUNRISE)
    t = N + ((6.0 - lngHour) / 24.0);
  else
    t = N + ((18.0 - lngHour) / 24.0);

  M = (0.9856 * t) - 3.289;																  	/* mean anomaly (M) */
  L = M + (1.916 * sin(Deg2Rad(M))) + (0.020 * sin(Deg2Rad(2 * M))) + 282.634;			  	/* true longitude (L) */
  L = FixValue(L, 0, 360);																  	/* right asc (RA) */
  RA = Rad2Deg(atan(0.91764 * tan(Deg2Rad(L))));											
  RA = FixValue(RA, 0, 360);
  Lquadrant = (floor(L / 90.0)) * 90.0;													  	/* adjust quadrant of RA */
  RAquadrant = (floor(RA / 90.0)) * 90.0;
  RA = RA + (Lquadrant - RAquadrant);
  RA = RA / 15.0;
  sinDec = 0.39782 * sin(Deg2Rad(L));													  		/* sin cos DEC (sinDec / cosDec) */
  cosDec = cos(asin(sinDec));
  cosH = (cos(Deg2Rad((double)zenith / 1000.0f)) - (sinDec * sin(Deg2Rad((double)Latitude)))) / (cosDec * cos(Deg2Rad((double)Latitude)));      /* local hour angle (cosH) */

  if (direction == SUNRISE)
    H = 360.0 - Rad2Deg(acos(cosH));
  else
    H = Rad2Deg(acos(cosH));


  H = H / 15.0;

  /* time (T) */
  T = H + RA - (0.06571 * t) - 6.622;

  /* universal time (T) */
  UT = T - lngHour;


  temp_utcoffset = utcOffset * 100;        // convert UTC to hour and min.

  temp_hour = temp_utcoffset / 100;
  temp_min = ((unsigned long)temp_utcoffset % 100);
  if(Time_change_dueto_DST == 1)           // add Day light saving time zone difference to min.
  {
    temp_min = temp_min + (int)SLC_DST_Time_Zone_Diff;
  }

  temp_utcoffset = (temp_hour + ((float)temp_min / 60));  // add min to UTC OFFSET
  UT += (double)temp_utcoffset;  // add UTC OFFset to computed time.

  UT = FixValue(UT, 0, 24);



  UT = (UT * 3600 );       // convert computed time from hour to sec.

  return (UT);  // Convert to seconds
}

/*************************************************************************
Function Name: Deg2Rad
input: angle in degree.
Output: radian.
Discription: this function is use for convert angle from degree to radian.
*************************************************************************/
double Deg2Rad(double angle)
{
  return (PI * angle / 180.0);
}

/*************************************************************************
Function Name: Rad2Deg
input: angle in degree.
Output: radian.
Discription: this function is use for convert angle from radian to degree.
*************************************************************************/
double Rad2Deg(double angle)
{
  return (180.0 * angle / PI);
}

/*************************************************************************
Function Name: FixValue
input: vaule which need to fix, min range and max range.
Output: formated value.
Discription: this function is use for convert value to its fix formated value.
*************************************************************************/
double FixValue(double value, double min, double max)
{
  while (value < min)
    value += (max - min);

  while (value >= max)
    value -= (max - min);

  return value;
}

/*************************************************************************
Function Name: DegreesToAngle
input: degree, minutes, seconds.
Output: value in angle.
Discription: this function is use for convert value from degree to angle.
*************************************************************************/
double DegreesToAngle(double degrees, double minutes, double seconds)
{
  if (degrees < 0)
    return ((double)(degrees - (minutes / 60.0) - (seconds / 3600.0)));
  else
    return ((double)(degrees + (minutes / 60.0) + (seconds / 3600.0)));
}

/*************************************************************************
Function Name: check_interlock
input: None.
Output: None.
Discription: this function is use for checking all voltage and current related
             interlocks.
*************************************************************************/
#ifdef NEW_LAMP_FAULT_LOGIC

void check_interlock(void)
{
//  unsigned char Display[100];
  float Per_Val;

  Per_Val = (float)Per_Val_Current/100;
  if((energy_time == 0) && (Commissioning_flag == 0))
  {
    if((chk_interlock == 1)	&& (Dimvalue == 0))	 // V6.1.12		//check interlock after do_on command and dimm value at 0 persent.
    {	
      Fault_Remove_Logic();
      if(detect_lamp_current == 0)				// check if steady current not saved
      {
        if(check_current_firsttime_counter > Curr_Steady_Time)
        {
          check_current_firsttime_counter = 0;
          lamp_current = irdoub;
 //         lamp_current = w1;        // this will store steady state watt.
          KW_Threshold = (float)w1;
          halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);
          pulseCounter.float_data = lamp_current;

//          if(lamp_current > 0.03)
//          {
//            current_creep_limit = lamp_current * 0.1;
//            if(current_creep_limit < 0.03)
//            {
//              current_creep_limit = 0.03;												// if current creep limit less then 0.03 make it 0.03	
//            }
//            else if(current_creep_limit > 0.1)
//            {
//              current_creep_limit = 0.1;												// if current creep limit more then 0.1 make it 0.1
//            }
//            else
//            {
//
//            }
//            //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 0,pulseCounter.byte[3]);
//            //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 1,pulseCounter.byte[2]);
//            //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 2,pulseCounter.byte[1]);
//            //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 3,pulseCounter.byte[0]);
//            halCommonSetToken(TOKEN_lamp_current,&lamp_current);
//
//            detect_lamp_current = 1;
//            //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT_SET,detect_lamp_current);
//            halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);
//            KW_Reference = 0;
//            cycling_lamp_fault = 0;
//            KW_average = 0;
//          }		
        #if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
               // if(lamp_current > 0.03)
                if(lamp_current > 0.05)
                {
                  current_creep_limit = lamp_current * 0.1;
                 // if(current_creep_limit < 0.03)
                  if(current_creep_limit < 0.05)
                  {
                    // current_creep_limit = 0.03;												// if current creep limit less then 0.03 make it 0.03	
                      current_creep_limit = 0.05;												// if current creep limit less then 0.03 make it 0.03	
                  }
                  else if(current_creep_limit > 0.1)
                  {
                    current_creep_limit = 0.1;												// if current creep limit more then 0.1 make it 0.1
                  }
                  else
                  {

                  }
                 
                  halCommonSetToken(TOKEN_lamp_current,&lamp_current);

                  detect_lamp_current = 1;
                 
                  halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);
                  KW_Reference = 0;
                  cycling_lamp_fault = 0;
                  KW_average = 0;
                }
        #else
                  if(lamp_current > 0.03)
                  {
                    current_creep_limit = lamp_current * 0.1;
                    if(current_creep_limit < 0.03)
                    {
                      current_creep_limit = 0.03;												// if current creep limit less then 0.03 make it 0.03	
                    }
                    else if(current_creep_limit > 0.1)
                    {
                      current_creep_limit = 0.1;												// if current creep limit more then 0.1 make it 0.1
                    }
                    else
                    {

                    }
                    //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 0,pulseCounter.byte[3]);
                    //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 1,pulseCounter.byte[2]);
                    //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 2,pulseCounter.byte[1]);
                    //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT + 3,pulseCounter.byte[0]);
                    halCommonSetToken(TOKEN_lamp_current,&lamp_current);

                    detect_lamp_current = 1;
                    //	WriteByte_EEPROM(EE_LAMP_STADY_CURRENT_SET,detect_lamp_current);
                    halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);
                    KW_Reference = 0;
                    cycling_lamp_fault = 0;
                    KW_average = 0;
                  }
        #endif
 
        }
      }
      else if((detect_lamp_current_first_time_on == 0) && (check_current_firsttime_counter > Curr_Steady_Time))
      {
        detect_lamp_current_first_time_on = 1;
        check_current_firsttime_counter = 0;
        check_half_current_counter = 0;
        KW_Reference = 0;
        cycling_lamp_fault = 0;
        KW_average = 0;
        KW_average_counter = 0;
        Lock_cyclic_check = 0;
        error_condition.bits.b6 = 0;
		      check_current_counter = 0;
      }
      else
      {
      }	

// ballast fault trip checking logic
//      if((irdoub < current_Threshold) && (w1 < KW_Threshold) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
      if((irdoub < current_creep_limit) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
      {
        if(check_current_counter >= Lamp_Fault_Time)
        {
          check_current_counter = 0;
          if(error_condition1.bits.b0 == 0)
          {
            if(Lamp_lock_condition == 1)
            {
              Set_Do(0);
            }
          }
          low_current_counter++;
          //sprintf(Display,"\nlow_current_counter = %u",low_current_counter);		
          //emberAfGuaranteedPrint("\r\n%p", Display);
          if(low_current_counter >= Lamp_faulty_retrieve_Count)							// check current low condition 3 times then lock the lamp.
          {
            Ballast_Fault_Occured =1;
            error_condition1.bits.b0 = 1;
            // ballast fail trip
            low_current_counter = 0;
            check_current_counter = 0;
            if(Lamp_lock_condition == 1)
            {
              lamp_lock = 1;
              Set_Do(0);	
            }
          }	
        }
      }
// ballast fault trip checking logic

// Lamp fault trip checking logic
//      else if((irdoub >= current_Threshold) && (w1 < KW_Threshold) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
      else if((w1 < (KW_Threshold * (1.0 - Per_Val))) &&
              (detect_lamp_current == 1) &&
              (detect_lamp_current_first_time_on == 1)
             )
      {
        if(check_current_counter >= Lamp_Fault_Time)
        {
          check_current_counter = 0;
          if(error_condition.bits.b3 == 0)
          {
            if(Lamp_lock_condition == 1)
            {
              Set_Do(0);
            }
          }
          low_current_counter++;
          //sprintf(Display,"\nlow_current_counter = %u",low_current_counter);		
          //emberAfGuaranteedPrint("\r\n%p", Display);
          if(low_current_counter >= Lamp_faulty_retrieve_Count)							// check current low condition 3 times then lock the lamp.
          {
            error_condition.bits.b3 = 1;
            Lamp_Fault_Occured =1;												// ballast fail trip
            low_current_counter = 0;
            check_current_counter = 0;
            if(Lamp_lock_condition == 1)
            {
              lamp_lock = 1;
              Set_Do(0);	
            }
          }	
        }
      }
// Lamp fault trip checking logic
      else
      {
        check_current_counter = 0;
        if((detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))
        {
                low_current_counter = 0;
        }
      }

// cyclic lamp fault
      if((detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1) && (Lock_cyclic_check == 0))
      {
        if(one_sec_cyclic == 1)
        {
            one_sec_cyclic = 0;
            KW_average = KW_average + w1;
            KW_average_counter++;
            if(KW_average_counter >= 10)
            {

                KW_average = KW_average/KW_average_counter;
                if(KW_Reference < KW_average)
                {
                   KW_Reference = KW_average;
                }
                KW_average_counter = 0;
                KW_average = 0;
            }
            //sprintf(Display,"KW_Reference = %f,KW_average = %f,w1 = %f",KW_Reference,KW_average,w1);		
            //emberAfGuaranteedPrint("\r\n%p", Display);
            if(w1 < (KW_Reference * (1-0.50)))     //|| (w1 > (KW_Reference * (1+0.50)))
            {
                KW_Reference = 0;
                KW_average_counter = 0;
                KW_average = 0;
                cycling_lamp_fault++;
                //sprintf(Display,"\r\ncycling_lamp_fault = %d",cycling_lamp_fault);		
                //emberAfGuaranteedPrint("\r\n%p", Display);
                if(cycling_lamp_fault >= 10)
                {
                    cycling_lamp_fault = 0;
                    error_condition.bits.b6 = 1;
                    if(Lamp_lock_condition == 1)
                    {
                         lamp_lock = 1;
                         Set_Do(0);
                    }
                }
                else
                {
                    //error_condition.bits.b6 = 0;
                }
                Lock_cyclic_check = 1;
                Lock_cyclic_Counter = 0;
            }
        }
      }
// cyclic lamp fault
    }
  }

  if((vrdoub < Vol_low) || (vrdoub > Vol_hi))													//Slc low voltage trip generate
  {
    if(check_counter >= 5)
    {
      error_condition.bits.b2 = 1;
      if(Lamp_lock_condition == 1)
      {
        Set_Do(0);
      }
    }
  }
  else
  {
    error_condition.bits.b2 = 0;
    check_counter = 0;
  }

  if(irdoub < current_creep_limit)
  {
    error_condition.bits.b0 = 0;												//SLC Lamp ON feedback.
    if(lamp_off_to_on == 1)
    {
      error_condition1.bits.b5 = 0;											// relay weld condition remove.	
      Relay_weld_timer = 0;
    }
  }
  else
  {
    error_condition.bits.b0 = 1;			 												//SLC Lamp ON feedback.
    if((lamp_off_to_on == 1) && (Relay_weld_timer > 3))
    {
      error_condition1.bits.b5 = 1;											// relay weld condition generated			
    }				
  }

}

#elif defined(OLD_LAMP_FAULT_LOGIC)

void check_interlock(void)
{

  if((energy_time == 0) && (Commissioning_flag == 0))  // if Energy meter not working properly or SLC in commissioning then not check interlock.
  {
    if((chk_interlock == 1)	&& (Dimvalue == 0))	 // if dimming percentage set to non 0 value or lamp off then not check interlock.
    {			
      if(detect_lamp_current == 0)				// check if steady current not saved
      {
        if(check_current_firsttime_counter > Curr_Steady_Time)   // wait for current become stady.
        {
          check_current_firsttime_counter = 0;
          lamp_current = irdoub;                         // store currnet in to current variable.
          pulseCounter.float_data = lamp_current;

          if(lamp_current > 0.03)             // if proper current detect then generate creep limit value for fault detection.
          {
            current_creep_limit = lamp_current * 0.1;
            if(current_creep_limit < 0.03)
            {
              current_creep_limit = 0.03;												// if current creep limit less then 0.03 make it 0.03	
            }
            else if(current_creep_limit > 0.1)
            {
              current_creep_limit = 0.1;												// if current creep limit more then 0.1 make it 0.1
            }
            else
            {

            }
            halCommonSetToken(TOKEN_lamp_current,&lamp_current);
            detect_lamp_current = 1;                // set flag for lamp stady current stored.
            halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);
          }		
        }
      }
      else if((detect_lamp_current_first_time_on == 0) &&
              (check_current_firsttime_counter > Curr_Steady_Time))   // if steady current already stored then wait up to steady current time when every lamp on so current became steady.
      {
        detect_lamp_current_first_time_on = 1;
        check_current_firsttime_counter = 0;
        check_half_current_counter = 0;
      }
      else
      {
      }	


      if((irdoub < current_creep_limit) && (detect_lamp_current == 1)
         && (detect_lamp_current_first_time_on == 1))  // after steady current time over then check lamp current
      {
        if(check_current_counter >= Lamp_Fault_Time)  // check if current remain law up to fault time or not
        {
          if(error_condition1.bits.b0 == 0)  // check if lamp fault declered or not.
          {
            if(Lamp_lock_condition == 1)     // if lamp lock required then do lamp off.
            {
              Set_Do(0);
            }
          }
          low_current_counter++;              // increase fault counter if greater then retrieve count then declare fault condtion.
          if(low_current_counter >= Lamp_faulty_retrieve_Count)							// check current low condition 3 times then lock the lamp.
          {
            Ballast_Fault_Occured =1;
            error_condition1.bits.b0 = 1;												// balast fail trip
            low_current_counter = 0;
            check_current_counter = 0;
            if(Lamp_lock_condition == 1)      // if lamp lock required then off lamp.
            {
              lamp_lock = 1;
              Set_Do(0);	
            }
          }	
        }

        if(lamp_current_low == 0)     // if perviously lamp current high and become low thne increase cyclic counter.
        {
          cycling_lamp_fault++;
          lamp_current_low = 1;
        }
      }
      else
      {												
        if((check_cycling_current_counter >= 1) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1)) // check cyclic current at every sec after current steady time over.
        {		
          check_current_counter = 0;
          low_current_counter = 0;							
          check_cycling_current_counter = 0;
          Per_Val = (float)Per_Val_Current/100;
          if((irdoub < (lamp_current * (1.0-Per_Val))) || (irdoub >= (lamp_current * (1.0+Per_Val))))	    // check if curret less then 20% or greter then 20%
          {
            if(lamp_current_low == 0)        // if current become law then increase counter
            {
              cycling_lamp_fault++;
              lamp_current_low = 1;
            }
            if(check_half_current_counter >= Lamp_Fault_Time)					   						// SLC low current trip.
            {
              half_current_counter++;
              if(half_current_counter >= Lamp_faulty_retrieve_Count)    // if current become law up to retrive count then declear Lamp fault.
              {
                error_condition.bits.b3 = 1;
                Lamp_Fault_Occured =1;
                half_current_counter = 0;
                check_half_current_counter = 0;
                if(Lamp_lock_condition == 1)            // if lamp lock condtion required then make lamp off
                {
                  lamp_lock = 1;
                  Set_Do(0);
                }
              }
              if(error_condition.bits.b3 == 0)
              {
                if(Lamp_lock_condition == 1)            // if lamp lock condtion required then make lamp off
                {
                  Set_Do(0);
                }	
              }
            }
          }
          else
          {
            if(lamp_current_low == 1)
            {
              cycling_lamp_fault++;
              lamp_current_low = 0;
            }
            check_half_current_counter = 0;
            half_current_counter = 0;
          }

          if(cycling_lamp_fault >= 10)        // if current become high to law or low to high 10 times then declere lamp cycling.
          {
            error_condition.bits.b6 = 1;
            if(Lamp_lock_condition == 1)        // if lamp lock condtion required then make lamp off
            {
              lamp_lock = 1;
              Set_Do(0);
            }
          }
          else
          {
            error_condition.bits.b6 = 0;
          }
        }
      }
    }
  }

  if((vrdoub < Vol_low) || (vrdoub > Vol_hi))													//Slc low/high voltage trip generate
  {
    if(check_counter >= 5)
    {
      error_condition.bits.b2 = 1;
      if(Lamp_lock_condition == 1)         // if lamp lock condtion required then make lamp off
      {
        Set_Do(0);
      }
    }
  }
  else
  {
    error_condition.bits.b2 = 0;
    check_counter = 0;
  }

  if(irdoub < current_creep_limit)
  {
    error_condition.bits.b0 = 0;												//SLC Lamp ON feedback.
    if(lamp_off_to_on == 1)
    {
      error_condition1.bits.b5 = 0;											// relay weld condition remove.	
      Relay_weld_timer = 0;
    }
  }
  else
  {
    error_condition.bits.b0 = 1;			 												//SLC Lamp ON feedback.
    if((lamp_off_to_on == 1) && (Relay_weld_timer > 3))
    {
      error_condition1.bits.b5 = 1;											// relay weld condition generated			
    }				
  }

}

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

/*************************************************************************
Function Name: check_photocell_interlock
input: None.
Output: None.
Discription: this function is use for checking all Photocell related
             interlocks like photocell faults, photocell ossilating.
*************************************************************************/
void check_photocell_interlock(void)
{
  if(Photo_Cell_Ok == 1)															// if photocell ok then only generate event trip or photocell status.
  {
    if(Photo_feedback == 1)
    {
      error_condition.bits.b5 = 1;
    }
    else
    {
      error_condition.bits.b5 = 0;
    }

    if(Commissioning_flag == 0)            // if SLC in commissioning mode then not check any photocell trip.
    {
      if(Photo_feedback == 1)     // check photocell high/low transection
      {
        if(photo_cell_toggel == 1)
        {
          photo_cell_timer = 0;
          photo_cell_toggel = 0;
          photo_cell_toggel_counter++;     // increase photocell toggel counter on every transection
        }
        else
        {
          photo_cell_toggel = 0;
        }
      }
      else
      {
        if(photo_cell_toggel == 0)
        {
          photo_cell_timer = 0;
          photo_cell_toggel = 1;
          photo_cell_toggel_counter++;     // increase photocell toggel counter on every transection
        }
        else
        {
          photo_cell_toggel = 1;
        }
      }

      if(photo_cell_timer >= Photocell_steady_timeout_Val)   // if photocell not change its status up to configured time then generate photocell fault trip.
      {
        Photo_Cell_Ok = 0;
        halCommonSetToken(TOKEN_Photo_Cell_Ok,&Photo_Cell_Ok);   // store photocell fault status in to NVM.
        Photo_Cell_error = 1;
        halCommonSetToken(TOKEN_Photo_Cell_error,&Photo_Cell_error);   // store photocell error in to NVM.
        error_condition.bits.b1 = 1;
      }
      else
      {
        error_condition.bits.b1 = 0;
      }
      if((Valid_DCU_Flag == 1)&&(Photocell_unsteady_timeout_Val>5)) //osscilation trip genrerate after valid dcu gets
      {
        if((photo_cell_toggel_counter >= photo_cell_toggel_counter_Val) &&
           (photo_cell_toggel_timer <= photo_cell_toggel_timer_Val))    // if photocell toggel more then configured count within a configured time then generate photocell ossilating trip.
         {
            photo_cell_toggel_timer = 0;
            photo_cell_toggel_counter = 0;
            error_condition.bits.b4 = 1;    											// photo_cell oscillating error.
            Photo_Cell_Ok = 0;
            halCommonSetToken(TOKEN_Photo_Cell_Ok,&Photo_Cell_Ok);   // store mode in to NVM.
            Photo_Cell_error = 2;
            halCommonSetToken(TOKEN_Photo_Cell_error,&Photo_Cell_error);   // store photocell error in to NVM.
         }
         else
         {
           if(photo_cell_toggel_timer > photo_cell_toggel_timer_Val)
           {
             photo_cell_toggel_timer = 0;
             photo_cell_toggel_counter = 0;
           }
         }
      }
      else
      {
             photo_cell_toggel_timer = 0;
             photo_cell_toggel_counter = 0;

      }
    }
  }
}

/*************************************************************************
Function Name: dimming_applied
input: None.
Output: None.
Discription: this function is use to apply dimming by PWM
*************************************************************************/
void dimming_applied(unsigned char dim_val)
{

  if(automode == PANIC_MODE)
  {
    dim_val = panic_dim;
  }

#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
  if(idimmer_en == 0)                 // check for idimmer enable.
  {
//#if defined(DALI_SUPPORT)
   if(dali_support == 1)
   {
    dim_val = 100 - dim_val;

    if(dim_inc_val > 0)
    {
      if((old_dim_val < dim_val) && (dim_inc_timer >= dim_inc_val))
      {
        dim_inc_timer = 0;

        dim_cycle	= ( 254 * (old_dim_val/100.0));		//SAN
				    DALI_DIMMING_CMD(dim_cycle);					//SAN

        old_dim_val++;
        if(old_dim_val > 100)
        {
          old_dim_val = 100;	
        }
      }
      else if((old_dim_val > dim_val) && (dim_inc_timer >= dim_inc_val))
      {
        dim_inc_timer = 0;

        dim_cycle	= ( 254 * (old_dim_val/100.0));		//SAN
				    DALI_DIMMING_CMD(dim_cycle);					//SAN

        old_dim_val--;
        if(old_dim_val < 1)
        {
          old_dim_val = 1;
        }	
      }
      else if((old_dim_val==dim_val) && (dim_inc_timer >= dim_inc_val))
      {
        dim_inc_timer = 0;

        dim_cycle	= ( 254 * (old_dim_val/100.0));		//SAN
				    DALI_DIMMING_CMD(dim_cycle);					//SAN
      }
      else
      {
      }
    }
    else
    {
        if(dimm_cmd_rec == 1)
        {
          dimm_cmd_rec=0;
          old_dim_val =dim_val+1;
        }	

        if((old_dim_val != dim_val)&&(dali_delay >=3)&&(set_do_flag ==1))	//SAN
        {
          dali_delay =0;
          old_dim_val = dim_val;
          dim_cycle = ( 254 * (dim_val/100.0));    //3199 for 32Mhz
          DALI_DIMMING_CMD(dim_cycle);
        }
    }
    Dimvalue =dim_val;	          // load current dim value in to Data string resistor.
    Dimvalue = 100 - Dimvalue;   // reverse value for disply proper.
   }
//#else
else
{
    dim_val = 100 - dim_val;          // 100% dim means 0V DC so need to make PWM 0 dutycycle
    if(dim_inc_val > 0)               // check if required step dimming or not?
    {
      if(old_dim_val < dim_val)     // increase dimming step by 1% at every configured time if current dim percentage is less then set dim percent.
      {

        if(dim_inc_timer != temp_dim_inc_val)
        {
          temp_dim_inc_val = dim_inc_timer;
          if((Dimming_Jump_Set == 0)||(dim_val!= pre_dim_val))
            {
              dim_inc_timer =0;
              pre_dim_val = dim_val;
                Dimming_Jump_Set = 1;
                old_dim_val_f=old_dim_val;
                Dimming_Jump = (float)(dim_val-old_dim_val)/dim_inc_val;
                temp_dim_inc_val =0;
            }

            if(dim_inc_timer >= dim_inc_val)
            {
                dim_inc_timer = 0;

            }
            old_dim_val_f = old_dim_val_f + Dimming_Jump;
            old_dim_val = (unsigned char)old_dim_val_f;

            if(old_dim_val >= dim_val)
            {
              old_dim_val = dim_val;
              Dimming_Jump_Set = 0;
            }
            duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));
            DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;                  // set dimming percentage value to PWM resistor
        }
      }
      else if(old_dim_val > dim_val)   // decrease dimming step by 1% at every configured time if current dim percentage is greater then set dim percent.
      {
////////////////////////

        if(dim_inc_timer != temp_dim_inc_val)
        {
              temp_dim_inc_val = dim_inc_timer;
            if((Dimming_Jump_Set == 0)||(dim_val!= pre_dim_val))
            {
              dim_inc_timer =0;
              pre_dim_val = dim_val;
              Dimming_Jump_Set =1;
              old_dim_val_f=old_dim_val;
              Dimming_Jump = (float)(old_dim_val-dim_val)/dim_inc_val;
              temp_dim_inc_val =0;
            }

            if(dim_inc_timer >= dim_inc_val)
            {
                dim_inc_timer = 0;
            }
            if(old_dim_val <= Dimming_Jump)
            {
              old_dim_val = dim_val;
            }
            else
            {

                old_dim_val_f = old_dim_val_f - Dimming_Jump;
                old_dim_val = (unsigned char)old_dim_val_f;
            }
            if(old_dim_val <= dim_val)
            {
              old_dim_val = dim_val;
              Dimming_Jump_Set = 0;
            }		
            duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));
            DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;                  // set dimming percentage value to PWM resistor
        }

      }
     // else if((old_dim_val==dim_val) && (dim_inc_timer >= dim_inc_val))
      else if(old_dim_val==dim_val)
      {
        Dimming_Jump_Set = 0;
        dim_inc_timer = 0;
        duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));
        DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;                   // set dimming percentage value to PWM resistor
      }
      else
      {
      }
    }
    else
    {
      duty_cycle	= ( PWM_INIT_VAL * (dim_val/100.0));
      DUTY_CYCLE=(dim_val*PWM_FREQ)/100;	
    }	
    Dimvalue =dim_val;	          // load current dim value in to Data string resistor.
    Dimvalue = 100 - Dimvalue;   // reverse value for disply proper.

}
//#endif
  }
  else
  {
    dim_val = 100 - dim_val;
    idimmer_dimming_applied(dim_val); // send dimming percentage value to idimmer by message.
    Dimvalue = 100 - dim_val;	
  }	
#else
  Dimvalue = 0;
#endif
}

/*************************************************************************
Function Name: adaptive_dimming
input: None.
Output: None.
Discription: this function is use to run day light harvesting logic based
             dimming
*************************************************************************/
void adaptive_dimming(void)
{
  unsigned char temp = 0;
  unsigned long int temp1 = 0;
//#ifndef ISLC_10
  idimmer_en =0;
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
  if(idimmer_en == 0)                 // check for idimmer enable.
  {
    if(dim_inc_timer >= dim_inc_val) // check for step dimming timer over
    {
      dim_inc_timer = 0;
      if(lamp_lumen <= (desir_lamp_lumen))     // if current lamp lumnet is less then desired lamp lumen then increase brightness of lamp.
      {

        temp1 = desir_lamp_lumen - lamp_lumen;  // take difference of desired brightness and current brightness.
        temp1 = temp1 * 100;			
        temp = (unsigned char)(temp1 / desir_lamp_lumen);   // calculate percentage of lumen difference.
        old_dim_val = temp;
        duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));  // change PWM to that percentage.
        DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;
      }
      else
      {
        old_dim_val = 0;
        duty_cycle	= ( PWM_INIT_VAL * (old_dim_val/100.0));
        DUTY_CYCLE=(old_dim_val*PWM_FREQ)/100;
      }			
      Dimvalue = old_dim_val;
      Dimvalue = 100 - Dimvalue;        // reverse value for disply proper.
    }
  }
  else
  {
    if(dim_inc_timer >= dim_inc_val)
    {
      dim_inc_timer = 0;
      if(lamp_lumen <= (desir_lamp_lumen))
      {

        temp1 = desir_lamp_lumen - lamp_lumen;
        temp1 = temp1 * 100;			
        temp = (unsigned char)(temp1 / desir_lamp_lumen);
        old_dim_val = temp;
      }
      else
      {
        old_dim_val = 0;
      }			
    }
    idimmer_dimming_applied(old_dim_val);     // send dimming value to idimmer.
    Dimvalue = 100 - old_dim_val;
  }
#endif
}	


/*************************************************************************
Function Name: Mix_MOD_Motion_Based_dimming
input: None.
Output: None.
Discription: this function is use to run Motion based dimming.
*************************************************************************/
void Mix_MOD_Motion_Based_dimming(void)
{
//#ifndef ISLC_10
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
  if((MOTION_DI1 == 1) && (Motion_pre_val == 1))      // check for Motion digital input and its pervious status.
  {										
    Motion_pre_val = 0;
 //   emberAfGuaranteedPrint("%p", "\r\nMotion high Pulse detect");
    if(Motion_Sensor_Type == 1)                       // sensor configured for High pulse?
    {                                                 // increase Motion detect counter if counter increase then pulse rate then motion detect.
      Motion_countr++;
      if(Motion_detected == 1)
      {
        if(Motion_continue_timer > Motion_Detect_Timeout)  // if previously motion detected and again motion detect after configured time then motion continue.
        {
          Motion_Continiue = 1;
          motion_dimming_timer = 0;
          Motion_detect_sec = 0;
        }	
      }	
    }
  }
  else if((MOTION_DI1 == 0) && (Motion_pre_val == 0))     // check for Motion digital input and its pervious status.
  {

    Motion_pre_val = 1;
//    emberAfGuaranteedPrint("%p", "\r\nMotion Low pulse detect");
    if(Motion_Sensor_Type == 0)                           // sensor configured for Low pulse?
    {
      Motion_countr++;                                    // increase Motion detect counter if counter increase then pulse rate then motion detect.
      if(Motion_detected == 1)
      {
        if(Motion_continue_timer > Motion_Detect_Timeout)
        {
          Motion_Continiue = 1;                           // if previously motion detected and again motion detect after configured time then motion continue.
          motion_dimming_timer = 0;
          Motion_detect_sec = 0;
        }	
      }	
    }
  }
  else if((MOTION_DI1 == 0) && (Motion_pre_val == 1) && (Motion_Sensor_Type == 0))
  {					
    if(Motion_half_sec == 1)          // check motion level at every half sec.
    {				
 //     emberAfGuaranteedPrint("%p", "\r\nMotion Low level detect");
      Motion_countr++;                // increase Motion detect counter if counter increase then pulse rate then motion detect.
      Motion_half_sec = 0;
      if(Motion_detected == 1)
      {
        if(Motion_continue_timer > Motion_Detect_Timeout)
        {
          Motion_Continiue = 1;         // if previously motion detected and again motion detect after configured time then motion continue.
          motion_dimming_timer = 0;
          Motion_detect_sec = 0;
        }	
      }
    }
  }
  else if((MOTION_DI1 == 1) && (Motion_pre_val == 0) && (Motion_Sensor_Type == 1))
  {					
    if(Motion_half_sec == 1)             // check motion level at every half sec.
    {				
//      emberAfGuaranteedPrint("%p", "\r\nMotion High level detect");
      Motion_countr++;                  // increase Motion detect counter if counter increase then pulse rate then motion detect.
      Motion_half_sec = 0;
      if(Motion_detected == 1)
      {
        if(Motion_continue_timer > Motion_Detect_Timeout)
        {
          Motion_Continiue = 1;                 // if previously motion detected and again motion detect after configured time then motion continue.
          motion_dimming_timer = 0;
          Motion_detect_sec = 0;
        }	
      }
    }
  }


  if(Motion_detected == 1)            // check if motion detected or not.
  {

    if(motion_dimming_timer < Motion_dimming_time)  // if detected and motion timer is not over then make lamp on and set dimming to configured value.
    {
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {	
          if(Mix_Mode_Motion_Dimming_Set == 1)
          {
            Mix_Mode_Motion_Dimming_Set =0;
            dimming_applied(Mix_Mode_Motion_Dimming_Value);
          }
          else
          {
            dimming_applied(Motion_dimming_percentage);
          }
        }						
      }
      if((motion_dimming_timer == 0) && (Motion_detect_sec == 0) && (motion_dimming_Broadcast_send == 0) && (Motion_Received_broadcast == 0))  // if motion detected then send message broadcast to other SLCs in a group.
      {
        motion_dimming_Broadcast_send = 1;
        motion_dimming_send = 1;
        motion_broadcast_timer = 0;	
        //iteDebugMsg("\n Motion broadcast send");
        //emberAfGuaranteedPrint("%p", "\r\nMotion broadcast send");
        //Motion_broadcast_Receive_counter++;
        motion_dimming_timer = 0;
        Motion_detect_sec = 0;
      }
      else if((Motion_Continiue == 1) &&
              (motion_broadcast_timer >= Motion_Rebroadcast_timeout))    // if second motion detected before motion timer over and broadcast timer over then send message for motion continue.
      {
        motion_dimming_Broadcast_send = 0;	
        motion_broadcast_timer = 0;	
        Motion_Continiue = 0;
        motion_dimming_timer = 0;
        Motion_detect_sec = 0;
        //iteDebugMsg("\n Motion broadcast re-send");
        //emberAfGuaranteedPrint("%p", "\r\nMotion broadcast re-send");
        //Motion_broadcast_Receive_counter++;						
      }
      else
      {
      }			
    }
    else
    {
      Motion_detected = 0;                      // if motion dimming timer over then reset flag for motion detect.
      motion_dimming_Broadcast_send = 0;
      //WriteDebugMsg("\n Motion dimming timer over");
    }			
  }					
  else if(Motion_detected_broadcast == 1)       // if SLC receive broadcast of motion detect then make lamp On with configured dimming value.
  {
    if(motion_dimming_timer <= Motion_dimming_time)
    {
      //			dimming_applied(Motion_dimming_percentage);							
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {		
          //dimming_applied(Motion_dimming_percentage);
          if(Mix_Mode_Motion_Dimming_Set == 1)
          {
            Mix_Mode_Motion_Dimming_Set =0;
            dimming_applied(Mix_Mode_Motion_Dimming_Value);
          }
          else
          {
            dimming_applied(Motion_dimming_percentage);
          }
        }						
      }
    }
    else
    {
      Motion_detected_broadcast = 0;							
    }	
  }
  else if(Motion_intersection_detected == 1)              // if intersectiond SLC receive broadcast of motion detect then make lamp On with configured dimming value.
  {
    if(motion_intersection_dimming_timer < Motion_dimming_time)
    {
      //			dimming_applied(Motion_dimming_percentage);							
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {		
          //dimming_applied(Motion_dimming_percentage);
          if(Mix_Mode_Motion_Dimming_Set == 1)
          {
            Mix_Mode_Motion_Dimming_Set =0;
            dimming_applied(Mix_Mode_Motion_Dimming_Value);
          }
          else
          {
            dimming_applied(Motion_dimming_percentage);
          }
        }						
      }
    }
    else
    {
      Motion_intersection_detected = 0;							
    }
  }
  else
  {
    if(Motion_normal_dimming_percentage != 100)        // if timer over for motion detected and normal dimming percentage is not configured to 100% then make lamp ON and change only dimming percentage.
    {
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {		
          dimming_applied(Motion_normal_dimming_percentage);
        }						
      }
    }
    else
    {
      Set_Do(0);       // if normal dimming percentage is set to 100% then make lamp off.
    }
  }
#else

  if(Motion_detected_broadcast == 1)
  {
    if(motion_dimming_timer <= Motion_dimming_time)
    {
      //			dimming_applied(Motion_dimming_percentage);							
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {		
          //dimming_applied(Motion_dimming_percentage);
          if(Mix_Mode_Motion_Dimming_Set == 1)
          {
            Mix_Mode_Motion_Dimming_Set =0;
            dimming_applied(Mix_Mode_Motion_Dimming_Value);
          }
          else
          {
            dimming_applied(Motion_dimming_percentage);
          }
        }						
      }
    }
    else
    {
      Motion_detected_broadcast = 0;							
    }	
  }
  else if(Motion_intersection_detected == 1)
  {
    if(motion_intersection_dimming_timer < Motion_dimming_time)
    {
      //			dimming_applied(Motion_dimming_percentage);							
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {		
          //dimming_applied(Motion_dimming_percentage);
          if(Mix_Mode_Motion_Dimming_Set == 1)
          {
            Mix_Mode_Motion_Dimming_Set =0;
            dimming_applied(Mix_Mode_Motion_Dimming_Value);
          }
          else
          {
            dimming_applied(Motion_dimming_percentage);
          }
        }						
      }
    }
    else
    {
      Motion_intersection_detected = 0;							
    }
  }
  else
  {
    //		dimming_applied(Motion_normal_dimming_percentage);		

    if(Motion_normal_dimming_percentage != 100)
    {
      Set_Do(1);
      check_dimming_protection();					
      if(lamp_lock == 0)
      {
        if((Start_dim_flag == 1) && (set_do_flag == 1))
        {		
          dimming_applied(Motion_normal_dimming_percentage);
        }						
      }
    }
    else
    {
      Set_Do(0);
    }
  }
#endif
}

/*************************************************************************
Function Name: idimmer_dimming_applied
input: Dimming value.
Output: None.
Discription: this function is use send dimming command to idimmer hardware.
*************************************************************************/
void idimmer_dimming_applied(unsigned char dim_val)
{
////#ifndef ISLC_10
//#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
//  if(dim_val != current_dimval)      // send dimming value to Idimmer by RF message.
//  {
//    if((Send_dimming_cmd >= 5) && (dimming_cmd_ack == 0) && (dimming_cmd_cnt <= 2))    // if ACk of dimming message not received then send message at every 6 sec.
//    {
//      dimming_cmd_cnt ++;
//      if(dimming_cmd_cnt >= 3) dimming_cmd_cnt = 4;
//      idimmer_dimval = dim_val;
//      send_idimmer_Val_frame = 1;
//      Send_dimming_cmd=0;
//    }
//    else if(dimming_cmd_ack == 1)
//    {
//      dimming_cmd_cnt=0;
//      dimming_cmd_ack=0;	
//      idimmer_trip_erase = 0;
//    }
//    else if((dimming_cmd_cnt == 4)&&(dimming_cmd_ack == 0))
//    {
//      dimming_cmd_cnt=0;
//      current_dimval = dim_val;
//      dimming_cmd_ack=0;
//      Send_dimming_cmd=0;
//    }
//  }
//
//  if(Send_dimming_cmd >= 600)
//  {
//    idimmer_dimval = dim_val;
//    send_idimmer_Val_frame = 1;
//    Send_dimming_cmd=0;
//    dimming_cmd_cnt=0;
//    current_dimval = dim_val;
//    dimming_cmd_ack=0;
//  }
//#endif
}

/*************************************************************************
Function Name: Check_day_burning_fault
input: None.
Output: None.
Discription: this function is use to check day burning like if lamp remain on in
             out of Astro schedule time then generate this trip.
*************************************************************************/
void Check_day_burning_fault(void)
{
  unsigned char n,m,i;
  unsigned char scF[20];	

  for(n=0;n<2;n++)
  {
    scF[n] = 0;
    if(Astro_sSch[n].bSchFlag)
    {
      if((Time.Hour >= Astro_sSch[n].cStartHour) &&
         (Time.Hour <= Astro_sSch[n].cStopHour)) // if RTC hour is in between start and stop hour then set schedule match flag.
        scF[n] = 1;
      if((Time.Hour == Astro_sSch[n].cStartHour) &&
         (Time.Min < Astro_sSch[n].cStartMin))   // if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.
        scF[n] = 0;
      if((Time.Hour == Astro_sSch[n].cStopHour) &&
         (Time.Min > Astro_sSch[n].cStopMin))     // if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  								
        scF[n] = 0;
    }
  }

  if((scF[0] == 1) || (scF[1] == 1))    // night time
  {
    error_condition1.bits.b7 = 0;			// day burning fault clear.
  }
  else
  {
    if(error_condition.bits.b0 == 1)
    {
      error_condition1.bits.b7 = 1;			// day burning fault detected.
    }
    else
    {
      error_condition1.bits.b7 = 0;			// day burning fault Clear.
    }		
  }	
}	

/*************************************************************************
Function Name: check_Mix_Mode_schedule
input: None.
Output: None.
Discription: this function is use to check mix-mode schedule an if schedule
             match copy that schedule in to run-mode schedule.
*************************************************************************/
void check_Mix_Mode_schedule(void)
{
  unsigned char i=0,j=0,temp,Calendar_sch_match = 0,n=0,temp_sch_flag = 0;
  long temp_sunset = 0,temp_sunrise = 0;
  unsigned char Sch_Date, Sch_Month;

  if(Schedule_offset == 1)        // check schedule offset configured?
  {
    Sch_Date = PDate.Date;       // copy offset date at schedule date.
    Sch_Month = PDate.Month;     // copy offset Month at schedule Month.
    Date.Week  = week_day(Date.Year,Sch_Date,Sch_Month);		// find week_day from year,date,month.
				
    if(Date.Week == 0)             // sunday    convert week_day in binary formate
    {
      day_sch_R.Val = 1;
    }
    else if(Date.Week == 1)			// monday         convert week_day in binary formate
    {
      day_sch_R.Val = 64;
    }
    else if(Date.Week == 2)        // tuesday    convert week_day in binary formate
    {
      day_sch_R.Val = 32;
    }
    else if(Date.Week == 3)			// wenesday         convert week_day in binary formate
    {
      day_sch_R.Val = 16;
    }
    else if(Date.Week == 4)			// thrusday        convert week_day in binary formate
    {
      day_sch_R.Val = 8;
    }
    else if(Date.Week == 5)			// friday           convert week_day in binary formate
    {
      day_sch_R.Val = 4;
    }
    else if(Date.Week == 6)			// seturday         convert week_day in binary formate
    {
      day_sch_R.Val = 2;
    }
    else
    {
    }
  }
  else
  {
    Sch_Date = Date.Date;       // copy RTC_date on to schedule date
    Sch_Month = Date.Month;	    // copy RTC_Month on to schedule date
  }

/* clear all result variables before schedule check */
  run_Mix_Mode_sch = 0;
  Mix_photo_override = 0;

  for(n=0;n<9;n++)
  {
    Mix_sSch[n].bSchFlag = 0;
  }
/* clear all result variables before schedule check */

  for(j=0;j<5;j++)              // check special day schedule first (5 location)
  {

    unCompress_Mix_sch(j);      // we have stored mix mode schedule in compress form so uncompress schedule first.

    if(Slc_Mix_Mode_schedule[0].SLC_Mix_En == 1)      // if mix mode schedule flag enable then check othere location.
    {
      if(Sch_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month)  // for special day only check date and month of schedule.
      {
        if(Sch_Date == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date)
        {					
          run_Mix_Mode_sch = 1;        // if date and month match then set flag for schedule check.
          Calendar_sch_match = 1;
          Mix_photo_override = Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override;
          for(i=0;i<9;i++)             // check internal 9 schedule location.
          {
            if(Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].bSchFlag == 1)        // if internal schedule enable then check all field of internal schedule.
            {
              Mix_sSch[i].bSchFlag = 1;

              if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 24) &&
                 (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 24))  // if start and stop time is 24 then generate astro schedule time and copy on to run schedule location.
              {
                Civil_Twilight = 0;
                temp_sunset = Calculate(SUNSET);
                temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);	
                Mix_sSch[i].cStartHour = temp_sunset / 3600;
                Mix_sSch[i].cStartMin = (temp_sunset % 3600)/60;
              }
              else if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 26) &&
                 (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 26))    // if start and stop time is 26 then generate civil twilight schedule time and copy on to run schedule location.
              {
                Civil_Twilight = 1;
                temp_sunset = Calculate(SUNSET);
                temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);	
                Mix_sSch[i].cStartHour = temp_sunset / 3600;
                Mix_sSch[i].cStartMin = (temp_sunset % 3600)/60;
              }
              else
              {
                Mix_sSch[i].cStartHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour;
                Mix_sSch[i].cStartMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin;
              }


              if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 25) &&
                 (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 25))    // if start and stop time is 25 then generate astro schedule time and copy on to run schedule location.
              {
                Civil_Twilight = 0;
                temp_sunrise = Calculate(SUNRISE);
                temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);	
                Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                Mix_sSch[i].cStopMin = (temp_sunrise % 3600)/60;
                //   emberAfGuaranteedPrint("%p", "ASTRO");
              }
              else if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 27) &&
                 (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 27))    // if start and stop time is 28 then generate civil twilight schedule time and copy on to run schedule location.
              {
                Civil_Twilight = 1;
                temp_sunrise = Calculate(SUNRISE);
                temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);	
                Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                Mix_sSch[i].cStopMin = (temp_sunrise % 3600)/60;
              }
              else
              {
                Mix_sSch[i].cStopHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour;   // if no astro or civil twilight then copy mix-mode schedule to run schedule location.
                Mix_sSch[i].cStopMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin;
              }
              Mix_sSch[i].dimvalue = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].dimvalue;    // copy dim percentage on to run schedule location.
              Mix_sSch[i].Dim_Mode = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].Dim_Mode;    // copy dim Mode on to run schedule location.
            }
            else
            {
              Mix_sSch[i].bSchFlag = 0;	  // disable run mode schedule.
            }			
          }
          break;
        }
      }
    }	
  }	

  temp_sch_flag = 0;
  if(Calendar_sch_match == 0)      // if no any special day schedule match then check monthly schedule.
  {
//    for(j=5;j<15;j++)  //removed lasty five mix mode schedule
     for(j=5;j<10;j++)
    {

      unCompress_Mix_sch(j);            // we have stored mix mode schedule in compress form so uncompress schedule first.
      if(Slc_Mix_Mode_schedule[0].SLC_Mix_En == 1)
      {
        if((Sch_Month >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month) &&
           (Sch_Month <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month))
        {
          if(Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month) // if schedule start month and stop month not mathc then check logic for month checking.
          {
            if(((Sch_Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) &&
                (Sch_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month)) ||
               ((Sch_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month) &&
                (Sch_Month != Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)) ||
                 ((Sch_Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date) &&
                  (Sch_Month == Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month)))       // if RTC date and month in between schedule start,motnh and stop date,moth then run schedule.
            {
              temp_sch_flag = 1;
            }
            else
            {
              temp_sch_flag = 0;
            }
          }
          else
          {
            if((Sch_Date >= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date) &&
               (Sch_Date <= Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date))      // if RTC Date is greaterthen schedule month then run schedule.
            {
              temp_sch_flag = 1;
            }
            else
            {
              temp_sch_flag = 0;
            }
          }

          if(temp_sch_flag == 1)
          {
            temp = check_mix_mode_week_day(Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay);  // check current weekday is match with schedule week day if yes then check other field.
            if(temp == 1)
            {
              run_Mix_Mode_sch = 1;     // set falg for run mix mode schedule.
              Mix_photo_override = Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override;  // copy photocell override falg for run schedule.
              for(i=0;i<9;i++)
              {
                if(Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].bSchFlag == 1)  // if internal schedule flag enable then check other field of mix-mod schedule.
                {
                  Mix_sSch[i].bSchFlag = 1;

                  if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 24) &&
                     (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 24))   // if start and stop time is 24 then generate astro schedule time and copy on to run schedule location.
                  {
                    Civil_Twilight = 0;
                    temp_sunset = Calculate(SUNSET);
                    temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);	
                    Mix_sSch[i].cStartHour = temp_sunset / 3600;
                    Mix_sSch[i].cStartMin = (temp_sunset % 3600)/60;
                  }

                  else if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour == 26) &&
                          (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin == 26))  // if start and stop time is 26 then generate civil twilight schedule time and copy on to run schedule location.
                  {
                    Civil_Twilight = 1;
                    temp_sunset = Calculate(SUNSET);
                    temp_sunset = temp_sunset + (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff * 60);	
                    Mix_sSch[i].cStartHour = temp_sunset / 3600;
                    Mix_sSch[i].cStartMin = (temp_sunset % 3600)/60;	
                  }
                  else
                  {
                    Mix_sSch[i].cStartHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour;
                    Mix_sSch[i].cStartMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin;
                  }


                  if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 25) &&
                     (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 25))   // if start and stop time is 25 then generate astro schedule time and copy on to run schedule location.
                  {
                    Civil_Twilight = 0;
                    temp_sunrise = Calculate(SUNRISE);
                    temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);	
                    Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                    Mix_sSch[i].cStopMin = (temp_sunrise % 3600)/60;
                  }
                  else if((Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour == 27) &&
                          (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin == 27))  // if start and stop time is 28 then generate civil twilight schedule time and copy on to run schedule location.
                  {
                    Civil_Twilight = 1;
                    temp_sunrise = Calculate(SUNRISE);
                    temp_sunrise = temp_sunrise - (Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff * 60);	
                    Mix_sSch[i].cStopHour = temp_sunrise / 3600;
                    Mix_sSch[i].cStopMin = (temp_sunrise % 3600)/60;
                  }
                  else
                  {
                    Mix_sSch[i].cStopHour = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour;      // if no astro or civil twilight then copy mix-mode schedule to run schedule location.
                    Mix_sSch[i].cStopMin = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin;
                  }
                  Mix_sSch[i].dimvalue = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].dimvalue;      // copy dim percentage on to run schedule location.
                  Mix_sSch[i].Dim_Mode = Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].Dim_Mode;      // copy dim Mode on to run schedule location.
                }
                else
                {
                  Mix_sSch[i].bSchFlag = 0;	
                }			
              }
              break;
            }
          }
        }
      }	
    }
  }		
}

/*************************************************************************
Function Name: Run_Mix_Mode_Sch
input: None.
Output: None.
Discription: this function is use to run logic from mix-mode matched schedule.
*************************************************************************/
void Run_Mix_Mode_Sch(void)
{
  unsigned char n,m,i,break_flag = 0;
  unsigned char scF;

  if(run_Mix_Mode_sch == 1)      // if mix-mode schedule location mathc with current RTC date,month and enable schedule flag then check other location.
  {
    for(n=0;n<9;n++)
    {
      scF = 0;
      if(Mix_sSch[n].bSchFlag)			// if schedule flag enable then check time of schedule.
      {
        if((Time.Hour >= Mix_sSch[n].cStartHour) && (Time.Hour <= Mix_sSch[n].cStopHour))       // if RTC hour is in between start and stop hour then run schedule.
          scF = 1;
        if((Time.Hour == Mix_sSch[n].cStartHour) && (Time.Min < Mix_sSch[n].cStartMin))         // if RTC hour match to start hour and RTC minute is less then start minute then stop schedule.  																	
          scF = 0;
        if((Time.Hour == Mix_sSch[n].cStopHour) && (Time.Min >= Mix_sSch[n].cStopMin))		        // if RTC hour match to stop hour and RTC minute is greater then stop minute then stop schedule.  															
          scF = 0;
        if(((Time.Hour == Mix_sSch[n].cStopHour) && (Mix_sSch[n].cStopHour == 23)) && ((Time.Min == Mix_sSch[n].cStopMin) && (Mix_sSch[n].cStopMin == 59)))		// if RTC time is 23:59 then start schedule.
          scF = 1;

        if(scF == 1)  // if schedule match then check dimming percentage and dimming mode
        {
          if(Mix_sSch[n].Dim_Mode == 0)          // normal dimming
          {
            if(Mix_sSch[n].dimvalue == 0)        // if dimmminng value is 0% then make lamp On with full brightness.
            {
              Set_Do(1);	
              dimming_applied(0);
            }
            else if(Mix_sSch[n].dimvalue == 100)        // if dimmminng value is 100% then make lamp OFF.
            {
              Set_Do(0);
              dimming_applied(100);
            }
            else if((Mix_sSch[n].dimvalue > 0) && (Mix_sSch[n].dimvalue < 100))  // if dimmminng value is in between 0-100 then make lamp ON with configured brightness.
            {
              Set_Do(1);
              check_dimming_protection();
              if((Start_dim_flag == 1) && (set_do_flag == 1))
              {
                dimming_applied(Mix_sSch[n].dimvalue);
              }	
            }
            else
            {
            }
          }
          else if(Mix_sSch[n].Dim_Mode == 3)
          {	

            Mix_Mode_Motion_Dimming_Set=1;
            Mix_Mode_Motion_Dimming_Value=Mix_sSch[n].dimvalue;
            Mix_MOD_Motion_Based_dimming();   // check motion based dimming.

          }
          else if(Mix_sSch[n].Dim_Mode == 2)  // Day lighting Harvesting dimming
          {
            Set_Do(1);
            check_dimming_protection();					
            if(lamp_lock == 0)
            {
              if((Start_dim_flag == 1) && (set_do_flag == 1))
              {
                adaptive_dimming();
              }
            }	

          }
          break_flag = 1;
          break;					
        }	
      }	
    }			
  }

  if(break_flag == 0)      // if schedule not match then check photocell override condition
  {
    if(Mix_photo_override == 1)
    {
      if((Cloudy_Flag == 1) || ((Photo_feedback == 0) && (Photo_Cell_Ok == 1)))  // if cloudy condition arived or photocell gives status for dark then make lamp on else off.
      {
        Set_Do(1);
        dimming_applied(0);
      }
      else
      {
        Set_Do(0);
      }		
    }
    else
    {
      Set_Do(0);	
    }		
  }		
}

/*************************************************************************
Function Name: check_Day_Light_Saving
input: None.
Output: None.
Discription: this function is use to check day light saveing rule and if RTC
             time in between rule then shift colck according to timezone offset.
*************************************************************************/
void check_Day_Light_Saving(void)
{
  unsigned char DST_On = 0;
//  unsigned char Display[50];
  int temp;
  char temp_hour,temp_min;

  if(SLC_DST_En == 1)       // check if DST enable?
  {

    if(old_year != Date.Year) // if year change then find new date from DST Rule.
    {
      if(SLC_DST_Rule_Enable == 1)
      {
        SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule,SLC_DST_Start_Month);
        SLC_DST_R_Stop_Date =  find_dst_date_from_rule(SLC_DST_Stop_Rule,SLC_DST_Stop_Month);
      }
      else
      {
        SLC_DST_R_Start_Date = SLC_DST_Start_Date;
        SLC_DST_R_Stop_Date =  SLC_DST_Stop_Date;
      }
      old_year = Date.Year;
    }

    if(SLC_DST_Start_Month <= SLC_DST_Stop_Month)              // first check like start month is always less then stop month.
    {
      if((Date.Month >= SLC_DST_Start_Month) && (Date.Month <= SLC_DST_Stop_Month))  // if rtc moth is greater then start month and less then stop month then check other field.
      {
        if((SLC_DST_Start_Month == SLC_DST_Stop_Month) &&
           (SLC_DST_R_Start_Date > SLC_DST_R_Stop_Date)) // in same moth if configured start date is greater then stop date then wrong rule so no need to check any thing and make DST off.
        {
          DST_On = 0;              // DST date not configured properly.
        }
        else
        {
          if(SLC_DST_Start_Month != SLC_DST_Stop_Month)  // if start month is not same as stop month then check date checking.
          {
            if(Date.Month == SLC_DST_Start_Month) // if RTC month is same as start moth and RTC date is same as start date amd RTC hour is greater then start hour then start DST.
            {							
              if((Date.Date == SLC_DST_R_Start_Date))
              {	
                if((Time.Hour >= SLC_DST_Start_Time))
                {
                  DST_On = 1;
                } 	
                else
                {
                  DST_On = 0;
                }
              }
              else if((Date.Date > SLC_DST_R_Start_Date)) // if RTC date is greater then start date then start DST.
              {
                DST_On = 1;
              }
              else
              {
                DST_On = 0;
              }							

            }
            else if(Date.Month == SLC_DST_Stop_Month)   // if rtc month is equal to stop month then check other field.
            {
              if(Date.Date == SLC_DST_R_Stop_Date)  // if RTC date is same as stop date and RTC hour is less then stop hour then start DST.
              {
                if(Time.Hour < SLC_DST_Stop_Time)
                {
                  DST_On = 1;
                }
                else
                {
                  DST_On = 0;
                }
              }
              else if(Date.Date < SLC_DST_R_Stop_Date)  // if RTC date is less then stop date then start DST.
              {
                DST_On = 1;
              }
              else
              {
                DST_On = 0;
              }
            }
            else if((Date.Month > SLC_DST_Start_Month) && (Date.Month < SLC_DST_Stop_Month))  // if RTC month is in between start month and stop month then start DST.
            {
              DST_On = 1;
            }
            else
            {
              DST_On = 0;	
            }
          }
          else
          {

            if(SLC_DST_R_Start_Date != SLC_DST_R_Stop_Date)  // if start month and stop month is same and start date and stop date is same then check other field.
            {
              if((Date.Date == SLC_DST_R_Start_Date)) // if RTC Date is equal to start date and RTC hour is greaterthen start time then start DST.
              {	
                if((Time.Hour >= SLC_DST_Start_Time))
                {
                  DST_On = 1;
                } 	
                else
                {
                  DST_On = 0;
                }
              }							
              else if(Date.Date == SLC_DST_R_Stop_Date)  // if start date and stop date is same then check only time.
              {
                if(Time.Hour < SLC_DST_Stop_Time) // if RTC time is less then stop time then start DST.
                {
                  DST_On = 1;
                }
                else
                {
                  DST_On = 0;
                }
              }
              else if((Date.Date > SLC_DST_R_Start_Date) && (Date.Date < SLC_DST_R_Stop_Date)) // if RTC date is in between start date and stop date then start DST.
              {
                DST_On = 1;
              }
              else
              {
                DST_On = 0;
              }

            }
            else
            {
              if((Time.Hour >= SLC_DST_Start_Time) &&
                 (Time.Hour < SLC_DST_Stop_Time)&&
                   (Date.Date == SLC_DST_R_Start_Date))  // if star and stop date are same and RTC time is in between start and stop time then start DST.
              {
                DST_On = 1;
              } 	
              else
              {
                DST_On = 0;
              }
            }

          }
        }
      }
      else
      {
        DST_On = 0;                              // RTC month not match with Start month and stop month then DST not match.
      }
    }
    else
    {
////////////////////////reverse DST checking//////////////////////////////////////////
      if((Date.Month <= SLC_DST_Stop_Month) || (Date.Month >= SLC_DST_Start_Month))     // if start month is greater then stop month then check reverse condition like if RTC month is in between stop month and start month then DST stop else DST Start.
      {
          if(SLC_DST_Start_Month != SLC_DST_Stop_Month)       // check if start month and stop month is not same.
          {
            if(Date.Month == SLC_DST_Start_Month)
            {							
              if((Date.Date == SLC_DST_R_Start_Date))
              {	
                if((Time.Hour >= SLC_DST_Start_Time))
                {
                  DST_On = 1;                         // if RTC date and month are same as start date and month and RTC Hour is greater then start hour then start DST.
                } 	
                else
                {
                  DST_On = 0;
                }
              }
              else if((Date.Date > SLC_DST_R_Start_Date))  // if date is greater then start date then start DST.
              {
                DST_On = 1;
              }
              else
              {
                DST_On = 0;
              }							

            }
            else if(Date.Month == SLC_DST_Stop_Month)    // if rtc month is match with stop month and RTC date is mathc with stop date and rtc hour is less then stop hour then start DST.
            {
              if(Date.Date == SLC_DST_R_Stop_Date)
              {
                if(Time.Hour < SLC_DST_Stop_Time)
                {
                  DST_On = 1;
                }
                else
                {
                  DST_On = 0;
                }
              }
              else if(Date.Date < SLC_DST_R_Stop_Date)   // if RTC date is less then stop date then start DST.
              {
                DST_On = 1;
              }
              else
              {
                DST_On = 0;
              }
            }
            else if((Date.Month > SLC_DST_Start_Month) || (Date.Month < SLC_DST_Stop_Month)) // if RTC month is in between start and stop month then start DST.
            {
              DST_On = 1;
            }
            else
            {
              DST_On = 0;	
            }
          }
          else
          {
            DST_On = 0;
          }
      }
      else
      {
        DST_On = 0;                              // RTC month not match with Start month and stop month then DST not match.
      }
////////////////////////////reverse DST checking/////////////////////////////////////
    }		

    if(DST_On == 1)
    {
      emberAfCorePrintln("\r\nDST Match");
    }
    else
    {
      emberAfCorePrintln("\r\nDST Note Match");
    }

    if((DST_On == 1) && (Time_change_dueto_DST == 0))  // if DST start and time not change then add time zone diff in to local time.
    {
      Time_change_dueto_DST = 1;
      //WriteByte_EEPROM(EE_Time_change_dueto_DST,Time_change_dueto_DST);
      halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
      emberAfCorePrintln("\r\nTime change due to DST");

      if(SLC_DST_Time_Zone_Diff > 0)     // if time zone diff is positive then add diff
      {
        temp = (unsigned char)SLC_DST_Time_Zone_Diff;   // convert time zone diff in to hour and min.
        temp_hour = temp / 60;
        temp_min  = temp % 60;
        Time.Min = Time.Min + temp_min; // add min to RTC min.
        if(Time.Min >= 60)       // if min greater then 59 then add hour in to RTC Hour.
        {
          Time.Min = Time.Min - 60;
          Time.Hour = Time.Hour + 1;
        }
        Time.Hour = Time.Hour + temp_hour;
        if(Time.Hour >= 24)       // if RTC hour greater then 23 then increase RTC Date.
        {
          Time.Hour = Time.Hour - 24;
          Increment_Date();
        }
        RTC_SET_TIME(Time);      // Set RTC time.

        Local_Time.Hour = Time.Hour;																										
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        GetSCHTimeFrom_LAT_LOG();  // compute Astro time from new RTC time.
        Astro_sSch[0].cStartHour = sunsetTime/3600;				
        Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
        Astro_sSch[0].cStopHour  = 23;							
        Astro_sSch[0].cStopMin   = 59;							
        Astro_sSch[0].bSchFlag   = 1;							

        Astro_sSch[1].cStartHour = 00;							
        Astro_sSch[1].cStartMin  = 00;							
        Astro_sSch[1].cStopHour  = sunriseTime/3600;			
        Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;										
        Astro_sSch[1].bSchFlag   = 1;							
      }
      else          // if time zone diff is negative then subtrect diff.
      {
        temp = (signed char)SLC_DST_Time_Zone_Diff;   // convert time zone diff in to hour and min.
        temp_hour = temp / 60;
        temp_min  = temp % 60;

        Time.Min = Time.Min - temp_min; // subtract min from RTC min.
        if(Time.Min < 0)
        {
          Time.Min = Time.Min + 60;        // if min lessthen 0 then subtract hour from RTC hour.
          Time.Hour = Time.Hour - 1;
        }
        Time.Hour = Time.Hour - temp_hour;	// subtract RTC hour.
        if(Time.Hour < 0)
        {
          Time.Hour = Time.Hour + 24;            // if hour < 0 then decrement date from RTC Date.
          Decrement_Date();
        }
        			
        RTC_SET_TIME(Time);                // Set RTC new time.

        Local_Time.Hour = Time.Hour;																										
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        GetSCHTimeFrom_LAT_LOG();          // compute Astro time from new RTC time.

        Astro_sSch[0].cStartHour = sunsetTime/3600;				
        Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
        Astro_sSch[0].cStopHour  = 23;							
        Astro_sSch[0].cStopMin   = 59;							
        Astro_sSch[0].bSchFlag   = 1;							

        Astro_sSch[1].cStartHour = 00;							
        Astro_sSch[1].cStartMin  = 00;							
        Astro_sSch[1].cStopHour  = sunriseTime/3600;			
        Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
        Astro_sSch[1].bSchFlag   = 1;							
      }

    }

    if((DST_On == 0) && (Time_change_dueto_DST == 1))    // if DST over then roll back RTC time.
    {
      Time_change_dueto_DST = 2;

      halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
      emberAfCorePrintln("\r\nTime Revert back from DST");
      if(SLC_DST_Time_Zone_Diff > 0)         // subtract time zone diff from current time.
      {
        temp = (unsigned char)SLC_DST_Time_Zone_Diff;
        temp_hour = temp / 60;
        temp_min  = temp % 60;
        Time.Min = Time.Min - (int)temp_min;
        if(Time.Min < 0)
        {
          Time.Min = Time.Min + 60;
          Time.Hour = Time.Hour - 1;
        }
        Time.Hour = Time.Hour - temp_hour;	
        if(Time.Hour < 0)
        {
          Time.Hour = Time.Hour + 24;
          Decrement_Date();
        }
      																											
        RTC_SET_TIME(Time);     // Set RTC time.

        Local_Time.Hour = Time.Hour;																										
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        GetSCHTimeFrom_LAT_LOG();   // compute Astro time from new time.
        Astro_sSch[0].cStartHour = sunsetTime/3600;				
        Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
        Astro_sSch[0].cStopHour  = 23;							
        Astro_sSch[0].cStopMin   = 59;							
        Astro_sSch[0].bSchFlag   = 1;							

        Astro_sSch[1].cStartHour = 00;							
        Astro_sSch[1].cStartMin  = 00;						
        Astro_sSch[1].cStopHour  = sunriseTime/3600;			
        Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;								
        Astro_sSch[1].bSchFlag   = 1;							
      }
      else      // add time zone diff to RTC time.
      {
        temp = (signed char)SLC_DST_Time_Zone_Diff;
        temp_hour = temp / 60;
        temp_min  = temp % 60;

        Time.Min = Time.Min + temp_min;
        if(Time.Min >= 60)
        {
          Time.Min = Time.Min - 60;
          Time.Hour = Time.Hour + 1;
        }
        Time.Hour = Time.Hour + temp_hour;
        if(Time.Hour >= 24)
        {
          Time.Hour = Time.Hour - 24;
          Increment_Date();
        }

        RTC_SET_TIME(Time);   // Set RTC Time

        Local_Time.Hour = Time.Hour;																										
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        GetSCHTimeFrom_LAT_LOG(); // Compute Astro time from RTC new time.
        Astro_sSch[0].cStartHour = sunsetTime/3600;				
        Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
        Astro_sSch[0].cStopHour  = 23;							
        Astro_sSch[0].cStopMin   = 59;							
        Astro_sSch[0].bSchFlag   = 1;							

        Astro_sSch[1].cStartHour = 00;							
        Astro_sSch[1].cStartMin  = 00;							
        Astro_sSch[1].cStopHour  = sunriseTime/3600;			
        Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
        Astro_sSch[1].bSchFlag   = 1;							
      }																										
    }			
  }
  else         // if DST disable from application and SLC running in DST then roll back time.
  {
    if((Time_change_dueto_DST == 1))
    {
      Time_change_dueto_DST = 0;

      halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
      emberAfCorePrintln("\r\nTime Revert back from DST");
      if(SLC_DST_Time_Zone_Diff > 0)      // if time zone diff is positive then subtract diff from RTC time.
      {
        temp = (unsigned char)SLC_DST_Time_Zone_Diff;
        temp_hour = temp / 60;
        temp_min  = temp % 60;

        Time.Min = Time.Min - temp_min;
        if(Time.Min < 0)
        {
          Time.Min = Time.Min + 60;
          Time.Hour = Time.Hour - 1;
        }
        Time.Hour = Time.Hour - temp_hour;
        if(Time.Hour < 0)
        {
          Time.Hour = Time.Hour + 24;
          Decrement_Date();
        }
        				
        RTC_SET_TIME(Time);       // Set RTC new time.

        Local_Time.Hour = Time.Hour;																										
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        GetSCHTimeFrom_LAT_LOG(); // Compute Astro time from new time.
        Astro_sSch[0].cStartHour = sunsetTime/3600;				
        Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
        Astro_sSch[0].cStopHour  = 23;						
        Astro_sSch[0].cStopMin   = 59;							
        Astro_sSch[0].bSchFlag   = 1;							

        Astro_sSch[1].cStartHour = 00;							
        Astro_sSch[1].cStartMin  = 00;							
        Astro_sSch[1].cStopHour  = sunriseTime/3600;			
        Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;										
        Astro_sSch[1].bSchFlag   = 1;							
      }
      else     // if time zone diff is negative then add diff in to time.
      {
        temp = (signed char)SLC_DST_Time_Zone_Diff;
        temp_hour = temp / 60;
        temp_min  = temp % 60;

        Time.Min = Time.Min + temp_min;
        if(Time.Min >= 60)
        {
          Time.Min = Time.Min - 60;
          Time.Hour = Time.Hour + 1;
        }
        Time.Hour = Time.Hour + temp_hour;
        if(Time.Hour >= 24)
        {
          Time.Hour = Time.Hour - 24;
          Increment_Date();
        }

        RTC_SET_TIME(Time);             // set RTC new time.

        Local_Time.Hour = Time.Hour;																										
        Local_Time.Min = Time.Min;
        Local_Time.Sec = Time.Sec;
        Local_Date.Date = Date.Date;
        Local_Date.Month = Date.Month;
        Local_Date.Year = Date.Year;

        GetSCHTimeFrom_LAT_LOG();       // compute Astro time from new time.
        Astro_sSch[0].cStartHour = sunsetTime/3600;				
        Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
        Astro_sSch[0].cStopHour  = 23;							
        Astro_sSch[0].cStopMin   = 59;							
        Astro_sSch[0].bSchFlag   = 1;							

        Astro_sSch[1].cStartHour = 00;							
        Astro_sSch[1].cStartMin  = 00;							
        Astro_sSch[1].cStopHour  = sunriseTime/3600;			
        Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
        Astro_sSch[1].bSchFlag   = 1;							
      }

    }		
  }			
}


/*************************************************************************
Function Name: find_dst_date_from_rule
input: occurance and occurance of month.
Output: None.
Discription: this function is use to find date of rule like if configured second
             monday of february then this fnction finds date of this occurance.
*************************************************************************/
unsigned char find_dst_date_from_rule(unsigned char occurance,unsigned char occurance_month)
{
  unsigned char occ_day = 0,occ_weekday = 0,i = 0,find_date = 0,N = 0;
  unsigned char temp_week_day = 0,temp_occ_day = 0;

  // variable 21 is equal to second monday.

  occ_day = occurance / 10;     // find occurance day and occurance week day form start rule variable.
  occ_weekday = occurance % 10;



  /* find no of days in to given month */
		if((occurance_month==1) || (occurance_month==3) || (occurance_month==5) || (occurance_month==7) || (occurance_month==8) || (occurance_month==10) || (occurance_month==12) )
		{
    N=31;
		}
		else if((occurance_month==4) || (occurance_month==6) || (occurance_month==9) || (occurance_month==11))
		{
    N=30;
		}		
		
		if(occurance_month==2)
		{
    if(Date.Year%4==0)
      N=29;
    else
      N=28;
		}


  /* count occurance of that day from week_day find function */

		for(i=1;i<=N;i++)
		{
    temp_week_day = week_day(Date.Year,i,occurance_month);
    if(temp_week_day == occ_weekday)
    {
      temp_occ_day++;
      if(temp_occ_day == occ_day)   // if occurance match then return that day.
      {
        find_date = i;
        break;
      }
    }
		}

		if(find_date == 0)        // in some month there is no occurce of 5th saturday or suncay in this case return 4th occurance.
		{
    occ_day = occ_day - 1;
    temp_occ_day = 0;
    for(i=1;i<=N;i++)
    {
      temp_week_day = week_day(Date.Year,i,occurance_month);
      if(temp_week_day == occ_weekday)
      {
        temp_occ_day++;
        if(temp_occ_day == occ_day)
        {
          find_date = i;        // if occurance match then return that day.
          break;
        }
      }
    }
		}
		
		return find_date;
}

/*************************************************************************
Function Name: check_mix_mode_week_day
input: None.
Output: result.
Discription: this function is use comapre RTC week day to schedule week day if
             week day match then return 1 else return 0.
*************************************************************************/
unsigned char check_mix_mode_week_day(unsigned char sch_weekday)
{
  unsigned char i = 0;
  day_sch.Val = sch_weekday;

  if(((day_sch.bits.b0 == day_sch_R.bits.b0) && (day_sch_R.bits.b0 == 1)) ||
     ((day_sch.bits.b1 == day_sch_R.bits.b1) && (day_sch_R.bits.b1 == 1)) ||
       ((day_sch.bits.b2 == day_sch_R.bits.b2) && (day_sch_R.bits.b2 == 1)) ||
         ((day_sch.bits.b3 == day_sch_R.bits.b3) && (day_sch_R.bits.b3 == 1)) ||	
           ((day_sch.bits.b4 == day_sch_R.bits.b4) && (day_sch_R.bits.b4 == 1)) ||	
             ((day_sch.bits.b5 == day_sch_R.bits.b5) && (day_sch_R.bits.b5 == 1)) ||
               ((day_sch.bits.b6 == day_sch_R.bits.b6) && (day_sch_R.bits.b6 == 1)))     // check bit of variable for match current day with schedule day b0 - sunday.
  {
    return 1;
  }
  else
  {
    return 0;		
  }

}				

/*************************************************************************
Function Name: Increment_Date
input: None.
Output: None.
Discription: this function is use increment current date and if due to this
             change in month and year then do it.
*************************************************************************/
void Increment_Date(void)
{
  unsigned char occ_day = 0,occ_weekday = 0,i = 0,find_date = 0,N = 0;
  unsigned char temp_week_day = 0,temp_occ_day = 0;

  /* find number of days in to given month */
		if((Date.Month==1) || (Date.Month==3) || (Date.Month==5) || (Date.Month==7) || (Date.Month==8) || (Date.Month==10) || (Date.Month==12) )
		{
    N=31;
		}
		else if((Date.Month==4) || (Date.Month==6) || (Date.Month==9) || (Date.Month==11))
		{
    N=30;
		}		
		
		if(Date.Month==2)     // check for february
		{
    if(Date.Year%4==0)  // check for leap year
      N=29;
    else
      N=28;
		}
  /* find number of days in to given month */



	 if(Date.Date == N)  // if RTC date is last date of given month then make date = 1 and increase month.
  {
        Date.Date = 1;
        Date.Month = Date.Month + 1;
        if(Date.Month > 12) // if month is last month then increase year and make month = 1.
        {
          Date.Month = 1;
          Date.Year = Date.Year + 1;
        }
  }
  else
  {
    Date.Date = Date.Date + 1; // if RTC date is not last for given month then increase date.
  }
  RTC_SET_DATE(Date);  // Set new date in to RTC.
}

/*************************************************************************
Function Name: Decrement_Date
input: None.
Output: None.
Discription: this function is use decrement current date and if due to this
             change in month and year then do it.
*************************************************************************/
void Decrement_Date(void)
{
  unsigned char occ_day = 0,occ_weekday = 0,i = 0,find_date = 0,N = 0;
  unsigned char temp_week_day = 0,temp_occ_day = 0;
  unsigned char temp_date,temp_month,temp_year;

  Date.Date = Date.Date - 1;       // decrement RTC date by one
  if(Date.Date == 0)       // if date is 0 means current date is first date of that month.
  {
        Date.Month = Date.Month - 1;  // decrement RTC Month.
        if(Date.Month == 0)  // if month is 0 means it is a first month so after decrement it has to 12.
        {
                Date.Month = 12;
                Date.Year = Date.Year - 1;  // decrement year by 1.
        }

        /* calculate no of days in new month */
        if((Date.Month==1) || (Date.Month==3) || (Date.Month==5) || (Date.Month==7) || (Date.Month==8) || (Date.Month==10) || (Date.Month==12) )
        {
                N=31;
        }
        else if((Date.Month==4) || (Date.Month==6) || (Date.Month==9) || (Date.Month==11))
        {
                N=30;
        }		

        if(Date.Month==2)          // check for february
        {
                if(Date.Year%4==0)   // check for leap year
                        N=29;
                else
                        N=28;
        }
        /* calculate no of days in new month */
        Date.Date = N;
  }
  RTC_SET_DATE(Date); // Set new date in to RTC.
}

/*************************************************************************
Function Name: Increment_Local_Timer
input: None.
Output: None.
Discription: this function is use increment current date and if due to this
             change in month and year then do it.
*************************************************************************/
void Increment_Local_Timer(void)
{
  unsigned char occ_day = 0,occ_weekday = 0,i = 0,find_date = 0,N = 0;
  unsigned char temp_week_day = 0,temp_occ_day = 0;


  Local_Time.Sec = Local_Time.Sec + 1;
  if(Local_Time.Sec > 59)
  {
    Local_Time.Sec = 0;
    Local_Time.Min = Local_Time.Min + 1;
    if(Local_Time.Min > 59)
    {
      Local_Time.Min = 0;
      Local_Time.Hour = Local_Time.Hour + 1;
      if(Local_Time.Hour > 23)
      {
        Local_Time.Hour = 0;
        Local_Date.Date = Local_Date.Date + 1;
        if((Local_Date.Month==1) || (Local_Date.Month==3) || (Local_Date.Month==5) || (Local_Date.Month==7) || (Local_Date.Month==8) || (Local_Date.Month==10) || (Local_Date.Month==12) )
        {
          N=31;
        }
        else if((Local_Date.Month==4) || (Local_Date.Month==6) || (Local_Date.Month==9) || (Local_Date.Month==11))
        {
          N=30;
        }		

        if(Local_Date.Month==2)     // check for february
        {
          if(Local_Date.Year%4==0)  // check for leap year
            N=29;
          else
            N=28;
        }

        if(Local_Date.Date > N)  // if RTC date is last date of given month then make date = 1 and increase month.
        {
          Local_Date.Date = 1;
          Local_Date.Month = Local_Date.Month + 1;
          if(Local_Date.Month > 12) // if month is last month then increase year and make month = 1.
          {
            Local_Date.Month = 1;
            Local_Date.Year = Local_Date.Year + 1;
          }
        }
      }
    }
  }
}

void check_RTC_faulty_logic(void)
{
  unsigned char Display[20];
  signed int Temp_RTC_drift = 0;
  if(check_RTC_logic_cycle == 1)
  {
    check_RTC_logic_cycle = 0;
//    if(((Time.Hour == 23)&&(Time.Min == 59)) || ((Local_Time.Hour == 23) && (Local_Time.Min == 59)))
    if(((Time.Hour == 23)&&(Time.Min >= 30)) || ((Local_Time.Hour == 23) && (Local_Time.Min >= 30)))
    {
    }
    else
    {
        Temp_RTC_drift = (((Time.Hour * 60 * 60) + (Time.Min * 60) + (Time.Sec)) - ((Local_Time.Hour * 60 * 60) + (Local_Time.Min * 60) + (Local_Time.Sec)));
        if(Temp_RTC_drift < 0)
        {
            Temp_RTC_drift = Temp_RTC_drift * -1;
        }
        RTC_drift = Temp_RTC_drift;

        if(RTC_drift>25000)
        {
            return;
        }

//        #if(defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
//            sprintf(Display,"\r\nRTC Drift = %u",RTC_drift);
//            emberAfGuaranteedPrint(" %p", Display);
//            emberSerialWaitSend(APP_SERIAL);
//        #endif

        #if(defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)|| defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
            if((RTC_drift > 60) && (GPS_error_condition.bits.b7 == 1))   // 60
            {
                //        send_gps_command();
                Time_set_by_GPS = 0;
                need_to_send_GPS_command = 1;
            }
        #endif

        if((RTC_drift > 900) && (RTC_faulty_detected == 1))     // 900
        {
            RTC_faulty_detected = 0;    // 0 = faulty.
            error_condition1.bits.b2 = 1;

            if(RTC_Faulty_Shift_to_Local_Timer == 1) //this shows GPS is available and shift to previous mode as RTC  ok and driven by local timer
            {
                RTC_faulty_detected = 1;    // 1 = RTC Ok after new configuration.
                if(previous_mode != automode)
                {
                    automode = previous_mode;
                    halCommonSetToken(TOKEN_automode, &automode);   // store mode in to NVM.
                }
                return;
            }
            if((automode == 2) || (automode == 3) || (automode == 5) || (automode == 7))
            {
                previous_mode = automode;  // store mode value before chage.
                halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
                automode = 1;              // move SLC in to Photocell mode
                halCommonSetToken(TOKEN_automode, &automode);   // store mode in to NVM.
//                #if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
//                    sprintf(Display,"\r\nPrevious mode change by RTC function 2");
//                    emberAfGuaranteedPrint(" %p", Display);
//                    emberSerialWaitSend(APP_SERIAL);
//                #endif
            }

        }
        else
        {
            if(RTC_need_to_varify == 1)
            {
                RTC_need_to_varify = 0;
                if(RTC_drift < 10)
                {
                    RTC_faulty_detected = 1;    // 1 = RTC Ok after new configuration.
                    error_condition1.bits.b2 = 0;

                    if(previous_mode != automode)
                    {
                        automode = previous_mode;
                        halCommonSetToken(TOKEN_automode, &automode);   // store mode in to NVM.
                    }
                }
                else
                {

                    error_condition1.bits.b2 = 1;
                    RTC_faulty_detected = 0;    // 1 = RTC Ok after new configuration.
                    if(RTC_Faulty_Shift_to_Local_Timer == 1) //this shows GPS is available and shift to previous mode as RTC  ok and driven by local timer
                    {
                        RTC_faulty_detected = 1;    // 1 = RTC Ok after new configuration.
                        if(previous_mode != automode)
                        {
                            automode = previous_mode;
                            halCommonSetToken(TOKEN_automode, &automode);   // store mode in to NVM.
                        }
                        return;
                    }
                    if((automode == 2) || (automode == 3) || (automode == 5) || (automode == 7))
                    {
                        previous_mode = automode;  // store mode value before chage.
                        halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
                        automode = 1;              // move SLC in to Photocell mode
                        halCommonSetToken(TOKEN_automode, &automode);   // store mode in to NVM.
                        #if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
                            sprintf(Display,"\r\nPrevious mode change by RTC function 1");
                            emberAfGuaranteedPrint(" %p", Display);
                            emberSerialWaitSend(APP_SERIAL);
                        #endif
                    }

                }
            }
         }
    }
  }
}


///////////////////2.0.19///////////////////////////
			
//void Time_Slot_Config_Attribute(signed long int pass_value_for_time_slot)
//{
//    if(Set_Value_Respective_Attribute == 1)
//    {
//
//            Set_Value_Respective_Attribute =0;
//            if(pass_value_for_time_slot>235959){
//                    S_V_GE_Bit =0x40; //error code in response
//                    R_value=0x03; //set value too high
//            }
//            else if(pass_value_for_time_slot<0)
//            {
//                    pass_value_for_time_slot=0;
//                    S_V_GE_Bit =0x40; //error code in response
//                    R_value=0x02; //set value too low
//            }
//            else
//            {
//                    S_V_GE_Bit =0x00;//atr set ok		
//
//                    sprintf(Time_Conv_Decimal,"%000006lu",pass_value_for_time_slot);
//
//                            Time.Hour= (((Time_Conv_Decimal[0]-0x30)*10) +(Time_Conv_Decimal[1]-0x30));
//                            Time.Min= (((Time_Conv_Decimal[2]-0x30)*10) +(Time_Conv_Decimal[3]-0x30));
//                            Time.Sec= (((Time_Conv_Decimal[4]-0x30)*10) +(Time_Conv_Decimal[5]-0x30));
//            //	Dimvalue=100-Dimvalue;
//                    if((Time.Hour <=23)&&(Time.Min<=59)&&(Time.Sec<=59))
//                    {
//
//                            Energy_Saving();
//                    }
//                    else
//                    {
//                            S_V_GE_Bit =0x40;
//                            R_value=0x04;
//                    }		
//
//
//            }
//    }
//    else if(Get_Set_Value_Respective_Attribute == 1)
//    {
//
//            Get_Set_Value_Respective_Attribute =0;
//
//            if(pass_value_for_time_slot>235959){
//
//                    S_V_GE_Bit =0x40; //error code in response
//                    R_value=0x03; //set value too high
//            }
//            else if(pass_value_for_time_slot<0)
//            {
//                    pass_value_for_time_slot=0;
//                    S_V_GE_Bit =0x40; //error code in response
//                    R_value=0x02; //set value too low
//            }
//            else
//            {
//                    S_V_GE_Bit =0x80;//value follows
//
//                    sprintf(Time_Conv_Decimal,"%000006lu",pass_value_for_time_slot);
//                            Time.Hour= (((Time_Conv_Decimal[0]-0x30)*10) +(Time_Conv_Decimal[1]-0x30));
//                            Time.Min= (((Time_Conv_Decimal[2]-0x30)*10) +(Time_Conv_Decimal[3]-0x30));
//                            Time.Sec= (((Time_Conv_Decimal[4]-0x30)*10) +(Time_Conv_Decimal[5]-0x30));
//            //	Dimvalue=100-Dimvalue;
//                    if((Time.Hour <=23)&&(Time.Min<=59)&&(Time.Sec<=59))
//                    {
//
//                            Energy_Saving();	
//                    }
//                    else
//                    {
//                            S_V_GE_Bit =0x40;
//                            R_value=0x04;
//                    }
//            }
//    }
//    else if(Get_Value_Respective_Attribute == 1)
//    {
//            S_V_GE_Bit =0x80; //value follows
//            Get_Value_Respective_Attribute =0;
//            sprintf(Time_Conv_Decimal,"%d%d%d",Time.Hour,Time.Min,Time.Sec);
//            hhhh =atof(Time_Conv_Decimal);
//            R_value =(unsigned long int)hhhh;// R_value1 | R_value2 |R_value3;
//    }
//}				
//	
//void Energy_Saving(unsigned char loop_cnt)	
//{
//    unsigned char g=0;
//
//    for(g=0;g<loop_cnt;g++)
//    {
//            if(Time_Slot.Slot_Start[g] == 1)
//            {
//                Time_Slot.Slot_Start_Hour[g] = Time.Hour;
//                WriteByte_EEPROM(EE_Slot_Configure + (g*8),Time_Slot.Slot_Start_Hour[g]);
//                Time_Slot.Slot_Start_Min[g]= Time.Min;
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+1),Time_Slot.Slot_Start_Min[g]);
//
//            }	
//            if(Time_Slot.Slot_end[g] == 1)
//            {
//                Time_Slot.Slot_End_Hour[g] = Time.Hour;
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+2),Time_Slot.Slot_End_Hour[g]);
//                Time_Slot.Slot_End_Min[g]= Time.Min;
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+3),Time_Slot.Slot_End_Min[g]);
//                Time_Slot.Slot_Pulses[g] =0;
//                Time_Slot.kwh_value[g]= 0;
//                pulseCounter.long_data =Time_Slot.Slot_Pulses[g] =0;
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+4) +0,pulseCounter.byte[0]);
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+4) +1,pulseCounter.byte[1]);
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+4) +2,pulseCounter.byte[2]);
//                WriteByte_EEPROM(EE_Slot_Configure + ((g*8)+4) +3,pulseCounter.byte[3]);
//            }	
//    }	
//}

	
//void Enrgy_Save_Time_Slot_identify(void)
//{
///////////////////////////////////////
//    unsigned char i=0,h=0;
//    for(h =0;h<2;h++)
//    {
//       if((Date.Year >= Time_Slot.Slot_Start_Year[h])&&(Date.Year <= Time_Slot.Slot_End_Year[h]))
//        {
//            Start_To_Cnt_Kwh =1;
//
//            if((Time_Slot.Slot_Start_Month[h] <=Time_Slot.Slot_End_Month[h]))
//            {
//                if((Date.Month >=Time_Slot.Slot_Start_Month[h])&&(Date.Month<=Time_Slot.Slot_End_Month[h]))
//                {
//                    if((Date.Date < Time_Slot.Slot_Start_Date[h])&&(Date.Month ==Time_Slot.Slot_Start_Month[h])&&
//                       (Date.Year ==Time_Slot.Slot_Start_Year[h]))
//                    {
//                      Start_To_Cnt_Kwh =0;
//
//                    }
//                    if((Date.Date>Time_Slot.Slot_End_Date[h])&&(Date.Month==Time_Slot.Slot_End_Month[h])&&
//                       (Date.Year ==Time_Slot.Slot_End_Year[h]))
//                    {
//                      Start_To_Cnt_Kwh =0;
//                    }
//                }
//            }
//            else
//            {
//                if(Date.Month>=Time_Slot.Slot_Start_Month[h])
//                {
//
//                    if((Date.Date<Time_Slot.Slot_Start_Date[h])&&(Date.Month==Time_Slot.Slot_Start_Month[h])&&
//                       (Date.Year ==Time_Slot.Slot_Start_Year[h]))
//                    {
//                        Start_To_Cnt_Kwh =0;
//                    }
//    //                if((Date.Date>=Time_Slot.Slot_End_Date[i])&&(Date.Month==Time_Slot.Slot_End_Month[i]))
//    //                {
//    //                    Start_To_Cnt_Kwh =0;
//    //                }
//                }
//                else if(Date.Month<=Time_Slot.Slot_End_Month[h])
//                {
//                    Start_To_Cnt_Kwh =0;
//
//                    if((Date.Date>=Time_Slot.Slot_End_Date[h])&&(Date.Month==Time_Slot.Slot_End_Month[h])&&
//                       (Date.Year ==Time_Slot.Slot_End_Year[h]))
//                    {
//                        Start_To_Cnt_Kwh =0;
//                    }
//                }
//            }
//        }
//        if(Start_To_Cnt_Kwh)
//        {
//          Start_To_Cnt_Kwh =0;
//
//            for(i=0;i<12;i++)
//            {	
//                if(((Time_Slot.Slot_Start_Hour[i] == 0)&&(Time_Slot.Slot_Start_Min[i] == 0))&&
//                   ((Time_Slot.Slot_End_Hour[i] == 0)&&(Time_Slot.Slot_End_Min[i] == 0)))
//                {
//                        continue;	
//                }	
//                if(Time_Slot.Slot_Start_Hour[i]<=Time_Slot.Slot_End_Hour[i])
//                {
//                    if((Time.Hour>=Time_Slot.Slot_Start_Hour[i])&&(Time.Hour<=Time_Slot.Slot_End_Hour[i]))
//                    {
//                        Time_Slot.Save_Energy_In_Slot[i]=1;
//                        Time_Slot.Save_data_slot[i]=1;
//                        if((Time.Min<Time_Slot.Slot_Start_Min[i])&&(Time.Hour==Time_Slot.Slot_Start_Hour[i]))
//                        {
//                            Time_Slot.Save_Energy_In_Slot[i]=0;
//                        }
//                        if((Time.Min>=Time_Slot.Slot_End_Min[i])&&(Time.Hour==Time_Slot.Slot_End_Hour[i]))
//                        {
//                            Time_Slot.Save_Energy_In_Slot[i]=0;
//                        }	
//                    }
//                }
//                else
//                {
//                    if(Time.Hour>=Time_Slot.Slot_Start_Hour[i])
//                    {
//                        Time_Slot.Save_Energy_In_Slot[i]=1;
//                        Time_Slot.Save_data_slot[i]=1;
//                        if((Time.Min<Time_Slot.Slot_Start_Min[i])&&(Time.Hour==Time_Slot.Slot_Start_Hour[i]))
//                        {
//                            Time_Slot.Save_Energy_In_Slot[i]=0;
//                        }
//                        if((Time.Min>=Time_Slot.Slot_End_Min[i])&&(Time.Hour==Time_Slot.Slot_End_Hour[i]))
//                        {
//                            Time_Slot.Save_Energy_In_Slot[i]=0;
//                        }
//                    }
//                    else if(Time.Hour<=Time_Slot.Slot_End_Hour[i])
//                    {
//                        Time_Slot.Save_Energy_In_Slot[i]=1;
//                        Time_Slot.Save_data_slot[i]=1;
//
//                        if((Time.Min>=Time_Slot.Slot_End_Min[i])&&(Time.Hour==Time_Slot.Slot_End_Hour[i]))
//                        {
//                            Time_Slot.Save_Energy_In_Slot[i]=0;
//                        }
//                    }
//                }
//            }
//        }
//    }
//    if((Date.Week>=1)&&(Date.Week<=5))
//    {
//        Time_Slot.Save_Energy_In_Slot[3]=0;
//          Time_Slot.Save_Energy_In_Slot[4]=0;
//            Time_Slot.Save_Energy_In_Slot[5]=0;
//              Time_Slot.Save_Energy_In_Slot[9]=0;
//                Time_Slot.Save_Energy_In_Slot[10]=0;
//                  Time_Slot.Save_Energy_In_Slot[11]=0;
//    }
//    else
//    {
//         Time_Slot.Save_Energy_In_Slot[0]=0;
//          Time_Slot.Save_Energy_In_Slot[1]=0;
//            Time_Slot.Save_Energy_In_Slot[2]=0;
//              Time_Slot.Save_Energy_In_Slot[6]=0;
//                Time_Slot.Save_Energy_In_Slot[7]=0;
//                  Time_Slot.Save_Energy_In_Slot[8]=0;
//
//    }
//}
////////////////////////////////////////////////



void Fill_RTC_BY_Local_Timer(void)
{
  unsigned char Local_SLC_Mix_R_weekday =0;
//  Local_Time.Hour = Time.Hour;																										
//  Local_Time.Min = Time.Min;
//  Local_Time.Sec = Time.Sec;
//  Local_Date.Date = Date.Date;
//  Local_Date.Month = Date.Month;
//  Local_Date.Year = Date.Year;

        Time.Sec  = Local_Time.Sec;
        Time.Min  = Local_Time.Min;
        Time.Hour = Local_Time.Hour;
        Date.Date = Local_Date.Date;
        Date.Month = Local_Date.Month;
        Date.Year = Local_Date.Year;				
        Date.Week  = week_day(Date.Year,Date.Date,Date.Month);		



        if(Date.Week == 0)                  // sunday
        {
          Local_SLC_Mix_R_weekday = 1;
        }
        else if(Date.Week == 1)			          // monday
        {
          Local_SLC_Mix_R_weekday = 64;
        }
        else if(Date.Week == 2)             // tuesday
        {
          Local_SLC_Mix_R_weekday = 32;
        }
        else if(Date.Week == 3)			          // wenesday
        {
          Local_SLC_Mix_R_weekday = 16;
        }
        else if(Date.Week == 4)			          // thrusday
        {
          Local_SLC_Mix_R_weekday = 8;
        }
        else if(Date.Week == 5)			          // friday
        {
          Local_SLC_Mix_R_weekday = 4;
        }
        else if(Date.Week == 6)			          // seturday
        {
          Local_SLC_Mix_R_weekday = 2;
        }
        else
        {
        }

        day_sch_R.Val = Local_SLC_Mix_R_weekday;	


        if(Time.Hour < 12)              // for schedule off set find date and time from function for 12 hour offset.
        {
          Find_Previous_date_Month();
        }
        else
        {
          PDate.Date = Date.Date;   // after 12 o'clock change date and time to acthual value.
          PDate.Month = Date.Month;
        }
}

void Execute_Panic_App(void)
{

  if(Panic_Cnt<Panic_Time_Out)
  {
//    if((Panic_Lamp_On_Cnt<Lamp_On_Time))
//    {
//        Panic_Lamp_OFF_Cnt =0;
//        Set_Do(1);
//        dimming_applied(0);
//
//    }
//     else if((Panic_Lamp_OFF_Cnt<Lamp_OFF_Time))
//    {
//        if(Panic_Dim_Value !=100) //0% brightness
//            dimming_applied(Panic_Dim_Value);
//        else
//            Set_Do(0);
//    }
//    else
//    {
//        Panic_Lamp_On_Cnt =0;
//    }
    //if((Panic_Lamp_On_Cnt<Lamp_On_Time)&&(Lamp_ON_Flag ==1))
    if((Lamp_ON_Flag == 1)||(Lamp_ON_First_Responder == 1))
    {
        Set_Do(1);
        panic_dim =0;
        dimming_applied(0);
    }
    if((Lamp_OFF_Flag == 1)&&(Lamp_ON_First_Responder == 0))
    {
        if(Panic_Dim_Value !=100) //0% brightness
        {
          panic_dim = Panic_Dim_Value;
          dimming_applied(Panic_Dim_Value);
        }
        else
        {
          panic_dim =100;
            Set_Do(0);
        }
    }

  }
  else
  {
     automode =previous_mode;
  }
}
//#if(defined(DALI_SUPPORT))
void Multiple_Dali_Driver_Handle(void)
{
  unsigned char j=0,k=0,serach=0;
   for(k=0;k<15;k++)
   {
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);

              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
    }
              halClearLed(EN_DALI); // Enable
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);

              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);

              intialize_randomize();
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);

                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);

                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);


                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);

                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);
                delay(60060);     // 55 msec
                delay(60060);

          for (serach=0;serach<4;serach++)
          {
                delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              delay(60060);     // 55 msec
              delay(60060);
              assigning_address();
          }
}
//#endif
void Read_Dali_Scene(void)
{
          delay(60060);     // 55 msec
          delay(60060);
          delay(60060);     // 55 msec
          delay(60060);

       Read_Dali_Ballast_Information(3,9);
          delay(60060);     // 55 msec
          delay(60060);
          delay(60060);     // 55 msec
          delay(60060);

       Read_Dali_Ballast_Information(1,11);

          delay(60060);     // 55 msec
          delay(60060);
          delay(60060);     // 55 msec
          delay(60060);

       Read_Dali_Ballast_Information(2,7);
          delay(60060);     // 55 msec
          delay(60060);
          delay(60060);     // 55 msec
          delay(60060);
       //Read_Dali_Ballast_Information(0,3);
       Read_Dali_Ballast_Information(0,5);
          delay(60060);     // 55 msec
          delay(60060);
          delay(60060);     // 55 msec
          delay(60060);
}
/*************************************************************************************
* Function 		: sendDaliCommand_ToCheck_DimmerDriver
* Use			: Broadcast message for QUERY STATUS
* Argument		: 
* return		: 1 - Dali Reply Received	||	0 - Dali Reply Not Received
**************************************************************************************/

unsigned char sendDaliCommand_ToCheck_DimmerDriver()
{
  unsigned char j;
  unsigned char Display[50];
  
  halClearLed(EN_DALI);
  delay(60060);
  delay(60060);
//  forward    = ((0xFF << 8) | (0x99));
  forward    = ((0xFF << 8) | (0x97)); //version no

   f_dalitx =  0;                                // clear DALI send flag 
   f_dalirx =  0;                                // clear DALI receive (answer) flag 
   DALI_Send();                                  // DALI send data to slave(s) 

  for(j=0;j<10;j++)
    
  delay(60060);

  if(f_dalirx)
  {
      return 0x01;
  }
  else
  {
      return 0x00;
  }	
}
/*************************************************************************************
* Function 		: detectDimmerDriver
* Use			: 
* Argument		: 
* return		: 1 - Dali 	||	0 - AO
**************************************************************************************/
unsigned char detectDimmerDriver()
{
  unsigned char dali_detect_count;
  unsigned char Display_1[50];
  unsigned int i;
      
    //----------- NO Relay -------------//  
    if(set_do_flag == 0)
    {
      lamp_off_on_timer = Lamp_off_on_Time;
      Set_Do(1);

    }
#if defined(ISLC_3100_V7_7)       
    halSetLed(MUX_A0);	 //0	
    halClearLed(MUX_A1);  //1
#elif defined(ISLC_3100_V7_9)
    halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif
    
    halStackIndicatePresence();
    for(i=0;i<300;i++)
    {
      Ember_Stack_run_Fun();
      delay(10000);
    }
    dali_detect_count=sendDaliCommand_ToCheck_DimmerDriver();
    dali_detect_count=dali_detect_count+sendDaliCommand_ToCheck_DimmerDriver();   
    dali_detect_count=dali_detect_count+sendDaliCommand_ToCheck_DimmerDriver();  
    if(dali_detect_count>0)
    {
      dali_support=1;
      DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
      halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);
#if defined(ISLC_3100_V7_7)
      halSetLed(MUX_A0);	 //0	
      halClearLed(MUX_A1);  //1
#elif defined(ISLC_3100_V7_9)      
      halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif
      halStackIndicatePresence();
    }
    else
    {
      dali_support=0;
#if defined(ISLC_3100_V7_7)      
      halSetLed(MUX_A1);	 //0	
      halClearLed(MUX_A0);  //1
#elif defined(ISLC_3100_V7_9)
      halClearLed(MUX_A0);  // Set=0=dali & clear=1=AO
#endif      
      halStackIndicatePresence();
      CheckAoDimmerForDetectionTimmer=0;
      
      duty_cycle = ( PWM_INIT_VAL * (100/100.0));
      DUTY_CYCLE =(100*PWM_FREQ)/100;
      while(CheckAoDimmerForDetectionTimmer<=3)
      {
        Ember_Stack_run_Fun();
        Read_EnergyMeter();																					// resent power on flag
        calculate_3p4w();    																					// claculate en
        delay(20);
      } 
      full_brightPower = w1; 

      duty_cycle = (PWM_INIT_VAL * (30/100.0));
      DUTY_CYCLE =(30*PWM_FREQ)/100;
      CheckAoDimmerForDetectionTimmer = 0;
      while(CheckAoDimmerForDetectionTimmer<=3)
      {
        Ember_Stack_run_Fun();
        Read_EnergyMeter();																					// resent power on flag
        calculate_3p4w();    
        delay(20);
      }  
      full_brightPower_at_30 = full_brightPower *0.2;
      if((full_brightPower-w1)>(full_brightPower_at_30))
      {
        dali_support = 0;
        DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
        halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);        
      }
      else
      {
        dali_support = 3;
        DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
        halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);       
      }
      
    } 
    lamp_off_on_timer = Lamp_off_on_Time;       
    lamp_on_first=0;
    default_dali=0;
    Set_Do(1);

    return dali_support;
}


/*************************************************************************************
* Function 		: Select_RS485_Or_DI2
* Use			: Select RS485 or DI2 according to argument pass
                        : For RS485 default RS485 in Rx mode 
* Argument		: rs485_0_Or_di2_1 (if 0 then rs485 and 0 then DI2) 
* return		: 
**************************************************************************************/

unsigned char Select_RS485_Or_DI2(unsigned char rs485_0_Or_di2_1)
{
  if(rs485_0_Or_di2_1==0)
  {
#if defined(ISLC_3100_V7_7)
    RS485_ModeSelection_0RX_1TX(RS485RX);                             //Default RS485 RX Enable
#elif defined(ISLC_3100_V7_9)  
    halClearLed(RS485_DI2_EN2);                    // Set means Set to 0
#endif     
    
    SET_REG_FIELD(GPIO_PBCFGL, PB1_CFG, GPIOCFG_OUT_ALT);
    SET_REG_FIELD(GPIO_PBCFGL, PB2_CFG, GPIOCFG_IN_PUD);
   
    //emberSerialInit(RS485_SERIAL,BAUD_115200, PARITY_NONE, 1);
    emberSerialInit(RS485_SERIAL,BAUD_9600, PARITY_NONE, 1);
    //send_data_on_RS485("ghuiteshshfjsfsdfj",25);
  }
  else if(rs485_0_Or_di2_1==1)
  {
    
    SET_REG_FIELD(GPIO_PBCFGL, PB1_CFG, GPIOCFG_IN);
#if defined(ISLC_3100_V7_7)    
    halSetLed(RS485_DI2_EN1);                    // Set means Set to 0
    halSetLed(RS485_DI2_EN2);                    // Set means Set to 0
#elif defined(ISLC_3100_V7_9)  
    halSetLed(RS485_DI2_EN2);                    // Set means Set to 0
#endif  
  }
  return 0;
}

/*************************************************************************************
* Function 		: RS485_ModeSelection_0RX_1TX
* Use			: 
* Argument		: RS485RX_0_Or_RS485TX_1
* return		: 
**************************************************************************************/
#if defined(ISLC_3100_V7_7)
unsigned char RS485_ModeSelection_0RX_1TX(unsigned char RS485RX_0_Or_RS485TX_1)
{
  if(RS485RX_0_Or_RS485TX_1==0)
  {
    halSetLed(RS485_DI2_EN1);                    // Set means Set to 0
    halClearLed(RS485_DI2_EN2);                  // clear means Set to 1
  }
  else if(RS485RX_0_Or_RS485TX_1==1)
  {
    halClearLed(RS485_DI2_EN1);                  // clear means Set to 1
    halClearLed(RS485_DI2_EN2);                  // clear means Set to 1
  }  
  return 0;
}
#endif
/*************************************************************************************
* Function 		: send_data_on_RS485_uart
* Use			: used for trasmite data on rs485 UART post
                        : IT just transmite data on  uart port , it does not set any selection for Rs485 TX or RX mode
                        : use ony to trasmite on uart port.if need output at RS485 connection use send_data_on_RS485 function 
* Argument		: data : array of char and len : length of array
* return		: 
**************************************************************************************/

void send_data_on_RS485_uart(unsigned char *data,unsigned int len)
{
    unsigned int i;

    for(i=0;i<len;i++)
    {
      emberSerialWaitSend(RS485_SERIAL);
      emberSerialWriteByte(RS485_SERIAL,data[i]);
    }
}
/*************************************************************************************
* Function 		: send_data_on_RS485
* Use			: used for trasmite data on rs485 post
                        : it enable Tx and send data on RS485_SERIAL and Again Set Rs485 in Rx mode
* Argument		: data : array of char and len : length of array
* return		: 
**************************************************************************************/
#if defined(ISLC_3100_V7_7)
void send_data_on_RS485(unsigned char *data,unsigned int len)
{
    unsigned int i;
    RS485_ModeSelection_0RX_1TX(RS485TX);      // rs485 Tx enable
    for(i=0;i<len;i++)
    {
      emberSerialWaitSend(RS485_SERIAL);
      emberSerialWriteByte(RS485_SERIAL,data[i]);
    }
    delay(1000);  // Todo : Fix Delay to Receive on RS485
    RS485_ModeSelection_0RX_1TX(RS485RX);      // rs485 Rx enable
}
#elif defined(ISLC_3100_V7_9)
void send_data_on_RS485(unsigned char *data,unsigned int len)
{
    unsigned int i;
    for(i=0;i<len;i++)
    {
      emberSerialWaitSend(RS485_SERIAL);
      emberSerialWriteByte(RS485_SERIAL,data[i]);
    }
    delay(1000);  // Todo : Fix Delay to Receive on RS485
}
#endif

void Fault_Remove_Logic(void)
{
	//if(((Lamp_Balast_fault_Remove_Cnt>=(Lamp_Balast_fault_Remove_Time*3600)))&&(Lamp__Ballast_Fault_Occured == 1))
//	if((Lamp_Balast_fault_Remove_Cnt>=60))
  if(Lamp_Balast_fault_Remove_Time!=0)
  {
        if((Lamp_Balast_fault_Remove_Cnt>=Lamp_Balast_fault_Remove_Time))
        {
            Lamp_Balast_fault_Remove_Cnt =0;
            if(Ballast_Fault_Occured == 1)
            { 
              if((irdoub < current_creep_limit) && (detect_lamp_current == 1) && (detect_lamp_current_first_time_on == 1))// ballast fault trip checking logic
              {
                Ballast_Fault_Remove_Retry_Cnt_3 =0;
              }
              else
              {
                Ballast_Fault_Remove_Retry_Cnt_3 ++;
                if(Ballast_Fault_Remove_Retry_Cnt_3 >= 3)
                {
                  Ballast_Fault_Remove_Retry_Cnt_3 =0; 
                  //Open_circuit = 0;
                  error_condition1.bits.b0 = 0;	// ballast fail trip //open circuit trip	
                  low_current_counter = 0;
                  check_current_counter = 0;
                  Ballast_Fault_Occured =0;	
                  //	Lamp__Ballast_Fault_Occured =0;		
                          //	No_check_Ballast_After_This_Event =1;
                 } 
              }	 
            }
            if(Lamp_Fault_Occured == 1)
            {
              if((w1 < (KW_Threshold * (1.0 - Per_Val))) && 
                (detect_lamp_current == 1) && 
                (detect_lamp_current_first_time_on == 1)
               )// LAmp fault trip checking logic
               {
                  Lamp_Fault_Remove_Retry_Cnt_3 =0;
               }
               else
               {
                 Lamp_Fault_Remove_Retry_Cnt_3 ++;
                 if(Lamp_Fault_Remove_Retry_Cnt_3 >= 3)
                 {
                    Lamp_Fault_Remove_Retry_Cnt_3 =0; 
                    //Lamp_Failure = 0;
                    error_condition.bits.b3 = 0; // lamp fail trip	
                    low_current_counter = 0;
                    check_current_counter = 0;
                    Lamp_Fault_Occured =0;
                    //No_check_Lamp_After_This_Event =1;
                    //Lamp__Ballast_Fault_Occured =0;
                  } 
               }  
            }              
	}
	else
	{
	}
		
	if((Ballast_Fault_Occured == 0)&&(Lamp_Fault_Occured == 0))
	{
          Lamp_Balast_fault_Remove_Cnt =0;	
	}
  }
	
		
}	