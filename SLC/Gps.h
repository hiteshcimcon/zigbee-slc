/////////////////////////////Gps.h////////////////////////////

#ifndef _GPS_H_
#define _GPS_H_

struct setalit
{
  unsigned int ID;
  unsigned int ev_angle;
  unsigned int azimuth;
  unsigned int RSSI;
};

struct GPS_Status
{
  unsigned char Status;
  unsigned char Date;
  unsigned char Month;
  unsigned char Year;
  unsigned char hour;
  unsigned char Min;
  unsigned char Sec;
  float Latitude;
  float Longitude;
  unsigned char GPS_Data_Valid;
  unsigned char threeD_Fix_successfully;
  unsigned char Setalite_Visinity_Proper;
  unsigned char Setalite_reading_OK;
  unsigned char SBAS_Active;
  unsigned char gagan_Setalite_Id;
  unsigned char gagan_Setalite_RSSI;
  unsigned char HDOP_OK;
};

extern unsigned char GPS_Str_rec;
extern unsigned char GPS_Read_Enable;
extern unsigned int GPS_read_success_counter;
extern unsigned char GPS_Data_Valid;
extern unsigned int GPS_Wake_timer;
extern unsigned char Packate_send_success;
//extern struct GPS_Status GPS_Info[2];

extern unsigned char No_of_setalite_threshold;
extern unsigned char Setalite_angle_threshold;
extern unsigned char setalite_Rssi_threshold;
extern unsigned char Maximum_no_of_Setalite_reading;

extern float HDOP_Gps;
extern float HDOP_Gps_threshold;
extern unsigned char HDOP_OK;
extern unsigned char Sbas_setalite_Rssi_threshold;
extern unsigned char Check_Sbas_Enable;

extern unsigned char GPS_reading_comming_OK;
extern unsigned char Start_averaging_of_GPS;
extern unsigned int averaging_counter,averaging_Timer;
extern double avg_latitude,avg_longitude;
extern unsigned char need_to_send_GPS_command;
extern unsigned char Packate_send_threshold_success;

extern unsigned int GPRMC_Received_counter;
extern unsigned int GPGSA_Received_counter;
extern unsigned int GPGGA_Received_counter;
extern unsigned int GPGSV_Received_counter;

extern unsigned char Send_GPS_Data_at_valid_Dcu;
extern unsigned char GPS_Rescan;
extern unsigned char GPS_power_On_detected;
extern unsigned char Auto_Gps_Data_Send;
extern unsigned char need_to_send_Shutdown_command;
//extern BYTE_VAL  GPS_error_condition;
extern unsigned char GPS_Delay_timer;
extern unsigned char GPS_Data_throw_status;
extern unsigned char Time_set_by_GPS;
extern unsigned char need_Average_value_Gps;

unsigned char CheckCRC(char* gpr_string, unsigned char Gps_len);
float strtoflt(char inputstr[] ,int start,int length);
void Parse_GPS_Query(unsigned char *rec_buff,unsigned int temp_rx_serial);
int FindSubstr(char *listPointer,char *itemPointer);
unsigned char GeneratekCRC(char* gpr_string, unsigned char Gps_len);
void send_gps_command(void);
double ddmm2degree(double data1);
void send_data_on_uart(unsigned char *data,unsigned int len);
void shutdown_GPS(void);
unsigned int Find_value_from_string(unsigned char *res_buff,unsigned char no_of_comma,unsigned int temp_rx_serial);
unsigned int find_comma_Position(unsigned char *res_buff,unsigned char no_of_comma,unsigned int temp_rx_serial);
void check_Gps_Statistics(void);
double Find_float_from_string(unsigned char *res_buff,unsigned char no_of_comma,unsigned int temp_rx_serial);
void Set_GPS_Time_to_RTC(void);
void find_time_from_GPS(unsigned char *res_buff,unsigned char temp_rx_serial);

#endif