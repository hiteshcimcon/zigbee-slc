//SLC defination

#ifndef SLC_DEFINATION
#define SLC_DEFINATION

                //open below comment to complie code with new lamp fault logic.
#define NEW_LAMP_FAULT_LOGIC

                //open below comment to complie code with old lamp fault logic.
//#define OLD_LAMP_FAULT_LOGIC

                //open any one comment to compile code with appropriate GPS module baud-rate.
#define GPS_BAUD_9600
                //#define GPS_BAUD_115200

//open comment if V9 PCB to complie code with DALI interface with ballast.
//#define DALI_SUPPORT

//open comment if V9 PCB to complie code with DALI interface with ballast.
//#define SUPER_CAP

//open comment if V9 PCB to complie code with DALI interface with ballast.
//#define SLC_RF_LOWPOWER

//open any one comment to complie code with appropriate hardware type.
//#define ISLC_3100_V7
//#define ISLC_3100_V5_V6
//#define ISLC_10
//#define ISLC_T8
//#define ISLC_3100_7P_V1
//#define ISLC_3100_V9
//#define ISLC_3100_V7_2
//#define ISLC_3100_V7_3_0216
//#define ISLC_3100_V7_7    //EM IC change RTC change and Calibration value change only
#define ISLC_3100_V7_9
//#define ISLC_3100_480V_Rev3  //EM related changes only & remaining logic application is same as having in ISLC_3100_V7_3_0216 ||defined(ISLC_3100_V7_7)
//#define ISLC_3100_V9
//#define ISLC_3100_V9_2

//#define ISLC_T8_V4  //this version Not support Dali.
//#define SMALL_CITI_2000
//#define ISLC3300_T8_V4
//#define ISLC3300_T8_V2


#ifdef ISLC3300_T8_V4
    #define ISLC3300_T8_V2
    #define BATTERY_RTC
#endif

#ifdef ISLC3300_T8_V2
    #ifdef BATTERY_RTC
    #else
        #define SUPER_CAP
    #endif

    #define SMALL_CITI_2000
#endif

#ifdef ISLC_T8_V4
    #define ISLC_T8
    #define SUPER_CAP
#endif


#endif
