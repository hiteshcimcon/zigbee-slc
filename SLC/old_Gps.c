//////////////////GPS.c///////////////////

#include "app/framework/include/af.h"
#include "generic.h"
#include "Gps.h"
#include "Application_logic.h"
#include <stdio.h>
#include <string.h>

unsigned char GPS_reading = 1;
unsigned char GPS_Data_Valid = 0;
float Gps_latitude = 0.0;
float Gps_longitude = 0.0;
unsigned char GPS_Str_rec = 0;
unsigned char GPS_Read_Enable = 1;
unsigned int GPS_read_success_counter = 0;
unsigned int GPS_Wake_timer = 120;
unsigned char Packate_send_success = 0;
unsigned char GPS_power_On_detected = 1;

extern float Latitude,longitude;
extern unsigned char need_to_send_lat_long;
//extern struct Astro_sSch[2];

void Parse_GPS_Query(unsigned char *rec_buff,unsigned char temp_rx_serial)
{
  unsigned char GPS_RMC_Revd_Byte;
  unsigned int x=0;
  float difference1,difference2;
  //  unsigned char temp[10];
  //  unsigned int i=0;
  
  
  for(x=0;x<temp_rx_serial;x++)
  {
    if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'G') && (rec_buff[x+2] == 'P')&& (rec_buff[x+3] == 'R') && (rec_buff[x+4] == 'M') && (rec_buff[x+5] == 'C'))
    {
      //emberAfGuaranteedPrint("\r\n RMC string received");
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      { 
        //emberAfGuaranteedPrint("\r\n CRC Match");
        if(rec_buff[x+18] == 'A')
        {	
          GPS_Data_Valid = 1;	
          //emberAfGuaranteedPrint("\r\n GPS string valid");
          GPS_read_success_counter++;
          if(GPS_read_success_counter >= GPS_Wake_timer)
          {
            GPS_read_success_counter = 0;
            GPS_Data_Valid = 3;              // shutdown due after valid lat/long received.
            shutdown_GPS();
          }
          Gps_latitude = ddmm2degree(strtoflt((char *)rec_buff , x+20 ,9));
          if(rec_buff[x+30] == 'S')
          {
            Gps_latitude = Gps_latitude * -1;   
          }
          Gps_longitude = ddmm2degree(strtoflt((char *)rec_buff , x+32 ,10));
          if(rec_buff[x+43] == 'W')
          {
            Gps_longitude = Gps_longitude * -1;  
          }
          
          difference1 = Latitude - Gps_latitude;
          if(difference1 < 0)
          {
            difference1 = (difference1 * (-1));  
          }
          
          difference2 = longitude - Gps_longitude;
          if(difference2 < 0)
          {
            difference2 = (difference2 * (-1));  
          }
          
          if(GPS_read_success_counter >= (GPS_Wake_timer - 10))
          {
            if((difference1 > 0.0001) || (difference2 > 0.0001))
            {
              Latitude = Gps_latitude;
              halCommonSetToken(TOKEN_Latitude,&Latitude);
              longitude = Gps_longitude;
              halCommonSetToken(TOKEN_longitude,&longitude);
              
              GetSCHTimeFrom_LAT_LOG();								//get sunset sunrise schedule			
              Astro_sSch[0].cStartHour = sunsetTime/3600;				// V6.1.10
              Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		// V6.1.10
              Astro_sSch[0].cStopHour  = 23;							// V6.1.10
              Astro_sSch[0].cStopMin   = 59;							// V6.1.10
              Astro_sSch[0].bSchFlag   = 1;							// V6.1.10
              
              Astro_sSch[1].cStartHour = 00;							// V6.1.10
              Astro_sSch[1].cStartMin  = 00;							// V6.1.10
              Astro_sSch[1].cStopHour  = sunriseTime/3600;			// V6.1.10
              Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;		// V6.1.10									
              Astro_sSch[1].bSchFlag   = 1;							// V6.1.10
            } 
            
            if(((difference1 > 0.001) || (difference2 > 0.001)) && (GPS_power_On_detected == 1))
            {
              GPS_power_On_detected = 0;
              need_to_send_lat_long = 1;
            }
            
            Latitude = Gps_latitude;        // GPS TEST
            longitude = Gps_longitude;
          }   
        }
        else
        {
          GPS_Data_Valid = 0;
          GPS_read_success_counter++;
          if(GPS_read_success_counter >= GPS_Wake_timer)
          {
            GPS_read_success_counter = 0;
            GPS_Data_Valid = 4;             // shutdown due to no valid lat/long received.
            shutdown_GPS();
          }
        }	
      }
      else
      {
        GPS_Data_Valid = 2;
      }
    } 
  }
  
  for(x=0;x<temp_rx_serial;x++)
  {
    if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'P') && (rec_buff[x+2] == 'M')&& (rec_buff[x+3] == 'T') && (rec_buff[x+4] == 'K') && (rec_buff[x+5] == '0') && (rec_buff[x+6] == '0') && (rec_buff[x+7] == '1'))
    { 
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      { 
        if((rec_buff[x+8] == ',') && (rec_buff[x+9] == '3') && (rec_buff[x+10] == '1')&& (rec_buff[x+11] == '4') && (rec_buff[x+12] == ','))  
        {
          if(rec_buff[x+13] == '3')
          {
            Packate_send_success = 1;
          }
          else
          {
            Packate_send_success = 0;  
          }
        }
      }
    }
  }
}

unsigned char CheckCRC(char* gpr_string, unsigned char Gps_len)
{
	int i=0;
	unsigned char ByteSUM=0;
 unsigned char dispStr[5];

	for(i=0;i<Gps_len;i++)
	{
		   ByteSUM^=gpr_string[i];					  		//Calculate Sun here 
	}

	sprintf((char *)dispStr,"%02X",ByteSUM);
	if(strncmp((char *)&dispStr[0],(char *)&gpr_string[i+1],2)==0)
	{
		return(1);
	}
	else
	{
		return(0);
	}
}

unsigned char GeneratekCRC(char* gpr_string, unsigned char Gps_len)
{
	int i=0;
	unsigned char ByteSUM=0;

	for(i=0;i<Gps_len;i++)
	{
		   ByteSUM^=gpr_string[i];					  		//Calculate Sun here 
	}

	return ByteSUM;	
}

float strtoflt(char inputstr[],int start,int length)
{
   float result= 0;   
   int dotpos = 0;
   int n;
   for (n =0; n < length; n++)
   {
     if (inputstr[n + start ] == '.')
     {
       dotpos = length- n -1;
     }
     else
     {
       result = result * 10 + (inputstr[n + start]-'0');
     }
   }
   while ( dotpos--)
   {
     result /= 10;
   }
   return result;
}

int FindSubstr(char *listPointer,char *itemPointer)
{
  int t;
  char *p, *p2;

  for(t=0; listPointer[t]; t++) 
  {
    p = &listPointer[t];
    p2 = itemPointer;

    while(*p2 && *p2==*p) 
	   {
      p++;
      p2++;
    }
    if(!*p2) 
	   {
		     return t; /* 1st return */
	   }
  }
  return -1; /* 2nd return */
}

void send_gps_command(void)
{
//	unsigned char Crc_Byte;
	unsigned char temp_buff[80];

 
 sprintf((char *)temp_buff,"$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
 send_data_on_uart((char *)temp_buff,strlen(temp_buff));
 
//	sprintf(temp_buff,"PSRF103,00,00,00,01");
//	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
//	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
//	WriteData_UART1_GPS(Tx_buffer,strlen(Tx_buffer));
//	Delay(100);
//	
//	sprintf(temp_buff,"PSRF103,01,00,00,01");
//	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
//	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
//	WriteData_UART1_GPS(Tx_buffer,strlen(Tx_buffer));
//	Delay(100);
//	
//	sprintf(temp_buff,"PSRF103,02,00,00,01");
//	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
//	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
//	WriteData_UART1_GPS(Tx_buffer,strlen(Tx_buffer));
//	Delay(100);
//	
//	sprintf(temp_buff,"PSRF103,03,00,00,01");
//	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
//	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
//	WriteData_UART1_GPS(Tx_buffer,strlen(Tx_buffer));
//	Delay(100);
//
////	sprintf(temp_buff,"PSRF103,04,00,03,01");
////	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
////	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
////	WriteData_UART1_GPS(Tx_buffer,strlen(Tx_buffer));
////	Delay(100);
//	
//	sprintf(temp_buff,"PSRF103,05,00,00,01");
//	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
//	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
//	WriteData_UART1_GPS(Tx_buffer,strlen(Tx_buffer));
//	Delay(100);	
}

void shutdown_GPS(void)
{
  unsigned char temp_buff[80];

   sprintf((char *)temp_buff,"$PMTK161,0*28\r\n");
   send_data_on_uart((char *)temp_buff,strlen(temp_buff));
}


float ddmm2degree(float data1)
{
	float x,ans;
	
	x = ((int)(data1/100));
	ans = x + (data1 - (x*100))/60;
    return(ans);
}