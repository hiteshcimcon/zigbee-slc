//#include "extern.h"
/*****************************************************************************************/
/*
   I2CMEM.C for V3.0.0
*/
/*****************************************************************************************/

void DelayI2C(long K)
{
long i;
for(i=0;i<K;i++)
 {;}
}

void SetSCL()
{
	 IOSET0 |= SCL0;
	 DelayI2C(150);	 
}
void SetSDA()
{
	IODIR0 |= SDA0;			//SDA0 as Write only direction
	IOSET0 |= SDA0;
	DelayI2C(150);
}
void ClrSCL()
{
	IOCLR0 |= SCL0;
	DelayI2C(150);	
}
void ClrSDA()
{
	IODIR0 |= SDA0;			//SDA0 as Write only direction
	IOCLR0 |= SDA0;
	DelayI2C(150);
}
char ReadSDA()
{
	long data;
	IODIR0 &= (~SDA0);
	DelayI2C(100);
	data = IOPIN0;
	if(data & SDA0)
		return 1;
	else
		return 0;
}
void InitI2C (void)
{
	unsigned char i;

	PINSEL1 &= 0xFC3FFFFF;	//select SCL & SDA pin as GPIO

	IODIR0 |= SCL0;			//SCL0 as Write only direction

	SetSDA();
	SetSCL();

	for(i=0;i<9;i++)
	{
		ClrSCL();
		SetSCL();
	}
}

/*AT24C512 */
void Start(void)
{
	SetSDA();
	SetSCL();
	ClrSDA();
	ClrSCL();
}

void Stop(void)
{
	ClrSCL();
	SetSDA();
	ClrSDA();
	SetSCL();
	SetSDA();
}

void DeviceAddress (unsigned char bankno, unsigned char mode)
{
	unsigned char deviceid, i;

	switch(bankno)
	{
	case 0:
		deviceid = 0xA0;
		break;
	case 1:
		deviceid = 0xA2;
		break;
	case 2:
		deviceid = 0xA4;
		break;
	case 3:
		deviceid = 0xA6;
		break;
	}

	if(mode == READ)
		deviceid |= 0x01;

	
	for(i=0; i<8; i++)
	{
		ClrSCL();
		if((deviceid & 0x80))
			SetSDA();
		else
			ClrSDA();
		
		SetSCL();

		deviceid = deviceid << 1;
	}
	
	ClrSCL();
	SetSCL();

	if(ReadSDA())
	{
		//NACK
	}
	else
	{
		//ACK
	}
	ClrSCL();
}

void AddressWrite (char datai)
{

unsigned char i;

	
	for(i=0; i<8; i++)
	{
		ClrSCL();
		if((datai & 0x80))
			SetSDA();
		else
			ClrSDA();

		SetSCL();

		datai = datai << 1;
	}
	

  	ClrSCL();
	SetSCL();

	if(ReadSDA())
	{
		//NACK
	}
	else
	{
		//ACK
	}
	ClrSCL();
}

void DataWrite (char datai)
{

unsigned char i;

	
	for(i=0; i<8; i++)
	{
		ClrSCL();
		if((datai & 0x80))
			SetSDA();
		else
			ClrSDA();
	
		SetSCL();
		datai = datai << 1;
	}
	

	ClrSCL();
	SetSCL();
	if(ReadSDA())
	{
		//NACK
	}
	else
	{
		//ACK
	}
	ClrSCL();
}

unsigned char DataRead (void)
{
	unsigned char i, datain=0, temp = 0x01;
	
	for(i=0;i<8;i++)
	{
		ClrSCL();
		SetSCL();
		if(ReadSDA())
			datain |= (temp << (7-i));
	}	
	ClrSCL();
	
	return(datain);
}

void AckI2C (void)
{

	ClrSCL();
	SetSDA();
	ClrSDA();
	SetSCL();
	ClrSCL();
	SetSDA();
}

void NackI2C (void)
{
	ClrSCL();
	SetSDA();
	SetSCL();
	ClrSCL();
}
/*****************************************************************************/
/*      WriteByte ():

 		Description: This function stores one data byte to the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss at which data to be stored
					databyte -> data byte to be stored 

		Return Value: none

*/
/*****************************************************************************/	  
void WriteByte (unsigned char bankno, unsigned int address, unsigned char databyte) // void WriteByte(EEPROM address, byte to write);
{
	Start();
	DeviceAddress(bankno,WRITE);

	AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
	AddressWrite((char)(address & 0x00FF));	//low byte address

	DataWrite(databyte);
	
	Stop();
	DelayI2C(100000);
}

/*****************************************************************************/
/*      WriteInt():

 		Description: This function stores one data int to the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss at which data to be stored
					databyte -> data byte to be stored 

		Return Value: none

*/
/*****************************************************************************/	  
void WriteshortInt(unsigned char bankno, unsigned int address, unsigned short int databyte) // void WriteByte(EEPROM address, byte to write);
{
//	dprintf("in write short int");
	para.tempshortint[0] = databyte;
	WriteByte(bankno,address,para.tempchar[0]);
	WriteByte(bankno,address+1,para.tempchar[1]);
}

/*****************************************************************************/
/*      WriteInt():

 		Description: This function stores one data int to the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss at which data to be stored
					databyte -> data byte to be stored 

		Return Value: none

*/
/*****************************************************************************/	  
void WriteInt(unsigned char bankno, unsigned int address, unsigned int databyte) // void WriteByte(EEPROM address, byte to write);
{
	para.tempint = databyte;
	WriteByte(bankno,address,para.tempchar[0]);
	WriteByte(bankno,address+1,para.tempchar[1]);
	WriteByte(bankno,address+2,para.tempchar[2]);
	WriteByte(bankno,address+3,para.tempchar[3]);
}

/*****************************************************************************/
/*      Writefloat():

 		Description: This function stores one data int to the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss at which data to be stored
					databyte -> float to be stored 

		Return Value: none

*/
/*****************************************************************************/	  
void Writefloat(unsigned char bankno, unsigned int address,float databyte) // void WriteByte(EEPROM address, byte to write);
{
	para.tempfloat = databyte;
	WriteByte(bankno,address,para.tempchar[0]);
	WriteByte(bankno,address+1,para.tempchar[1]);
	WriteByte(bankno,address+2,para.tempchar[2]);
	WriteByte(bankno,address+3,para.tempchar[3]);
}

/*****************************************************************************/
/*      ReadByte ():

 		Description: This function returns one data byte from 
					 the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					

		Return Value: returns one byte data from specified address

*/
/*****************************************************************************/	  
unsigned char ReadByte (unsigned char bankno, unsigned int address) // read byte ReadByte (EEPROM address);
{
	unsigned char datain;
	Start();
	DeviceAddress(bankno,WRITE);

	AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
	AddressWrite((char)(address & 0x00FF));	//low byte address

	Start();
	DeviceAddress(bankno,READ);
	datain = DataRead();
	NackI2C();
	Stop();

	return(datain);
}

/*****************************************************************************/
/*      ReadshortInt ():

 		Description: This function returns one data int from 
					 the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					

		Return Value: returns one int data from specified address

*/
/*****************************************************************************/	  
unsigned short int ReadshortInt(unsigned char bankno, unsigned int address) // read byte ReadByte (EEPROM address);
{
	para.tempchar[0] = ReadByte(bankno,address);
	para.tempchar[1] = ReadByte(bankno,address+1);

	return(para.tempshortint[0]);
}

/*****************************************************************************/
/*      ReadInt ():

 		Description: This function returns one data int from 
					 the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					

		Return Value: returns one int data from specified address

*/
/*****************************************************************************/	  
unsigned int ReadInt(unsigned char bankno, unsigned int address) // read byte ReadByte (EEPROM address);
{
	para.tempchar[0] = ReadByte(bankno,address);
	para.tempchar[1] = ReadByte(bankno,address+1);
	para.tempchar[2] = ReadByte(bankno,address+2);
	para.tempchar[3] = ReadByte(bankno,address+3);
	return(para.tempint);
}

/*****************************************************************************/
/*      ReadInt ():

 		Description: This function returns one data int from 
					 the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					

		Return Value: returns one int data from specified address

*/
/*****************************************************************************/	  
float Readfloat(unsigned char bankno, unsigned int address) // read byte ReadByte (EEPROM address);
{
	para.tempchar[0] = ReadByte(bankno,address);
	para.tempchar[1] = ReadByte(bankno,address+1);
	para.tempchar[2] = ReadByte(bankno,address+2);
	para.tempchar[3] = ReadByte(bankno,address+3);
	return(para.tempfloat);
}

/*****************************************************************************/
/*      WriteArray ():

 		Description: This function returns one data byte from 
					 the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					
					*buffer -> pointer to buffer from which data to be stored
					len		-> length of data of buffer to be stored

		Return Value: none

*/
/*****************************************************************************/	  
void WriteArray (unsigned char bankno, unsigned int address,char* bufadd,unsigned int len) //void WriteByte(EEPROM address, BUFFER address,no of bytes to write < 127);
{
unsigned int i,pageadd,length;
unsigned char writeflag=0;

	pageadd = address & 0xFF80;
	length = (128-(char)(address & 0x007F));
	if(len <= length)
		length = len;
	do
	{
		Start();
		DeviceAddress(bankno,WRITE);

		AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
		AddressWrite((char)(address & 0x00FF));	//low byte address

		for(i=0;i<length;i++)
			DataWrite(*(bufadd++));
		
		Stop();
		DelayI2C(100000);

		if(len<=length)
			writeflag=1;
		else
		{
			len -= length;
			if(len>128)
				length = 128;
			else
				length = len;

			address = pageadd + 128;
			pageadd = address;
		}
	}while (writeflag != 1);
}

/*****************************************************************************/
/*      WriteStringArray():

 		Description: This function returns one data byte from 
					 the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					
					*buffer -> pointer to buffer from which data to be stored
					len		-> length of data of buffer to be stored

		Return Value: none

*/
/*****************************************************************************/	  
void WriteStringArray(unsigned char bankno, unsigned int address,char* bufadd,unsigned int len) //void WriteByte(EEPROM address, BUFFER address,no of bytes to write < 127);
{
unsigned int i,pageadd,length;
unsigned char writeflag=0;
unsigned char stopwriteflag=0;

	pageadd = address & 0xFF80;
	length = (128-(char)(address & 0x007F));
	if(len <= length)
		length = len;
	do
	{
		Start();
		DeviceAddress(bankno,WRITE);

		AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
		AddressWrite((char)(address & 0x00FF));	//low byte address

		for(i=0;i<length;i++)
		{
			DataWrite(*bufadd);
//			if(*bufadd == '\0')
//			{
//				stopwriteflag = 1;
//				break;	
//			}
			bufadd++;
		}
		
		Stop();
		DelayI2C(100000);

		if(len<=length)
			writeflag=1;
		else
		{
			len -= length;
			if(len>128)
				length = 128;
			else
				length = len;

			address = pageadd + 128;
			pageadd = address;
		}
	}while ((writeflag != 1) && (stopwriteflag != 1));
}

/*****************************************************************************/
/*      ReadArray():

 		Description: This function reads the data bytes of specified length 
					 from the starting address specified and store into buffer.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					
					*buffer -> pointer to buffer from which read data to be stored
					len		-> no of bytes to be read from the EEPROM

		Return Value: none

*/
/*****************************************************************************/	  
void ReadArray(unsigned char bankno, unsigned int address,char* bufadd,unsigned int len) //void ReadArray(EEPROM address, BUFFER address, no of bytes to read);
{
	unsigned char i;

	Start();
	DeviceAddress(bankno,WRITE);

	AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
	AddressWrite((char)(address & 0x00FF));	//low byte address

	Start();
	DeviceAddress(bankno,READ);

	for(i=0;i<(len-1);i++)
	{
		*(bufadd++) = DataRead();
		AckI2C();		
	}
	*(bufadd++) = DataRead();
	NackI2C();
	Stop();

}

/*****************************************************************************/
/*      ReadArray():

 		Description: This function reads the data bytes of specified length 
					 from the starting address specified and store into buffer.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which data to be read					
					*buffer -> pointer to buffer from which read data to be stored
					len		-> no of bytes to be read from the EEPROM

		Return Value: none

*/
/*****************************************************************************/	  
void ReadstringArray(unsigned char bankno, unsigned int address,char* bufadd,unsigned int len) //void ReadArray(EEPROM address, BUFFER address, no of bytes to read);
{
	unsigned char i;

	Start();
	DeviceAddress(bankno,WRITE);

	AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
	AddressWrite((char)(address & 0x00FF));	//low byte address

	Start();
	DeviceAddress(bankno,READ);

	for(i=0;i<(len-1);i++)
	{
		*bufadd = DataRead();
		AckI2C();
//		if(*bufadd == '\0');
//		{
//			break;
//		}
		bufadd++;		
	}
	*(bufadd++) = DataRead();
	NackI2C();
	Stop();
}

/*****************************************************************************/
/*      FillMemory ():

 		Description: This function fills the memory with specified data byte 
					 from specified starting address upto specified length.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss from which specified data to be filled
					filldata -> specified data byte to be filled
					len		-> no of bytes to be filled from the EEPROM

		Return Value: none

*/
/*****************************************************************************/	  
void FillMemory (unsigned char bankno, unsigned int address,unsigned char filldata,unsigned int len) //void WriteByte(EEPROM address, BUFFER address,no of bytes to write < 127);
{
unsigned int i,pageadd,length;
unsigned char writeflag=0,pcent=1,cnt=0;
char str[20];


	pageadd = address & 0xFF80;
	length = (128-(char)(address & 0x007F));
	if(len <= length)
		length = len;
	do
	{
		Start();
		DeviceAddress(bankno,WRITE);

		AddressWrite((char)((address & 0xFF00) >> 8));	//high byte address
		AddressWrite((char)(address & 0x00FF));	//low byte address

		for(i=0;i<length;i++)
			DataWrite(filldata);
		
		Stop();
		DelayI2C(100000);

		if(++cnt>=5)
		{
			cnt=0;
			pcent++;
			if(pcent>=100)
				pcent=100;
			sprintf(str,"%d%%",(int)pcent);
			DispAt(29,str);
			Task_Sleep(1);
		}	

		if(len<=length)
			writeflag=1;
		else
		{
			len -= length;
			if(len>128)
				length = 128;
			else
				length = len;

			address = pageadd + 128;
			pageadd = address;
		}
	}while (writeflag != 1);
}
/*****************************************************************************/
/*      InitEEPROM ():

 		Description: This function initialize the EEPROM.

		Arguments: 	none

		Return Value: none

*/
/*****************************************************************************/	  
void InitEEPROM ()
{
	InitI2C();
}
/*****************************************************************************/
/*      EraseEEPROM ():

 		Description: This function Erase the EEPROM as per Bank selected.

		Arguments: 	bankno ->  Bank no to be Erase.

		Return Value: none

*/
/*****************************************************************************/	  
void EraseEEPROM (unsigned char bankno)
{
unsigned char i;
char str[20]; 	

	if(bankno == ALLBANK)
	{
		for(i=BANK1;i<MAXBANK;i++)
		{
			sprintf(str,"Erasing Bank %d  ",(int)(i+1));
			DispAt(1,str);
			DispAt(17,"In Progress.    ");
			FillMemory (i,0x0000,0xFF,0xFFFF);			
		}
	}
	else
	{
		sprintf(str,"Erasing Bank %d  ",(int)(bankno+1));
		DispAt(1,str);
		DispAt(17,"In Progress.    ");
		FillMemory (bankno,0x0000,0xFF,0xFFFF);
	}
	DispAt(1,"Erased EEPROM...");	
	DispAt(17,"  Successfully  ");
//	DelayI2C(50000000);
}
/*****************************************************************************/
/*      ReadSMSfromMemory():

 		Description: This function Reads SMS from memory as pointed by head 
					 and returns SMS string in buffer.

		Arguments: 	head ->  pointer of the head.
					*buffer -> buffer pointer

		Return Value: none

*/
/*****************************************************************************/	  
void ReadSMSfromMemory(unsigned int head,char *buffer)
{
unsigned int bankno,addptr;
unsigned int noofrecords,recordsize;

	noofrecords = NOOFRECORDS; recordsize = RECORDSIZE;	
	bankno = (unsigned int)((head+1)/noofrecords);
 	addptr = (head-noofrecords*bankno)*recordsize+1;
	ReadArray(bankno,addptr,buffer,(recordsize-1));
}

void ReadSMStimefromMemory(unsigned int head,char *buffer)
{
unsigned int bankno,addptr;
unsigned int noofrecords,recordsize;

	noofrecords = NOOFRECORDS; recordsize = RECORDSIZE;	
	bankno = (unsigned int)((head+1)/noofrecords);
 	addptr = (head-noofrecords*bankno)*recordsize+1;
	ReadArray(bankno,addptr,buffer,16);
}
/*****************************************************************************/
/*      WriteSMStoMemory():

 		Description: This function Writes SMS (stored in buffer) string to 
					 memory as pointed by tail.

		Arguments: 	tail ->  pointer of the tail.
					*buffer -> buffer pointer

		Return Value: none

*/
/*****************************************************************************/	  
void WriteSMStoMemory(unsigned int tail,char *buffer)
{
unsigned int bankno,addptr;
unsigned int noofrecords,recordsize;

	noofrecords = NOOFRECORDS; recordsize = RECORDSIZE;
	bankno = (unsigned int)((tail+1)/noofrecords);	
	addptr = (tail-noofrecords*bankno)*recordsize;
	WriteArray(bankno,addptr,buffer,recordsize);
}
/*****************************************************************************/
/*      StoreSMS():

 		Description: This function stores SMS string to 
					 memory with send status as pointed by tail.

		Arguments: 	tail ->  pointer of the tail.
					*buffer -> buffer pointer

		Return Value: none

*/
/*****************************************************************************/	  
unsigned char StoreSMS(unsigned int tailptr,char* buffer,unsigned char sendflag)
{
unsigned int len=0;
char i2cbuffer[200],i2cReadbuffer[200];

	i2cbuffer[len++] = sendflag;
	memmove(&i2cbuffer[len],buffer,SMS_LEN);		
	WriteSMStoMemory(tailptr,i2cbuffer);
	ReadSMSfromMemory(tailptr,i2cReadbuffer);
	if(memcmp(i2cbuffer+1,i2cReadbuffer,SMS_LEN)==0)
		return(TRUE);
	else
		return(FALSE);
}
/*****************************************************************************/
/*      SearchByDate():

 		Description: This function search SMS string by date. 

		Arguments: 	head ->  pointer of the head.
					*buffer -> buffer pointer
					*datetime -> pointer to date time structure

		Return Value: TRUE -> if match date SMS found, SMS is stored in buffer[]
					  FALSE -> if match date SMS not found

*/
/*****************************************************************************/	  
unsigned char SearchByDate(unsigned int head,char *buffer,struct datetime* dtptr)
{
unsigned int index;
char temp[5];
int date,month,year;

	ReadSMSfromMemory(head,buffer);
	index=0;

	while(index < 7)
	{
		if(buffer[index++] == ',')
		{	
			temp[0] = buffer[index++]; //date
			temp[1] = buffer[index++];
			temp[2] = '\0';
			date=atoi(temp);
							
			temp[0] = buffer[index++]; //month
			temp[1] = buffer[index++];
			temp[2] = '\0';
			month=atoi(temp);

			temp[0] = buffer[index++]; //year
			temp[1] = buffer[index++];
			temp[2] = '\0';
			year=atoi(temp);


			if(year == dtptr->YY)
				if(month == dtptr->MM)
					if(date == dtptr->DD)						
						return(TRUE);
			
		}
	}

	return(FALSE);
}
/*****************************************************************************/
/*      SearchByDateTime():

 		Description: This function search SMS string by date and time(hour only). 					 

		Arguments: 	head ->  pointer of the head.
					*buffer -> buffer pointer
					*datetime -> pointer to date time structure

		Return Value: TRUE -> if match date & time SMS found, SMS is stored in buffer[]
					  FALSE -> if match date & time SMS not found

*/
/*****************************************************************************/	  
unsigned char SearchByDateTime(unsigned int head, char *buffer,struct datetime* dtptr)
{
unsigned int index;
char temp[5];
unsigned int date,month,year,hour,minute;

	
//	ReadSMStimefromMemory(head,buffer);
//	WriteDebugMsg(buffer);
	ReadSMSfromMemory(head,buffer);
	index=0;

	while(index < 7)
	{
		if(buffer[index++] == ',')
		{				
			temp[0] = buffer[index++]; //date
			temp[1] = buffer[index++];
			temp[2] = '\0';
			date=atoi(temp);
			
			temp[0] = buffer[index++]; //month
			temp[1] = buffer[index++];
			temp[2] = '\0';
			month=atoi(temp);
		
			temp[0] = buffer[index++]; //year
			temp[1] = buffer[index++];
			temp[2] = '\0';
			year=atoi(temp);

			index++;
			temp[0] = buffer[index++]; //hour
			temp[1] = buffer[index++];
			temp[2] = '\0';
			hour=atoi(temp);
								
			temp[0] = buffer[index++]; //minute
			temp[1] = buffer[index++];
			temp[2] = '\0';
			minute=atoi(temp);

/*			sprintf(dispStr,"\n%d/%d/%d-%d:%d ",(int)year,(int)month,(int)date,(int)hour,(int)minute);
			WriteDebugMsg(dispStr);
			if(year == dtptr->YY)			
			{
				WriteDebugMsg("\nYY ");
				if(month == dtptr->MM)
				{
					WriteDebugMsg("MM ");
					if(date == dtptr->DD)						
					{
						WriteDebugMsg("DD ");
						if(hour == dtptr->hh)
						{
							WriteDebugMsg("hh ");
							if(minute == dtptr->mm)
							{
								WriteDebugMsg("mm ");
								return(TRUE);
							}
						}
					}
				}
			}*/

			if(year == dtptr->YY)
			{			
				if(month == dtptr->MM)
				{
					if(date == dtptr->DD)
					{						
						if(hour == dtptr->hh)
						{
							if(minute == dtptr->mm)
							{
//								ReadSMSfromMemory(head,buffer);
								return(TRUE);
							}
						}
					}
				}
			}
			break;							
		}
	}
		   
	return(FALSE);
}
/*****************************************************************************/
/*      SearchByTime():

 		Description: This function search SMS string by time. 

		Arguments: 	head ->  pointer of the head.
					*buffer -> buffer pointer
					*datetime -> pointer to date time structure

		Return Value: TRUE -> if match time SMS found, SMS is stored in buffer[]
					  FALSE -> if match time SMS not found

*/
/*****************************************************************************/	  
unsigned char SearchByTime(unsigned int head, char *buffer,struct datetime* dtptr)
{
unsigned int index;
char temp[5];
unsigned int hour,minute;

	ReadSMSfromMemory(head,buffer);
	index=0;

	while(index < 7)
	{
		if(buffer[index++] == ',')
		{				
			index+=7;
			temp[0] = buffer[index++]; //hour
			temp[1] = buffer[index++];
			temp[2] = '\0';
			hour=atoi(temp);
						
			temp[0] = buffer[index++]; //hour
			temp[1] = buffer[index++];
			temp[2] = '\0';
			minute=atoi(temp);

			if(hour == dtptr->hh)
				if(minute == dtptr->mm)
					return(TRUE);
			
			break;							
		}
	}
	return(FALSE);
}
/*****************************************************************************/
/*      SearchBurst():

 		Description: This function search SMS string by send status (if not sent). 
					 head pointer automatically incremented to next pointer after 
					 finding send status(not sent) string.

		Arguments: 	*head ->  pointer of the head.
					*buffer -> buffer pointer					

		Return Value: TRUE -> if not sent status of SMS found, SMS is stored in buffer[]
					  FALSE -> if all SMS have sent status found

*/
/*****************************************************************************/	  
unsigned char SearchBurst(unsigned int head, char *buffer)
{
unsigned int bankno,addptr,sendflag=SENT;
unsigned int noofrecords,recordsize;

	noofrecords = NOOFRECORDS; recordsize = RECORDSIZE;
	bankno = (unsigned int)((head+1)/noofrecords);
	addptr = (head-noofrecords*bankno)*recordsize;
	sendflag = ReadByte (bankno, addptr);

	if(sendflag == NOTSENT)
	{
		ReadArray(bankno,addptr+1,buffer,recordsize);	
		return(TRUE);
	}
	else
		return(FALSE);
}
/*****************************************************************************/
/*      RetrieveSMS():

 		Description: This function retrieve SMS string by different mode of
					 searching i.e by date, by date/time, by time & not sent. 
					 head pointer automatically incremented to next pointer after 
					 finding match date string.

		Arguments: 	mode -> mode of search
					*headptr ->  pointer of the head.
					*buffer -> buffer pointer
					datetime* -> pointer to date time structure

		Return Value: TRUE -> if matched SMS found
					  FALSE -> if matched SMS not found

*/
/*****************************************************************************/	  
unsigned char RetrieveSMS(unsigned char mode,unsigned int headptr,char *buffer,struct datetime* datetimeptr)
{
	switch (mode)
	 {
		case BYDATE:
			if(SearchByDate(headptr,buffer,datetimeptr))
				return(TRUE);
			break;
		case BYDATETIME:					
			//WriteDebugMsg("\nin DateTime");
			if(SearchByDateTime(headptr,buffer,datetimeptr))
				return(TRUE);
			break;
		case BYTIME:
			if(SearchByTime(headptr,buffer,datetimeptr))
				return(TRUE);
			break;
		case BURST:
			if(SearchBurst(headptr,buffer))
				return(TRUE);
			break;
	}
	return(FALSE);
}

/*****************************************************************************/
/*      AtoI():

 		Description: This function converts Ascii string into integer.Maximum
					 integer can be converted is 9999. If string is greater than "9999"
					 then it will return 0 value.

		Arguments: 	*str -> pointer to numeric string

		Return Value: returns integer value of numeric ASCII string or 
					  returns 0 value if string length is greater than 4.

*/
/*****************************************************************************/	  
unsigned int AtoI(const char* str)
{
unsigned int len,intval,i;
	len = strlen(str);
	if(len>4)return 0;

	while(*str)str++;
	str--;
	intval=0; i=1;
	while(len)
	{
		intval += ((*str)-0x30)*i;
		str--;i*=10;
		len--;
	}
	return(intval);
}
