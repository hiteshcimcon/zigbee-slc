

#ifndef AMR_CS5463
#define AMR_CS5463
#include "app/framework/include/af.h"
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
    #define nreset		PORTB_PIN(4)//  o/p       //PORTEbits.RE0
#else
    #define nreset		PORTC_PIN(1)//  o/p       //PORTEbits.RE0
#endif
#define INT1_ADC	PORTB_PIN(6)//  i/p      //PORTEbits.RE5
//#define int1		0//PORTDbits.RD9

/*#define sclk		PORTA_PIN(2)  //o/p        //PORTEbits.RE2				    			// PORT PIN ALLOCATED FOR CS 5460 OPERATION..
#define sdi		PORTA_PIN(1)  //o/p	//PORTEbits.RE3
#define sdo		PORTA_PIN(0)  //i/p	//PORTEbits.RE1
#define cs1		PORTA_PIN(3)  //o/p	//PORTEbits.RE4
*/
/*
 #define sclk	        PORTB_PIN(5)  //o/p        //PORTEbits.RE2				    			// PORT PIN ALLOCATED FOR CS 5460 OPERATION..
  #define sdi		PORTA_PIN(0)  //o/p	//PORTEbits.RE3
  #define sdo		PORTA_PIN(1)  //i/p	//PORTEbits.RE1
  #define cs1		PORTA_PIN(5)  //o/p	//PORTEbits.RE4
*/


#ifdef ISLC_3100_V9_2		    			// PORT PIN ALLOCATED FOR CS 5460 OPERATION..
  #define sclk	        PORTB_PIN(5)  //o/p        //PORTEbits.RE2				    			// PORT PIN ALLOCATED FOR CS 5460 OPERATION..
  #define sdi		PORTA_PIN(0)  //o/p	//PORTEbits.RE3
  #define sdo		PORTA_PIN(1)  //i/p	//PORTEbits.RE1
  #define cs1		PORTA_PIN(5)  //o/p	//PORTEbits.RE4
#elif (defined(ISLC_3100_V9)||defined(ISLC_3100_V7)||defined(ISLC_3100_V5_V6)||defined(ISLC_10)||defined(ISLC_T8)||defined(ISLC3300_T8_V2)||defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2))
  #define sclk	        PORTA_PIN(2)  //o/p        //PORTEbits.RE2				    			// PORT PIN ALLOCATED FOR CS 5460 OPERATION..
  #define sdi		PORTA_PIN(0)  //o/p	//PORTEbits.RE3
  #define sdo		PORTA_PIN(1)  //i/p	//PORTEbits.RE1
  #define cs1		PORTA_PIN(3)  //o/p	//PORTEbits.RE4
#elif (defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_480V_Rev3)||defined(ISLC_3100_V7_9))
  #define sclk	        PORTA_PIN(4)  //o/p        //PORTEbits.RE2				    			// PORT PIN ALLOCATED FOR CS 5460 OPERATION..
  #define sdi		PORTA_PIN(0)  //o/p	//PORTEbits.RE3
  #define sdo		PORTA_PIN(1)  //i/p	//PORTEbits.RE1
  #define cs1		PORTA_PIN(5)  //o/p	//PORTEbits.RE4

#else
   #error "please select SLC PCB version"
#endif


//#define ENERGY_INT_PIN	PORTDbits.RD9
//#define ENERGYIF		IFS1bits.INT2IF
//#define ENERGYIE		IEC1bits.INT2IE

#define	PCB5	0xf0						
#define	PCB1	0xf1
//#define	VRN	240.00												// DEFINE CALIBRATION VOLTAGE FOR RPHASE
//#define	IR	3.000								    			// DEFINE CALIBRATION CURRENT FOR RPHASE

/////////////////////////////////////
#ifdef ISLC_3100_480V_Rev3
        #define	VRN	415.696 //414.5               //414.6              //414.5		  	
        #define	IR	 3.000               //0.771              //3.000			
        #define PULSES	0.0300398        //0.029978874     //0.0300398
#else
        #define	VRN	    240.00									
        #define	IR	    3.000								
        #define PULSES	    0.03
        #define PULSES_120  0.015

		// DEFINE CALIBRATION FOR PULSES FOR 3P4W
#endif
/////////////////////////////////////

//#define PULSES	0.1											// DEFINE CALIBRATION FOR PULSES FOR 3P4W
//#define PULSES	0.03											// DEFINE CALIBRATION FOR PULSES FOR 3P4W

//#define CREEP_LIMIT	0.030										// DEFINE MINIMUM CURRENT LIMIT
#if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
	#define CREEP_LIMIT	0.050 //0.030		
#else
	#define CREEP_LIMIT	0.030										// DEFINE MINIMUM CURRENT LIMIT
#endif


#define	CT_PRIMARY		0x00									// DEFINE EEPROM ADDRESS FOR CT_PRIMARY VARIALBE STORAGE
#define	CT_SECONDARY	0X03									// DEFINE EEPROM ADDRESS FOR CT_SECONDARY  DATA STORAGE
#define	PT_RATIO		0X06									// DEFINE EEPROM ADDRESS FOR PT_RATIO VARIALBE STORAGE

#define	VR_MF			0X30									// DEFINE EEPROM ADDRESS FOR STATION ID VARIALBE STORAGE
#define	IR_MF			0X3f									// DEFINE EEPROM ADDRESS FOR STATION ID VARIALBE STORAGE
#define PULSES_R		0x4e									// DEFINE EEPROM ADDRESS FOR STATION ID VARIALBE STORAGE
#define KWHCOUNT_R		0x5d

#define PHASE_ANGLE_MF  (180/3.1428571428571428571428571428571)


extern unsigned long int pulses_r;
extern float newkwh;
extern float diff;


extern unsigned char datalow,datahigh,datamid;

extern unsigned char zempbyte;

extern unsigned long int zemplong;
extern unsigned long int reg_data_r[5];
extern unsigned char tempbyte;
extern unsigned char intesec;

extern float base;
extern float zempdoub;

extern float kbase;

extern float irdoub;

//extern float syskwh;
//extern float syskvarh;
//extern float syskvah;
//extern float syspf;
extern float vry;
//extern float pf1;
extern float pf;
extern float syskw;
extern float syskva;
//extern float syskvar;
extern float va1;
extern float w1;
extern float var1;
//extern float var2;
//extern float var3;
//extern float kw1;
extern float kwh;

extern float gunile1;
extern float mf1;
extern float mf2;
extern float mf3;
extern float mf4;

//extern float var_rdoub;

extern float vrdoub;

extern unsigned char CAL_page;
extern unsigned char power_on_flag;
extern unsigned short int emt_int_count,Kwh_count;
//extern unsigned int em_reset_counter;
extern unsigned char emt_pin_toggel;
extern unsigned long int Em_status_reg;
//extern float temperature;
extern char temperature1;
//extern unsigned char temperature2;
//extern unsigned char temperature3;
extern unsigned char check_day_burnar_flag;

void init_cs5463(void);
unsigned char receive_byte(void);
void transfer_byte(unsigned char);
void read_a_reg(unsigned char,unsigned char);
void write_to_reg(unsigned char,unsigned char,unsigned char,unsigned char,unsigned char);
void write_to_all_regs(unsigned char,unsigned char,unsigned char,unsigned char);
void calculate_3p4w(void);
float get_ieee(unsigned long int);
void calculate_kwh(void);
void Read_EnergyMeter(void);
void set_mf_energy(void);
void calibration_Energy(void);
void calculate_kwh_LampOFF(void);
void Calculate_burnhour_Lampoff(void);

#endif


