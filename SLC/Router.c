
#include <stdio.h>
#include "Generic.h"
#include "SLC/MSPI.h"
#include"DS1339.h"
#include "AMR_CS5463.h"
#include "Application_logic.h"
#include "protocol.h"
#include "micro/adc.h"
#include "GPS.h"

//////////////Calibration////////////
unsigned char Event_Delay_To_Store_Proper_Value=0;
unsigned char Restore_KWH_On_Power_Cycle =0,Hold_data_Cnt=0;
void Dali_Driver_Check(void);
float Dali_At_100Dim=0.0,Dali_At_75Dim=0.0,Dali_At_50Dim=0.0,Dali_At_25Dim=0.0,Dali_At_0Dim=0.0;
unsigned short int Old_Network_Leave_Hr_Cnt =0;
unsigned int  Network_Leave_Hr_Value=0;

unsigned short int Conflict_PAN_ID =0,Temp_PAN_ID =0,Blue_Light_Id =0;

unsigned char Lamp_ON_OFF =0,Lamp_Fail =0,Ballast_Fail =0,Day_Burner =0;
unsigned int Time_Slice_Cnt =0;
unsigned char Auto_EVENT_Send_Enable_Disable =0,Send_To_DCU=0;
unsigned char Read_Data_Retry_cnt =0,Old_Hr=0,Event_Detected=0,Send_Event_Data =0;
extern volatile unsigned int Tilt_Timeout_Cnt;
unsigned char Send_Data_Success =0;
unsigned char Tilt_detect_flag =0;
void SLC_RESET_FUNCTION(void);
extern unsigned char power_on_count;
unsigned char SLC_RESET_ONFREEJOIN_MODE =0;
unsigned char Addcnt =0;
unsigned char dimming_cnt=0,auto_address_done =0;
float ADC_Result_100=0,ADC_Result_50=0,ADC_Result_0=0;
unsigned char Time_Sync_GPS =0;
float Vr_Cal=0.0,Ir_Cal = 0.0,KW_Cal = 0.0,KW_Cal_120 =0.0;
unsigned char DI_GET_LOW =0,DI_GET_HIGH =0;
extern BYTE_VAL  GPS_error_condition;
extern unsigned char RTC_Faulty_Shift_to_Local_Timer;
void set_Linear_Logarithmic_dimming(unsigned char Curv_type);
///////////TILT//////////////////////////
unsigned int Tilt_counter=0;
void Tilt_Sensor_Detect(void);
//unsigned char Tilt_Occured=0;
unsigned int Tilt_Cnt=0;
unsigned char Tilt_Get_Previously=0;
/////////////////////////////////////
void Read_para_EEPROM(void);
void HardwareInit( void );
void ProcessNONZigBeeTasks(void);
void key_scan(void);
void store_data_in_mem(void);
void check_power_cycle(void);
void check_dimming_protection(void);						
void Commissioning_routine(void);
void SLC_main(void);
void Check_RTC(unsigned char time);
void Check_EMT(unsigned char time);
void Check_Lamp(unsigned char time);
void Check_photocell(unsigned char time);
void Check_CT(unsigned char time);
void Check_eeprom(void);
void Check_Slc_Peripheral(void);
void Check_Slc_Test_1(void);
void check_dimming_hardware(void);

BYTE       temp2,CAL_page;
unsigned char calibmode,single_time_execute=1;
unsigned int Photocell_Detect_Time = 5;
unsigned char half_sec;
unsigned char temp_frame_time,run_m_sch =0;
unsigned int minute_timer;
unsigned int RF_communication_check_Counter;
unsigned char read_energy_meter = 0,Set_default_perameter =0;
unsigned char sec_timer_flag = 0;
unsigned char save_run_hour=0;
unsigned char ee_default_state = 0,Photo_steady_cnt =0;
unsigned char one_sec = 0,one_sec_Ntw_Search = 0;
unsigned char temp_slc_event = 0,temp_slc_event1 = 0;
unsigned int rf_event_send_counter=0;
unsigned int temp_timer_delay=0;
unsigned char send_all_rf_event = 0;
unsigned char Master_event_generation = 0;
unsigned char Time_Out=0;
unsigned char cheak_emt_timer_sec = 0;
unsigned int Photocell_int_timer = 0;
unsigned char Pro_count =0,Protec_lock=0,protec_flag=0;
unsigned int Protec_sec=0;
unsigned char Start_dim_flag =0;
unsigned char send_event_with_delay = 0;
unsigned char send_event_delay_timer = 0;	
unsigned char Photo_feedback_status = 0;
unsigned int Time_change_dueto_DST_Timer = 0;
unsigned char send_gateway_data=0,read_attr_data=0,H_S=0,pre=0;
unsigned char one_min = 1,do_toggel = 1;
unsigned char do_min_timer = 0;
unsigned char do_on_timer = 5,Id_Send_Cnt=8,Id_Frame_Cnt =0;
unsigned long Network_Scan_Cnt,Network_Scan_Cnt_Time_out = 86400;
unsigned char lamp_on=0;
unsigned char  Rtc_Power_on_result = 0;
unsigned char check_reset_need_condition = 0,check_reset_need_condition_timer = 0;
unsigned int previous_rx_counter = 0,previous_tx_counter = 0;
unsigned char Tx_Rx_increase_counter = 0;
unsigned int Tx_Rx_not_increase_counter = 0;
unsigned int Slc_reset_counter;
unsigned char one_time_flag=0,Default_Para_Set=0;
unsigned char temp_var=0;
float Analog_Result;
float presentHourKwhBuffer[3],previousHourKwhBuffer[4];
unsigned char Send_Data_WithLastHourEnergy=0;
unsigned char bitsForStoredKwh = 0;
float daliAoADC_FLOAT;
extern unsigned int dali_dim_cnt,send_data_cnt;
extern unsigned char DALI_Ballast_Info[4][16],default_dali,lamp_off_to_on;
extern unsigned char answer,answer1;
extern unsigned char Lamp_type ;
extern unsigned char Single_time_run_cmd;
extern unsigned char Link_Key[];
extern unsigned char SLC_New_Manula_Mode_En,SLC_New_Manula_Mode_Val;
extern unsigned int Photocell_int_counter;
extern unsigned char set_do_flag ;	
extern float Dimvalue;				
extern unsigned char token_r=0,data_cnt=0,kk;
extern unsigned char Send_gatewaye_response,read_attr,hh;
extern unsigned int scann_chan ;
extern unsigned long int channel_set;
extern unsigned char Frame_send_timer,search_ntw,SLC_Rejoin_SameChannel;
extern int16u emberCounters[EMBER_COUNTER_TYPE_COUNT];
extern unsigned char Ember_network_up_send_frame;
extern unsigned int Lamp_off_on_Time;
extern unsigned char cheak_emt_timer_sec;
extern unsigned char SLC_Start_netwrok_Search;
extern unsigned char SLC_Start_netwrok_Search_Timeout;
extern unsigned char SLC_intermetent_netwrok_Search_Timeout;
extern unsigned char Read_RTC;
extern unsigned char Commissioning_timer;
extern unsigned char Enable_Security,address1;

unsigned char DimmerDriverSelectionProcess=0,DimmerDriver_SELECTION=0x02;
volatile unsigned char CheckAoDimmerForDetectionTimmer=0,CheckAoDimmerForDetection=0;
float full_brightPower,full_brightPower_at_30;
unsigned char dali_support=1;  // default Dali
//#if defined(DALI_SUPPORT)
extern unsigned char f_dalitx;
extern unsigned char f_dalirx;
unsigned char dali_delay =0;
//#endif

//+++++++++++++++++RS485 And DI2+++++++++++++++++++++++++


extern unsigned char RS485_Read_buff_success;
extern unsigned int RS485_Read_buff_Loc;
extern unsigned char RS485_Serial_Read_buff[];

extern unsigned char Serial_Read_buff_sam[];
extern unsigned char Read_buff_success;
extern unsigned int Read_buff_Loc;
//extern unsigned int Read_buff_Time_Out = 0;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*************************************************************************
Function Name: SLC_main
input: None.
Output: None.
Discription: this function is main function of SLC. all SLC related function
             called from this function. this is a main entry for SLC code.
*************************************************************************/
unsigned char HH=0;
void SLC_main(void)
{
  if(!one_time_flag)             // do all things here which required access to only one time.
  {
    one_time_flag=1;
    Configured_Event.Val =0xff;
    Restore_KWH_On_Power_Cycle =1;
    ////////
    /*
    A1 A0
    0  0  //ADC selection
    0  1  //PWM
    1  0  //DALI
    */
//    if(dali_support == 1)
//    {
//    //#if(defined(DALI_SUPPORT))
//        halSetLed(MUX_A0);	 //0	
//        halClearLed(MUX_A1);  //1
//    
//    }
//    //#else
//    else
//    {
//        halSetLed(MUX_A1);	 //0	
//        halClearLed(MUX_A0);  //1
//    
//    }

    //#endif

    
 
    /////////
    HardwareInit();														   // hardware init function call for peripehral call
    calibmode=0;														      // after calibration over make this flag as zero for normal function
    halResetWatchdog();             // Periodically reset the watchdog.
    Read_para_EEPROM();	

    //Select_RS485_Or_DI2(RS485_0_Or_DI2_1);
    //////select RS485 for EM Calibration/////////////
    Select_RS485_Or_DI2(RS485_0_Or_DI2_1);  
    /////////////////////////////
    Old_Hr =Time.Hour;
    temp_slc_event = 1;													// power on event generation.
    Motion_detected = 0;												// at power on condition reset motion detected flag.
    Set_Do(0);															       // make Lamp off initially.
    energy_time = 0;													   // Reset EM timer on start up.
    error_condition1.bits.b6 = 0;			// power on event generation.
    Pro_count = 0;
    Temp_Slc_Multi_group = check_multi_group(Group_val1.Val);     // set flag for SLC in multiple group.
    if(Commissioning_flag == 1)     // if SLC in Commissioning then run Lamp ON steps for commissioning.
    {
        Commissioning_routine();
        automode =0; //if anyhow power cycle occured and commissioning is not over then need to keep in manual mode
    }
    if(Commissioning_flag == 2) //need to set here to avoid time sync with gps.
    {
        //SLC_Test_Query_2_received=1; //get impact during test one routine

        Time_set_by_GPS =1;

    }
    test_result.bits.b6=0;				//GPS faulty.
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1) ||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
    //send_gps_command();             // send GPS command to stop all unwanted strings.

    if(GPS_Read_Enable == 1)
    {
        need_to_send_GPS_command = 1;
    }
    else
    {
      need_to_send_Shutdown_command = 1;
      //shutdown_GPS();
    }
#endif
/*Need to add below routine because In old Logic with Digital photocell,
When power cycle occured and photocell detects Dark then it make Lamp ON instantly.
In Analog Photocell not behave as above so need to add below routine.
*/
/////////////////////////////
#if (defined (ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
    for(HH =0;HH<20;HH++)
    {
      ADC_Process();
      Ember_Stack_run_Fun();           // call all ember tick function to increase stack timeing
    }
#endif
/////////////////////////////

    #ifndef ISLC3300_T8_V2
    if(Photo_feedback_pin == 1)
    {
      Photo_feedback = 0;
      photo_cell_toggel = 1;
    }
    else
    {
      Photo_feedback = 1;
      photo_cell_toggel = 0;
    }
    #endif
  }
  if(check_peripheral == 1)          // check peripherals if diagnosis command received from LG.
  {
    Check_Slc_Peripheral();
  }
  else
  {
    ProcessNONZigBeeTasks();        // all SLC related task call from here.
  }

}


/*************************************************************************
Function Name: ProcessNONZigBeeTasks
input: None.
Output: None.
Discription: this function handles all peripheral calling and logic functions
*************************************************************************/
unsigned char temp_dimming_value=0;


void ProcessNONZigBeeTasks(void)
{
    unsigned char j=0,i=0;
  EmberNetworkParameters networkParams;
  EmberNodeType nodeTypeResult = 0xFF;
  //unsigned char Display[200];
  if(Soft_Reboot == 235)
  {

    if(cheak_emt_timer_sec>4)
      {
        Soft_Reboot=0;
        halReboot();
      }

  }

  if(dali_support == 2)
  {
    detectDimmerDriver();
  }
  
//#if(defined(DALI_SUPPORT))
 if(dali_support == 1)
 {
  
  if (Cmd_Rcv_To_Change_Curv_Ype == 2)
  {
    if(cheak_emt_timer_sec>4)
    {

      Cmd_Rcv_To_Change_Curv_Ype =0;
        cheak_emt_timer_sec =0;
        if(set_do_flag == 1)
        {

          if(auto_address_done == 0)
          {

            Multiple_Dali_Driver_Handle();
            old_dim_val =Dimvalue +1;

          }
            auto_address_done =1;
            //halReboot();
        }
    }
  }
if(Send_Read_Scene_From_Dali == 2)
{

  if(cheak_emt_timer_sec>2)
  {
    Read_Dali_Scene();
    Send_Read_Scene_From_Dali =1;
  }
}
}
//#endif

#if (defined (ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
    if(Lamp_type == 0xF0) //Need to add odd value cince LG make client base combination
    {
      Tilt_Sensor_Detect();
    }
    ADC_Process();
#endif
  if(Read_RTC == 1)
  {
    Read_RTC = 0;
/////////////////////////////////////
    /*
        Below logic need to execute when RTC becomes faulty
        Shift to local timer when gps valid string available & RTC faulty flag true.
        This logic sync 3 times in a day by GPS.
    */
#if(defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)|| defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
    if((error_condition1.bits.b2 == 1)&&(RTC_Faulty_Shift_to_Local_Timer == 1))
    {
        Fill_RTC_BY_Local_Timer();
        if((Time.Hour == 0)||(Time.Hour == 8)||(Time.Hour == 16))
        {
            if(Time_Sync_GPS == 0)
            {
                Time_Sync_GPS =1;
                Time_set_by_GPS = 0;  // set time by gps, while gps valid string available.
                if(GPS_error_condition.bits.b7 == 1)   // invoke GPS if available so if sender's time wrong then SLC correct it by GPS.
                {
                  need_to_send_GPS_command = 1;
                }
            }

        }
        else
        {
          Time_Sync_GPS =0;
        }
    }
    else
    {
      read_ds1302_bytes();               // read RTC at every 500ms
    }
#else
       read_ds1302_bytes();               // read RTC at every 500ms
#endif

////////////////////////////////////
  }
/////////////////////////////////////////////////////////
//    if((dali_dim_cnt >= 15)&&(default_dali==1)&&(lamp_off_to_on==0))
//     {
//          delay(60060);     // 55 msec
//          delay(60060);
//          delay(60060);     // 55 msec
//          delay(60060);
//
//       Read_Dali_Ballast_Information(3,9);
//          delay(60060);     // 55 msec
//          delay(60060);
//          delay(60060);     // 55 msec
//          delay(60060);
//
//       Read_Dali_Ballast_Information(1,11);
//
//          delay(60060);     // 55 msec
//          delay(60060);
//          delay(60060);     // 55 msec
//          delay(60060);
//
//       Read_Dali_Ballast_Information(2,7);
//          delay(60060);     // 55 msec
//          delay(60060);
//          delay(60060);     // 55 msec
//          delay(60060);
//       //Read_Dali_Ballast_Information(0,3);
//       Read_Dali_Ballast_Information(0,5);
//          delay(60060);     // 55 msec
//          delay(60060);
//          delay(60060);     // 55 msec
//          delay(60060);
//
//
//        for(j=0;j<4;j++)
//        {
//          emberAfGuaranteedPrint("Dali_driver[%d]=", (j+1));
//          emberSerialWaitSend(APP_SERIAL);
//
//          for(i=0;i<16;i++)
//          {
//          emberAfGuaranteedPrint(",%d", DALI_Ballast_Info[j][i]);
//          emberSerialWaitSend(APP_SERIAL);
//          }
//
//          emberAfGuaranteedPrint("                                \n");
//          emberSerialWaitSend(APP_SERIAL);
//
//        }
//          emberAfGuaranteedPrint("\n");
//          emberSerialWaitSend(APP_SERIAL);
//
//        dimming_cnt = dimming_cnt + 50 ;
//        if(dimming_cnt >=255) dimming_cnt =0;
//
//        dali_dim_cnt=0;
//     }
//////////////////////////////////////////////////////////
  if(automode == PANIC_MODE)
  {
    Execute_Panic_App();

  }

  if(one_sec == 1)          // do all things which required at every sec.
  {
///////////////////////// Logic To Store EM kwh data at 15min ////////////////////////////////////
    
// Todo maulin : make bitsForStoredKwh= 0 with rtc time syn or DST applied or any change in time
//               and if change in hour,date,month or year than clear presentHourKwhBuffer and previousHourKwhBuffer
//               if (minute diffrence >15) then clear previousHourKwhBuffer  
        if(Restore_KWH_On_Power_Cycle == 1)
        {
          if(vrdoub>65)
          {
            Restore_KWH_On_Power_Cycle = 0;
            presentHourKwhBuffer[0] = kwh;
            presentHourKwhBuffer[1] = kwh;
            presentHourKwhBuffer[2] = kwh;
            previousHourKwhBuffer[0] = kwh;
            previousHourKwhBuffer[1] = kwh;
            previousHourKwhBuffer[2] = kwh;
            previousHourKwhBuffer[3] = kwh;
            
          }
        }
        else
        {
          if((Time.Min==15) && ((bitsForStoredKwh < 0x01) || (bitsForStoredKwh >= 0x08)))                // (Time.Min == lograte.min+15)
          {
            presentHourKwhBuffer[0]=kwh;
            bitsForStoredKwh = 0x01;       
          }
          else if(Time.Min==30 && (bitsForStoredKwh < 0x02))
          {
            presentHourKwhBuffer[1]=kwh;
            bitsForStoredKwh |= 0x02;
          }
          else if(Time.Min==45 && (bitsForStoredKwh < 0x04))
          {
            presentHourKwhBuffer[2]=kwh;
            bitsForStoredKwh|= 0x04;
          }
          else if(Time.Min==0 && (bitsForStoredKwh < 0x08))
          {
            bitsForStoredKwh|= 0x08;
            previousHourKwhBuffer[0]=presentHourKwhBuffer[0];
            previousHourKwhBuffer[1]=presentHourKwhBuffer[1];
            previousHourKwhBuffer[2]=presentHourKwhBuffer[2];
            previousHourKwhBuffer[3]=kwh;
          } 
        }
//    if(Date.Date==EM_Interval_Date.Date && Date.Month==EM_Interval_Date.Month && Date.Year==EM_Interval_Date.Year 
//       && Time.Hour==EM_Interval_Time.Hour && Time.Min==EM_Interval_Time.Min)
//    {
//      update_EM_Interval();   
//      if(Time.Min==15)  // (Time.Min == lograte.min+15)
//      {
//        presentHourKwhBuffer[0]=kwh;
//      }
//      else if(Time.Min==30)
//      {
//        presentHourKwhBuffer[1]=kwh;
//      }
//      else if(Time.Min==45)
//      {
//        presentHourKwhBuffer[2]=kwh;
//      }
//      else if(Time.Min==0)
//      {
//        previousHourKwhBuffer[0]=presentHourKwhBuffer[0];
//        previousHourKwhBuffer[1]=presentHourKwhBuffer[1];
//        previousHourKwhBuffer[2]=presentHourKwhBuffer[2];
//        previousHourKwhBuffer[3]=kwh;
//      }      
//    }
    
///////////////////////// END : Logic To Store EM kwh data at 15min ////////////////////////////////////
        
//#if (defined(ISLC_3100_V7_7))  
//    
//    //send_data_on_RS485_uart("RX485 TX\r\n",strlen("RX485 TX\r\n"));
//    //send_data_on_RS485("RX485 TX\r\n",strlen("RX485 TX\r\n"));
//    emberAfGuaranteedPrint("\r\n",2);
//    emberSerialWaitSend(APP_SERIAL); 
//    sprintf(Display,"\r\n[SLC:%04u][R_T:%02u/%02u/%02u %02u:%02u:%02u][L_T:%02u:%02u:%02u][T (%d)485_0/DI2_1:%02u][MUX:%02u][Boot:%04d][Mode:%u]",amr_id,Date.Date,Date.Month,Date.Year,Time.Hour,Time.Min,Time.Sec,Local_Time.Hour,Local_Time.Min,Local_Time.Sec,dali_support,RS485_0_Or_DI2_1,DimmerDriverSelectionProcess,Slc_reset_counter,automode);
//    emberAfGuaranteedPrint("%p", Display);
//    emberSerialWaitSend(APP_SERIAL);    
////    sprintf(Display,"[V:%4.2f][I:%4.2f][KW:%4.2f][KWH:%4.2f][Burn:%4.2f][Dim:%4.2f][Photo:%4.2f][DI2:%02u][Tilt:%02u][AI:%6.3f]",vrdoub,irdoub,w1/1000.0,kwh,lamp_burn_hour,Dimvalue,Photosensor_FootCandle,MOTION_DI1,TILT_DI1,Analog_Result);
//    sprintf(Display,"[V:%4.2f][I:%4.2f][KW:%4.2f][KWH:%4.2f][Burn:%4.2f][Dim:%4.2f][Photo:%4.2f][DI2:%02u][Tilt:%02u][AI:%6.3f]",vrdoub,irdoub,w1/1000.0,kwh,lamp_burn_hour,Dimvalue,Photosensor_FootCandle,MOTION_DI1,TILT_DI1,daliAoADC_FLOAT);
//    emberAfGuaranteedPrint(" %p", Display);
//    emberSerialWaitSend(APP_SERIAL);
//    
//#endif  
#if (defined(ISLC_3100_V7_9))  
    
//    //send_data_on_RS485_uart("RX485 TX\r\n",strlen("RX485 TX\r\n"));
//    //send_data_on_RS485("RX485 TX\r\n",strlen("RX485 TX\r\n"));
//    emberAfGuaranteedPrint("\r\n",2);
//    emberSerialWaitSend(APP_SERIAL); 
//    sprintf(Display,"\r\n[SLC:%04u][R_T:%02u/%02u/%02u %02u:%02u:%02u][L_T:%02u:%02u:%02u][AO-DALI:%02u][485_0/DI2_1:%02u][MUX:%02u][Boot:%04d][Mode:%u]",amr_id,Date.Date,Date.Month,Date.Year,Time.Hour,Time.Min,Time.Sec,Local_Time.Hour,Local_Time.Min,Local_Time.Sec,dali_support,RS485_0_Or_DI2_1,DimmerDriverSelectionProcess,Slc_reset_counter,automode);
//    emberAfGuaranteedPrint("%p", Display);
//    emberSerialWaitSend(APP_SERIAL);    
//    sprintf(Display,"[V:%4.2f][I:%4.2f][KW:%4.2f][KWH:%4.2f][Burn:%4.2f][Dim:%4.2f][Photo:%4.2f][DI2:%02u][Tilt:%02u][AI:%6.3f][LG:%02u]",vrdoub,irdoub,w1/1000.0,kwh,lamp_burn_hour,Dimvalue,Photosensor_FootCandle,MOTION_DI1,TILT_DI1,Analog_Result,Last_Gasp_Detect);
////    sprintf(Display,"[V:%4.2f][I:%4.2f][KW:%4.2f][KWH:%4.2f][Burn:%4.2f][Dim:%4.2f][Photo:%4.2f][DI2:%02u][Tilt:%02u][AI:%6.3f]",vrdoub,irdoub,w1/1000.0,kwh,lamp_burn_hour,Dimvalue,Photosensor_FootCandle,MOTION_DI1,TILT_DI1,daliAoADC_FLOAT);
//    emberAfGuaranteedPrint(" %p", Display);
//    emberSerialWaitSend(APP_SERIAL);
    
#endif     
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))

    sprintf(Display,"\r\nSLC ID = %u,LTime = %02u/%02u/%02u %02u:%02u:%02u",amr_id,Local_Date.Date,Local_Date.Month,Local_Date.Year,Local_Time.Hour,Local_Time.Min,Local_Time.Sec);
    emberAfGuaranteedPrint("%p", Display);
    emberSerialWaitSend(APP_SERIAL);
    sprintf(Display,"\r\nSLC ID = %u,Time = %02u/%02u/%02u %02u:%02u:%02u",amr_id,Date.Date,Date.Month,Date.Year,Time.Hour,Time.Min,Time.Sec);
    emberAfGuaranteedPrint("%p", Display);
    emberSerialWaitSend(APP_SERIAL);
//    sprintf(Display,"\r\nSLC ID = %u,GTime = %02u/%02u/%02u %02u:%02u:%02u",amr_id,GPS_Date.Date,GPS_Date.Month,GPS_Date.Year,GPS_Time.Hour,GPS_Time.Min,GPS_Time.Sec);
//    emberAfGuaranteedPrint("%p", Display);
//    emberSerialWaitSend(APP_SERIAL);
    sprintf(Display,",%4.2f,%4.2f,%4.2f,%4.2f,%4.2f",vrdoub,irdoub,w1/1000.0,kwh,Dimvalue);
    emberAfGuaranteedPrint(" %p", Display);
    emberSerialWaitSend(APP_SERIAL);

// #ifndef ISLC_10
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_T8))
    sprintf(Display,",DI1 = %u,AI1 = %6.3f",MOTION_DI1,Analog_Result);
    emberAfGuaranteedPrint(" %p", Display);
    emberSerialWaitSend(APP_SERIAL);
#endif

    sprintf(Display,"Photocell feedback = %u RTC_faulty = %u Photocell_faulty = %u",Photo_feedback_pin,RTC_faulty_detected,Photo_Cell_Ok);
    emberAfGuaranteedPrint(" %p", Display);
    emberSerialWaitSend(APP_SERIAL);

    sprintf(Display,"\r\nSLC Mode = %u SLC Pre mode = %u Photocell_int_timer = %u Photocell_Detect_Time = %u",automode,previous_mode,Photocell_int_timer,Photocell_Detect_Time);
    emberAfGuaranteedPrint(" %p", Display);
    emberSerialWaitSend(APP_SERIAL);
#endif

    check_Day_Light_Saving();        // check DST if applicable or not
    Ember_Stack_run_Fun();           // call all ember tick function to increase stack timeing

    if((Network_Scan_Cnt >= Network_Scan_Cnt_Time_out) || (SLC_Rejoin_SameChannel == 1))     // check SLC RF working or not at every defined time period default is 24hours
    {
      Network_Scan_Cnt =0;
      SLC_Rejoin_SameChannel =0;
      //      if((Valid_DCU_Flag==1)&&(search_ntw ==0))          // if SLC not receive any read data command from last 24 hours and netwrok is UP then do network rejoin process
      //      {
      //         emberAfGetNetworkParameters(&nodeTypeResult, &networkParams);     // save current channel in memory.
      //
      //         scann_chan=networkParams.radioChannel;
      //         scann_chan=0x00000001<<(scann_chan-11);
      //         if(scann_chan == 0)
      //         {
      //            scann_chan  =0x7fff;
      //         }
      //         channel_set=scann_chan<<11;
      //         halCommonSetToken(TOKEN_scann_chan,&scann_chan);
      //         Valid_DCU_Flag =0;                                         //added because if any other dcu have in same channel then need to leave network untill it get proper dcu added on 20DEC
      //         halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
      //
      //         Id_frame_received =0;
      //         search_ntw =1;
      //         RF_communication_check_Counter = 0;
      //         networkLeaveCommand();
      //      }
      //
      //      if(search_ntw ==1)                         // if network down then check reset need condition.
      {
        check_reset_need_condition = 1;                  // set flage to check MAC tx/rx counter if error occurs then do soft-reset so SLC become again active
        check_reset_need_condition_timer = 0;            // reset timer
        previous_rx_counter = emberCounters[0];          // copy ember current counter to previous counter.
        previous_tx_counter = emberCounters[1];          // copy ember current counter to previous counter.
        Tx_Rx_increase_counter = 0;                      // reset all previous increase/decrease counter
        Tx_Rx_not_increase_counter = 0;                  // reset all previous increase/decrease counter
        SLC_RESET_ONFREEJOIN_MODE =1;
        power_on_count =0;

      }
    }
////////RESET LOGIC ADDED FROM 2.0.32 version/////////////////
    if(Valid_DCU_Flag == 0)
    {
        if((SLC_RESET_ONFREEJOIN_MODE == 1)&&(power_on_count >= 1))
        {
            power_on_count =0;
            if(Network_Scan_Cnt_Time_out >= 86400)//this is configurable perameter so need to set hardcoded value for 24 hour
            {
                SLC_RESET_FUNCTION();
            }
        }
    }
    else
    {
      SLC_RESET_ONFREEJOIN_MODE =0;
    }
////////////////////////////////
    one_sec = 0;

    if(Send_gatewaye_response)                   // if ember getway command receive then send responce to getway
    {
      Send_gatewaye_response=0;
      send_gateway_data=1;
      send_fream();                             // send fream to DCU when flag generate for any sending message is pending.
    }
    if(read_attr)                               // if read attributed command receive from gatetway then send responce to getway.
    {
      read_attr=0;
      read_attr_data=1;
      send_fream();                             // send fream to DCU when flag generate for any sending message is pending.
    }
    
//////////////////AutoSendLogic//////////////////    
    if(((Auto_Send_Enable_Disable == 1)||((Auto_Send_Enable_Disable == 3)))&&(Valid_DCU_Flag == 1)&&(automode !=PANIC_MODE)) 
    {
        if(Event_Detected == 1)
        {
          if(Event_Send_Cnt>= Time_Slice)
          {
            if(Send_Data_Success == 1)
            {
                Send_Data_Success =0;
                Read_Data_Retry_cnt=0;
                if((Auto_ack_Rf_Track_Id == 25)||(Auto_ack_Rf_Track_Id == 26)) 
                {
                }
                else if(Auto_ack_Rf_Track_Id == 27)
                {
                   //Lamp_Fail =0;
                }
                else if(Auto_ack_Rf_Track_Id == 28)
                {
                  //Ballast_Fail =0;
                }
                else if(Auto_ack_Rf_Track_Id == 29)
                {
                  //Day_Burner =0;
                }
                Auto_Event_delet_data_from_mem(Auto_ack_Rf_Track_Id);
                Auto_ack_Rf_Track_Id =0;
            }
            if(Read_Data_send_cnt > Read_Data_send_Timer)
            {
                Read_Data_send_cnt =0;
                if(Read_Data_Retry_cnt <= Auto_Send_Retry) //5
                {
                  Read_Data_Retry_cnt++;
                  Send_To_DCU =1;
                  Send_Data_Success=0;
                  read_event_flag =0; //if dcu old version ask for event data then we must make it disable so set this flagh value 0
                  send_auto_event();
                }
                else
                {
                  Event_Send_Cnt =0;
                  Event_Detected =0;
                  Read_Data_Retry_cnt=0;
                }
            }
          }
        }
        else
        {
          Event_Send_Cnt =0;
        }
    }
    if(((Auto_Send_Enable_Disable == 2)||(Auto_Send_Enable_Disable == 3))&&(Valid_DCU_Flag == 1)&&(automode !=PANIC_MODE)) 
    {
        if(((Time.Hour % Auto_Send_Lograte_Interval) == 0)&&(Old_Hr !=Time.Hour))
        {   
            if(((Time.Min*60)+Time.Sec)>= Time_Slice)
            {
                if(Read_Data_send_cnt > Read_Data_send_Timer)
                {
                    Read_Data_send_cnt =0;
                    if(Read_Data_Retry_cnt <= Auto_Send_Retry) //5
                    {
                        Read_Data_Retry_cnt++;
                        if(Send_Data_Success == 0)
                        {
                          //  Send_Data =1;
                          //  Send_To_DCU =1;
                          if(Restore_KWH_On_Power_Cycle == 0)
                          {
                            Send_Data =1;
                            Send_To_DCU =1;
                            //Send_Data_WithLastHourEnergy =1;
                          }
                          else
                          {
                            Hold_data_Cnt++;
                            if(Hold_data_Cnt >1)
                            {
                              Hold_data_Cnt =0;
                              Restore_KWH_On_Power_Cycle =0;
                              presentHourKwhBuffer[0] = kwh;
                              presentHourKwhBuffer[1] = kwh;
                              presentHourKwhBuffer[2] = kwh;
                              previousHourKwhBuffer[0] = kwh;
                              previousHourKwhBuffer[1] = kwh;
                              previousHourKwhBuffer[2] = kwh;
                              previousHourKwhBuffer[3] = kwh;
                            }
                          }                            
                        }
                        else
                        {
                          Old_Hr =Time.Hour;
                          Send_Data_Success =0;
                        }
                    }
                    else
                    {
                      Read_Data_Retry_cnt =0;
                      Old_Hr =Time.Hour;
                      Send_Data_Success =0;
                    }
                }
            }
        }
        
        
         
    }
        ///////////////////Network Leave after configured Time out  
    //      Conflict_PAN_ID=emberGetPanId();
      if(Network_leave_Flag == 1)
      {
        if(Valid_DCU_Flag == 1)
        {
          if(Old_Network_Leave_Hr_Cnt != Network_Leave_Hr_Cnt)
          {
            Old_Network_Leave_Hr_Cnt = Network_Leave_Hr_Cnt;
            if(Network_Leave_Hr_Cnt == 0)
            {
              halCommonGetToken(&Network_Leave_Hr_Cnt,TOKEN_Network_Leave_Hr_Cnt);
              if(Network_Leave_Hr_Cnt != 0) //put this to avoid multiple time writting in Token when it is o value
              {
                Network_Leave_Hr_Cnt =0; //put this to avoid multiple time writting in Token
                halCommonSetToken(TOKEN_Network_Leave_Hr_Cnt,&Network_Leave_Hr_Cnt);
              }
            }
            else
            {
              halCommonSetToken(TOKEN_Network_Leave_Hr_Cnt,&Network_Leave_Hr_Cnt);
            }
          }
          
          if(Network_Leave_Hr_Cnt>=Network_Leave_Hr_Value)
          {
            Network_Leave_Hr_Cnt =0;
          //  Temp_PAN_ID=emberGetPanId();
           // emberGetRadioChannel();
          //  if(Conflict_PAN_ID != Temp_PAN_ID)
            scann_chan = 0x7FFE;            // mask channel 11 which use for commissioning.
            channel_set=scann_chan<<11;
            halCommonSetToken(TOKEN_scann_chan,&scann_chan);
                    
            Valid_DCU_Flag =0;              // start search for new DCU.
            halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
            Id_frame_received = 0;                // clear ID frame received flag to search new DCU in to new channel.
            RF_communication_check_Counter = 0;   // reset Id frame timer.
            networkLeaveCommand();
            
          }
          
        }
        else
        {
           Network_Leave_Hr_Cnt =0;
        }
    
    
      }
    /////////////////////////////////////////////////////////////  
      
//    if((Auto_EVENT_Send_Enable_Disable == 1)&&(Valid_DCU_Flag == 1)&&(automode !=PANIC_MODE)) 
//    {
//          if(Event_Detected == 1)
//          {
//              if(((error_condition.bits.b0 & Configured_Event.bits.b0) == 1)||((error_condition.bits.b1 & Configured_Event.bits.b1) == 1)||
//                 ((error_condition.bits.b2 & Configured_Event.bits.b2) == 1)||((error_condition.bits.b3 & Configured_Event.bits.b3) == 1)||
//                 ((error_condition.bits.b4 & Configured_Event.bits.b4) == 1)||((error_condition.bits.b5 & Configured_Event.bits.b5) == 1)||
//                 ((error_condition.bits.b6 & Configured_Event.bits.b6) == 1)||((error_condition.bits.b7 & Configured_Event.bits.b7) == 1))
//              {
//                      Send_Event_Data =1;
//              }
//              else
//              {
//                if(((error_condition1.bits.b0 & Configured_Event1.bits.b0) == 1)||((error_condition1.bits.b1 & Configured_Event1.bits.b1) == 1)||
//                 ((error_condition1.bits.b2 & Configured_Event1.bits.b2) == 1)||((error_condition1.bits.b3 & Configured_Event1.bits.b3) == 1)||
//                 ((error_condition1.bits.b4 & Configured_Event1.bits.b4) == 1)||((error_condition1.bits.b5 & Configured_Event1.bits.b5) == 1)||
//                 ((error_condition1.bits.b6 & Configured_Event1.bits.b6) == 1)||((error_condition1.bits.b7 & Configured_Event1.bits.b7) == 1))
//                  {
//                    Send_Event_Data =1;
//                  }
//              }
//              if(Send_Event_Data == 1)
//              {
//                  Time_Slice_Cnt++;
//                  if(Time_Slice_Cnt >= Time_Slice)
//                  {
//                      if(Read_Data_send_cnt > Read_Data_send_Timer)
//                      {
//                          Read_Data_send_cnt =0;
//                          if(Read_Data_Retry_cnt <= Auto_Send_Retry) //5
//                          {
//                              Read_Data_Retry_cnt++;
//                              if(Send_Data_Success == 0)
//                              {
//                                  Send_Data =1;
//                              }
//                              else
//                              {
//                               Event_Detected =0; 
//                               Send_Event_Data =0; 
//                               Send_Data_Success =0;
//                              }
//                          }
//                          else
//                          {
//                            Read_Data_Retry_cnt =0;
//                            Event_Detected =0; 
//                            Send_Event_Data =0; 
//                            Send_Data_Success =0;
//                          }
//                      }
//                  }
//              }
//          }
//    }
///////////////////////////////////////////////////    
    if(Id_frame_received == 1)																				// wait for ID frame sync with DCU to start event generation
    {
      if((Master_event_generation == 0)&&(automode !=PANIC_MODE))																	// check the master event generation for generate event
      {
        if((temp_slc_event != error_condition.Val) || (temp_slc_event1 != error_condition1.Val))		// compare previous digital val with new digital val if miss-match generate event
        {
            Event_Delay_To_Store_Proper_Value++;
          if(Event_Delay_To_Store_Proper_Value> 3)
          { 
            Event_Delay_To_Store_Proper_Value =0;

              if(Auto_event == 1)                // check for auto event send required or not
              {
                if(((temp_slc_event | 0x21) != (error_condition.Val | 0x21)) ||
                   (temp_slc_event1 != error_condition1.Val))		// compare previous digital val with new digital val if miss-match generate event.
                                                               // Do not generate auto event for feed back of lamp and photocell
                {
                  send_event_with_delay = 1;       // set flag for event send and reset timer to provide some delay so all analog value update
                  send_event_delay_timer = 0;	
                  send_data_direct_counter = 0;
                }

                event_counter++;                           // incriment event counter
                if(event_counter > 40)			                  // if it reaches to 40 stop event generation resent this at every 1 hour
                {
                  Master_event_generation = 1;
                  error_condition1.bits.b3 = 1;             																				
                }
                store_data_in_mem();                       // store event in memory
              }
              else
              {
              if((temp_slc_event | 0x20) != (error_condition.Val | 0x20)) 
              {
                event_counter++;                           // incriment event counter
                if(event_counter > 40)			                  // if it reaches to 40 stop event generation resent this at every 1 hour
                {
                  Master_event_generation = 1;
                  error_condition1.bits.b3 = 1;             															
                }
                
                store_data_in_mem();
              }
              }
              temp_slc_event = error_condition.Val;
              temp_slc_event1 = error_condition1.Val;
          }
        }
      }
    }

    if((check_reset_need_condition == 1) &&
       (check_reset_need_condition_timer > 90))     // check SLC need reset condition at every 3 min.
    {
      check_reset_need_condition_timer = 0;
      if(emberCounters[0] >= 0xffff)           // if counter reches to full value then make it zeor
      {
        emberCounters[0] = 0;
      }
      if(emberCounters[1] >= 0xffff)
      {
        emberCounters[1] = 0;
      }
      //        if(Ember_network_up_send_frame == 1)        // if network up in between then no need to check SLC need reset condition.
      //        {
      //          check_reset_need_condition = 0;
      //          Tx_Rx_increase_counter = 0;
      //          Tx_Rx_not_increase_counter = 0;
      //        }
      //        else
      //        {

      if((previous_rx_counter != emberCounters[0]) && (previous_tx_counter != emberCounters[1]))
      {
        previous_rx_counter = emberCounters[0];
        previous_tx_counter = emberCounters[1];
        Tx_Rx_increase_counter++;
        if(Tx_Rx_increase_counter > 3)             // if counter not match with previous condition then no need to do reset
        {
          Tx_Rx_increase_counter = 0;
          check_reset_need_condition = 0;

        }
      }
      else
      {
        Tx_Rx_not_increase_counter++;
        if(Tx_Rx_not_increase_counter > 3)         // if counters match with previous value then need to do reset
        {
          if(error_condition.bits.b0 == 0)         // if Lamp off then do reset
          {
            newkwh = kwh;			
            pulseCounter.long_data = pulses_r;
            halCommonSetToken(TOKEN_pulses_r,&pulses_r);
            halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);    // save all cumilative value in to EEPROM
            halReboot();                                                // do SLC soft reset
          }
          else
          {
            if(Tx_Rx_not_increase_counter > 500)            // if lamp on then wait for anothere 24 hours after that do soft reset.
            {
              newkwh = kwh;			                              // store all quimilative parameter in to EEPROM
              pulseCounter.long_data = pulses_r;
              //Slc_reset_counter++;
              //delay(1000);
              halCommonSetToken(TOKEN_pulses_r,&pulses_r);
              halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);
              //halCommonSetToken(TOKEN_Slc_reset_counter,&Slc_reset_counter);
              halReboot();               // soft-reset command for Ember
            }
          }
        }
      }
      //        }
    }

    if(new_dcu_detected == 1)          // if DCU first time detect then make lamp on/off for 30 sec.
    {
      new_dcu_detected = 0;
      DCU_detect_action();
    }
//#ifndef ISLC_10
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2))
    ADC_Process();							// read analog input and scale in to lumen according to configuration.
#endif

if((Time.Hour == 12) && (Time.Min == 0) && (Time.Sec < 2))								// calculate the new sunset or sunrise time at every 12:00 PM o'clock
    {
      GetSCHTimeFrom_LAT_LOG();                                        // make new Astro schedule time from Lat/long and date.
      Ember_Stack_run_Fun();                                           // call all ember tick function to increase stack timeing
      Astro_sSch[0].cStartHour = sunsetTime/3600;				
      Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
      Astro_sSch[0].cStopHour  = 23;							
      Astro_sSch[0].cStopMin   = 59;							
      Astro_sSch[0].bSchFlag   = 1;							

      Astro_sSch[1].cStartHour = 00;							
      Astro_sSch[1].cStartMin  = 00;							
      Astro_sSch[1].cStopHour  = sunriseTime/3600;			
      Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
      Astro_sSch[1].bSchFlag   = 1;							
      halCommonSetToken(TOKEN_schedule_Astro_sSch,&Astro_sSch[0]);
    }
	
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
    GPS_Delay_timer++;
    if(GPS_Delay_timer >= 5)
    {
     if(GPS_Read_Enable == 1)
     {
          GPS_Delay_timer = 0;
          if(need_to_send_GPS_command == 1)
          {
            send_gps_command();
          }
          else if(need_to_send_Shutdown_command == 1)
          {
            shutdown_GPS();
          }
     }
    }
#endif
  }

  send_fream();									// send fream to DCU when flag generate for any sending message is pending.


//  if(Photo_steady_cnt == 0)
//  {
//    if((Photo_feedback_pin == 0) && (Photo_feedback_status == 0))
//    {
//      Photo_feedback_status = 1;
//      Photocell_int_counter++;	
//      Photo_steady_cnt =1;
//    }
//    else if((Photo_feedback_pin == 1) && (Photo_feedback_status == 1))
//    {
//      Photo_feedback_status = 0;
//      Photocell_int_counter++;
//      Photo_steady_cnt =1;
//    }
//    else
//    {
//
//    }			
//  }

  Ember_Stack_run_Fun();             // call all ember tick function to increase stack timeing
  if((SLC_Test_Query_2_received == 0) && (Start_dimming_test == 0))
  {
    Read_EnergyMeter(); 																						// read energy meter parameter
    if((power_on_flag == 1) && (energy_meter_ok == 0))                 																	// wait for power on flag for check logic and calculate energy parameter
    {
      power_on_flag = 0;																						// resent power on flag
      error_condition1.bits.b4 = 0;	
      //Enrgy_Save_Time_Slot_identify();
      calculate_3p4w();    																					// claculate energy parameter
      check_interlock();   																					// check interlock 				
    }

    if(energy_time >= 60)									// if low level not detected up to 60 sec generate EM fault
    {
      energy_meter_ok = 1;                                   // set flag for EM Fault
      error_condition1.bits.b4 = 1;                          // set Bit in digital status for EM Fault
      read_a_reg(0x01,0x1e);                                 // read Status resistor from EM chip
      pulseCounter.byte[0] = datalow;
      pulseCounter.byte[1] = datamid;
      pulseCounter.byte[2] = datahigh;
      pulseCounter.byte[3] = 0x00;
      Em_status_reg = pulseCounter.long_data;                // Clear Status resistor in EM chip
      write_to_reg(0x01,0x5e,0x00,0x00,0x80);
      init_cs5463();									                                // Reset EM engine
      energy_time = 1;								                               // Make EM fault timer to 1 so no trip checking up to EM not helthy. 	
    }
    if((automode == 1)||(automode == 4)||(automode == 6)||(automode == 7)) //1 photocell 4 astro with photocell 6 civil twilight with photocell 7 mix mode trip check only in photocell mode
    {
        check_photocell_interlock();							                      // check photo cell related trip.

    }
    else
    {
        photo_cell_timer =0;
        photo_cell_toggel_timer = 0;
	photo_cell_toggel_counter = 0;

        if(Photo_feedback == 1)
        {
          error_condition.bits.b5 = 1;
        }
        else
        {
          error_condition.bits.b5 = 0;
        }
    }
    Ember_Stack_run_Fun();                                   // call all ember tick function to increase stack timeing
    if(SLC_New_Manula_Mode_En == 0)                          // check SLC in new manual mode or not if in new manual mode then stop running normal mode.
    {
      if(automode != 7)
      {
        if(ballast_type == 1)						                          // electronic ballast selected
        {
          Master_Sch_Match = 0;
          if(adaptive_light_dimming == 0)                    // normal dimming schedule based
          {
            check_dimming_protection();			                   // check analog output short circuit
            if((Start_dim_flag == 1) && (set_do_flag == 1))  // check dimming schedule after Lamp on.
            {
              Check_MASTER_Schedule();						                 // Master sch 15/04/11
            }
          }
          else if(adaptive_light_dimming == 1)               // adaptive light based dimming
          {
            check_dimming_protection();			                   // check analog output short circuit				
            if(lamp_lock == 0)					                          // check if Lamp not lock then do dimming
            {
              if((Start_dim_flag == 1) && (set_do_flag == 1))
              {
                adaptive_dimming();                          // Do lamp dimming according to light sensor.
              }
            }	
          }
          //          else if(adaptive_light_dimming == 2)     // motion sensor based dimming
          //         {
          //           check_dimming_protection();			 // check analog output short circuit				
          //            if(lamp_lock == 0)					 // check if Lamp not lock then do dimming
          //            {
          //              if((Start_dim_flag == 1) && (set_do_flag == 1))        // check dimming schedule after Lamp on.
          //              {
          //                Motion_Based_dimming();	
          //              }						
          //            }
          //          }		
        }
        else
        {
          Check_MASTER_Schedule();						                        // if ballast type is magnatic thne check-
                                                                // dimming schedule and if schedule match then-
                                                                // do lamp off instand of dimming
        }
        ////////dimming logic////////////////////////////

        if(Master_Sch_Match == 0)						                         // Master sch 15/04/11
        {
          check_logic();                                        // check SLC mod and so lamp on/off according to that mode.
          Ember_Stack_run_Fun();                                // call all ember tick function to increase stack timeing
        }
      }
      else
      {
        if((RTC_faulty_detected == 1) || (RTC_New_fault_logic_Enable == 0))
        {
          if(run_m_sch == 1)            // if SLC mode is mix-mode then check mix-mode entry at every second
          {
            run_m_sch = 0;
            if((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0))  // check if vlotage under-over trip or Lamp lock condition not occur
            {
              check_Mix_Mode_schedule();  // check mix-mode schedule
              //Run_Mix_Mode_Sch();
            }
          }
          if((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0))   // check if vlotage under-over trip or Lamp lock condition not occur
          {
            Run_Mix_Mode_Sch();           // if mix-mode schedule match then do lamp operation according to value.
          }
        }
        else
        {
          previous_mode = automode;  // store mode value before chage.
          halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
          automode = 1;								
          halCommonSetToken(TOKEN_automode, &automode);
#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
              sprintf(Display,"\r\nPrevious mode change by Mixmode function");
              emberAfGuaranteedPrint(" %p", Display);
              emberSerialWaitSend(APP_SERIAL);
#endif
        }
          /////////////Mix mode logic////////////
      }		
    }
    else
    {
      if((error_condition.bits.b2 == 0) || (Lamp_lock_condition == 0))        // check if vlotage under-over trip or Lamp lock condition not occur
      {
        if(SLC_New_Manula_Mode_Val == 100)   // if new manual mode dimm value = 100 then off lamp
        {
          Set_Do(0);	
        }
        else
        {
          Set_Do(1);                                   // on Lamp and do dimming according to configured value.
          check_dimming_protection();
          if((Start_dim_flag == 1) && (set_do_flag == 1))
          {
            dimming_applied(SLC_New_Manula_Mode_Val);						// set dimm value to dimming
          }
        }

        if(SLC_New_Manual_Mode_Timer > 0)    // if new manual mode timer over then switch to normal mode.
        {
          if(SLC_New_Manual_Mode_counter >= ((SLC_New_Manual_Mode_Timer)*60))
          {
            SLC_New_Manual_Mode_Timer = 0;
            SLC_New_Manual_Mode_counter = 0;
            SLC_New_Manula_Mode_Val = 101;
            SLC_New_Manula_Mode_En = 0;
            halCommonSetToken(TOKEN_SLC_New_Manual_Mode_Timer,&SLC_New_Manual_Mode_Timer);
            halCommonSetToken(TOKEN_SLC_New_Manula_Mode_Val,&SLC_New_Manula_Mode_Val);
          }
        }

      }
    }

    if((Time.Hour == 12) && (Time.Min == 0) && (Time.Sec == 0))								// calculate the new sunset or sunrise time at every 12:00 PM o'clock
    {
      GetSCHTimeFrom_LAT_LOG();                                        // make new Astro schedule time from Lat/long and date.
      Ember_Stack_run_Fun();                                           // call all ember tick function to increase stack timeing
      Astro_sSch[0].cStartHour = sunsetTime/3600;				
      Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
      Astro_sSch[0].cStopHour  = 23;							
      Astro_sSch[0].cStopMin   = 59;							
      Astro_sSch[0].bSchFlag   = 1;							

      Astro_sSch[1].cStartHour = 00;							
      Astro_sSch[1].cStartMin  = 00;							
      Astro_sSch[1].cStopHour  = sunriseTime/3600;			
      Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
      Astro_sSch[1].bSchFlag   = 1;							
      halCommonSetToken(TOKEN_schedule_Astro_sSch,&Astro_sSch[0]);
    }

    if(save_run_hour == 1)																	// Save Burn Hour at every 3 hours.
    {
      save_run_hour = 0;
      halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);
    }	

    if((check_day_burnar_flag == 1) && (day_burner_timer >= 3)) // check day burnar happening of not after Lamp switch from on/off or off/on
    {
      day_burner_timer = 0;
      check_day_burnar_flag = 0;                // read lamp status after 3 sec to became current stable.
      Check_day_burning_fault();								        // check day burning fault.
    }
  }
  else
  {
    if(Start_dimming_test == 0)
    {
      Set_Do(0);	
//      DI_GET_HIGH =0;
//      DI_GET_LOW =0;
//      cheak_emt_timer_sec = 0;
    }
    else                                        // if dimming test command receive from application then start test
    {
      Lamp_off_on_Time = 0;
      Set_Do(1);
      dim_inc_val = 0;
      dimming_applied(Test_dimming_val);       // set percentage of dimming to configured value.


//        if(cheak_emt_timer_sec > 1)
//        {
//          cheak_emt_timer_sec = 0;
//          if(MOTION_DI1 == 1)
//          {
//            DI_GET_HIGH = 1;
//          }
//          if(MOTION_DI1 == 0) //DI High detect code has reverse logic
//          {
//            DI_GET_LOW = 1;
//          }
//        }

    }
  }  																							// check slc is working in which logic

  if((Id_frame_received == 0) && (RF_communication_check_Counter >= Id_Send_Cnt))									// Send ID fream after every 30 second until not get replay from DCU
  {

    if(!Valid_DCU_Flag)      // if SLC not found valid DCU then send ID frame
    {
      send_id_frame = 1;

      RF_communication_check_Counter = 0;
      Id_Frame_Cnt++;
      Id_Send_Cnt=9;
      if(Id_Frame_Cnt>=3)         // if responce of ID frame not received 3 times then leave current network and search another network.
      {
        send_id_frame = 0;
        Id_Frame_Cnt =0;
        if(search_ntw ==0)      // check for network status like joind or not
        {
          networkLeaveCommand(); //if network joined then apply this command to leave current network.
        }

      }
    }
    else
    {
      //Id_Send_Cnt=30;               // if SLC joind in network then send ID frame at every 30 sec, up to not receive responce.
      Id_Send_Cnt=30+Addcnt;               // if SLC joind in network then send ID frame at every 30 sec, up to not receive responce.
      Addcnt=Addcnt + 5;
      if(Addcnt >220){
          Addcnt =221;
      }
      send_id_frame = 1;
      RF_communication_check_Counter = 0;
    }
  }
  else if(Id_frame_received == 1)							// If ID received from DCU then send event when generate
  {						
    if(read_event_flag == 1)												// Send pending event when DCU send read event command
    {
      if((Unicast_time_delay/3) >= 5)
      {
        if(rf_event_send_counter >= Unicast_time_delay/3)
        {
          rf_event_send_counter = 0;
          send_all_rf_event = 1;	
        }
      }
      else
      {
        if(rf_event_send_counter >= 5)
        {
          rf_event_send_counter = 0;
          send_all_rf_event = 1;	
        }
      }
    }
//    if((idimmer_en == 1) && (idimmer_Id_frame_received == 0) && (RF_communication_check_Counter >= 15))
//    {
//      send_idimmer_id_frame = 1;                // if idimmer flage enable then request for Idimmer id frame at every 15 sec to DCU.
//      RF_communication_check_Counter = 0;	
//    }	

  }
  else
  {
  }

  Check_Slc_Peripheral();																	// check all SLC peripheral

  if((change_Link_key_timer > 30) && (change_Link_key == 1))	 // if Link key change command received then
                                                              // after 30 sec change link key and start searching
                                                              // new network with this new Key.																		
  {
    change_Link_key = 0;
    change_Link_key_timer = 0;	

    emberAfGetNetworkParameters(&nodeTypeResult, &networkParams);     // Get and store current channel from module.

    scann_chan=networkParams.radioChannel;
    scann_chan=0x00000001<<(scann_chan-11);
    if(scann_chan == 0)
    {
      scann_chan  =0x7fff;
    }
    channel_set=scann_chan<<11;
    halCommonSetToken(TOKEN_scann_chan,&scann_chan);
    channel_set=scann_chan<<11;
    search_ntw =1;
    Valid_DCU_Flag =0;                                  //added because if any other dcu have in same channel then need to leave network untill it get proper dcu added on20DEC BJP
    halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
    Id_frame_received = 0;                              // clear Id frame received flage because SLC join with new DCU after network leave.
    RF_communication_check_Counter = 0;
    networkLeaveCommand();                              // leave current network.
  }

  if((need_to_send_lat_long == 1) && (Auto_Gps_Data_Send == 1))       //&& (GPS_Data_Valid == 3)
  {
        need_to_send_lat_long = 0;
        send_get_mode_replay_auto = 1;   // send lat/long value automatically when SLC join valid DCU first time.
//        halCommonSetToken(TOKEN_need_to_send_lat_long,&need_to_send_lat_long);
  }

  if(RTC_New_fault_logic_Enable == 1)    // check for logic enable or not.
  {
#if(defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)|| defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
    if((error_condition1.bits.b2 == 1)&&(RTC_Faulty_Shift_to_Local_Timer == 1)&&(RTC_need_to_varify == 1)&&(check_RTC_logic_cycle == 1))//condition becomes true  when synce either by GPS or DCU.
    {
        read_ds1302_bytes();       //to check if RTC is ok after sync by GPS or DCu.
        check_RTC_faulty_logic();
        Fill_RTC_BY_Local_Timer(); //IF rtc NOT OK file time by local timer
    }
    else
    {
        check_RTC_faulty_logic();
    }
#else
    check_RTC_faulty_logic();
#endif
  }
}


/*******************************************************************************
HardwareInit
All port directioning and SPI must be initialized before calling ZigBeeInit().

For demonstration purposes, required signals are configured individually.
*******************************************************************************/
void HardwareInit(void)
{
  init_ds1302();
  init_cs5463();                          // Init Energy Meter IC
  check_power_cycle();
}


/*************************************************************************
Function Name: Read_para_EEPROM
input: None.
Output: None.
Discription: this function is used to read all stored variables from NV memory
*************************************************************************/
void Read_para_EEPROM(void)
{
  unsigned char temp,i,j;


  halCommonGetToken(&Default_Para_Set,TOKEN_Default_Para_Set);  // Read default Parameters.
  if( Default_Para_Set != 'E')   // check if default parameters set of not if not then reset all parametes.
  {
    Default_Para_Set ='E';
    Set_default_perameter = 1;
    //emberAfGuaranteedPrint("\r\n%p", "Set_default_perameter = 1");
    halCommonSetToken(TOKEN_Default_Para_Set,&Default_Para_Set);
  }

  halCommonGetToken(&automode,TOKEN_automode);

  if((automode > 8) || (Set_default_perameter == 1))
  {
#if (defined(ISLC_T8)||defined(ISLC3300_T8_V2))
    automode = 0;
    previous_mode = 0;
#else
    automode = 0;
    previous_mode = 1;
#endif
    halCommonSetToken(TOKEN_automode, &automode);
    halCommonSetToken(TOKEN_previous_mode,&previous_mode);
  }
//  sprintf(Display,"automode =%d",automode);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);
  halCommonGetToken(&temp,TOKEN_temp);
  if(Set_default_perameter == 1)
  {
    temp = 0;
  }
  if(automode == 0)
  {
    if(temp == 1)
    {
      Set_Do_On = 1;
    }
    else if(temp == 0)
    {
      Set_Do_On = 0;
    }
  }


  halCommonGetToken(&amr_id,TOKEN_amr_id);
  halCommonGetToken(&Blue_Light_Id,TOKEN_Blue_Light_Id);
//  sprintf(Display,"amr_id =%d",amr_id);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);


  //if((amr_id > 65000) || (amr_id == 0) || (Set_default_perameter == 1))
  if((amr_id == 0) || (Set_default_perameter == 1))
  {
    amr_id = 1;	
   // amr_id = 111111;
    Blue_Light_Id =0;
    halCommonSetToken(TOKEN_Blue_Light_Id,&Blue_Light_Id);
    halCommonSetToken(TOKEN_amr_id,&amr_id);
  }
  	
  halCommonGetToken(&Latitude,TOKEN_Latitude);
//  delay(100);
  if(Set_default_perameter == 1)
  {
    Latitude = 42.3584;	
    halCommonSetToken(TOKEN_Latitude,&Latitude);
  }
//  sprintf(Display,"Latitude=%f",Latitude);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);

  if((Latitude > 90) || (Latitude < -90))

  {
    Latitude = 42.3584;
  }

  halCommonGetToken(&longitude,TOKEN_longitude);
  if(Set_default_perameter == 1)
  {
    longitude = -71.0597;
    halCommonSetToken(TOKEN_longitude,&longitude);
  }
//  sprintf(Display,"longitude=%f",longitude);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);
  if((longitude >  180) || (longitude < -180))
  {
    longitude = -71.0597;
  }

  halCommonGetToken(&utcOffset,TOKEN_utcOffset);
  if(Set_default_perameter == 1)
  {
    utcOffset = -4.0;
    halCommonSetToken(TOKEN_utcOffset,&utcOffset);
  }

  if((utcOffset > 13) || (utcOffset < -12))
  {
    utcOffset = -4.0;
  }
//  sprintf(Display,"utcOffset=%f",utcOffset);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);

  halCommonGetToken(&Sunset_delay,TOKEN_Sunset_delay);
  if(Set_default_perameter == 1)
  {
    Sunset_delay = 0;
    halCommonSetToken(TOKEN_Sunset_delay,&Sunset_delay);
  }

//  sprintf(Display,"Sunset_delay=%d",Sunset_delay);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);
  if((Sunset_delay > 7200) || (Sunset_delay < -7200))
  {
    Sunset_delay = 0;
    pulseCounter.int_data = 0;
    halCommonSetToken(TOKEN_Sunset_delay,&Sunset_delay);
  }

  halCommonGetToken(&Sunrise_delay,TOKEN_Sunrise_delay);
  if(Set_default_perameter == 1)
  {
    Sunrise_delay = 0;
    halCommonSetToken(TOKEN_Sunrise_delay,&Sunrise_delay);
  }
//  sprintf(Display,"Sunrise_delay=%d",Sunrise_delay);
//  emberAfGuaranteedPrint("\r\n%p", Display);
  delay(10);
  if((Sunrise_delay > 7200) || (Sunrise_delay < -7200))
  {
    Sunrise_delay = 0;
    pulseCounter.int_data = 0;
    halCommonSetToken(TOKEN_Sunrise_delay,&Sunrise_delay);
  }

  GetSCHTimeFrom_LAT_LOG();								//get sunset sunrise schedule			
  Astro_sSch[0].cStartHour = sunsetTime/3600;				// V6.1.10
  Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		// V6.1.10
  Astro_sSch[0].cStopHour  = 23;							// V6.1.10
  Astro_sSch[0].cStopMin   = 59;							// V6.1.10
  Astro_sSch[0].bSchFlag   = 1;							// V6.1.10

  Astro_sSch[1].cStartHour = 00;							// V6.1.10
  Astro_sSch[1].cStartMin  = 00;							// V6.1.10
  Astro_sSch[1].cStopHour  = sunriseTime/3600;			// V6.1.10
  Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;		// V6.1.10									
  Astro_sSch[1].bSchFlag   = 1;							// V6.1.10

  halCommonSetToken(TOKEN_schedule_Astro_sSch,&Astro_sSch[0]);


  halCommonGetToken(&sSch[0],TOKEN_schedule);
  halCommonGetToken(&sch_day[0],TOKEN_sch_day);
  for(j=0;j<1;j++)																// V6.1.10
  {
    for(i=0;i<10;i++)															// V6.1.10
    {
      if(i==0)
      {
          if((sSch[j][i].cStartHour > 23) || (Set_default_perameter == 1)) sSch[j][i].cStartHour = 18;
          if((sSch[j][i].cStartMin > 59) || (Set_default_perameter == 1))	sSch[j][i].cStartMin = 0;
          if((sSch[j][i].cStopHour > 23) || (Set_default_perameter == 1)) sSch[j][i].cStopHour = 23;
          if((sSch[j][i].cStopMin > 59) || (Set_default_perameter == 1)) sSch[j][i].cStopMin = 59;
          if((sSch[j][i].bSchFlag > 1) || (Set_default_perameter == 1)) sSch[j][i].bSchFlag = 1;
      }
      else if(i==1)
      {
          if((sSch[j][i].cStartHour > 23) || (Set_default_perameter == 1)) sSch[j][i].cStartHour = 0;
          if((sSch[j][i].cStartMin > 59) || (Set_default_perameter == 1))	sSch[j][i].cStartMin = 0;
          if((sSch[j][i].cStopHour > 23) || (Set_default_perameter == 1)) sSch[j][i].cStopHour = 6;
          if((sSch[j][i].cStopMin > 59) || (Set_default_perameter == 1)) sSch[j][i].cStopMin = 0;
          if((sSch[j][i].bSchFlag > 1) || (Set_default_perameter == 1)) sSch[j][i].bSchFlag = 1;
      }
      else{
          if((sSch[j][i].cStartHour > 23) || (Set_default_perameter == 1)) sSch[j][i].cStartHour = 0;
          if((sSch[j][i].cStartMin > 59) || (Set_default_perameter == 1))	sSch[j][i].cStartMin = 0;
          if((sSch[j][i].cStopHour > 23) || (Set_default_perameter == 1)) sSch[j][i].cStopHour = 0;
          if((sSch[j][i].cStopMin > 59) || (Set_default_perameter == 1)) sSch[j][i].cStopMin = 0;
          if((sSch[j][i].bSchFlag > 1) || (Set_default_perameter == 1)) sSch[j][i].bSchFlag = 0;
      }
    }
    if((sch_day[j] == 0xFF) || (Set_default_perameter == 1))	sch_day[j] = 0x7F;
  }

  if(Set_default_perameter == 1)
  {
    halCommonSetToken(TOKEN_schedule,&sSch[0]);
    halCommonSetToken(TOKEN_sch_day,&sch_day[0]);
  }



  halCommonGetToken(&Day_light_harvesting_start_offset,TOKEN_Day_light_harvesting_start_offset);
//  sprintf(Display,"Day_light_harvesting_start_offset=%d",Day_light_harvesting_start_offset);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);

  if((Day_light_harvesting_start_offset < -18000) || (Day_light_harvesting_start_offset > 18000) || (Set_default_perameter == 1))
  {
    Day_light_harvesting_start_offset = 0;	
  }	

  halCommonGetToken(&Day_light_harvesting_stop_offset,TOKEN_Day_light_harvesting_stop_offset);
  if((Day_light_harvesting_stop_offset < -18000) || (Day_light_harvesting_stop_offset > 18000) || (Set_default_perameter == 1))
  {
    Day_light_harvesting_stop_offset = 0;	
  }

  Day_light_harves_time();
  DLH_sSch[0].cStartHour = Day_light_harvesting_start_time/3600;				// V6.1.10		
  DLH_sSch[0].cStartMin  = (Day_light_harvesting_start_time%3600)/60;		// V6.1.10
  DLH_sSch[0].cStopHour  = 23;							// V6.1.10
  DLH_sSch[0].cStopMin   = 59;							// V6.1.10
  DLH_sSch[0].bSchFlag   = 1;							// V6.1.10

  DLH_sSch[1].cStartHour = 00;							// V6.1.10
  DLH_sSch[1].cStartMin  = 00;							// V6.1.10
  DLH_sSch[1].cStopHour  = Day_light_harvesting_stop_time/3600;			// V6.1.10
  DLH_sSch[1].cStopMin   = (Day_light_harvesting_stop_time%3600)/60;		// V6.1.10									
  DLH_sSch[1].bSchFlag   = 1;




  halCommonGetToken(&Master_sSch[0],TOKEN_Master_schedule);
  halCommonGetToken(&sch_Master_day[0],TOKEN_Master_sch_day);
  for(j=0;j<1;j++)																// V6.1.10
  {	
    for(i=0;i<10;i++)															// V6.1.10
    {
        if(i == 0)
        {
             if((Master_sSch[j][i].cStartHour > 23) || (Set_default_perameter == 1)) Master_sSch[j][i].cStartHour = 18;
             if((Master_sSch[j][i].cStartMin > 59) || (Set_default_perameter == 1))	Master_sSch[j][i].cStartMin = 0;
             if((Master_sSch[j][i].cStopHour > 23) || (Set_default_perameter == 1))	Master_sSch[j][i].cStopHour = 23;
             if((Master_sSch[j][i].cStopMin > 59)	 || (Set_default_perameter == 1))	Master_sSch[j][i].cStopMin = 59;
             if((Master_sSch[j][i].bSchFlag > 1)	 || (Set_default_perameter == 1))	Master_sSch[j][i].bSchFlag = 1;
             if((Master_sSch[j][i].dimvalue > 100) || (Set_default_perameter == 1))	Master_sSch[j][i].dimvalue = 0;

        }
        else if(i == 1)
        {
          if((Master_sSch[j][i].cStartHour > 23) || (Set_default_perameter == 1)) Master_sSch[j][i].cStartHour = 0;
          if((Master_sSch[j][i].cStartMin > 59) || (Set_default_perameter == 1))	Master_sSch[j][i].cStartMin = 0;
          if((Master_sSch[j][i].cStopHour > 23) || (Set_default_perameter == 1))	Master_sSch[j][i].cStopHour = 6;
          if((Master_sSch[j][i].cStopMin > 59)	 || (Set_default_perameter == 1))	Master_sSch[j][i].cStopMin = 0;
          if((Master_sSch[j][i].bSchFlag > 1)	 || (Set_default_perameter == 1))	Master_sSch[j][i].bSchFlag = 1;
          if((Master_sSch[j][i].dimvalue > 100) || (Set_default_perameter == 1))	Master_sSch[j][i].dimvalue = 0;

        }
        else
        {
          if((Master_sSch[j][i].cStartHour > 23) || (Set_default_perameter == 1)) Master_sSch[j][i].cStartHour = 0;
          if((Master_sSch[j][i].cStartMin > 59) || (Set_default_perameter == 1))	Master_sSch[j][i].cStartMin = 0;
          if((Master_sSch[j][i].cStopHour > 23) || (Set_default_perameter == 1))	Master_sSch[j][i].cStopHour = 0;
          if((Master_sSch[j][i].cStopMin > 59)	 || (Set_default_perameter == 1))	Master_sSch[j][i].cStopMin = 0;
          if((Master_sSch[j][i].bSchFlag > 1)	 || (Set_default_perameter == 1))	Master_sSch[j][i].bSchFlag = 0;
          if((Master_sSch[j][i].dimvalue > 100) || (Set_default_perameter == 1))	Master_sSch[j][i].dimvalue = 0;
        }
    }
    if((sch_Master_day[j] == 0xFF) || (Set_default_perameter == 1))	sch_Master_day[j] = 0x7F;				
  }

  if(Set_default_perameter == 1)
  {
        halCommonSetToken(TOKEN_Master_schedule,&Master_sSch[0]);
        halCommonSetToken(TOKEN_Master_sch_day,&sch_Master_day[0]);
  }

#if (defined(ISLC_10) || defined(ISLC_3100_7P_V1))     // for shunt hardware
  vrdoub = 0.475199165;
  irdoub = 0.11522299;
  pulses_r = 320;
#else

    #ifdef SMALL_CITI_2000
      vrdoub = 0.44579867;
      irdoub = 0.115281; //0.1154125
      pulses_r = 301;//[301 after calibration]//323; //need to calibrate this
    #else
      vrdoub = 0.444267;// 0.44401625;
      irdoub = 0.248402;//0.249616225;
      pulses_r = 648;// 647;
    #endif


#endif
////////////////

#ifdef ISLC_3100_480V_Rev3
        vrdoub = 0.337805;// added based on 12 Dec mail from sandip 0.33756656;//0.3381145;  //0.337407547;               //0.3346963;
        irdoub = 0.247792;//0.2478934;//0.247823;   //0.063858273;               //0.249616225;
        pulses_r = 284;//  change according to sandip mail on 18 Jan2017//425;//434;//428;;//434;//428;      //281;                     //284;
#endif
///////////////

/////////////////Calibration///////////
#if  (defined(ISLC_3100_V9_2)||defined(ISLC_3100_V7)|| defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216))

   halCommonGetToken(&Vr_Cal,TOKEN_Vr_Cal);
   halCommonGetToken(&Ir_Cal,TOKEN_Ir_Cal);
   halCommonGetToken(&KW_Cal,TOKEN_KW_Cal);
////////////H120///////////
  halCommonGetToken(&KW_Cal_120,TOKEN_KW_Cal_120);
        if((Set_default_perameter == 1) || (KW_Cal_120 == 0))
	{
		KW_Cal_120 = 325;  //set 325 value date according to mail from sandip 14Dec2016
                halCommonSetToken(TOKEN_KW_Cal_120,&KW_Cal_120);
	}
//////////////////////

  if((Set_default_perameter == 1) || (Vr_Cal == 0))
  {

    Vr_Cal = 0.444267;//0.44401625;
    halCommonSetToken(TOKEN_Vr_Cal,&Vr_Cal);

  }
  if((Set_default_perameter == 1) || (Ir_Cal == 0))
  {

    Ir_Cal = 0.248402;//0.249616225;
    halCommonSetToken(TOKEN_Ir_Cal,&Ir_Cal);

  }
  if((Set_default_perameter == 1) || (KW_Cal == 0))
  {

    KW_Cal = 648;//647;
    halCommonSetToken(TOKEN_KW_Cal,&KW_Cal);

  }
  vrdoub = Vr_Cal;
  irdoub = Ir_Cal;
  pulses_r = (unsigned long) KW_Cal;
//////////////
#elif (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
   halCommonGetToken(&Vr_Cal,TOKEN_Vr_Cal);
   halCommonGetToken(&Ir_Cal,TOKEN_Ir_Cal);
   halCommonGetToken(&KW_Cal,TOKEN_KW_Cal);
   halCommonGetToken(&KW_Cal_120,TOKEN_KW_Cal_120);
  if((Set_default_perameter == 1) || (KW_Cal_120 == 0))
  {
          KW_Cal_120 = 1749;
          halCommonSetToken(TOKEN_KW_Cal_120,&KW_Cal_120);
  }

  if((Set_default_perameter == 1) || (Vr_Cal == 0))
  {
    Vr_Cal = 0.459758;
    halCommonSetToken(TOKEN_Vr_Cal,&Vr_Cal);
  }
  if((Set_default_perameter == 1) || (Ir_Cal == 0))
  {
    Ir_Cal = 0.256713667;
    #if defined (ISLC_3100_V7_9)
      Ir_Cal = 0.027144;
    #endif
    
    halCommonSetToken(TOKEN_Ir_Cal,&Ir_Cal);
  }
  if((Set_default_perameter == 1) || (KW_Cal == 0))
  {

    KW_Cal = 3499;
    halCommonSetToken(TOKEN_KW_Cal,&KW_Cal);
  }
  vrdoub = Vr_Cal;
  irdoub = Ir_Cal;
  pulses_r = (unsigned long) KW_Cal;
  //////////////
#elif defined(ISLC_3100_480V_Rev3) //require here if need to load direct calibrated value
  halCommonGetToken(&Vr_Cal,TOKEN_Vr_Cal);
   halCommonGetToken(&Ir_Cal,TOKEN_Ir_Cal);
   halCommonGetToken(&KW_Cal,TOKEN_KW_Cal);
   

  if((Set_default_perameter == 1) || (Vr_Cal == 0))
  {

    Vr_Cal = 0.337805;//0.33756656;//0.44401625;
    halCommonSetToken(TOKEN_Vr_Cal,&Vr_Cal);

  }
  if((Set_default_perameter == 1) || (Ir_Cal == 0))
  {

    Ir_Cal = 0.247792;//0.2478934;//0.249616225;
    halCommonSetToken(TOKEN_Ir_Cal,&Ir_Cal);

  }
  if((Set_default_perameter == 1) || (KW_Cal == 0))
  {

    KW_Cal = 284;//  change according to sandip mail on 18 Jan2017//425;//434;//428;//434;//647;
    halCommonSetToken(TOKEN_KW_Cal,&KW_Cal);

  }
  vrdoub = Vr_Cal;
  irdoub = Ir_Cal;
  pulses_r = (unsigned long) KW_Cal;

#endif
////////////////////////////////////////


  set_mf_energy();

  halCommonGetToken(&pulses_r,TOKEN_pulses_r);

  delay(10);

  if((Set_default_perameter == 1))
  {
    pulses_r = 0;
    pulseCounter.long_data = pulses_r;
    halCommonSetToken(TOKEN_pulses_r,&pulses_r);
  }

//  kwh=(float)pulses_r * (PULSES/701);
  kwh=(((float)pulses_r*mf3));
  newkwh = kwh;

  halCommonGetToken(&lamp_burn_hour,TOKEN_lamp_burn_hour);
  if((Set_default_perameter == 1))
  {
    lamp_burn_hour=0;
    halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);
  }

  halCommonGetToken(&lamp_current,TOKEN_lamp_current);
  halCommonGetToken(&detect_lamp_current,TOKEN_detect_lamp_current);
  if((detect_lamp_current != 1) || (Set_default_perameter == 1))
  {
    detect_lamp_current = 0;

    halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);


    lamp_current = 0;
    halCommonSetToken(TOKEN_lamp_current,&lamp_current);

  }

  current_creep_limit = lamp_current * 0.1;			// find current creep limit for checking lamp ON/OFF

//  if(current_creep_limit < 0.03)
//  {
//    current_creep_limit = 0.03;						// if current creep limit less then 0.03 make it 0.03	
//  }
//  else if(current_creep_limit > 0.1)
//  {
//    current_creep_limit = 0.1;						// if current creep limit more then 0.1 make it 0.1
//  }
//  else
//  {
//
//  }
//////////////////////////
  #if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))

  if(current_creep_limit < 0.05)
  {
    current_creep_limit = 0.05;						// if current creep limit less then 0.03 make it 0.03	
  }
  else if(current_creep_limit > 0.1)
  {
    current_creep_limit = 0.1;						// if current creep limit more then 0.1 make it 0.1
  }
  else
  {

  }

#else
  if(current_creep_limit < 0.03)
  {
    current_creep_limit = 0.03;						// if current creep limit less then 0.03 make it 0.03	
  }
  else if(current_creep_limit > 0.1)
  {
    current_creep_limit = 0.1;						// if current creep limit more then 0.1 make it 0.1
  }
  else
  {

  }
#endif
////////////////////////////  

  halCommonGetToken(&Vol_hi,TOKEN_Vol_hi);
//  if((Vol_hi < 30) || (Vol_hi > 400) || (Set_default_perameter == 1))
//  {
//    Vol_hi = 260;
//    halCommonSetToken(TOKEN_Vol_hi,&Vol_hi);
//  }
//////////////////////////////////
 #ifdef ISLC_3100_480V_Rev3
  if((Vol_hi < 30) || (Vol_hi > 600) || (Set_default_perameter == 1))
#else
  if((Vol_hi < 30) || (Vol_hi > 400) || (Set_default_perameter == 1))
#endif
  {
    #ifdef ISLC_3100_480V_Rev3
        Vol_hi = 530;
    #else
        Vol_hi = 260;
    #endif
     halCommonSetToken(TOKEN_Vol_hi,&Vol_hi);
  }
//////////////////////////////////
//  sprintf(Display,"Vol_hi =%d",Vol_hi);
//  emberAfGuaranteedPrint("\r\n%p", Display);
//  delay(1000);
  halCommonGetToken(&Vol_low,TOKEN_Vol_low);
  if((Vol_low < 30) || (Vol_low > 400) || (Set_default_perameter == 1))
  {
    Vol_low = 90;
    halCommonSetToken(TOKEN_Vol_low,&Vol_low);
  }


  halCommonGetToken(&Curr_Steady_Time,TOKEN_Curr_Steady_Time);

  if((Curr_Steady_Time > 1200) || (Set_default_perameter == 1))
  {
    Curr_Steady_Time = 600;
    halCommonSetToken(TOKEN_Curr_Steady_Time,&Curr_Steady_Time);
  }


  delay(10);


  halCommonGetToken(&Per_Val_Current,TOKEN_Per_Val_Current);
  if((Per_Val_Current < 10) || (Per_Val_Current > 90) || (Set_default_perameter == 1))
  {
#ifdef NEW_LAMP_FAULT_LOGIC
    Per_Val_Current = 70;
#elif defined(OLD_LAMP_FAULT_LOGIC)
	   Per_Val_Current = 20;
#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif
    halCommonSetToken(TOKEN_Per_Val_Current,&Per_Val_Current);
  }

  halCommonGetToken(&Lamp_Fault_Time,TOKEN_Lamp_Fault_Time);

#ifdef NEW_LAMP_FAULT_LOGIC
  if((Lamp_Fault_Time > 1800) || (Set_default_perameter == 1))
  {
    Lamp_Fault_Time = 600;
    halCommonSetToken(TOKEN_Lamp_Fault_Time,&Lamp_Fault_Time);
  }
#elif defined(OLD_LAMP_FAULT_LOGIC)
  if((Lamp_Fault_Time > 600) || (Set_default_perameter == 1))
  {
    Lamp_Fault_Time = 30;
    halCommonSetToken(TOKEN_Lamp_Fault_Time,&Lamp_Fault_Time);
  }
#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

  halCommonGetToken(&Lamp_off_on_Time,TOKEN_Lamp_off_on_Time);
  if((Lamp_off_on_Time > 600) || (Set_default_perameter == 1))
  {
    Lamp_off_on_Time = 0; //according to sudhanshu set it 0 on 2Sep2015
    halCommonSetToken(TOKEN_Lamp_off_on_Time,&Lamp_off_on_Time);
  }

  halCommonGetToken(&Lamp_faulty_retrieve_Count,TOKEN_Lamp_faulty_retrieve_Count);
  if((Lamp_faulty_retrieve_Count < 1) || (Lamp_faulty_retrieve_Count > 20) || (Set_default_perameter == 1))
  {
    Lamp_faulty_retrieve_Count = 3;
    halCommonSetToken(TOKEN_Lamp_faulty_retrieve_Count,&Lamp_faulty_retrieve_Count);
  }

#ifdef NEW_LAMP_FAULT_LOGIC
  halCommonGetToken(&KW_Threshold,TOKEN_KW_Threshold);
  if((Set_default_perameter == 1) || (KW_Threshold == 0))
  {
    KW_Threshold = 0;
    halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);
    detect_lamp_current = 0;
  }
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif


  halCommonGetToken(&Broadcast_time_delay,TOKEN_Broadcast_time_delay);
  if((Broadcast_time_delay < 1) || (Broadcast_time_delay > 60) || (Set_default_perameter == 1))
  {
    Broadcast_time_delay = 2;
    halCommonSetToken(TOKEN_Broadcast_time_delay,&Broadcast_time_delay);
  }

  halCommonGetToken(&Unicast_time_delay,TOKEN_Unicast_time_delay);
  if((Unicast_time_delay < 1) || (Unicast_time_delay > 60) || (Set_default_perameter == 1))
  {
    Unicast_time_delay = 30;
    halCommonSetToken(TOKEN_Unicast_time_delay,&Unicast_time_delay);
  }

  delay(10);
  halCommonGetToken(&Lamp_lock_condition,TOKEN_Lamp_lock_condition);
  if((Lamp_lock_condition > 1) || (Set_default_perameter == 1))
  {
    Lamp_lock_condition = 0;
    halCommonSetToken(TOKEN_Lamp_lock_condition,&Lamp_lock_condition);
  }

  halCommonGetToken(&analog_input_scaling_high_Value,TOKEN_analog_input_scaling_high_Value);
  if((analog_input_scaling_high_Value > 65000) || (Set_default_perameter == 1))
  {
    analog_input_scaling_high_Value = 50000;
    pulseCounter.int_data = 0;
    halCommonSetToken(TOKEN_analog_input_scaling_high_Value,&analog_input_scaling_high_Value);
  }
//  halResetWatchdog();   // Periodically reset the watchdog.
  halCommonGetToken(&analog_input_scaling_low_Value ,TOKEN_analog_input_scaling_low_Value );
  if((analog_input_scaling_low_Value > 65000) || (Set_default_perameter == 1))
  {
    analog_input_scaling_low_Value = 0;
    halCommonSetToken(TOKEN_analog_input_scaling_low_Value,&analog_input_scaling_low_Value);
  }	


  halCommonGetToken(&desir_lamp_lumen ,TOKEN_desir_lamp_lumen );
  if((desir_lamp_lumen > 65000) ||(desir_lamp_lumen == 0) || (Set_default_perameter == 1))
  {
    desir_lamp_lumen = pulseCounter.int_data = 50000;
    halCommonSetToken(TOKEN_desir_lamp_lumen,&desir_lamp_lumen);
  }

  halCommonGetToken(&lumen_tollarence ,TOKEN_lumen_tollarence );
  if((lumen_tollarence > 100) || (Set_default_perameter == 1))
  {
    lumen_tollarence = 0;
    halCommonSetToken(TOKEN_lumen_tollarence ,&lumen_tollarence );
  }

  halCommonGetToken(&ballast_type ,TOKEN_ballast_type );
  if((ballast_type > 1) || (Set_default_perameter == 1))
  {
    ballast_type = 1;
    halCommonSetToken(TOKEN_ballast_type,&ballast_type);
  }

  halCommonGetToken(&adaptive_light_dimming,TOKEN_adaptive_light_dimming);
  if((adaptive_light_dimming > 3) || (Set_default_perameter == 1))
  {
    adaptive_light_dimming = 0;
    halCommonSetToken(TOKEN_adaptive_light_dimming,&adaptive_light_dimming);
  }

  delay(10);

  halCommonGetToken(&Motion_pulse_rate ,TOKEN_Motion_pulse_rate );
  if(Set_default_perameter == 1)
  {
    Motion_pulse_rate = 1;
    halCommonSetToken(TOKEN_Motion_pulse_rate,&Motion_pulse_rate);
  }

  halCommonGetToken(&Motion_dimming_percentage  ,TOKEN_Motion_dimming_percentage );
  if((Motion_dimming_percentage > 100) || (Set_default_perameter == 1))
  {
    Motion_dimming_percentage = 50;
    halCommonSetToken(TOKEN_Motion_dimming_percentage,&Motion_dimming_percentage);
  }

  halCommonGetToken(&Motion_dimming_time  ,TOKEN_Motion_dimming_time );
  if((Motion_dimming_time > (120 * 60)) || (Set_default_perameter == 1))
  {
    Motion_dimming_time = 120;
    halCommonSetToken(TOKEN_Motion_dimming_time,&Motion_dimming_time);
  }

  if(Motion_dimming_time > 59)
  {
    Motion_Rebroadcast_timeout = 58;
  }
  else
  {
    Motion_Rebroadcast_timeout = Motion_dimming_time - 2;
  }

  halCommonGetToken(&Motion_group_id ,TOKEN_Motion_group_id );
  if(Set_default_perameter == 1)
  {
    Motion_group_id = 0;
    halCommonSetToken(TOKEN_Motion_group_id,&Motion_group_id);
  }
  Group_val1.Val = Motion_group_id;
/////////////////////////////////////////
    halCommonGetToken(&Motion_grp_Select_32,TOKEN_Motion_grp_Select_32);
    halCommonGetToken(&Motion_group_id_32,TOKEN_Motion_group_id_32);
    if(Set_default_perameter == 1)
    {
            Motion_grp_Select_32 =1; //1 indicates it supports only 32 group..
            halCommonSetToken(TOKEN_Motion_grp_Select_32,&Motion_grp_Select_32);
            Motion_group_id_32 =0;
            halCommonSetToken(TOKEN_Motion_group_id_32,&Motion_group_id_32);
    }

    Group_val1_32.Val = Motion_group_id_32;
  /////////////////////////////////////////

  halCommonGetToken(&dim_applay_time ,TOKEN_dim_applay_time );
  if((dim_applay_time > 1800) || (Set_default_perameter == 1))
  {
    dim_applay_time = 0;
    halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);
  }

  halCommonGetToken(&dim_inc_val,TOKEN_dim_inc_val);
  if((dim_inc_val > 60) || (Set_default_perameter == 1))
  {
    dim_inc_val = 0;
    halCommonSetToken(TOKEN_dim_inc_val ,&dim_inc_val);
  }


  halCommonGetToken(&Motion_normal_dimming_percentage,TOKEN_Motion_normal_dimming_percentage );

  if((Motion_normal_dimming_percentage > 100) || (Set_default_perameter == 1))
  {
    Motion_normal_dimming_percentage = 100;
    halCommonSetToken(TOKEN_Motion_normal_dimming_percentage ,&Motion_normal_dimming_percentage );
  }
	
  halCommonGetToken(&Motion_Detect_Timeout,TOKEN_Motion_Detect_Timeout);
	 if((Motion_Detect_Timeout > 59) || (Set_default_perameter == 1))
	 {
    Motion_Detect_Timeout = 5;
    halCommonSetToken(TOKEN_Motion_Detect_Timeout,&Motion_Detect_Timeout);
	 }
  delay(10);

  halCommonGetToken(&Motion_Broadcast_Timeout,TOKEN_Motion_Broadcast_Timeout);
	 if((Motion_Broadcast_Timeout > 59) || (Set_default_perameter == 1))
	 {
    Motion_Broadcast_Timeout = 5;
    halCommonSetToken(TOKEN_Motion_Broadcast_Timeout,&Motion_Broadcast_Timeout);
	 }

  halCommonGetToken(&Motion_Sensor_Type,TOKEN_Motion_Sensor_Type);
	 if((Motion_Sensor_Type > 1) || (Set_default_perameter == 1))
	 {	
    Motion_Sensor_Type = 0;
    halCommonSetToken(TOKEN_Motion_Sensor_Type,&Motion_Sensor_Type);
	 }

  halCommonGetToken(&Network_leave_Flag,TOKEN_idimmer_en);
  if((Set_default_perameter == 1) || (Network_leave_Flag > 1))
  {
    Network_leave_Flag =1;

    halCommonSetToken(TOKEN_idimmer_en,&Network_leave_Flag);
  }	
  idimmer_en = 0;

  halCommonGetToken(&Network_Leave_Hr_Value,TOKEN_idimmer_id);
  if((Set_default_perameter == 1)||(Network_Leave_Hr_Value == 0))
  {
    Network_Leave_Hr_Value =360;
    halCommonSetToken(TOKEN_idimmer_id,&Network_Leave_Hr_Value);
  }
 ////////////////
  
  halCommonGetToken(&Network_Leave_Hr_Cnt,TOKEN_Network_Leave_Hr_Cnt);
  if(Set_default_perameter == 1)
  {
    Network_Leave_Hr_Cnt =0;
    halCommonSetToken(TOKEN_Network_Leave_Hr_Cnt,&Network_Leave_Hr_Cnt);
  }  
  //////////////
  halCommonGetToken(&idimmer_longaddress[0],TOKEN_idimmer_longaddress);
/////////////////////
  if(Set_default_perameter == 1)
  {  
                idimmer_longaddress[0] =0; //Auto_Send_Enable_Disable;
                idimmer_longaddress[1] =0xff;// Auto_Send_Lograte_Interval;
                idimmer_longaddress[2] =0xff;// Auto_Send_Retry;
                idimmer_longaddress[3] =0xff;// Read_Data_send_Timer;
                
                idimmer_longaddress[4] = Time_Slice/256;;//pulseCounter.byte[0]; //Time_Slice 0
                idimmer_longaddress[5] = Time_Slice%256;;//pulseCounter.byte[1]; //Time_Slice 1
                idimmer_longaddress[6] = 0;//Configured_Event.Val;
                idimmer_longaddress[7] =0;
                idimmer_longaddress[8] =0;
                idimmer_longaddress[9] =0;
                halCommonSetToken(TOKEN_idimmer_longaddress,&idimmer_longaddress[0]);
  }
      Auto_Send_Enable_Disable =idimmer_longaddress[0];
      Auto_Send_Lograte_Interval=idimmer_longaddress[1];
      Auto_Send_Retry=idimmer_longaddress[2];
      Read_Data_send_Timer=idimmer_longaddress[3];
      pulseCounter.byte[1] =idimmer_longaddress[4]; //Time_Slice Higher Byte
      pulseCounter.byte[0] =idimmer_longaddress[5]; //Time_Slice Lower Byte
      Time_Slice =pulseCounter.int_data;
      Configured_Event.Val =idimmer_longaddress[6];

/////////////////////  


  halCommonGetToken(&SLC_DST_En,TOKEN_SLC_DST_En);
  halCommonGetToken(&SLC_DST_Start_Rule,TOKEN_SLC_DST_Start_Rule);
  halCommonGetToken(&SLC_DST_Start_Month,TOKEN_SLC_DST_Start_Month);
  halCommonGetToken(&SLC_DST_Start_Time,TOKEN_SLC_DST_Start_Time);
  halCommonGetToken(&SLC_DST_Stop_Rule,TOKEN_SLC_DST_Stop_Rule);
  halCommonGetToken(&SLC_DST_Stop_Month,TOKEN_SLC_DST_Stop_Month);
  halCommonGetToken(&SLC_DST_Stop_Time,TOKEN_SLC_DST_Stop_Time);
  halCommonGetToken(&SLC_DST_Time_Zone_Diff,TOKEN_SLC_DST_Time_Zone_Diff);
  halCommonGetToken(&Time_change_dueto_DST ,TOKEN_Time_change_dueto_DST);
  halCommonGetToken(&SLC_DST_Rule_Enable,TOKEN_SLC_DST_Rule_Enable);
  halCommonGetToken(&SLC_DST_Start_Date,TOKEN_SLC_DST_Start_Date);
  halCommonGetToken(&SLC_DST_Stop_Date,TOKEN_SLC_DST_Stop_Date);

  old_year = Date.Year;

  if((Set_default_perameter == 1) || (SLC_DST_En > 1))
  {
    SLC_DST_En = 0;

    halCommonSetToken(TOKEN_SLC_DST_En,&SLC_DST_En);
    SLC_DST_Start_Rule = 0;

    halCommonSetToken(TOKEN_SLC_DST_Start_Rule,&SLC_DST_Start_Rule);
    SLC_DST_Start_Month = 0;

    halCommonSetToken(TOKEN_SLC_DST_Start_Month,&SLC_DST_Start_Month);
    SLC_DST_Start_Time = 0;

    halCommonSetToken(TOKEN_SLC_DST_Start_Time,&SLC_DST_Start_Time);
    SLC_DST_Stop_Rule =	0;

    halCommonSetToken(TOKEN_SLC_DST_Stop_Rule,&SLC_DST_Stop_Rule);
    SLC_DST_Stop_Month = 0;

    halCommonSetToken(TOKEN_SLC_DST_Stop_Month,&SLC_DST_Stop_Month);
    SLC_DST_Stop_Time =	0;

    halCommonSetToken(TOKEN_SLC_DST_Stop_Time,&SLC_DST_Stop_Time);
    SLC_DST_Time_Zone_Diff = pulseCounter.float_data = 0;

    halCommonSetToken(TOKEN_SLC_DST_Time_Zone_Diff,&SLC_DST_Time_Zone_Diff);
    Time_change_dueto_DST = 0;

    halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
    SLC_DST_Rule_Enable = 0;

    halCommonSetToken(TOKEN_SLC_DST_Rule_Enable,&SLC_DST_Rule_Enable);
    SLC_DST_Start_Date = 0;

    halCommonSetToken(TOKEN_SLC_DST_Start_Date,&SLC_DST_Start_Date);
    SLC_DST_Stop_Date =	0;

    halCommonSetToken(TOKEN_SLC_DST_Stop_Date,&SLC_DST_Stop_Date);
  }

  if(SLC_DST_En == 1)
  {
    if(SLC_DST_Rule_Enable == 1)
    {
      SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule,SLC_DST_Start_Month);
      SLC_DST_R_Stop_Date =  find_dst_date_from_rule(SLC_DST_Stop_Rule,SLC_DST_Stop_Month);
    }
    else
    {
      SLC_DST_R_Start_Date = SLC_DST_Start_Date;
      SLC_DST_R_Stop_Date =  SLC_DST_Stop_Date;
    }
  }

  halCommonGetToken(&Auto_event,TOKEN_Auto_event);
  if((Set_default_perameter == 1) || (Auto_event > 1))
  {
    Auto_event = 0;
    halCommonSetToken(TOKEN_Auto_event,&Auto_event);
  }
  delay(10);

  halCommonGetToken(&SLC_New_Manula_Mode_Val,TOKEN_SLC_New_Manula_Mode_Val);
  halCommonGetToken(&SLC_New_Manual_Mode_Timer,TOKEN_SLC_New_Manual_Mode_Timer);
  if(Set_default_perameter == 1)
  {
    SLC_New_Manula_Mode_Val = 101;
    halCommonSetToken(TOKEN_SLC_New_Manula_Mode_Val,&SLC_New_Manula_Mode_Val);
    SLC_New_Manual_Mode_Timer = 0;
    halCommonSetToken(TOKEN_SLC_New_Manual_Mode_Timer,&SLC_New_Manual_Mode_Timer);
  }
  if(SLC_New_Manula_Mode_Val < 101)
  {
    SLC_New_Manula_Mode_En = 1;	
  }
  else
  {
    SLC_New_Manula_Mode_En = 0;	
    SLC_New_Manual_Mode_Timer = 0;
    halCommonSetToken(TOKEN_SLC_New_Manual_Mode_Timer,&SLC_New_Manual_Mode_Timer);
  }

  if(Set_default_perameter == 1)
  {

    Link_Key[0] = 0x00;
    Link_Key[1] = 0x11;
    Link_Key[2] = 0x22;
    Link_Key[3] = 0x33;
    Link_Key[4] = 0x44;
    Link_Key[5] = 0x55;
    Link_Key[6] = 0x66;
    Link_Key[7] = 0x77;
    Link_Key[8] = 0x88;
    Link_Key[9] = 0x99;
    Link_Key[10] = 0xAA;
    Link_Key[11] = 0xBB;
    Link_Key[12] = 0xCC;
    Link_Key[13] = 0xDD;
    Link_Key[14] = 0xEE;
    Link_Key[15] = 0xFF;

    halCommonSetToken(TOKEN_TLink_Key,&Link_Key);
  }
  halCommonGetToken(&Link_Key,TOKEN_TLink_Key);

  if((Link_Key[0]==0x00) &&(Link_Key[1]==0x00)&&(Link_Key[2]==0x00)&&(Link_Key[3]==0x00)
     &&(Link_Key[4]==0x00)&&(Link_Key[5]==0x00)&&(Link_Key[6]==0x00)&&(Link_Key[7]==0x00)
       &&(Link_Key[8]==0x00)&&(Link_Key[9]==0x00)&&(Link_Key[10]==0x00)&&(Link_Key[10]==0x00)
         &&(Link_Key[12]==0x00)&&(Link_Key[13]==0x00)&&(Link_Key[14]==0x00)&&(Link_Key[15]==0x00))
  {
    Link_Key[0] = 0x00;
    Link_Key[1] = 0x11;
    Link_Key[2] = 0x22;
    Link_Key[3] = 0x33;
    Link_Key[4] = 0x44;
    Link_Key[5] = 0x55;
    Link_Key[6] = 0x66;
    Link_Key[7] = 0x77;
    Link_Key[8] = 0x88;
    Link_Key[9] = 0x99;
    Link_Key[10] = 0xAA;
    Link_Key[11] = 0xBB;
    Link_Key[12] = 0xCC;
    Link_Key[13] = 0xDD;
    Link_Key[14] = 0xEE;
    Link_Key[15] = 0xFF;
    halCommonSetToken(TOKEN_TLink_Key,&Link_Key);
  }

  halCommonGetToken(&Lamp_type,TOKEN_Lamp_type);
  if(Set_default_perameter == 1)
  {
    Lamp_type =0;
    halCommonSetToken(TOKEN_Lamp_type,&Lamp_type);
  }



  halCommonGetToken(&Photocell_steady_timeout_Val,TOKEN_Photocell_steady_timeout_Val);
  halCommonGetToken(&photo_cell_toggel_counter_Val,TOKEN_photo_cell_toggel_counter_Val);
  halCommonGetToken(&photo_cell_toggel_timer_Val,TOKEN_photo_cell_toggel_timer_Val);  	
  halCommonGetToken(&Photo_cell_Frequency,TOKEN_Photo_cell_Frequency);  	
  halCommonGetToken(&Photocell_unsteady_timeout_Val,TOKEN_Photocell_unsteady_timeout_Val);  	
  halCommonGetToken(&Schedule_offset,TOKEN_Schedule_offset);  	

  if((Set_default_perameter == 1) || (Photocell_steady_timeout_Val == 0))
  {
    Photocell_steady_timeout_Val = 18;
    halCommonSetToken(TOKEN_Photocell_steady_timeout_Val,&Photocell_steady_timeout_Val);
    photo_cell_toggel_counter_Val = 10; 	
    halCommonSetToken(TOKEN_photo_cell_toggel_counter_Val,&photo_cell_toggel_counter_Val);
    photo_cell_toggel_timer_Val  = 18;
    halCommonSetToken(TOKEN_photo_cell_toggel_timer_Val,&photo_cell_toggel_timer_Val);
    Photo_cell_Frequency  = 30;
    halCommonSetToken(TOKEN_Photo_cell_Frequency,&Photo_cell_Frequency);
    Photocell_unsteady_timeout_Val = 5;//180;
    halCommonSetToken(TOKEN_Photocell_unsteady_timeout_Val,&Photocell_unsteady_timeout_Val);
    Schedule_offset = 0;
    halCommonSetToken(TOKEN_Schedule_offset,&Schedule_offset);
  }


  Id_frame_received = 0;


  Photocell_Detect_Time = Photocell_unsteady_timeout_Val;

  if(Set_default_perameter == 1)
  {
    Slc_reset_counter = 0;
    halCommonSetToken(TOKEN_Slc_reset_counter,&Slc_reset_counter);
  }
  halCommonGetToken(&Slc_reset_counter,TOKEN_Slc_reset_counter);
  Slc_reset_counter++;
  halCommonSetToken(TOKEN_Slc_reset_counter,&Slc_reset_counter);

  halCommonGetToken(&SLC_Start_netwrok_Search_Timeout,TOKEN_SLC_Start_netwrok_Search_Timeout);
  halCommonGetToken(&Network_Scan_Cnt_Time_out,TOKEN_Network_Scan_Cnt_Time_out);

  if((Set_default_perameter == 1) || (Network_Scan_Cnt_Time_out == 0))
  {
        SLC_Start_netwrok_Search_Timeout = 1;
        Network_Scan_Cnt_Time_out = 86400;
        halCommonSetToken(TOKEN_SLC_Start_netwrok_Search_Timeout,&SLC_Start_netwrok_Search_Timeout);
        halCommonSetToken(TOKEN_Network_Scan_Cnt_Time_out,&Network_Scan_Cnt_Time_out);
  }

  if(Set_default_perameter == 1)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_1,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_2,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_3,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_4,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_5,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_6,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_7,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_8,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_9,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_10,&Slc_Comp_Mix_Mode_schedule[0]);
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_11,&Slc_Comp_Mix_Mode_schedule[0]);
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_12,&Slc_Comp_Mix_Mode_schedule[0]);
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_13,&Slc_Comp_Mix_Mode_schedule[0]);
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_14,&Slc_Comp_Mix_Mode_schedule[0]);
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_15,&Slc_Comp_Mix_Mode_schedule[0]);
  }

  halCommonGetToken(&Commissioning_flag,TOKEN_Commissioning_flag);
  if(Commissioning_flag == 3)
  {
    Commissioning_flag = 0;
    halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);
    networkLeaveCommand();  //leave current network.
    
    /*
    This logic is added after discussed with Sudhanshu Date 14Dec 2019
    On next power cycle we must have to execute Auto detection logic. 
    */
    DimmerDriverSelectionProcess = 0x01;
    dali_support = 2;
    DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
    halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);
  }
  if((Commissioning_flag > 2) || (Set_default_perameter == 1))
  {
        Commissioning_flag = 0;
        halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);
  }

  halCommonGetToken(&Valid_DCU_Flag,TOKEN_Valid_DCU_Flag);
  if((Valid_DCU_Flag > 1) || (Set_default_perameter == 1))
  {
        Valid_DCU_Flag = 0;
        halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
  }

#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
  halCommonGetToken(&GPS_Read_Enable,TOKEN_GPS_Read_Enable);
  if((GPS_Read_Enable > 1) || (Set_default_perameter == 1))
  {
        GPS_Read_Enable = 1;
        halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
  }

  halCommonGetToken(&Send_GPS_Data_at_valid_Dcu,TOKEN_Send_GPS_Data_at_valid_Dcu);
  if((Send_GPS_Data_at_valid_Dcu > 2) || (Set_default_perameter == 1))
  {
    Send_GPS_Data_at_valid_Dcu = 0;
    halCommonSetToken(TOKEN_Send_GPS_Data_at_valid_Dcu,&Send_GPS_Data_at_valid_Dcu);
  }

  halCommonGetToken(&GPS_Wake_timer,TOKEN_GPS_Wake_timer);
  if((GPS_Wake_timer > 3600) || (Set_default_perameter == 1))
  {
    GPS_Wake_timer = 120;
    halCommonSetToken(TOKEN_GPS_Wake_timer,&GPS_Wake_timer);
  }

  halCommonGetToken(&No_of_setalite_threshold,TOKEN_No_of_setalite_threshold);
  if((Set_default_perameter == 1))
  {
    No_of_setalite_threshold = 8;
    halCommonSetToken(TOKEN_No_of_setalite_threshold,&No_of_setalite_threshold);
  }

  halCommonGetToken(&Setalite_angle_threshold,TOKEN_Setalite_angle_threshold);
  if((Set_default_perameter == 1))
  {
    Setalite_angle_threshold = 15;
    halCommonSetToken(TOKEN_Setalite_angle_threshold,&Setalite_angle_threshold);
  }

  halCommonGetToken(&setalite_Rssi_threshold,TOKEN_setalite_Rssi_threshold);
  if((Set_default_perameter == 1))
  {
    setalite_Rssi_threshold = 35;
    halCommonSetToken(TOKEN_setalite_Rssi_threshold,&setalite_Rssi_threshold);
  }

  halCommonGetToken(&Maximum_no_of_Setalite_reading,TOKEN_Maximum_no_of_Setalite_reading);
  if((Set_default_perameter == 1))
  {
    Maximum_no_of_Setalite_reading = 4;
    halCommonSetToken(TOKEN_Maximum_no_of_Setalite_reading,&Maximum_no_of_Setalite_reading);
  }

  halCommonGetToken(&HDOP_Gps_threshold,TOKEN_HDOP_Gps_threshold);
  if(Set_default_perameter == 1)
  {
    HDOP_Gps_threshold = 1.0;
    halCommonSetToken(TOKEN_HDOP_Gps_threshold,&HDOP_Gps_threshold);
  }

  halCommonGetToken(&Check_Sbas_Enable,TOKEN_Check_Sbas_Enable);
  if(Set_default_perameter == 1)
  {
    Check_Sbas_Enable = 1;
    halCommonSetToken(TOKEN_Check_Sbas_Enable,&Check_Sbas_Enable);
  }

  halCommonGetToken(&Sbas_setalite_Rssi_threshold,TOKEN_Sbas_setalite_Rssi_threshold);
  if(Set_default_perameter == 1)
  {
    Sbas_setalite_Rssi_threshold = 35;
    halCommonSetToken(TOKEN_Sbas_setalite_Rssi_threshold,&Sbas_setalite_Rssi_threshold);
  }

  halCommonGetToken(&Auto_Gps_Data_Send,TOKEN_Auto_Gps_Data_Send);
  if((Auto_Gps_Data_Send > 1) || (Set_default_perameter == 1))
  {
    Auto_Gps_Data_Send = 1;
    halCommonSetToken(TOKEN_Auto_Gps_Data_Send,&Auto_Gps_Data_Send);
  }


#endif

  halCommonGetToken(&previous_mode,TOKEN_previous_mode);
  if((previous_mode > 8) || (Set_default_perameter == 1))
  {
    /*
    //automode; comnet on 30MArch2018 since default is photocell mode is required for Previous mode.
        Since in this verswion we set automode is manual mode and it gets impact when RTC fualt occured it swutched in previous mode which was manual mode.
    */
    previous_mode = 1;//automode;

  }

  halCommonGetToken(&RTC_New_fault_logic_Enable,TOKEN_RTC_New_fault_logic_Enable);
  if((RTC_New_fault_logic_Enable < 1) || (RTC_New_fault_logic_Enable > 2) || (Set_default_perameter == 1))
  {
        RTC_New_fault_logic_Enable = 1;
        halCommonSetToken(TOKEN_RTC_New_fault_logic_Enable,&RTC_New_fault_logic_Enable);
  }

  if(RTC_New_fault_logic_Enable == 1)
  {
    halCommonGetToken(&Photo_Cell_Ok,TOKEN_Photo_Cell_Ok);   // store mode in to NVM.
    if(Set_default_perameter == 1)
    {
#if (defined(ISLC_T8)||defined(ISLC3300_T8_V2))
      Photo_Cell_Ok = 0;
#else
      Photo_Cell_Ok = 1;
#endif

      halCommonSetToken(TOKEN_Photo_Cell_Ok,&Photo_Cell_Ok);   // store mode in to NVM.
    }

    halCommonGetToken(&Photo_Cell_error,TOKEN_Photo_Cell_error);   // store photocell error in to NVM.
    if(Set_default_perameter == 1)
    {
#if (defined(ISLC_T8)||defined(ISLC3300_T8_V2))
      Photo_Cell_error = 1;
#else
      Photo_Cell_error = 0;
#endif

      halCommonSetToken(TOKEN_Photo_Cell_error,&Photo_Cell_error);   // store photocell error in to NVM.
    }

    if(Photo_Cell_Ok == 0)   // if photocell faulty then set error bit according to photocell fault.
    {
      if(Photo_Cell_error == 1)
      {
        error_condition.bits.b1 = 1;
      }
      else if(Photo_Cell_error == 2)
      {
        error_condition.bits.b4 = 1;    											// photo_cell oscillating error.
      }
    }
  }

//    if(Set_default_perameter == 1)
//    {
//        halCommonSetToken(TOKEN_Slot_Start_Hour,&Time_Slot.Slot_Start_Hour[0]);
//        halCommonSetToken(TOKEN_Slot_Start_Min,&Time_Slot.Slot_Start_Min[0]);
//        halCommonSetToken(TOKEN_Slot_End_Hour,&Time_Slot.Slot_End_Hour[0]);
//        halCommonSetToken(TOKEN_Slot_End_Min,&Time_Slot.Slot_End_Min[0]);
//        halCommonSetToken(TOKEN_Slot_Pulses,&Time_Slot.Slot_Pulses[0]);
//
//        halCommonSetToken(TOKEN_Slot_Start_Date,&Time_Slot.Slot_Start_Date[0]);
//        halCommonSetToken(TOKEN_Slot_Start_Month,&Time_Slot.Slot_Start_Month[0]);
//        halCommonSetToken(TOKEN_Slot_Start_Year,&Time_Slot.Slot_Start_Year[0]);
//        halCommonSetToken(TOKEN_Slot_End_Date,&Time_Slot.Slot_End_Date[0]);
//        halCommonSetToken(TOKEN_Slot_End_Month,&Time_Slot.Slot_End_Month[0]);
//        halCommonSetToken(TOKEN_Slot_End_Year,&Time_Slot.Slot_End_Year[0]);
//    }
//    halCommonGetToken(&Time_Slot.Slot_Start_Hour[0],TOKEN_Slot_Start_Hour);
//    halCommonGetToken(&Time_Slot.Slot_Start_Min[0],TOKEN_Slot_Start_Min);
//    halCommonGetToken(&Time_Slot.Slot_End_Hour[0],TOKEN_Slot_End_Hour);
//    halCommonGetToken(&Time_Slot.Slot_End_Min[0],TOKEN_Slot_End_Min);
//    halCommonGetToken(&Time_Slot.Slot_Pulses[0],TOKEN_Slot_Pulses);
//
//    halCommonGetToken(&Time_Slot.Slot_Start_Date[0],TOKEN_Slot_Start_Date);
//    halCommonGetToken(&Time_Slot.Slot_Start_Month[0],TOKEN_Slot_Start_Month);
//    halCommonGetToken(&Time_Slot.Slot_Start_Year[0],TOKEN_Slot_Start_Year);
//    halCommonGetToken(&Time_Slot.Slot_End_Date[0],TOKEN_Slot_End_Date);
//    halCommonGetToken(&Time_Slot.Slot_End_Month[0],TOKEN_Slot_End_Month);
//    halCommonGetToken(&Time_Slot.Slot_End_Year[0],TOKEN_Slot_End_Year);

    //////////////

    halCommonGetToken(&Photosensor_Set_FC,TOKEN_Photosensor_Set_FC);
    halCommonGetToken(&Photosensor_Set_FC_Lamp_OFF,TOKEN_Photosensor_Set_FC_Lamp_OFF);
    if(Set_default_perameter == 1)
    {
        Photosensor_Set_FC =2.5;
        halCommonSetToken(TOKEN_Photosensor_Set_FC,&Photosensor_Set_FC);

        Photosensor_Set_FC_Lamp_OFF =3.7;
        halCommonSetToken(TOKEN_Photosensor_Set_FC_Lamp_OFF,&Photosensor_Set_FC_Lamp_OFF);
    }

  halCommonGetToken(&CURV_TYPE,TOKEN_CURV_TYPE);
  if(Set_default_perameter == 1)
  {
    CURV_TYPE = 1;
    halCommonSetToken(TOKEN_CURV_TYPE,&CURV_TYPE);
  }
  
  
  halCommonGetToken(&RS485_0_Or_DI2_1,TOKEN_RS485_0_Or_DI2_1);
  if(Set_default_perameter == 1)
  {
    RS485_0_Or_DI2_1 = 0; //default RS485
    halCommonSetToken(TOKEN_RS485_0_Or_DI2_1,&RS485_0_Or_DI2_1);
  }

  halCommonGetToken(&Last_Gsp_Enable,TOKEN_Last_Gsp_Enable);
  if(Set_default_perameter == 1)
  {
    Last_Gsp_Enable = 1; //Last_Gsp_Enable
    halCommonSetToken(TOKEN_Last_Gsp_Enable,&Last_Gsp_Enable);
  }  
  
  halCommonGetToken(&Lamp_Balast_fault_Remove_Time,TOKEN_Lamp_Balast_fault_Remove_Time);
  if((Set_default_perameter == 1)||(Lamp_Balast_fault_Remove_Time > 24)||(Lamp_Balast_fault_Remove_Time <1))
  {
          Lamp_Balast_fault_Remove_Time = 4;
          halCommonSetToken(TOKEN_Lamp_Balast_fault_Remove_Time,&Lamp_Balast_fault_Remove_Time);
  } 
  
  
    /*//////////////////////////////////////////////////////

      Higher 4 bit of DimmerDriver_SELECTION is DimmerDriverSelectionProcess and Lower 4bit is dali_support
        ==> DimmerDriverSelectionProcess
            0 = Auto Detection In Each Power Cycle
            1 = Auto Detection In NextPower Cycle
            2 = Default Dali
            3 = Default AO
        ==> dali_support
            0 = AO As Driver
            1 = Dali As Driver
            2 = Driver Needs To Detect
            3 = Error in Driver detection AO is Selected

    *///////////////////////////////////////////////////
  
  halCommonGetToken(&DimmerDriver_SELECTION,TOKEN_DimmerDriver_SELECTION);
  if(Set_default_perameter == 1)
  {
    DimmerDriverSelectionProcess = 0x03;
    dali_support = 0x00;
    DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
    halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);
  }
  DimmerDriverSelectionProcess = (DimmerDriver_SELECTION >>4); // Higher 4bits as DimmerDriverSelectionProcess
  dali_support = DimmerDriver_SELECTION & 0x0F; // Lower 4bits as dali_support
  if(DimmerDriverSelectionProcess == 0)
  {
#if defined(ISLC_3100_V7_7)    
    halSetLed(MUX_A0);	        //0	
    halSetLed(MUX_A1);          //0  
#elif defined(ISLC_3100_V7_9)
    halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif 
    dali_support = 2;
  }
  else if (DimmerDriverSelectionProcess == 1)
  {
    if((dali_support == 0) || (dali_support == 3))  // AO as Dimmer
    {
#if defined(ISLC_3100_V7_7)      
      halSetLed(MUX_A1);	 //0	
      halClearLed(MUX_A0);       //1
#elif defined(ISLC_3100_V7_9)
      halClearLed(MUX_A0);   // Set=0=dali & clear=1=AO      
#endif      
      halStackIndicatePresence(); 
    }
    else if(dali_support == 1)   // Dali as Dimmer
    {
#if defined(ISLC_3100_V7_7)      
      halSetLed(MUX_A0);	 //0	
      halClearLed(MUX_A1);       //1
#elif defined(ISLC_3100_V7_9)
      halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO 
#endif      
      halStackIndicatePresence();
    }
    else
    {
#if defined(ISLC_3100_V7_7)      
      halSetLed(MUX_A0);	 //0	
      halSetLed(MUX_A1);         //0   
#elif defined(ISLC_3100_V7_9)
      halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif      
      dali_support = 2;   // Need To detect Driver
    }
  }
  else if(DimmerDriverSelectionProcess == 2)
  {
    dali_support = 1;
#if defined(ISLC_3100_V7_7)
    halSetLed(MUX_A0);	        //0	
    halClearLed(MUX_A1);        //1
#elif defined(ISLC_3100_V7_9)
    halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif    
    halStackIndicatePresence();   
  }
  else if(DimmerDriverSelectionProcess >= 3)
  {
    dali_support = 0;
#if defined(ISLC_3100_V7_7)    
    halSetLed(MUX_A1);	        //0	
    halClearLed(MUX_A0);        //1
#elif defined(ISLC_3100_V7_9)
    halClearLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif
    halStackIndicatePresence();     
  }
  else
  {}
}

/*************************************************************************
Function Name: Check_Slc_Peripheral
input: None.
Output: None.
Discription: this function is used check all peripherals working properly or not
             and result of checking to application.
*************************************************************************/
void Check_Slc_Peripheral(void)
{
  //unsigned char i;
  if(check_peripheral == 1)    // if check peripherals command received from application then start peripheral routins.
  {
    if(single_time_execute)    // clear all count before test start.
    {
      single_time_execute=0;
      emt_int_count = 0;
      Kwh_count = 0;
      Set_Do(1);                // do lamp ON for Energy meter testing.
      Diagnosis_DI = 1;//test_EEPROM();
      cheak_emt_timer = 0;
    }
    if(cheak_emt_timer < 2)
    {
      Set_Do(1);
      Read_EnergyMeter();
      calculate_3p4w();
      if(one_sec == 1)
      {
        //  sprintf(Display,"Sec_Counter==%d cheak_emt_timer==%d",Sec_Counter,cheak_emt_timer);
        //  emberAfGuaranteedPrint("\r\n%p", Display);
        one_sec = 0;
        read_ds1302_bytes();
      }
    }
    else
    {
      single_time_execute=1;          // send result of test after 3 minute.
      //emberAfGuaranteedPrint("\r\n%p", "cheak_emt_timer < 2_out");
      Send_Slc_Diagnosis = 1;	
      temp_timer_delay = 0;
      check_peripheral = 0;
    }
  }
}

/*************************************************************************
Function Name: Check_Slc_Test_1
input: None.
Output: None.
Discription: this function is used to test all peripherals and its accuracy.
*************************************************************************/
void Check_Slc_Test_1(void)
{
  Lamp_off_on_Time = 20;            // make lamp off/on delay fast so testing done fast.
  Photocell_Detect_Time = 5;//16;       // make photocell detect time fast so testing done fast.
  //emberAfGuaranteedPrint("\r\n%p", "Test query 1 start");
  Check_RTC(10);                    // check RTC for 10 sec.
  Check_EMT(30);                    // check EMT for 30 sec.
  Check_Lamp(2);                    // check Lamp for 2 on/off cycle.
  if(photocell_test_dicision == 1)												// if photocell present in testing
  {
    Check_photocell(2);             // check photocell for 2 on/off cycle
  }
  Check_CT(2);                      // check CT for 2 on/off cycle.
  Set_Do(0);
  Check_eeprom();	                  // check EEPROM
  if(Dimming_test_dicision == 1)
  {
    ////////Switch from RS485 to DI2//////
//    halCommonGetToken(&RS485_0_Or_DI2_1,TOKEN_RS485_0_Or_DI2_1);
//    if(RS485_0_Or_DI2_1 == 1)
//    {
//      
//    }
//    else
//    {
      RS485_0_Or_DI2_1 =1;
      halCommonSetToken(TOKEN_RS485_0_Or_DI2_1,&RS485_0_Or_DI2_1);
      Select_RS485_Or_DI2(RS485_0_Or_DI2_1);  
//    }
    //////////////
    check_dimming_hardware();       // check dimming hardware if set for test
    
  }
  ///////////
  if(dali_support == 1)
  {
    Dali_Driver_Check();
  }
  ///////////
  send_SLC_test_query_1 = 1;	       // send result of testing to application.
  test_query_1_send_timer = 0;
  //emberAfGuaranteedPrint("\r\n%p", "Test query 1 end");
  Photocell_Detect_Time = Photocell_unsteady_timeout_Val; // roll photocell detect time to proper operation.	
  //     Photocell_Detect_Time =1;
}	

/*************************************************************************
Function Name: Check_RTC
input: time for test.
Output: None.
Discription: this function is used to check RTC working properly or not
*************************************************************************/
void Check_RTC(unsigned char time)
{
  unsigned char dd=0,mo=0,yy=0,hh=0,mm=0,ss=0;
  unsigned char temp_arr[10];
  //emberAfGuaranteedPrint("\r\n%p", "RTC test start");


  read_ds1302_bytes();         // read RTC time
  read_ds1302_bytes();          // read RTC time
  read_ds1302_bytes();          // read RTC time
		
  dd = Date.Date;               // store RTC time in to local variables.
  mo = Date.Month;
  yy = Date.Year;
  hh = Time.Hour;
  mm = Time.Min;
  ss = Time.Sec;

  cheak_emt_timer_sec = 0;
  while(cheak_emt_timer_sec < time)           // read RTC for 10 sec.
  {
    if(Read_RTC == 1)
    {
      Read_RTC = 0;
      //	CLRWDT() need to add watchdog from ember
      read_ds1302_bytes();
    }	
    Ember_Stack_run_Fun();     // call all ember tick function to increase stack timeing
  }

  if((dd == Date.Date) && (mo == Date.Month) &&
     (yy == Date.Year) && (hh == Time.Hour) && (mm == Time.Min)
       && (ss == Time.Sec)) // compare RTC time with previouse RTC time
  {
    test_result.bits.b0 = 0;				// if Time not increase then RTC faulty
  }
  else
  {
    test_result.bits.b0 = 1;				// RTC ok.
  }

  temp_arr[0]='S';
  temp_arr[1]='L';
  temp_arr[2]=amr_id/256;
  temp_arr[3]=amr_id%256;
  temp_arr[4] = RTC_TEST_DONE;

  Send_data_RF(temp_arr,5,1);        // send message of RTC Test done.
  Ember_Stack_run_Fun();                     // call all ember tick function to increase stack timeing
  //emberAfGuaranteedPrint("\r\n%p", "RTC End");


  //        emberTick();          // Allow the stack to run.
  //        emberAfTick();
  //        appTick();
  //        emberAfRunEvents();
}	

/*************************************************************************
Function Name: Check_EMT
input: time for test.
Output: None.
Discription: this function is used to check Energy meter working properly or not
*************************************************************************/
void Check_EMT(unsigned char time)
{
  unsigned char temp_arr[10];
  //emberAfGuaranteedPrint("\r\n%p", "EMT test Start");
  emt_int_count = 0;
  Kwh_count = 0;
  Set_Do(1);
  cheak_emt_timer_sec = 0;
  while(cheak_emt_timer_sec < time)   // Read EM for 30 sec.
  {
    Set_Do(1);
    Read_EnergyMeter();
    calculate_3p4w();
    if(one_sec == 1)
    {
      one_sec = 0;
      //	CLRWDT() need to add watchdog from ember
      read_ds1302_bytes();
    }
    ////////////////
    Ember_Stack_run_Fun();                          // call all ember tick function to increase stack timeing
    //                        emberTick();          // Allow the stack to run.
    //                        emberAfTick();
    //                        appTick();
    //                        emberAfRunEvents();

    ////////////////////
  }

  if(emt_int_count == 0)
  {
    test_result.bits.b1 = 0;				// if EM read En interrupt not increase then EMT IC faulty	
  }
  else if((emt_int_count > 0) && (irdoub > CREEP_LIMIT)
          && (Kwh_count == 0))
  {
    test_result.bits.b1 = 0;				// if KWH interrupt is not increase then EMT IC faulty
  }
  else
  {
    test_result.bits.b1 = 1;				// EMT IC OK	
  }

  test_voltage  = vrdoub;
  test_current  = irdoub;	
  test_emt_int_count = emt_int_count;
  test_Kwh_count = Kwh_count;

  temp_arr[0]='S';
  temp_arr[1]='L';
  temp_arr[2]=amr_id/256;
  temp_arr[3]=amr_id%256;
  temp_arr[4] = EMT_TEST_DONE;

  Send_data_RF(temp_arr,5,1);            // send EM test done message to application
  Ember_Stack_run_Fun();                 // call all ember tick function to increase stack timeing
  //emberAfGuaranteedPrint("\r\n%p", "EMT test End");
}

/*************************************************************************
Function Name: Check_Lamp
input: time for test.
Output: None.
Discription: this function is used to check Lamp working properly or not
*************************************************************************/
void Check_Lamp(unsigned char time)
{
  unsigned char i=0;
  unsigned char temp_arr[10];
  test_result.bits.b2 = 1;

  //emberAfGuaranteedPrint("\r\n%p", "LAMP test Start");

  for(i=0;i<time;i++)           // do lamp on/off cycle 2 times.
  {		

    cheak_emt_timer_sec = 0;
    Photocell_int_timer = 0;
    while(cheak_emt_timer_sec < 30)   // make lamp on for 30 sec.
    {
      Set_Do(1);
      Read_EnergyMeter();
      calculate_3p4w();
      //	CLRWDT() need to add watchdog from ember
      if(one_sec == 1)
      {
        one_sec = 0;
        //	CLRWDT() need to add watchdog from ember
			    	read_ds1302_bytes();
      }	
      Ember_Stack_run_Fun();                          // call all ember tick function to increase stack timeing
      //                        emberTick();          // Allow the stack to run.
      //                        emberAfTick();
      //                        appTick();
      //                        emberAfRunEvents();
    }

    if(photocell_test_dicision == 1)												// if photocell present in testing
    {
      if((irdoub > CREEP_LIMIT) || ( Photo_feedback == 1))  // check photocell feed back and current if both OK then Lamp OK
      {
        if(test_result.bits.b2 != 0)
        {
          test_result.bits.b2 = 1;			 // Lamp OK.
        }

      }
      else
      {					
        test_result.bits.b2 = 0;			 // Lamp faulty.	
      }
    }
    else
    {
      if(irdoub < CREEP_LIMIT)      // Photocell not available for testing so check only current. if current reading is OK then Lamp OK.
      {
        test_result.bits.b2 = 0;			 // Lamp faulty.	
      }
      else
      {
        if(test_result.bits.b2 != 0)
        {
          test_result.bits.b2 = 1;			 // Lamp OK.	
        }
      }
    }


    cheak_emt_timer_sec = 0;
    Photocell_int_timer = 0;
    while(cheak_emt_timer_sec < 30)  // OFF Lamp for 30 sec.
    {
      Set_Do(0);
      Read_EnergyMeter();
      calculate_3p4w();
      //	CLRWDT() need to add watchdog from ember
      if(one_sec == 1)
      {
        one_sec = 0;
        //	CLRWDT() need to add watchdog from ember
			    	read_ds1302_bytes();
      }	
      Ember_Stack_run_Fun();                          // call all ember tick function to increase stack timeing
      //                        emberTick();          // Allow the stack to run.
      //                        emberAfTick();
      //                        appTick();
      //                        emberAfRunEvents();
    }	


    if(photocell_test_dicision == 1)												// if photocell present in testing
    {		
      if((irdoub < CREEP_LIMIT) || ( Photo_feedback == 0))  // check photocell feed back and current if both OK then Lamp OK.
      {
        if(test_result.bits.b2 != 0)
        {
          test_result.bits.b2 = 1;             // Lamp OK.
        }	
      }
      else
      {
        test_result.bits.b2 = 0;             // Lamp faulty.	
      }			
    }
    else
    {
      if(irdoub > CREEP_LIMIT)               // check only current if OK then Lamp OK.
      {
        test_result.bits.b2 = 0;             // Lamp faulty.	
      }
      else
      {
        if(test_result.bits.b2 != 0)
        {
          test_result.bits.b2 = 1;             // Lamp OK.	
        }
      }
    }

  }

  temp_arr[0]='S';
  temp_arr[1]='L';
  temp_arr[2]=amr_id/256;
  temp_arr[3]=amr_id%256;
  temp_arr[4] = LAMP_TEST_DONE;

  Send_data_RF(temp_arr,5,1);	            // send Lamp testing done message to application.
  Ember_Stack_run_Fun();                  // call all ember tick function to increase stack timeing
  //emberAfGuaranteedPrint("\r\n%p", "LAMP test End");
}

/*************************************************************************
Function Name: Check_photocell
input: time for test.
Output: None.
Discription: this function is used to check Photocell working properly or not
*************************************************************************/
void Check_photocell(unsigned char time)
{
  unsigned char i=0;
  unsigned char temp_arr[10];
  test_result.bits.b3 = 1;
  //emberAfGuaranteedPrint("\r\n%p", "Photocell test Start");
  for(i=0;i<time;i++)
  {		

    cheak_emt_timer_sec = 0;
    Photocell_int_timer = 0;
    while(cheak_emt_timer_sec < 30)
    {
#if (defined (ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))

    ADC_Process();
#endif
      Set_Do(1);                      // do lamp ON for 30 sec.
      Read_EnergyMeter();
      calculate_3p4w();
      //	CLRWDT() need to add watchdog from ember
      if(one_sec == 1)
      {
        one_sec = 0;
        //	CLRWDT() need to add watchdog from ember
			    	read_ds1302_bytes();
      }	
      Ember_Stack_run_Fun();                        // call all ember tick function to increase stack timeing
      //                        emberTick();          // Allow the stack to run.
      //                        emberAfTick();
      //                        appTick();
      //                        emberAfRunEvents();
    }

    if( Photo_feedback == 1 )   // check photocell feed bakc if OK then Photocell OK.
    {
      if(test_result.bits.b3 != 0)
      {
        test_result.bits.b3 = 1;			 // Photocell OK
      }	
    }
    else
    {
      test_result.bits.b3 = 0;			 // Photocell Faulty	
    }


    cheak_emt_timer_sec = 0;
    Photocell_int_timer = 0;
    while(cheak_emt_timer_sec < 30)
    {
#if (defined (ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
    ADC_Process();
#endif

      Set_Do(0);              // Lamp OFF for 30 sec.
      Read_EnergyMeter();
      calculate_3p4w();

      if(one_sec == 1)
      {
        one_sec = 0;	
        read_ds1302_bytes();
      }	
      Ember_Stack_run_Fun();                        // call all ember tick function to increase stack timeing
      //                        emberTick();          // Allow the stack to run.
      //                        emberAfTick();
      //                        appTick();
      //                        emberAfRunEvents();
    }	

    if( Photo_feedback == 0)           // check photocell feedback if OK then Photocell OK.
    {
      if(test_result.bits.b3 != 0)
      {
        test_result.bits.b3 = 1;             // Photocell OK.	
      }
    }
    else
    {
      test_result.bits.b3 = 0;             // Photocell Faulty.				
    }
  }

  temp_arr[0]='S';
  temp_arr[1]='L';
  temp_arr[2]=amr_id/256;
  temp_arr[3]=amr_id%256;
  temp_arr[4] = PHOTOCELL_TEST_DONE;

  Send_data_RF(temp_arr,5,1);	            // Send photocell test done message to application.
  Ember_Stack_run_Fun();                  // call all ember tick function to increase stack timeing
  //emberAfGuaranteedPrint("\r\n%p", "Photocell test End");	
}

/*************************************************************************
Function Name: Check_CT
input: time for test.
Output: None.
Discription: this function is used to check CT working properly or not
*************************************************************************/
void Check_CT(unsigned char time)
{
  unsigned char i=0;
  unsigned char temp_arr[10];
  test_result.bits.b4 = 1;
  //emberAfGuaranteedPrint("\r\n%p", "CT test Start");
  for(i=0;i<time;i++)
  {		

    cheak_emt_timer_sec = 0;
    Photocell_int_timer = 0;
    while(cheak_emt_timer_sec < 30)
    {
      Set_Do(1);                 // Lamp ON for 30 sec.
      Read_EnergyMeter();
      calculate_3p4w();
      //	CLRWDT() need to add watchdog from ember
      if(one_sec == 1)
      {
        one_sec = 0;
        //	CLRWDT() need to add watchdog from ember
			    	read_ds1302_bytes();
      }	
      Ember_Stack_run_Fun();                          // call all ember tick function to increase stack timeing
      //                        emberTick();          // Allow the stack to run.
      //                        emberAfTick();
      //                        appTick();
      //                        emberAfRunEvents();
    }

    if(photocell_test_dicision == 1)												// if photocell present in testing
    {		
      if((irdoub > CREEP_LIMIT))
      {
        if(test_result.bits.b4 != 0)
        {
          test_result.bits.b4 = 1;			 // CT OK.
        }	
      }
      else
      {

        test_result.bits.b4 = 0;			 // CT Faulty.	

      }
    }
    else
    {
      if(irdoub < CREEP_LIMIT)
      {
        test_result.bits.b4 = 0;			 // CT faulty.	
      }
      else
      {
        if(test_result.bits.b4 != 0)
        {
          test_result.bits.b4 = 1;			 // CT OK.	
        }
      }
    }


    cheak_emt_timer_sec = 0;
    Photocell_int_timer = 0;
    while(cheak_emt_timer_sec < 30)
    {
      Set_Do(0);
      Read_EnergyMeter();
      calculate_3p4w();
      //	CLRWDT() need to add watchdog from ember
      if(one_sec == 1)
      {
        one_sec = 0;
        //	CLRWDT() need to add watchdog from ember
			    	read_ds1302_bytes();
      }	
      Ember_Stack_run_Fun();                          // call all ember tick function to increase stack timeing
      //                        emberTick();          // Allow the stack to run.
      //                        emberAfTick();
      //                        appTick();
      //                        emberAfRunEvents();
    }	

    if(photocell_test_dicision == 1)												// if photocell present in testing
    {		
      if(irdoub < CREEP_LIMIT)
      {
        if(test_result.bits.b4 != 0)
        {
          test_result.bits.b4 = 1;             // CT OK.
        }	
      }
      else
      {
        test_result.bits.b4 = 0;             // CT Faulty.	
      }
    }
    else
    {
      if((irdoub > CREEP_LIMIT))
      {
        test_result.bits.b4 = 0;             // CT faulty.	
      }
      else
      {
        if(test_result.bits.b4 != 0)
        {
          test_result.bits.b4 = 1;             // CT OK.	
        }
      }
    }

  }

  temp_arr[0]='S';
  temp_arr[1]='L';
  temp_arr[2]=amr_id/256;
  temp_arr[3]=amr_id%256;
  temp_arr[4] = CT_TEST_DONE;

  Send_data_RF(temp_arr,5,1);         // send CT test done message to application
  Ember_Stack_run_Fun();              // call all ember tick function to increase stack timeing
  //emberAfGuaranteedPrint("\r\n%p", "CT test End");

}

/*************************************************************************
Function Name: Check_eeprom
input: None.
Output: None.
Discription: this function is used to check EEPROM working properly or not
             Now there is no EEPROM so make this falg to OK for application
             compatiblity
*************************************************************************/
void Check_eeprom(void)
{
//  unsigned int i;
//  unsigned char success=0,temp=0;
//  unsigned char break_flag=0;
//
//  //emberAfGuaranteedPrint("\r\n%p", "EEPROM test Start");
//  for(i=0;i<2000;i++)
//  {
//    //	WriteByte_EEPROM(i,0x55);
//    //		delay(10);
//    //	temp = ReadByte_EEPROM(i);
//
//    if(temp == 0x55)
//    {
//      test_result.bits.b5=1;				//EEPROM ok			
//    }
//    else
//    {
//      test_result.bits.b5=0;				//EEPROM Faulty	
//      break_flag = 1;
//      break;			
//    }
//    Ember_Stack_run_Fun();         // call all ember tick function to increase stack timeing
//  }
//
//  if(break_flag == 0)
//  {		
//    for(i=0;i<2000;i++)
//    {
//      //WriteByte_EEPROM(i,0xAA);
//      //	delay(10);
//      //temp = ReadByte_EEPROM(i);
//
//      if(temp == 0xAA)
//      {
//        test_result.bits.b5=1;				//EEPROM ok
//      }
//      else
//      {
//        test_result.bits.b5=0;				//EEPROM Faulty
//        break_flag = 1;
//        break;			
//      }
//    }
//    Ember_Stack_run_Fun();              // call all ember tick function to increase stack timeing
//  }	

  test_result.bits.b5=1;				//in Ember there is no EEPROM so set this bit always 1

  //emberAfGuaranteedPrint("\r\n%p", "EEPROM test End");
}	

/*************************************************************************
Function Name: check_dimming_hardware
input: None.
Output: None.
Discription: this function is used to check Dimming section working properly
             or not.
*************************************************************************/
void check_dimming_hardware(void)
{
  unsigned char temp_arr[10];

        analog_input_scaling_high_Value = 10;
        analog_input_scaling_low_Value = 0;
        DI_GET_LOW =0;
        DI_GET_HIGH =0;

        //emberAfGuaranteedPrint("\r\n%p", "Dimming test Start");

        cheak_emt_timer_sec = 0;
        while(cheak_emt_timer_sec < 3)
        {
                dimming_applied(0);          // set dimming value to 100%

                ADC_Process();               // Read ADC value.

                Ember_Stack_run_Fun();          // call all ember tick function to increase stack timeing
        }
//#if(defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)) //this is general feature of DI
#ifndef ISLC_3100_V9_2
        if(MOTION_DI1 == 0)
          {
            DI_GET_HIGH = 1;
          }
#endif
        ADC_Result_0 = Analog_Result;

        cheak_emt_timer_sec = 0;
        while(cheak_emt_timer_sec < 3)
        {
                dimming_applied(50);              // Set dimming to 50%

                    ADC_Process();                  // Read ADC Value

                Ember_Stack_run_Fun();            // call all ember tick function to increase stack timeing
        }
//        Dimming_val[1] = ADC_Result;
        ADC_Result_50 = Analog_Result;

        cheak_emt_timer_sec = 0;
        while(cheak_emt_timer_sec < 3)
        {
                dimming_applied(100);              // Set dimming to 0%

                    ADC_Process();                 // Read ADC vlaue.

                Ember_Stack_run_Fun();           // call all ember tick function to increase stack timeing
        }
//#if(defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
#ifndef ISLC_3100_V9_2
          if(MOTION_DI1 == 1) //DI High detect code has reverse logic
          {
            DI_GET_LOW = 1;
          }
#endif
//        Dimming_val[2] = ADC_Result;
        ADC_Result_100 = Analog_Result;

        temp_arr[0]='S';
        temp_arr[1]='L';
        temp_arr[2]=amr_id/256;
        temp_arr[3]=amr_id%256;
        temp_arr[4] = DIMMING_TEST_DONE;

        Send_data_RF(temp_arr,5,1);              // Send dimming test done message to applciation.
        Ember_Stack_run_Fun();                   // call all ember tick function to increase stack timeing
        //emberAfGuaranteedPrint("\r\n%p", "Dimming test End");
}

/*************************************************************************
Function Name: check_power_cycle
input: None.
Output: None.
Discription: this function is used to check RTC and EEPROM after power cycle.
*************************************************************************/
void check_power_cycle(void)
{
  unsigned char dd=0,mo=0,yy=0,hh=0,mm=0,ss=0,temp = 0,break_flag = 0;
  unsigned int i=0;

  test_result2.bits.b6=1;	
  read_ds1302_bytes();  // Read RTC after power ON.

  dd = Date.Date;
  mo = Date.Month;
  yy = Date.Year;
  hh = Time.Hour;
  mm = Time.Min;
  ss = Time.Sec;

  cheak_emt_timer_sec = 0;
  while(cheak_emt_timer_sec < 3)
  {	
    Ember_Stack_run_Fun();              // call all ember tick function to increase stack timeing
    if(Read_RTC == 1)
    {
      Read_RTC = 0;
      read_ds1302_bytes();              // Read RTC for 3 sec.
      read_ds1302_bytes();
    }
  }  	

  if((dd == Date.Date) && (mo == Date.Month) && (yy == Date.Year) && (hh == Time.Hour) && (mm == Time.Min) && (ss == Time.Sec))
  {
    test_result2.bits.b7 = 0;				           // RTC faulty
    Time.Hour = 0;																										// if currept then set Default RTC Time
    Time.Min = 0;
    Time.Sec = 0;
    Date.Date = 1;
    Date.Month = 1;
    Date.Year = 10;
    RTC_SET_TIME(Time);                      // Set RTC to default time if currupt time.
    RTC_SET_DATE(Date);
    //emberAfGuaranteedPrint("\r\n%p", "RTC Faulty");
  }
  else
  {
    if((Time.Hour > 23) || (Time.Min > 59) || (Time.Sec > 59) || (Date.Date > 31) || (Date.Date < 1) || (Date.Month > 12) || (Date.Month < 1) || (Date.Year < 1) || (Date.Year > 99))																// Check RTC time
    {
      Time.Hour = 0;																										// if currept then set Default RTC Time
      Time.Min = 0;
      Time.Sec = 0;
      Date.Date = 1;
      Date.Month = 1;
      Date.Year = 10;
      RTC_SET_TIME(Time);
      RTC_SET_DATE(Date);                       // Set RTC to default time if currupt time.
      test_result2.bits.b7 = 0;
      //emberAfGuaranteedPrint("\r\n%p", "RTC Faulty");
    }
    else
    {
      test_result2.bits.b7 = 1;
      //emberAfGuaranteedPrint("\r\n%p", "RTC OK");
    }	
  }
  Rtc_Power_on_result = test_result2.Val;

  Local_Time.Hour = Time.Hour;																										
  Local_Time.Min = Time.Min;
  Local_Time.Sec = Time.Sec;
  Local_Date.Date = Date.Date;
  Local_Date.Month = Date.Month;
  Local_Date.Year = Date.Year;
  error_condition1.bits.b2 = 0;
  if(RTC_New_fault_logic_Enable == 1)
  {
    if(test_result2.bits.b7 == 0)
    {
      RTC_faulty_detected = 0;    // 0 = faulty.
      error_condition1.bits.b2 = 1;
      //        halCommonSetToken(TOKEN_RTC_faulty_detected,&RTC_faulty_detected);   // store mode in to NVM.
      if((automode == 2) || (automode == 3) || (automode == 5))
      {
        previous_mode = automode;  // store mode value before chage.
        halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
        automode = 1;              // move SLC in to Photocell mode
        halCommonSetToken(TOKEN_automode, &automode);   // store mode in to NVM.
      }
    }
  }
  //  emberAfCorePrintln("\r\nTest query result 2 at Power on = %x", Rtc_Power_on_result);
}

/*************************************************************************
Function Name: check_dimming_protection
input: None.
Output: None.
Discription: this function is used to check Dimming hardware short circuit.
*************************************************************************/
void check_dimming_protection(void)						// SAN
{	
  //	if((protec_flag) && (Pro_count >= 1))
  //	{
  //			protec_flag =0;
  //			T4CONbits.TON = 0;
  //			Protec_sec =0;
  ////			PDO = 1;
  //			if(Protec_lock ==0)
  //			{	
  ////				PDO =1;
  //				if(Lamp_lock_condition == 1)
  //				{
  //					PDO =1;
  //					Set_Do(1);												
  //				}	
  //			}
  //
  //	}
  //	if(Pro_count >= 5)							// samir
  //	{
  //		 Protec_lock =1;
  //		 Pro_count = 0;
  //		 if(Lamp_lock_condition == 1)
  //		 {
  //			 lamp_lock = 1;
  //			 Set_Do(0);
  //		 }
  //		 error_condition1.bits.b6 = 1;					// Dimming short circuit event generation.
  //	}	
}														// SAN

/*************************************************************************
Function Name: Commissioning_routine
input: None.
Output: None.
Discription: this function is used to make lamp on and follow 4 dimming steps so
             installation team can be verify installation done propery or not.
*************************************************************************/
void Commissioning_routine(void)
{
        Lamp_off_on_Time = 0;


        Commissioning_timer = 0;               // Set lamp dimm value to 75%,50%,25% and 0% after every 5 sec to check dimming after power ON.
        while(Commissioning_timer < 5)
        {
          Set_Do(1);	
          dimming_applied(75);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 5)
        {
          Set_Do(1);	
          dimming_applied(50);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 5)
        {
          Set_Do(1);	
          dimming_applied(25);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 5)
        {
          Set_Do(1);	
          dimming_applied(0);
          delay(100);
        }

//        automode = 1;
//        Photocell_Detect_Time = 5;
}

/*************************************************************************
Function Name: DCU_detect_action
input: None.
Output: None.
Discription: this function is used to make lamp flicker when first time DCU detect
             after commissioning.
*************************************************************************/
void DCU_detect_action(void)
{
        Commissioning_timer = 0;           // if DCU detect first time then to lamp blink up to 30 sec.
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(80);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(0);
          delay(20);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(80);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(0);
          delay(20);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(80);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(0);
          delay(20);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(80);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(0);
          delay(20);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(80);
          delay(100);
        }

        Commissioning_timer = 0;
        while(Commissioning_timer < 3)
        {
          Set_Do(1);	
          dimming_applied(0);
          delay(20);
        }
}

////////////////////
//void Tilt_Sensor_Detect(void)
//{
//    if((TILT_DI1 == 1)&&(Tilt_Get_Previously == 2))
//    {
//        Tilt_Cnt =0;
//        Tilt_Occured =0;
//        error_condition1.bits.b6 = 0;
//        if((Auto_event == 0)&&(Id_frame_received == 1)&&(Tilt_Get_Previously == 2))
//        {
//          Tilt_Get_Previously =0;
////          Send_Data=1; //No need to add retry here since
//  //        send_fream();
//    //      Ember_Stack_run_Fun();
//        }
//    }
////    else if(TILT_DI1 == 0)	//Tilt detect on this condition
//    else if((TILT_DI1 == 0)&&(Tilt_Get_Previously != 2))
//    {
//
//        if((Tilt_Cnt >100)&&(Tilt_Get_Previously == 0))
//        {
//            Tilt_Cnt =0;
//            Tilt_Occured =1;
//            Tilt_Get_Previously =1;
//            error_condition1.bits.b6 = 1;
//        }
//        if((Auto_event == 0)&&(Id_frame_received == 1)&&(Tilt_Get_Previously == 1))
//        {
//            Tilt_Get_Previously =2;
//            Send_Data=1; //No need to add retry here since
//            send_fream();
//            Ember_Stack_run_Fun();
//        }
//    }
//    else
//    {
//
//    }		
//}
////////////////////

void Tilt_Sensor_Detect(void)
{
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
    if((TILT_DI1 == 1)&&(Tilt_Get_Previously == 1)) //Normal Tilt not detected
    {
        Tilt_Cnt =0;
        //Tilt_Occured =0;
        error_condition1.bits.b6 = 0;
        Tilt_Get_Previously =0;
        if(Tilt_detect_flag == 2)
        {
          Tilt_Timeout_Cnt =180;
        }
//        if((Auto_event == 0)&&(Id_frame_received == 1)&&(Tilt_Get_Previously == 2))
//        {
//          Tilt_Get_Previously =0;
//        }
    }
//    else if((TILT_DI1 == 0)&&(Tilt_Get_Previously != 2)) //Tilt detect on this condition
    else if((TILT_DI1 == 0)&&(Tilt_Get_Previously == 0)) //Tilt detect on this condition
    {

        if((Tilt_Cnt >100))
        {
            Tilt_Cnt =0;
            //Tilt_Occured =1;
            Tilt_Get_Previously =1;
            error_condition1.bits.b6 = 1;
          if(Tilt_detect_flag == 2)
          {
            Tilt_Timeout_Cnt =180;
          }

            
        }
        if((Auto_event == 0)&&(Id_frame_received == 1)&&(Tilt_Get_Previously == 1))
        {
           // Tilt_Get_Previously =2;
            //Send_Data=1; //No need to add retry here since
          if(Tilt_detect_flag == 0)
          {
            Tilt_detect_flag =1;
            Tilt_Timeout_Cnt =0;
          }
          if((Tilt_Timeout_Cnt < 180)&&(Tilt_detect_flag == 1))
          { 
            
            send_data_direct =1;
            send_fream();
            Ember_Stack_run_Fun();
          }else
          {
            Tilt_detect_flag =2;
          }
          
          
        }
    }
    else
    {
      if(Tilt_detect_flag == 2)
      {
        if(Tilt_Timeout_Cnt >300) //if tilt not detect continously for 2 min then make it normal
        {
          Tilt_detect_flag =0;
          Tilt_Timeout_Cnt =0;
        }
      }
    }	
    
#endif
}
////////////////////

void SLC_RESET_FUNCTION(void)
{
    if((energy_meter_ok == 0)&&(set_do_flag == 0))
    {
      if(error_condition.bits.b0 == 0)         // if Lamp off then do reset
      {
        SLC_RESET_ONFREEJOIN_MODE =0;
        newkwh = kwh;			
        pulseCounter.long_data = pulses_r;
        halCommonSetToken(TOKEN_pulses_r,&pulses_r);
        halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);    // save all cumilative value in to EEPROM
        halReboot();                                                // do SLC soft reset
      }
    }
}

//unsigned char gNextHour[24] = {0};

//unsigned char gNextMin[24]  = {0};
//unsigned char StartHour=0,StartMin=0,StopHour=0,StopMin=0;
//
//void check_log_rate(void)
//{
//    unsigned int i=0,j=0;
//    
//    for(i=0;i<(24/LogRate);i++)
//    {
//        if((gNextHour[i] == Time.Hour) && (gNextMin[i] == Time.Min))
//        {	
//            if((gNextHour[i] > (StartHour-2)) && (gNextHour[i] < (StartHour+2)))  
//            {
//                if(Time.Sec < 3)
//                {
//                  //WriteDebugMsg("\n log Rate near to sunset time...............");
//                }
//                log_rate_match = 0;
//            }
//            else if((gNextHour[i] > (StopHour-2)) && (gNextHour[i] < (StopHour+2)))
//            {
//                if(Time.Sec < 3)
//                {
//                WriteDebugMsg("\n log Rate near to sunrise time...............");
//                }
//                log_rate_match = 0;
//            }
//            else
//            {
//                if(Time.Sec <= 1)
//                {
//                    WriteDebugMsg("\n log Rate match...............");
//                    log_rate_match = 1;
//                    if((i == 0))
//                    {
//                      for(j=0;j<max_slc_no;j++)
//                      {
//                        Slc_Id[j].Slc_Rf_Fail = 0;									
//                      }
//                    }
//                }
//            }	
//        }
//    }
//}


void Dali_Driver_Check(void)
{
    Set_Do(1);
    cheak_emt_timer_sec =0;
    while(cheak_emt_timer_sec < 5)
    {
            dimming_applied(0);
            Read_EnergyMeter();
            calculate_3p4w();
            Dali_At_0Dim = w1;
    }
    while(cheak_emt_timer_sec < 10)
    {

            dimming_applied(25);
            Read_EnergyMeter();
            calculate_3p4w();
            Dali_At_25Dim = w1;

    }
    while(cheak_emt_timer_sec < 15)
    {
            dimming_applied(50);
            Read_EnergyMeter();
            calculate_3p4w();
            Dali_At_50Dim = w1;
    }
    while(cheak_emt_timer_sec < 20)
    {
            dimming_applied(75);
            Read_EnergyMeter();
            calculate_3p4w();
            Dali_At_75Dim = w1;

    }
    while(cheak_emt_timer_sec < 25)
    {
            dimming_applied(100);
            Read_EnergyMeter();
            calculate_3p4w();
            Dali_At_100Dim = w1;

    }
    Set_Do(0);
}


