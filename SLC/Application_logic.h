#ifndef APPLICATION_LOGIC
#define APPLICATION_LOGIC
#include "app/framework/include/af.h"
#define DO_1  PORTA_PIN(6)//PORTFbits.RF4
#define DO_2  PORTA_PIN(7)//PORTFbits.RF5

//#define DO_feedback 	PORTBbits.RB8
//#define Photo_feedback  PORTBbits.RB9		// POH
#if defined(ISLC_3100_V7_7)
#define MUX_A0   PORTD_PIN(4)
#define MUX_A1   PORTE_PIN(0)  
#elif defined(ISLC_3100_V7_9)
#define MUX_A0   PORTD_PIN(4)
#endif

#define PDO				PORTDbits.RD0


#define PWM_INIT_VAL 3199

// GPS hardware shifting

#ifdef ISLC_3100_V7
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // inbuilt photosensor
   #define MOTION_DI1      (unsigned char)((GPIO_PAIN&PA4)>>PA4_BIT)       // DI-1
   //  #define MOTION_DI1      (unsigned char)((GPIO_PAIN&PA5)>>PA5_BIT)       // DI-2
#elif defined(ISLC_3100_V5_V6)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // inbuilt photosensor
   #define MOTION_DI1      (unsigned char)((GPIO_PBIN&PB2)>>PB2_BIT)       // DI-2
#elif defined(ISLC_10)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // inbuilt photosensor
#elif defined(ISLC_T8)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // DI-3
   #define MOTION_DI1      (unsigned char)((GPIO_PBIN&PB2)>>PB2_BIT)       // DI-2
#elif defined(ISLC_T8_V4)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // DI-3
   #define MOTION_DI1      (unsigned char)((GPIO_PBIN&PB2)>>PB2_BIT)       // DI-2
   #define DI1      (unsigned char)((GPIO_PBIN&PB1)>>PB1_BIT)                // DI-1
#elif defined(ISLC3300_T8_V2)
  // #define Photo_feedback_pin 0// (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // DI-3
   #define MOTION_DI1      (unsigned char)((GPIO_PBIN&PB2)>>PB2_BIT)       // DI-2
   #define DI1      (unsigned char)((GPIO_PBIN&PB1)>>PB1_BIT)                // DI-1

#elif defined(ISLC_3100_7P_V1)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // inbuilt photosensor

#elif defined(ISLC_3100_V7_2)
/*
As discussed with Sudhanshu,Jatin,Jagdish,Finalized to swap below 2 pin
*/
//     #define TILT_DI1      (unsigned char)((GPIO_PAIN&PA4)>>PA4_BIT)       // DI-1
//     #define MOTION_DI1      (unsigned char)((GPIO_PAIN&PA5)>>PA5_BIT)       // DI-2
     #define MOTION_DI1      (unsigned char)((GPIO_PAIN&PA4)>>PA4_BIT)       // DI-1
     #define TILT_DI1      (unsigned char)((GPIO_PAIN&PA5)>>PA5_BIT)       // DI-2

   //unsigned char Photo_feedback_pin =1;// analog photosensor no need to give input #define Photo_feedback_pin  (unsigned char)((GPIO_PCIN&PC1)>>PC1_BIT)   // inbuilt photosensor
#elif (defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_480V_Rev3)||defined(ISLC_3100_V7_9))
     //#define TILT_DI1      (unsigned char)((GPIO_PAIN&PA2)>>PA2_BIT)       // DI-1
     //#define MOTION_DI1      (unsigned char)((GPIO_PAIN&PA3)>>PA3_BIT)       // DI-2
      #define TILT_DI1      (unsigned char)((GPIO_PEIN&PE1)>>PE1_BIT)       // DI-1
      #define MOTION_DI1      (unsigned char)((GPIO_PBIN&PB1)>>PB1_BIT)       // DI-2
      #define TX_DALI       PORTE_PIN(2)//(unsigned char)((GPIO_PAIN&PA4)>>PA4_BIT)
      #define EN_DALI      PORTD_PIN(3)// (unsigned char)((GPIO_PBIN&PB5)>>PB5_BIT)   // inbuilt photosensor
      #if defined(ISLC_3100_V7_7)
            #define RS485_DI2_EN1  PORTE_PIN(3)
            #define RS485_DI2_EN2  PORTC_PIN(0)
      #elif defined(ISLC_3100_V7_9)
            #define RS485_DI2_EN2  PORTC_PIN(0)
      #endif
    ////////
#elif defined(ISLC_3100_V9)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // inbuilt photosensor
   #define TX_DALI       PORTA_PIN(4)//(unsigned char)((GPIO_PAIN&PA4)>>PA4_BIT)
   #define EN_DALI      PORTB_PIN(5)// (unsigned char)((GPIO_PBIN&PB5)>>PB5_BIT)   // inbuilt photosensor
#elif defined(ISLC_3100_V9_2)
   #define Photo_feedback_pin  (unsigned char)((GPIO_PBIN&PB4)>>PB4_BIT)   // inbuilt photosensor
   #define TX_DALI       PORTA_PIN(4)//(unsigned char)((GPIO_PAIN&PA4)>>PA4_BIT)
   #define EN_DALI      PORTB_PIN(3)// (unsigned char)((GPIO_PBIN&PB5)>>PB5_BIT)   // inbuilt photosensor
#else
   #error "please select SLC PCB version"
#endif


#if (defined(ISLC_3100_V7) && (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1) ||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)))
   #error "More than one PCB type can not define"
#elif (defined(ISLC_3100_V9_2) && (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1) ||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)))
   #error "More than one PCB type can not define"

#elif (defined(ISLC_3100_V5_V6) && (defined(ISLC_3100_V7) || defined(ISLC_10) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)))
   #error "More than one PCB type can not define"
#elif (defined(ISLC_10) && (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)))
   #error "More than one PCB type can not define"
#elif (defined(ISLC_T8) && (defined(ISLC_3100_V7)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)))
   #error "More than one PCB type can not define"
#elif (defined(ISLC3300_T8_V2) && (defined(ISLC_3100_V7)||defined(ISLC_T8) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)))
   #error "More than one PCB type can not define"
#elif (defined(ISLC_3100_7P_V1) && (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8)||defined(ISLC3300_T8_V2)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)|| defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2)))
   #error "More than one PCB type can not define"
#elif (defined(ISLC_3100_V9) && defined(ISLC_3100_V9_2)&& (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)))
   #error "More than one PCB type can not define"
#elif defined(NEW_LAMP_FAULT_LOGIC) && defined(OLD_LAMP_FAULT_LOGIC)
   #error "More than one LAMP fault type can not define"
#endif

#if (defined(DALI_SUPPORT) && (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8)||defined(ISLC3300_T8_V2) || defined(ISLC_3100_7P_V1)|| defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_480V_Rev3)))
   #error "DALI Supports only V9 PCB"
#endif

#if (defined(SUPER_CAP) && (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8) || defined(ISLC_3100_7P_V1)|| defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3)))
    #ifndef ISLC_T8_V4
       #error "SUPER CAP Supports only V9 PCB"
    #endif
#endif

#define SUNRISE 0
#define SUNSET  1

#define PI 3.1415926535897932384626433


struct Schedule
{
	unsigned char	bSchFlag;
	unsigned char 	cStartHour;
	unsigned char	cStartMin;
	unsigned char 	cStopHour;
	unsigned char	cStopMin;
};

//SAN
struct DimSchedule
{
	unsigned char	bSchFlag;
	unsigned char 	cStartHour;
	unsigned char	cStartMin;
	unsigned char 	cStopHour;
	unsigned char	cStopMin;
	unsigned char   dimvalue;
};

struct Mix_DimSch
{
	unsigned char	bSchFlag;
	unsigned char 	cStartHour;
	unsigned char	cStartMin;
	unsigned char 	cStopHour;
	unsigned char	cStopMin;
	unsigned char   dimvalue;
	unsigned char   Dim_Mode;
};
struct M_Schedule
{
	unsigned char	bSchFlag;
	unsigned char 	cStartHour;
	unsigned char	cStartMin;
	signed char		cStartOff;
	unsigned char 	cStopHour;
	unsigned char	cStopMin;
	signed char		cStopOff;
	unsigned char   dimvalue;
	unsigned char   Dim_Mode;
};


struct Stree_light_Lat
{
	 char deg;
	 char min;
	 char sec;
};

struct Stree_light_Log
{

	 char 	deg;
	 char 	min;
	 char 	sec;
};

struct Stree_light_UTC
{
	 char 	hour;
	 char 	min;
	 char ScheduleConfig;
};
struct M_Compress_Schedule
{
	unsigned char compress1;
	unsigned char compress2;
	unsigned char compress3;
	unsigned char compress4;
	unsigned char compress5;
	unsigned char compress6;
};
 struct Slc_M_Sch
{
	unsigned char SLC_Mix_En;
	unsigned char SLC_Mix_Sch_Start_Date;
	unsigned char SLC_Mix_Sch_Start_Month;
	unsigned char SLC_Mix_Sch_Stop_Date;
	unsigned char SLC_Mix_Sch_Stop_Month;
	unsigned char SLC_Mix_Sch_WeekDay;
	unsigned char SLC_Mix_Sch_Photocell_Override;
	struct M_Schedule Slc_Mix_Sch[9];
};
struct Slc_Compress_M_Sch
{
	unsigned char SLC_Mix_En;
	unsigned char SLC_Mix_Sch_Start_Date;
	unsigned char SLC_Mix_Sch_Start_Month;
	unsigned char SLC_Mix_Sch_Stop_Date;
	unsigned char SLC_Mix_Sch_Stop_Month;
	unsigned char SLC_Mix_Sch_WeekDay;
	unsigned char SLC_Mix_Sch_Photocell_Override;
	struct M_Compress_Schedule Slc_Mix_Sch[9];
};

extern unsigned char Default_Para_Set;
extern unsigned char RTC_Faulty_Shift_to_Local_Timer;
extern struct Stree_light_Lat Lat;
extern struct Stree_light_Log Log;
extern struct Stree_light_UTC UTC;
extern float Latitude,longitude;
extern float utcOffset;
extern long sunriseTime,sunsetTime;
extern BYTE_VAL error_condition,error_condition1,test_result,day_sch,test_result2,day_sch_R;
extern struct Schedule sSch[1][10],Astro_sSch[2],DLH_sSch[2];	// V6.1.10
extern struct Slc_Compress_M_Sch Slc_Comp_Mix_Mode_schedule[];
extern struct DimSchedule Master_sSch[1][10];
extern struct Mix_DimSch Mix_sSch[];
extern unsigned char photo_cell_timer;
extern unsigned char photo_cell_toggel_timer;
extern unsigned char cycling_lamp_fault;
extern unsigned int check_current_counter;
extern unsigned int check_current_firsttime_counter;
extern unsigned char check_cycling_current_counter;
extern unsigned char check_half_current_counter;
extern unsigned int lamp_off_on_timer,Dimming_start_timer;
extern unsigned char detect_lamp_current;
extern float lamp_current;
extern unsigned char chk_interlock;
extern unsigned char check_counter;
extern unsigned char lamp_lock;
extern unsigned char photo_cell_toggel_counter;
extern unsigned char low_current_counter;
extern unsigned int Vol_hi;
extern unsigned int Vol_low;
extern unsigned int Curr_Steady_Time;
extern unsigned int Per_Val_Current;
extern unsigned int Lamp_Fault_Time;
extern unsigned int Lamp_off_on_Time;
extern unsigned int Lamp_faulty_retrieve_Count;
extern unsigned int Sec_Counter;
extern unsigned char automode;
extern unsigned char Photo_Cell_Ok;
extern unsigned char Set_Do_On;
extern unsigned char Cloudy_Flag;
extern unsigned char Master_event_generation;
extern unsigned char event_counter;

extern unsigned char energy_time;
extern unsigned char energy_meter_ok;
extern float current_creep_limit;
extern unsigned int Lamp_lock_condition;

extern unsigned char protec_flag;
extern unsigned char old_dim_val;
extern unsigned char dim_inc_timer;
extern unsigned char dim_inc_val;
extern unsigned int dim_applay_time;

extern unsigned char Master_Sch_Match;				// Master sch 15/04/11
extern unsigned char Photo_feedback;

extern unsigned char ballast_type;
extern unsigned char adaptive_light_dimming;
extern unsigned int analog_input_scaling_high_Value;
extern unsigned int analog_input_scaling_low_Value;
extern unsigned int desir_lamp_lumen;
extern unsigned char lumen_tollarence;
extern unsigned int desir_lamp_lumen;


extern unsigned char Motion_pre_val;
extern unsigned char Motion_countr;
extern unsigned char Motion_detected;
extern unsigned int motion_dimming_timer;
extern unsigned int motion_intersection_dimming_timer;
extern unsigned int Motion_dimming_time;
extern unsigned char Motion_dimming_percentage;
extern unsigned char motion_dimming_Broadcast_send;
extern unsigned char motion_dimming_send;
extern unsigned char Motion_detected_broadcast;
extern unsigned char Motion_normal_dimming_percentage;
extern unsigned char Motion_group_id;
extern unsigned char Motion_pulse_rate;
extern unsigned char motion_broadcast_timer;
extern unsigned char Motion_half_sec;
extern unsigned char Motion_Continiue;
extern unsigned char Motion_detect_sec;
extern unsigned char Motion_Detect_Timeout;
extern unsigned char Motion_Broadcast_Timeout;
extern unsigned char Motion_Sensor_Type;
extern unsigned char Motion_continue_timer;
extern unsigned char Motion_Received_broadcast;
extern unsigned char Motion_Received_broadcast_counter;
extern unsigned char Motion_Rebroadcast_timeout;
extern unsigned char Relay_weld_timer;
extern unsigned char Motion_intersection_detected;

extern struct Slc_M_Sch Slc_Mix_Mode_schedule[];
extern unsigned char Civil_Twilight;

extern unsigned char Schedule_offset;
extern unsigned char Photocell_steady_timeout_Val;    // hour
extern unsigned char photo_cell_toggel_counter_Val;   // no of count
extern unsigned char photo_cell_toggel_timer_Val;     // hour
extern unsigned char Photo_cell_Frequency;            // 30 pulse per sec.
extern unsigned int Photocell_unsteady_timeout_Val;   // up to this sec photocell not steady.
extern unsigned char day_burner_timer;
extern unsigned char RTC_faulty_detected;
extern unsigned char previous_mode;
extern unsigned char photo_cell_Logic_toggel_counter;
extern unsigned char RTC_Fault_Logic_Result;
extern unsigned char RTC_New_fault_logic_Enable;
//extern struct RTC_Fault_Local RTC_Fault_Logic_data[2];
extern unsigned char photo_cell_toggel;
extern unsigned int RTC_drift;
extern unsigned char Photo_Cell_error;

extern unsigned char f_dalirx;
extern unsigned short int forward;
extern unsigned char answer;

#ifdef NEW_LAMP_FAULT_LOGIC

  extern float KW_Threshold;
  extern unsigned char one_sec_cyclic;

#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

void CheckSchedule (void);
void Set_Do(unsigned char i);
void check_logic(void);
double DegreesToAngle(double degrees, double minutes, double seconds);
double FixValue(double value, double min, double max);
double Rad2Deg(double angle);
double Deg2Rad(double angle);
long int Calculate(unsigned char direction);
void GetSCHTimeFrom_LAT_LOG(void);
unsigned char CheckSchedule_sun(void);
void check_interlock(void);
void check_photocell_interlock(void);
void Check_MASTER_Schedule(void);					// Master sch 15/04/11
void dimming_applied(unsigned char dim_val);
void Check_day_burning_fault(void);
void adaptive_dimming(void);
//void Motion_dimming_applied(unsigned char dim_val);
void Motion_Based_dimming(void);
void Day_light_harves_time(void);
//void Motion_dimming_applied(unsigned char dim_val);
void idimmer_dimming_applied(unsigned char dim_val);
void check_Mix_Mode_schedule(void);
void Run_Mix_Mode_Sch(void);
void check_Day_Light_Saving(void);
unsigned char find_dst_date_from_rule(unsigned char occurance,unsigned char occurance_month);
unsigned char check_mix_mode_week_day(unsigned char sch_weekday);
void Mix_MOD_Motion_Based_dimming(void);
void Increment_Date(void);
void Decrement_Date(void);
void check_RTC_faulty_logic(void);
void Fill_RTC_BY_Local_Timer(void);

//void Local_Decrement_Date(void);
//void Local_Increment_Date(void);
//void check_Local_Day_Light_Saving(void);
void GetSCHTimeFrom_LAT_LOG_Local(void);
//long int Local_Calculate(unsigned char direction);
//void store_time_photocell_toggle(unsigned char occurance);
unsigned char sendDaliCommand_ToCheck_DimmerDriver();
unsigned char detectDimmerDriver();
//+++++++++++++++++RS485 And DI2+++++++++++++++++++++++++

#define RS485RX         0
#define RS485TX         1
extern  unsigned char RS485_0_Or_DI2_1;
unsigned char Select_RS485_Or_DI2(unsigned char rs485_0_Or_di2_1);
unsigned char RS485_ModeSelection_0RX_1TX(unsigned char RS485RX_0_Or_RS485TX_1);
void send_data_on_RS485(unsigned char *data,unsigned int len);
void send_data_on_RS485_uart(unsigned char *data,unsigned int len);
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

//extern unsigned int temp_ms_timer;
#endif

