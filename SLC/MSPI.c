/*********************************************************************
 *
 *                  Master SPI routintes
 *
 *********************************************************************
 * FileName:        MSPI.c
 * Dependencies:
 * Processor:       PIC18 / PIC24 / dsPIC33
 * Complier:        MCC18 v1.00.50 or higher
 *                  MCC30 v2.05 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (c) 2004-2008 Microchip Technology Inc.  All rights reserved.
 *
 * Microchip licenses to you the right to use, copy and distribute Software 
 * only when embedded on a Microchip microcontroller or digital signal 
 * controller and used with a Microchip radio frequency transceiver, which 
 * are integrated into your product or third party product (pursuant to the 
 * sublicense terms in the accompanying license agreement).  You may NOT 
 * modify or create derivative works of the Software.  
 *
 * If you intend to use this Software in the development of a product for 
 * sale, you must be a member of the ZigBee Alliance.  For more information, 
 * go to www.zigbee.org.
 *
 * You should refer to the license agreement accompanying this Software for 
 * additional information regarding your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY 
 * OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR 
 * PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED 
 * UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF 
 * WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR 
 * EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, 
 * PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF 
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY 
 * THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER 
 * SIMILAR COSTS.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Nilesh Rajbharti     7/12/04 Original
 * Nilesh Rajbharti     11/1/04 Pre-release version
 * DF/KO                04/29/05 Microchip ZigBee Stack v1.0-2.0
 * DF/KO                07/18/05 Microchip ZigBee Stack v1.0-3.0
 * DF/KO                07/27/05 Microchip ZigBee Stack v1.0-3.1
 * DF/KO                01/09/06 Microchip ZigBee Stack v1.0-3.5
 * DF/KO                08/31/06 Microchip ZigBee Stack v1.0-3.6
 * DF/KO/YY             11/27/06 Microchip ZigBee Stack v1.0-3.7
 * DF/KO/YY				01/12/07 Microchip ZigBee Stack v1.0-3.8
 * DF/KO/YY             02/26/07 Microchip ZigBee Stack v1.0-3.8.1
 ********************************************************************/
#include "SLC/MSPI.h"
#include "app/framework/include/af.h"
#include  "hal/micro/serial.h"
//#include "Zigbee.def"
//#include "Compiler.h"

//#if defined(__dsPIC33F__) || defined(__PIC24F__) || defined(__PIC24H__)

//	void RF_SPIPut(BYTE v)
//	{
//    	BYTE dummy;
//  	    
//        Time_Out = 0;  
//	    RF_SSPIF_BIT = 0;
//    	dummy = SPI1BUF;
//	    RF_SSPBUF_REG = v;
//        while(RF_SSPIF_BIT == 0) 
//		{
//			if(Time_Out > 5)
//			{
//				break;
//			}
//		}
//	}
//
//	BYTE RF_SPIGet(void)
//	{
//    	RF_SPIPut(0x00);
//	    return RF_SSPBUF_REG; 
//	}
//
//	void EE_SPIPut(BYTE v)
//	{
//		BYTE dummy;
//		Time_Out = 0;
//		EE_SSPIF_BIT = 0;
//		dummy = EE_SSPBUF_REG; 
//		EE_SSPBUF_REG = v;
//		while(EE_SSPIF_BIT == 0 )
//		{
//			if(Time_Out > 5)
//			{
//				break;
//			}
//		}
//	}
//
//	BYTE EE_SPIGet(void)
//	{
//		EE_SPIPut(0x00);
//		return EE_SSPBUF_REG; 
//	}

//unsigned char ReadByte_EEPROM(unsigned int add)
//{
//	 unsigned char dest=0;
//	 
//	 add = EXTERNAL_NVM_BYTES + 10 + add;
//
//     SPISelectEEPROM();
//     EE_SPIPut( SPIREAD );
//     EE_SPIPut( (BYTE)(((WORD)add>>8) & 0xFF) );
//     EE_SPIPut( (BYTE)((WORD)add & 0xFF) );
//     dest = EE_SPIGet();
//     SPIUnselectEEPROM();
//
//	 return dest;
//}

//void WriteByte_EEPROM( unsigned int add,unsigned char data)
//{
// unsigned char status;      
//
//  		add = EXTERNAL_NVM_BYTES + 10 + add;
//        // Make sure the chip is unlocked.
//        SPISelectEEPROM();                  // Enable chip select
//        EE_SPIPut( SPIWRSR );                  // Send WRSR - Write Status Register opcode
//        EE_SPIPut( 0x00 );                     // Write the status register
//        SPIUnselectEEPROM();                // Disable Chip Select
//
//        SPISelectEEPROM();                          // Enable chip select
//        EE_SPIPut( SPIWREN );                          // Transmit the write enable instruction
//        SPIUnselectEEPROM();                        // Disable Chip Select to enable Write Enable Latch
//
//        SPISelectEEPROM();                          // Enable chip select
//        EE_SPIPut( SPIWRITE );                         // Transmit write instruction
//        EE_SPIPut( (BYTE)((add>>8) & 0xFF) );   // Transmit address high byte
//        EE_SPIPut( (BYTE)((add) & 0xFF) );      // Trabsmit address low byte
//
//		EE_SPIPut(data);	
//		SPIUnselectEEPROM();
//		Time_Out = 0;
//        do
//        {
//            SPISelectEEPROM();                  // Enable chip select
//            EE_SPIPut( SPIRDSR );                  // Send RDSR - Read Status Register opcode
//            status = EE_SPIGet();;                 // Read the status register
//            SPIUnselectEEPROM();                // Disable Chip Select
//			CLRWDT()
//			if(Time_Out > 5)
//			{
//				break;
//			}
//        }
//        while (status & WIP_MASK);

 //}



//#else
//    #error Unknown processor.  See Compiler.h
//#endif

///////////////////////////////////I2C interface//////////////////////////////////////////////////////////////////////////////////////////
//#include "extern.h"
/*****************************************************************************************/
/*
   I2CMEM.C for V3.0.0
*/
/*****************************************************************************************/

void DelayI2C(long K)
{
long i;
for(i=0;i<K;i++)
 {halResetWatchdog();}
}

void SetSCL()
{
	// IOSET0 |= SCL0; //change by hitesh
  halClearLed(SCL0);
	 DelayI2C(150);	 
}
void SetSDA()
{
//	IODIR0 |= SDA0;			//SDA0 as Write only direction
//	IOSET0 |= SDA0;
//  GPIO_PBCFGL = (GPIOCFG_OUT        <<PB0_CFG_BIT)|               
//                (GPIOCFG_OUT        <<PB1_CFG_BIT)|  
//                (GPIOCFG_OUT     <<PB2_CFG_BIT)|  
//                (GPIOCFG_OUT     <<PB3_CFG_BIT); 
  halClearLed(SDA0);
	DelayI2C(150);
}
void ClrSCL()
{
	//IOCLR0 |= SCL0;
  halSetLed(SCL0);
	DelayI2C(150);	
}
void ClrSDA()
{
//	IODIR0 |= SDA0;			//SDA0 as Write only direction
//	IOCLR0 |= SDA0;
//   GPIO_PBCFGL = (GPIOCFG_OUT        <<PB0_CFG_BIT)|               
//                (GPIOCFG_OUT        <<PB1_CFG_BIT)|  
//                (GPIOCFG_OUT     <<PB2_CFG_BIT)|  
//                (GPIOCFG_OUT     <<PB3_CFG_BIT); 
  halSetLed(SDA0);
	DelayI2C(150);
}

unsigned char ReadSDA()
{
	long data;
        unsigned char TmpByte=0;
        
//    GPIO_PBCFGL = (GPIOCFG_OUT        <<PB0_CFG_BIT)|               
//                (GPIOCFG_OUT        <<PB1_CFG_BIT)|  
//                (GPIOCFG_IN_PUD     <<PB2_CFG_BIT)|  
//                (GPIOCFG_OUT     <<PB3_CFG_BIT);  
        DelayI2C(100);
    TmpByte = (unsigned char)((GPIO_PBIN&PB2)>>PB2_BIT);
    
  if(TmpByte ==1)
    return 1;
  else
    return 0;
////	IODIR0 &= (~SDA0);
//	DelayI2C(100);
////	data = IOPIN0;
//	if(data & SDA0)
//		return 1;
//	else
//		return 0;
}
void InitI2C (void)
{
   #ifndef USE_UART
	unsigned char i;

//	PINSEL1 &= 0xFC3FFFFF;	//select SCL & SDA pin as GPIO

//	IODIR0 |= SCL0;			//SCL0 as Write only direction
        halSetLed(WP);//  put low for normal operation
	SetSDA();
	SetSCL();
      
	for(i=0;i<9;i++)
	{
		ClrSCL();
		SetSCL();
	}
#endif
}

/*AT24C512 */
void Start(void)
{
	SetSDA();
	SetSCL();
	ClrSDA();
	ClrSCL();
}

void Stop(void)
{
	ClrSCL();
	SetSDA();
	ClrSDA();
	SetSCL();
	SetSDA();
}

void DeviceAddress (unsigned char bankno, unsigned char mode)
{
	unsigned char deviceid, i;

	switch(bankno)
	{
	case 0:
		deviceid = 0xA0;
		break;
	case 1:
		deviceid = 0xA2;
		break;
	case 2:
		deviceid = 0xA4;
		break;
	case 3:
		deviceid = 0xA6;
		break;
	}

	if(mode == READ)
		deviceid |= 0x01;

//	AddressWrite_eeprom(deviceid);
	for(i=0; i<8; i++)
	{
		ClrSCL();
		if((deviceid & 0x80))
			SetSDA();
		else
			ClrSDA();
		
		SetSCL();

		deviceid = deviceid << 1;
	}
	
        ClrSCL();
	SetSCL();
	if(ReadSDA())
	{
		//NACK
	}
	else
	{
		//ACK
	}
          ClrSCL();
}

void AddressWrite_eeprom (unsigned char datai)
{

unsigned char j;

	
	for(j=0; j<8; j++)
	{
		ClrSCL();
		if((datai & 0x80))
			SetSDA();
		else
			ClrSDA();

		SetSCL();

		datai = datai << 1;
	}
	

  	ClrSCL();
	SetSCL();

	if(ReadSDA())
	{
		//NACK
	}
	else
	{
		//ACK
	}
	ClrSCL();
}

void DataWrite ( unsigned char datai)
{

unsigned char k;

	
	for(k=0; k<8; k++)
	{
		ClrSCL();
		if((datai & 0x80))
			SetSDA();
		else
			ClrSDA();
	
		SetSCL();
		datai = datai << 1;
	}
	

	ClrSCL();
	SetSCL();
	if(ReadSDA())
	{
		//NACK
	}
	else
	{
		//ACK
	}
	ClrSCL();
}

unsigned char DataRead (void)
{
	unsigned char i, datain=0, temp = 0x01;
	
	for(i=0;i<8;i++)
	{
		ClrSCL();
		SetSCL();
                //hitesh_samir=ReadSDA();
		if(ReadSDA())
			datain |= (temp << (7-i));
	}	
	ClrSCL();
	
	return(datain);
}

void AckI2C (void)
{

	ClrSCL();
	SetSDA();
	ClrSDA();
	SetSCL();
	ClrSCL();
	SetSDA();
}

void NackI2C (void)
{
	ClrSCL();
	SetSDA();
	SetSCL();
	ClrSCL();
}
/*****************************************************************************/
/*      WriteByte ():

 		Description: This function stores one data byte to the address specified.

		Arguments: 	bankno -> bank no of EEPROM
					address -> addresss at which data to be stored
					databyte -> data byte to be stored 

		Return Value: none

*/
/*****************************************************************************/	  
//void WriteByte (unsigned int address, unsigned char databyte) // void WriteByte(EEPROM address, byte to write);
void WriteByte_EEPROM( unsigned int address,unsigned char databyte)
{
#ifndef USE_UART
    unsigned char bankno=0;
	Start();
	DeviceAddress(bankno,WRITE);
	AddressWrite_eeprom((unsigned char)(address & 0xFF00) >> 8);	//high byte address

	AddressWrite_eeprom((unsigned char)(address & 0x00FF));	//low byte address

        //AddressWrite_eeprom(databyte);
	DataWrite(databyte);
	
	Stop();
	DelayI2C(100000);
#endif       
}

//unsigned char ReadByte (unsigned int address) // read byte ReadByte (EEPROM address);
unsigned char ReadByte_EEPROM(unsigned int address)
{
  #ifndef USE_UART
	unsigned char datain,bankno=0;
	Start();
	DeviceAddress(bankno,WRITE);


	AddressWrite_eeprom((unsigned char)(address & 0xFF00) >> 8);	//high byte address

	AddressWrite_eeprom((unsigned char)(address & 0x00FF));	//low byte address

	Start();
	DeviceAddress(bankno,READ);
	datain = DataRead();
	NackI2C();
	Stop();

	return(datain);
#endif
}

/*****************************************************************************/	  
void InitEEPROM ()
{
//	InitI2C();
}
