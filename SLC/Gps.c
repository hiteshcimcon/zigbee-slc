//////////////////GPS.c///////////////////

#include "app/framework/include/af.h"
#include "generic.h"
#include "Gps.h"
#include "Application_logic.h"
#include "protocol.h"
#include "DS1339.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
float stof(const char* s);
unsigned char RTC_Faulty_Shift_to_Local_Timer =0;
unsigned char GPS_reading = 1;
unsigned char GPS_Data_Valid = 0;
double Gps_latitude = 0.0;
double Gps_longitude = 0.0;
unsigned char GPS_Str_rec = 0;
unsigned char GPS_Read_Enable = 1;
unsigned int GPS_read_success_counter = 0;
unsigned int GPS_Wake_timer = 120;
unsigned char Packate_send_success = 0;
unsigned char Time_set_by_GPS = 0;
unsigned char GPS_power_On_detected = 1;
unsigned char Packate_send_threshold_success = 0;
unsigned char Setalite_Visinity_Proper = 0;
unsigned char threeD_Fix_successfully = 0;
unsigned char SBAS_Active = 0;
unsigned int gagan_Setalite_Id = 0;
unsigned int gagan_Setalite_RSSI = 0;
struct setalit Setalite_info[32];
//struct GPS_Status GPS_Info[2];

unsigned char No_of_setalite_threshold = 6;
unsigned char Setalite_angle_threshold = 15;
unsigned char setalite_Rssi_threshold = 35;
unsigned char Maximum_no_of_Setalite_reading = 4;
unsigned char Sbas_setalite_Rssi_threshold = 35;
unsigned char Check_Sbas_Enable = 1;

float HDOP_Gps = 0.0;
float HDOP_Gps_threshold = 2.00;
unsigned char HDOP_OK = 0;
unsigned char Sbas_reading_OK = 0;
unsigned char GPS_reading_comming_OK = 0;
unsigned char Start_averaging_of_GPS = 0;
unsigned int averaging_counter = 0,averaging_Timer = 0;
double avg_latitude = 0.0,avg_longitude = 0.0;
unsigned char need_to_send_GPS_command = 0;

unsigned int GPRMC_Received_counter = 0;
unsigned int GPGSA_Received_counter = 0;
unsigned int GPGGA_Received_counter = 0;
unsigned int GPGSV_Received_counter = 0;

unsigned char Send_GPS_Data_at_valid_Dcu = 0;
unsigned char GPS_Rescan = 0;
unsigned char need_to_send_Shutdown_command = 0;
unsigned char GPS_Delay_timer = 0;
unsigned char GPS_Data_throw_status = 0;
unsigned char need_Average_value_Gps = 0;

extern float Latitude,longitude;
extern unsigned char need_to_send_lat_long;
//extern struct Astro_sSch[2];
float difference1,difference2;
unsigned char Auto_Gps_Data_Send = 1;
BYTE_VAL  GPS_error_condition;

unsigned char GPS_run_extra_count = 0,Time_Date_Not_Ok =0;
unsigned char GPS_String_Valid = 0;


void Parse_GPS_Query(unsigned char *rec_buff,unsigned int temp_rx_serial)
{
  unsigned char GPS_RMC_Revd_Byte;
  unsigned int x=0,temp_position,Setalite_string_No;

//  unsigned char Display[50];

//  sprintf(Display,"\r\ntemp_rx_serial = %u",temp_rx_serial);
//  emberAfGuaranteedPrint(" %p", Display);
//  emberSerialWaitSend(APP_SERIAL);

  for(x=0;x<temp_rx_serial;x++)
  {
    if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'G') && (rec_buff[x+2] == 'P')&& (rec_buff[x+3] == 'R') && (rec_buff[x+4] == 'M') && (rec_buff[x+5] == 'C'))
    {
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      {
        GPS_String_Valid = 1;
//        emberAfGuaranteedPrint(" %p", "\r\n GPRMC Received");
//        emberSerialWaitSend(APP_SERIAL);
//        GPRMC_Received_counter++;
        //if(rec_buff[x+18] == 'A')
	if((rec_buff[x+17] == ',')&&(rec_buff[x+18] == 'A')&&(rec_buff[x+19] == ','))
        {	
          GPS_Data_Valid = 1;	

//          GPS_read_success_counter++;

          Gps_latitude = ddmm2degree(Find_float_from_string(&rec_buff[x+0],3,temp_rx_serial));
          if(rec_buff[x+30] == 'S')
          {
            Gps_latitude = Gps_latitude * -1;
          }
//          sprintf(Display,"\r\nGps_latitude = %f",Gps_latitude);
//          emberAfGuaranteedPrint(" %p", Display);
//          Gps_longitude = strtoflt((char *)rec_buff , x+33 ,9);
          Gps_longitude = ddmm2degree(Find_float_from_string(&rec_buff[x+0],5,temp_rx_serial));
          if(rec_buff[x+43] == 'W')
          {
            Gps_longitude = Gps_longitude * -1;
          }
//          sprintf(Display,"\r\nGps_longitude = %f",Gps_longitude);
//          emberAfGuaranteedPrint(" %p", Display);

          if((Time_set_by_GPS == 0))
          {
                Time_Date_Not_Ok =0;
                find_time_from_GPS(&rec_buff[x+0],temp_rx_serial);
                if((Time_Date_Not_Ok == 1)||(GPS_Date.Date == 0)||(GPS_Date.Month == 0)||(GPS_Date.Year == 0)) //added this since get randomly 0,0,0 value and due to this derivd value becomes 311295 so to avoid this added this logic...
                {
                  GPS_String_Valid =0; //put this checking here since if stirng is not valid then no need to update LAt,Long
                  GPS_Data_Valid = 0;
                }
                else
                {
                    Set_GPS_Time_to_RTC();
                    Time_set_by_GPS = 1;
                    RTC_need_to_varify = 1;
                    RTC_power_on_Logic_Timer = 0;         // for testing
                    RTC_Faulty_Shift_to_Local_Timer =1; //no need to put here rtc faulty check

                }
          }

        }
        else
        {
          GPS_Data_Valid = 0;
//          GPS_read_success_counter++;
        }	
      }
      else
      {
//        emberAfGuaranteedPrint(" %p", "\r\n GPRMC CRC ERROR");
//        emberSerialWaitSend(APP_SERIAL);
        GPS_Data_Valid = 2;
//        GPS_read_success_counter++;
      }
    }
    else if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'G') && (rec_buff[x+2] == 'P')&& (rec_buff[x+3] == 'G') && (rec_buff[x+4] == 'S') && (rec_buff[x+5] == 'A') && (rec_buff[x+6] == ',') && (rec_buff[x+7] == 'A') && (rec_buff[x+8] == ','))
    {
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      {
        GPS_String_Valid = 1;
//        emberAfGuaranteedPrint(" %p", "\r\n GPGSA Received");
//        emberSerialWaitSend(APP_SERIAL);
        GPGSA_Received_counter++;
        if(rec_buff[x+9] == '3')
        {
          threeD_Fix_successfully = 1; //
        }
        else
        {
          threeD_Fix_successfully = 0; //
        }
//        sprintf(Display,"\r\nthreeD_Fix_successfully = %u",threeD_Fix_successfully);
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);

        HDOP_Gps = (float)Find_float_from_string(&rec_buff[x+0],16,temp_rx_serial);

        if(HDOP_Gps_threshold != 0)
        {
          if(HDOP_Gps <= HDOP_Gps_threshold)
          {
            HDOP_OK = 1;
          }
          else
          {
            HDOP_OK = 0;
          }
        }
        else
        {
          HDOP_OK = 1;
        }

//        sprintf(Display,"\r\nHDOP = %f",HDOP_Gps);
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);
      }
      else
      {
//        emberAfGuaranteedPrint(" %p", "\r\n GPGSA CRC Error");
//        emberSerialWaitSend(APP_SERIAL);
      }
    }
    else if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'G') && (rec_buff[x+2] == 'P')&& (rec_buff[x+3] == 'G') && (rec_buff[x+4] == 'G') && (rec_buff[x+5] == 'A'))
    {
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      {
        GPS_String_Valid = 1;
//        emberAfGuaranteedPrint(" %p", "\r\n GPGGA Received");
//        emberSerialWaitSend(APP_SERIAL);
        GPGGA_Received_counter++;
        temp_position = find_comma_Position(&rec_buff[x+0],7,temp_rx_serial);

//        sprintf(Display,"\r\nNo_of_setalite = %u",(rec_buff[x+temp_position+1] - 0x30));
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);

        if((rec_buff[x+temp_position+1] - 0x30) >= No_of_setalite_threshold)
        {
          Setalite_Visinity_Proper = 1;
        }
        else
        {
          Setalite_Visinity_Proper = 0;
        }
//        sprintf(Display,"\r\nSetalite_Visinity_Proper = %u",Setalite_Visinity_Proper);
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);

//        sprintf(Display,"\r\nSBAS Value = %u",(rec_buff[x+temp_position-1] - 0x30));
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);

        if(Check_Sbas_Enable == 1)
        {
          if((rec_buff[x+temp_position-1] - 0x30) == 2)
          {
            SBAS_Active = 1;
          }
          else
          {
            SBAS_Active = 0;
          }
        }
        else
        {
          SBAS_Active = 0;
        }

//        sprintf(Display,"\r\nSBAS_Active = %u",SBAS_Active);
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);
      }
      else
      {
//        emberAfGuaranteedPrint(" %p", "\r\n GPGGA CRC Error");
//        emberSerialWaitSend(APP_SERIAL);
      }
    }
    else if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'G') && (rec_buff[x+2] == 'P')&& (rec_buff[x+3] == 'G') && (rec_buff[x+4] == 'S') && (rec_buff[x+5] == 'V') && (rec_buff[x+6] == ','))
    {
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      {
        GPS_String_Valid = 1;
//        emberAfGuaranteedPrint(" %p", "\r\n GPGSV Received");
//        emberSerialWaitSend(APP_SERIAL);
        GPGSV_Received_counter++;
        temp_position = find_comma_Position(&rec_buff[x+0],2,temp_rx_serial);
        Setalite_string_No = rec_buff[x+temp_position+1] - 0x30;
        if((Setalite_string_No >= 1) && (Setalite_string_No < 9))
        {
          Setalite_string_No = Setalite_string_No - 1;
          Setalite_info[(Setalite_string_No*4) + 0].ID = Find_value_from_string(&rec_buff[x+0],4,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 0].ev_angle = Find_value_from_string(&rec_buff[x+0],5,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 0].azimuth = Find_value_from_string(&rec_buff[x+0],6,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 0].RSSI = Find_value_from_string(&rec_buff[x+0],7,temp_rx_serial);

          Setalite_info[(Setalite_string_No*4) + 1].ID = Find_value_from_string(&rec_buff[x+0],8,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 1].ev_angle = Find_value_from_string(&rec_buff[x+0],9,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 1].azimuth = Find_value_from_string(&rec_buff[x+0],10,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 1].RSSI = Find_value_from_string(&rec_buff[x+0],11,temp_rx_serial);

          Setalite_info[(Setalite_string_No*4) + 2].ID = Find_value_from_string(&rec_buff[x+0],12,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 2].ev_angle = Find_value_from_string(&rec_buff[x+0],13,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 2].azimuth = Find_value_from_string(&rec_buff[x+0],14,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 2].RSSI = Find_value_from_string(&rec_buff[x+0],15,temp_rx_serial);

          Setalite_info[(Setalite_string_No*4) + 3].ID = Find_value_from_string(&rec_buff[x+0],16,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 3].ev_angle = Find_value_from_string(&rec_buff[x+0],17,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 3].azimuth = Find_value_from_string(&rec_buff[x+0],18,temp_rx_serial);
          Setalite_info[(Setalite_string_No*4) + 3].RSSI = Find_value_from_string(&rec_buff[x+0],19,temp_rx_serial);
//          sprintf(Display,"\r\nSetalite_string_No = %u",Setalite_string_No);
//          emberAfGuaranteedPrint(" %p", Display);
//          emberSerialWaitSend(APP_SERIAL);
        }
      }
      else
      {
//        emberAfGuaranteedPrint(" %p", "\r\n GPGSV CRC Error");
//        emberSerialWaitSend(APP_SERIAL);
      }
    }
    else if((rec_buff[x+0] == '$') && (rec_buff[x+1] == 'P') && (rec_buff[x+2] == 'M')&& (rec_buff[x+3] == 'T') && (rec_buff[x+4] == 'K') && (rec_buff[x+5] == '0') && (rec_buff[x+6] == '0') && (rec_buff[x+7] == '1'))
    {
      GPS_RMC_Revd_Byte=FindSubstr((char *)&rec_buff[x+1],"*");
      if(CheckCRC((char *)&rec_buff[x+1],GPS_RMC_Revd_Byte))
      {
        if((rec_buff[x+8] == ',') && (rec_buff[x+9] == '1') && (rec_buff[x+10] == '6')&& (rec_buff[x+11] == '1') && (rec_buff[x+12] == ','))
        {
          if(rec_buff[x+13] == '3')
          {
            Packate_send_success = 1;
            need_to_send_Shutdown_command = 0;
            GPS_Data_Valid = 3;              // shutdown due after valid lat/long received.
            GPS_error_condition.bits.b7 = 1;
            GPS_error_condition.bits.b6 = 0;
            GPS_error_condition.bits.b5 = 0;
            GPS_error_condition.bits.b4 = 0;
            GPS_error_condition.bits.b3 = 0;
            GPS_error_condition.bits.b2 = 0;
            GPS_error_condition.bits.b1 = 0;
            GPS_error_condition.bits.b0 = 0;
            GPS_String_Valid = 0;
          }
          else
          {
            Packate_send_success = 0;
          }
        }
        else if((rec_buff[x+8] == ',') && (rec_buff[x+9] == '3') && (rec_buff[x+10] == '8')&& (rec_buff[x+11] == '6') && (rec_buff[x+12] == ','))
        {
          if(rec_buff[x+13] == '3')
          {
            Packate_send_threshold_success = 1;
            test_result.bits.b6=1;
            need_to_send_GPS_command = 0;
            GPS_error_condition.bits.b7 = 0;
          }
          else
          {
            Packate_send_threshold_success = 0;
          }
        }
      }
    }
    else
    {
    }
  }
}

unsigned char CheckCRC(char* gpr_string, unsigned char Gps_len)
{
	int i=0;
	unsigned char ByteSUM=0;
 unsigned char dispStr[5];

	for(i=0;i<Gps_len;i++)
	{
		   ByteSUM^=gpr_string[i];					  		//Calculate Sun here
	}

	sprintf((char *)dispStr,"%02X",ByteSUM);
	if(strncmp((char *)&dispStr[0],(char *)&gpr_string[i+1],2)==0)
	{
		return(1);
	}
	else
	{
		return(0);
	}
}

unsigned char GeneratekCRC(char* gpr_string, unsigned char Gps_len)
{
	int i=0;
	unsigned char ByteSUM=0;

	for(i=0;i<Gps_len;i++)
	{
		   ByteSUM^=gpr_string[i];					  		//Calculate Sun here
	}

	return ByteSUM;	
}

float strtoflt(char inputstr[],int start,int length)
{
   float result= 0;
   int dotpos = 0;
   int n;
   for (n =0; n < length; n++)
   {
     if (inputstr[n + start ] == '.')
     {
       dotpos = length- n -1;
     }
     else
     {
       result = result * 10 + (inputstr[n + start]-'0');
     }
   }
   while ( dotpos--)
   {
     result /= 10;
   }
   return result;
}

int FindSubstr(char *listPointer,char *itemPointer)
{
  int t;
  char *p, *p2;

  for(t=0; listPointer[t]; t++)
  {
    p = &listPointer[t];
    p2 = itemPointer;

    while(*p2 && *p2==*p)
	   {
      p++;
      p2++;
    }
    if(!*p2)
	   {
		     return t; /* 1st return */
	   }
  }
  return -1; /* 2nd return */
}

void send_gps_command(void)
{
	unsigned char Crc_Byte;
	unsigned char temp_buff[80],Tx_buffer[80];


// sprintf((char *)temp_buff,"$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
// send_data_on_uart((char *)temp_buff,strlen(temp_buff));

	sprintf(temp_buff,"PMTK386,0.0");
	Crc_Byte = GeneratekCRC(temp_buff,strlen(temp_buff));
	sprintf(Tx_buffer,"$%s*%x\r\n",temp_buff,Crc_Byte);	
	send_data_on_uart((char *)Tx_buffer,strlen(Tx_buffer));
 Packate_send_threshold_success = 0;
 GPS_error_condition.bits.b7 = 0;
	//Delay(100);
}

void shutdown_GPS(void)
{
  unsigned char temp_buff[80];

   sprintf((char *)temp_buff,"$PMTK161,0*28\r\n");
   send_data_on_uart((char *)temp_buff,strlen(temp_buff));
   GPS_Data_Valid = 3;              // shutdown due after valid lat/long received.
   Packate_send_success = 0;
   Start_averaging_of_GPS = 0;
//   GPS_error_condition.bits.b7 = 1;
//   GPS_error_condition.bits.b6 = 0;
//   GPS_error_condition.bits.b5 = 0;
//   GPS_error_condition.bits.b4 = 0;
//   GPS_error_condition.bits.b3 = 0;
//   GPS_error_condition.bits.b2 = 0;
//   GPS_error_condition.bits.b1 = 0;
//   GPS_error_condition.bits.b0 = 0;
}


double ddmm2degree(double data1)
{
	double x,ans;
	
	x = ((int)(data1/100));
	ans = x + (data1 - (x*100))/60;
    return(ans);
}

unsigned int find_comma_Position(unsigned char *res_buff,unsigned char no_of_comma,unsigned int temp_rx_serial)
{
  unsigned int i=0,comma=0;

  for(i=0;i<temp_rx_serial;i++)
  {
    if(res_buff[i] == ',')
    {
      comma++;
      if(comma == no_of_comma)
      {
        return i;
      }
    }
  }
  return 0;
}

unsigned int Find_value_from_string(unsigned char *res_buff,unsigned char no_of_comma,unsigned int temp_rx_serial)
{
  unsigned int i=0,comma=0,j=0,temp_val,Start_read=0;
  unsigned char temp_buff[15];   // ,Display[50]

  for(i=0;i<temp_rx_serial;i++)
  {
    if(res_buff[i] == ',')
    {
      comma++;
      if(comma == no_of_comma)
      {
        Start_read = 1;
//        sprintf(Display,"\r\nstart at str[%d]",i);
//        emberAfGuaranteedPrint(" %p", Display);
//        emberSerialWaitSend(APP_SERIAL);
      }
      else if(comma > no_of_comma)
      {
//        emberAfGuaranteedPrint(" %p", "\r\nBreak at 1");
//        emberSerialWaitSend(APP_SERIAL);
        break;
      }
    }
    else if(res_buff[i] == '*')
    {
//      emberAfGuaranteedPrint(" %p", "\r\nBreak at 2");
//      emberSerialWaitSend(APP_SERIAL);
      break;
    }
    if((Start_read == 1) && (res_buff[i] != ','))
    {
        temp_buff[j] = res_buff[i];
        //sprintf(Display,"\r\ntemp_buff[%d] = %c",j,temp_buff[j]);
        //emberAfGuaranteedPrint(" %p", Display);
        j++;
    }
    //sprintf(Display,"\r\nstr[%d] = %c",i,res_buff[i]);
    //emberAfGuaranteedPrint(" %p", Display);
    //emberSerialWaitSend(APP_SERIAL);
  }
  temp_buff[j] = 0;
  temp_val = atoi(temp_buff);
//  sprintf(Display,"\r\nInt value = %u",temp_val);
//  emberAfGuaranteedPrint(" %p", Display);
//  emberSerialWaitSend(APP_SERIAL);
  return temp_val;
}

double Find_float_from_string(unsigned char *res_buff,unsigned char no_of_comma,unsigned int temp_rx_serial)
{
  unsigned int i=0,comma=0,j=0,Start_read=0;
  double temp_val;
  char temp_buff[15];    // Display[50],

  //sprintf(Display,"\r\ntemp_rx_serial = %d",temp_rx_serial);
  //emberAfGuaranteedPrint(" %p", Display);

  for(i=0;i<temp_rx_serial;i++)
  {
    if(res_buff[i] == ',')
    {
      comma++;
      if(comma == no_of_comma)
      {
        Start_read = 1;
        //sprintf(Display,"\r\nstart at str[%d]",i);
        //emberAfGuaranteedPrint(" %p", Display);
        //emberSerialWaitSend(APP_SERIAL);
      }
      else if(comma > no_of_comma)
      {
        //emberAfGuaranteedPrint(" %p", "\r\nBreak at 1");
        //emberSerialWaitSend(APP_SERIAL);
        break;
      }
    }
    else if(res_buff[i] == '*')
    {
      //emberAfGuaranteedPrint(" %p", "\r\nBreak at 2");
      //emberSerialWaitSend(APP_SERIAL);
      break;
    }
    if((Start_read == 1) && (res_buff[i] != ','))
    {
        temp_buff[j] = res_buff[i];
        //sprintf(Display,"\r\ntemp_buff[%d] = %c",j,temp_buff[j]);
        j++;
        //emberAfGuaranteedPrint(" %p", Display);
        //emberSerialWaitSend(APP_SERIAL);
    }
    //sprintf(Display,"\r\nstr[%d] = %c",i,res_buff[i]);
    //emberAfGuaranteedPrint(" %p", Display);
    //emberSerialWaitSend(APP_SERIAL);
  }
  temp_buff[j] = 0;
//  emberAfGuaranteedPrint(" %p", temp_buff);
  temp_val = atof(temp_buff);
  
    temp_val = stof(temp_buff);
//  sprintf(Display,"\r\nfloat = %f",temp_val);
//  emberAfGuaranteedPrint(" %p", Display);
//  emberSerialWaitSend(APP_SERIAL);
  return temp_val;
}

void check_Gps_Statistics(void)
{
  unsigned char i = 0,Setalite_reading_counter = 0,Setalite_reading_OK = 0;
  Sbas_reading_OK = 0;
//  unsigned char Display[50];
  if(GPS_String_Valid == 1)
  {
    GPS_String_Valid = 0;
    for(i=0;i<32;i++)
    {
      //    sprintf(Display,"\r\nSetalite[%d].ID = %d",i,Setalite_info[i].ID);
      //    emberAfGuaranteedPrint(" %p", Display);
      //    emberSerialWaitSend(APP_SERIAL);
      //
      //    sprintf(Display,"\r\nSetalite[%d].ev_angle = %d",i,Setalite_info[i].ev_angle);
      //    emberAfGuaranteedPrint(" %p", Display);
      //    emberSerialWaitSend(APP_SERIAL);
      //
      //    sprintf(Display,"\r\nSetalite[%d].azimuth = %d",i,Setalite_info[i].azimuth);
      //    emberAfGuaranteedPrint(" %p", Display);
      //    emberSerialWaitSend(APP_SERIAL);
      //
      //    sprintf(Display,"\r\nSetalite[%d].RSSI = %d",i,Setalite_info[i].RSSI);
      //    emberAfGuaranteedPrint(" %p", Display);
      //    emberSerialWaitSend(APP_SERIAL);

      if((Setalite_info[i].ID <= 32) && (Setalite_info[i].ID != 0))
      {
        if((Setalite_info[i].ev_angle >= Setalite_angle_threshold) && (Setalite_info[i].RSSI >= setalite_Rssi_threshold))
        {
          Setalite_reading_counter++;
          if(Setalite_reading_counter >= Maximum_no_of_Setalite_reading)
          {
            Setalite_reading_OK = 1;
          }
          else
          {
            Setalite_reading_OK = 0;
          }
        }
      }
      else if((Setalite_info[i].ID > 32) && (Setalite_info[i].ID <= 64))
      {
        if(Check_Sbas_Enable == 1)
        {
          if(Setalite_info[i].RSSI >= Sbas_setalite_Rssi_threshold)
          {
            Sbas_reading_OK = 1;
            gagan_Setalite_Id  = Setalite_info[i].ID;
            gagan_Setalite_RSSI = Setalite_info[i].RSSI;
          }
          else
          {
            if(Sbas_reading_OK != 1)
            {
              Sbas_reading_OK = 0;
            }
          }
        }
        else
        {
          Sbas_reading_OK = 0;
        }
      }

      Setalite_info[i].ID = 0;
      Setalite_info[i].RSSI = 0;
      Setalite_info[i].ev_angle = 0;
      Setalite_info[i].azimuth = 0;
    }

    //  sprintf(Display,"\r\nSetalite_Proper_reading_counter = %d",Setalite_reading_counter);
    //  emberAfGuaranteedPrint(" %p", Display);
    //  emberSerialWaitSend(APP_SERIAL);

    if((GPS_Data_Valid == 1) && (threeD_Fix_successfully == 1) && (Setalite_Visinity_Proper == 1) && (Setalite_reading_OK == 1) && (HDOP_OK == 1))
    {
      if((Check_Sbas_Enable == 1))
      {
        if((SBAS_Active == 1) && (Sbas_reading_OK == 1))
        {
          GPS_reading_comming_OK = 1;
        }
        else
        {
          GPS_reading_comming_OK = 0;
        }
      }
      else
      {
        GPS_reading_comming_OK = 1;
      }
    }
    else
    {
      GPS_reading_comming_OK = 0;
    }




    if((GPS_power_On_detected == 1) && (Send_GPS_Data_at_valid_Dcu == 0))
    {
      GPS_read_success_counter++;
      if(GPS_read_success_counter >= 120)
      {
        GPS_read_success_counter = 120;
        if((GPS_Data_Valid == 1) && (threeD_Fix_successfully == 1))
        {
          GPS_Rescan = 0;
          difference1 = Latitude - (float)Gps_latitude;
          if(difference1 < 0)
          {
            difference1 = (difference1 * (-1));
          }

          difference2 = longitude - (float)Gps_longitude;
          if(difference2 < 0)
          {
            difference2 = (difference2 * (-1));
          }

          if((difference1 > 0.005) || (difference2 > 0.005))
          {
            Latitude = (float)Gps_latitude;
            halCommonSetToken(TOKEN_Latitude,&Latitude);
            longitude = (float)Gps_longitude;
            halCommonSetToken(TOKEN_longitude,&longitude);

            GetSCHTimeFrom_LAT_LOG();								//get sunset sunrise schedule			
            Astro_sSch[0].cStartHour = sunsetTime/3600;				// V6.1.10
            Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		// V6.1.10
            Astro_sSch[0].cStopHour  = 23;							// V6.1.10
            Astro_sSch[0].cStopMin   = 59;							// V6.1.10
            Astro_sSch[0].bSchFlag   = 1;							// V6.1.10

            Astro_sSch[1].cStartHour = 00;							// V6.1.10
            Astro_sSch[1].cStartMin  = 00;							// V6.1.10
            Astro_sSch[1].cStopHour  = sunriseTime/3600;			// V6.1.10
            Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;		// V6.1.10									
            Astro_sSch[1].bSchFlag   = 1;							// V6.1.10
            need_to_send_lat_long = 1;
            GPS_Data_throw_status = 1;
            need_Average_value_Gps = 1;
          }
          else
          {
            //shutdown_GPS();
            need_to_send_Shutdown_command = 1;
          }
          GPS_power_On_detected = 0;
          GPS_read_success_counter = 0;
        }
      }
      GPS_run_extra_count = 0;
      // on power up check if difference between average value and last stored value is more then 100 meter then resend auto data to LG.
    }
    else if(GPS_Rescan == 2)
    {
      // LG need lat/long value without average so send it immidiatly after 120 sec and fixed GPRMC status valid.
      GPS_read_success_counter++;
      if(GPS_read_success_counter >= 120)
      {
        GPS_read_success_counter = 120;
        if((GPS_Data_Valid == 1) && (threeD_Fix_successfully == 1))
        {
          GPS_Rescan = 0;
          Latitude = (float)Gps_latitude;
          halCommonSetToken(TOKEN_Latitude,&Latitude);
          longitude = (float)Gps_longitude;
          halCommonSetToken(TOKEN_longitude,&longitude);
          GetSCHTimeFrom_LAT_LOG();								//get sunset sunrise schedule			
          Astro_sSch[0].cStartHour = sunsetTime/3600;				// V6.1.10
          Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		// V6.1.10
          Astro_sSch[0].cStopHour  = 23;							// V6.1.10
          Astro_sSch[0].cStopMin   = 59;							// V6.1.10
          Astro_sSch[0].bSchFlag   = 1;							// V6.1.10

          Astro_sSch[1].cStartHour = 00;							// V6.1.10
          Astro_sSch[1].cStartMin  = 00;							// V6.1.10
          Astro_sSch[1].cStopHour  = sunriseTime/3600;			// V6.1.10
          Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;		// V6.1.10									
          Astro_sSch[1].bSchFlag   = 1;							// V6.1.10
          //shutdown_GPS();
          need_to_send_Shutdown_command = 1;
          GPS_Rescan = 0;
          need_to_send_lat_long = 1;
          GPS_Data_throw_status = 2;
          GPS_read_success_counter = 0;
        }
      }
      GPS_run_extra_count = 0;
    }
    else if(Send_GPS_Data_at_valid_Dcu == 1)
    {
      GPS_read_success_counter++;
      if(GPS_read_success_counter >= 120)
      {
        GPS_read_success_counter = 120;
        if((GPS_Data_Valid == 1) && (threeD_Fix_successfully == 1))
        {
          Latitude = (float)Gps_latitude;
          halCommonSetToken(TOKEN_Latitude,&Latitude);
          longitude = (float)Gps_longitude;
          halCommonSetToken(TOKEN_longitude,&longitude);
          GetSCHTimeFrom_LAT_LOG();								//get sunset sunrise schedule			
          Astro_sSch[0].cStartHour = sunsetTime/3600;				// V6.1.10
          Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		// V6.1.10
          Astro_sSch[0].cStopHour  = 23;							// V6.1.10
          Astro_sSch[0].cStopMin   = 59;							// V6.1.10
          Astro_sSch[0].bSchFlag   = 1;							// V6.1.10

          Astro_sSch[1].cStartHour = 00;							// V6.1.10
          Astro_sSch[1].cStartMin  = 00;							// V6.1.10
          Astro_sSch[1].cStopHour  = sunriseTime/3600;			// V6.1.10
          Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;		// V6.1.10									
          Astro_sSch[1].bSchFlag   = 1;							// V6.1.10
          need_to_send_lat_long = 1;
          GPS_Data_throw_status = 3;
          GPS_read_success_counter = 0;
          Send_GPS_Data_at_valid_Dcu = 2;
          halCommonSetToken(TOKEN_Send_GPS_Data_at_valid_Dcu,&Send_GPS_Data_at_valid_Dcu);
          need_Average_value_Gps = 1;
        }
      }
      GPS_run_extra_count = 0;
    }
    else if((need_Average_value_Gps == 1) || (Send_GPS_Data_at_valid_Dcu == 2))
    {
      if(GPS_reading_comming_OK == 1)
      {
        if(Start_averaging_of_GPS == 0)
        {
          Start_averaging_of_GPS = 1;
          averaging_counter = 0;
          averaging_Timer = 0;
          avg_latitude = 0;
          avg_longitude = 0;
        }
        avg_latitude = avg_latitude + Gps_latitude;
        avg_longitude = avg_longitude + Gps_longitude;
        averaging_counter++;
      }
      else
      {

      }

      if(Start_averaging_of_GPS == 1)
      {
        averaging_Timer++;
        if(averaging_Timer >= (GPS_Wake_timer * 60))
        {
          Latitude = (float)avg_latitude/averaging_counter;
          halCommonSetToken(TOKEN_Latitude,&Latitude);
          longitude = (float)avg_longitude/averaging_counter;
          halCommonSetToken(TOKEN_longitude,&longitude);
          Start_averaging_of_GPS = 0;
          need_to_send_lat_long = 1;
          GPS_Data_throw_status = 4;
          averaging_Timer = 0;
          avg_latitude = 0;
          avg_longitude = 0;
          //        shutdown_GPS();
          need_to_send_Shutdown_command = 1;
          if((Send_GPS_Data_at_valid_Dcu == 1) || (Send_GPS_Data_at_valid_Dcu == 2))
          {
            Send_GPS_Data_at_valid_Dcu = 0;
            GPS_power_On_detected = 0;
            halCommonSetToken(TOKEN_Send_GPS_Data_at_valid_Dcu,&Send_GPS_Data_at_valid_Dcu);

          }
          need_Average_value_Gps = 0;
        }
      }
      GPS_run_extra_count = 0;
    }
    else if(Time_set_by_GPS == 0)
    {
      GPS_run_extra_count = 0;
    }
    else
    {
      GPS_run_extra_count++;
      if(GPS_run_extra_count >= 10)
      {
        GPS_run_extra_count = 0;
        need_to_send_Shutdown_command = 1;
      }
    }


    if(GPS_Data_Valid == 1)
    {
      GPS_error_condition.bits.b0 = 1;
    }
    else
    {
      GPS_error_condition.bits.b0 = 0;
    }

    if(threeD_Fix_successfully == 1)
    {
      GPS_error_condition.bits.b1 = 1;
    }
    else
    {
      GPS_error_condition.bits.b1 = 0;
    }

    if(Setalite_Visinity_Proper == 1)
    {
      GPS_error_condition.bits.b2 = 1;
    }
    else
    {
      GPS_error_condition.bits.b2 = 0;
    }

    if(Setalite_reading_OK == 1)
    {
      GPS_error_condition.bits.b3 = 1;
    }
    else
    {
      GPS_error_condition.bits.b3 = 0;
    }

    if(HDOP_OK == 1)
    {
      GPS_error_condition.bits.b4 = 1;
    }
    else
    {
      GPS_error_condition.bits.b4 = 0;
    }

    if(SBAS_Active == 1)
    {
      GPS_error_condition.bits.b5 = 1;
    }
    else
    {
      GPS_error_condition.bits.b5 = 0;
    }

    if(Sbas_reading_OK == 1)
    {
      GPS_error_condition.bits.b6 = 1;
    }
    else
    {
      GPS_error_condition.bits.b6 = 0;
    }

    //  if(Packate_send_success == 1)
    //  {
    //    GPS_error_condition.bits.b7 = 1;
    //  }
    //  else
    //  {
    //    GPS_error_condition.bits.b7 = 0;
    //  }




    //  if(GPS_Data_Valid == 3)
    //  {
    //    GPS_error_condition.Val = 0x80;
    //  }

    threeD_Fix_successfully = 0;
    Setalite_Visinity_Proper = 0;
    Setalite_reading_OK = 0;
    HDOP_OK = 0;
    SBAS_Active = 0;
    Sbas_reading_OK = 0;
    gagan_Setalite_Id = 0;
    gagan_Setalite_RSSI = 0;
  }
}


/*************************************************************************
Function Name: Increment_Local_Timer
input: None.
Output: None.
Discription: this function is use increment current date and if due to this
             change in month and year then do it.
*************************************************************************/
void Set_GPS_Time_to_RTC(void)
{
  unsigned char occ_day = 0,occ_weekday = 0,i = 0,find_date = 0,N = 0;
  unsigned char temp_week_day = 0,temp_occ_day = 0;
  signed short int temp_hour = 0,temp_min = 0;	
  float temp_utcoffset = 0.0;
  signed char signOfTimeZone = 0;
  unsigned char Display[100];
//  unsigned char Display[100];

  ////////////////////
//  GPS_Time.Hour = Time.Hour;																										
//  GPS_Time.Min = Time.Min;
//  GPS_Time.Sec = Time.Sec;
//  GPS_Date.Date = Date.Date;
//  GPS_Date.Month = Date.Month;
//  GPS_Date.Year = Date.Year;
//  temp_utcoffset = -12.00 * 100;
  ////////////////////

  temp_utcoffset = utcOffset * 100;        // convert UTC to hour and min.
  temp_hour = (short int)temp_utcoffset / 100;
  temp_min = (short int)temp_utcoffset % 100;

  if(utcOffset >= 0)
  {
    signOfTimeZone = 1;  // time zone positive
  }
  else
  {
    signOfTimeZone = 2;  // time zone negative
    temp_hour = temp_hour * -1;
    temp_min = temp_min * -1;
  }


  if(signOfTimeZone == 1)
  {
    GPS_Time.Min = GPS_Time.Min + temp_min;
    GPS_Time.Hour = GPS_Time.Hour + temp_hour;
    if(GPS_Time.Min > 59)
    {
      GPS_Time.Min = GPS_Time.Min - 60;
      GPS_Time.Hour = GPS_Time.Hour + 1;
    }
    if(GPS_Time.Hour > 23)
    {
      GPS_Time.Hour = GPS_Time.Hour - 24;
      GPS_Date.Date = GPS_Date.Date + 1;
      if((GPS_Date.Month==1) || (GPS_Date.Month==3) || (GPS_Date.Month==5) || (GPS_Date.Month==7) || (GPS_Date.Month==8) || (GPS_Date.Month==10) || (GPS_Date.Month==12) )
      {
        N=31;
      }
      else if((GPS_Date.Month==4) || (GPS_Date.Month==6) || (GPS_Date.Month==9) || (GPS_Date.Month==11))
      {
        N=30;
      }		

      if(GPS_Date.Month==2)     // check for february
      {
        if(GPS_Date.Year%4==0)  // check for leap year
          N=29;
        else
          N=28;
      }

      if(GPS_Date.Date > N)  // if RTC date is last date of given month then make date = 1 and increase month.
      {
        GPS_Date.Date = GPS_Date.Date - N;
        GPS_Date.Month = GPS_Date.Month + 1;
        if(GPS_Date.Month > 12) // if month is last month then increase year and make month = 1.
        {
          GPS_Date.Month = GPS_Date.Month - 12;
          GPS_Date.Year = GPS_Date.Year + 1;
        }
      }
    }
  }
  else if(signOfTimeZone == 2)
  {
    GPS_Time.Min = GPS_Time.Min - temp_min;
    GPS_Time.Hour = GPS_Time.Hour - temp_hour;
    if(GPS_Time.Min < 0)
    {
      GPS_Time.Min = 60 - (GPS_Time.Min * -1);
      GPS_Time.Hour = GPS_Time.Hour - 1;
    }

    if(GPS_Time.Hour < 0)
    {
      GPS_Time.Hour = 24 - (GPS_Time.Hour * -1);
      GPS_Date.Date = GPS_Date.Date - 1;

      if(GPS_Date.Date <= 0)  // if RTC date is last date of given month then make date = 1 and increase month.
      {
        GPS_Date.Month = GPS_Date.Month - 1;
        if(GPS_Date.Month <= 0) // if month is last month then increase year and make month = 1.
        {
          GPS_Date.Month = 12;
          GPS_Date.Year = GPS_Date.Year - 1;

        }
        if((GPS_Date.Month==1) || (GPS_Date.Month==3) || (GPS_Date.Month==5) || (GPS_Date.Month==7) || (GPS_Date.Month==8) || (GPS_Date.Month==10) || (GPS_Date.Month==12) )
        {
          N=31;
        }
        else if((GPS_Date.Month==4) || (GPS_Date.Month==6) || (GPS_Date.Month==9) || (GPS_Date.Month==11))
        {
          N=30;
        }		

        if(GPS_Date.Month==2)     // check for february
        {
          if(GPS_Date.Year%4==0)  // check for leap year
            N=29;
          else
            N=28;
        }
        GPS_Date.Date = N;
      }
    }
  }

  Time.Hour = GPS_Time.Hour;																										
  Time.Min = GPS_Time.Min;
  Time.Sec = GPS_Time.Sec;
  Date.Date = GPS_Date.Date;
  Date.Month = GPS_Date.Month;
  Date.Year = GPS_Date.Year;

  Time_change_dueto_DST = 0;
  check_Day_Light_Saving();

  RTC_SET_TIME(Time);
  RTC_SET_DATE(Date);
  Local_Time.Hour = Time.Hour;																										
  Local_Time.Min = Time.Min;
  Local_Time.Sec = Time.Sec;
  Local_Date.Date = Date.Date;
  Local_Date.Month = Date.Month;
  Local_Date.Year = Date.Year;


#if (defined(ISLC_3100_V5_V6) || defined(ISLC_10) || defined(ISLC_T8))
  sprintf(Display,"\r\nSLC ID = %u,GTime = %02u/%02u/%02u %02u:%02u:%02u",amr_id,GPS_Date.Date,GPS_Date.Month,GPS_Date.Year,GPS_Time.Hour,GPS_Time.Min,GPS_Time.Sec);
  emberAfGuaranteedPrint("%p", Display);
  emberSerialWaitSend(APP_SERIAL);
#endif
}

void find_time_from_GPS(unsigned char *res_buff,unsigned char temp_rx_serial)
{
  unsigned int i=0,No_of_comma=0;

  for(i=0;i<temp_rx_serial;i++)
  {
    if(res_buff[i] == ',')
    {
      No_of_comma++;
      if(No_of_comma == 1)
      {
        if((res_buff[i+1])==',') //next byte is ',' then string is not ok and not update the time
        {
          Time_Date_Not_Ok =1;
          return;
        }
        GPS_Time.Hour = ((res_buff[i+1] - 0x30) * 10) + (res_buff[i+2] - 0x30);
        GPS_Time.Min = ((res_buff[i+3] - 0x30) * 10) + (res_buff[i+4] - 0x30);
        GPS_Time.Sec = ((res_buff[i+5] - 0x30) * 10) + (res_buff[i+6] - 0x30);
      }
      else if (No_of_comma == 2)
      {
        if((res_buff[i+1])!='A') //check if string is valid or not
        {
          Time_Date_Not_Ok =1;
          return;
        }
      }
      else if(No_of_comma == 9)
      {
        if((res_buff[i+1])==',') //next byte is ',' then string is not ok and not update the time
        {
          Time_Date_Not_Ok =1;
          return;
        }
        GPS_Date.Date = ((res_buff[i+1] - 0x30) * 10) + (res_buff[i+2] - 0x30);
        GPS_Date.Month = ((res_buff[i+3] - 0x30) * 10) + (res_buff[i+4] - 0x30);
        GPS_Date.Year = ((res_buff[i+5] - 0x30) * 10) + (res_buff[i+6] - 0x30);
      }
    }
  }
}


float stof(const char* s){
  float rez = 0, fact = 1;
  if (*s == '-'){
    s++;
    fact = -1;
  };
  for (int point_seen = 0; *s; s++){
    if (*s == '.'){
      point_seen = 1; 
      continue;
    };
    int d = *s - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      rez = rez * 10.0f + (float)d;
    };
  };
  return rez * fact;
};