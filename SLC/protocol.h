/****************************************protocol.h**************************/
/************************ HEADERS **********************************/

#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

#include "generic.h"

/************************defines for AMR *****************/
#define CMD			    'C'
#define DATA		    'D'
#define SEND_DIAGNOSIS  'P'
#define SEND_FAULT_PARA	'R'
#define AMSTATUS	    'Y'
#define GET_NAK_PARA    'M'
#define GET_SCH_PARA    'Q'
#define GET_DIMM_SCH_PARA    'W'							// V6.1.10 // Master sch 15/04/11
#define  GET_VERSION_PARA     'V'							// V6.1.10
#define DIM_SCHEDULE 		 'Z'							// SAN
#define GET_ADAPTIVE_DIMMING 'F'
#define GET_MOTION_DIMMING	 'B'
#define	GET_SLC_MIX_MODE_SCH_RESPONCE 'G'
#define GET_SLC_TEST_1  0x64
#define GET_SLC_TEST_2  0x65

#define RTC_TEST_DONE        0x66
#define EMT_TEST_DONE        0x67
#define LAMP_TEST_DONE       0x68
#define PHOTOCELL_TEST_DONE  0x69
#define CT_TEST_DONE  		     0x6A
#define DIMMING_TEST_DONE    0x6B
#define GPS_STATISTIC        0x6C
#define SAVE_CALIBRATION_DATA  0x6D

#define GATE_CALIBRATION_DATA  0x6E

//query type
#define DATA_QUERY	     		 0x01
#define SET_RTC		     		   0x02
#define GET_RTC		     		   0x03
#define ERS_EEP          		0x09
#define LL_AMR           		0x06
#define SYNC_RTC         		0x07
#define CLOUDY_FLAG      		0x08
#define ERASE_TRIP_SLC   		0x11
#define EVENT_START      		0x12
#define EVENT_QUERY      		0x13
#define GET_MODE		 		0x14
#define SET_FAULT_PARA   		0x15
#define GET_FAULT_PARA   		0x16
#define SLC_DIAGNOSIS    		0x17
#define SET_NETWORK_PARA 		0x18
#define GET_NETWORK_PARA 		0x19
#define GET_SCH			 		0x20
#define GET_PHOTOCELL_CONFIG      0x2A	
#define SET_NETWORK_SEARCH_PARA   0x2B
#define GET_NETWORK_SEARCH_PARA   0x2C
#define COMMISSIONING_PARA        0x2D
#define GPS_CONFIG                0x2E
#define GET_GPS_CONFIG            0x2F
#define SLC_TEST_QUERY_1 		       0x21
#define SLC_TEST_QUERY_1_ACK 	    0x22
#define SLC_TEST_QUERY_2 		       0x23
#define SLC_TEST_QUERY_2_ACK 	    0x24
//#define MASTER_SCH				          0x25			// V6.1.10	// Master sch 15/04/11
#define PHOTOCELL_CONFIG          0x25
#define GET_DIMMING_SCH			        0x26			// V6.1.10	// Master sch 15/04/11
#define GET_VERSION_INFO          0x27			// V6.1.10
#define ADAPTIVE_DIMMING_PARA     0x30	
#define GET_ADAPTIVE_DIMMING_PARA 0x31
#define MOTION_DETECT             0x32
/////////////

#define SET_RF_NETWORK_PARA       0x33
#define CHANGE_RF_NETWORK_PARA    0x34

#define SLC_CONFIG_PARA           0x35
#define GET_SLC_CONFIG_PARA       0x36
#define SLC_DST_CONFIG_PARA		  0x37
#define GET_SLC_DST_CONFIG_PARA	  0x38
#define SLC_MIX_MODE_SCH          0x39
#define GET_SLC_MIX_MODE_SCH      0x3A
#define SLC_MMOD                  0x3B
#define CHANGE_KEY                0x3C
#define SET_LINK_KEY_SLC          0x3D

#define GET_LINK_KEY_SLC          0x3E
#define TEST_DIMMING              0x3F
///////////TOF///////////////////////////////////
#define SET_TIME_OF_USE                         0x40
#define GET_TIME_OF_USE                         0x41
#define GET_KWH_SLOT                            0x42

#define SET_FOOT_CANDLE                         0xC8
#define GET_FOOT_CANDLE                         0xC9
#define ADAPTIVE_DIMMING_PARA_32GROUP           0xCA
#define GET_ADAPTIVE_DIMMING_PARA_32            0xCB
#define GET_ADAPTIVE_DIMMING_32                 0xCC
#define MOTION_DETECT_32                        0xCD
#define PANIC_CMD_START                         0xD0
#define PANIC_CMD_STOP                          0xD1
////////////////
#define CURV_TYPE_SET                           0xD2
#define CURV_TYPE_GET                           0xD3
#define RESET_CMD                               0xD4
#define MIX_SCHEDULE_RESET_CMD                  0xD5
#define MOTION_SCHEDULE_RESET_CMD               0xD6

#define EM_RESET_CMD               0xD7
#define ID_FRAME_STOP               0xD8
#define CONF_CMD_IN_TEST_MODE               0xD9
#define GET_CONF_CMD_IN_TEST_MODE           0xDA
#define BROADCAST_RESPONSE                  0xDB
#define AUTO_ADDRESS_CMD        0xDC
#define READ_SCENE_FROM_DALI 0xDD
#define WRITE_SCENE_TO_DALI 0xDE
#define AUTO_SEND_READ_DATA 0xDF
////////////////
//0xE1  might be occupied in DCU
///////////////
#define GET_AUTO_SEND_READ_DATA             0xE2
#define AUTO_NETWORK_LEAVE  0xE3
#define GET_AUTO_NETWORK_LEAVE  0xE4
#define OTA_CONFIG              0xE5
#define GET_OTA_DATA            0xE6
#define Rs485_DI2_CONFIG        0xE7
#define GET_Rs485_DI2_CONFIG    0xE8
#define DALI_AO_CONFIG          0xE9
#define GET_DALI_AO_CONFIG      0xEA
#define LastGasp_CONFIG	        0xEB
#define GET_LastGasp_CONFIG	0xEC
#define LASTGASP        	0xED
#define FAULT_REMOVE_CONFIG     0xEE
#define GET_FAULT_REMOVE_CONFIG 0xEF
#define DATA_QUERY_WITH_LAST_HOUR_ENERGY 0xF0
#define DATA_WITH_LAST_HOUR_ENERGY 0xF1
#define PANIC_MODE          9
//#define EE_AMR_ID   	0
//#define EE_LOGICMODE	2
////#define EE_SCHEDULE     4
//#define EE_DO			80
//#define EE_SCH_DAY      4			// V6.1.10
//#define EE_MASTER_SCH_DAY 24
//
//#define EE_VRDOUB	100				//energy meter section
//#define EE_IRDOUB   (EE_VRDOUB + 4)
//#define EE_PULSES_R (EE_IRDOUB + 4)
//#define EE_KWH		(EE_PULSES_R + 4)
//
//#define EE_LATITUDE (EE_KWH + 4)
//#define EE_LONGITUDE (EE_LATITUDE + 4)
//#define EE_TIMEZONE (EE_LONGITUDE + 4)
//#define EE_BURN_HOUR (EE_TIMEZONE +4)
//#define EE_LAMP_STADY_CURRENT (EE_BURN_HOUR + 4)
//#define EE_LAMP_STADY_CURRENT_SET (EE_LAMP_STADY_CURRENT + 4)
//
//
//#define EE_Vol_hi					  (EE_LAMP_STADY_CURRENT_SET + 4)
//#define EE_Vol_low					  (EE_Vol_hi + 2)
//#define EE_Curr_Steady_Time			  (EE_Vol_low + 2)
//#define EE_Per_Val_Current			  (EE_Curr_Steady_Time + 2)
//#define EE_Lamp_Fault_Time			  (EE_Per_Val_Current + 2)
//#define EE_Lamp_off_on_Time			  (EE_Lamp_Fault_Time + 2)
//#define EE_Lamp_faulty_retrieve_Count (EE_Lamp_off_on_Time + 2)
//#define EE_Sunset_delay				  (EE_Lamp_faulty_retrieve_Count + 2)
//#define EE_Sunrise_delay              (EE_Sunset_delay + 2)
//#define EE_Broadcast_time_delay		  (EE_Sunrise_delay + 2)
//#define EE_Unicast_time_delay		  (EE_Broadcast_time_delay + 2)
//#define EE_Lamp_lock_condition	      (EE_Unicast_time_delay + 2)
//
//#define EE_analog_input_scaling_high_Value (EE_Lamp_lock_condition + 2)
//#define EE_analog_input_scaling_low_Value (EE_analog_input_scaling_high_Value + 2)
//#define EE_desir_lamp_lumen (EE_analog_input_scaling_low_Value + 2)
//#define EE_lumen_tollarence (EE_desir_lamp_lumen + 2)
//#define EE_ballast_type (EE_lumen_tollarence + 1)
//#define EE_adaptive_light_dimming (EE_ballast_type + 1)
//
//#define EE_Motion_pulse_rate (EE_adaptive_light_dimming + 1)
//#define EE_Motion_dimming_percentage (EE_Motion_pulse_rate + 1)
//#define EE_Motion_dimming_time (EE_Motion_dimming_percentage + 1)
//#define EE_Motion_group_id (EE_Motion_dimming_time + 2)
//#define EE_dim_applay_time (EE_Motion_group_id + 1)
//#define EE_dim_inc_val (EE_dim_applay_time + 2)
//#define EE_Motion_normal_dimming_percentage (EE_dim_inc_val + 1)
//////////////////////////
//#define EE_Day_light_harvesting_start_offset (EE_Motion_normal_dimming_percentage + 1)  // 181
//#define EE_Day_light_harvesting_stop_offset (EE_Day_light_harvesting_start_offset + 2)  // 183
//
//#define EE_IDIMMER_EN (EE_Day_light_harvesting_stop_offset + 2)           // 185
//#define EE_IDIMMER_ID (EE_IDIMMER_EN + 1)								  // 186
//#define EE_IDIMMER_MAC (EE_IDIMMER_ID + 2)                                // 188
//
//#define EE_Valid_DCU (EE_IDIMMER_MAC + 8)								  // 196
//#define EE_Lamp_type (EE_Valid_DCU + 1)									  // 197
//#define EE_SLC_DST_En (EE_Lamp_type + 1)								  // 198
//#define EE_SLC_DST_Start_Rule (EE_SLC_DST_En + 1)						// 199
//#define EE_SLC_DST_Start_Month (EE_SLC_DST_Start_Rule + 1)						// 199
//#define EE_SLC_DST_Start_Time (EE_SLC_DST_Start_Month + 1)				// 200
//#define EE_SLC_DST_Stop_Rule (EE_SLC_DST_Start_Time + 1)				// 201
//#define EE_SLC_DST_Stop_Month (EE_SLC_DST_Stop_Rule + 1)					// 202
//#define EE_SLC_DST_Stop_Time (EE_SLC_DST_Stop_Month + 1)					// 202
//#define EE_SLC_DST_Time_Zone_Diff (EE_SLC_DST_Stop_Time + 1)			// 203
//#define EE_Time_change_dueto_DST (EE_SLC_DST_Time_Zone_Diff + 4)        // 207
//#define EE_SLC_DST_Rule_Enable (EE_Time_change_dueto_DST + 1)           // 208
//#define EE_SLC_DST_Start_Date (EE_SLC_DST_Rule_Enable + 1)						// 199
//#define EE_SLC_DST_Stop_Date (EE_SLC_DST_Start_Date + 1)				// 201
//#define EE_Auto_event (EE_SLC_DST_Stop_Date + 1)                        // 202
//#define EE_SLC_New_Manula_Mode_Val (EE_Auto_event + 1)                  // 203
//#define EE_Link_Key (EE_SLC_New_Manula_Mode_Val + 1)                 // 204
//#define EE_SLC_New_Manual_Mode_Timer (EE_Link_Key + 20)              // 224
//#define EE_Photocell_steady_timeout_Val (EE_SLC_New_Manual_Mode_Timer + 2) // 226
//#define EE_photo_cell_toggel_counter_Val (EE_Photocell_steady_timeout_Val + 1)   //227
//#define EE_photo_cell_toggel_timer_Val   (EE_photo_cell_toggel_counter_Val + 1)  //228
//#define EE_Photo_cell_Frequency           (EE_photo_cell_toggel_timer_Val + 1)   //229
//#define EE_Photocell_unsteady_timeout_Val (EE_Photo_cell_Frequency + 1)           //230
//#define EE_Schedule_offset                (EE_Photocell_unsteady_timeout_Val + 2) //232
//#define EE_Motion_Detect_Timeout          (EE_Schedule_offset + 1)                //233
//#define EE_Motion_Broadcast_Timeout       (EE_Motion_Detect_Timeout + 1)          //234
//#define EE_Motion_Sensor_Type             (EE_Motion_Broadcast_Timeout + 1)       //235
//
///////////////////////////
//
//#define ASTRO_EE_SCHEDULE	  300									// V6.1.10
//#define EE_SCHEDULE           320									// V6.1.10
//#define EE_MASTER_SCHEDULE    EE_SCHEDULE + 50//350						// V6.1.10 (7(days) * 6(no of loc) * 10(no of sch per day) = 350)
//#define EE_MIX_SCHEDULE       EE_MASTER_SCHEDULE + 60//450				// V6.1.10
//#define EE_FRESH_LOCATION_1	  EE_MIX_SCHEDULE + 1000                 //
//#define EE_FRESH_LOCATION_2   EE_FRESH_LOCATION_1 + 1
//#define EE_LAST_LOC 1432

//#define LED_2	0//LATBbits.LATB12
//#define LED_1	0//LATBbits.LATB13

//extern unsigned char Serial_RF_Buff[];

#define RECEIVE_BROADCAST_OTA                           0
#define WAIT_FOR_COMMAND_TO_RETRY_MISSED_OFFSET         1
#define REQUEST_FOR_MISSED_OFFSET                       2
#define READY_FOR_VERIFICATION                          3
#define WAIT_FOR_COMMAND_TO_UPDATE                      4
extern unsigned short int Blue_Light_Id;
extern float Dali_At_100Dim,Dali_At_75Dim,Dali_At_50Dim,Dali_At_25Dim,Dali_At_0Dim;
extern unsigned char DimmerDriverSelectionProcess,DimmerDriver_SELECTION;
extern volatile unsigned char CheckAoDimmerForDetectionTimmer,CheckAoDimmerForDetection;
extern float full_brightPower,full_brightPower_at_30;
extern unsigned char dali_support;
extern unsigned char OTA_Master;
extern unsigned long Network_Scan_Cnt;
extern unsigned int  Network_Leave_Hr_Value;
extern unsigned char Network_leave_Flag;
extern unsigned short int Network_Leave_Hr_Cnt ;
extern unsigned char Lamp_ON_OFF ,Lamp_Fail,Ballast_Fail,Day_Burner;
extern volatile unsigned int Event_Send_Cnt;
extern unsigned char Event_Detected;
extern BYTE_VAL Configured_Event,Configured_Event1;
extern unsigned int Time_Slice,Temp_Time_Slice;
extern unsigned long int ack_Rf_Track_Id,Auto_ack_Rf_Track_Id;
extern unsigned char Send_Data_Success,Send_To_DCU ;
extern unsigned char Auto_Send_Lograte_Interval,Auto_Send_Enable_Disable,Auto_Send_Retry,Read_Data_send_Timer;
extern unsigned char Cali_buff[50];//80
extern unsigned char Send_Read_Scene_From_Dali;
extern unsigned char Valid_DCU_Flag;
extern unsigned char Panic_Dim_Value;
extern unsigned char Lamp_ON_First_Responder;
extern volatile unsigned char Lamp_ON_Flag,Lamp_OFF_Flag;
extern unsigned char Soft_Reboot;
extern unsigned char Cmd_Rcv_To_Change_Curv_Ype;
extern float ADC_Result_100,ADC_Result_50,ADC_Result_0;
extern unsigned char data_rec_RF;
#if (defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3))
 extern unsigned char Photo_feedback_pin;
#endif
////////Tilt////////
//extern unsigned char Tilt_Occured;
extern unsigned char  CURV_TYPE;
extern unsigned int Tilt_Cnt;
extern unsigned char Tilt_Get_Previously;
extern float Photosensor_Set_FC,Photosensor_Set_FC_Lamp_OFF,Photosensor_FootCandle;
 /////////////////
extern float PA4_Analog_Value;
//////////////Calibration////////////
extern float Calculated_Frequency;
extern unsigned char Read_KWH_Value;
extern float Vr_Cal,Ir_Cal,KW_Cal,KW_Cal_120;
extern unsigned int SSN_CNT ;
extern unsigned char Read_Em_Calibration;
extern unsigned char Send_Kw_calibration,Send_Kw_calibration_120;
extern unsigned char KW_Calibration,KW_Calibration_120;
extern unsigned char Current_calibration;
extern unsigned char Send_Current_calibration;
extern unsigned char Send_Voltage_calibration;
extern unsigned char Voltage_calibration;
extern unsigned char calibmode;
extern unsigned char Cali_Cnt;

/////////////////////////////////////

extern unsigned char Rtc_set_Replay,Get_rtc_Replay,Send_Data,Send_Data_autosend;
extern unsigned int Frm_no;
extern unsigned int amr_id;
extern unsigned char temp_received[];
//extern unsigned char temp_arr[];
extern ALLTYPES pulseCounter;
extern unsigned char Eep_Ers;
extern unsigned char connection_ok;
extern unsigned char send_id_frame;
extern unsigned char Id_frame_received;
extern unsigned long lamp_burn_hour;
extern unsigned char change_Link_key_timer ;
extern unsigned char change_Link_key;

extern unsigned int Broadcast_time_delay;
extern unsigned int Unicast_time_delay;

extern unsigned char temp_data_buff[20][40];
extern unsigned char send_rf_event;
extern unsigned char no_of_pending_event;
extern unsigned char broadcast_read_req;
extern unsigned char stop_event;
extern unsigned int rf_event_send_counter;
extern unsigned char read_event_flag;
extern unsigned char send_all_rf_event;
extern unsigned char send_get_mode_replay;
extern unsigned char send_get_mode_replay_auto;
extern unsigned char Send_Fault_Para_Replay;
extern unsigned char Send_Slc_Diagnosis;
extern unsigned char cheak_emt_timer;
extern unsigned char Diagnosis_DI;
extern unsigned int RF_communication_check_Counter;
extern unsigned char check_peripheral;
extern short int  Sunset_delay;
extern short int  Sunrise_delay;
extern short int  Local_Sunset_delay;
extern short int  Local_Sunrise_delay;


extern unsigned char Send_Network_Para_Replay;
extern unsigned char Send_Get_Sch_Replay;
extern unsigned char delet_no;
extern unsigned char Slc_test_query_1;
extern unsigned char test_query_1_send_timer;
extern unsigned char send_SLC_test_query_1;
extern unsigned char send_SLC_test_query_2;
extern float test_voltage;
extern float test_current;
extern unsigned short int test_emt_int_count;
extern unsigned short int test_Kwh_count;
extern unsigned char photocell_test_dicision;
extern unsigned char SLC_Test_Query_2_received;

extern unsigned char Send_Get_Master_Sch_Replay;			// V6.1.10			// Master sch 15/04/11
extern unsigned char Sch_week_day; 							// V6.1.10
extern unsigned char Get_Sch_timer;							// V6.1.10
extern unsigned char Get_Master_Sch_timer;					// V6.1.10
extern unsigned char Send_Get_Version_info_Replay;			// V6.1.10
extern unsigned char get_sch_day;							// V6.1.10
//extern unsigned char sch_day_send[];						// V6.1.10
extern unsigned char sch_day[];								// V6.1.10
extern unsigned char sch_Master_day[];						// V6.1.10
//extern unsigned char sch_Master_day_send[];					// V6.1.10

extern unsigned char Protec_lock;
extern unsigned char send_data_direct;
extern unsigned char Send_Data_WithLastHourEnergy;
extern float presentHourKwhBuffer[3],previousHourKwhBuffer[4];
extern  unsigned char Rtc_Power_on_result;
//////////////
extern unsigned int SLC_New_Manual_Mode_Timer;
extern unsigned long int SLC_New_Manual_Mode_counter;
extern unsigned long int Motion_broadcast_Receive_counter,Temp_Motion_broadcast_Receive_counter;

extern short int Day_light_harvesting_start_offset;
extern short int Day_light_harvesting_stop_offset;

extern long Day_light_harvesting_start_time;
extern long Day_light_harvesting_stop_time;

extern unsigned char idimmer_en;
extern unsigned int idimmer_id;
extern unsigned char idimmer_longaddress[];
extern unsigned char send_idimmer_id_frame;
//extern unsigned int Send_dimming_cmd;
extern volatile unsigned char Read_Data_send_cnt;
extern unsigned char idimmer_Id_frame_received;
extern unsigned char send_idimmer_Val_frame;
extern unsigned char dimming_cmd_ack;
extern unsigned char idimmer_trip_erase;
extern unsigned char idimmer_dimval;
extern unsigned char current_dimval;
extern unsigned char dimming_cmd_cnt;
extern unsigned char read_dim_val_timer;
extern unsigned char read_dim_val_idimmer;


extern unsigned char Read_db_val;


extern unsigned char SLC_DST_En;		
extern unsigned char SLC_DST_Start_Rule;
extern unsigned char SLC_DST_Start_Month;
extern unsigned char SLC_DST_Start_Time;
extern unsigned char SLC_DST_Stop_Rule;
extern unsigned char SLC_DST_Stop_Month;
extern unsigned char SLC_DST_Stop_Time;
extern float SLC_DST_Time_Zone_Diff;
extern unsigned char SLC_DST_Rule_Enable;
extern unsigned char SLC_DST_Start_Date;
extern unsigned char SLC_DST_Stop_Date;

extern unsigned char Time_change_dueto_DST;
extern unsigned char SLC_DST_R_Start_Date;
extern unsigned char SLC_DST_R_Stop_Date;

extern unsigned int old_year,Local_old_year;

extern unsigned char SLC_Mix_Sch_Loc_No;
extern unsigned char Send_Get_Mix_Mode_Sch;
extern unsigned char Get_Mix_mode_Sch_Loc;
extern unsigned char L_No;
extern unsigned char Mix_photo_override;
extern unsigned char Auto_event;
extern unsigned char send_data_direct_counter;
extern unsigned char send_event_with_delay;
extern unsigned char send_event_delay_timer;
extern unsigned int Photocell_Detect_Time;
extern unsigned char Temp_Slc_Multi_group;
extern BYTE_VAL Group_val1;
extern DWORD_VAL Group_val1_32;
extern unsigned char check_reset_need_condition,check_reset_need_condition_timer;
extern unsigned int previous_rx_counter,previous_tx_counter;
extern unsigned char Tx_Rx_increase_counter;
extern unsigned int Tx_Rx_not_increase_counter;
extern unsigned char SLC_reset_info,SLC_extreset_info;
extern unsigned int Slc_reset_counter;
extern unsigned char Dimming_test_dicision;
//extern float Dimming_val[3];

extern unsigned char Test_dimming_val;
extern unsigned char Start_dimming_test;

extern unsigned char Commissioning_flag;
extern unsigned char new_dcu_detected;
extern unsigned char need_to_send_lat_long;
extern unsigned int RTC_power_on_Logic_Timer;
extern unsigned char check_RTC_logic_cycle;
extern unsigned char Lat_long_need_to_varify;
extern unsigned char RTC_need_to_varify;
extern unsigned int Panic_Lamp_On_Cnt,Panic_Lamp_OFF_Cnt;
extern unsigned short int Panic_Cnt;
extern unsigned short int SLC_Find_For_Panic;
extern unsigned int Lamp_On_Time,Lamp_OFF_Time;
extern unsigned short int Panic_Time_Out;
//extern unsigned char Send_RTC_Logic_Analysis_Data;
extern unsigned char Last_Gsp_Enable;
extern volatile unsigned int LastGasp_Timer;
extern unsigned char Send_LastGasp,LastGasp_unicast_status;
extern unsigned char Lamp_status_On_LastGasp,Last_Gasp_Detect,LAmp_previous_status_On_Aftr_LastGasp,LG_cnt;
///////////////
void send_data_frame(unsigned int received_short_id,unsigned char *msg,unsigned char length,unsigned char only_DCU);

void send_fream(void);
void checkfream(unsigned char *temp);
void send_data_zeegbee(void);
void Send_data_RF(unsigned char *data,unsigned char len,unsigned char only_DCU);
void store_data_in_mem(void);
void get_data_from_mem_and_send(void);
void delet_data_from_mem(unsigned long int real_rf_track_id);
void delet_all_data(void);
void send_all_event(void);
void Write_NI_Para(unsigned int id);
void Check_SLC_Test_query_1(void);
void Write_ZS_Para(void);
////////
extern unsigned char DALI_Ballast_Info[4][16];
void Read_Dali_Scene(void);
void Multiple_Dali_Driver_Handle(void);
void Send_data_RF_idimmer(unsigned char *data,unsigned char len);
void DB_info_SLC(void);
void Configure_Slc_Mix_Mode_Sch(unsigned char *temp_str);
void unCompress_Mix_sch(unsigned char loc_no);
void Ember_Stack_run_Fun(void);
unsigned char check_group(unsigned char temp_G1);
unsigned char check_multi_group(unsigned char temp_G1);
extern unsigned int Motion_group_id_32;
extern unsigned char Motion_grp_Select_32;
void DCU_detect_action(void);
unsigned char check_multi_group_32(unsigned int temp_G1);
unsigned char check_32group(unsigned int temp_G1);

extern unsigned int Lamp_Balast_fault_Remove_Time;
extern unsigned int Lamp_Balast_fault_Remove_Cnt; 
extern unsigned char No_check_Lamp_After_This_Event;
extern unsigned char Ballast_Fault_Remove_Retry_Cnt_3;
extern unsigned char Lamp_Fault_Remove_Retry_Cnt_3;
extern unsigned char Lamp__Ballast_Fault_Occured;
#endif

////////////////////////1.0.19
//typedef struct {
//	unsigned char Slot_Start_Date[2];
//        unsigned char Slot_Start_Month[2];
//        unsigned char Slot_Start_Year[2];
//
//        unsigned char Slot_End_Date[2];
//        unsigned char Slot_End_Month[2];
//        unsigned char Slot_End_Year[2];
//
//	unsigned char Slot_Start_Hour[12];
//	unsigned char Slot_Start_Min[12];
//	unsigned char Slot_End_Hour[12];
//	unsigned char Slot_End_Min[12];
//
//	unsigned char Save_Energy_In_Slot[12];
//	unsigned char Save_data_slot[12];
//
//	
//	float kwh_value[12];	
//	unsigned long int Slot_Pulses[12];
//}TimeOfUse;
//extern TimeOfUse Time_Slot;

//////////////////////////////
