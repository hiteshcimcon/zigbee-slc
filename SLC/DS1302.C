
#include "DS1302.h"


unsigned char DSsec,DSmin,DShr,DSdy,DSdt,DSmn,DSyr;
unsigned int count1;

RTCTime Time;
RTCDate	Date;

void DelayRTC(long K)
{
long i;
for(i=0;i<K;i++)
 {;}
}
void SetRTC_SCLK()
{
//	 IOSET0 |= 1<<RTCSCLK;
	 DelayRTC(1000);	 
}
void SetRTC_IO()
{
//	IODIR0 |= 1<<RTCIO;			//SDA0 as Write only direction
//	IOSET0 |= 1<<RTCIO;
	DelayRTC(1000);
}
void RTC_ENB()
{
//	 IOSET0 |= 1<<RTCEN;
	 DelayRTC(1000);	 
}
void ClrRTC_SCLK()
{
//	IOCLR0 |= 1<<RTCSCLK;
	DelayRTC(1000);	
}
void ClrRTC_IO()
{
//	IODIR0 |= 1<<RTCIO;			//SDA0 as Write only direction
//	IOCLR0 |= 1<<RTCIO;
	DelayRTC(1000);
}
void RTC_DIS()
{
//	IOCLR0 |= 1<<RTCEN;
	DelayRTC(1000);	
}
char ReadRTC_IO()
{
	long data;
//	IODIR0 &= ~(1<<RTCIO);
	DelayRTC(1000);
//	data = IOPIN0;
//	if(data & RTCPIN)
	if(1)
		return 1;
	else
		return 0;
}

void disable_RTC ()
{
	RTC_DIS();
 for(count=0;count<=1000;count++);
}

void reset_3w()
{
	ClrRTC_SCLK();
	for(count=0;count<1000;count++);
	RTC_DIS();
	for(count=0;count<1000;count++);
	RTC_ENB();
	for(count=0;count<1000;count++);
}
/* --- This routine writes one byte from the RTC --- */

void WBYTE_3W(unsigned char W_Byte)	
{
unsigned char i;

	for(i = 0; i < 8; ++i)
	{
		ClrRTC_IO();
		if(W_Byte & 0x01)
		{
			SetRTC_IO();
		}
		ClrRTC_SCLK();
		SetRTC_SCLK();
		W_Byte >>= 1;
	}
	for(count1=0;count1<1000;count1++);
}
/* ---- This routine reads one byte from the RTC ----- */

unsigned char RBYTE_3W()
{
	unsigned char i, R_Byte, TmpByte;

	R_Byte = 0x00;
	IODIR0 &= ~(1<<RTCIO);
	for(i = 0; i < 8; ++i)
	{
		SetRTC_SCLK();
		ClrRTC_SCLK();
		TmpByte = (unsigned char)(ReadRTC_IO());
		TmpByte <<= 7;
		R_Byte >>= 1;
		R_Byte |= TmpByte; 
	}
	for(count1=0;count1<1000;count1++);
	return R_Byte;
}

void RTCInit()
{

//	PINSEL0	&= 0xFFFC0FFF;//select EN,IO & SCLK pin as GPIO
//	IODIR0 |= 1<<6;	//SCLK as O/P
//	IODIR0 |= 1<<8;	//ENABLE as O/P
  	reset_3w();
	WBYTE_3W(0x8e);		/* control register */
	WBYTE_3W(0);		/* disable write protect */
	reset_3w();
}
/******************************************************************
** Function name:		RTC_default
**
** Descriptions:	 This fuction is used to initilise the RTC

** parameters:			None
** Returned value:		None
** 
				
*****************************************************************/ 
void RTC_default(RTCTime Time , RTCDate Date)
{

 // RTC_SEC =   Time.Sec ;	   	 		 /* Second value - [0,59] */
 // RTC_MIN =   Time.Min;  			 /* Minute value - [0,59] */
 // RTC_HOUR =  Time.Hour;			     /* Hour value - [0,23] */
 // RTC_DOM =   Date.Date;  		     /*	Day of the month value - [1,31] */
 // RTC_DOW =   Date.Week;  			 /* Day of week value - [0,6] */
 // RTC_DOY =   Date.DofYear;  		 /*	Day of year value - [1,365] */
 // RTC_MONTH = Date.Month;  				 /*	Month value - [1,12] */  
 // RTC_YEAR =  Date.Year;  				 /* Year value - [0,4095] */
}
/******************************************************************************
** Function name:	RTCGetTime
**
** Descriptions:	The function will read the time of RTC		

** parameters:			None
** Returned value:		Base Address of the structure for time 
** 
*****************************************************************************/ 
RTCTime RTCGetTime( void ) 
{
RTCTime LocalTime;
unsigned char Ss,Mn,Hh,Dy,Dt,Mm,Yy,i;

	reset_3w();
	WBYTE_3W(0xBF);

	Ss = RBYTE_3W();
	Mn = RBYTE_3W();
	Hh = RBYTE_3W();		

	Dt = RBYTE_3W();
	Mm = RBYTE_3W();
	Dy = RBYTE_3W();
	Yy = RBYTE_3W();
	reset_3w();	

	Hh = (Hh  & 0x3f);

	i = (Ss / 16);
	LocalTime.Sec = (Ss - (i * 6));


	i = (Mn / 16);
	LocalTime.Min = (Mn - (i * 6));	   

	i = (Hh / 16);
	LocalTime.Hour = (Hh - (i * 6));

	disable_RTC();
  return ( LocalTime );    
}
/******************************************************************************
** Function name:	RTCGetDate
**
** Descriptions:	The function will read the Date of RTC		

** parameters:			None
** Returned value:		Base Address of the structure for Date 
** 
*****************************************************************************/ 

RTCDate RTCGetDate( void ) 
{
RTCDate LocalDate;
unsigned char Ss,Mn,Hh,Dy,Dt,Mm,Yy,i;

	reset_3w();
	WBYTE_3W(0xBF);

	Ss = RBYTE_3W();
	Mn = RBYTE_3W();
	Hh = RBYTE_3W();		

	Dt = RBYTE_3W();
	Mm = RBYTE_3W();
	Dy = RBYTE_3W();
	Yy = RBYTE_3W();
	reset_3w();	

	i = (Dy / 16);
	LocalDate.Week = (Dy - (i * 6));
			
	i = (Dt / 16);
	LocalDate.Date = (Dt - (i * 6));
	
	i = (Mm / 16);
	LocalDate.Month = (Mm - (i * 6));

	i = (Yy / 16);
	LocalDate.Year = (Yy - (i * 6));	
	disable_RTC();

  return ( LocalDate );    
}
/******************************************************************************
** Function name:	RTCSetDate
**
** Descriptions:	The function will set the date of RTC		
**				
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/

void RTCSetDate(RTCDate Date) 			  //it is used to set date 
{  
unsigned char sdDt,sdMn,sdYr,sdDy=Date.Week,i;

	sdDt = Date.Date;
	i = (sdDt / 10);
	sdDt = (sdDt + (i * 6));

	sdMn = Date.Month;
	i = (sdMn / 10);
	sdMn = (sdMn + (i * 6));

	sdYr = Date.Year;
	i = (sdYr / 10);
	sdYr = (sdYr + (i * 6));

	reset_3w();
	 WBYTE_3W(0x86);  //date 
	 WBYTE_3W(sdDt);
	reset_3w();
	 WBYTE_3W(0x88); //month 
	 WBYTE_3W(sdMn);
	reset_3w();
	 WBYTE_3W(0x8A);  //day
	 WBYTE_3W(sdDy);
	reset_3w();
	 WBYTE_3W(0x8C);  //year
	 WBYTE_3W(sdYr);
	reset_3w();
 	disable_RTC();      
}

/******************************************************************************
** Function name:	RTCSetTime
**
** Descriptions:	The function will set the time of RTC		
**				
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
void RTCSetTime(RTCTime RTC_Time )
{
unsigned char sdHr,sdMin,sdSs,i;

	sdHr = RTC_Time.Hour;
	i = (sdHr / 10);
	sdHr = (sdHr + (i * 6));

	sdMin = RTC_Time.Min;
	i = (sdMin / 10);
	sdMin = (sdMin + (i * 6));

	sdSs = RTC_Time.Sec;
	i = (sdSs / 10);
	sdSs = (sdSs + (i * 6));   
	
	reset_3w();
	 WBYTE_3W(0x80);//SECONDS
	 WBYTE_3W(sdSs);
	reset_3w();
	 WBYTE_3W(0x82); //	MIN
	 WBYTE_3W(sdMin);
	reset_3w();
	 WBYTE_3W(0x84);   //HOUR
	 sdHr=sdHr & 0x3f;
	 WBYTE_3W(sdHr);
	reset_3w();
	disable_RTC();
}

