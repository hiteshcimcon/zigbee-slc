#define SCL0 0x10000000
#define SDA0 0x08000000

#define SMS_LEN		160
#define RECORDSIZE	1+SMS_LEN
#define NOOFRECORDS	400				/* No of Records (SMS) in 1 memory bank*/
//#define MAXRECORDS	(4*NOOFRECORDS)	/* No of Records (SMS) in 4 memory banks*/
#define MAXRECORDS	(3*NOOFRECORDS)	/* No of Records (SMS) in 4 memory banks*/
//#define MAXRECORDS	200	/* No of Records (SMS) in 4 memory banks*/

#define NOTSENT		1
#define SENT		2

#define ASCII		1
#define HEX			2

#define READ		1
#define WRITE		2

#define BANK1		0
#define BANK2		1
#define BANK3		2
#define BANK4		3
#define ALLBANK		4	
#define MAXBANK		ALLBANK 	/*maximum bank*/

#define BYDATE			1
#define BYDATETIME		2
#define BYTIME			3
#define BURST			4


struct datetime
{
	unsigned int DD;	/*date*/
	unsigned int MM;	/*month*/
	unsigned int YY;	/*year*/
	unsigned int hh;	/*hour*/
	unsigned int mm;	/*minute*/
	unsigned int ss;	/*second*/
};

void InitEEPROM (void);
void InitI2C (void);
void Start(void);
void Stop(void);

void WriteByte (unsigned char, unsigned int, unsigned char); // void WriteByte(bank no, EEPROM address, byte to write);
void WriteshortInt(unsigned char, unsigned int, unsigned short int); // void WriteByte(EEPROM address, byte to write);
void WriteInt(unsigned char, unsigned int, unsigned int); // void WriteByte(EEPROM address, byte to write);
void Writefloat(unsigned char, unsigned int,float); // void WriteByte(EEPROM address, byte to write);
void WriteStringArray(unsigned char , unsigned int, char* ,unsigned int); //void WriteByte(EEPROM address, BUFFER address,no of bytes to write < 127);

unsigned char ReadByte (unsigned char, unsigned int); // read byte ReadByte (bank no, EEPROM address);
unsigned short int ReadshortInt(unsigned char, unsigned int); // read int ReadByte (EEPROM address);
unsigned int ReadInt(unsigned char, unsigned int); // read byte ReadByte (EEPROM address);
float Readfloat(unsigned char, unsigned int); // read byte ReadByte (EEPROM address);
void ReadstringArray(unsigned char, unsigned int, char* , unsigned int); //void ReadArray(EEPROM address, BUFFER address, no of bytes to read);

void WriteArray (unsigned char, unsigned int,char*,unsigned int); //void WriteArray(bank no, EEPROM address, BUFFER address ptr,no of bytes to write);
void ReadArray(unsigned char, unsigned int,char*,unsigned int); //void ReadArray(bank no, EEPROM address, BUFFER address ptr, no of bytes to read);
void FillMemory (unsigned char , unsigned int ,unsigned char ,unsigned int); //void FillMemory(bank no, EEPROM address, Data to fill,no of bytes to write);
void EraseEEPROM (unsigned char);
void ReadSMSfromMemory(unsigned int ,char *);
void ReadSMStimefromMemory(unsigned int ,char *);
void WriteSMStoMemory(unsigned int ,char *);
unsigned char StoreSMS(unsigned int ,char* ,unsigned char);
unsigned char RetrieveSMS(unsigned char ,unsigned int ,char *,struct datetime*);
unsigned char SearchByDate(unsigned int , char *,struct datetime*);
unsigned char SearchByDateTime(unsigned int , char *,struct datetime*);
unsigned char SearchByTime(unsigned int , char *,struct datetime* );
unsigned char SearchBurst(unsigned int , char *);


unsigned int AtoI(const char* );


