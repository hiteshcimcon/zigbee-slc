///** 
// * @file token-stack.h
// * @brief Definitions for stack tokens.
// * See @ref token_stack for documentation.
// *
// * The file token-stack.h should not be included directly. 
// * It is accessed by the other token files. 
// *
// * <!--Brooks Barrett-->
// * <!-- Copyright 2005 by Ember Corporation. All rights reserved. -->
// */
//
///**
// * @addtogroup token_stack
// *
// * The tokens listed here are divided into three sections (the three main
// * types of tokens mentioned in token.h):
// * - manufacturing
// * - stack
// * - application
// *
// * For a full explanation of the tokens, see hal/micro/token.h. 
// * See token-stack.h for source code. 
// *
// * There is a set of tokens predefined in the APPLICATION DATA section at the
// * end of token-stack.h because these tokens are required by the stack, 
// * but they are classified as application tokens since they are sized by the 
// * application via its CONFIGURATION_HEADER.
// * 
// * The user application can include its own tokens in a header file similar
// * to this one. The macro ::APPLICATION_TOKEN_HEADER should be defined to equal
// * the name of the header file in which application tokens are defined. 
// * See the APPLICATION DATA section at the end of token-stack.h 
// * for examples of token definitions.
// * 
// * Since token-stack.h contains both the typedefs and the token defs, there are
// * two \#defines used to select which one is needed when this file is included.
// * \#define DEFINETYPES is used to select the type definitions and
// * \#define DEFINETOKENS is used to select the token definitions.
// * Refer to token.h and token.c to see how these are used.
// *
// * @{
// */
//
//
//#ifndef DEFINEADDRESSES
///** 
// * @brief By default, tokens are automatically located after the previous token.
// *
// * If a token needs to be placed at a specific location,
// * one of the DEFINE_FIXED_* definitions should be used.  This macro is
// * inherently used in the DEFINE_FIXED_* definition to locate a token, and
// * under special circumstances (such as manufacturing tokens) it may be
// * explicitely used.
// * 
// * @param region   A name for the next region being located.
// * @param address  The address of the beginning of the next region.
// */
//  #define TOKEN_NEXT_ADDRESS(region, address)
//#endif
//
//
//// The basic TOKEN_DEF macro should not be used directly since the simplified
////  definitions are safer to use.  For completeness of information, the basic
////  macro has the following format:
////
////  TOKEN_DEF(name,creator,iscnt,isidx,type,arraysize,...)
////  name - The root name used for the token
////  creator - a "creator code" used to uniquely identify the token
////  iscnt - a boolean flag that is set to identify a counter token
////  isidx - a boolean flag that is set to identify an indexed token
////  type - the basic type or typdef of the token
////  arraysize - the number of elements making up an indexed token
////  ... - initializers used when reseting the tokens to default values
////
////
//// The following convenience macros are used to simplify the definition
////  process for commonly specified parameters to the basic TOKEN_DEF macro
////  DEFINE_BASIC_TOKEN(name, type, ...)
////  DEFINE_INDEXED_TOKEN(name, type, arraysize, ...)
////  DEFINE_COUNTER_TOKEN(name, type, ...)
////  DEFINE_FIXED_BASIC_TOKEN(name, type, address, ...)
////  DEFINE_FIXED_INDEXED_TOKEN(name, type, arraysize, address, ...)
////  DEFINE_FIXED_COUNTER_TOKEN(name, type, address, ...)
////  DEFINE_MFG_TOKEN(name, type, address, ...)
////  
//
///**
// * @name Convenience Macros
// * @brief The following convenience macros are used to simplify the definition
// * process for commonly specified parameters to the basic TOKEN_DEF macro.
// * Please see hal/micro/token.h for a more complete explanation.
// *@{
// */
//#define DEFINE_BASIC_TOKEN(name, type, ...)  \
//  TOKEN_DEF(name, CREATOR_##name, 0, 0, type, 1,  __VA_ARGS__)
//
//#define DEFINE_COUNTER_TOKEN(name, type, ...)  \
//  TOKEN_DEF(name, CREATOR_##name, 1, 0, type, 1,  __VA_ARGS__)
//
//#define DEFINE_INDEXED_TOKEN(name, type, arraysize, ...)  \
//  TOKEN_DEF(name, CREATOR_##name, 0, 1, type, (arraysize),  __VA_ARGS__)
//
//#define DEFINE_FIXED_BASIC_TOKEN(name, type, address, ...)  \
//  TOKEN_NEXT_ADDRESS(name,(address))                          \
//  TOKEN_DEF(name, CREATOR_##name, 0, 0, type, 1,  __VA_ARGS__)
//
//#define DEFINE_FIXED_COUNTER_TOKEN(name, type, address, ...)  \
//  TOKEN_NEXT_ADDRESS(name,(address))                            \
//  TOKEN_DEF(name, CREATOR_##name, 1, 0, type, 1,  __VA_ARGS__)
//
//#define DEFINE_FIXED_INDEXED_TOKEN(name, type, arraysize, address, ...)  \
//  TOKEN_NEXT_ADDRESS(name,(address))                                       \
//  TOKEN_DEF(name, CREATOR_##name, 0, 1, type, (arraysize),  __VA_ARGS__)
//
//#define DEFINE_MFG_TOKEN(name, type, address, ...)  \
//  TOKEN_NEXT_ADDRESS(name,(address))                  \
//  TOKEN_MFG(name, CREATOR_##name, 0, 0, type, 1,  __VA_ARGS__)
//  
///** @} END Convenience Macros */
//
//
//// The Simulated EEPROM unit tests define all of their own tokens.
//#ifndef SIM_EEPROM_TEST
//
//// The creator codes are here in one list instead of next to their token
//// definitions so comparision of the codes is easier.  The only requirement
//// on these creator definitions is that they all must be unique.  A favorite
//// method for picking creator codes is to use two ASCII characters inorder
//// to make the codes more memorable.
//
///**
// * @name Creator Codes
// * @brief The CREATOR is used as a distinct identifier tag for the
// * token.  
// *
// * The CREATOR is necessary because the token name is defined
// * differently depending on the hardware platform, therefore the CREATOR makes
// * sure that token definitions and data stay tagged and known.  The only
// * requirement is that each creator definition must be unique.  Please
// * see hal/micro/token.h for a more complete explanation.
// *@{
// */
// 
//// STACK CREATORS
//#define CREATOR_STACK_NVDATA_VERSION                         0xFF01
//#define CREATOR_STACK_BOOT_COUNTER                           0xE263
//#define CREATOR_STACK_NONCE_COUNTER                          0xE563
//#define CREATOR_STACK_ANALYSIS_REBOOT                        0xE162
//#define CREATOR_STACK_KEYS                                   0xEB79
//#define CREATOR_STACK_NODE_DATA                              0xEE64
//#define CREATOR_STACK_CLASSIC_DATA                           0xE364
//#define CREATOR_STACK_ALTERNATE_KEY                          0xE475
//#define CREATOR_STACK_APS_FRAME_COUNTER                      0xE123
//#define CREATOR_STACK_TRUST_CENTER                           0xE124
//#define CREATOR_STACK_NETWORK_MANAGEMENT                     0xE125
//#define CREATOR_STACK_PARENT_INFO                            0xE126
//// MULTI-NETWORK STACK CREATORS
//#define CREATOR_MULTI_NETWORK_STACK_KEYS                     0xE210
//#define CREATOR_MULTI_NETWORK_STACK_NODE_DATA                0xE211
//#define CREATOR_MULTI_NETWORK_STACK_ALTERNATE_KEY            0xE212
//#define CREATOR_MULTI_NETWORK_STACK_TRUST_CENTER             0xE213
//#define CREATOR_MULTI_NETWORK_STACK_NETWORK_MANAGEMENT       0xE214
//#define CREATOR_MULTI_NETWORK_STACK_PARENT_INFO              0xE215
//// Temporary solution for multi-network nwk counters: for now we define
//// the following counter which will be used on the network with index 1.
//#define CREATOR_MULTI_NETWORK_STACK_NONCE_COUNTER            0xE220
//
//// RF4CE stack tokens.
//#define CREATOR_STACK_RF4CE_DATA                             0xE250
//#define CREATOR_STACK_RF4CE_PAIRING_TABLE                    0xE251
//
//// APP CREATORS
//#define CREATOR_STACK_BINDING_TABLE                          0xE274
//#define CREATOR_STACK_CHILD_TABLE                            0xFF0D
//#define CREATOR_STACK_KEY_TABLE                              0xE456
//#define CREATOR_STACK_CERTIFICATE_TABLE                      0xE500
//#define CREATOR_STACK_ZLL_DATA                               0xE501
//#define CREATOR_STACK_ZLL_SECURITY                           0xE502
//
///** @} END Creator Codes  */
//
//
////////////////////////////////////////////////////////////////////////////////
//// MANUFACTURING DATA
//// Since the manufacturing data is platform specific, we pull in the proper
//// file here.
//#if defined(AVR_ATMEGA)
//  #include "hal/micro/avr-atmega/token-manufacturing.h"
//#elif defined(MSP430)
//  #include "hal/micro/msp430/token-manufacturing.h"
//#elif defined(XAP2B)
//  #include "hal/micro/xap2b/token-manufacturing.h"
//#elif defined(CORTEXM3)
//  // cortexm3 handles mfg tokens seperately via mfg-token.h
//#elif defined(EMBER_TEST)
//  #include "hal/micro/avr-atmega/token-manufacturing.h"
//#else
//  #error no platform defined
//#endif
//
//
////////////////////////////////////////////////////////////////////////////////
//// STACK DATA
//// *the addresses of these tokens must not change*
//
///**
// * @brief The current version number of the stack tokens.
// * MSB is the version, LSB is a complement.  
// *
// * Please see hal/micro/token.h for a more complete explanation.
// */
//#define CURRENT_STACK_TOKEN_VERSION 0x03FC //MSB is version, LSB is complement
//
//#ifdef DEFINETYPES
//typedef int16u tokTypeStackNvdataVersion;
//typedef int32u tokTypeStackBootCounter;
//typedef int16u tokTypeStackAnalysisReboot;
//typedef int32u tokTypeStackNonceCounter;
//typedef struct {
//  int8u networkKey[16];
//  int8u activeKeySeqNum;
//} tokTypeStackKeys;
//typedef struct {
//  int16u panId;
//  int8s radioTxPower;
//  int8u radioFreqChannel;
//  int8u stackProfile;
//  int8u nodeType;
//  int16u zigbeeNodeId;
//  int8u extendedPanId[8];
//} tokTypeStackNodeData;
//typedef struct {
//  int16u mode;
//  int8u eui64[8];
//  int8u key[16];
//} tokTypeStackTrustCenter;
//typedef struct {
//  int32u activeChannels;
//  int16u managerNodeId;
//  int8u updateId;
//} tokTypeStackNetworkManagement;
//typedef struct {
//  int8u parentEui[8];
//  int16u parentNodeId;
//} tokTypeStackParentInfo;
//#endif //DEFINETYPES
//
//#ifdef DEFINETOKENS
//// The Stack tokens also need to be stored at well-defined locations
////  None of these addresses should ever change without extremely great care
//#define STACK_VERSION_LOCATION    128 //  2   bytes
//#define STACK_APS_NONCE_LOCATION  130 //  4   bytes
//#define STACK_ALT_NWK_KEY_LOCATION 134 // 17  bytes (key + sequence number)
//// reserved                       151     1   bytes
//#define STACK_BOOT_COUNT_LOCATION 152 //  2   bytes
//// reserved                       154     2   bytes
//#define STACK_NONCE_LOCATION      156 //  4   bytes
//// reserved                       160     1   bytes
//#define STACK_REBOOT_LOCATION     161 //  2   bytes
//// reserved                       163     7   bytes
//#define STACK_KEYS_LOCATION       170 //  17  bytes
//// reserved                       187     5   bytes
//#define STACK_NODE_DATA_LOCATION  192 //  16  bytes
//#define STACK_CLASSIC_LOCATION    208 //  26  bytes
//#define STACK_TRUST_CENTER_LOCATION 234 //26  bytes
//// reserved                       260     8   bytes
//#define STACK_NETWORK_MANAGEMENT_LOCATION 268
//                                      //  7   bytes
//// reserved                       275     109 bytes
//
//DEFINE_FIXED_BASIC_TOKEN(STACK_NVDATA_VERSION, 
//                         tokTypeStackNvdataVersion,
//                         STACK_VERSION_LOCATION,
//                         CURRENT_STACK_TOKEN_VERSION)
//DEFINE_FIXED_COUNTER_TOKEN(STACK_APS_FRAME_COUNTER,
//                           tokTypeStackNonceCounter,
//                           STACK_APS_NONCE_LOCATION,
//                           0x00000000)
//DEFINE_FIXED_BASIC_TOKEN(STACK_ALTERNATE_KEY,
//                         tokTypeStackKeys,
//                         STACK_ALT_NWK_KEY_LOCATION,
//                         {{0,}})
//DEFINE_FIXED_COUNTER_TOKEN(STACK_BOOT_COUNTER,
//                           tokTypeStackBootCounter,
//                           STACK_BOOT_COUNT_LOCATION,
//                           0x0000)
//DEFINE_FIXED_COUNTER_TOKEN(STACK_NONCE_COUNTER,
//                           tokTypeStackNonceCounter,
//                           STACK_NONCE_LOCATION,
//                           0x00000000)
//DEFINE_FIXED_BASIC_TOKEN(STACK_ANALYSIS_REBOOT,
//                         tokTypeStackAnalysisReboot,
//                         STACK_REBOOT_LOCATION,
//                         0x0000)
//DEFINE_FIXED_BASIC_TOKEN(STACK_KEYS,
//                         tokTypeStackKeys,
//                         STACK_KEYS_LOCATION,
//                         {{0,}})
//DEFINE_FIXED_BASIC_TOKEN(STACK_NODE_DATA,
//                         tokTypeStackNodeData,
//                         STACK_NODE_DATA_LOCATION,
//                         {0xFFFF,-1,0,0x00,0x00,0x0000})
//DEFINE_FIXED_BASIC_TOKEN(STACK_TRUST_CENTER,
//                         tokTypeStackTrustCenter,
//                         STACK_TRUST_CENTER_LOCATION,
//                         {0,})
//DEFINE_FIXED_BASIC_TOKEN(STACK_NETWORK_MANAGEMENT,
//                         tokTypeStackNetworkManagement,
//                         STACK_NETWORK_MANAGEMENT_LOCATION,
//                         {0, 0xFFFF, 0})
//DEFINE_BASIC_TOKEN(STACK_PARENT_INFO,
//                   tokTypeStackParentInfo,
//                   { {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 0xFFFF} )
//
//#endif //DEFINETOKENS
//
//
////////////////////////////////////////////////////////////////////////////////
//// PHY DATA
//#include "token-phy.h"
//
////////////////////////////////////////////////////////////////////////////////
//// MULTI-NETWORK STACK TOKENS: Tokens for the networks with index > 0.
//// The 0-index network info is stored in the usual tokens.
//
//#ifdef DEFINETOKENS
//#if !defined(EMBER_MULTI_NETWORK_STRIPPED)
//#define EXTRA_NETWORKS_NUMBER (EMBER_SUPPORTED_NETWORKS - 1)
//DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_KEYS,
//                     tokTypeStackKeys,
//                     EXTRA_NETWORKS_NUMBER,
//                     {{0,}})
//DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_NODE_DATA,
//                     tokTypeStackNodeData,
//                     EXTRA_NETWORKS_NUMBER,
//                     {0,})
//DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_ALTERNATE_KEY,
//                     tokTypeStackKeys,
//                     EXTRA_NETWORKS_NUMBER,
//                     {{0,}})
//DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_TRUST_CENTER,
//                     tokTypeStackTrustCenter,
//                     EXTRA_NETWORKS_NUMBER,
//                     {0,})
//DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_NETWORK_MANAGEMENT,
//                     tokTypeStackNetworkManagement,
//                     EXTRA_NETWORKS_NUMBER,
//                     {0,})
//DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_PARENT_INFO,
//                     tokTypeStackParentInfo,
//                     EXTRA_NETWORKS_NUMBER,
//                     {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 0xFFFF} )
//
//// Temporary solution for NWK counter token: the following is used for 1-index
//// network.
//DEFINE_COUNTER_TOKEN(MULTI_NETWORK_STACK_NONCE_COUNTER,
//                     tokTypeStackNonceCounter,
//                     0x00000000)
//#endif // EMBER_MULTI_NETWORK_STRIPPED
//#endif // DEFINETOKENS
//
//
////////////////////////////////////////////////////////////////////////////////
//// APPLICATION DATA
//// *If a fixed application token is desired, its address must be above 384.*
//
//#ifdef DEFINETYPES
//typedef int8u tokTypeStackBindingTable[13];
//typedef int8u tokTypeStackChildTable[11];
//typedef int8u tokTypeStackKeyTable[25];
//// Certificate Table Entry
////   Certificate:    48-bytes
////   CA Public Key:  22-bytes
////   Private Key:    21-bytes
////   Flags:          1-byte
//#define TOKEN_CERTIFICATE_TABLE_ENTRY_SIZE (48 + 22 + 21 + 1)
//#define TOKEN_CERTIFICATE_TABLE_ENTRY_FLAGS_INDEX (TOKEN_CERTIFICATE_TABLE_ENTRY_SIZE - 1)
//typedef int8u tokTypeStackCertificateTable[TOKEN_CERTIFICATE_TABLE_ENTRY_SIZE];
//#endif //DEFINETYPES
//
//// The following application tokens are required by the stack, but are sized by 
////  the application via its CONFIGURATION_HEADER, which is why they are present
////  within the application data section.  Any special application defined
////  tokens will follow.  
//// NOTE: changing the size of these tokens within the CONFIGURATION_HEADER
////  WILL move automatically move any custom application tokens that are defined
////  in the APPLICATION_TOKEN_HEADER
//#ifdef DEFINETOKENS
//// Application tokens start at location 384 and are automatically positioned.
//TOKEN_NEXT_ADDRESS(APP,384)
//DEFINE_INDEXED_TOKEN(STACK_BINDING_TABLE,
//                     tokTypeStackBindingTable,
//                     EMBER_BINDING_TABLE_TOKEN_SIZE,
//                     {0,})
//DEFINE_INDEXED_TOKEN(STACK_CHILD_TABLE,
//                     tokTypeStackChildTable,
//                     EMBER_CHILD_TABLE_TOKEN_SIZE,
//                     {0,})
//DEFINE_INDEXED_TOKEN(STACK_KEY_TABLE,
//                     tokTypeStackKeyTable,
//                     EMBER_KEY_TABLE_TOKEN_SIZE,
//                     {0,})
//DEFINE_INDEXED_TOKEN(STACK_CERTIFICATE_TABLE,
//                     tokTypeStackCertificateTable,
//                     EMBER_CERTIFICATE_TABLE_SIZE,
//                     {0,})
//#endif //DEFINETOKENS
//
//// These must appear before the application header so that the token
//// numbering is consistent regardless of whether application tokens are
//// defined.
//#ifndef XAP2B
//  #include "stack/zll/zll-token-config.h"
//  #include "stack/rf4ce/rf4ce-token-config.h"
//#endif
//
//#ifdef APPLICATION_TOKEN_HEADER
//  #include APPLICATION_TOKEN_HEADER
//#endif
//
////The tokens defined below are test tokens.  They are normally not used by
////anything but are left here as a convenience so test tokens do not have to
////be recreated.  If test code needs temporary, non-volatile storage, simply
////uncomment and alter the set below as needed.
////#define CREATOR_TT01 1
////#define CREATOR_TT02 2
////#define CREATOR_TT03 3
////#define CREATOR_TT04 4
////#define CREATOR_TT05 5
////#define CREATOR_TT06 6
////#ifdef DEFINETYPES
////typedef int32u tokTypeTT01;
////typedef int32u tokTypeTT02;
////typedef int32u tokTypeTT03;
////typedef int32u tokTypeTT04;
////typedef int16u tokTypeTT05;
////typedef int16u tokTypeTT06;
////#endif //DEFINETYPES
////#ifdef DEFINETOKENS
////#define TT01_LOCATION 1
////#define TT02_LOCATION 2
////#define TT03_LOCATION 3
////#define TT04_LOCATION 4
////#define TT05_LOCATION 5
////#define TT06_LOCATION 6
////DEFINE_FIXED_BASIC_TOKEN(TT01, tokTypeTT01, TT01_LOCATION, 0x0000)
////DEFINE_FIXED_BASIC_TOKEN(TT02, tokTypeTT02, TT02_LOCATION, 0x0000)
////DEFINE_FIXED_BASIC_TOKEN(TT03, tokTypeTT03, TT03_LOCATION, 0x0000)
////DEFINE_FIXED_BASIC_TOKEN(TT04, tokTypeTT04, TT04_LOCATION, 0x0000)
////DEFINE_FIXED_BASIC_TOKEN(TT05, tokTypeTT05, TT05_LOCATION, 0x0000)
////DEFINE_FIXED_BASIC_TOKEN(TT06, tokTypeTT06, TT06_LOCATION, 0x0000)
////#endif //DEFINETOKENS
//
//
//
//#else //SIM_EEPROM_TEST
//
//  //The Simulated EEPROM unit tests define all of their tokens via the
//  //APPLICATION_TOKEN_HEADER macro.
//  #ifdef APPLICATION_TOKEN_HEADER
//    #include APPLICATION_TOKEN_HEADER
//  #endif
//
//#endif //SIM_EEPROM_TEST
//
//#ifndef DEFINEADDRESSES
//  #undef TOKEN_NEXT_ADDRESS
//#endif
//
///** @} END addtogroup */
//
///**
// * <!-- HIDDEN
// * @page 2p5_to_3p0
// * <hr>
// * The file token-stack.h is described in @ref token_stack and includes 
// * the following:
// * <ul>
// * <li> <b>New items</b>
// *   - ::CREATOR_STACK_ALTERNATE_KEY
// *   - ::CREATOR_STACK_APS_FRAME_COUNTER
// *   - ::CREATOR_STACK_LINK_KEY_TABLE
// *   .
// * <li> <b>Changed items</b>
// *   - 
// *   - 
// *   .
// * <li> <b>Removed items</b>
// *   - ::CREATOR_STACK_DISCOVERY_CACHE
// *   - ::CREATOR_STACK_APS_INDIRECT_BINDING_TABLE
// *   .
// * </ul>
// * HIDDEN -->
// */

/**
 * @file token-stack.h
 * @brief Definitions for stack tokens.
 * See @ref token_stack for documentation.
 *
 * The file token-stack.h should not be included directly.
 * It is accessed by the other token files.
 *
 * <!--Brooks Barrett-->
 * <!-- Copyright 2005 by Ember Corporation. All rights reserved. -->
 */

/**
 * @addtogroup token_stack
 *
 * The tokens listed here are divided into three sections (the three main
 * types of tokens mentioned in token.h):
 * - manufacturing
 * - stack
 * - application
 *
 * For a full explanation of the tokens, see hal/micro/token.h.
 * See token-stack.h for source code.
 *
 * There is a set of tokens predefined in the APPLICATION DATA section at the
 * end of token-stack.h because these tokens are required by the stack,
 * but they are classified as application tokens since they are sized by the
 * application via its CONFIGURATION_HEADER.
 *
 * The user application can include its own tokens in a header file similar
 * to this one. The macro ::APPLICATION_TOKEN_HEADER should be defined to equal
 * the name of the header file in which application tokens are defined.
 * See the APPLICATION DATA section at the end of token-stack.h
 * for examples of token definitions.
 *
 * Since token-stack.h contains both the typedefs and the token defs, there are
 * two \#defines used to select which one is needed when this file is included.
 * \#define DEFINETYPES is used to select the type definitions and
 * \#define DEFINETOKENS is used to select the token definitions.
 * Refer to token.h and token.c to see how these are used.
 *
 * @{
 */

#include "SLC/SLC_defination.h"

#ifndef DEFINEADDRESSES
/**
 * @brief By default, tokens are automatically located after the previous token.
 *
 * If a token needs to be placed at a specific location,
 * one of the DEFINE_FIXED_* definitions should be used.  This macro is
 * inherently used in the DEFINE_FIXED_* definition to locate a token, and
 * under special circumstances (such as manufacturing tokens) it may be
 * explicitely used.
 *
 * @param region   A name for the next region being located.
 * @param address  The address of the beginning of the next region.
 */
  #define TOKEN_NEXT_ADDRESS(region, address)
#endif


// The basic TOKEN_DEF macro should not be used directly since the simplified
//  definitions are safer to use.  For completeness of information, the basic
//  macro has the following format:
//
//  TOKEN_DEF(name,creator,iscnt,isidx,type,arraysize,...)
//  name - The root name used for the token
//  creator - a "creator code" used to uniquely identify the token
//  iscnt - a boolean flag that is set to identify a counter token
//  isidx - a boolean flag that is set to identify an indexed token
//  type - the basic type or typdef of the token
//  arraysize - the number of elements making up an indexed token
//  ... - initializers used when reseting the tokens to default values
//
//
// The following convenience macros are used to simplify the definition
//  process for commonly specified parameters to the basic TOKEN_DEF macro
//  DEFINE_BASIC_TOKEN(name, type, ...)
//  DEFINE_INDEXED_TOKEN(name, type, arraysize, ...)
//  DEFINE_COUNTER_TOKEN(name, type, ...)
//  DEFINE_FIXED_BASIC_TOKEN(name, type, address, ...)
//  DEFINE_FIXED_INDEXED_TOKEN(name, type, arraysize, address, ...)
//  DEFINE_FIXED_COUNTER_TOKEN(name, type, address, ...)
//  DEFINE_MFG_TOKEN(name, type, address, ...)
//

/**
 * @name Convenience Macros
 * @brief The following convenience macros are used to simplify the definition
 * process for commonly specified parameters to the basic TOKEN_DEF macro.
 * Please see hal/micro/token.h for a more complete explanation.
 *@{
 */
#define DEFINE_BASIC_TOKEN(name, type, ...)  \
  TOKEN_DEF(name, CREATOR_##name, 0, 0, type, 1,  __VA_ARGS__)

#define DEFINE_COUNTER_TOKEN(name, type, ...)  \
  TOKEN_DEF(name, CREATOR_##name, 1, 0, type, 1,  __VA_ARGS__)

#define DEFINE_INDEXED_TOKEN(name, type, arraysize, ...)  \
  TOKEN_DEF(name, CREATOR_##name, 0, 1, type, (arraysize),  __VA_ARGS__)

#define DEFINE_FIXED_BASIC_TOKEN(name, type, address, ...)  \
  TOKEN_NEXT_ADDRESS(name,(address))                          \
  TOKEN_DEF(name, CREATOR_##name, 0, 0, type, 1,  __VA_ARGS__)

#define DEFINE_FIXED_COUNTER_TOKEN(name, type, address, ...)  \
  TOKEN_NEXT_ADDRESS(name,(address))                            \
  TOKEN_DEF(name, CREATOR_##name, 1, 0, type, 1,  __VA_ARGS__)

#define DEFINE_FIXED_INDEXED_TOKEN(name, type, arraysize, address, ...)  \
  TOKEN_NEXT_ADDRESS(name,(address))                                       \
  TOKEN_DEF(name, CREATOR_##name, 0, 1, type, (arraysize),  __VA_ARGS__)

#define DEFINE_MFG_TOKEN(name, type, address, ...)  \
  TOKEN_NEXT_ADDRESS(name,(address))                  \
  TOKEN_MFG(name, CREATOR_##name, 0, 0, type, 1,  __VA_ARGS__)
  
/** @} END Convenience Macros */


// The Simulated EEPROM unit tests define all of their own tokens.
#ifndef SIM_EEPROM_TEST

// The creator codes are here in one list instead of next to their token
// definitions so comparision of the codes is easier.  The only requirement
// on these creator definitions is that they all must be unique.  A favorite
// method for picking creator codes is to use two ASCII characters inorder
// to make the codes more memorable.

/**
 * @name Creator Codes
 * @brief The CREATOR is used as a distinct identifier tag for the
 * token.  
 *
 * The CREATOR is necessary because the token name is defined
 * differently depending on the hardware platform, therefore the CREATOR makes
 * sure that token definitions and data stay tagged and known.  The only
 * requirement is that each creator definition must be unique.  Please
 * see hal/micro/token.h for a more complete explanation.
 *@{
 */
 
// STACK CREATORS
#define CREATOR_STACK_NVDATA_VERSION                         0xFF01
#define CREATOR_STACK_BOOT_COUNTER                           0xE263
#define CREATOR_STACK_NONCE_COUNTER                          0xE563
#define CREATOR_STACK_ANALYSIS_REBOOT                        0xE162
#define CREATOR_STACK_KEYS                                   0xEB79
#define CREATOR_STACK_NODE_DATA                              0xEE64
#define CREATOR_STACK_CLASSIC_DATA                           0xE364
#define CREATOR_STACK_ALTERNATE_KEY                          0xE475
#define CREATOR_STACK_APS_FRAME_COUNTER                      0xE123
#define CREATOR_STACK_TRUST_CENTER                           0xE124
#define CREATOR_STACK_NETWORK_MANAGEMENT                     0xE125
#define CREATOR_STACK_PARENT_INFO                            0xE126
// MULTI-NETWORK STACK CREATORS
#define CREATOR_MULTI_NETWORK_STACK_KEYS                     0xE210
#define CREATOR_MULTI_NETWORK_STACK_NODE_DATA                0xE211
#define CREATOR_MULTI_NETWORK_STACK_ALTERNATE_KEY            0xE212
#define CREATOR_MULTI_NETWORK_STACK_TRUST_CENTER             0xE213
#define CREATOR_MULTI_NETWORK_STACK_NETWORK_MANAGEMENT       0xE214
#define CREATOR_MULTI_NETWORK_STACK_PARENT_INFO              0xE215
// Temporary solution for multi-network nwk counters: for now we define
// the following counter which will be used on the network with index 1.
#define CREATOR_MULTI_NETWORK_STACK_NONCE_COUNTER            0xE220

// RF4CE stack tokens.
#define CREATOR_STACK_RF4CE_DATA                             0xE250
#define CREATOR_STACK_RF4CE_PAIRING_TABLE                    0xE251

// APP CREATORS
#define CREATOR_STACK_BINDING_TABLE                          0xE274
#define CREATOR_STACK_CHILD_TABLE                            0xFF0D
#define CREATOR_STACK_KEY_TABLE                              0xE456
#define CREATOR_STACK_CERTIFICATE_TABLE                      0xE500
#define CREATOR_STACK_ZLL_DATA                               0xE501
#define CREATOR_STACK_ZLL_SECURITY                           0xE502

/** @} END Creator Codes  */


//////////////////////////////////////////////////////////////////////////////
// MANUFACTURING DATA
// Since the manufacturing data is platform specific, we pull in the proper
// file here.
#if defined(AVR_ATMEGA)
  #include "hal/micro/avr-atmega/token-manufacturing.h"
#elif defined(MSP430)
  #include "hal/micro/msp430/token-manufacturing.h"
#elif defined(XAP2B)
  #include "hal/micro/xap2b/token-manufacturing.h"
#elif defined(CORTEXM3)
  // cortexm3 handles mfg tokens seperately via mfg-token.h
#elif defined(EMBER_TEST)
  #include "hal/micro/avr-atmega/token-manufacturing.h"
#else
  #error no platform defined
#endif


//////////////////////////////////////////////////////////////////////////////
// STACK DATA
// *the addresses of these tokens must not change*

/**
 * @brief The current version number of the stack tokens.
 * MSB is the version, LSB is a complement.
 *
 * Please see hal/micro/token.h for a more complete explanation.
 */
#define CURRENT_STACK_TOKEN_VERSION 0x03FC //MSB is version, LSB is complement

#ifdef DEFINETYPES
typedef int16u tokTypeStackNvdataVersion;
typedef int32u tokTypeStackBootCounter;
typedef int16u tokTypeStackAnalysisReboot;
typedef int32u tokTypeStackNonceCounter;
typedef struct {
  int8u networkKey[16];
  int8u activeKeySeqNum;
} tokTypeStackKeys;
typedef struct {
  int16u panId;
  int8s radioTxPower;
  int8u radioFreqChannel;
  int8u stackProfile;
  int8u nodeType;
  int16u zigbeeNodeId;
  int8u extendedPanId[8];
} tokTypeStackNodeData;
typedef struct {
  int16u mode;
  int8u eui64[8];
  int8u key[16];
} tokTypeStackTrustCenter;
typedef struct {
  int32u activeChannels;
  int16u managerNodeId;
  int8u updateId;
} tokTypeStackNetworkManagement;
typedef struct {
  int8u parentEui[8];
  int16u parentNodeId;
} tokTypeStackParentInfo;
#endif //DEFINETYPES

#ifdef DEFINETOKENS
// The Stack tokens also need to be stored at well-defined locations
//  None of these addresses should ever change without extremely great care
#define STACK_VERSION_LOCATION    128 //  2   bytes
#define STACK_APS_NONCE_LOCATION  130 //  4   bytes
#define STACK_ALT_NWK_KEY_LOCATION 134 // 17  bytes (key + sequence number)
// reserved                       151     1   bytes
#define STACK_BOOT_COUNT_LOCATION 152 //  2   bytes
// reserved                       154     2   bytes
#define STACK_NONCE_LOCATION      156 //  4   bytes
// reserved                       160     1   bytes
#define STACK_REBOOT_LOCATION     161 //  2   bytes
// reserved                       163     7   bytes
#define STACK_KEYS_LOCATION       170 //  17  bytes
// reserved                       187     5   bytes
#define STACK_NODE_DATA_LOCATION  192 //  16  bytes
#define STACK_CLASSIC_LOCATION    208 //  26  bytes
#define STACK_TRUST_CENTER_LOCATION 234 //26  bytes
// reserved                       260     8   bytes
#define STACK_NETWORK_MANAGEMENT_LOCATION 268
                                      //  7   bytes
// reserved                       275     109 bytes

DEFINE_FIXED_BASIC_TOKEN(STACK_NVDATA_VERSION, 
                         tokTypeStackNvdataVersion,
                         STACK_VERSION_LOCATION,
                         CURRENT_STACK_TOKEN_VERSION)
DEFINE_FIXED_COUNTER_TOKEN(STACK_APS_FRAME_COUNTER,
                           tokTypeStackNonceCounter,
                           STACK_APS_NONCE_LOCATION,
                           0x00000000)
DEFINE_FIXED_BASIC_TOKEN(STACK_ALTERNATE_KEY,
                         tokTypeStackKeys,
                         STACK_ALT_NWK_KEY_LOCATION,
                         {{0,}})
DEFINE_FIXED_COUNTER_TOKEN(STACK_BOOT_COUNTER,
                           tokTypeStackBootCounter,
                           STACK_BOOT_COUNT_LOCATION,
                           0x0000)
DEFINE_FIXED_COUNTER_TOKEN(STACK_NONCE_COUNTER,
                           tokTypeStackNonceCounter,
                           STACK_NONCE_LOCATION,
                           0x00000000)
DEFINE_FIXED_BASIC_TOKEN(STACK_ANALYSIS_REBOOT,
                         tokTypeStackAnalysisReboot,
                         STACK_REBOOT_LOCATION,
                         0x0000)
DEFINE_FIXED_BASIC_TOKEN(STACK_KEYS,
                         tokTypeStackKeys,
                         STACK_KEYS_LOCATION,
                         {{0,}})
DEFINE_FIXED_BASIC_TOKEN(STACK_NODE_DATA,
                         tokTypeStackNodeData,
                         STACK_NODE_DATA_LOCATION,
                         {0xFFFF,-1,0,0x00,0x00,0x0000})
DEFINE_FIXED_BASIC_TOKEN(STACK_TRUST_CENTER,
                         tokTypeStackTrustCenter,
                         STACK_TRUST_CENTER_LOCATION,
                         {0,})
DEFINE_FIXED_BASIC_TOKEN(STACK_NETWORK_MANAGEMENT,
                         tokTypeStackNetworkManagement,
                         STACK_NETWORK_MANAGEMENT_LOCATION,
                         {0, 0xFFFF, 0})
DEFINE_BASIC_TOKEN(STACK_PARENT_INFO,
                   tokTypeStackParentInfo,
                   { {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 0xFFFF} )

#endif //DEFINETOKENS


//////////////////////////////////////////////////////////////////////////////
// PHY DATA
#include "token-phy.h"

//////////////////////////////////////////////////////////////////////////////
// MULTI-NETWORK STACK TOKENS: Tokens for the networks with index > 0.
// The 0-index network info is stored in the usual tokens.

#ifdef DEFINETOKENS
#if !defined(EMBER_MULTI_NETWORK_STRIPPED)
#define EXTRA_NETWORKS_NUMBER (EMBER_SUPPORTED_NETWORKS - 1)
DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_KEYS,
                     tokTypeStackKeys,
                     EXTRA_NETWORKS_NUMBER,
                     {{0,}})
DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_NODE_DATA,
                     tokTypeStackNodeData,
                     EXTRA_NETWORKS_NUMBER,
                     {0,})
DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_ALTERNATE_KEY,
                     tokTypeStackKeys,
                     EXTRA_NETWORKS_NUMBER,
                     {{0,}})
DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_TRUST_CENTER,
                     tokTypeStackTrustCenter,
                     EXTRA_NETWORKS_NUMBER,
                     {0,})
DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_NETWORK_MANAGEMENT,
                     tokTypeStackNetworkManagement,
                     EXTRA_NETWORKS_NUMBER,
                     {0,})
DEFINE_INDEXED_TOKEN(MULTI_NETWORK_STACK_PARENT_INFO,
                     tokTypeStackParentInfo,
                     EXTRA_NETWORKS_NUMBER,
                     {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 0xFFFF} )

// Temporary solution for NWK counter token: the following is used for 1-index
// network.
DEFINE_COUNTER_TOKEN(MULTI_NETWORK_STACK_NONCE_COUNTER,
                     tokTypeStackNonceCounter,
                     0x00000000)
#endif // EMBER_MULTI_NETWORK_STRIPPED
#endif // DEFINETOKENS


//////////////////////////////////////////////////////////////////////////////
// APPLICATION DATA
// *If a fixed application token is desired, its address must be above 384.*

#ifdef DEFINETYPES
typedef int8u tokTypeStackBindingTable[13];
typedef int8u tokTypeStackChildTable[11];
typedef int8u tokTypeStackKeyTable[25];
// Certificate Table Entry
//   Certificate:    48-bytes
//   CA Public Key:  22-bytes
//   Private Key:    21-bytes
//   Flags:          1-byte
#define TOKEN_CERTIFICATE_TABLE_ENTRY_SIZE (48 + 22 + 21 + 1)
#define TOKEN_CERTIFICATE_TABLE_ENTRY_FLAGS_INDEX (TOKEN_CERTIFICATE_TABLE_ENTRY_SIZE - 1)
typedef int8u tokTypeStackCertificateTable[TOKEN_CERTIFICATE_TABLE_ENTRY_SIZE];
#endif //DEFINETYPES

// The following application tokens are required by the stack, but are sized by
//  the application via its CONFIGURATION_HEADER, which is why they are present
//  within the application data section.  Any special application defined
//  tokens will follow.
// NOTE: changing the size of these tokens within the CONFIGURATION_HEADER
//  WILL move automatically move any custom application tokens that are defined
//  in the APPLICATION_TOKEN_HEADER
#ifdef DEFINETOKENS
// Application tokens start at location 384 and are automatically positioned.
TOKEN_NEXT_ADDRESS(APP,384)
DEFINE_INDEXED_TOKEN(STACK_BINDING_TABLE,
                     tokTypeStackBindingTable,
                     EMBER_BINDING_TABLE_TOKEN_SIZE,
                     {0,})
DEFINE_INDEXED_TOKEN(STACK_CHILD_TABLE,
                     tokTypeStackChildTable,
                     EMBER_CHILD_TABLE_TOKEN_SIZE,
                     {0,})
DEFINE_INDEXED_TOKEN(STACK_KEY_TABLE,
                     tokTypeStackKeyTable,
                     EMBER_KEY_TABLE_TOKEN_SIZE,
                     {0,})
DEFINE_INDEXED_TOKEN(STACK_CERTIFICATE_TABLE,
                     tokTypeStackCertificateTable,
                     EMBER_CERTIFICATE_TABLE_SIZE,
                     {0,})
#endif //DEFINETOKENS

// These must appear before the application header so that the token
// numbering is consistent regardless of whether application tokens are
// defined.
#ifndef XAP2B
  #include "stack/zll/zll-token-config.h"
  #include "stack/rf4ce/rf4ce-token-config.h"
#endif

#ifdef APPLICATION_TOKEN_HEADER
  #include APPLICATION_TOKEN_HEADER
#endif

//The tokens defined below are test tokens.  They are normally not used by
//anything but are left here as a convenience so test tokens do not have to
//be recreated.  If test code needs temporary, non-volatile storage, simply
//uncomment and alter the set below as needed.
#define CREATOR_automode  1
#define CREATOR_temp      2
#define CREATOR_amr_id    3
#define CREATOR_Latitude  4
#define CREATOR_longitude 5
#define CREATOR_utcOffset 6
#define CREATOR_Sunset_delay 7
#define CREATOR_Sunrise_delay 8
#define CREATOR_pulses_r      9
#define CREATOR_lamp_burn_hour 10
#define CREATOR_lamp_current 11
#define CREATOR_detect_lamp_current 12
#define CREATOR_Vol_hi   13
#define CREATOR_Vol_low 14
#define CREATOR_Curr_Steady_Time 15
#define CREATOR_Per_Val_Current  16
#define CREATOR_Lamp_Fault_Time  17
#define CREATOR_Lamp_off_on_Time 18
#define CREATOR_Lamp_faulty_retrieve_Count 19
#define CREATOR_Broadcast_time_delay  20
#define CREATOR_Unicast_time_delay   21
#define CREATOR_Lamp_lock_condition  22
#define CREATOR_schedule  23
#define CREATOR_Single_time_run_cmd 24
#define CREATOR_scann_chan  25
#define CREATOR_PanId 26
#define CREATOR_ExtPanId_set 27
//#define CREATOR_schedule67  28
#define CREATOR_sch_day 28
#define CREATOR_Master_schedule  29
//#define CREATOR_Master_schedule67  30
#define CREATOR_Master_sch_day 30

#define CREATOR_schedule_Astro_sSch  31
#define CREATOR_adaptive_light_dimming 32
#define CREATOR_analog_input_scaling_high_Value 33
#define CREATOR_analog_input_scaling_low_Value 34
#define CREATOR_desir_lamp_lumen 35
#define CREATOR_lumen_tollarence 36
#define CREATOR_ballast_type  37

#define CREATOR_Motion_pulse_rate 38
#define CREATOR_Motion_dimming_percentage 39
#define CREATOR_Motion_dimming_time 40
#define CREATOR_Motion_group_id 41
#define CREATOR_dim_applay_time 42
#define CREATOR_dim_inc_val 43
#define CREATOR_Motion_normal_dimming_percentage 44
#define CREATOR_Valid_DCU_Flag 45
#define CREATOR_Default_Para_Set 46
#define CREATOR_Slc_Mix_Mode_schedule_1 47
#define CREATOR_Slc_Mix_Mode_schedule_2 48
#define CREATOR_Slc_Mix_Mode_schedule_3 49
#define CREATOR_Slc_Mix_Mode_schedule_4 50
#define CREATOR_Slc_Mix_Mode_schedule_5 51
#define CREATOR_Slc_Mix_Mode_schedule_6 52
#define CREATOR_Slc_Mix_Mode_schedule_7 53
#define CREATOR_Slc_Mix_Mode_schedule_8 54
#define CREATOR_Slc_Mix_Mode_schedule_9 55
#define CREATOR_Slc_Mix_Mode_schedule_10 56
//#define CREATOR_Slc_Mix_Mode_schedule_11 57
//#define CREATOR_Slc_Mix_Mode_schedule_12 58
//#define CREATOR_Slc_Mix_Mode_schedule_13 59
//#define CREATOR_Slc_Mix_Mode_schedule_14 60
//#define CREATOR_Slc_Mix_Mode_schedule_15 61
////////////////////idimmer////////////////////////////
#define CREATOR_idimmer_en 57
#define CREATOR_idimmer_id 58
#define CREATOR_idimmer_longaddress 59
#define CREATOR_SLC_DST_En 60
#define CREATOR_SLC_DST_Start_Rule 61
#define CREATOR_SLC_DST_Start_Month 62
#define CREATOR_SLC_DST_Start_Time 63
#define CREATOR_SLC_DST_Stop_Rule 64
#define CREATOR_SLC_DST_Stop_Month 65
#define CREATOR_SLC_DST_Stop_Time 66

#define CREATOR_SLC_DST_Time_Zone_Diff 67
#define CREATOR_Time_change_dueto_DST  68
#define CREATOR_SLC_DST_Rule_Enable  69
#define CREATOR_SLC_DST_Start_Date  70
#define CREATOR_SLC_DST_Stop_Date 71
#define CREATOR_Auto_event 72
#define CREATOR_Day_light_harvesting_start_offset 73
#define CREATOR_Day_light_harvesting_stop_offset 74
#define CREATOR_Lamp_type 75
#define CREATOR_SLC_New_Manula_Mode_Val 76
#define CREATOR_TLink_Key 77
#define CREATOR_SLC_New_Manual_Mode_Timer 78

#define CREATOR_Photocell_steady_timeout_Val 79
#define CREATOR_photo_cell_toggel_counter_Val 80	
#define CREATOR_photo_cell_toggel_timer_Val 81
#define CREATOR_Photo_cell_Frequency 82
#define CREATOR_Photocell_unsteady_timeout_Val 83
#define CREATOR_Schedule_offset 84
#define CREATOR_Motion_Detect_Timeout 85
#define CREATOR_Motion_Broadcast_Timeout 86
#define CREATOR_Motion_Sensor_Type 87
#define CREATOR_Slc_reset_counter 88
#define CREATOR_SLC_Start_netwrok_Search_Timeout 89
#define CREATOR_Network_Scan_Cnt_Time_out 90
#define CREATOR_Commissioning_flag      91
#define CREATOR_Enable_Security 92
#define CREATOR_GPS_Read_Enable 93
#define CREATOR_Send_GPS_Data_at_valid_Dcu 94
#define CREATOR_GPS_Wake_timer 95
#define CREATOR_No_of_setalite_threshold 96		
#define CREATOR_Setalite_angle_threshold 97
#define CREATOR_setalite_Rssi_threshold 98
#define CREATOR_Maximum_no_of_Setalite_reading 99
#define CREATOR_HDOP_Gps_threshold 100
#define CREATOR_Check_Sbas_Enable 101
#define CREATOR_Sbas_setalite_Rssi_threshold 102
#define CREATOR_Auto_Gps_Data_Send 103
#define CREATOR_previous_mode 104
#define CREATOR_RTC_New_fault_logic_Enable 105
#define CREATOR_Photo_Cell_Ok 106
#define CREATOR_Photo_Cell_error 107

#ifdef NEW_LAMP_FAULT_LOGIC
  #define CREATOR_KW_Threshold         108
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

#define CREATOR_Vr_Cal 109
#define CREATOR_Ir_Cal 110
#define CREATOR_KW_Cal 111

//
//
#define CREATOR_Slot_Start_Date 112
#define CREATOR_Slot_Start_Month 113
#define CREATOR_Slot_Start_Year 114

#define CREATOR_Slot_End_Date 115
#define CREATOR_Slot_End_Month 116
#define CREATOR_Slot_End_Year 117
#define CREATOR_Slot_Start_Hour 118
#define CREATOR_Slot_Start_Min 119
#define CREATOR_Slot_End_Hour  120
#define CREATOR_Slot_End_Min 121
#define CREATOR_Save_data_slot 122
#define CREATOR_Save_Energy_In_Slot 123

#define CREATOR_Slot_Pulses 124

#define CREATOR_Photosensor_Set_FC 125
#define CREATOR_Photosensor_Set_FC_Lamp_OFF 126
//#define CREATOR_Motion_group_id 127

#define CREATOR_CURV_TYPE 127
#define CREATOR_Motion_group_id_32 128
#define CREATOR_Motion_grp_Select_32 129
#define CREATOR_KW_Cal_120 130

//////////////////////idimmer end//////////////////////////

#ifdef DEFINETYPES

typedef unsigned char tokTypeautomode;         // 1
typedef unsigned char tokTypetemp;             // 2
typedef unsigned int tokTypeamr_id ;           // 6
typedef float tokTypeLatitude;                 // 10
typedef float tokTypelongitude;                // 14
typedef float tokTypeutcOffset;                // 18
typedef short int tokTypeSunset_delay;         // 20
typedef short int tokTypeSunrise_delay;        // 22
typedef unsigned  long int tokTypepulses_r;    // 26
typedef unsigned long int tokTypelamp_burn_hour;           // 30
typedef float tokTypelamp_current;             // 34
typedef unsigned char tokTypedetect_lamp_current;  // 35
typedef unsigned int tokTypeVol_hi;                // 39
typedef unsigned int tokTypeVol_low;               // 43
typedef unsigned int tokTypeCurr_Steady_Time;      // 47
typedef unsigned int tokTypePer_Val_Current;       // 51
typedef unsigned int tokTypeLamp_Fault_Time;       // 55
typedef unsigned int tokTypeLamp_off_on_Time;      // 59
typedef unsigned int tokTypeLamp_faulty_retrieve_Count; // 63
typedef unsigned int tokTypeBroadcast_time_delay;       // 67
typedef unsigned int tokTypeUnicast_time_delay;         // 71
typedef unsigned int tokTypeLamp_lock_condition;        // 75

//////////////Mix mode///////////////////////////
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_1[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_2[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_3[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_4[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_5[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_6[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_7[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_8[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_9[61];
typedef unsigned char tokTypeSlc_Mix_Mode_schedule_10[61];
//typedef unsigned char tokTypeSlc_Mix_Mode_schedule_11[61];
//typedef unsigned char tokTypeSlc_Mix_Mode_schedule_12[61];
//typedef unsigned char tokTypeSlc_Mix_Mode_schedule_13[61];
//typedef unsigned char tokTypeSlc_Mix_Mode_schedule_14[61];
//typedef unsigned char tokTypeSlc_Mix_Mode_schedule_15[61];

//typedef struct //Slc_M_Sch
//{
//	unsigned char SLC_Mix_En;
//	unsigned char SLC_Mix_Sch_Start_Date;
//	unsigned char SLC_Mix_Sch_Start_Month;
//	unsigned char SLC_Mix_Sch_Stop_Date;
//	unsigned char SLC_Mix_Sch_Stop_Month;
//	unsigned char SLC_Mix_Sch_WeekDay;
//	unsigned char SLC_Mix_Sch_Photocell_Override;	
//	struct M_Schedule Slc_Mix_Sch[9];
//};

typedef unsigned char tokTypeSlot_Start_Date[2];
typedef unsigned char tokTypeSlot_Start_Month[2];
typedef unsigned char tokTypeSlot_Start_Year[2];
//
typedef unsigned char tokTypeSlot_End_Date[2];
typedef unsigned char tokTypeSlot_End_Month[2];
typedef unsigned char tokTypeSlot_End_Year[2];
typedef unsigned char tokTypeSlot_Start_Hour[12];
typedef unsigned char tokTypeSlot_Start_Min[12];
typedef unsigned char tokTypeSlot_End_Hour[12];
typedef unsigned char tokTypeSlot_End_Min[12];
//
typedef unsigned char tokTypeSave_Energy_In_Slot[12];
typedef unsigned char tokTypeSave_data_slot[12];

typedef unsigned long int tokTypeSlot_Pulses[12];

//////////////////////////////////////////
typedef struct
{
        unsigned char	emb_bSchFlag[10]; //[50]
	unsigned char 	emb_cStartHour[10];
	unsigned char	emb_cStartMin[10];
	unsigned char 	emb_cStopHour[10];
	unsigned char	emb_cStopMin[10];
}tokTypeschedule;
//typedef struct
//{
//        unsigned char	emb_bSchFlag67[6];//[30]
//	unsigned char 	emb_cStartHour67[6];
//	unsigned char	emb_cStartMin67[6];
//	unsigned char 	emb_cStopHour67[6];
//	unsigned char	emb_cStopMin67[6];
//}tokTypeschedule67;
typedef struct
{
        unsigned char	emb_bSchFlagMaster[10]; //[50]
	unsigned char 	emb_cStartHourMaster[10];
	unsigned char	emb_cStartMinMaster[10];
	unsigned char 	emb_cStopHourMaster[10];
	unsigned char	emb_cStopMinMaster[10];
        unsigned char   emb_dimvalue[10];
}tokTypeMaster_schedule;
//
//typedef struct
//{
//        unsigned char	emb_bSchFlagMaster67[6]; //[30
//	unsigned char 	emb_cStartHourMaster67[6];
//	unsigned char	emb_cStartMinMaster67[6];
//	unsigned char 	emb_cStopHourMaster67[6];
//	unsigned char	emb_cStopMinMaster67[6];
//}tokTypeMaster_schedule67;
typedef struct
{
        unsigned char	emb_bSchFlag_Astro_sSch[2]; //[10]
	unsigned char 	emb_cStartHour_Astro_sSch[2];
	unsigned char	emb_cStartMin_Astro_sSch[2];
	unsigned char 	emb_cStopHour_Astro_sSch[2];
	unsigned char	emb_cStopMin_Astro_sSch[2];
}tokTypeschedule_Astro_sSch;

typedef unsigned char tokTypeSingle_time_run_cmd;
typedef unsigned int tokTypescann_chan;
typedef unsigned int tokTypePanId;
typedef unsigned char tokTypeExtPanId_set[10];
typedef unsigned char tokTypesch_day[10];
typedef unsigned char tokTypeMaster_sch_day[10];
typedef unsigned char tokTypeadaptive_light_dimming;
typedef unsigned int tokTypeanalog_input_scaling_high_Value;
typedef unsigned int tokTypeanalog_input_scaling_low_Value;
typedef unsigned int tokTypedesir_lamp_lumen;
typedef unsigned char tokTypelumen_tollarence;
typedef unsigned char tokTypeballast_type;

typedef unsigned char tokTypeMotion_pulse_rate;               // 1156
typedef unsigned char tokTypeMotion_dimming_percentage;       // 1157
typedef unsigned int tokTypeMotion_dimming_time;              // 1161
typedef unsigned char tokTypeMotion_group_id;                 // 1162
typedef unsigned int tokTypeMotion_group_id_32;
typedef unsigned char tokTypeMotion_grp_Select_32;
typedef unsigned int tokTypedim_applay_time;                  // 1166
typedef unsigned char tokTypedim_inc_val;                        // 1167
typedef unsigned char tokTypeMotion_normal_dimming_percentage;   // 1168
typedef unsigned char tokTypeValid_DCU_Flag;                     // 1169
typedef unsigned char tokTypeDefault_Para_Set;                   // 1162
typedef unsigned char tokTypeidimmer_en;                         // 1163
typedef unsigned int tokTypeidimmer_id;                          // 1167
typedef unsigned char tokTypeidimmer_longaddress[10];            // 1177
typedef unsigned char tokTypeSLC_DST_En ;                        // 1178
typedef unsigned char tokTypeSLC_DST_Start_Rule;                 // 1179
typedef unsigned char tokTypeSLC_DST_Start_Month;                // 1180
typedef unsigned char tokTypeSLC_DST_Start_Time;                 // 1181
typedef unsigned char tokTypeSLC_DST_Stop_Rule;                  // 1182
typedef unsigned char tokTypeSLC_DST_Stop_Month;                 // 1183
typedef unsigned char tokTypeSLC_DST_Stop_Time;                  // 1184

typedef  float tokTypeSLC_DST_Time_Zone_Diff;                    // 1188
typedef unsigned char tokTypeTime_change_dueto_DST;              // 1189
typedef unsigned char tokTypeSLC_DST_Rule_Enable;                // 1190
typedef unsigned char tokTypeSLC_DST_Start_Date;                 // 1191
typedef unsigned char tokTypeSLC_DST_Stop_Date;                  // 1192
typedef unsigned char tokTypeAuto_event;                         // 1193
typedef signed short int tokTypeDay_light_harvesting_start_offset;  // 1195
typedef signed short int tokTypeDay_light_harvesting_stop_offset;   // 1197
typedef unsigned char tokTypeLamp_type ;                            // 1198

typedef unsigned char tokTypeSLC_New_Manula_Mode_Val;               // 1199
typedef unsigned char tokTypeTLink_Key[20];                         // 1219
typedef unsigned int tokTypeSLC_New_Manual_Mode_Timer;

typedef unsigned char tokTypePhotocell_steady_timeout_Val;          // 1220
typedef unsigned char tokTypephoto_cell_toggel_counter_Val; 	       // 1221
typedef unsigned char tokTypephoto_cell_toggel_timer_Val;           // 1222
typedef unsigned char tokTypePhoto_cell_Frequency;                  // 1223
typedef unsigned int  tokTypePhotocell_unsteady_timeout_Val;        // 1227
typedef unsigned char tokTypeSchedule_offset;                       // 1228
typedef unsigned char tokTypeMotion_Detect_Timeout;                 // 1229
typedef unsigned char tokTypeMotion_Broadcast_Timeout;              // 1230
typedef unsigned char tokTypeMotion_Sensor_Type;                    // 1231
typedef unsigned int  tokTypeSlc_reset_counter;                      // 1235
typedef unsigned char tokTypeSLC_Start_netwrok_Search_Timeout;       // 1236
typedef unsigned long tokTypeNetwork_Scan_Cnt_Time_out;          // 1240
typedef unsigned char tokTypeCommissioning_flag;
typedef unsigned char tokTypeEnable_Security;
typedef unsigned char tokTypeGPS_Read_Enable;
typedef unsigned char tokTypeSend_GPS_Data_at_valid_Dcu;
typedef unsigned int  tokTypeGPS_Wake_timer;
#ifdef NEW_LAMP_FAULT_LOGIC
  typedef float tokTypeKW_Threshold;
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

typedef float tokTypeVr_Cal;
typedef float tokTypeIr_Cal;
typedef float tokTypeKW_Cal;
typedef float tokTypeKW_Cal_120;

typedef unsigned char tokTypeNo_of_setalite_threshold;
typedef unsigned char tokTypeSetalite_angle_threshold;
typedef unsigned char tokTypesetalite_Rssi_threshold;
typedef unsigned char tokTypeMaximum_no_of_Setalite_reading;
typedef float tokTypeHDOP_Gps_threshold;
typedef unsigned char tokTypeCheck_Sbas_Enable;
typedef unsigned char tokTypeSbas_setalite_Rssi_threshold;
typedef unsigned char tokTypeAuto_Gps_Data_Send;
typedef unsigned char tokTypeprevious_mode;
typedef unsigned char tokTypeRTC_New_fault_logic_Enable;
typedef unsigned char tokTypePhoto_Cell_Ok;
typedef unsigned char tokTypePhoto_Cell_error;

typedef float tokTypePhotosensor_Set_FC;
typedef float tokTypePhotosensor_Set_FC_Lamp_OFF;
typedef unsigned char tokTypeCURV_TYPE;

#endif //DEFINETYPES
#ifdef DEFINETOKENS
#define automode_LOCATION 1
#define temp_LOCATION     2
#define amr_id_LOCATION   3
#define Latitude_LOCATION 4
#define longitude_LOCATION 5
#define utcOffset_LOCATION 6
#define Sunset_delay_LOCATION 7
#define Sunrise_delay_LOCATION 8
#define pulses_r_LOCATION 9
#define lamp_burn_hour_LOCATION 10
#define lamp_current_LOCATION 11
#define detect_lamp_current_LOCATION 12
#define Vol_hi_LOCATION 13
#define Vol_low_LOCATION 14

#define Curr_Steady_Time_LOCATION 15
#define Per_Val_Current_LOCATION  16
#define Lamp_Fault_Time_LOCATION  17
#define Lamp_off_on_Time_LOCATION 18
#define Lamp_faulty_retrieve_Count_LOCATION 19
#define Broadcast_time_delay_LOCATION  20
#define Unicast_time_delay_LOCATION   21
#define Lamp_lock_condition_LOCATION  22
#define schedule_LOCATION 23
#define Single_time_run_cmd_LOCATION  24
#define scann_chan_LOCATION 25
#define PanId_LOCATION 26
#define ExtPanId_set_LOCATION 27
//#define schedule67_LOCATION  28
#define sch_day_LOCATION  28
#define Master_schedule_LOCATION 29
//#define Master_schedule67_LOCATION 31
#define Master_sch_day_LOCATION 30
#define schedule_Astro_sSch_LOCATION 31
#define adaptive_light_dimming_LOCATION 32
#define analog_input_scaling_high_Value_LOCATION 33
#define analog_input_scaling_low_Value_LOCATION 34
#define desir_lamp_lumen_LOCATION 35
#define lumen_tollarence_LOCATION 36
#define ballast_type_LOCATION 37

#define Motion_pulse_rate_LOCATION 38
#define Motion_dimming_percentage_LOCATION 39
#define Motion_dimming_time_LOCATION 40
#define Motion_group_id_LOCATION 41
#define dim_applay_time_LOCATION 42
#define dim_inc_val_LOCATION 43
#define Motion_normal_dimming_percentage_LOCATION 44
#define Valid_DCU_Flag_LOCATION 45
#define Default_Para_Set_LOCATION 46

#define Slc_Mix_Mode_schedule_1_LOCATION 47
#define Slc_Mix_Mode_schedule_2_LOCATION 48
#define Slc_Mix_Mode_schedule_3_LOCATION 49
#define Slc_Mix_Mode_schedule_4_LOCATION 50
#define Slc_Mix_Mode_schedule_5_LOCATION 51
#define Slc_Mix_Mode_schedule_6_LOCATION 52
#define Slc_Mix_Mode_schedule_7_LOCATION 53
#define Slc_Mix_Mode_schedule_8_LOCATION 54
#define Slc_Mix_Mode_schedule_9_LOCATION 55
#define Slc_Mix_Mode_schedule_10_LOCATION 56
//#define Slc_Mix_Mode_schedule_11_LOCATION 57
//#define Slc_Mix_Mode_schedule_12_LOCATION 58
//#define Slc_Mix_Mode_schedule_13_LOCATION 59
//#define Slc_Mix_Mode_schedule_14_LOCATION 60
//#define Slc_Mix_Mode_schedule_15_LOCATION 61
#define idimmer_en_LOCATION 57
#define idimmer_id_LOCAION 58
#define idimmer_longaddress_LOCATION 59
///////////////////////////////////////
#define SLC_DST_En_LOCATION 60
#define SLC_DST_Start_Rule_LOCATION 61
#define SLC_DST_Start_Month_LOCATION 62
#define SLC_DST_Start_Time_LOCATION 63
#define SLC_DST_Stop_Rule_LOCATION 64
#define SLC_DST_Stop_Month_LOCATION 65
#define SLC_DST_Stop_Time_LOCATION 66

#define SLC_DST_Time_Zone_Diff_LOCATION 67
#define Time_change_dueto_DST_LOCATION  68
#define SLC_DST_Rule_Enable_LOCATION  69
#define SLC_DST_Start_Date_LOCATION  70
#define SLC_DST_Stop_Date_LOCATION 71
#define Auto_event_LOCATION 72
#define Day_light_harvesting_start_offset_LOCATION 73
#define Day_light_harvesting_stop_offset_LOCATION 74
#define Lamp_type_LOCATION 75
#define SLC_New_Manula_Mode_Val_LOCATION 76
#define TLink_Key_LOCATION 77
#define SLC_New_Manual_Mode_Timer_LOCATION 78

#define Photocell_steady_timeout_Val_LOCATION 79
#define photo_cell_toggel_counter_Val_LOCATION 80	
#define photo_cell_toggel_timer_Val_LOCATION 81
#define Photo_cell_Frequency_LOCATION 82
#define Photocell_unsteady_timeout_Val_LOCATION 83
#define Schedule_offset_LOCATION 84
#define Motion_Detect_Timeout_LOCATION 85
#define Motion_Broadcast_Timeout_LOCATION 86
#define Motion_Sensor_Type_LOCATION 87
#define Slc_reset_counter_LOCATION 88
#define SLC_Start_netwrok_Search_Timeout_LOCATION 89
#define Network_Scan_Cnt_Time_out_LOCATION 90
#define Commissioning_flag_LOCATION 91
#define Enable_Security_LOCATION  92
#define GPS_Read_Enable_LOCATION  93
#define Send_GPS_Data_at_valid_Dcu_LOCATION  94
#define GPS_Wake_timer_LOCATION  95

//#define previous_mode_LOCATION 96
#define No_of_setalite_threshold_LOCATION 96
#define Setalite_angle_threshold_LOCATION 97
#define setalite_Rssi_threshold_LOCATION 98
#define Maximum_no_of_Setalite_reading_LOCATION 99
#define HDOP_Gps_threshold_LOCATION 100
#define Check_Sbas_Enable_LOCATION 101
#define Sbas_setalite_Rssi_threshold_LOCATION 102
#define Auto_Gps_Data_Send_LOCATION 103
#define previous_mode_LOCATION 104
#define RTC_New_fault_logic_Enable_LOCATION 105
#define Photo_Cell_Ok_LOCATION 106
#define Photo_Cell_error_LOCATION 107


#ifdef NEW_LAMP_FAULT_LOGIC
  #define KW_Threshold_LOCATION 108
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

 #define Vr_Cal_LOCATION 109
  #define Ir_Cal_LOCATION 110
  #define KW_Cal_LOCATION 111

#define Slot_Start_Date_LOCATION 112


#define Slot_Start_Month_LOCATION 113
#define Slot_Start_Year_LOCATION 114
//
#define Slot_End_Date_LOCATION 115
#define Slot_End_Month_LOCATION 116
#define Slot_End_Year_LOCATION 117
#define Slot_Start_Hour_LOCATION 118
#define Slot_Start_Min_LOCATION 119
#define Slot_End_Hour_LOCATION  120
#define Slot_End_Min_LOCATION 121
//
#define Save_data_slot_LOCATION 122

#define Save_Energy_In_Slot_LOCATION 123
#define Slot_Pulses_LOCATION 124

#define Photosensor_Set_FC_LOCATION 125
#define Photosensor_Set_FC_Lamp_OFF_LOCATION 126

#define CURV_TYPE_LOCATION 127

#define Motion_group_id_32_LOCATION 128
#define Motion_grp_Select_32_LOCATION 129
#define KW_Cal_120_LOCATION 130

////////////Mix mode////////////////////////
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_1, tokTypeSlc_Mix_Mode_schedule_1, Slc_Mix_Mode_schedule_1_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_2, tokTypeSlc_Mix_Mode_schedule_2, Slc_Mix_Mode_schedule_2_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_3, tokTypeSlc_Mix_Mode_schedule_3, Slc_Mix_Mode_schedule_3_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_4, tokTypeSlc_Mix_Mode_schedule_4, Slc_Mix_Mode_schedule_4_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_5, tokTypeSlc_Mix_Mode_schedule_5, Slc_Mix_Mode_schedule_5_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_6, tokTypeSlc_Mix_Mode_schedule_6, Slc_Mix_Mode_schedule_6_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_7, tokTypeSlc_Mix_Mode_schedule_7, Slc_Mix_Mode_schedule_7_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_8, tokTypeSlc_Mix_Mode_schedule_8, Slc_Mix_Mode_schedule_8_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_9, tokTypeSlc_Mix_Mode_schedule_9, Slc_Mix_Mode_schedule_9_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_10, tokTypeSlc_Mix_Mode_schedule_10, Slc_Mix_Mode_schedule_10_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_11, tokTypeSlc_Mix_Mode_schedule_11, Slc_Mix_Mode_schedule_11_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_12, tokTypeSlc_Mix_Mode_schedule_12, Slc_Mix_Mode_schedule_12_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_13, tokTypeSlc_Mix_Mode_schedule_13, Slc_Mix_Mode_schedule_13_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_14, tokTypeSlc_Mix_Mode_schedule_14, Slc_Mix_Mode_schedule_14_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(Slc_Mix_Mode_schedule_15, tokTypeSlc_Mix_Mode_schedule_15, Slc_Mix_Mode_schedule_15_LOCATION, 0x0000)

/////////////////////////////////////
DEFINE_FIXED_BASIC_TOKEN(automode, tokTypeautomode, automode_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(temp, tokTypetemp, temp_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(amr_id , tokTypeamr_id , amr_id _LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Latitude, tokTypeLatitude, Latitude_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(longitude, tokTypelongitude, longitude_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(utcOffset, tokTypeutcOffset, utcOffset_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Sunset_delay, tokTypeSunset_delay, Sunset_delay_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Sunrise_delay, tokTypeSunrise_delay, Sunrise_delay_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(pulses_r, tokTypepulses_r, pulses_r_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(lamp_burn_hour, tokTypelamp_burn_hour, lamp_burn_hour_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(lamp_current, tokTypelamp_current, lamp_current_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(detect_lamp_current, tokTypedetect_lamp_current, detect_lamp_current_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Vol_hi, tokTypeVol_hi, Vol_hi_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Vol_low, tokTypeVol_low, Vol_low_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Curr_Steady_Time, tokTypeCurr_Steady_Time, Curr_Steady_Time_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Per_Val_Current, tokTypePer_Val_Current, Per_Val_Current_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Lamp_Fault_Time, tokTypeLamp_Fault_Time, Lamp_Fault_Time_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Lamp_off_on_Time, tokTypeLamp_off_on_Time, Lamp_off_on_Time_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Lamp_faulty_retrieve_Count, tokTypeLamp_faulty_retrieve_Count, Lamp_faulty_retrieve_Count_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Broadcast_time_delay, tokTypeBroadcast_time_delay, Broadcast_time_delay_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Unicast_time_delay, tokTypeUnicast_time_delay, Unicast_time_delay_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Lamp_lock_condition, tokTypeLamp_lock_condition, Lamp_lock_condition_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(schedule, tokTypeschedule, schedule_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Single_time_run_cmd, tokTypeSingle_time_run_cmd, Single_time_run_cmd_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(scann_chan, tokTypescann_chan, scann_chan_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(PanId, tokTypePanId, PanId_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(ExtPanId_set, tokTypeExtPanId_set, ExtPanId_set_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(schedule67, tokTypeschedule67, schedule67_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(sch_day, tokTypesch_day, sch_day_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Master_schedule, tokTypeMaster_schedule, Master_schedule_LOCATION, 0x0000)
//DEFINE_FIXED_BASIC_TOKEN(Master_schedule67, tokTypeMaster_schedule67, Master_schedule67_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Master_sch_day, tokTypeMaster_sch_day, Master_sch_day_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(schedule_Astro_sSch, tokTypeschedule_Astro_sSch, schedule_Astro_sSch_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(adaptive_light_dimming, tokTypeadaptive_light_dimming, adaptive_light_dimming_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(analog_input_scaling_high_Value, tokTypeanalog_input_scaling_high_Value, analog_input_scaling_high_Value_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(analog_input_scaling_low_Value, tokTypeanalog_input_scaling_low_Value, analog_input_scaling_low_Value_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(desir_lamp_lumen, tokTypedesir_lamp_lumen, desir_lamp_lumen_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(lumen_tollarence, tokTypelumen_tollarence, lumen_tollarence_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(ballast_type, tokTypeballast_type, ballast_type_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Motion_pulse_rate, tokTypeMotion_pulse_rate, Motion_pulse_rate_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Motion_dimming_percentage, tokTypeMotion_dimming_percentage, Motion_dimming_percentage_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Motion_dimming_time, tokTypeMotion_dimming_time, Motion_dimming_time_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Motion_group_id, tokTypeMotion_group_id, Motion_group_id_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(dim_applay_time, tokTypedim_applay_time, dim_applay_time_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(dim_inc_val, tokTypedim_inc_val, dim_inc_val_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Motion_normal_dimming_percentage, tokTypeMotion_normal_dimming_percentage, Motion_normal_dimming_percentage_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Valid_DCU_Flag, tokTypeValid_DCU_Flag, Valid_DCU_Flag_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Default_Para_Set, tokTypeDefault_Para_Set, Default_Para_Set_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(idimmer_en, tokTypeidimmer_en, idimmer_en_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(idimmer_id, tokTypeidimmer_id, idimmer_id_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(idimmer_longaddress, tokTypeidimmer_longaddress, idimmer_longaddress_LOCATION, 0x0000)

///////////////////////////////////////////////////
DEFINE_FIXED_BASIC_TOKEN(SLC_DST_En,tokTypeSLC_DST_En, SLC_DST_En_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Start_Rule, tokTypeSLC_DST_Start_Rule, SLC_DST_Start_Rule_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Start_Month,tokTypeSLC_DST_Start_Month, SLC_DST_Start_Month_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Start_Time,tokTypeSLC_DST_Start_Time, SLC_DST_Start_Time_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Stop_Rule,tokTypeSLC_DST_Stop_Rule, SLC_DST_Stop_Rule_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Stop_Month,tokTypeSLC_DST_Stop_Month, SLC_DST_Stop_Month_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Stop_Time,tokTypeSLC_DST_Stop_Time, SLC_DST_Stop_Time_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Time_Zone_Diff,tokTypeSLC_DST_Time_Zone_Diff, SLC_DST_Time_Zone_Diff_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Time_change_dueto_DST,tokTypeTime_change_dueto_DST, Time_change_dueto_DST_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Rule_Enable,tokTypeSLC_DST_Rule_Enable, SLC_DST_Rule_Enable_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Start_Date,tokTypeSLC_DST_Start_Date, SLC_DST_Start_Date_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(SLC_DST_Stop_Date,tokTypeSLC_DST_Stop_Date, SLC_DST_Stop_Date_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Auto_event,tokTypeAuto_event, Auto_event_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Day_light_harvesting_start_offset,tokTypeDay_light_harvesting_start_offset, Day_light_harvesting_start_offset_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Day_light_harvesting_stop_offset,tokTypeDay_light_harvesting_stop_offset, Day_light_harvesting_stop_offset_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Lamp_type,tokTypeLamp_type,Lamp_type_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_New_Manula_Mode_Val,tokTypeSLC_New_Manula_Mode_Val,SLC_New_Manula_Mode_Val_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(TLink_Key,tokTypeTLink_Key,TLink_Key_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_New_Manual_Mode_Timer,tokTypeSLC_New_Manual_Mode_Timer,SLC_New_Manual_Mode_Timer_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Photocell_steady_timeout_Val,tokTypePhotocell_steady_timeout_Val,Photocell_steady_timeout_Val_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(photo_cell_toggel_counter_Val,tokTypephoto_cell_toggel_counter_Val,photo_cell_toggel_counter_Val_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(photo_cell_toggel_timer_Val,tokTypephoto_cell_toggel_timer_Val,photo_cell_toggel_timer_Val_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Photo_cell_Frequency,tokTypePhoto_cell_Frequency,Photo_cell_Frequency_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Photocell_unsteady_timeout_Val,tokTypePhotocell_unsteady_timeout_Val,Photocell_unsteady_timeout_Val_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Schedule_offset,tokTypeSchedule_offset,Schedule_offset_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Motion_Detect_Timeout,tokTypeMotion_Detect_Timeout,Motion_Detect_Timeout_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Motion_Broadcast_Timeout,tokTypeMotion_Broadcast_Timeout,Motion_Broadcast_Timeout_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Motion_Sensor_Type,tokTypeMotion_Sensor_Type,Motion_Sensor_Type_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Slc_reset_counter,tokTypeSlc_reset_counter,Slc_reset_counter_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(SLC_Start_netwrok_Search_Timeout,tokTypeSLC_Start_netwrok_Search_Timeout,SLC_Start_netwrok_Search_Timeout_LOCATION, 0x0000)  	
DEFINE_FIXED_BASIC_TOKEN(Network_Scan_Cnt_Time_out,tokTypeNetwork_Scan_Cnt_Time_out,Network_Scan_Cnt_Time_out_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Commissioning_flag,tokTypeCommissioning_flag,Commissioning_flag_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Enable_Security,tokTypeEnable_Security,Enable_Security_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(GPS_Read_Enable,tokTypeGPS_Read_Enable,GPS_Read_Enable_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Send_GPS_Data_at_valid_Dcu,tokTypeSend_GPS_Data_at_valid_Dcu,Send_GPS_Data_at_valid_Dcu_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(GPS_Wake_timer,tokTypeGPS_Wake_timer,GPS_Wake_timer_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(previous_mode,tokTypeprevious_mode,previous_mode_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(RTC_New_fault_logic_Enable,tokTypeRTC_New_fault_logic_Enable,RTC_New_fault_logic_Enable_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Photo_Cell_Ok,tokTypePhoto_Cell_Ok,Photo_Cell_Ok_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Photo_Cell_error,tokTypePhoto_Cell_error,Photo_Cell_error_LOCATION, 0x0000)

#ifdef NEW_LAMP_FAULT_LOGIC
  DEFINE_FIXED_BASIC_TOKEN(KW_Threshold,tokTypeKW_Threshold,KW_Threshold_LOCATION, 0x0000)
#elif defined(OLD_LAMP_FAULT_LOGIC)

#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif
DEFINE_FIXED_BASIC_TOKEN(Vr_Cal,tokTypeVr_Cal,Vr_Cal_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Ir_Cal,tokTypeIr_Cal,Ir_Cal_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(KW_Cal,tokTypeKW_Cal,KW_Cal_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(No_of_setalite_threshold,tokTypeNo_of_setalite_threshold,No_of_setalite_threshold_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Setalite_angle_threshold,tokTypeSetalite_angle_threshold,Setalite_angle_threshold_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(setalite_Rssi_threshold,tokTypesetalite_Rssi_threshold,setalite_Rssi_threshold_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Maximum_no_of_Setalite_reading,tokTypeMaximum_no_of_Setalite_reading,Maximum_no_of_Setalite_reading_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(HDOP_Gps_threshold,tokTypeHDOP_Gps_threshold,HDOP_Gps_threshold_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Check_Sbas_Enable,tokTypeCheck_Sbas_Enable,Check_Sbas_Enable_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Sbas_setalite_Rssi_threshold,tokTypeSbas_setalite_Rssi_threshold,Sbas_setalite_Rssi_threshold_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Auto_Gps_Data_Send,tokTypeAuto_Gps_Data_Send,Auto_Gps_Data_Send_LOCATION, 0x0000)


////////////////
////////////////
DEFINE_FIXED_BASIC_TOKEN(KW_Cal_120,tokTypeKW_Cal_120,KW_Cal_LOCATION_120, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_Start_Date,tokTypeSlot_Start_Date, Slot_Start_Date_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_Start_Month,tokTypeSlot_Start_Month, Slot_Start_Month_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_Start_Year,tokTypeSlot_Start_Year, Slot_Start_Year_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_End_Date,tokTypeSlot_End_Date, Slot_End_Date_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_End_Month,tokTypeSlot_End_Month, Slot_End_Month_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_End_Year,tokTypeSlot_End_Year, Slot_End_Year_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_Start_Hour,tokTypeSlot_Start_Hour, Slot_Start_Hour_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_Start_Min,tokTypeSlot_Start_Min, Slot_Start_Min_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_End_Hour,tokTypeSlot_End_Hour, Slot_End_Hour_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_End_Min,tokTypeSlot_End_Min, Slot_End_Min_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Save_Energy_In_Slot,tokTypeSave_Energy_In_Slot, Save_Energy_In_Slot_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Save_data_slot,tokTypeSave_data_slot, Save_data_slot_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Slot_Pulses,tokTypeSlot_Pulses, Slot_Pulses_LOCAION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Photosensor_Set_FC,tokTypePhotosensor_Set_FC, Photosensor_Set_FC_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Photosensor_Set_FC_Lamp_OFF,tokTypePhotosensor_Set_FC_Lamp_OFF, Photosensor_Set_FC_Lamp_OFF_LOCATION, 0x0000)

DEFINE_FIXED_BASIC_TOKEN(Motion_group_id_32, tokTypeMotion_group_id_32, Motion_group_id_32_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(Motion_grp_Select_32, tokTypeMotion_grp_Select_32, Motion_grp_Select_32_LOCATION, 0x0000)
DEFINE_FIXED_BASIC_TOKEN(CURV_TYPE,tokTypeCURV_TYPE, CURV_TYPE_LOCATION, 0x0000)
#endif //DEFINETOKENS

#else //SIM_EEPROM_TEST

  //The Simulated EEPROM unit tests define all of their tokens via the
  //APPLICATION_TOKEN_HEADER macro.
  #ifdef APPLICATION_TOKEN_HEADER
    #include APPLICATION_TOKEN_HEADER
  #endif

#endif //SIM_EEPROM_TEST

#ifndef DEFINEADDRESSES
  #undef TOKEN_NEXT_ADDRESS
#endif

/** @} END addtogroup */

/**
 * <!-- HIDDEN
 * @page 2p5_to_3p0
 * <hr>
 * The file token-stack.h is described in @ref token_stack and includes
 * the following:
 * <ul>
 * <li> <b>New items</b>
 *   - ::CREATOR_STACK_ALTERNATE_KEY
 *   - ::CREATOR_STACK_APS_FRAME_COUNTER
 *   - ::CREATOR_STACK_LINK_KEY_TABLE
 *   .
 * <li> <b>Changed items</b>
 *   -
 *   -
 *   .
 * <li> <b>Removed items</b>
 *   - ::CREATOR_STACK_DISCOVERY_CACHE
 *   - ::CREATOR_STACK_APS_INDIRECT_BINDING_TABLE
 *   .
 * </ul>
 * HIDDEN -->
 */
