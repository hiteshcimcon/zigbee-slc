

#ifndef SERIAL_H
#define SERIAL_H

#define BAUD_9600	155					//25
//#define BAUD_115200	51					//25

//#define B_SIZE_SERIAL_RF 200

	extern unsigned char Serial_RF_Buff[];
	extern unsigned char data_rec_RF;
	extern unsigned int temp_rx_RF;
	
	void ConfigIntUART2(unsigned int config);
	void Configure_UART2(void);
	void WriteData_UART2(unsigned char *dataPtr,unsigned int len);
	void Parse_Query(void);
	void WriteDebugMsg(unsigned char *dataPtr);

#endif



