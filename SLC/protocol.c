/****************************************protocol.c************************************/

# include <stdio.h>
# include <ctype.h>
# include <math.h>
# include <string.h>
# include <stdlib.h>


#include "generic.h"
#include "protocol.h"
#include "DS1339.h"
#include "Application_logic.h"
#include "AMR_cs5463.h"
#include "GPS.h"
#include "SLC_defination.h"
#include "app/framework/include/af.h"
#include "stack/include/ember-types.h"
////////////////////Calibration/////////////
//unsigned char Send_Data_Save_Kwh =0; 
unsigned char SLC_ID_Buf[6];
extern EmberNodeId serverNodeId;
extern EmberAfOtaImageId currentDownloadFile;
unsigned char otaClientEraseOtaFile =0;
unsigned char otaClientUpgradeNewFirmware =0;
unsigned char otaClientServerDiscoveryComplete = 0;
unsigned char otaClientImageQueryComplete = 0 ;
unsigned char otaClientDownloadPercentage = 0;
unsigned int otaClientNumberOfMissedOffsetBlock = 0; 
unsigned char otaClientDownLoadComplete = 0;
unsigned char otaClientLastByteMaskIndex = 0;
unsigned char otaClientAlreadyUpgraded = 0;
unsigned char otaClientVerificationComplete = 0;
unsigned char otaClientWaitForCommadToRetryMissedOffset=0;

extern unsigned char otaClientStart;
extern unsigned char Old_Hr;
BYTE_VAL Configured_Event,Configured_Event1;
unsigned char GET_AUTO_NETWORK_LEAVE_Flag =0,Network_leave_Flag =0;
unsigned int Time_Slice=5,Temp_Time_Slice=0;
unsigned char Auto_Send_Lograte_Interval=1,Auto_Send_Enable_Disable =0,Auto_Send_Retry =25,Read_Data_send_Timer =3;
unsigned char Send_Read_Scene_From_Dali=0,scenes_value_write =0;
unsigned char Time_Out_Flag=0,Time_Out_Check=60;
extern unsigned char Id_Frame_Cnt,Time_Out;
unsigned char Test_Config_Data_Send=0;
unsigned char Panic_Dim_Value =0;
unsigned char Lamp_ON_First_Responder =0;
float Lamp_Trigger_Time =0.0;
unsigned char Soft_Reboot =0;
extern unsigned char set_do_flag ;
extern unsigned char lamp_on_first;
extern unsigned char default_dali;
unsigned char Cmd_Rcv_To_Change_Curv_Ype =0;
extern unsigned char cheak_emt_timer_sec ;
unsigned int Motion_group_id_32 = 0;
unsigned char Motion_grp_Select_32 =0;
unsigned int Panic_Lamp_On_Cnt =0,Panic_Lamp_OFF_Cnt =0;
unsigned short int Panic_Cnt =0;
unsigned short int SLC_Find_For_Panic =0;
unsigned int Lamp_On_Time =0,Lamp_OFF_Time =0;
unsigned short int Panic_Time_Out =0;
unsigned char Total_Panic_SLC =0;
extern unsigned char DI_GET_LOW,DI_GET_HIGH ;
extern unsigned int Tilt_counter;
extern float Calculated_Frequency_row;
unsigned char Get_calibration_data,send_to_Router;
unsigned char Read_KWH_Value;
unsigned char Read_Em_Calibration = 0;
unsigned char Send_Kw_calibration = 0,Send_Kw_calibration_120 =0;;
unsigned char KW_Calibration = 0,KW_Calibration_120 =0;
unsigned char Current_calibration = 0;
unsigned char Send_Current_calibration = 0;
unsigned char Send_Voltage_calibration = 0;
unsigned char Voltage_calibration = 0;
///////////////////////////////////////////
unsigned char Send_curv_type =0;
unsigned char uChar_GET_FOOT_CANDLE = 0;
unsigned char no_of_Sch=0,Get_Sch_no =0;
unsigned char Send_Time_Of_Use_Sch =0;
unsigned char Send_Kwh_Slot_To_Dcu =0;
//TimeOfUse Time_Slot;
unsigned char Link_Key[20] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA,0xBB,0xCC,0xDD,0xEE,0xFF};
unsigned char change_Link_key_timer = 0;
unsigned char change_Link_key = 0;
unsigned char Rtc_set_Replay,Get_rtc_Replay,Send_Data,Send_Data_autosend;
unsigned int Frm_no = 1;
unsigned int amr_id =0;
unsigned char Valid_DCU_Flag =0;
unsigned char Eep_Ers = 0;
unsigned char Send_COUNT = 0;
unsigned char send_id_frame = 0;
unsigned char Id_frame_received = 0;
unsigned long int lamp_burn_hour = 0;
unsigned char connection_ok = 0;
unsigned char Sch_receive = 0,DO_receive=0,auto_receive=0;
ALLTYPES pulseCounter;				
unsigned long int Rf_Track_Id = 0;
unsigned long int ack_Rf_Track_Id = 0,Auto_ack_Rf_Track_Id=0;
unsigned char temp_data_buff[20][40];
unsigned char temp_data_retrive_counter[20];
unsigned char Send_Link_key = 0;
unsigned char send_rf_event=0;					
unsigned char no_of_pending_event = 0;
unsigned char broadcast_read_req = 0;
unsigned char stop_event = 0;
unsigned char read_event_flag = 0;
unsigned char send_get_mode_replay = 0;
unsigned char send_get_mode_replay_auto = 0;
unsigned char Send_Fault_Para_Replay=0;
unsigned char Send_Slc_Diagnosis=0;
unsigned char cheak_emt_timer=0;
unsigned char Diagnosis_DI=0;
unsigned char check_peripheral = 0;
unsigned char Send_Network_Para_Replay=0;
unsigned char Send_Get_Sch_Replay=0;
unsigned int SLC_New_Manual_Mode_Timer = 0;
unsigned long int SLC_New_Manual_Mode_counter = 0;
short int Sunset_delay=0;
short int Sunrise_delay=0;
unsigned int Broadcast_time_delay=0;
unsigned int Unicast_time_delay=0;
unsigned char delet_no=0,Send_gatewaye_response=0;
unsigned char send_SLC_test_query_1 = 0;
unsigned char send_SLC_test_query_2 = 0;
unsigned char test_query_1_send_timer = 0;
float test_voltage;
float test_current;
unsigned short int test_emt_int_count;
unsigned short int test_Kwh_count;
unsigned int scann_chan=0;
unsigned char photocell_test_dicision = 0,read_attr=0,write_attr=0;
unsigned char SLC_Test_Query_2_received = 0;
unsigned char Send_Get_Master_Sch_Replay=0;		
unsigned char Sch_week_day = 0; 				
unsigned char get_sch_day = 0;					
unsigned char get_MASTER_sch_day = 0;		
unsigned char Get_Sch_timer = 0;				
unsigned char Get_Master_Sch_timer = 0;	
unsigned char Send_Get_Version_info_Replay = 0;
unsigned char sch_day[10];
unsigned char sch_Master_day[10];
unsigned char Send_Get_Adaptive_dimming_para_Replay = 0,Send_Get_Adaptive_dimming_para_Replay_32 = 0,SLC_Rejoin_SameChannel =0;
unsigned char send_data_direct = 0;
unsigned char Motion_broadcast_miss_counter = 0;
unsigned long int Motion_broadcast_Receive_counter = 0,Temp_Motion_broadcast_Receive_counter = 0;
signed short int Day_light_harvesting_start_offset = 0;
signed short int Day_light_harvesting_stop_offset = 0;
long Day_light_harvesting_start_time = 0;
long Day_light_harvesting_stop_time = 0;
unsigned char idimmer_en = 0;
unsigned int idimmer_id = 0;
unsigned char idimmer_longaddress[10];
unsigned char send_idimmer_id_frame = 0;
//unsigned int Send_dimming_cmd = 0;
unsigned char idimmer_Id_frame_received = 0;
unsigned char send_idimmer_Val_frame = 0;
unsigned char dimming_cmd_ack = 0;
unsigned char idimmer_trip_erase = 0;
unsigned char idimmer_dimval = 0;
unsigned char current_dimval = 0;
unsigned char dimming_cmd_cnt = 0;
unsigned char read_dim_val_timer = 0;
unsigned char read_dim_val_idimmer = 0;
unsigned char Send_dimming_short_ack = 0;
unsigned char Lamp_type = 0;
unsigned char Send_Get_Slc_Config_para_Replay = 0;
unsigned char SLC_joinig_retry_counter = 0;
unsigned int PanId=0;
unsigned long int channel_set=0;
unsigned char ExtPanId_set[10];
unsigned char Read_db_val = 0;
int8s Slc_db = 0;
unsigned char SLC_DST_En = 0;	
unsigned char SLC_New_Manula_Mode_En = 0,SLC_New_Manula_Mode_Val =0;
unsigned char SLC_DST_Start_Rule = 0;
unsigned char SLC_DST_Start_Month = 0;
unsigned char SLC_DST_Start_Time = 0;
unsigned char SLC_DST_Stop_Rule = 0;
unsigned char SLC_DST_Stop_Month = 0;
unsigned char SLC_DST_Stop_Time = 0;
float SLC_DST_Time_Zone_Diff = 0;
unsigned char SLC_DST_Rule_Enable = 0;	
unsigned char SLC_DST_Start_Date = 0;
unsigned char SLC_DST_Stop_Date = 0;
unsigned char SLC_DST_R_Start_Date;
unsigned char SLC_DST_R_Stop_Date;
unsigned char Time_change_dueto_DST = 0;
unsigned int old_year = 0;
unsigned char send_get_slc_dst_config_para = 0;
unsigned char Send_Get_Mix_Mode_Sch = 0;
unsigned char L_No = 0;
unsigned char Get_Mix_mode_Sch_Loc = 0;
unsigned char Mix_photo_override = 0;
unsigned char Auto_event = 0;
unsigned char send_data_direct_counter = 0,Ember_Test_Response =0;
unsigned char send_get_photocell_config = 0;
unsigned char send_get_OTA_Data = 0;
unsigned char send_get_Rs485_DI2_Config =0;
unsigned char send_GET_FAULT_REMOVE_CONFIG=0;
unsigned char send_GET_LastGasp_CONFIG=0;
unsigned char send_get_Auto_send_config = 0;
unsigned char Temp_Slc_Multi_group = 0;
BYTE_VAL Group_val1;
DWORD_VAL Group_val1_32;
unsigned char Dimming_test_dicision = 0;
unsigned char Test_dimming_val = 0;
unsigned char Start_dimming_test = 0;
unsigned char send_get_network_search_para = 0;
unsigned char Commissioning_flag = 0;
unsigned char new_dcu_detected = 0;
EmberEUI64 eui64;
unsigned char temp_arr[80];
unsigned char Cali_buff[50];//80
unsigned char send_get_Gps_Config = 0;
unsigned char need_to_send_lat_long = 0;
unsigned int  RTC_power_on_Logic_Timer = 0;
unsigned char check_RTC_logic_cycle = 0;
unsigned char Lat_long_need_to_varify = 0;
unsigned char RTC_need_to_varify = 0;
//unsigned char Send_RTC_Logic_Analysis_Data = 0;
unsigned char dimm_cmd_rec =0;


extern unsigned char rf_wake_flag;
extern unsigned char chek_connection,automode;
extern unsigned char send_gateway_data,read_attr_data;
extern unsigned int received_short_id;
extern void get_data(unsigned char *data,unsigned int fr_no);
extern unsigned int timer_arm;
extern float Dimvalue;

extern unsigned char Pro_count;
extern unsigned char Single_time_run_cmd,Gateway_Rsp_Cnt,one_sec_Ntw_Search;
extern unsigned long Network_Scan_Cnt,Network_Scan_Cnt_Time_out;
extern unsigned char SLC_Start_netwrok_Search_Timeout;
extern unsigned char SLC_intermetent_netwrok_Search_Timeout;
extern unsigned char SLC_Start_netwrok_Search_counter;
extern unsigned char Enable_Security; //To enable the security put value 1
extern BYTE_VAL  GPS_error_condition;

void Response_Zero_For_Mix_Schedule_11_To_15(void);
void Erase_Mix_Mode(void);
void Erase_Motion_perameter_And_Set_Default(void);
float Blink_Freq_Time =0;
//**** OTA **********
unsigned char Flag_CommandReceivedTORetryMissedOffset=0;
//*******************
unsigned char Last_Gsp_Enable =1;
volatile unsigned int LastGasp_Timer =0;
unsigned char Send_LastGasp=0,LastGasp_unicast_status=0;
unsigned char Lamp_status_On_LastGasp=0,Last_Gasp_Detect=0,LAmp_previous_status_On_Aftr_LastGasp =0,LG_cnt=0;
unsigned char GET_DALI_AO_CONFIG_Flag;
/*************************************************************************
Function Name: checkfream
input: serial receive buffer.
Output: none.
Discription: This function is use to parce all rf messages and store value
             of all variables accordingly
*************************************************************************/
void checkfream(unsigned char *temp)
{
  unsigned int temp_amr =0,convert_1=0,convert_2=0,HS=0;
  unsigned char t=0,HG=0,HM =0;
  unsigned char check_time = 0,emb_temp,Local_arry[10],y=0,Temp_Slc_Group_Match = 0,Temp_Slc_Multi_broadcast_group = 0;
  unsigned int i = 0,s = 0,k=0,j=0;
  unsigned char m =0,b=0;
  RTCDate LocalDate;
  RTCTime LocalTime;

  if((temp[0] == ';') && (temp[1] == ';'))
  {
    pulseCounter.byte[3] = temp[2];
    pulseCounter.byte[2] = temp[3];
    pulseCounter.byte[1] = temp[4];
    pulseCounter.byte[0] = temp[5];
    temp_amr = pulseCounter.dword;
    temp = &temp[6];
  }
  
  if((temp[0] == 'E') && (temp[1] == 'M')&& (temp[2] == 'B')&&
     (temp[3] == 'E')&& (temp[4] == 'R')&& (temp[5] == 'T')&& (temp[6] == 'E')
       && (temp[7] == 'S')&& (temp[8] == 'T'))          // if message received form module test applcaiton then send responce.
  {
    Ember_Test_Response =1;
//    emberAfAppPrint("\r\n TEST_RSSI=%d",Slc_db);
  }
  if((temp[0] == 'I') && (temp[1] == 'D'))       // if receive ID frame responce from DCU then save responce.
  {
    Network_Leave_Hr_Cnt =0;
    temp_amr = temp[2] * 256;
    temp_amr = temp_amr + temp[3];
    amr_id = temp_amr;
    if(temp[4] == '$')
    {
//    if(Blue_Light_Id !=temp_amr)
//    {
//      Blue_Light_Id =  temp_amr;
//      halCommonSetToken(TOKEN_Blue_Light_Id,&Blue_Light_Id);
//    }
    pulseCounter.byte[3] =temp[5];
    pulseCounter.byte[2]=temp[6];
    pulseCounter.byte[1]=temp[7];
    pulseCounter.byte[0]=temp[8];
    amr_id = pulseCounter.dword;
    }
    
    temp_amr = temp[9] * 256;
    temp_amr = temp_amr + temp[10];
    if(Blue_Light_Id !=temp_amr)
    {
      Blue_Light_Id =  temp_amr;
      halCommonSetToken(TOKEN_Blue_Light_Id,&Blue_Light_Id);
    }
    
    halCommonSetToken(TOKEN_amr_id,&amr_id);				
    Id_frame_received = 1;

    Network_Scan_Cnt=0;
//    idimmer_en = temp[4];                              // idimmer enable/disable flage
    //halCommonSetToken(TOKEN_idimmer_en,&idimmer_en);

//    idimmer_id = (temp[5] * 256) + temp[6];            // idimmer ID
//    halCommonSetToken(TOKEN_idimmer_id,&idimmer_id);
//      if(Valid_DCU_Flag == 0)
//      {
//        Conflict_PAN_ID=emberGetPanId();
//        halCommonSetToken(TOKEN_idimmer_id,&Conflict_PAN_ID);
//      }

//    idimmer_longaddress[0] = temp[7];                  // idimmer MAC Address
//    idimmer_longaddress[1] = temp[8];
//    idimmer_longaddress[2] = temp[9];
//    idimmer_longaddress[3] = temp[10];
//    idimmer_longaddress[4] = temp[11];
//    idimmer_longaddress[5] = temp[12];
//    idimmer_longaddress[6] = temp[13];
//    idimmer_longaddress[7] = temp[14];
//    halCommonSetToken(TOKEN_idimmer_longaddress,&idimmer_longaddress[0]);

    if(temp[15] == 1)                    // test application.
    {
      Valid_DCU_Flag = 1;
      halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);

//      Valid_DCU_Flag = 1;
//      scann_chan =0x7FFF;
//      channel_set=scann_chan<<11;
//      halCommonSetToken(TOKEN_scann_chan,&scann_chan);
//
      ExtPanId_set[7] = 0;	
      ExtPanId_set[6] = 0;
      ExtPanId_set[5] = 0;
      ExtPanId_set[4] = 0;
      ExtPanId_set[3] = 0;
      ExtPanId_set[2] = 0;
      ExtPanId_set[1] = 0;
      ExtPanId_set[0] = 0;
      halCommonSetToken(TOKEN_ExtPanId_set,&ExtPanId_set);

      Link_Key[0] = 0x00;
      Link_Key[1] = 0x11;
      Link_Key[2] = 0x22;
      Link_Key[3] = 0x33;
      Link_Key[4] = 0x44;
      Link_Key[5] = 0x55;
      Link_Key[6] = 0x66;
      Link_Key[7] = 0x77;
      Link_Key[8] = 0x88;
      Link_Key[9] = 0x99;
      Link_Key[10] = 0xAA;
      Link_Key[11] = 0xBB;
      Link_Key[12] = 0xCC;
      Link_Key[13] = 0xDD;
      Link_Key[14] = 0xEE;
      Link_Key[15] = 0xFF;

      halCommonSetToken(TOKEN_TLink_Key,&Link_Key);

    }
    else                           // DCU Detect.
    {
      if(Valid_DCU_Flag == 0)
      {
        new_dcu_detected = 1;       // if DCU detect first time then do lamp on/off sequence for 30 sec.
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
        Send_GPS_Data_at_valid_Dcu = 1;
        halCommonSetToken(TOKEN_Send_GPS_Data_at_valid_Dcu,&Send_GPS_Data_at_valid_Dcu);
        GPS_read_success_counter = 0;
        need_to_send_GPS_command = 1;
        averaging_Timer = 0;
        Start_averaging_of_GPS = 0;
        GPS_power_On_detected = 0;
        //send_get_mode_replay = 1;   // send lat/long value automatically when SLC join valid DCU first time.
#endif
      }
      Valid_DCU_Flag =1;            // store valid dcu flage in to NVM.
      halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
    }
  }
  else if((temp[0] == 'B') && (temp[1] == 'T') && (temp[2] == 0x55) && (temp[3] == 0x55))
  {
    //asm("reset"); removed by hitesh no need in ember.
  }
  else if((temp[0] == 'D') && (temp[1] == 'M'))      // idimmer related query.
  {
    if((temp[2] == 'I') && (temp[3] == 'D') && (temp[4] == 'R'))
    {
      if(temp[5] == 1)
      {
        idimmer_Id_frame_received = 1;      // responce received from idimmer.
      }	
    }
    else if((temp[2] == 'V') && (temp[3] == 'R'))
    {
      if(temp[4] == 1)
      {
        dimming_cmd_ack = 1;
        current_dimval = idimmer_dimval;       // dimming value from idimmer.		    			
      }	
    }
    else if(temp[2] == 'R')	
    {
      Dimvalue = temp[3];	
      Dimvalue = 100 - Dimvalue;
    }
    else if(temp[2] == 'T')
    {
      if(temp[3] == 1)
      {
        Pro_count = 5;
        Send_dimming_short_ack = 1;         // dimming short trip detect from idimmer.
      }	
    }			
  }	
  else if((temp[0] == 'S') && (temp[1] == 'L'))     // SLC operation related string header receive
  {
    Network_Leave_Hr_Cnt =0;
   if(temp_amr == 0)
   { 
    temp_amr = temp[2] * 256;
    temp_amr = temp_amr + temp[3];     // generate SLC ID from received string.
   }
    if((temp_amr == amr_id) || (temp_amr == 0x00))      // if generated SLC ID match with acthual SLC ID or broadcast message receive then parse it else ignore it.
    {

      switch(temp[4])
      {

      case CMD:                       // SLC command string received.

        if(temp[5] == DATA_QUERY)     // read data string received from DCU.
        {
          Frm_no = 255;									
          Network_Scan_Cnt=0;
          if(temp_amr == 0x00)   // donot send any responce to DCU for read data broadcast message.
          {
          }
          else
          {
//            if(idimmer_en == 1)    // if idimmer enable then make delay for 3 sec to send responce and send query for read data to idimmer.
//            {
//              read_dim_val_idimmer = 1;
//              read_dim_val_timer = 0;
//              Send_Data = 1;
//              Read_db_val = 1;
//            }	
//            else
//            {
              Send_Data = 1;             // send responce of read data to dcu.
              read_dim_val_timer = 2;	
              Read_db_val = 1;
              
//            }
          }
          emberAfCorePrintln("\r\nRead Data Request received");
        }
        else if(temp[5] == DATA_QUERY_WITH_LAST_HOUR_ENERGY)
        {
          Send_Data_WithLastHourEnergy = 1;
        }        
        else if(temp[5] == SET_RTC)      // RTC set command received from DCU.
        {
          if(temp[6] > 0 && temp[6] <= 31)  // check validation for Date
          {
            LocalDate.Date  = temp[6];
          }else
          {
            check_time = 1;
          }

          if(temp[7] > 0 && temp[7] <= 12)  // check validation for Month
          {
            LocalDate.Month = temp[7];
          }
          else
          {
            check_time = 1;
          }


          if(temp[8] > 0 && temp[8] <= 99)    // check validation for Year
          {
            LocalDate.Year  = temp[8];
          }
          else
          {
            check_time = 1;
          }

          if(temp[9] < 24)                  // check validation for Hour
          {
            LocalTime.Hour  = temp[9];
          }
          else
          {
            check_time = 1;
          }

          if(temp[10] < 60)                // check validation for Min
          {
            LocalTime.Min   = temp[10];
          }
          else
          {
            check_time = 1;
          }

          if(temp[11] < 60)              // check validation for Sec.
          {
            LocalTime.Sec   = temp[11];
          }
          else
          {
            check_time = 1;
          }

#if (defined(ISLC_T8)||defined(ISLC3300_T8_V2))
          if(temp[12] != 0x31)
          {
            automode = temp[12]-0x30;        // convert SLC Mode from ascii to hex
            halCommonSetToken(TOKEN_automode, &automode);
            previous_mode = automode;
            halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
            emberAfCorePrintln("\r\nSet RTC command received");
          }

#else
          automode = temp[12]-0x30;        // convert SLC Mode from ascii to hex
          halCommonSetToken(TOKEN_automode, &automode);
          previous_mode = automode;
          halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.
          emberAfCorePrintln("\r\nSet RTC command received");
#endif
        if(SLC_Test_Query_2_received == 1)
          {
            automode =0;
          }

        }
        else if(temp[5] == GET_RTC)
        {
          Get_rtc_Replay = 1;       // Send responce of RTC Get command.
        }
        else if(temp[5] == ERS_EEP)
        {
          pulses_r = 0;											//erase KWH reading.
          halCommonSetToken(TOKEN_pulses_r,&pulses_r);
          ////Erase TOU/////
//          for(t =0;t<11;t++){
//              Time_Slot.Slot_Pulses[t]=0;
//          }
//           halCommonSetToken(TOKEN_Slot_Pulses,&Time_Slot.Slot_Pulses[0]);
          ///////////
          newkwh = 0;
          kwh = 0;
          presentHourKwhBuffer[0] = kwh;
          presentHourKwhBuffer[1] = kwh;
          presentHourKwhBuffer[2] = kwh;
          previousHourKwhBuffer[0] = kwh;
          previousHourKwhBuffer[1] = kwh;
          previousHourKwhBuffer[2] = kwh;
          previousHourKwhBuffer[3] = kwh;          
          lamp_burn_hour = 0;
          
          halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);

          detect_lamp_current = 0;
          halCommonSetToken(TOKEN_detect_lamp_current,&detect_lamp_current);
#ifdef NEW_LAMP_FAULT_LOGIC
          KW_Threshold = 0;
          halCommonSetToken(TOKEN_KW_Threshold,&KW_Threshold);
#endif
          lamp_current = 0;
          halCommonSetToken(TOKEN_lamp_current,&lamp_current);

          check_current_firsttime_counter = 0;
          current_creep_limit = lamp_current * 0.1;	
          if(current_creep_limit < 0.03)
          {
            current_creep_limit = 0.03;						// if current creep limit less then 0.03 make it 0.03	
          }
          else if(current_creep_limit > 0.1)
          {
            current_creep_limit = 0.1;						// if current creep limit more then 0.1 make it 0.1
          }
          else
          {

          }	
          emberAfCorePrintln("\r\nErase EEPROM command received");
        }
        else if(temp[5] == LL_AMR)       // Set Latitude/Longitude command received from DCU
        {
          pulseCounter.byte[3] = temp[6];
          pulseCounter.byte[2] = temp[7];
          pulseCounter.byte[1] = temp[8];
          pulseCounter.byte[0] = temp[9];
          Latitude = pulseCounter.float_data;       // Store Latitude
          halCommonSetToken(TOKEN_Latitude,&Latitude);

          pulseCounter.byte[3] = temp[10];
          pulseCounter.byte[2] = temp[11];
          pulseCounter.byte[1] = temp[12];
          pulseCounter.byte[0] = temp[13];
          longitude = pulseCounter.float_data;        // Store Longitude
          halCommonSetToken(TOKEN_longitude,&longitude);

          pulseCounter.byte[3] = temp[14];
          pulseCounter.byte[2] = temp[15];
          pulseCounter.byte[1] = temp[16];
          pulseCounter.byte[0] = temp[17];
          utcOffset = pulseCounter.float_data;         // Store TimeZone
          halCommonSetToken(TOKEN_utcOffset,&utcOffset);

          pulseCounter.byte[1] = temp[18];
          pulseCounter.byte[0] = temp[19];
          Sunset_delay = pulseCounter.int_data;        // Store Sunset delay
          halCommonSetToken(TOKEN_Sunset_delay,&Sunset_delay);

          pulseCounter.byte[1] = temp[20];
          pulseCounter.byte[0] = temp[21];
          Sunrise_delay = pulseCounter.int_data;       // Store Sun rise delay
          halCommonSetToken(TOKEN_Sunrise_delay,&Sunrise_delay);

          GetSCHTimeFrom_LAT_LOG();                   // compute Astro time from lat/long
          Astro_sSch[0].cStartHour = sunsetTime/3600;						
          Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
          Astro_sSch[0].cStopHour  = 23;							
          Astro_sSch[0].cStopMin   = 59;							
          Astro_sSch[0].bSchFlag   = 1;							

          Astro_sSch[1].cStartHour = 00;							
          Astro_sSch[1].cStartMin  = 00;							
          Astro_sSch[1].cStopHour  = sunriseTime/3600;			
          Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
          Astro_sSch[1].bSchFlag   = 1;							
          halCommonSetToken(TOKEN_schedule_Astro_sSch,&Astro_sSch[0]);
          emberAfCorePrintln("\r\nSet Latitude/Longitude command received");

        }
        else if(temp[5] == SYNC_RTC)     // Sync RTC command received.
        {
          if(temp[6] > 0 && temp[6] <= 31)   // check validation for Date
          {
            LocalDate.Date  = temp[6];
          }
          else
          {
            check_time = 1;
          }

          if(temp[7] > 0 && temp[7] <= 12)  // check validation for Month
          {
            LocalDate.Month = temp[7];
          }
          else
          {
            check_time = 1;
          }


          if(temp[8] > 0 && temp[8] <= 99)   // check validation for year
          {
            LocalDate.Year  = temp[8];
          }
          else
          {
            check_time = 1;
          }

          if(temp[9] < 24)                    // check validation for Hour
          {
            LocalTime.Hour  = temp[9];
          }
          else
          {
            check_time = 1;
          }

          if(temp[10] < 60)                        // // check validation for Minute
          {
            LocalTime.Min   = temp[10];
          }
          else
          {
            check_time = 1;
          }

          if(temp[11] < 60)                        // check validation for Sec
          {
            LocalTime.Sec   = temp[11];
          }
          else
          {
            check_time = 1;
          }


          if(check_time == 0)                 // if all validation proper then set time and date in RTC.
          {
            RTC_SET_TIME(LocalTime);
            RTC_SET_DATE(LocalDate);
            if(temp[12] <= 1)               // check DST enable or Disable in DCU time.
            {
              Time_change_dueto_DST = temp[12];
              halCommonSetToken(TOKEN_Time_change_dueto_DST,&Time_change_dueto_DST);
            }

            Local_Time.Hour = LocalTime.Hour;																										
            Local_Time.Min = LocalTime.Min;
            Local_Time.Sec = LocalTime.Sec;
            Local_Date.Date = LocalDate.Date;
            Local_Date.Month = LocalDate.Month;
            Local_Date.Year = LocalDate.Year;
            RTC_need_to_varify = 1;
            RTC_power_on_Logic_Timer = 0;         // for testing
            RTC_Faulty_Shift_to_Local_Timer =1; //When GPS not available and RTC becomes faulty then shift to local timer on DCU time sync
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))

            if(GPS_error_condition.bits.b7 == 1)   // invoke GPS if available so if sender's time wrong then SLC correct it by GPS.
            {
              //send_gps_command();
              Time_set_by_GPS = 0;
              need_to_send_GPS_command = 1;
            }
#endif
          }
        }
        else if(temp[5] == CLOUDY_FLAG)   // cloudy condition command received
        {
          Cloudy_Flag = temp[6];
        }
        else if(temp[5] == ERASE_TRIP_SLC)     // Erase SLC Trip command received.
        {
          lamp_lock = 0;                       // Set all variables used in trip logic to 0 to regenerate trip for nex time.
          low_current_counter = 0;
          cycling_lamp_fault = 0;

//          Photo_Cell_Ok = 1;
#if (defined(ISLC_T8)||defined(ISLC3300_T8_V2))
          Photo_Cell_Ok = 0;
          error_condition.bits.b1 = 1;
#else
          Photo_Cell_Ok = 1;
          photo_cell_toggel_counter = 0;
          photo_cell_toggel_timer = 0;
          photo_cell_timer = 0;
#endif
          halCommonSetToken(TOKEN_Photo_Cell_Ok,&Photo_Cell_Ok);   // store mode in to NVM.
          Master_event_generation = 0;
          event_counter = 0;		
          energy_meter_ok = 0;
          energy_time = 0; 						
          error_condition.bits.b4 = 0;
          error_condition.bits.b6 = 0;
          error_condition.bits.b3 = 0;
          error_condition1.bits.b0 = 0;
          error_condition1.bits.b3 = 0;
          error_condition1.bits.b4 = 0;
          error_condition1.bits.b6 = 0;										
          Protec_lock =0;														
          Pro_count =0;
          Lamp_Fail =0;
          Ballast_Fail =0;
          Day_Burner =0;

//          if(idimmer_en == 1)
//          {
//            idimmer_trip_erase = 1;
//            send_idimmer_Val_frame = 1;
//          }
          if(Lamp_lock_condition == 1)
          {
            Set_Do(0);
          }
          delet_all_data();
          Slc_reset_counter = 0;
          halCommonSetToken(TOKEN_Slc_reset_counter,&Slc_reset_counter);
        }
        else if(temp[5] == EVENT_START)
        {
          if(temp[6] == 1)
          {
            stop_event = 0;	
          }
        }
        else if(temp[5] == EVENT_QUERY)       // Read all event command received.
        {
          read_event_flag = 1;
          rf_event_send_counter = Unicast_time_delay;	
        }
        else if(temp[5] == GET_MODE)   // Get Mode command received from DCU.
        {
          send_get_mode_replay = 1;
          GPS_Data_throw_status = 5;
          emberAfCorePrintln("\r\nGet Mode command received");
        }
        else if(temp[5] == SET_FAULT_PARA) // Set SLC lamp fault parameters string from DCU.
        {
          pulseCounter.byte[1] = temp[6];
          pulseCounter.byte[0] = temp[7];
          Vol_hi = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Vol_hi,&Vol_hi);


          pulseCounter.byte[1] = temp[8];
          pulseCounter.byte[0] = temp[9];
          Vol_low = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Vol_low,&Vol_low);

          pulseCounter.byte[1] = temp[10];
          pulseCounter.byte[0] = temp[11];
          Curr_Steady_Time = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Curr_Steady_Time,&Curr_Steady_Time);

          pulseCounter.byte[1] = temp[12];
          pulseCounter.byte[0] = temp[13];
          Per_Val_Current = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Per_Val_Current,&Per_Val_Current);

          pulseCounter.byte[1] = temp[14];
          pulseCounter.byte[0] = temp[15];
          Lamp_Fault_Time = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Lamp_Fault_Time,&Lamp_Fault_Time);

          pulseCounter.byte[1] = temp[16];
          pulseCounter.byte[0] = temp[17];
          Lamp_off_on_Time = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Lamp_off_on_Time,&Lamp_off_on_Time);

          pulseCounter.byte[1] = temp[18];
          pulseCounter.byte[0] = temp[19];
          Lamp_faulty_retrieve_Count = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Lamp_faulty_retrieve_Count,&Lamp_faulty_retrieve_Count);
          pulseCounter.byte[1] = temp[20];
          pulseCounter.byte[0] = temp[21];
          Lamp_lock_condition = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Lamp_lock_condition,&Lamp_lock_condition);

        }
        else if(temp[5] == GET_FAULT_PARA)             // Get SLC falut parameters command received from DCU.
        {
          Send_Fault_Para_Replay = 1;
          emberAfCorePrintln("\r\nGet Fault command received");
        }
        else if(temp[5] == SLC_DIAGNOSIS)   // Get Diagnosis command received from DCU.
        {
          check_peripheral = 1;
          Master_event_generation = 1;
        }
        else if(temp[5] == SET_NETWORK_PARA)    // Set Network parameters received from DCU.
        {
          pulseCounter.byte[1] = temp[6];
          pulseCounter.byte[0] = temp[7];
          Broadcast_time_delay = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Broadcast_time_delay,&Broadcast_time_delay);
          pulseCounter.byte[1] = temp[8];
          pulseCounter.byte[0] = temp[9];
          Unicast_time_delay = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Unicast_time_delay,&Unicast_time_delay);

          pulseCounter.byte[1] = temp[10];
          pulseCounter.byte[0] = temp[11];
          Lamp_lock_condition = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Lamp_lock_condition,&Lamp_lock_condition);
        }
        else if(temp[5] == GET_NETWORK_PARA)    // Get Network parameters received from DCU
        {
          Send_Network_Para_Replay = 1;
        }
        else if(temp[5] == GET_SCH)             // Get Schedule parameters received from DCU
        {
          Send_Get_Sch_Replay = 1;
          Get_Sch_timer = 5;
          emberAfCorePrintln("\r\nGet Sch command received");
        }
        else if(temp[5] == SLC_TEST_QUERY_1)      // Get Test query received from Test application.
        {									
          Master_event_generation = 1;
          //emberAfGuaranteedPrint("\r\n%p", "Test Query 1 received");
          Date.Date = temp[6];
          Date.Month = temp[7];
          Date.Year = temp[8];
          RTC_SET_DATE(Date);									
          Time.Hour = temp[9];																										
          Time.Min = temp[10];
          Time.Sec = temp[11];
          photocell_test_dicision = temp[12];
          Dimming_test_dicision = temp[13];
          
          //////////////AO-DALI SELECTION/////////////
          
          if(temp[14] == 'A')
          {
            DimmerDriverSelectionProcess = 3;
            dali_support = 0;
#if defined(ISLC_3100_V7_7)
            halSetLed(MUX_A1);	        //0	
            halClearLed(MUX_A0);        //1
#elif defined(ISLC_3100_V7_9) 
            halClearLed(MUX_A0);   // Set=0=dali & clear=1=AO
#endif            
            halStackIndicatePresence(); 
            DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
           halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);
          }
          else if (temp[14] == 'D')
          {
            DimmerDriverSelectionProcess = 0x02;
            dali_support = 1;
#if defined(ISLC_3100_V7_7)             
            halSetLed(MUX_A0);	 //0	
            halClearLed(MUX_A1);       //1
#elif defined(ISLC_3100_V7_9) 
            halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO  
#endif            
            halStackIndicatePresence();
            DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(dali_support);
           halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION);
            
          }
          else if (temp[14] == 'S') //Single Time detection
          {
            DimmerDriverSelectionProcess = 0x01;
            dali_support = 2;
            detectDimmerDriver();
            
          }
           
          ////////////////////////////

          RTC_SET_TIME(Time);

          if(SLC_Test_Query_2_received == 0)       // if Test query two not running then check all peripheral.
          {
            Check_Slc_Test_1();
          }
          SLC_Test_Query_2_received = 1;									
          Commissioning_flag = 2;                 // Set commissioning flag in SLC.
          halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);

        GPS_Read_Enable = 0; //GPS changes the coordinate so get impact in testing so make it disable in test routine.
       // halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
        }
        else if(temp[5] == SLC_TEST_QUERY_1_ACK)      // received Acknowledge of test query one result.
        {
          if((temp[6] == 'A') && (temp[7] == 'C') && (temp[8] == 'K'))
          {
            send_SLC_test_query_1 = 0;
            Master_event_generation = 0;	
          }	
        }
        else if(temp[5] == SLC_TEST_QUERY_2)     // get test query two command from test application.
        {
          send_SLC_test_query_2 = 1;	
          test_query_1_send_timer = 0;
          Master_event_generation = 1;
          SLC_Test_Query_2_received = 1;
          GPS_Read_Enable =0;
          Commissioning_flag = 1;                 // Set commissioning flag in SLC.
          halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);
        }
        else if(temp[5] == SLC_TEST_QUERY_2_ACK) // received Acknowledge of Test query two result.
        {
          if((temp[6] == 'A') && (temp[7] == 'C') && (temp[8] == 'K'))
          {
            send_SLC_test_query_2 = 0;
            Master_event_generation = 0;
            test_query_1_send_timer = 0;	
          }
        }
        else if(temp[5] == GET_DIMMING_SCH) // Get dimming command received from DCU.
        {
          Send_Get_Master_Sch_Replay = 1;
          Get_Master_Sch_timer = 5;
          emberAfCorePrintln("\r\nGet Dimming Sch command received");
        }
        else if(temp[5] == GET_VERSION_INFO)			// Get SLC version command received from DCU.
        {
          Send_Get_Version_info_Replay = 1;	
        }
        else if(temp[5] == ADAPTIVE_DIMMING_PARA) // Get adaptive dimming command from DCU.
        {
//#ifndef ISLC_10
//#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8))
          adaptive_light_dimming = temp[14];

          halCommonSetToken(TOKEN_adaptive_light_dimming,&adaptive_light_dimming);
          if(adaptive_light_dimming == 0)    // normal dimming schedule
          {
            ballast_type = 1;
            halCommonSetToken(TOKEN_ballast_type,&ballast_type);

            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay
            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);

            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	
            halCommonSetToken(TOKEN_dim_inc_val ,&dim_inc_val);

          }	
          if(adaptive_light_dimming == 1)
          {
            pulseCounter.byte[1] = temp[6];
            pulseCounter.byte[0] = temp[7];
            analog_input_scaling_high_Value = pulseCounter.int_data;
            if(analog_input_scaling_high_Value == 0)
            {
              analog_input_scaling_high_Value = 50000;
            }
            halCommonSetToken(TOKEN_analog_input_scaling_high_Value,&analog_input_scaling_high_Value);


            pulseCounter.byte[1] = temp[8];
            pulseCounter.byte[0] = temp[9];
            analog_input_scaling_low_Value = pulseCounter.int_data;

            halCommonSetToken(TOKEN_analog_input_scaling_low_Value,&analog_input_scaling_low_Value);
            pulseCounter.byte[1] = temp[10];
            pulseCounter.byte[0] = temp[11];
            desir_lamp_lumen = pulseCounter.int_data;
            if(desir_lamp_lumen == 0)
            {
              desir_lamp_lumen = analog_input_scaling_high_Value;
            }

            halCommonSetToken(TOKEN_desir_lamp_lumen,&desir_lamp_lumen);
            lumen_tollarence = temp[12];

            halCommonSetToken(TOKEN_lumen_tollarence,&lumen_tollarence);
            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay

            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);
            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	

            halCommonSetToken(TOKEN_dim_inc_val,&dim_inc_val);

            pulseCounter.byte[1] = temp[23];
            pulseCounter.byte[0] = temp[24];
            Day_light_harvesting_start_offset = pulseCounter.int_data;
            Day_light_harvesting_start_offset = Day_light_harvesting_start_offset * 60;
            halCommonSetToken(TOKEN_Day_light_harvesting_start_offset,&Day_light_harvesting_start_offset);

            pulseCounter.byte[1] = temp[25];
            pulseCounter.byte[0] = temp[26];
            Day_light_harvesting_stop_offset = pulseCounter.int_data;
            Day_light_harvesting_stop_offset = Day_light_harvesting_stop_offset * 60;

            halCommonSetToken(TOKEN_Day_light_harvesting_stop_offset,&Day_light_harvesting_stop_offset);
            ballast_type = 1;

            halCommonSetToken(TOKEN_ballast_type,&ballast_type);
            Day_light_harves_time();                             // compute astro time from latitude,longitude,UTCOffset,daylight harvesting delays.
            DLH_sSch[0].cStartHour = Day_light_harvesting_start_time/3600;				
            DLH_sSch[0].cStartMin  = (Day_light_harvesting_start_time%3600)/60;		
            DLH_sSch[0].cStopHour  = 23;							
            DLH_sSch[0].cStopMin   = 59;							
            DLH_sSch[0].bSchFlag   = 1;							

            DLH_sSch[1].cStartHour = 00;							
            DLH_sSch[1].cStartMin  = 00;							
            DLH_sSch[1].cStopHour  = Day_light_harvesting_stop_time/3600;			
            DLH_sSch[1].cStopMin   = (Day_light_harvesting_stop_time%3600)/60;											
            DLH_sSch[1].bSchFlag   = 1;							
          }
          else if(adaptive_light_dimming == 2)
          {	

            Motion_pulse_rate = temp[15];
            if(Motion_pulse_rate == 0)
            {
              Motion_pulse_rate = 1;
            }

            halCommonSetToken(TOKEN_Motion_pulse_rate,&Motion_pulse_rate);
            Motion_dimming_percentage = temp[16];

            halCommonSetToken(TOKEN_Motion_dimming_percentage,&Motion_dimming_percentage);
            Motion_dimming_time = (temp[17] * 256) + temp[18];
            if(Motion_dimming_time < 10)
            {
              Motion_dimming_time = 10;
            }
            if(Motion_dimming_time > 59)
            {
              Motion_Rebroadcast_timeout = 58;
            }
            else
            {
              Motion_Rebroadcast_timeout = Motion_dimming_time - 2;
            }

            halCommonSetToken(TOKEN_Motion_dimming_time,&Motion_dimming_time);

            Motion_group_id = temp[19];
            Group_val1.Val = Motion_group_id;

            halCommonSetToken(TOKEN_Motion_group_id,&Motion_group_id);

            Motion_grp_Select_32 =0; //0 indicates it supports only 8 group..
            halCommonSetToken(TOKEN_Motion_grp_Select_32,&Motion_grp_Select_32);

            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay

            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);

            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	

            halCommonSetToken(TOKEN_dim_inc_val,&dim_inc_val);

            Motion_normal_dimming_percentage = temp[22];

            halCommonSetToken(TOKEN_Motion_normal_dimming_percentage,&Motion_normal_dimming_percentage);

            Motion_Detect_Timeout = temp[27];
            if((Motion_Detect_Timeout >= Motion_dimming_time) || (Motion_Detect_Timeout >  59) || (Motion_Detect_Timeout == 0))
            {
              Motion_Detect_Timeout = 5;	
            }
            halCommonSetToken(TOKEN_Motion_Detect_Timeout,&Motion_Detect_Timeout);

            Motion_Broadcast_Timeout= temp[28];
            if((Motion_Broadcast_Timeout >= Motion_dimming_time) || (Motion_Broadcast_Timeout >  59) || (Motion_Broadcast_Timeout ==  0))
            {
              Motion_Broadcast_Timeout = 5;	
            }
            halCommonSetToken(TOKEN_Motion_Broadcast_Timeout,&Motion_Broadcast_Timeout);

            Motion_Sensor_Type = temp[29];
            halCommonSetToken(TOKEN_Motion_Sensor_Type,&Motion_Sensor_Type);

            ballast_type = 1;
            halCommonSetToken(TOKEN_ballast_type,&ballast_type);
          }
          else if(adaptive_light_dimming == 3)          // discrite dimming
          {
            ballast_type = 0;
            halCommonSetToken(TOKEN_ballast_type,&ballast_type);		
          }
          else
          {

          }
//#elif (defined(ISLC_3100_7P_V1) || defined(ISLC_3100_V9))
//
//          adaptive_light_dimming = 0;
//          halCommonSetToken(TOKEN_adaptive_light_dimming,&adaptive_light_dimming);
//          if(adaptive_light_dimming == 0)    // normal dimming schedule
//          {
//            ballast_type = 1;
//            halCommonSetToken(TOKEN_ballast_type,&ballast_type);
//
//            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay
//            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);
//
//            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	
//            halCommonSetToken(TOKEN_dim_inc_val ,&dim_inc_val);
//
//          }
//#endif
        }
        /////////////////////////////////////
        else if(temp[5] == ADAPTIVE_DIMMING_PARA_32GROUP) // Get adaptive dimming command from DCU.
        {
//#ifndef ISLC_10
//#if (defined(ISLC_3100_V7) || defined(ISLC_3100_V5_V6) || defined(ISLC_T8))
          adaptive_light_dimming = temp[14];

          halCommonSetToken(TOKEN_adaptive_light_dimming,&adaptive_light_dimming);
          if(adaptive_light_dimming == 0)    // normal dimming schedule
          {
            ballast_type = 1;
            halCommonSetToken(TOKEN_ballast_type,&ballast_type);

            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay
            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);

            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	
            halCommonSetToken(TOKEN_dim_inc_val ,&dim_inc_val);

          }	
          if(adaptive_light_dimming == 1)
          {
            pulseCounter.byte[1] = temp[6];
            pulseCounter.byte[0] = temp[7];
            analog_input_scaling_high_Value = pulseCounter.int_data;
            if(analog_input_scaling_high_Value == 0)
            {
              analog_input_scaling_high_Value = 50000;
            }
            halCommonSetToken(TOKEN_analog_input_scaling_high_Value,&analog_input_scaling_high_Value);


            pulseCounter.byte[1] = temp[8];
            pulseCounter.byte[0] = temp[9];
            analog_input_scaling_low_Value = pulseCounter.int_data;

            halCommonSetToken(TOKEN_analog_input_scaling_low_Value,&analog_input_scaling_low_Value);
            pulseCounter.byte[1] = temp[10];
            pulseCounter.byte[0] = temp[11];
            desir_lamp_lumen = pulseCounter.int_data;
            if(desir_lamp_lumen == 0)
            {
              desir_lamp_lumen = analog_input_scaling_high_Value;
            }

            halCommonSetToken(TOKEN_desir_lamp_lumen,&desir_lamp_lumen);
            lumen_tollarence = temp[12];

            halCommonSetToken(TOKEN_lumen_tollarence,&lumen_tollarence);
            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay

            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);
            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	

            halCommonSetToken(TOKEN_dim_inc_val,&dim_inc_val);

            pulseCounter.byte[1] = temp[23];
            pulseCounter.byte[0] = temp[24];
            Day_light_harvesting_start_offset = pulseCounter.int_data;
            Day_light_harvesting_start_offset = Day_light_harvesting_start_offset * 60;
            halCommonSetToken(TOKEN_Day_light_harvesting_start_offset,&Day_light_harvesting_start_offset);

            pulseCounter.byte[1] = temp[25];
            pulseCounter.byte[0] = temp[26];
            Day_light_harvesting_stop_offset = pulseCounter.int_data;
            Day_light_harvesting_stop_offset = Day_light_harvesting_stop_offset * 60;

            halCommonSetToken(TOKEN_Day_light_harvesting_stop_offset,&Day_light_harvesting_stop_offset);
            ballast_type = 1;

            halCommonSetToken(TOKEN_ballast_type,&ballast_type);
            Day_light_harves_time();                             // compute astro time from latitude,longitude,UTCOffset,daylight harvesting delays.
            DLH_sSch[0].cStartHour = Day_light_harvesting_start_time/3600;				
            DLH_sSch[0].cStartMin  = (Day_light_harvesting_start_time%3600)/60;		
            DLH_sSch[0].cStopHour  = 23;							
            DLH_sSch[0].cStopMin   = 59;							
            DLH_sSch[0].bSchFlag   = 1;							

            DLH_sSch[1].cStartHour = 00;							
            DLH_sSch[1].cStartMin  = 00;							
            DLH_sSch[1].cStopHour  = Day_light_harvesting_stop_time/3600;			
            DLH_sSch[1].cStopMin   = (Day_light_harvesting_stop_time%3600)/60;											
            DLH_sSch[1].bSchFlag   = 1;							
          }
          else if(adaptive_light_dimming == 2)
          {	

            Motion_pulse_rate = temp[15];
            if(Motion_pulse_rate == 0)
            {
              Motion_pulse_rate = 1;
            }

            halCommonSetToken(TOKEN_Motion_pulse_rate,&Motion_pulse_rate);
            Motion_dimming_percentage = temp[16];

            halCommonSetToken(TOKEN_Motion_dimming_percentage,&Motion_dimming_percentage);
            Motion_dimming_time = (temp[17] * 256) + temp[18];
            if(Motion_dimming_time < 10)
            {
              Motion_dimming_time = 10;
            }
            if(Motion_dimming_time > 59)
            {
              Motion_Rebroadcast_timeout = 58;
            }
            else
            {
              Motion_Rebroadcast_timeout = Motion_dimming_time - 2;
            }

            halCommonSetToken(TOKEN_Motion_dimming_time,&Motion_dimming_time);

            //Motion_group_id = temp[19];
            //Group_val1.Val = Motion_group_id;

            //halCommonSetToken(TOKEN_Motion_group_id,&Motion_group_id);

            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay

            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);

            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	

            halCommonSetToken(TOKEN_dim_inc_val,&dim_inc_val);

            Motion_normal_dimming_percentage = temp[22];

            halCommonSetToken(TOKEN_Motion_normal_dimming_percentage,&Motion_normal_dimming_percentage);

            Motion_Detect_Timeout = temp[27];
            if((Motion_Detect_Timeout >= Motion_dimming_time) || (Motion_Detect_Timeout >  59) || (Motion_Detect_Timeout == 0))
            {
              Motion_Detect_Timeout = 5;	
            }
            halCommonSetToken(TOKEN_Motion_Detect_Timeout,&Motion_Detect_Timeout);

            Motion_Broadcast_Timeout= temp[28];
            if((Motion_Broadcast_Timeout >= Motion_dimming_time) || (Motion_Broadcast_Timeout >  59) || (Motion_Broadcast_Timeout ==  0))
            {
              Motion_Broadcast_Timeout = 5;	
            }
            halCommonSetToken(TOKEN_Motion_Broadcast_Timeout,&Motion_Broadcast_Timeout);

            Motion_Sensor_Type = temp[29];
            halCommonSetToken(TOKEN_Motion_Sensor_Type,&Motion_Sensor_Type);

            ballast_type = 1;
            halCommonSetToken(TOKEN_ballast_type,&ballast_type);
//////////////////////////////////////////
            pulseCounter.byte[3] = temp[30];
            pulseCounter.byte[2] = temp[31];
            pulseCounter.byte[1] = temp[32];
            pulseCounter.byte[0] = temp[33];

            Motion_group_id_32= pulseCounter.dword;

            Group_val1_32.Val = Motion_group_id_32;

            //halCommonSetToken(TOKEN_Motion_group_id,&Motion_group_id);
            halCommonSetToken(TOKEN_Motion_group_id_32,&Motion_group_id_32);

            Motion_grp_Select_32 =1; //1 indicates it supports only 32 group..
            halCommonSetToken(TOKEN_Motion_grp_Select_32,&Motion_grp_Select_32);
////////////////////////////////////////////
          }
          else if(adaptive_light_dimming == 3)          // discrite dimming
          {
            ballast_type = 0;
            halCommonSetToken(TOKEN_ballast_type,&ballast_type);		
          }
          else
          {

          }
//#elif (defined(ISLC_3100_7P_V1) || defined(ISLC_3100_V9))
//
//          adaptive_light_dimming = 0;
//          halCommonSetToken(TOKEN_adaptive_light_dimming,&adaptive_light_dimming);
//          if(adaptive_light_dimming == 0)    // normal dimming schedule
//          {
//            ballast_type = 1;
//            halCommonSetToken(TOKEN_ballast_type,&ballast_type);
//
//            dim_applay_time = temp[20] * 60;                      // Dimming_Apply_delay
//            halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);
//
//            dim_inc_val	= temp[21];                         // gradually_dimming_time 																	
//            halCommonSetToken(TOKEN_dim_inc_val ,&dim_inc_val);
//
//          }
//#endif
        }
        //////////////////////////////////////
        else if(temp[5] == GET_ADAPTIVE_DIMMING_PARA)       // Get adaptive dimming command received from DCU.
        {
          Send_Get_Adaptive_dimming_para_Replay = 1;	
          emberAfCorePrintln("\r\nGet Adaptive command received");
        }
        else if(temp[5] == GET_ADAPTIVE_DIMMING_PARA_32)       // Get adaptive dimming command received from DCU.
        {
          Send_Get_Adaptive_dimming_para_Replay_32 = 1;	
          emberAfCorePrintln("\r\nGet Adaptive command received");
        }	
        else if((temp[5] == MOTION_DETECT)||(temp[5] == MOTION_DETECT_32))      // motion detected broadcast received from other SLC.
        {
//////////////////////////////////////////////////////////
          if((Motion_grp_Select_32 == 1))
          {
            if(temp[5] == MOTION_DETECT){
              return;
            }

              Motion_broadcast_Receive_counter++;   // increase counter for motion detect.
              pulseCounter.byte[3] = temp[10];
              pulseCounter.byte[2] = temp[11];
              pulseCounter.byte[1] = temp[12];
              pulseCounter.byte[0] = temp[13];
              Temp_Motion_broadcast_Receive_counter = pulseCounter.long_data;
              if(Motion_broadcast_Receive_counter < Temp_Motion_broadcast_Receive_counter)
              {
                Motion_broadcast_Receive_counter = Temp_Motion_broadcast_Receive_counter;
                Motion_broadcast_miss_counter++;    // if motion counter not match then increase missing counter.		
              }
              pulseCounter.byte[3] = temp[6];
              pulseCounter.byte[2] = temp[7];
              pulseCounter.byte[1] = temp[8];
              pulseCounter.byte[0] = temp[9];


              Temp_Slc_Group_Match = check_32group(pulseCounter.dword);     // check group_id received in msg with SLC group if match it returns 1.
              Temp_Slc_Multi_broadcast_group = check_multi_group_32(pulseCounter.dword);  // if message received from intersection SLC then set flag to 1.
          }
          else
          {
            if(temp[5] == MOTION_DETECT_32)
            {
              return;
            }

              Motion_broadcast_Receive_counter++;   // increase counter for motion detect.
              pulseCounter.byte[3] = temp[7];
              pulseCounter.byte[2] = temp[8];
              pulseCounter.byte[1] = temp[9];
              pulseCounter.byte[0] = temp[10];
              Temp_Motion_broadcast_Receive_counter = pulseCounter.long_data;
              if(Motion_broadcast_Receive_counter < Temp_Motion_broadcast_Receive_counter)
              {
                Motion_broadcast_Receive_counter = Temp_Motion_broadcast_Receive_counter;
                Motion_broadcast_miss_counter++;    // if motion counter not match then increase missing counter.		
              }

              Temp_Slc_Group_Match = check_group(temp[6]);     // check group_id received in msg with SLC group if match it returns 1.
              Temp_Slc_Multi_broadcast_group = check_multi_group(temp[6]);  // if message received from intersection SLC then set flag to 1.
          }
/////////////////////////////////////////////////////////
          if((Temp_Slc_Multi_group == 1) && (Temp_Slc_Multi_broadcast_group == 1))  // if message received from intersection SLC to intersection SLC then it makes lamp on with full brightness.
          {
            if((Temp_Slc_Group_Match == 1))     // if group match then set flag for intersection SLC.
            {
              Motion_intersection_detected = 1;
              motion_intersection_dimming_timer = 0;		// reload dimming timer for intersection SLC to 0.	
            }
          }
          else
          {
            if((Temp_Slc_Group_Match == 1))   // check for message group is match with SLC group.
            {
              Motion_intersection_detected = 0;  // clear flag of intersection.
              Motion_detected_broadcast = 1;     // set flag for motion arrived.
              if(Temp_Slc_Multi_group == 0)      // if SLC not a part of intersection and message is alos not comming from the intersection SLC then set motion continue timer.
              {
                Motion_Received_broadcast = 1;		
                Motion_Continiue = 0;
                motion_broadcast_timer = 0;
                Motion_continue_timer = 0;	
              }
              else
              {
                Motion_Received_broadcast = 1;     // if SLC in intersection part then no need to set continue broadcast.
                Motion_detected = 0;	
              }

              if(Motion_detected == 0)       // if motion not detected by sensor then set it by broadcast.
              {
                	
              }
              else
              {
                  motion_dimming_Broadcast_send = 1;	 // if motion already detected then send flag set to 1 for broadcast.
              }

              motion_dimming_timer = 0;  // reload motion dimming timer and motion detect sec to 0 for next operation running.
              Motion_detect_sec = 0;

//              if(temp_amr != 0)         // if motion broadcast received unicast from DCU then clear missing counter.
//              {
//                Motion_broadcast_miss_counter = 0;
//                WriteDebugMsg("\n Motion Unicast received");
//              }
//              else
//              {
//                WriteDebugMsg("\n Motion broadcast received");
//              }
            }	
          }
        }
        else if(temp[5] == TEST_DIMMING)       // received messag for dimming test from test application.
        {
                Test_dimming_val = temp[6];
                if(Test_dimming_val <= 100)
                {
                        Start_dimming_test = 1;
                }
                else
                {
                        Start_dimming_test = 0;         // if dimming value received more then 100 then stop dimming test.
                }
        }
        else if(temp[5] == SET_RF_NETWORK_PARA)       // Set RF network para received from DCU, no need of this case in Ember SLC.
        {

        }
        else if(temp[5] == CHANGE_RF_NETWORK_PARA)   // change RF network parameter command received from DCU.
        {
          ExtPanId_set[7] = temp[6];	
          ExtPanId_set[6] = temp[7];
          ExtPanId_set[5] = temp[8];
          ExtPanId_set[4] = temp[9];
          ExtPanId_set[3] = temp[10];
          ExtPanId_set[2] = temp[11];
          ExtPanId_set[1] = temp[12];
          ExtPanId_set[0] = temp[13];
          halCommonSetToken(TOKEN_ExtPanId_set,&ExtPanId_set);   // Set extended Pan ID to NVM.

          scann_chan = temp[14];
          scann_chan=0x00000001<<(scann_chan-11);    // (convert channel number to binary formate like channel 11 represet LSB Bit0....channel 25 represent MSB Bit15)

          if(scann_chan == 0)         // if no information in channel variable then set to all channel
          {
            scann_chan =0x7FFF;
          }
          channel_set=scann_chan<<11;
          halCommonSetToken(TOKEN_scann_chan,&scann_chan);    // Set channel to NVM.


          Id_frame_received = 0;                // clear ID frame received flag to search new DCU in to new channel.
          RF_communication_check_Counter = 0;   // reset Id frame timer.
          Valid_DCU_Flag =0;                    // clear valid DCU flage to search DCU.
          halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);
          networkLeaveCommand();                // leave current network for search new network.
        }
        else if(temp[5] == SLC_CONFIG_PARA)     // SLC configuration parameters received from DCU.
        {									
          Lamp_type = temp[6];

          halCommonSetToken(TOKEN_Lamp_type,&Lamp_type);

          Auto_event = temp[7];

          halCommonSetToken(TOKEN_Auto_event,&Auto_event);

          Schedule_offset = temp[8];
          halCommonSetToken(TOKEN_Schedule_offset,&Schedule_offset);

          if((temp[9] > 0) && (temp[9] < 3))
          {
            if((RTC_New_fault_logic_Enable != 1) && (temp[9] == 1))
            {
              RTC_power_on_Logic_Timer = 0;
              photo_cell_Logic_toggel_counter = 0;
            }

            RTC_New_fault_logic_Enable = temp[9];     // 1 = enable, 2 = disable
            halCommonSetToken(TOKEN_RTC_New_fault_logic_Enable,&RTC_New_fault_logic_Enable);
          }
        }
///////////////////////////
        else if(temp[5] == SET_FOOT_CANDLE)     // Get SLC configuration parameter command received from DCU.
        {
            pulseCounter.byte[3]= temp[6];
            pulseCounter.byte[2]= temp[7];
            pulseCounter.byte[1]= temp[8];
            pulseCounter.byte[0]= temp[9];
            Photosensor_Set_FC = pulseCounter.float_data;

            halCommonSetToken(TOKEN_Photosensor_Set_FC,&Photosensor_Set_FC);

            pulseCounter.byte[3]= temp[10];
            pulseCounter.byte[2]= temp[11];
            pulseCounter.byte[1]= temp[12];
            pulseCounter.byte[0]= temp[13];
            Photosensor_Set_FC_Lamp_OFF = pulseCounter.float_data;

            halCommonSetToken(TOKEN_Photosensor_Set_FC_Lamp_OFF,&Photosensor_Set_FC_Lamp_OFF);



        }
        else if(temp[5] == GET_FOOT_CANDLE)     // Get SLC configuration parameter command received from DCU.
        {
          uChar_GET_FOOT_CANDLE = 1;

        }

//////////////////////////////
        else if(temp[5] == GET_SLC_CONFIG_PARA)     // Get SLC configuration parameter command received from DCU.
        {
          Send_Get_Slc_Config_para_Replay = 1;
        }
        else if(temp[5] == SLC_DST_CONFIG_PARA)    // Set DST rule command received from DCU.
        {
          SLC_DST_En = temp[6];

          halCommonSetToken(TOKEN_SLC_DST_En,&SLC_DST_En);
          if(SLC_DST_En == 1)
          {
            SLC_DST_Start_Month = temp[7];

            halCommonSetToken(TOKEN_SLC_DST_Start_Month,&SLC_DST_Start_Month);
            SLC_DST_Start_Rule = temp[8];

            halCommonSetToken(TOKEN_SLC_DST_Start_Rule,&SLC_DST_Start_Rule);
            SLC_DST_Start_Time = temp[9];

            halCommonSetToken(TOKEN_SLC_DST_Start_Time,&SLC_DST_Start_Time);
            SLC_DST_Stop_Month =	temp[10];

            halCommonSetToken(TOKEN_SLC_DST_Stop_Month,&SLC_DST_Stop_Month);
            SLC_DST_Stop_Rule =	temp[11];

            halCommonSetToken(TOKEN_SLC_DST_Stop_Rule,&SLC_DST_Stop_Rule);

            SLC_DST_Stop_Time =	temp[12];

            halCommonSetToken(TOKEN_SLC_DST_Stop_Time,&SLC_DST_Stop_Time);

            pulseCounter.byte[3] = temp[13];
            pulseCounter.byte[2] = temp[14];
            pulseCounter.byte[1] = temp[15];
            pulseCounter.byte[0] = temp[16];
            SLC_DST_Time_Zone_Diff = pulseCounter.float_data;

            halCommonSetToken(TOKEN_SLC_DST_Time_Zone_Diff,&SLC_DST_Time_Zone_Diff);
            SLC_DST_Rule_Enable = temp[17];

            halCommonSetToken(TOKEN_SLC_DST_Rule_Enable,&SLC_DST_Rule_Enable);
            SLC_DST_Start_Date = temp[18];

            halCommonSetToken(TOKEN_SLC_DST_Start_Date,&SLC_DST_Start_Date);
            SLC_DST_Stop_Date =	temp[19];

            halCommonSetToken(TOKEN_SLC_DST_Stop_Date,&SLC_DST_Stop_Date);

            if(SLC_DST_Rule_Enable == 1)       // if command received for rule then find Date from rule.
            {
              SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule,SLC_DST_Start_Month);
              SLC_DST_R_Stop_Date =  find_dst_date_from_rule(SLC_DST_Stop_Rule,SLC_DST_Stop_Month);
            }
            else
            {
              SLC_DST_R_Start_Date = SLC_DST_Start_Date;
              SLC_DST_R_Stop_Date =  SLC_DST_Stop_Date;
            }

          }									
        }
        else if(temp[5] == GET_SLC_DST_CONFIG_PARA)   // Get SLC DST Parameters message received from DCU.
        {
          send_get_slc_dst_config_para = 1;	
        }
        else if(temp[5] == GET_SLC_MIX_MODE_SCH)      // Get SLC Mix-mode command received from DCU.
        {
          Send_Get_Mix_Mode_Sch = 1;
          Get_Mix_mode_Sch_Loc = temp[6];           // copy schedule location which we need to send.
        }
        else if(temp[5] == SLC_MMOD)        // SLC new manual mode received from LG.
        {
          if(temp[6] < 101)                // if dimming value is in between 0 to 100 then start new manual mode working.
          {
            SLC_New_Manula_Mode_En = 1;
            SLC_New_Manual_Mode_Timer = (temp[7] * 256) + temp[8];      // load new manual timer from message.
            halCommonSetToken(TOKEN_SLC_New_Manual_Mode_Timer,&SLC_New_Manual_Mode_Timer);
          }
          else
          {
            SLC_New_Manula_Mode_En = 0;   // disable new manual mode	
          }

          SLC_New_Manula_Mode_Val = temp[6];
          halCommonSetToken(TOKEN_SLC_New_Manula_Mode_Val,&SLC_New_Manula_Mode_Val);
        }
        else if(temp[5] == SET_LINK_KEY_SLC)     // Set Link key received from LG.
        {
          Link_Key[0] = temp[6];	
          Link_Key[1] = temp[7];	
          Link_Key[2] = temp[8];	
          Link_Key[3] = temp[9];	
          Link_Key[4] = temp[10];	
          Link_Key[5] = temp[11];	
          Link_Key[6] = temp[12];	
          Link_Key[7] = temp[13];	
          Link_Key[8] = temp[14];	
          Link_Key[9] = temp[15];	
          Link_Key[10] = temp[16];	
          Link_Key[11] = temp[17];	
          Link_Key[12] = temp[18];	
          Link_Key[13] = temp[19];	
          Link_Key[14] = temp[20];	
          Link_Key[15] = temp[21];	
          halCommonSetToken(TOKEN_TLink_Key,&Link_Key);
        }
        else if(temp[5] == GET_LINK_KEY_SLC)      // Get Link key command received from DCU.
        {
          Send_Link_key = 1;
        }	
        else if(temp[5] == CHANGE_KEY)     // Change key command received from DCU.
        {
          change_Link_key = 1;
          Enable_Security =1;
           halCommonSetToken(TOKEN_Enable_Security,&Enable_Security);
          change_Link_key_timer = 0;	      // after receive command leave netwrok after 30 sec.
        }
        else if(temp[5] == PHOTOCELL_CONFIG)   // Set photocell falut parameters configuration.
        {
          Photocell_steady_timeout_Val = temp[6];
          halCommonSetToken(TOKEN_Photocell_steady_timeout_Val,&Photocell_steady_timeout_Val);

          photo_cell_toggel_counter_Val = temp[7];
          halCommonSetToken(TOKEN_photo_cell_toggel_counter_Val,&photo_cell_toggel_counter_Val);

          photo_cell_toggel_timer_Val = temp[8];
          halCommonSetToken(TOKEN_photo_cell_toggel_timer_Val,&photo_cell_toggel_timer_Val);

          Photo_cell_Frequency = temp[9];
          halCommonSetToken(TOKEN_Photo_cell_Frequency,&Photo_cell_Frequency);

          pulseCounter.byte[1] = temp[10];
          pulseCounter.byte[0] = temp[11];
          Photocell_unsteady_timeout_Val = pulseCounter.int_data;
          halCommonSetToken(TOKEN_Photocell_unsteady_timeout_Val,&Photocell_unsteady_timeout_Val);

          Photocell_Detect_Time = Photocell_unsteady_timeout_Val;
        }
        else if(temp[5] ==  GET_OTA_DATA)  
        {
          send_get_OTA_Data = 1;
        }
        else if(temp[5] ==  GET_Rs485_DI2_CONFIG)  
        {
          send_get_Rs485_DI2_Config = 1;
        } 
        else if(temp[5] ==  GET_FAULT_REMOVE_CONFIG)  
        {
          send_GET_FAULT_REMOVE_CONFIG = 1;
        }         
        else if(temp[5] ==  GET_LastGasp_CONFIG)  
        {
          send_GET_LastGasp_CONFIG = 1;
        }         
        else if(temp[5] ==  GET_PHOTOCELL_CONFIG)  // Get photocell fault configuration command received.
        {
          send_get_photocell_config = 1;
        }
        else if(temp[5] ==  GET_AUTO_SEND_READ_DATA)  // Get photocell fault configuration command received.
        {
          send_get_Auto_send_config = 1;
        }        
        else if(temp[5] ==  SET_NETWORK_SEARCH_PARA)   // Set network search parameters received.
        {
          if(temp[7] != 0)        // if value not 0 then load it to final variable else discard message.
          {
                SLC_Start_netwrok_Search_Timeout = temp[6];  // after all channel scan add this much of delay in search again.
                Network_Scan_Cnt_Time_out = temp[7] * 3600;  // leave current network after this timeout if no message received from DCU.

                halCommonSetToken(TOKEN_SLC_Start_netwrok_Search_Timeout,&SLC_Start_netwrok_Search_Timeout);
                halCommonSetToken(TOKEN_Network_Scan_Cnt_Time_out,&Network_Scan_Cnt_Time_out);
          }
        }
        else if(temp[5] ==  GET_NETWORK_SEARCH_PARA)   // Get network search parameter received from DCU.
        {
                send_get_network_search_para = 1;
        }
        else if(temp[5] == COMMISSIONING_PARA)   // commissioning over command reecived from reader.
        {

                Valid_DCU_Flag =0;              // start search for new DCU.
                halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);

                scann_chan = 0x7FFE;            // mask channel 11 which use for commissioning.
                channel_set=scann_chan<<11;
                halCommonSetToken(TOKEN_scann_chan,&scann_chan);
                if(temp[6] == 0x63)
                {
                    GPS_Read_Enable = 1;
                    halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
                    GPS_Read_Enable = 0; //gps will read after power cycle to avoid the lat/long update in the test routine.
                    Commissioning_flag = 3;
                    halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);
                    

                }
                else
                {
                    Commissioning_flag = 0;
                    halCommonSetToken(TOKEN_Commissioning_flag,&Commissioning_flag);

                    SLC_Start_netwrok_Search_counter = 0;
                    SLC_intermetent_netwrok_Search_Timeout = 0;
                    Id_frame_received = 0;
                    RF_communication_check_Counter = 0;
                    RF_communication_check_Counter = 24; //need to leave network after 5-6 sec
                    Id_Frame_Cnt =3;

                    GPS_Read_Enable = 1;
                    halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
                    //                #ifndef ISLC3300_T8_V2
                    //                    automode = 1;     // set SLC mode to Photocell mode after commissioning over.
                    //                    halCommonSetToken(TOKEN_automode, &automode);
                    //                #endif

                /*
                    networkLeaveCommand commented since valid DCU is 0 now so it will leave after 27 second
                 */
                    networkLeaveCommand();  //leave current network.
                }

        }
        else if(temp[5] == GPS_CONFIG)
        {
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
                if((temp[7] == 1) || (temp[7] == 2))
                {
                  if(GPS_Read_Enable == 1)
                  {
                    GPS_Rescan = temp[7];
                    if(GPS_Rescan == 1)
                    {
                      need_Average_value_Gps = 1;
                    }
                    else
                    {
                      need_Average_value_Gps = 0;
                    }
                    GPS_read_success_counter = 0;
                    need_to_send_GPS_command = 1;
                    averaging_Timer = 0;
                    Start_averaging_of_GPS = 0;
                    averaging_counter = 0;
                  }
                }
                else
                {
                  if(temp[6] == 1)
                  {
                    GPS_Read_Enable = 1;
                    halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
                  }
                  else
                  {
                    GPS_Read_Enable = 0;
                    halCommonSetToken(TOKEN_GPS_Read_Enable,&GPS_Read_Enable);
                    //need_to_send_Shutdown_command = 1;

                    if(Packate_send_success == 0)
                    {
                      shutdown_GPS();
                    }
                  }



                  GPS_Wake_timer = (temp[8] * 256) + temp[9];
                  halCommonSetToken(TOKEN_GPS_Wake_timer,&GPS_Wake_timer);

                  No_of_setalite_threshold = temp[10];
                  halCommonSetToken(TOKEN_No_of_setalite_threshold,&No_of_setalite_threshold);
                  Setalite_angle_threshold = temp[11];
                  halCommonSetToken(TOKEN_Setalite_angle_threshold,&Setalite_angle_threshold);
                  setalite_Rssi_threshold = temp[12];
                  halCommonSetToken(TOKEN_setalite_Rssi_threshold,&setalite_Rssi_threshold);
                  Maximum_no_of_Setalite_reading = temp[13];
                  halCommonSetToken(TOKEN_Maximum_no_of_Setalite_reading,&Maximum_no_of_Setalite_reading);

                  pulseCounter.byte[3] = temp[14];
                  pulseCounter.byte[2] = temp[15];
                  pulseCounter.byte[1] = temp[16];
                  pulseCounter.byte[0] = temp[17];
                  HDOP_Gps_threshold = pulseCounter.float_data;
                  halCommonSetToken(TOKEN_HDOP_Gps_threshold,&HDOP_Gps_threshold);

                  Check_Sbas_Enable = temp[18];
                  halCommonSetToken(TOKEN_Check_Sbas_Enable,&Check_Sbas_Enable);

                  Sbas_setalite_Rssi_threshold = temp[19];
                  halCommonSetToken(TOKEN_Sbas_setalite_Rssi_threshold,&Sbas_setalite_Rssi_threshold);

                  Auto_Gps_Data_Send = temp[20];
                  halCommonSetToken(TOKEN_Auto_Gps_Data_Send,&Auto_Gps_Data_Send);
                }

#endif
        }
        else if(temp[5] == GET_GPS_CONFIG)
        {
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)||defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))
          send_get_Gps_Config = 1;
#endif
        }
  ///////////////////////////////
        else if(temp[5] == SAVE_CALIBRATION_DATA)
        {
            if(temp[6] == 0x92)
            {
                pulseCounter.byte[3] = temp[7];
                pulseCounter.byte[2] = temp[8];
                pulseCounter.byte[1] = temp[9];
                pulseCounter.byte[0] = temp[10];
                Vr_Cal = pulseCounter.float_data;
                halCommonSetToken(TOKEN_Vr_Cal,&Vr_Cal);
            }
            if(temp[11] == 0x93)
            {
                pulseCounter.byte[3] = temp[12];
                pulseCounter.byte[2] = temp[13];
                pulseCounter.byte[1] = temp[14];
                pulseCounter.byte[0] = temp[15];
                Ir_Cal = pulseCounter.float_data;
                halCommonSetToken(TOKEN_Ir_Cal,&Ir_Cal);
            }
            if(temp[16] == 0x94)
            {
                  pulseCounter.byte[3] = temp[17];
                  pulseCounter.byte[2] = temp[18];
                  pulseCounter.byte[1] = temp[19];
                  pulseCounter.byte[0] = temp[20];
                  KW_Cal = pulseCounter.float_data;

                halCommonSetToken(TOKEN_KW_Cal,&KW_Cal);
            }
//            vrdoub = Vr_Cal;
//            irdoub = Ir_Cal;
//            pulses_r = (unsigned long) KW_Cal;
//            set_mf_energy();
                /*Need to put mf1,2.3 here to avoid of malfunction of KWH*/
                mf1 = VRN / Vr_Cal;
                mf2 = IR / Ir_Cal;
                mf3 = (PULSES/KW_Cal);
        }
        else if(temp[5] == GATE_CALIBRATION_DATA)
        {
            Get_calibration_data =1;
            if(temp[6] == 'H') //get this then not respond to DCU
            {
              send_to_Router =1;
            }
            else
            {
              send_to_Router =0;
            }

        }
        /////////////////////////
  ///////////////1.0.19/////////
//        else if(temp[5] == SET_TIME_OF_USE)
//        {
//            no_of_Sch =temp[6];
//            //no_of_Sch =no_of_Sch-1;
//          //for(g = 0;g<no_of_config;g++)
//
//            Time_Slot.Slot_Start_Date[no_of_Sch] =temp[7];
//            halCommonSetToken(TOKEN_Slot_Start_Date,&Time_Slot.Slot_Start_Date[0]);
//
//            Time_Slot.Slot_Start_Month[no_of_Sch]=temp[8];
//            halCommonSetToken(TOKEN_Slot_Start_Month,&Time_Slot.Slot_Start_Month[0]);
//
//            Time_Slot.Slot_Start_Year[no_of_Sch] =temp[9];
//            halCommonSetToken(TOKEN_Slot_Start_Year,&Time_Slot.Slot_Start_Year[0]);
//
//            Time_Slot.Slot_End_Date[no_of_Sch] =temp[10];
//            halCommonSetToken(TOKEN_Slot_End_Date,&Time_Slot.Slot_End_Date[0]);
//
//            Time_Slot.Slot_End_Month[no_of_Sch]=temp[11];
//            halCommonSetToken(TOKEN_Slot_End_Month,&Time_Slot.Slot_End_Month[0]);
//
//            Time_Slot.Slot_End_Year[no_of_Sch] =temp[12];
//            halCommonSetToken(TOKEN_Slot_End_Year,&Time_Slot.Slot_End_Year[0]);
//
//            for(m =0;m<2;m++)
//            {
//                Time_Slot.Slot_Start_Hour[(no_of_Sch*6)+((m*3)+0)] =temp[(m*12)+13];
//                Time_Slot.Slot_Start_Min[(no_of_Sch*6)+((m*3)+0)] =temp[(m*12)+14];
//
//                Time_Slot.Slot_End_Hour[(no_of_Sch*6)+((m*3)+0)] =temp[(m*12)+15];
//                Time_Slot.Slot_End_Min[(no_of_Sch*6)+((m*3)+0)] =temp[(m*12)+16];
//
//                Time_Slot.Slot_Start_Hour[(no_of_Sch*6)+((m*3)+1)] =temp[(m*12)+17];
//                Time_Slot.Slot_Start_Min[(no_of_Sch*6)+((m*3)+1)] =temp[(m*12)+18];
//
//                Time_Slot.Slot_End_Hour[(no_of_Sch*6)+((m*3)+1)] =temp[(m*12)+19];
//                Time_Slot.Slot_End_Min[(no_of_Sch*6)+((m*3)+1)] =temp[(m*12)+20];
//
//                Time_Slot.Slot_Start_Hour[(no_of_Sch*6)+((m*3)+2)] =temp[(m*12)+21];
//                Time_Slot.Slot_Start_Min[(no_of_Sch*6)+((m*3)+2)] =temp[(m*12)+22];
//
//                Time_Slot.Slot_End_Hour[(no_of_Sch*6)+((m*3)+2)] =temp[(m*12)+23];
//                Time_Slot.Slot_End_Min[(no_of_Sch*6)+((m*3)+2)] =temp[(m*12)+24];
//            }
//            halCommonSetToken(TOKEN_Slot_Start_Hour,&Time_Slot.Slot_Start_Hour[0]);
//            halCommonSetToken(TOKEN_Slot_Start_Min,&Time_Slot.Slot_Start_Min[0]);
//            halCommonSetToken(TOKEN_Slot_End_Hour,&Time_Slot.Slot_End_Hour[0]);
//            halCommonSetToken(TOKEN_Slot_End_Min,&Time_Slot.Slot_End_Min[0]);
//
//           // for(b=0;b<12;b++){
//            for(b=0;b<6;b++){
//                Time_Slot.Slot_Pulses[(no_of_Sch*6)+ b] =0;
//            }
////            halCommonSetToken(TOKEN_Slot_Pulses,&Time_Slot.Slot_Pulses[0]);
//
//          }
//          else if(temp[5] == GET_TIME_OF_USE)
//          {
//            Send_Time_Of_Use_Sch =1;
//            Get_Sch_no =temp[6];
//          }
//          else if(temp[5] == GET_KWH_SLOT)
//          {
//            Send_Kwh_Slot_To_Dcu =1;
//            Get_Sch_no= temp[6];
//          }
        ///////////////////
          else if(temp[5] == PANIC_CMD_START)
          {
//            Lamp_On_Time= (temp[6]*256) + temp[7];

//            Lamp_OFF_Time= (temp[8]*256) + temp[9];

            if(temp[7]!= 0){
                Lamp_ON_First_Responder =0;
                Blink_Freq_Time =temp[6];
                Lamp_Trigger_Time =temp[7];
                Lamp_Trigger_Time =(Blink_Freq_Time/Lamp_Trigger_Time);
                Lamp_Trigger_Time =Lamp_Trigger_Time*1000;
                Lamp_On_Time = (unsigned int)Lamp_Trigger_Time;
                Lamp_On_Time = Lamp_On_Time /2;
            }
            else
            {
                Lamp_ON_First_Responder =1;
            }
            //temp[8] for spare
            
            Panic_Dim_Value = temp[9];

            Panic_Time_Out =(temp[10]*256) + temp[11];
            Total_Panic_SLC = temp[12];
            if(amr_id <= 65535)
            {
              for(HG =0;HG<Total_Panic_SLC;HG++)
              {
                  SLC_Find_For_Panic = (temp[13+(HG*2)]*256) + temp[14+(HG*2)];
                  if(SLC_Find_For_Panic == amr_id)
                  {
                      if(automode != PANIC_MODE)
                      {
                          previous_mode = automode;
                      }
                      automode =PANIC_MODE;
                      Panic_Cnt =0;
                      Panic_Lamp_On_Cnt =0;
                      Panic_Lamp_OFF_Cnt =0;
                  }
              }
            }
            else
            {
                HM = 13 + (Total_Panic_SLC*2);   
                for(HG =0;HG<temp[8];HG++)
                {
                    SLC_Find_For_Panic = (temp[(HM)+(HG*2)]*256) + temp[((HM)+1)+(HG*2)];
                    if(SLC_Find_For_Panic == Blue_Light_Id)
                    {
                        if(automode != PANIC_MODE)
                        {
                            previous_mode = automode;
                        }
                        automode =PANIC_MODE;
                        Panic_Cnt =0;
                        Panic_Lamp_On_Cnt =0;
                        Panic_Lamp_OFF_Cnt =0;
                    }
                }
            }
            //////////////////////////
          }
          else if(temp[5] == PANIC_CMD_STOP)
          {
         ////////////////////////////////////////   
            if(amr_id <= 65535)
            {
              for(HG =0;HG<Total_Panic_SLC;HG++)
              {
                  SLC_Find_For_Panic = (temp[13+(HG*2)]*256) + temp[14+(HG*2)];
                  if(SLC_Find_For_Panic == amr_id)
                  {
                      if(automode == PANIC_MODE)
                      {
                          automode =previous_mode;
                          break;
                      }
                  }
              }
            }
            else
            {
                HM = 13 + (Total_Panic_SLC*2);   
                for(HG =0;HG<temp[8];HG++)
                {
                    SLC_Find_For_Panic = (temp[(HM)+(HG*2)]*256) + temp[((HM)+1)+(HG*2)];
                    if(SLC_Find_For_Panic == Blue_Light_Id)
                    {
                      if(automode == PANIC_MODE)
                      {
                          automode =previous_mode;
                          break;
                      }
                    }
                }
              
            }
         ///////////////////////////////////////         
          }
        //////////CURV_TYPE/////////
        //#if(defined(DALI_SUPPORT))
////////////////////////////////////////////
        else if((temp[5] == AUTO_ADDRESS_CMD) && (dali_support == 1))
        {
            Cmd_Rcv_To_Change_Curv_Ype =2;
            cheak_emt_timer_sec =0;

        }
        else if((temp[5] == READ_SCENE_FROM_DALI) && (dali_support == 1))
        {


            Send_Read_Scene_From_Dali =2;
            cheak_emt_timer_sec =0;

        }
        else if((temp[5] == WRITE_SCENE_TO_DALI) && (dali_support == 1))
        {
            //ask to sandip...
          for(HS=0;HS<63;HS++)
          {
              scenes_value_write = temp[6 + HS];
              if(HS <= 15)
              {
                  scenes_write3(HS,scenes_value_write,5);
              }
              else if((HS >15)&&(HS<=31))
              {
                  scenes_write3(HS-16,scenes_value_write,7);
              }
              else if((HS >31)&&(HS<=47))
              {
                  scenes_write3(HS-32,scenes_value_write,9);
              }
              else if((HS >47)&&(HS<=63))
              {
                  scenes_write3(HS-48,scenes_value_write,11);
              }



         }


        }
////////////////////////////////////////////
          else if((temp[5] == CURV_TYPE_SET ) && (dali_support == 1))
          {
            if((temp[6] ==0)||(temp[6] ==1)){
                CURV_TYPE = temp[6];

                halResetWatchdog();
                halCommonSetToken(TOKEN_CURV_TYPE,&CURV_TYPE);
                if(set_do_flag == 1){
//                  cheak_emt_timer_sec=0;
//
//                    while(cheak_emt_timer_sec<2)
//                    {
//                      Ember_Stack_run_Fun();
//                    }

                    set_Linear_Logarithmic_dimming(CURV_TYPE);
                }
                else
                {
                    Cmd_Rcv_To_Change_Curv_Ype =1;
                }
            }

          }
          else if((temp[5] == CURV_TYPE_GET) && (dali_support == 1))
          {
            Send_curv_type =1;
          }
       // #endif
          else if(temp[5] == RESET_CMD)
          {
            //if((temp[6] =='R')&&(temp[7]=='E')&&(temp[8]=='S')&&(temp[9]=='E')&&(temp[10]=='T'))
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
              halResetWatchdog();
              calculate_kwh_LampOFF();             // store last reading of KWH in to NV
              Calculate_burnhour_Lampoff();        // store last reading of Burn_Hour in to NV
              Soft_Reboot = 235;
              cheak_emt_timer_sec=0;
//              while(cheak_emt_timer_sec<5)
//              {
//                halResetWatchdog();
//                Ember_Stack_run_Fun();
//              }
              //halReboot();               // soft-reset command for Ember

            }
          }
          else if(temp[5] == MIX_SCHEDULE_RESET_CMD)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
                Erase_Mix_Mode();
            }
          }
          else if(temp[5] == MOTION_SCHEDULE_RESET_CMD)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
                Erase_Motion_perameter_And_Set_Default();
            }
          }

          else if(temp[5] == EM_RESET_CMD)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
              energy_meter_ok = 0;                                   // set flag for EM Fault
              error_condition1.bits.b4 = 0;                          // set Bit in digital status for EM Fault
              read_a_reg(0x01,0x1e);                                 // read Status resistor from EM chip
              pulseCounter.byte[0] = datalow;
              pulseCounter.byte[1] = datamid;
              pulseCounter.byte[2] = datahigh;
              pulseCounter.byte[3] = 0x00;
              Em_status_reg = pulseCounter.long_data;                // Clear Status resistor in EM chip
              write_to_reg(0x01,0x5e,0x00,0x00,0x80);
              init_cs5463();									                                // Reset EM engine
              energy_time = 1;
            }
          }
          else if(temp[5] == ID_FRAME_STOP)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
                if(Valid_DCU_Flag == 1)
                {
                    Id_frame_received =1;
                }

            }
          }
///////////////////////
          else if(temp[5] == AUTO_SEND_READ_DATA)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
              
              Old_Hr =Time.Hour;              
              Auto_Send_Enable_Disable =temp[8];
              if(temp[9] != 25)
              {
                Auto_Send_Lograte_Interval= temp[9];
              }
              if(temp[10] > 2)
              {
                Auto_Send_Retry=temp[10];
              }
              if(temp[11] > 2)
              {                
                Read_Data_send_Timer =temp[11];
              }
              pulseCounter.byte[1]=temp[12];
              pulseCounter.byte[0]=temp[13];
              if(pulseCounter.int_data!=0xFFFF)
              {
                if(pulseCounter.int_data != 0xfffe)
                {
                  Time_Slice =pulseCounter.int_data;
                }
              }
              else
              {
                Time_Slice = Temp_Time_Slice;
              }
              
              if(temp[14] != 0xff)
              {  
                Configured_Event.Val= temp[14];
              }
          
                idimmer_longaddress[0] = Auto_Send_Enable_Disable;
                idimmer_longaddress[1] = Auto_Send_Lograte_Interval;
                idimmer_longaddress[2] = Auto_Send_Retry;
                idimmer_longaddress[3] = Read_Data_send_Timer;
                idimmer_longaddress[4] = Time_Slice/256;;//pulseCounter.byte[1]; //Time_Slice Higher Byte
                idimmer_longaddress[5] = Time_Slice%256;;//pulseCounter.byte[0]; //Time_Slice Lower Byte                
                idimmer_longaddress[6] = Configured_Event.Val;
                idimmer_longaddress[7] =0;
                idimmer_longaddress[8] =0;
                idimmer_longaddress[9] =0;
                halCommonSetToken(TOKEN_idimmer_longaddress,&idimmer_longaddress[0]);
                
                
              
            }
            
          }
////////////////////        
        else if(temp[5] == AUTO_NETWORK_LEAVE)
        {
            if((temp[6]== 0xAf)&&(temp[7]== 0xfA))
            {
              Network_leave_Flag = temp[8];
              halCommonSetToken(TOKEN_idimmer_en,&Network_leave_Flag);
              pulseCounter.byte[1]=temp[9];
              pulseCounter.byte[0]=temp[10];
              Network_Leave_Hr_Value =pulseCounter.int_data;
              halCommonSetToken(TOKEN_idimmer_id,&Network_Leave_Hr_Value);
            }
        }
        else if(temp[5] == GET_AUTO_NETWORK_LEAVE)
        {
          if((temp[6]== 0xAf)&&(temp[7]== 0xfA))
          {
            GET_AUTO_NETWORK_LEAVE_Flag=1;
          }
          
        }
        
///////////////////////////    
        else if(temp[5] == DALI_AO_CONFIG)
        {
            if((temp[6]== 0xAf)&&(temp[7]== 0xfA))
            {
              DimmerDriverSelectionProcess = temp[8];
              if((DimmerDriverSelectionProcess == 0)||(DimmerDriverSelectionProcess == 1))
              {
                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(0x02);
                dali_support = 2;
              }
              else if(DimmerDriverSelectionProcess == 2)
              {
                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(0x01);
                dali_support = 1;
          #if defined(ISLC_3100_V7_7)      
                halSetLed(MUX_A0);	 //0	
                halClearLed(MUX_A1);  //1
          #elif defined(ISLC_3100_V7_9)
                halSetLed(MUX_A0);   // Set=0=dali & clear=1=AO  
          #endif      
                halStackIndicatePresence();
                lamp_off_on_timer = Lamp_off_on_Time;      
                lamp_on_first=0;
                default_dali=0;
                if(set_do_flag==1)
                {
                  Set_Do(1);
                }
              }
              else if(DimmerDriverSelectionProcess == 3)
              {
                DimmerDriver_SELECTION = (DimmerDriverSelectionProcess<<4)|(0x00);
                dali_support = 0;
           #if defined(ISLC_3100_V7_7)     
                halSetLed(MUX_A1);	 //0	
                halClearLed(MUX_A0);  //1
           #elif defined(ISLC_3100_V7_9)
                halClearLed(MUX_A0);   // Set=0=dali & clear=1=AO 
           #endif     
                halStackIndicatePresence();
                lamp_on_first=0;
                default_dali=0;
                if(set_do_flag==1)
                {
                  Set_Do(1);
                }                
              }             
              halCommonSetToken(TOKEN_DimmerDriver_SELECTION,&DimmerDriver_SELECTION); 
              if(temp[9]==1) // it is Reboot Flag if 1 than softreboot SLC
              {
                halResetWatchdog();
                calculate_kwh_LampOFF();             // store last reading of KWH in to NV
                Calculate_burnhour_Lampoff();        // store last reading of Burn_Hour in to NV
                Soft_Reboot = 235;
                cheak_emt_timer_sec=0;
              }
            }
        }
        else if(temp[5] == GET_DALI_AO_CONFIG)
        {
        
          if((temp[6]== 0xAf)&&(temp[7]== 0xfA))
          {
            GET_DALI_AO_CONFIG_Flag=1;
          }    
        }               
        
          else if(temp[5] == CONF_CMD_IN_TEST_MODE)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {
                automode = temp[8];
                halCommonSetToken(TOKEN_automode,&automode);
                previous_mode = automode;
                halCommonSetToken(TOKEN_previous_mode, &previous_mode);   // store mode in to NVM.


                pulseCounter.byte[3] = temp[9];
                pulseCounter.byte[2] = temp[10];
                pulseCounter.byte[1] = temp[11];
                pulseCounter.byte[0] = temp[12];
                Latitude = pulseCounter.float_data;       // Store Latitude
                halCommonSetToken(TOKEN_Latitude,&Latitude);

                pulseCounter.byte[3] = temp[13];
                pulseCounter.byte[2] = temp[14];
                pulseCounter.byte[1] = temp[15];
                pulseCounter.byte[0] = temp[16];
                longitude = pulseCounter.float_data;        // Store Longitude
                halCommonSetToken(TOKEN_longitude,&longitude);

                pulseCounter.byte[3] = temp[17];
                pulseCounter.byte[2] = temp[18];
                pulseCounter.byte[1] = temp[19];
                pulseCounter.byte[0] = temp[20];
                utcOffset = pulseCounter.float_data;         // Store TimeZone
                halCommonSetToken(TOKEN_utcOffset,&utcOffset);

                pulseCounter.byte[1] = temp[21];
                pulseCounter.byte[0] = temp[22];
                Sunset_delay = pulseCounter.int_data;        // Store Sunset delay
                halCommonSetToken(TOKEN_Sunset_delay,&Sunset_delay);

                pulseCounter.byte[1] = temp[23];
                pulseCounter.byte[0] = temp[24];
                Sunrise_delay = pulseCounter.int_data;       // Store Sun rise delay
                halCommonSetToken(TOKEN_Sunrise_delay,&Sunrise_delay);

                GetSCHTimeFrom_LAT_LOG();                   // compute Astro time from lat/long
                Astro_sSch[0].cStartHour = sunsetTime/3600;						
                Astro_sSch[0].cStartMin  = (sunsetTime%3600)/60;		
                Astro_sSch[0].cStopHour  = 23;							
                Astro_sSch[0].cStopMin   = 59;							
                Astro_sSch[0].bSchFlag   = 1;							

                Astro_sSch[1].cStartHour = 00;							
                Astro_sSch[1].cStartMin  = 00;							
                Astro_sSch[1].cStopHour  = sunriseTime/3600;			
                Astro_sSch[1].cStopMin   = (sunriseTime%3600)/60;											
                Astro_sSch[1].bSchFlag   = 1;							
                halCommonSetToken(TOKEN_schedule_Astro_sSch,&Astro_sSch[0]);
              //////////////
                SLC_DST_En = temp[25];

                halCommonSetToken(TOKEN_SLC_DST_En,&SLC_DST_En);
                if(SLC_DST_En == 1)
                {
                    SLC_DST_Start_Month = temp[26];
                    halCommonSetToken(TOKEN_SLC_DST_Start_Month,&SLC_DST_Start_Month);
                    SLC_DST_Start_Rule = temp[27];
                    halCommonSetToken(TOKEN_SLC_DST_Start_Rule,&SLC_DST_Start_Rule);
                    SLC_DST_Start_Time = temp[28];
                    halCommonSetToken(TOKEN_SLC_DST_Start_Time,&SLC_DST_Start_Time);
                    SLC_DST_Stop_Month =temp[29];
                    halCommonSetToken(TOKEN_SLC_DST_Stop_Month,&SLC_DST_Stop_Month);
                    SLC_DST_Stop_Rule =	temp[30];
                    halCommonSetToken(TOKEN_SLC_DST_Stop_Rule,&SLC_DST_Stop_Rule);
                    SLC_DST_Stop_Time =	temp[31];
                    halCommonSetToken(TOKEN_SLC_DST_Stop_Time,&SLC_DST_Stop_Time);
                    pulseCounter.byte[3] = temp[32];
                    pulseCounter.byte[2] = temp[33];
                    pulseCounter.byte[1] = temp[34];
                    pulseCounter.byte[0] = temp[35];
                    SLC_DST_Time_Zone_Diff = pulseCounter.float_data;
                    halCommonSetToken(TOKEN_SLC_DST_Time_Zone_Diff,&SLC_DST_Time_Zone_Diff);
                    SLC_DST_Rule_Enable = temp[36];
                    halCommonSetToken(TOKEN_SLC_DST_Rule_Enable,&SLC_DST_Rule_Enable);
                    SLC_DST_Start_Date = temp[37];
                    halCommonSetToken(TOKEN_SLC_DST_Start_Date,&SLC_DST_Start_Date);
                    SLC_DST_Stop_Date =	temp[38];
                    halCommonSetToken(TOKEN_SLC_DST_Stop_Date,&SLC_DST_Stop_Date);
                    if(SLC_DST_Rule_Enable == 1)       // if command received for rule then find Date from rule.
                    {
                        SLC_DST_R_Start_Date = find_dst_date_from_rule(SLC_DST_Start_Rule,SLC_DST_Start_Month);
                        SLC_DST_R_Stop_Date =  find_dst_date_from_rule(SLC_DST_Stop_Rule,SLC_DST_Stop_Month);
                    }
                    else
                    {
                        SLC_DST_R_Start_Date = SLC_DST_Start_Date;
                        SLC_DST_R_Stop_Date =  SLC_DST_Stop_Date;
                    }

                }
              //////////////
            }
          }
          else if(temp[5] == GET_CONF_CMD_IN_TEST_MODE)
          {
            Test_Config_Data_Send=1;
          }
          else if(temp[5] == Rs485_DI2_CONFIG)
          {
              if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
              {
                  RS485_0_Or_DI2_1 = temp[8];
                  Select_RS485_Or_DI2(RS485_0_Or_DI2_1);
                  halCommonSetToken(TOKEN_RS485_0_Or_DI2_1,&RS485_0_Or_DI2_1);
              }
          } 
          else if(temp[5] == FAULT_REMOVE_CONFIG)
          {
              if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
              {
                  Lamp_Balast_fault_Remove_Time = temp[8];
                  halCommonSetToken(TOKEN_Lamp_Balast_fault_Remove_Time,&Lamp_Balast_fault_Remove_Time);
              }
          }         
          else if(temp[5] == LastGasp_CONFIG)
          {
              if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
              {
                  Last_Gsp_Enable = temp[8];
                  halCommonSetToken(TOKEN_Last_Gsp_Enable,&Last_Gsp_Enable); // Todo : need to store in lastgasp token
              }
          }         
          else if(temp[5] == OTA_CONFIG)
          {
              if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
              {
                  OTA_Master = temp[8];		//1 Byte	1 = master 0 = slave
                 // pulseCounter.byte[1]= temp[9];
                 // pulseCounter.byte[0]= temp[10];
                 // = pulseCounter.int_data;                //2 Byte 		reprecent in second (delay According to Auto_Send data)
                  
                  pulseCounter.byte[1]= temp[11];
                  pulseCounter.byte[0]= temp[12];
                  EMBER_AF_PLUGIN_OTA_CLIENT_DOWNLOAD_DELAY_MS= pulseCounter.int_data;            //2 Byte
//                  otaClientServerDiscoveryDelay_M = temp[13];		        //1 Byte
//                  otaClientQueryDelay_M	= temp[14];		                //1 Byte
//                  otaClientDownloadPercentageUpdateEvent= temp[15];	        //1 Byte		"it is use to generate event when percentage of download complite" 
                  otaClientEraseOtaFile	= temp[16];		                //1 Byte              
                  Flag_CommandReceivedTORetryMissedOffset = temp[17];              //1 Byte
                  if(Flag_CommandReceivedTORetryMissedOffset==1 && OTA_Master==0)
                  {
                    State_OtaBroadcast = WAIT_FOR_COMMAND_TO_RETRY_MISSED_OFFSET;
                  }
                  otaClientUpgradeNewFirmware = temp[18];	                //1 Byte
                  if(otaClientEraseOtaFile == 1)
                  {
                    //emberAfOtaStorageDriverInvalidateImageCallback();
                    emAfOtaClientStop();  // 
//                    for(i=0;i<420;i++) 
//                    {
//                      offsetArray[i]=0;   //Flash_buffer
//                    }
//                    State_OtaBroadcast = RECEIVE_BROADCAST_OTA;
//                    Flag_CommandReceivedTORetryMissedOffset = 0;
//                    otaClientEraseOtaFile = 0;
//                    otaClientUpgradeNewFirmware = 0;
//                    otaClientStart = 0;
//                    otaClientServerDiscoveryComplete = 0;
//                    otaClientImageQueryComplete = 0;
//                    otaClientDownloadPercentage = 0;  //  
//                    otaClientNumberOfMissedOffsetBlock =0;
//                    otaClientWaitForCommadToRetryMissedOffset =0;
//                    otaClientDownLoadComplete =0;
//                    otaClientAlreadyUpgraded =0; //otaClientLastByteMaskIndex = 0;
//                    otaClientVerificationComplete =0;
                  }                   
                  otaClientStart = temp[19];				        //1 Byte
                  otaClientAlreadyUpgraded = 0;
              }
          }
          else if(temp[5] == BROADCAST_RESPONSE)
          {
            if((temp[6]== 0x0f)&&(temp[7]== 0xf0))
            {

                Time_Out_Check = temp[8];
                if(Time_Out_Flag ==0)
                {
                  Time_Out_Flag =1;
                  Time_Out =0;

                  if(temp[9] == 1)
                  {
                    Send_Data =1;
                  }
                  else if(temp[9] == 2)
                  {
                    send_get_mode_replay =1;
                  }
                  else if(temp[9] == 3)
                  {
                    Send_Get_Version_info_Replay =1;
                  }
                  else if(temp[9] == 4)
                  {
                    send_get_slc_dst_config_para =1;
                  }
                  else
                  {
                  }

                }


            }
          }
        ////////////////////
          else
          {
          }
        break;
      case 'A':                          // Event acknowledge received from DCu.
        pulseCounter.byte[3] = temp[5];
        pulseCounter.byte[2] = temp[6];
        pulseCounter.byte[1] = temp[7];
        pulseCounter.byte[0] = temp[8];
        ack_Rf_Track_Id = pulseCounter.long_data;     // copy track number from message and delete that event from memory.
        delet_data_from_mem(ack_Rf_Track_Id);
        rf_event_send_counter = Unicast_time_delay;
        break;
      case 'O':                                      // not used case in SLC just for testing.
        Send_COUNT = 1;
        break;
        //      case 'T':
        //        send_event_with_delay = 0;
        //        send_event_delay_timer = 0;
        //        send_data_direct_counter = 0;
        //        break;
      case 'S':
        temp[55] = 0x7F;           // schedule week day field now we have no support so set for all days.
        if(temp[55] == 0x7F)													
        {
          Sch_week_day = 0;
            for(i=0;i<10;i++)	     // copy all schedule value from message to schedule buffer and save it to NVM.											
            {
              sSch[Sch_week_day][i].cStartHour = temp[(i*5) + 5];														
              sSch[Sch_week_day][i].cStartMin = temp[(i*5) + 6];														
              sSch[Sch_week_day][i].cStopHour = temp[(i*5) + 7];														
              sSch[Sch_week_day][i].cStopMin = temp[(i*5) + 8];														
              sSch[Sch_week_day][i].bSchFlag = temp[(i*5) + 9];														
            }
            sch_day[Sch_week_day] = temp[55];
          halCommonSetToken(TOKEN_schedule,&sSch[0]);
          halCommonSetToken(TOKEN_sch_day,&sch_day[0]);
        }
        break;

      case 'Z':											
        temp[65] = 0x7F;       // Dimming schedule week day field now we have no support so set for all days.
        if(temp[65] == 0x7F)													
        {
          dimm_cmd_rec =1;
          Sch_week_day = 0;
          {
            for(i=0;i<10;i++)		// copy all dimming schedule value from message to schedule buffer and save it to NVM.													
            {
              Master_sSch[Sch_week_day][i].cStartHour = temp[(i*6) + 5];															
              Master_sSch[Sch_week_day][i].cStartMin = temp[(i*6) + 6];															
              Master_sSch[Sch_week_day][i].cStopHour = temp[(i*6) + 7];															
              Master_sSch[Sch_week_day][i].cStopMin = temp[(i*6) + 8];															
              Master_sSch[Sch_week_day][i].bSchFlag = temp[(i*6) + 9];															
              Master_sSch[Sch_week_day][i].dimvalue = temp[(i*6) + 10];															
            }
            sch_Master_day[Sch_week_day] = temp[65];
          }
          halCommonSetToken(TOKEN_Master_schedule,&Master_sSch[0]);
          halCommonSetToken(TOKEN_Master_sch_day,&sch_Master_day[0]);
        }
        break;							

      case 'X':                  // Manual mode Lamp on/off command received from DCU or reader.
        if(automode == 0)
        {

          //emberAfGuaranteedPrint("\r\n%p", "Lamp command received");
          if(temp[5]==0x01)       // if value 1 then do lamp ON.
          {
            emb_temp= temp[5];
            halCommonSetToken(TOKEN_temp,&emb_temp); //
            //emberAfGuaranteedPrint("\r\n%p", "Lamp On command received");

            Set_Do_On = 1;
          }
          else if(temp[5]==0x00)    // if value 0 then do lamp ON.
          {
            emb_temp= temp[5];
            halCommonSetToken(TOKEN_temp,&emb_temp); //
            Set_Do_On = 0;
            //emberAfGuaranteedPrint("\r\n%p", "Lamp off command received");
          }
        }
        break;					

      default:
        break;							
      }	
    }
  }
  else if((temp[0] == CMD) && (temp[1] == SLC_MIX_MODE_SCH))    // Mix-mode schedule command received from DCU.
  {
    Configure_Slc_Mix_Mode_Sch(&temp[2]);
  }
  else
  {
  }


  ///////////////////////////////////////
  if((temp[0] == 'E') && (temp[1] == 'M')&& (temp[2] == 'N')
     && (temp[3] == 'L') && (temp[4] == 'C'))                 // network leave command received from reader or DCU.
  {
    SLC_Rejoin_SameChannel =1;  // set flag to rejoin network.
    networkLeaveCommand();      // leave current network.
    Single_time_run_cmd =0;     // for all channel scan.
    halCommonSetToken(TOKEN_Single_time_run_cmd,&Single_time_run_cmd);
  }
  //////////////////////////////////////////
  else if((temp[0] == 'S') && (temp[1] == 'L')&& (temp[2] == 'G')
          && (temp[3] == 'E') && (temp[4] == 'T'))       // Get network Parameters like extended pan, short pan, channel no message received from ember getway application.
  {
    if(!Gateway_Rsp_Cnt)        // if responce send once then send second responce after 2 min. to reduce network traffic
    {
      Gateway_Rsp_Cnt =1;
      Send_gatewaye_response=1;
    }

  }
  else if((temp[0] == 'R') && (temp[1] == 'D')&& (temp[2] == 'A')
          && (temp[3] == 'T') && (temp[4] == 'R'))
  {
    read_attr=1;        // read attribute command received from ember getway applciation.

  }
  else if((temp[0] == 'W') && (temp[1] == 'A')&& (temp[2] == 'T')
          && (temp[3] == 'R'))
  {
    write_attr=1;               // Write attribute command received from ember getway application.

    Local_arry[0]=temp[4];
    Local_arry[1]=temp[5];
    Local_arry[2]=temp[6];
    Local_arry[3]=temp[7];
    Local_arry[4]=temp[8];
    Local_arry[5]='\0';

    scann_chan  = atol(Local_arry);         // convert channel from ascii to long.
    emberAfCorePrintln("\r\nscann_chan   =    %u",scann_chan);
    if(scann_chan == 0)
    {
      scann_chan =0x7FFF;
    }
    channel_set=scann_chan<<11;            //   convert channel from long to binary.
    halCommonSetToken(TOKEN_scann_chan,&scann_chan);

    convert_1 = ascii_to_hex(temp[9]);      // convert short pan from Ascii to hex.
    convert_2 = ascii_to_hex(temp[10]);
    Local_arry[0] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[11]);
    convert_2 = ascii_to_hex(temp[12]);
    Local_arry[1] = (convert_1 << 4) + convert_2;
    emberAfCorePrintln("\r\nLocal_arry=%x%x",Local_arry[0],Local_arry[1]);
    PanId=(Local_arry[0]*256)+Local_arry[1];
    halCommonSetToken(TOKEN_PanId,&PanId);    // store short pan to NVM.

    convert_1 = ascii_to_hex(temp[13]);       // convert Extended pan from Ascii to hex.
    convert_2 = ascii_to_hex(temp[14]);
    ExtPanId_set[0] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[15]);
    convert_2 = ascii_to_hex(temp[16]);
    ExtPanId_set[1] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[17]);
    convert_2 = ascii_to_hex(temp[18]);
    ExtPanId_set[2] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[19]);
    convert_2 = ascii_to_hex(temp[20]);
    ExtPanId_set[3] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[21]);
    convert_2 = ascii_to_hex(temp[22]);
    ExtPanId_set[4] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[23]);
    convert_2 = ascii_to_hex(temp[24]);
    ExtPanId_set[5] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[25]);
    convert_2 = ascii_to_hex(temp[26]);
    ExtPanId_set[6] = (convert_1 << 4) + convert_2;

    convert_1 = ascii_to_hex(temp[27]);
    convert_2 = ascii_to_hex(temp[28]);
    ExtPanId_set[7] = (convert_1 << 4) + convert_2;
    emberAfCorePrintln("\r\nLocal_arry=%x%x%x%x%x%x%x%x",ExtPanId_set[0],ExtPanId_set[1]
                       ,ExtPanId_set[2]
                         ,ExtPanId_set[3]
                           ,ExtPanId_set[4]
                             ,ExtPanId_set[5]
                               ,ExtPanId_set[6]
                                 ,ExtPanId_set[7]);

    halCommonSetToken(TOKEN_ExtPanId_set,&ExtPanId_set);  // store Extended pan to NVM.

    Id_frame_received =0;                 // set flag to search new network
    RF_communication_check_Counter = 0;

    Valid_DCU_Flag =0;                    // set flag to search new network
    halCommonSetToken(TOKEN_Valid_DCU_Flag,&Valid_DCU_Flag);

    networkLeaveCommand();       // leave current network

  }
}

/*************************************************************************
Function Name: send_fream
input: serial receive buffer.
Output: none.
Discription: send message to destination according to flage set for required
             message
*************************************************************************/
void send_fream(void)
{
  unsigned char i,j,k,g=0;
  unsigned char temp_DI1=0;
  unsigned char temp_DI2=0;
  unsigned char temp_DI3=0;
  unsigned char a =0;
//  unsigned char Cali_buff[80];
  EmberNetworkParameters networkParams;
  EmberNodeType nodeTypeResult = 0xFF;
  BYTE address[2];
  
  pulseCounter.dword = amr_id;       // voltage
   
SLC_ID_Buf[0] = ';';
SLC_ID_Buf[1] = ';';
SLC_ID_Buf[2] =pulseCounter.byte[3]; 
SLC_ID_Buf[3] =pulseCounter.byte[2]; 
SLC_ID_Buf[4] =pulseCounter.byte[1]; 
SLC_ID_Buf[5] =pulseCounter.byte[0]; 

  if(read_attr_data)            // Send responce of network attribute to application.
  {
    read_attr_data=0;
    temp_arr[0] = 'R';
    temp_arr[1] = 'D';
    temp_arr[2] = 'A';
    temp_arr[3] = 'T';
    temp_arr[4] = 'R';
    temp_arr[5] = amr_id/256;
    temp_arr[6] = amr_id%256;

    emberAfGetNetworkParameters(&nodeTypeResult, &networkParams);  // Read parameter from ember structure.

    temp_arr[7]=networkParams.radioChannel;
    temp_arr[8]=networkParams.panId/256;
    temp_arr[9]=networkParams.panId%256;
    temp_arr[10]=networkParams.extendedPanId[0];
    temp_arr[11]=networkParams.extendedPanId[1];
    temp_arr[12]=networkParams.extendedPanId[2];
    temp_arr[13]=networkParams.extendedPanId[3];
    temp_arr[14]=networkParams.extendedPanId[4];
    temp_arr[15]=networkParams.extendedPanId[5];
    temp_arr[16]=networkParams.extendedPanId[6];
    temp_arr[17]=networkParams.extendedPanId[7];


    Send_data_RF(temp_arr,18,0);
  }
  else if(send_gateway_data)       // send mac address of SLC to application.
  {
    temp_arr[0] = 'G';
    temp_arr[1] = 'E';
    temp_arr[2] = 'T';
    temp_arr[3] = amr_id/256;
    temp_arr[4] = amr_id%256;
    emberAfGetEui64(eui64);

    temp_arr[5]  = eui64[7];
    temp_arr[6]  =eui64[6];
    temp_arr[7]  =eui64[5];
    temp_arr[8]  =eui64[4];
    temp_arr[9]  =eui64[3];
    temp_arr[10] =eui64[2];
    temp_arr[11] =eui64[1];
    temp_arr[12] =eui64[0];
    Send_data_RF(temp_arr,13,0); 	
    send_gateway_data=0;
  }
  else if(Send_Data == 1)         // send data if read data command received.
  {
    //Send_Data_Save_Kwh =1; 
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = 'D';
    temp_arr[5] = 'P';//Frm_no; P for Push Logic
    temp_arr[6] = Date.Date;
    temp_arr[7] = Date.Month;
    temp_arr[8] = Date.Year;
    temp_arr[9] = Time.Hour;
    temp_arr[10] = Time.Min;
    temp_arr[11] = Time.Sec;


    temp_arr[12] = error_condition.Val;		// First char of digital data
    temp_arr[13] = error_condition1.Val;	// second char of digital data

    pulseCounter.float_data = vrdoub;       // voltage
    temp_arr[14] = pulseCounter.byte[3];
    temp_arr[15] = pulseCounter.byte[2];
    temp_arr[16] = pulseCounter.byte[1];
    temp_arr[17] = pulseCounter.byte[0];

    pulseCounter.float_data = irdoub;       // current
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = w1/1000.0;   // Kilo watt
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    pulseCounter.float_data = kwh;         // Kilo watt hour
    temp_arr[26] = pulseCounter.byte[3];
    temp_arr[27] = pulseCounter.byte[2];
    temp_arr[28] = pulseCounter.byte[1];
    temp_arr[29] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)lamp_burn_hour;   // Burn-hour
    temp_arr[30] = pulseCounter.byte[3];
    temp_arr[31] = pulseCounter.byte[2];
    temp_arr[32] = pulseCounter.byte[1];
    temp_arr[33] = pulseCounter.byte[0];

    pulseCounter.float_data = Dimvalue;      // dimming percentage
    temp_arr[34] = pulseCounter.byte[3];
    temp_arr[35] = pulseCounter.byte[2];
    temp_arr[36] = pulseCounter.byte[1];
    temp_arr[37] = pulseCounter.byte[0];

    pulseCounter.float_data = pf;           // power factor
//    pulseCounter.float_data = (float)RTC_drift;
    temp_arr[38] = pulseCounter.byte[3];
    temp_arr[39] = pulseCounter.byte[2];
    temp_arr[40] = pulseCounter.byte[1];
    temp_arr[41] = pulseCounter.byte[0];
#ifdef NEW_LAMP_FAULT_LOGIC
	   pulseCounter.float_data = KW_Threshold;
#elif defined(OLD_LAMP_FAULT_LOGIC)
    pulseCounter.float_data = lamp_current;   // Lamp steady current
#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif
    temp_arr[42] = pulseCounter.byte[3];
    temp_arr[43] = pulseCounter.byte[2];
    temp_arr[44] = pulseCounter.byte[1];
    temp_arr[45] = pulseCounter.byte[0];

    temp_arr[46] = no_of_pending_event;     // no of event in memory which is not send.
    if(SLC_New_Manula_Mode_En == 1)
    {
      temp_arr[47] = 0;               // SLC Mode
    }
    else
    {
      temp_arr[47] = automode;        // SLC Mode
    }

    temp_arr[48] = Motion_broadcast_miss_counter; // Motion Broadcast miss counter
    temp_arr[49] = Motion_group_id;       // Motion group ID.

    Slc_db=(Slc_db)*(-1);
    if(Slc_db<0)
      Slc_db =20;
    temp_arr[50] = Slc_db;                // last RF message receive signal strength.
    temp_arr[51] = temperature1;           // SLC temperature from Energy meter IC.
    temp_arr[52] = Slc_reset_counter/256;
    temp_arr[53] = Slc_reset_counter%256;  // SLC power reset counter
    temp_arr[54] = SLC_reset_info;         // Last reset cause.
    temp_arr[55] = SLC_extreset_info;
////////////////////AI////////////
#ifdef ISLC3300_T8_V2
        pulseCounter.float_data = PA4_Analog_Value;
        temp_arr[56] = pulseCounter.byte[3];
        temp_arr[57] = pulseCounter.byte[2];
        temp_arr[58] = pulseCounter.byte[1];
        temp_arr[59] = pulseCounter.byte[0];
        Send_data_RF(temp_arr,60,0);
#else
    Send_data_RF(temp_arr,56,0);
#endif
    Motion_broadcast_miss_counter = 0;
    Send_Data = 0;	
//   if(error_condition1.bits.b6 == 1)
//   {
//     Tilt_counter =0;
//
//   }
  }
  else if(Send_Data_WithLastHourEnergy == 1)         // send data if read data energy meter command received.
  {
    //Send_Data_Save_Kwh =1; 
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = DATA_WITH_LAST_HOUR_ENERGY;
    temp_arr[5] = SLC_reset_info;                //'P';          //Frm_no; P for Push Logic
    temp_arr[6] = Date.Date;
    temp_arr[7] = Date.Month;
    temp_arr[8] = Date.Year;
    temp_arr[9] = Time.Hour;
    temp_arr[10] = Time.Min;
    temp_arr[11] = Time.Sec;


    temp_arr[12] = error_condition.Val;		// First char of digital data
    temp_arr[13] = error_condition1.Val;	// second char of digital data

    pulseCounter.float_data = vrdoub;       // voltage
    temp_arr[14] = pulseCounter.byte[3];
    temp_arr[15] = pulseCounter.byte[2];
    temp_arr[16] = pulseCounter.byte[1];
    temp_arr[17] = pulseCounter.byte[0];

    pulseCounter.float_data = irdoub;       // current
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = w1/1000.0;   // Kilo watt
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    pulseCounter.float_data = kwh;         // Kilo watt hour
    temp_arr[26] = pulseCounter.byte[3];
    temp_arr[27] = pulseCounter.byte[2];
    temp_arr[28] = pulseCounter.byte[1];
    temp_arr[29] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)lamp_burn_hour;   // Burn-hour
    temp_arr[30] = pulseCounter.byte[3];
    temp_arr[31] = pulseCounter.byte[2];
    temp_arr[32] = pulseCounter.byte[1];
    temp_arr[33] = pulseCounter.byte[0];

    temp_arr[34] = Dimvalue;
    
//    pulseCounter.float_data = pf;           // power factor
//    temp_arr[35] = pulseCounter.byte[3];
//    temp_arr[36] = pulseCounter.byte[2];
//    temp_arr[37] = pulseCounter.byte[1];
//    temp_arr[38] = pulseCounter.byte[0];
    
    temp_arr[35] = (int)(pf*1000)/256;
    temp_arr[36] = (int)(pf*1000)%256;   
    
    temp_arr[37] = (int)(KW_Threshold)/256;
    temp_arr[38] = (int) (KW_Threshold)%256;
    temp_arr[39] = no_of_pending_event;     // no of event in memory which is not send.
    
    if(SLC_New_Manula_Mode_En == 1)
    {
      temp_arr[40] = 0;               // SLC Mode
    }
    else
    {
      temp_arr[40] = automode;        // SLC Mode
    }

    temp_arr[41] = Slc_reset_counter/256;
    temp_arr[42] = Slc_reset_counter%256;  // SLC power reset counter

    pulseCounter.float_data = previousHourKwhBuffer[0];         // Kilo watt hour 0-15 min interval
    temp_arr[43] = pulseCounter.byte[3];
    temp_arr[44] = pulseCounter.byte[2];
    temp_arr[45] = pulseCounter.byte[1];
    temp_arr[46] = pulseCounter.byte[0];

    pulseCounter.float_data = previousHourKwhBuffer[1];         // Kilo watt hour 15-30 min interval
    temp_arr[47] = pulseCounter.byte[3];
    temp_arr[48] = pulseCounter.byte[2];
    temp_arr[49] = pulseCounter.byte[1];
    temp_arr[50] = pulseCounter.byte[0];
    
    pulseCounter.float_data = previousHourKwhBuffer[2];         // Kilo watt hour 30-45 min interval
    temp_arr[51] = pulseCounter.byte[3];
    temp_arr[52] = pulseCounter.byte[2];
    temp_arr[53] = pulseCounter.byte[1];
    temp_arr[54] = pulseCounter.byte[0];
    
    pulseCounter.float_data = previousHourKwhBuffer[3];         // Kilo watt hour 45-60 min interval
    temp_arr[55] = pulseCounter.byte[3];
    temp_arr[56] = pulseCounter.byte[2];
    temp_arr[57] = pulseCounter.byte[1];
    temp_arr[58] = pulseCounter.byte[0];
    
    Send_data_RF(temp_arr,59,0);

    Send_Data_WithLastHourEnergy = 0;	

  }    
  else if(send_data_direct == 1)    // send data if auto event configured.
  {
    //Send_Data_Save_Kwh =1;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = 'T';
    temp_arr[5] = Frm_no;
    temp_arr[6] = Date.Date;
    temp_arr[7] = Date.Month;
    temp_arr[8] = Date.Year;
    temp_arr[9] = Time.Hour;
    temp_arr[10] = Time.Min;
    temp_arr[11] = Time.Sec;


    temp_arr[12] = error_condition.Val;		
    temp_arr[13] = error_condition1.Val;	

    pulseCounter.float_data = vrdoub;
    temp_arr[14] = pulseCounter.byte[3];
    temp_arr[15] = pulseCounter.byte[2];
    temp_arr[16] = pulseCounter.byte[1];
    temp_arr[17] = pulseCounter.byte[0];

    pulseCounter.float_data = irdoub;
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = w1/1000.0;
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    pulseCounter.float_data = kwh;
    temp_arr[26] = pulseCounter.byte[3];
    temp_arr[27] = pulseCounter.byte[2];
    temp_arr[28] = pulseCounter.byte[1];
    temp_arr[29] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)lamp_burn_hour;
    temp_arr[30] = pulseCounter.byte[3];
    temp_arr[31] = pulseCounter.byte[2];
    temp_arr[32] = pulseCounter.byte[1];
    temp_arr[33] = pulseCounter.byte[0];

    pulseCounter.float_data = Dimvalue;
    temp_arr[34] = pulseCounter.byte[3];
    temp_arr[35] = pulseCounter.byte[2];
    temp_arr[36] = pulseCounter.byte[1];
    temp_arr[37] = pulseCounter.byte[0];

    pulseCounter.float_data = pf;			
    temp_arr[38] = pulseCounter.byte[3];
    temp_arr[39] = pulseCounter.byte[2];
    temp_arr[40] = pulseCounter.byte[1];
    temp_arr[41] = pulseCounter.byte[0];

#ifdef NEW_LAMP_FAULT_LOGIC
	   pulseCounter.float_data = KW_Threshold;
#elif defined(OLD_LAMP_FAULT_LOGIC)
    pulseCounter.float_data = lamp_current;   // Lamp steady current
#else
 #error "Please define LAMP_FAULT_LOGIC_TYPE"
#endif

    temp_arr[42] = pulseCounter.byte[3];
    temp_arr[43] = pulseCounter.byte[2];
    temp_arr[44] = pulseCounter.byte[1];
    temp_arr[45] = pulseCounter.byte[0];

    temp_arr[46] = no_of_pending_event;

    if(SLC_New_Manula_Mode_En == 1)
    {
      temp_arr[47] = 0;
    }
    else
    {
      temp_arr[47] = automode;
    }

    temp_arr[48] = Motion_broadcast_miss_counter;
    temp_arr[49] = Motion_group_id;

    Slc_db=(Slc_db)*(-1);
    if(Slc_db<0)
      Slc_db =20;
    temp_arr[50] = Slc_db;

    temp_arr[51] = temperature1;
    temp_arr[52] = Slc_reset_counter/256;
    temp_arr[53] = Slc_reset_counter%256;
    temp_arr[54] = SLC_reset_info;
    temp_arr[55] = SLC_extreset_info;
    Send_data_RF(temp_arr,56,1);
    Motion_broadcast_miss_counter = 0;
    send_data_direct = 0;	
  }
  else if(send_id_frame == 1)       // send Id frame at every 9 sec.
  {

    //emberAfGuaranteedPrint("\r\n%p", "ID frame send");
    temp_arr[0] = 'I';
    temp_arr[1] = 'D';
    temp_arr[2] = amr_id/256;
    temp_arr[3] = amr_id%256;
    temp_arr[4] = SLC_joinig_retry_counter;
    send_id_frame = 0;
//    Send_data_RF(temp_arr,5,1);			
    temp_arr[5] = 'L';
    pulseCounter.float_data = Latitude;
    temp_arr[6] = pulseCounter.byte[3];
    temp_arr[7] = pulseCounter.byte[2];
    temp_arr[8] = pulseCounter.byte[1];
    temp_arr[9] = pulseCounter.byte[0];

    pulseCounter.float_data = longitude;
    temp_arr[10] = pulseCounter.byte[3];
    temp_arr[11] = pulseCounter.byte[2];
    temp_arr[12] = pulseCounter.byte[1];
    temp_arr[13] = pulseCounter.byte[0];
    temp_arr[14] = '$';
    
    pulseCounter.dword = amr_id;
    temp_arr[15] = pulseCounter.byte[3];
    temp_arr[16] = pulseCounter.byte[2];
    temp_arr[17] = pulseCounter.byte[1];
    temp_arr[18] = pulseCounter.byte[0];
    temp_arr[19] = Blue_Light_Id/256;
    temp_arr[20] = Blue_Light_Id%256;
    Send_data_RF(temp_arr,21,1);

  }
  else if(send_all_rf_event == 1)  // start send event data from memory.
  {
    send_all_rf_event = 0;
    send_all_event();
  }	
  else if(send_get_mode_replay == 1) // Send get mode responce to DCU.
  {
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = AMSTATUS;
    if((SLC_Test_Query_2_received == 1)||(Commissioning_flag == 1))
    {
        halCommonGetToken(&automode,TOKEN_automode);
        temp_arr[5] = automode;
        automode =0;
    }
    else
    {
     temp_arr[5] = automode;
    }

    pulseCounter.float_data = Latitude;
    temp_arr[6] = pulseCounter.byte[3];
    temp_arr[7] = pulseCounter.byte[2];
    temp_arr[8] = pulseCounter.byte[1];
    temp_arr[9] = pulseCounter.byte[0];

    pulseCounter.float_data = longitude;
    temp_arr[10] = pulseCounter.byte[3];
    temp_arr[11] = pulseCounter.byte[2];
    temp_arr[12] = pulseCounter.byte[1];
    temp_arr[13] = pulseCounter.byte[0];

    pulseCounter.float_data = utcOffset;
    temp_arr[14] = pulseCounter.byte[3];
    temp_arr[15] = pulseCounter.byte[2];
    temp_arr[16] = pulseCounter.byte[1];
    temp_arr[17] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)Sunset_delay;
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)Sunrise_delay;
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    temp_arr[26] = GPS_Data_throw_status;

    Send_data_RF(temp_arr,27,0);								
    emberAfCorePrintln("\r\nGet Mode responce send");
    send_get_mode_replay = 0;	
  }
  else if(send_get_mode_replay_auto == 1)    // if mode change from reader then send auto responce to DCU.
  {
    send_get_mode_replay_auto=0;

    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = AMSTATUS;

    temp_arr[5] = automode;

    pulseCounter.float_data = Latitude;
    temp_arr[6] = pulseCounter.byte[3];
    temp_arr[7] = pulseCounter.byte[2];
    temp_arr[8] = pulseCounter.byte[1];
    temp_arr[9] = pulseCounter.byte[0];

    pulseCounter.float_data = longitude;
    temp_arr[10] = pulseCounter.byte[3];
    temp_arr[11] = pulseCounter.byte[2];
    temp_arr[12] = pulseCounter.byte[1];
    temp_arr[13] = pulseCounter.byte[0];

    pulseCounter.float_data = utcOffset;
    temp_arr[14] = pulseCounter.byte[3];
    temp_arr[15] = pulseCounter.byte[2];
    temp_arr[16] = pulseCounter.byte[1];
    temp_arr[17] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)Sunset_delay;
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)Sunrise_delay;
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    Send_data_RF(temp_arr,26,1);											
  }	
  else if(Send_Fault_Para_Replay == 1)  // Send SLC Lamp fault para to DCU.
  {
    Send_Fault_Para_Replay = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = SEND_FAULT_PARA;

    pulseCounter.int_data = Vol_hi;
    temp_arr[5] = pulseCounter.byte[1];
    temp_arr[6] = pulseCounter.byte[0];

    pulseCounter.int_data = Vol_low;
    temp_arr[7] = pulseCounter.byte[1];
    temp_arr[8] = pulseCounter.byte[0];

    pulseCounter.int_data = Curr_Steady_Time;
    temp_arr[9] = pulseCounter.byte[1];
    temp_arr[10] = pulseCounter.byte[0];

    pulseCounter.int_data = Per_Val_Current;
    temp_arr[11] = pulseCounter.byte[1];
    temp_arr[12] = pulseCounter.byte[0];

    pulseCounter.int_data = Lamp_Fault_Time;
    temp_arr[13] = pulseCounter.byte[1];
    temp_arr[14] = pulseCounter.byte[0];

    pulseCounter.int_data = Lamp_faulty_retrieve_Count;
    temp_arr[15] = pulseCounter.byte[1];
    temp_arr[16] = pulseCounter.byte[0];

    pulseCounter.int_data = Lamp_off_on_Time;
    temp_arr[17] = pulseCounter.byte[1];
    temp_arr[18] = pulseCounter.byte[0];

    pulseCounter.int_data = Lamp_lock_condition;
    temp_arr[19] = pulseCounter.byte[1];
    temp_arr[20] = pulseCounter.byte[0];
    Send_data_RF(temp_arr,21,1);									
    emberAfCorePrintln("\r\nFault para responce send");
  }
  else if(Send_Slc_Diagnosis == 1)   // Send SLC diagnosis result to Dcu.
  {
    Send_Slc_Diagnosis = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = SEND_DIAGNOSIS;

    temp_arr[5] = Date.Date;
    temp_arr[6] = Date.Month;
    temp_arr[7] = Date.Year;
    temp_arr[8] = Time.Hour;
    temp_arr[9] = Time.Min;
    temp_arr[10] = Time.Sec;

    temp_arr[11] = Diagnosis_DI;

    pulseCounter.float_data = vrdoub;
    temp_arr[12] = pulseCounter.byte[3];
    temp_arr[13] = pulseCounter.byte[2];
    temp_arr[14] = pulseCounter.byte[1];
    temp_arr[15] = pulseCounter.byte[0];

    pulseCounter.float_data = irdoub;
    temp_arr[16] = pulseCounter.byte[3];
    temp_arr[17] = pulseCounter.byte[2];
    temp_arr[18] = pulseCounter.byte[1];
    temp_arr[19] = pulseCounter.byte[0];	

    pulseCounter.int_data = emt_int_count;
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.int_data = Kwh_count;
    temp_arr[22] = pulseCounter.byte[1];
    temp_arr[23] = pulseCounter.byte[0];

    pulseCounter.float_data = 0;
    temp_arr[24] = pulseCounter.byte[3];
    temp_arr[25] = pulseCounter.byte[2];
    temp_arr[26] = pulseCounter.byte[1];
    temp_arr[27] = pulseCounter.byte[0];

    Send_data_RF(temp_arr,28,1);									
    Master_event_generation = 0;

  }
  else if(Send_Network_Para_Replay == 1)  // Send network parametes to Dcu.
  {
    Send_Network_Para_Replay = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_NAK_PARA;

    pulseCounter.int_data = Broadcast_time_delay;
    temp_arr[5] = pulseCounter.byte[1];
    temp_arr[6] = pulseCounter.byte[0];

    pulseCounter.int_data = Unicast_time_delay;
    temp_arr[7] = pulseCounter.byte[1];
    temp_arr[8] = pulseCounter.byte[0];

    pulseCounter.int_data = Lamp_lock_condition;
    temp_arr[9] = pulseCounter.byte[1];
    temp_arr[10] = pulseCounter.byte[0];

    Send_data_RF(temp_arr,11,1);									
  }
  else if((Send_Get_Sch_Replay == 1) && (Get_Sch_timer >= 5))		 // Send schedule responce to DCU at every 5 second if week day schedule configure for different days.	
  {
    Get_Sch_timer = 0;	
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_SCH_PARA;


    k = 0;
    for(i=0;i<10;i++)
    {
      temp_arr[(i*5) + 5] = sSch[k][i].cStartHour;	
      temp_arr[(i*5) + 6] = sSch[k][i].cStartMin;		
      temp_arr[(i*5) + 7] = sSch[k][i].cStopHour;		
      temp_arr[(i*5) + 8] = sSch[k][i].cStopMin;		
      temp_arr[(i*5) + 9] = sSch[k][i].bSchFlag;						
    }


    temp_arr[55] = 0x7F;
    Send_data_RF(temp_arr,56,0);
    Send_Get_Sch_Replay = 0;
    emberAfCorePrintln("\r\nSchedule responce send");
  }
  else if((send_SLC_test_query_1 == 1) && (test_query_1_send_timer >= 6)) // Send test query 1 result to test applciaton.
  {
    test_query_1_send_timer = 0;

    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_SLC_TEST_1;

    temp_arr[5] = Date.Date;
    temp_arr[6] = Date.Month;
    temp_arr[7] = Date.Year;
    temp_arr[8] = Time.Hour;
    temp_arr[9] = Time.Min;
    temp_arr[10] = Time.Sec;

    temp_arr[11] = test_result.Val;

    pulseCounter.float_data = test_voltage;
    temp_arr[12] = pulseCounter.byte[3];
    temp_arr[13] = pulseCounter.byte[2];
    temp_arr[14] = pulseCounter.byte[1];
    temp_arr[15] = pulseCounter.byte[0];

    pulseCounter.float_data = test_current;
    temp_arr[16] = pulseCounter.byte[3];
    temp_arr[17] = pulseCounter.byte[2];
    temp_arr[18] = pulseCounter.byte[1];
    temp_arr[19] = pulseCounter.byte[0];	

    pulseCounter.int_data = test_emt_int_count;
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.int_data = test_Kwh_count;
    temp_arr[22] = pulseCounter.byte[1];
    temp_arr[23] = pulseCounter.byte[0];
    if((DI_GET_LOW == 1)&&(DI_GET_HIGH==1))
    {
      temp_arr[24] = DI_GET_LOW;

    }
    else
    {
      temp_arr[24] =0;
    }

    pulseCounter.float_data = ADC_Result_0;
    temp_arr[25] = pulseCounter.byte[3];
    temp_arr[26] = pulseCounter.byte[2];
    temp_arr[27] = pulseCounter.byte[1];
    temp_arr[28] = pulseCounter.byte[0];

    pulseCounter.float_data = ADC_Result_50;
    temp_arr[29] = pulseCounter.byte[3];
    temp_arr[30] = pulseCounter.byte[2];
    temp_arr[31] = pulseCounter.byte[1];
    temp_arr[32] = pulseCounter.byte[0];

    pulseCounter.float_data = ADC_Result_100;
    temp_arr[33] = pulseCounter.byte[3];
    temp_arr[34] = pulseCounter.byte[2];
    temp_arr[35] = pulseCounter.byte[1];
    temp_arr[36] = pulseCounter.byte[0];
    
    temp_arr[37] = dali_support;
    ///////////////////
    pulseCounter.float_data = Dali_At_0Dim;
    temp_arr[38] = pulseCounter.byte[3];
    temp_arr[39] = pulseCounter.byte[2];
    temp_arr[40] = pulseCounter.byte[1];
    temp_arr[41] = pulseCounter.byte[0];
    pulseCounter.float_data = Dali_At_25Dim;
    temp_arr[42] = pulseCounter.byte[3];
    temp_arr[43] = pulseCounter.byte[2];
    temp_arr[44] = pulseCounter.byte[1];
    temp_arr[45] = pulseCounter.byte[0];
    pulseCounter.float_data = Dali_At_50Dim;
    temp_arr[46] = pulseCounter.byte[3];
    temp_arr[47] = pulseCounter.byte[2];
    temp_arr[48] = pulseCounter.byte[1];
    temp_arr[49] = pulseCounter.byte[0];
    pulseCounter.float_data = Dali_At_75Dim;
    temp_arr[50] = pulseCounter.byte[3];
    temp_arr[51] = pulseCounter.byte[2];
    temp_arr[52] = pulseCounter.byte[1];
    temp_arr[53] = pulseCounter.byte[0];
    pulseCounter.float_data = Dali_At_100Dim;
    temp_arr[54] = pulseCounter.byte[3];
    temp_arr[55] = pulseCounter.byte[2];
    temp_arr[56] = pulseCounter.byte[1];
    temp_arr[57] = pulseCounter.byte[0];
    Send_data_RF(temp_arr,58,1);									
    ///////////////////

    //Send_data_RF(temp_arr,38,1);									
  }
  else if((send_SLC_test_query_2 == 1) && (test_query_1_send_timer >= 6))  // Send test query 2 result to application.
  {
    test_query_1_send_timer = 0;

    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_SLC_TEST_2;

    temp_arr[5] = Date.Date;
    temp_arr[6] = Date.Month;
    temp_arr[7] = Date.Year;
    temp_arr[8] = Time.Hour;
    temp_arr[9] = Time.Min;
    temp_arr[10] = Time.Sec;

    temp_arr[11] = Rtc_Power_on_result;
    Send_data_RF(temp_arr,12,1);		
  }
  else if((Send_Get_Master_Sch_Replay == 1) && (Get_Master_Sch_timer >= 5))	// send dimming schedule responce to DCU.
  {
    Get_Master_Sch_timer = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_DIMM_SCH_PARA;

    k = 0;
    for(i=0;i<10;i++)
    {
      temp_arr[(i*6) + 5] = Master_sSch[k][i].cStartHour;	
      temp_arr[(i*6) + 6] = Master_sSch[k][i].cStartMin;	
      temp_arr[(i*6) + 7] = Master_sSch[k][i].cStopHour;	
      temp_arr[(i*6) + 8] = Master_sSch[k][i].cStopMin;	
      temp_arr[(i*6) + 9] = Master_sSch[k][i].bSchFlag;	
      temp_arr[(i*6) + 10] = Master_sSch[k][i].dimvalue;	
    }			
    temp_arr[65] = 0x7F;
    Send_data_RF(temp_arr,66,0);
    Send_Get_Master_Sch_Replay = 0;

    emberAfCorePrintln("\r\nDimming schedule responce send");
  }
  else if(Send_Get_Version_info_Replay == 1)		 // Send version info to DCU,reader. 			
  {
    Send_Get_Version_info_Replay = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_VERSION_PARA;

//    temp_arr[5] = 'E';
//    temp_arr[6] = 'M';
//    temp_arr[7] = 'B';
//    temp_arr[8] = 'E';
//    temp_arr[9] = 'R';
//    temp_arr[10] = '_';
//    temp_arr[11] = 'S';
//    temp_arr[12] = 'L';
//    temp_arr[13] = 'C';
//    temp_arr[14] = '_';
//    temp_arr[15] = 'V';
//    temp_arr[16] = '2';												
//    temp_arr[17] = '.';													
//    temp_arr[18] = '0';
//    temp_arr[19] = '.';
//    temp_arr[20] = '1';
//    temp_arr[21] = '5';
//    temp_arr[22] = '\0';
//
//    Send_data_RF(temp_arr,23,0);
#ifdef ISLC_3100_V7
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';												
      temp_arr[17] = '7';													
      temp_arr[18] = '_';
      temp_arr[19] = '2';
      temp_arr[20] = '.';
      temp_arr[21] = '0';
      temp_arr[22] = '.';
      temp_arr[23] = '2';
      temp_arr[24] = '2';
      temp_arr[25] = '\0';
      Send_data_RF(temp_arr,26,0);

#elif defined(ISLC_3100_V5_V6)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';												
      temp_arr[17] = '6';													
      temp_arr[18] = '_';
      temp_arr[19] = '2';
      temp_arr[20] = '.';
      temp_arr[21] = '0';
      temp_arr[22] = '.';
      temp_arr[23] = '2';
      temp_arr[24] = '2';
      temp_arr[25] = '\0';
      Send_data_RF(temp_arr,26,0);
#elif defined(ISLC_10)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '1';
      temp_arr[12] = '0';
      temp_arr[13] = '_';
      temp_arr[14] = '2';
      temp_arr[15] = '.';
      temp_arr[16] = '0';
      temp_arr[17] = '.';
      temp_arr[18] = '2';
      temp_arr[19] = '2';
      temp_arr[20] = '\0';
      Send_data_RF(temp_arr,21,0);
#elif defined(ISLC_T8)
    #ifdef ISLC_T8_V4
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = 'T';
      temp_arr[12] = '8';
      temp_arr[13] = '_';
      temp_arr[14] = '4';
      temp_arr[15] = 'V';
      temp_arr[16] = '_';
      temp_arr[17] = '2';
      temp_arr[18] = '.';
      temp_arr[19] = '0';
      temp_arr[20] = '.';
      temp_arr[21] = '2';
      temp_arr[22] = '2';
      temp_arr[23] = '\0';
      Send_data_RF(temp_arr,24,0);
    #else
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = 'T';
      temp_arr[12] = '8';
      temp_arr[13] = '_';
      temp_arr[14] = '2';
      temp_arr[15] = '.';
      temp_arr[16] = '0';
      temp_arr[17] = '.';
      temp_arr[18] = '2';
      temp_arr[19] = '2';
      temp_arr[20] = '\0';
      Send_data_RF(temp_arr,21,0);
    #endif
//////////
#elif defined(ISLC3300_T8_V2)||defined(ISLC3300_T8_V4)

      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '3';
      temp_arr[11] = '3';
      temp_arr[12] = '0';
      temp_arr[13] = '0';
      temp_arr[14] = '_';
      temp_arr[15] = 'T';
      temp_arr[16] = '8';
      temp_arr[17] = '_';
      temp_arr[18] = 'V';
#ifdef  BATTERY_RTC
      temp_arr[19] = '4';
#else
      temp_arr[19] = '2';
#endif
      temp_arr[20] = '_';
      temp_arr[21] = '2';
      temp_arr[22] = '.';
      temp_arr[23] = '0';
      temp_arr[24] = '.';
      temp_arr[25] = '2';
      temp_arr[26] = '7';
      temp_arr[27] = '\0';
      Send_data_RF(temp_arr,28,0);

      ///////////
#elif defined(ISLC_3100_7P_V1)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = '7';
      temp_arr[17] = 'P';
      temp_arr[18] = '_';
      temp_arr[19] = 'V';
      temp_arr[20] = '1';
      temp_arr[21] = '_';
      temp_arr[22] = '2';
      temp_arr[23] = '.';
      temp_arr[24] = '0';
      temp_arr[25] = '.';
      temp_arr[26] = '2';
      temp_arr[27] = '2';
      temp_arr[28] = '\0';
      Send_data_RF(temp_arr,29,0);
      ///////////////

#elif defined(ISLC_3100_V7_2)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';
      temp_arr[17] = '7';
      temp_arr[18] = '_';
      temp_arr[19] = 'V';
      temp_arr[20] = '2';
      temp_arr[21] = '_';
      temp_arr[22] = '2';
      temp_arr[23] = '.';
      temp_arr[24] = '0';
      temp_arr[25] = '.';
      temp_arr[26] = '2';
      temp_arr[27] = '2';
      temp_arr[28] = '\0';
      Send_data_RF(temp_arr,29,0);
      //////////////////
#elif defined(ISLC_3100_V7_3_0216)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';
      temp_arr[17] = '7';
      temp_arr[18] = '_';
      temp_arr[19] = 'V';
      temp_arr[20] = '3';

       temp_arr[21] = '_';
        temp_arr[22] = '0';
         temp_arr[23] = '2';
          temp_arr[24] = '1';
           temp_arr[25] = '6';
           temp_arr[26] = '_';
      temp_arr[27] = 'V';
      temp_arr[28] = '2';
      temp_arr[29] = '.';
      temp_arr[30] = '0';
      temp_arr[31] = '.';
      temp_arr[32] = '3';
      temp_arr[33] = '5';
      temp_arr[34] = '\0';
      Send_data_RF(temp_arr,35,0);
      //////////////////
#elif defined(ISLC_3100_V7_7)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';
      temp_arr[17] = '7';
      temp_arr[18] = '_';
      temp_arr[19] = 'V';
      temp_arr[20] = '7';

//       temp_arr[21] = '_';
//        temp_arr[22] = '0';
//         temp_arr[23] = '2';
//          temp_arr[24] = '1';
//           temp_arr[25] = '6';
           temp_arr[21] = '_';
      temp_arr[22] = 'V';
      temp_arr[23] = '2';
      temp_arr[24] = '.';
      temp_arr[25] = '2';
      temp_arr[26] = '.';
      temp_arr[27] = '0';
      temp_arr[28] = '2';
      temp_arr[29] = '\0';
      Send_data_RF(temp_arr,30,0);
      //////////////////

#elif defined(ISLC_3100_V7_9)

      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';
      temp_arr[17] = '7';
      temp_arr[18] = '_';
      temp_arr[19] = 'V';
      temp_arr[20] = '9';

//       temp_arr[21] = '_';
//        temp_arr[22] = '0';
//         temp_arr[23] = '2';
//          temp_arr[24] = '1';
//           temp_arr[25] = '6';
           temp_arr[21] = '_';
      temp_arr[22] = 'V';
      temp_arr[23] = '2';
      temp_arr[24] = '.';
      temp_arr[25] = '2';
      temp_arr[26] = '.';
      temp_arr[27] = '0';
      temp_arr[28] = '2';
      temp_arr[29] = '.';
      temp_arr[30] = 'A';
      temp_arr[31] = '\0';
      Send_data_RF(temp_arr,32,0);
      //////////////////      
      
#elif defined(ISLC_3100_480V_Rev3)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = '4';
      temp_arr[17] = '8';
      temp_arr[18] = '0';
      temp_arr[19] = 'R';
      temp_arr[20] = '.';

      temp_arr[21] = '3';

      temp_arr[22] = '_';
      temp_arr[23] = 'V';
      temp_arr[24] = '2';
      temp_arr[25] = '.';
      temp_arr[26] = '0';
      temp_arr[27] = '.';
      temp_arr[28] = '2';
      temp_arr[29] = '7';
      temp_arr[30] = '\0';
      Send_data_RF(temp_arr,31,0);
      //////////////////

#elif defined(ISLC_3100_V9)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';
      temp_arr[17] = '9';
      temp_arr[18] = '_';
      temp_arr[19] = '2';
      temp_arr[20] = '.';
      temp_arr[21] = '0';
      temp_arr[22] = '.';
      temp_arr[23] = '2';
      temp_arr[24] = '2';
      temp_arr[25] = '\0';
      Send_data_RF(temp_arr,26,0);
#elif defined(ISLC_3100_V9_2)
      temp_arr[5] = 'E';
      temp_arr[6] = 'I';
      temp_arr[7] = 'S';
      temp_arr[8] = 'L';
      temp_arr[9] = 'C';
      temp_arr[10] = '_';
      temp_arr[11] = '3';
      temp_arr[12] = '1';
      temp_arr[13] = '0';
      temp_arr[14] = '0';
      temp_arr[15] = '_';
      temp_arr[16] = 'V';
      temp_arr[17] = '9';
      temp_arr[18] = '.';
      temp_arr[19] = '2';
      temp_arr[20] = '_';
      temp_arr[21] = '2';
      temp_arr[22] = '.';
      temp_arr[23] = '0';
      temp_arr[24] = '.';
      temp_arr[25] = '3';
      temp_arr[26] = '0';
      temp_arr[27] = '\0';
      Send_data_RF(temp_arr,28,0);
#else
   #error "please select SLC PCB version"
#endif


  }
  else if(Send_Get_Adaptive_dimming_para_Replay == 1)  // send adaptive dimming para result to DCU.
  {
    Send_Get_Adaptive_dimming_para_Replay = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_ADAPTIVE_DIMMING;

    pulseCounter.int_data = analog_input_scaling_high_Value;
    temp_arr[5] = pulseCounter.byte[1];
    temp_arr[6] = pulseCounter.byte[0];

    pulseCounter.int_data = analog_input_scaling_low_Value;
    temp_arr[7] = pulseCounter.byte[1];
    temp_arr[8] = pulseCounter.byte[0];

    pulseCounter.int_data = desir_lamp_lumen;
    temp_arr[9] = pulseCounter.byte[1];
    temp_arr[10] = pulseCounter.byte[0];

    temp_arr[11] = lumen_tollarence;			
    temp_arr[12] = ballast_type; 			
    temp_arr[13] = adaptive_light_dimming;	

    temp_arr[14] = Motion_pulse_rate;
    temp_arr[15] = Motion_dimming_percentage;
    temp_arr[16] = Motion_dimming_time/256;
    temp_arr[17] = Motion_dimming_time%256;
    temp_arr[18] = Motion_group_id;
    temp_arr[19] = dim_applay_time / 60;
    temp_arr[20] = dim_inc_val;
    temp_arr[21] = Motion_normal_dimming_percentage;

    pulseCounter.int_data = (int) (Day_light_harvesting_start_offset / 60);
    temp_arr[22] = pulseCounter.byte[1];
    temp_arr[23] = pulseCounter.byte[0];

    pulseCounter.int_data = (int) (Day_light_harvesting_stop_offset / 60);
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];		

    temp_arr[26] = Motion_Detect_Timeout;
    temp_arr[27] = Motion_Broadcast_Timeout;
    temp_arr[28] = Motion_Sensor_Type;
    Send_data_RF(temp_arr,29,0);
    }
  else if(Send_Get_Adaptive_dimming_para_Replay_32 == 1)  // send adaptive dimming para result to DCU.
  {
    Send_Get_Adaptive_dimming_para_Replay_32 = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_ADAPTIVE_DIMMING_32;

    pulseCounter.int_data = analog_input_scaling_high_Value;
    temp_arr[5] = pulseCounter.byte[1];
    temp_arr[6] = pulseCounter.byte[0];

    pulseCounter.int_data = analog_input_scaling_low_Value;
    temp_arr[7] = pulseCounter.byte[1];
    temp_arr[8] = pulseCounter.byte[0];

    pulseCounter.int_data = desir_lamp_lumen;
    temp_arr[9] = pulseCounter.byte[1];
    temp_arr[10] = pulseCounter.byte[0];

    temp_arr[11] = lumen_tollarence;			
    temp_arr[12] = ballast_type; 			
    temp_arr[13] = adaptive_light_dimming;	

    temp_arr[14] = Motion_pulse_rate;
    temp_arr[15] = Motion_dimming_percentage;
    temp_arr[16] = Motion_dimming_time/256;
    temp_arr[17] = Motion_dimming_time%256;
    temp_arr[18] = Motion_group_id;
    temp_arr[19] = dim_applay_time / 60;
    temp_arr[20] = dim_inc_val;
    temp_arr[21] = Motion_normal_dimming_percentage;

    pulseCounter.int_data = (int) (Day_light_harvesting_start_offset / 60);
    temp_arr[22] = pulseCounter.byte[1];
    temp_arr[23] = pulseCounter.byte[0];

    pulseCounter.int_data = (int) (Day_light_harvesting_stop_offset / 60);
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];		

    temp_arr[26] = Motion_Detect_Timeout;
    temp_arr[27] = Motion_Broadcast_Timeout;
    temp_arr[28] = Motion_Sensor_Type;

    pulseCounter.dword = Motion_group_id_32;
        temp_arr[29] = pulseCounter.byte[3];
        temp_arr[30] = pulseCounter.byte[2];
        temp_arr[31] = pulseCounter.byte[1];
        temp_arr[32] = pulseCounter.byte[0];

        Send_data_RF(temp_arr,33,0);


    }
    else if(motion_dimming_send == 1)     // if motion detect then send broadcast of motion to all grouped SLC.
    {
        if( Motion_grp_Select_32 == 1)
        {
            motion_dimming_send = 0;			
            temp_arr[0]='S';
            temp_arr[1]='L';
            temp_arr[2]=0x00;
            temp_arr[3]=0x00;
            temp_arr[4]=CMD;
            temp_arr[5] = MOTION_DETECT_32;
          //  temp_arr[6] = Motion_group_id;

            pulseCounter.dword = Motion_group_id_32;
            temp_arr[6] = pulseCounter.byte[3];
            temp_arr[7] = pulseCounter.byte[2];
            temp_arr[8] = pulseCounter.byte[1];
            temp_arr[9] = pulseCounter.byte[0];


            Motion_broadcast_Receive_counter++;
            pulseCounter.long_data = Motion_broadcast_Receive_counter;
            temp_arr[10] = pulseCounter.byte[3];
            temp_arr[11] = pulseCounter.byte[2];
            temp_arr[12] = pulseCounter.byte[1];
            temp_arr[13] = pulseCounter.byte[0];
            if(Motion_group_id_32 != 0)
            {
                Send_data_RF(temp_arr,14,2);
            }
        }
        else
        {
             motion_dimming_send = 0;			
            temp_arr[0]='S';
            temp_arr[1]='L';
            temp_arr[2]=0x00;
            temp_arr[3]=0x00;
            temp_arr[4]=CMD;
            temp_arr[5] = MOTION_DETECT;
            temp_arr[6] = Motion_group_id;

            Motion_broadcast_Receive_counter++;
            pulseCounter.long_data = Motion_broadcast_Receive_counter;
            temp_arr[7] = pulseCounter.byte[3];
            temp_arr[8] = pulseCounter.byte[2];
            temp_arr[9] = pulseCounter.byte[1];
            temp_arr[10] = pulseCounter.byte[0];
            if(Motion_group_id != 0)
            {
                Send_data_RF(temp_arr,11,2);
            }
        }
    }
//    else if(send_idimmer_id_frame == 1)  // Send idimmer id frame to idimmer.
//    {
//        send_idimmer_id_frame = 0;
//        temp_arr[0] = 'D';
//        temp_arr[1] = 'M';
//        temp_arr[2] = 'I';
//        temp_arr[3] = 'D';
//        temp_arr[4] = idimmer_id / 256;
//        temp_arr[5] = idimmer_id % 256;
//
//        Send_data_RF_idimmer(temp_arr,6);	
//    }		
//    else if(send_idimmer_Val_frame == 1)  // send adaptive dimming mode to Idimmer.
//    {
//        send_idimmer_Val_frame = 0;
//        temp_arr[0] = 'D';
//        temp_arr[1] = 'M';
//        temp_arr[2] = 'V';
//        temp_arr[3] = idimmer_dimval;
//        temp_arr[4] = adaptive_light_dimming;
//        temp_arr[5] = dim_inc_val;
//        temp_arr[6] = idimmer_trip_erase;
//
//        Send_data_RF_idimmer(temp_arr,7);
//        dimming_cmd_ack = 0;
//    }
//		else if(read_dim_val_idimmer == 1)   // read dimming mode from Idimmer.
//		{
//    read_dim_val_idimmer = 0;
//    temp_arr[0] = 'D';
//    temp_arr[1] = 'M';
//    temp_arr[2] = 'R';
//    Send_data_RF_idimmer(temp_arr,3);	
//		}
//		else if(Send_dimming_short_ack == 1)  // send Ack of dimming short trip to Idimmer.
//		{
//    Send_dimming_short_ack = 0;
//    temp_arr[0] = 'D';
//    temp_arr[1] = 'M';
//    temp_arr[2] = 'T';
//    temp_arr[3] = 'R';
//    Send_data_RF_idimmer(temp_arr,4);
//    }
    else if(uChar_GET_FOOT_CANDLE == 1)
    {
      uChar_GET_FOOT_CANDLE =0;
        temp_arr[0]='S';
        temp_arr[1]='L';
        temp_arr[2]= amr_id/256;
        temp_arr[3]= amr_id%256;
        temp_arr[4] = GET_FOOT_CANDLE;

    pulseCounter.float_data = Photosensor_Set_FC;				
    temp_arr[5] = pulseCounter.byte[3];
    temp_arr[6] = pulseCounter.byte[2];
    temp_arr[7] = pulseCounter.byte[1];
    temp_arr[8] = pulseCounter.byte[0];

    pulseCounter.float_data = Photosensor_Set_FC_Lamp_OFF;				
    temp_arr[9] = pulseCounter.byte[3];
    temp_arr[10] = pulseCounter.byte[2];
    temp_arr[11] = pulseCounter.byte[1];
    temp_arr[12] = pulseCounter.byte[0];

    pulseCounter.float_data = Photosensor_FootCandle;				
    temp_arr[13] = pulseCounter.byte[3];
    temp_arr[14] = pulseCounter.byte[2];
    temp_arr[15] = pulseCounter.byte[1];
    temp_arr[16] = pulseCounter.byte[0];

        Send_data_RF(temp_arr,17,1);				
    }
    else if(Send_Get_Slc_Config_para_Replay == 1)  // send Slc config para responce to Dcu.
    {
    Send_Get_Slc_Config_para_Replay = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_SLC_CONFIG_PARA;

    temp_arr[5] = Lamp_type;
    temp_arr[6] = Auto_event;
    temp_arr[7] = Schedule_offset;
    temp_arr[8] = RTC_New_fault_logic_Enable;
    Send_data_RF(temp_arr,9,1);				
		}
		else if(send_get_slc_dst_config_para == 1)  // Send Dst configuration to DCU.
		{
    send_get_slc_dst_config_para = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_SLC_DST_CONFIG_PARA;

    temp_arr[5] = SLC_DST_En;
    temp_arr[6] = SLC_DST_Start_Month;
    temp_arr[7] = SLC_DST_Start_Rule;
    temp_arr[8] = SLC_DST_Start_Time;
    temp_arr[9] = SLC_DST_Stop_Month;
    temp_arr[10] = SLC_DST_Stop_Rule;
    temp_arr[11] = SLC_DST_Stop_Time;

    pulseCounter.float_data = SLC_DST_Time_Zone_Diff;				
    temp_arr[12] = pulseCounter.byte[3];
    temp_arr[13] = pulseCounter.byte[2];
    temp_arr[14] = pulseCounter.byte[1];
    temp_arr[15] = pulseCounter.byte[0];
    temp_arr[16] = SLC_DST_Rule_Enable;	
    temp_arr[17] = SLC_DST_R_Start_Date;
    temp_arr[18] = SLC_DST_R_Stop_Date;

    Send_data_RF(temp_arr,19,1);		
    }
    else if(Send_Get_Mix_Mode_Sch == 1)   // Send Mix-mode value to DCU.
    {
    Send_Get_Mix_Mode_Sch = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_SLC_MIX_MODE_SCH_RESPONCE;
    temp_arr[5] = Get_Mix_mode_Sch_Loc;

     if(Get_Mix_mode_Sch_Loc == 0)       // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_1);
    }
    else if(Get_Mix_mode_Sch_Loc == 1)    // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_2);
    }
    else if(Get_Mix_mode_Sch_Loc == 2)    // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_3);
    }
    else if(Get_Mix_mode_Sch_Loc == 3)   // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_4);
    }
    else if(Get_Mix_mode_Sch_Loc == 4)    // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_5);
    }
    else  if(Get_Mix_mode_Sch_Loc == 5)    // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_6);
    }
    else if(Get_Mix_mode_Sch_Loc == 6)       // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_7);
    }
    else if(Get_Mix_mode_Sch_Loc == 7)      // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_8);
    }
    else if(Get_Mix_mode_Sch_Loc == 8)      // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_9);
    }
    else if(Get_Mix_mode_Sch_Loc == 9)     // Read location from message and read that locations value from flash.
    {
      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_10);
    }
      if(Get_Mix_mode_Sch_Loc == 10)        // Read location from message and read that locations value from flash.
    {
      Response_Zero_For_Mix_Schedule_11_To_15();
//      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_11);
    }
    else if(Get_Mix_mode_Sch_Loc == 11)      // Read location from message and read that locations value from flash.
    {
            Response_Zero_For_Mix_Schedule_11_To_15();
//      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_12);
    }
    else if(Get_Mix_mode_Sch_Loc == 12)     // Read location from message and read that locations value from flash.
    {
            Response_Zero_For_Mix_Schedule_11_To_15();
//      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_13);
    }
    else if(Get_Mix_mode_Sch_Loc == 13)     // Read location from message and read that locations value from flash.
    {
            Response_Zero_For_Mix_Schedule_11_To_15();
//      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_14);
    }
    else if(Get_Mix_mode_Sch_Loc ==14)     // Read location from message and read that locations value from flash.
    {
            Response_Zero_For_Mix_Schedule_11_To_15();
//      halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_15);
    }

    temp_arr[6] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_En;
    temp_arr[7] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date;
    temp_arr[8] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month;
    temp_arr[9] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date;
    temp_arr[10] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month;
    temp_arr[11] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay;
    temp_arr[12] = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override;

    temp_arr[13] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress1;
    temp_arr[14] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress2;
    temp_arr[15] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress3;
    temp_arr[16] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress4;			
    temp_arr[17] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress5;
    temp_arr[18] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress6;


    temp_arr[19] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress1;
    temp_arr[20] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress2;
    temp_arr[21] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress3;
    temp_arr[22] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress4;			
    temp_arr[23] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress5;
    temp_arr[24] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress6;


    temp_arr[25] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress1;
    temp_arr[26] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress2;
    temp_arr[27] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress3;
    temp_arr[28] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress4;			
    temp_arr[29] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress5;
    temp_arr[30] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress6;

    temp_arr[31] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress1;
    temp_arr[32] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress2;
    temp_arr[33] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress3;
    temp_arr[34] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress4;			
    temp_arr[35] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress5;
    temp_arr[36] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress6;

    temp_arr[37] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress1;
    temp_arr[38] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress2;
    temp_arr[39] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress3;
    temp_arr[40] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress4;			
    temp_arr[41] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress5;
    temp_arr[42] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress6;

    temp_arr[43] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress1;
    temp_arr[44] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress2;
    temp_arr[45] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress3;
    temp_arr[46] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress4;			
    temp_arr[47] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress5;
    temp_arr[48] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress6;

    temp_arr[49] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress1;
    temp_arr[50] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress2;
    temp_arr[51] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress3;
    temp_arr[52] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress4;			
    temp_arr[53] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress5;
    temp_arr[54] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress6;

    temp_arr[55] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress1;
    temp_arr[56] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress2;
    temp_arr[57] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress3;
    temp_arr[58] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress4;			
    temp_arr[59] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress5;
    temp_arr[60] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress6;

    temp_arr[61] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress1;
    temp_arr[62] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress2;
    temp_arr[63] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress3;
    temp_arr[64] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress4;			
    temp_arr[65] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress5;
    temp_arr[66] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress6;
    //emberAfGuaranteedPrint("%p", "\r\nSND[");
    Send_data_RF(temp_arr,67,1);
		}
		else if(Send_Link_key == 1)   // Send responce of link key to DCU.
		{
    Send_Link_key = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_LINK_KEY_SLC;
    temp_arr[5] = Link_Key[0];		
    temp_arr[6] = Link_Key[1];
    temp_arr[7] = Link_Key[2];
    temp_arr[8] = Link_Key[3];
    temp_arr[9] = Link_Key[4];
    temp_arr[10] =Link_Key[5];							
    temp_arr[11] = Link_Key[6];
    temp_arr[12] = Link_Key[7];
    temp_arr[13] = Link_Key[8];
    temp_arr[14] = Link_Key[9];
    temp_arr[15] = Link_Key[10];
    temp_arr[16] = Link_Key[11];
    temp_arr[17] = Link_Key[12];
    temp_arr[18] = Link_Key[13];						
    temp_arr[19] = Link_Key[14];
    temp_arr[20] = Link_Key[15];
    Send_data_RF(temp_arr,21,1);
		}
		else if(send_get_photocell_config == 1)		 // Send photocell Fault configuration to DCU.
		{
    send_get_photocell_config = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_PHOTOCELL_CONFIG;
    temp_arr[5] = Photocell_steady_timeout_Val;		
    temp_arr[6] = photo_cell_toggel_counter_Val;
    temp_arr[7] = photo_cell_toggel_timer_Val;
    temp_arr[8] = Photo_cell_Frequency;
    temp_arr[9] = Photocell_unsteady_timeout_Val/256;
    temp_arr[10] = Photocell_unsteady_timeout_Val%256;
    Send_data_RF(temp_arr,11,1);
		} 
  
  ////////////// get OTA Data ///////// 
  
 else if(send_get_OTA_Data == 1)
 {
   emberAfGuaranteedPrint("send_get_OTA_Data..........:\r\n");
    emberSerialWaitSend(APP_SERIAL);
    send_get_OTA_Data = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_OTA_DATA;
    temp_arr[5] = OTA_Master;		
    temp_arr[6] = 0;//otaClientStartDelay_S/256;
    temp_arr[7] = 0;//otaClientStartDelay_S%256;
    temp_arr[8] = EMBER_AF_PLUGIN_OTA_CLIENT_DOWNLOAD_DELAY_MS/256;
    temp_arr[9] = EMBER_AF_PLUGIN_OTA_CLIENT_DOWNLOAD_DELAY_MS%256;
    temp_arr[10] = 0;//otaClientServerDiscoveryDelay_M;
    temp_arr[11] = 0;//otaClientQueryDelay_M;
    temp_arr[12] = 0;//otaClientDownloadPercentageUpdateEvent;
    temp_arr[13] = otaClientEraseOtaFile;
    temp_arr[14] = Flag_CommandReceivedTORetryMissedOffset;
    temp_arr[15] = otaClientUpgradeNewFirmware;
    temp_arr[16] = otaClientStart;
    temp_arr[17] = otaClientServerDiscoveryComplete;
    temp_arr[18] = serverNodeId/256;
    temp_arr[19] = serverNodeId%256;
    temp_arr[20] = otaClientImageQueryComplete;
    
    pulseCounter.dword = currentDownloadFile.firmwareVersion;
    temp_arr[21] = pulseCounter.byte[3];
    temp_arr[22] = pulseCounter.byte[2];
    temp_arr[23] = pulseCounter.byte[1];
    temp_arr[24] = pulseCounter.byte[0];

    temp_arr[25] = otaClientDownloadPercentage; 
    temp_arr[26] = otaClientNumberOfMissedOffsetBlock/256; 
    temp_arr[27] = otaClientNumberOfMissedOffsetBlock%256;
    temp_arr[28] = otaClientWaitForCommadToRetryMissedOffset;
    temp_arr[29] = otaClientDownLoadComplete;
    temp_arr[30] = otaClientAlreadyUpgraded; //otaClientLastByteMaskIndex;
    temp_arr[31] = otaClientVerificationComplete;   // otaClientVerificationComplete => 1= Success and 2 = Fail 
    Send_data_RF(temp_arr,32,1);
    
 //     int32u currentOffset;
 //     EmberAfOtaStorage Status 
 //   status = emberAfOtaStorageCheckTempDataCallback(&currentOffset, 
 //                                                   &totalImageSize,
 //                                                   &currentDownloadFile);
    
    
    
//      1)  OTA_Master			                1 Byte		1 = master 0 = slave
//      2)  otaClientStartDelay_S			2 Byte 		reprecent in second (delay According to Auto_Send data)
//      3)  otaClientDownloadDelay_MS			2 Byte
//      4)  otaClientServerDiscoveryDelay_M		1 Byte
//      5)  otaClientImageQueryDelay_M			1 Byte
//      6)  otaClientDownloadPercentageUpdateEvent	1 Byte		"it is use to generate event when percentage of download complite" 
//      7)  otaClientEraseOtaFile			1 Byte
//      8)  Flag_CommandReceivedTORetryMissedOffset	1 Byte
//      9)  otaClientUpgradeNewFirmware			1 Byte
//      10) otaClientStart				1 Byte
//      11) otaClientServerDiscoveryComplete		1 Byte		check unknown ota server means FFFE then restart ota************
//      12) otaClientServerShortId			2 Byte		
//      13) otaClientImageQueryComplete			1 Byte		version same , equal or greater generate data accordingly
//      14) otaClientImageID				4 Byte
//      15) otaClientDownloadPercentage			1 Byte
//      16) otaClientNumberOfMissedOffsetBlock		2 Byte
//      17) otaClientWaitForCommadToRetryMissedOffset   1 Byte
//      17) otaClientDownLoadComplete			1 Byte
//      18) otaClientLastByteMaskIndex			1 Byte         otaClientAlreadyUpgraded 
//      19) otaClientVerificationComplete		1 Byte

 }
  else if(send_get_Rs485_DI2_Config == 1)		 // get_Rs485_DI2_Config
  {
    send_get_Rs485_DI2_Config = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_Rs485_DI2_CONFIG;
    temp_arr[5] = RS485_0_Or_DI2_1;		
    Send_data_RF(temp_arr,6,1);
  }
  else if(send_GET_FAULT_REMOVE_CONFIG == 1)		 // get_Rs485_DI2_Config
  {
    send_GET_FAULT_REMOVE_CONFIG = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_FAULT_REMOVE_CONFIG;
    temp_arr[5] = Lamp_Balast_fault_Remove_Time;		
    Send_data_RF(temp_arr,6,1);
  }
  else if(send_GET_LastGasp_CONFIG == 1)		 // GET_LastGasp_CONFIG
  {
    send_GET_LastGasp_CONFIG = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_LastGasp_CONFIG;
    temp_arr[5] = Last_Gsp_Enable;		
    Send_data_RF(temp_arr,6,1);
  }
  else if(Send_LastGasp == 1)		 // GET_LastGasp_CONFIG
  {
    //Send_LastGasp=0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = LASTGASP;
    temp_arr[5] = Date.Date;
    temp_arr[6] = Date.Month;
    temp_arr[7] = Date.Year;
    temp_arr[8] = Time.Hour;
    temp_arr[9] = Time.Min;
    temp_arr[10] = Time.Sec;
    if(LastGasp_unicast_status==2)
    {
      LastGasp_unicast_status=0;
      Send_data_RF(temp_arr,11,2); 
    }
    else
    {
      halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);
      halCommonSetToken(TOKEN_pulses_r,&pulses_r);
      Send_data_RF(temp_arr,11,1); 
    }
    Send_LastGasp=0;    
  }
  /////////////////////// Auto_send_Read_data
  else if(send_get_Auto_send_config == 1)		 // Auto_send_Read_data
  {
    send_get_Auto_send_config = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_AUTO_SEND_READ_DATA;
    temp_arr[5] = Auto_Send_Enable_Disable;		
    temp_arr[6] = Auto_Send_Lograte_Interval;
    temp_arr[7] = Auto_Send_Retry;
    temp_arr[8] = Read_Data_send_Timer;
    temp_arr[9] = Time_Slice/256;
    temp_arr[10] = Time_Slice%256;
    temp_arr[11] = Configured_Event.Val;
    Send_data_RF(temp_arr,12,1);
  }
  //////////////////////////
  else if(send_get_network_search_para == 1)    // Send responce of network search parameters to Dcu.
  {
    send_get_network_search_para = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_NETWORK_SEARCH_PARA;
    temp_arr[5] = SLC_Start_netwrok_Search_Timeout;		
    temp_arr[6] = Network_Scan_Cnt_Time_out / 3600;
    Send_data_RF(temp_arr,7,1);
  }
  else if(Send_curv_type == 1)    // Send responce of network search parameters to Dcu.
  {
    Send_curv_type = 0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = CURV_TYPE_GET;
    temp_arr[5] = CURV_TYPE;		
    Send_data_RF(temp_arr,6,1);
  }
  else if(send_get_Gps_Config == 1)
  {
#if (defined(ISLC_3100_V7) || defined(ISLC_3100_7P_V1)|| defined(ISLC_3100_V7_2)||defined(ISLC_3100_V7_3_0216)||defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9)||defined(ISLC_3100_480V_Rev3) || defined(ISLC_3100_V9)|| defined(ISLC_3100_V9_2))

    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_GPS_CONFIG;
    temp_arr[5] = GPS_Read_Enable;		
    temp_arr[6] = GPS_error_condition.Val;
    temp_arr[7] = GPS_Wake_timer/256;
    temp_arr[8] = GPS_Wake_timer%256;
    temp_arr[9] = No_of_setalite_threshold;		
    temp_arr[10] = Setalite_angle_threshold;
    temp_arr[11] = setalite_Rssi_threshold;
    temp_arr[12] = Maximum_no_of_Setalite_reading;

    pulseCounter.float_data = HDOP_Gps_threshold;
    temp_arr[13] = pulseCounter.byte[3];
    temp_arr[14] = pulseCounter.byte[2];
    temp_arr[15] = pulseCounter.byte[1];
    temp_arr[16] = pulseCounter.byte[0];

    temp_arr[17] = Check_Sbas_Enable;
    temp_arr[18] = Sbas_setalite_Rssi_threshold;
    temp_arr[19] = Auto_Gps_Data_Send;

    pulseCounter.int_data = averaging_counter;
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    Send_data_RF(temp_arr,22,1);

    if(GPS_Data_Valid != 3)
    {
      GPS_error_condition.Val = 0x00;
    }
#endif
    send_get_Gps_Config = 0;
  }
//////////////////////////////////////

  else if(Send_Read_Scene_From_Dali == 1)
  {

    Send_Read_Scene_From_Dali =0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = READ_SCENE_FROM_DALI;
    temp_arr[5] = DALI_Ballast_Info[0][0];
    temp_arr[6] = DALI_Ballast_Info[0][1];
    temp_arr[7] = DALI_Ballast_Info[0][2];
    temp_arr[8] = DALI_Ballast_Info[0][3];
    temp_arr[9] = DALI_Ballast_Info[0][4];
    temp_arr[10] = DALI_Ballast_Info[0][5];
    temp_arr[11] = DALI_Ballast_Info[0][6];
    temp_arr[12] = DALI_Ballast_Info[0][7];
    temp_arr[13] = DALI_Ballast_Info[0][8];
    temp_arr[14] = DALI_Ballast_Info[0][9];
    temp_arr[15] = DALI_Ballast_Info[0][10];
    temp_arr[16] = DALI_Ballast_Info[0][11];
    temp_arr[17] = DALI_Ballast_Info[0][12];
    temp_arr[18] = DALI_Ballast_Info[0][13];
    temp_arr[19] = DALI_Ballast_Info[0][14];
    temp_arr[20] = DALI_Ballast_Info[0][15];

    temp_arr[21] = DALI_Ballast_Info[1][0];
    temp_arr[22] = DALI_Ballast_Info[1][1];
    temp_arr[23] = DALI_Ballast_Info[1][2];
    temp_arr[24] = DALI_Ballast_Info[1][3];
    temp_arr[25] = DALI_Ballast_Info[1][4];
    temp_arr[26] = DALI_Ballast_Info[1][5];
    temp_arr[27] = DALI_Ballast_Info[1][6];
    temp_arr[28] = DALI_Ballast_Info[1][7];
    temp_arr[29] = DALI_Ballast_Info[1][8];
    temp_arr[30] = DALI_Ballast_Info[1][9];
    temp_arr[31] = DALI_Ballast_Info[1][10];
    temp_arr[32] = DALI_Ballast_Info[1][11];
    temp_arr[33] = DALI_Ballast_Info[1][12];
    temp_arr[34] = DALI_Ballast_Info[1][13];
    temp_arr[35] = DALI_Ballast_Info[1][14];
    temp_arr[36] = DALI_Ballast_Info[1][15];

    temp_arr[37] = DALI_Ballast_Info[2][0];
    temp_arr[38] = DALI_Ballast_Info[2][1];
    temp_arr[39] = DALI_Ballast_Info[2][2];
    temp_arr[40] = DALI_Ballast_Info[2][3];
    temp_arr[41] = DALI_Ballast_Info[2][4];
    temp_arr[42] = DALI_Ballast_Info[2][5];
    temp_arr[43] = DALI_Ballast_Info[2][6];
    temp_arr[44] = DALI_Ballast_Info[2][7];
    temp_arr[45] = DALI_Ballast_Info[2][8];
    temp_arr[46] = DALI_Ballast_Info[2][9];
    temp_arr[47] = DALI_Ballast_Info[2][10];
    temp_arr[48] = DALI_Ballast_Info[2][11];
    temp_arr[49] = DALI_Ballast_Info[2][12];
    temp_arr[50] = DALI_Ballast_Info[2][13];
    temp_arr[51] = DALI_Ballast_Info[2][14];
    temp_arr[52] = DALI_Ballast_Info[2][15];

    temp_arr[53] = DALI_Ballast_Info[3][0];
    temp_arr[54] = DALI_Ballast_Info[3][1];
    temp_arr[55] = DALI_Ballast_Info[3][2];
    temp_arr[56] = DALI_Ballast_Info[3][3];
    temp_arr[57] = DALI_Ballast_Info[3][4];
    temp_arr[58] = DALI_Ballast_Info[3][5];
    temp_arr[59] = DALI_Ballast_Info[3][6];
    temp_arr[60] = DALI_Ballast_Info[3][7];
    temp_arr[61] = DALI_Ballast_Info[3][8];
    temp_arr[62] = DALI_Ballast_Info[3][9];
    temp_arr[63] = DALI_Ballast_Info[3][10];
    temp_arr[64] = DALI_Ballast_Info[3][11];
    temp_arr[65] = DALI_Ballast_Info[3][12];
    temp_arr[66] = DALI_Ballast_Info[3][13];
    temp_arr[67] = DALI_Ballast_Info[3][14];
    temp_arr[68] = DALI_Ballast_Info[3][15];

    Send_data_RF(temp_arr,69,1);

  }
//////////////////////////////////////
  ////////////
    else if(GET_AUTO_NETWORK_LEAVE_Flag == 1)
    {

      temp_arr[0]='S';
      temp_arr[1]='L';
      temp_arr[2]=amr_id/256;
      temp_arr[3]=amr_id%256;
      temp_arr[4] = GET_AUTO_NETWORK_LEAVE;
      temp_arr[5] = Network_leave_Flag;
      pulseCounter.int_data =Network_Leave_Hr_Value;
      temp_arr[6] = pulseCounter.byte[1];
      temp_arr[7] = pulseCounter.byte[0];
      Send_data_RF(temp_arr,8,1);
      GET_AUTO_NETWORK_LEAVE_Flag =0;
    }
  
  ///////////////
    else if(GET_DALI_AO_CONFIG_Flag == 1)
    {
      temp_arr[0]='S';
      temp_arr[1]='L';
      temp_arr[2]=amr_id/256;
      temp_arr[3]=amr_id%256;
      temp_arr[4] = GET_DALI_AO_CONFIG;
      temp_arr[5] = DimmerDriverSelectionProcess;
      temp_arr[6] = dali_support;
      Send_data_RF(temp_arr,7,1);
      GET_DALI_AO_CONFIG_Flag =0;
    }  
  else if(Test_Config_Data_Send == 1)
  {

    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GET_CONF_CMD_IN_TEST_MODE;
    if((SLC_Test_Query_2_received == 1)||(Commissioning_flag == 1))
    {
        halCommonGetToken(&automode,TOKEN_automode);
        temp_arr[5] = automode;
        automode =0;
    }
    else
    {
     temp_arr[5] = automode;
    }

    pulseCounter.float_data = Latitude;
    temp_arr[6] = pulseCounter.byte[3];
    temp_arr[7] = pulseCounter.byte[2];
    temp_arr[8] = pulseCounter.byte[1];
    temp_arr[9] = pulseCounter.byte[0];

    pulseCounter.float_data = longitude;
    temp_arr[10] = pulseCounter.byte[3];
    temp_arr[11] = pulseCounter.byte[2];
    temp_arr[12] = pulseCounter.byte[1];
    temp_arr[13] = pulseCounter.byte[0];

    pulseCounter.float_data = utcOffset;
    temp_arr[14] = pulseCounter.byte[3];
    temp_arr[15] = pulseCounter.byte[2];
    temp_arr[16] = pulseCounter.byte[1];
    temp_arr[17] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)Sunset_delay;
    temp_arr[18] = pulseCounter.byte[3];
    temp_arr[19] = pulseCounter.byte[2];
    temp_arr[20] = pulseCounter.byte[1];
    temp_arr[21] = pulseCounter.byte[0];

    pulseCounter.float_data = (float)Sunrise_delay;
    temp_arr[22] = pulseCounter.byte[3];
    temp_arr[23] = pulseCounter.byte[2];
    temp_arr[24] = pulseCounter.byte[1];
    temp_arr[25] = pulseCounter.byte[0];

    temp_arr[26] = GPS_Data_throw_status;

    temp_arr[27] = SLC_DST_En;
    temp_arr[28] = SLC_DST_Start_Month;
    temp_arr[29] = SLC_DST_Start_Rule;
    temp_arr[30] = SLC_DST_Start_Time;
    temp_arr[31] = SLC_DST_Stop_Month;
    temp_arr[32] = SLC_DST_Stop_Rule;
    temp_arr[33] = SLC_DST_Stop_Time;

    pulseCounter.float_data = SLC_DST_Time_Zone_Diff;				
    temp_arr[34] = pulseCounter.byte[3];
    temp_arr[35] = pulseCounter.byte[2];
    temp_arr[36] = pulseCounter.byte[1];
    temp_arr[37] = pulseCounter.byte[0];
    temp_arr[38] = SLC_DST_Rule_Enable;	
    temp_arr[39] = SLC_DST_R_Start_Date;
    temp_arr[40] = SLC_DST_R_Stop_Date;

    Send_data_RF(temp_arr,41,1);
    Test_Config_Data_Send =0;

  }
  else if(Get_calibration_data == 1)
  {
    Get_calibration_data =0;
    temp_arr[0]='S';
    temp_arr[1]='L';
    temp_arr[2]=amr_id/256;
    temp_arr[3]=amr_id%256;
    temp_arr[4] = GATE_CALIBRATION_DATA;

    pulseCounter.float_data = Vr_Cal;
    temp_arr[5] = pulseCounter.byte[3];
    temp_arr[6] = pulseCounter.byte[2];
    temp_arr[7] = pulseCounter.byte[1];
    temp_arr[8] = pulseCounter.byte[0];

    pulseCounter.float_data = Ir_Cal;
    temp_arr[9] = pulseCounter.byte[3];
    temp_arr[10] = pulseCounter.byte[2];
    temp_arr[11] = pulseCounter.byte[1];
    temp_arr[12] = pulseCounter.byte[0];

    pulseCounter.float_data = KW_Cal;
    temp_arr[13] = pulseCounter.byte[3];
    temp_arr[14] = pulseCounter.byte[2];
    temp_arr[15] = pulseCounter.byte[1];
    temp_arr[16] = pulseCounter.byte[0];

    if(send_to_Router == 1)
    {
      send_to_Router =0;
        Send_data_RF(temp_arr,17,1);
    }
    else
    {
      Send_data_RF(temp_arr,17,0);
    }


  }
//  else if(Send_Time_Of_Use_Sch == 1)
//  {
//    Send_Time_Of_Use_Sch =0;
//    temp_arr[0]='S';
//    temp_arr[1]='L';
//    temp_arr[2]=amr_id/256;
//    temp_arr[3]=amr_id%256;
//    temp_arr[4] = GET_TIME_OF_USE;
//    temp_arr[5] = Get_Sch_no;
//
//
//    if((Get_Sch_no<0)||(Get_Sch_no>1)) //If received value improper return 1 schedule only
//        Get_Sch_no =0;
//    temp_arr[6] = Time_Slot.Slot_Start_Date[Get_Sch_no];
//    temp_arr[7] = Time_Slot.Slot_Start_Month[Get_Sch_no];
//    temp_arr[8] = Time_Slot.Slot_Start_Year[Get_Sch_no];
//
//    temp_arr[9] = Time_Slot.Slot_End_Date[Get_Sch_no];
//    temp_arr[10] = Time_Slot.Slot_End_Month[Get_Sch_no];
//    temp_arr[11] = Time_Slot.Slot_End_Year[Get_Sch_no];
//    for(a=0;a<2;a++)
//    {
//        temp_arr[(a*12)+12] = Time_Slot.Slot_Start_Hour[(Get_Sch_no*6)+(a*3)+0];
//        temp_arr[(a*12)+13] = Time_Slot.Slot_Start_Min[(Get_Sch_no*6)+(a*3)+0];
//
//        temp_arr[(a*12)+14] = Time_Slot.Slot_End_Hour[(Get_Sch_no*6)+(a*3)+0];
//        temp_arr[(a*12)+15] = Time_Slot.Slot_End_Min[(Get_Sch_no*6)+(a*3)+0];
//
//        temp_arr[(a*12)+16] = Time_Slot.Slot_Start_Hour[(Get_Sch_no*6)+(a*3)+1];
//        temp_arr[(a*12)+17] = Time_Slot.Slot_Start_Min[(Get_Sch_no*6)+(a*3)+1];
//
//        temp_arr[(a*12)+18] = Time_Slot.Slot_End_Hour[(Get_Sch_no*6)+(a*3)+1];
//        temp_arr[(a*12)+19] = Time_Slot.Slot_End_Min[(Get_Sch_no*6)+(a*3)+1];
//
//        temp_arr[(a*12)+20] = Time_Slot.Slot_Start_Hour[(Get_Sch_no*6)+(a*3)+2];
//        temp_arr[(a*12)+21] = Time_Slot.Slot_Start_Min[(Get_Sch_no*6)+(a*3)+2];
//
//        temp_arr[(a*12)+22] = Time_Slot.Slot_End_Hour[(Get_Sch_no*6)+(a*3)+2];
//        temp_arr[(a*12)+23] = Time_Slot.Slot_End_Min[(Get_Sch_no*6)+(a*3)+2];
//    }
//
//    Send_data_RF(temp_arr,36,1);
//
//  }
//  else if(Send_Kwh_Slot_To_Dcu == 1)
//  {
//    Send_Kwh_Slot_To_Dcu =0;
//    temp_arr[0]='S';
//    temp_arr[1]='L';
//    temp_arr[2]=amr_id/256;
//    temp_arr[3]=amr_id%256;
//    temp_arr[4] = GET_KWH_SLOT;
//    temp_arr[5] = Get_Sch_no;
//
//    pulseCounter.float_data = Time_Slot.kwh_value[(Get_Sch_no*6)+0];
//    temp_arr[6] = pulseCounter.byte[3];
//    temp_arr[7] = pulseCounter.byte[2];
//    temp_arr[8] = pulseCounter.byte[1];
//    temp_arr[9] = pulseCounter.byte[0];
//
//    pulseCounter.float_data = Time_Slot.kwh_value[(Get_Sch_no*6)+1];
//    temp_arr[10] = pulseCounter.byte[3];
//    temp_arr[11] = pulseCounter.byte[2];
//    temp_arr[12] = pulseCounter.byte[1];
//    temp_arr[13] = pulseCounter.byte[0];
//
//    pulseCounter.float_data = Time_Slot.kwh_value[(Get_Sch_no*6)+2];
//    temp_arr[14] = pulseCounter.byte[3];
//    temp_arr[15] = pulseCounter.byte[2];
//    temp_arr[16] = pulseCounter.byte[1];
//    temp_arr[17] = pulseCounter.byte[0];
//
//    pulseCounter.float_data = Time_Slot.kwh_value[(Get_Sch_no*6)+3];
//    temp_arr[18] = pulseCounter.byte[3];
//    temp_arr[19] = pulseCounter.byte[2];
//    temp_arr[20] = pulseCounter.byte[1];
//    temp_arr[21] = pulseCounter.byte[0];
//
//    pulseCounter.float_data = Time_Slot.kwh_value[(Get_Sch_no*6)+4];
//    temp_arr[22] = pulseCounter.byte[3];
//    temp_arr[23] = pulseCounter.byte[2];
//    temp_arr[24] = pulseCounter.byte[1];
//    temp_arr[25] = pulseCounter.byte[0];
//
//    pulseCounter.float_data = Time_Slot.kwh_value[(Get_Sch_no*6)+5];
//    temp_arr[26] = pulseCounter.byte[3];
//    temp_arr[27] = pulseCounter.byte[2];
//    temp_arr[28] = pulseCounter.byte[1];
//    temp_arr[29] = pulseCounter.byte[0];
//    Send_data_RF(temp_arr,30,1);
//
//  }
  //////////
  /////////////Calibration///////////////
  else if(Send_Voltage_calibration == 1)
   {
	Send_Voltage_calibration = 0;
	Cali_buff[0]=0x8E;
	Cali_buff[1]=0x4C;
//	Cali_buff[2]=amr_id/256;
//	Cali_buff[3]=amr_id%256;
///////////////////
        pulseCounter.long_data=amr_id;
        Cali_buff[2]=pulseCounter.byte[0];
        Cali_buff[3]=pulseCounter.byte[1];
        Cali_buff[4]=pulseCounter.byte[2];
        Cali_buff[5]=pulseCounter.byte[3];
///////////////////
	Cali_buff[6] = 0x92;
	pulseCounter.float_data = Vr_Cal;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];

        pulseCounter.float_data = Calculated_Frequency_row;
        Cali_buff[11] = pulseCounter.byte[3];
        Cali_buff[12] = pulseCounter.byte[2];
        Cali_buff[13] = pulseCounter.byte[1];
        Cali_buff[14] = pulseCounter.byte[0];
        ///////////
       // WriteData_UART1(Cali_buff,11);

        send_data_on_RS485((char *)Cali_buff,15);


    }
    else if(Send_Current_calibration == 1)
    {
        Send_Current_calibration = 0;
        Cali_buff[0]=0x8E;
        Cali_buff[1]=0x4C;
//	Cali_buff[2]=amr_id/256;
//	Cali_buff[3]=amr_id%256;
///////////////////
        pulseCounter.long_data=amr_id;
        Cali_buff[2]=pulseCounter.byte[0];
        Cali_buff[3]=pulseCounter.byte[1];
        Cali_buff[4]=pulseCounter.byte[2];
        Cali_buff[5]=pulseCounter.byte[3];
///////////////////
        Cali_buff[6] = 0x93;
        pulseCounter.float_data = Ir_Cal;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
      //  WriteData_UART1(Cali_buff,11);

        send_data_on_RS485((char *)Cali_buff,11);

    }
    else if(Send_Kw_calibration == 1)
    {
        Send_Kw_calibration = 0;
        Cali_buff[0]=0x8E;
        Cali_buff[1]=0x4C;
//	Cali_buff[2]=amr_id/256;
//	Cali_buff[3]=amr_id%256;
///////////////////
         pulseCounter.long_data=amr_id;
        Cali_buff[2]=pulseCounter.byte[0];
        Cali_buff[3]=pulseCounter.byte[1];
        Cali_buff[4]=pulseCounter.byte[2];
        Cali_buff[5]=pulseCounter.byte[3];

///////////////////
        Cali_buff[6] = 0x94;
        pulseCounter.float_data = KW_Cal;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
       // WriteData_UART1(Cali_buff,11);
       
        send_data_on_RS485((char *)Cali_buff,11);

    }
////////////////////////////////
    else if(Send_Kw_calibration_120 == 1)
    {
            Send_Kw_calibration_120 = 0;
            Cali_buff[0]=0x8E;
            Cali_buff[1]=0x4C;

    ///////////////////
                    pulseCounter.long_data=amr_id;
                    Cali_buff[2]=pulseCounter.byte[0];
                    Cali_buff[3]=pulseCounter.byte[1];
                    Cali_buff[4]=pulseCounter.byte[2];
                    Cali_buff[5]=pulseCounter.byte[3];

///////////////////
            Cali_buff[6] = 0x97;
            pulseCounter.float_data = KW_Cal_120;
            Cali_buff[7] = pulseCounter.byte[3];
            Cali_buff[8] = pulseCounter.byte[2];
            Cali_buff[9] = pulseCounter.byte[1];
            Cali_buff[10] = pulseCounter.byte[0];
            //WriteData_UART1(temp_arr,11);

            send_data_on_RS485((char *)Cali_buff,11);

    }

////////////////////////////////
    else if(Read_Em_Calibration == 1)
    {
        Read_Em_Calibration = 0;
        Cali_buff[0]=0x8E;
        Cali_buff[1]=0x4C;
//	Cali_buff[2]=amr_id/256;
//	Cali_buff[3]=amr_id%256;
///////////////////
         pulseCounter.long_data=amr_id;
        Cali_buff[2]=pulseCounter.byte[0];
        Cali_buff[3]=pulseCounter.byte[1];
        Cali_buff[4]=pulseCounter.byte[2];
        Cali_buff[5]=pulseCounter.byte[3];

///////////////////
        Cali_buff[6] = 0x95;

        Cali_buff[7] = Date.Date;
        Cali_buff[8] = Date.Month;
        Cali_buff[9] = Date.Year;
        Cali_buff[10] = Time.Hour;
        Cali_buff[11] = Time.Min;
        Cali_buff[12] = Time.Sec;

        pulseCounter.float_data = vrdoub;
        Cali_buff[13] = pulseCounter.byte[3];
        Cali_buff[14] = pulseCounter.byte[2];
        Cali_buff[15] = pulseCounter.byte[1];
        Cali_buff[16] = pulseCounter.byte[0];
        pulseCounter.float_data = irdoub;
        Cali_buff[17] = pulseCounter.byte[3];
        Cali_buff[18] = pulseCounter.byte[2];
        Cali_buff[19] = pulseCounter.byte[1];
        Cali_buff[20] = pulseCounter.byte[0];
        pulseCounter.float_data = kwh;
        Cali_buff[21] = pulseCounter.byte[3];
        Cali_buff[22] = pulseCounter.byte[2];
        Cali_buff[23] = pulseCounter.byte[1];
        Cali_buff[24] = pulseCounter.byte[0];
//////////////////////////////
			Cali_Cnt++;
			if(Cali_Cnt == 3)//added patch on 6Jan2017 due to cali appl
			{
				Cali_Cnt =0;
				kwh=0;
				pulses_r =0;

                                halCommonSetToken(TOKEN_pulses_r,&pulses_r);
//				pulseCounter.long_data = pulses_r;
//				WriteByte_EEPROM(EE_KWH+0,pulseCounter.byte[0]);
//				WriteByte_EEPROM(EE_KWH+1,pulseCounter.byte[1]);
//				WriteByte_EEPROM(EE_KWH+2,pulseCounter.byte[2]);
//				WriteByte_EEPROM(EE_KWH+3,pulseCounter.byte[3]);
			}
///////////////////////////////			

                /*Frequency_Cal Added in 1.0.0014beta1 version*/
        pulseCounter.float_data = Calculated_Frequency;//SilverSpring_Frequency;
        Cali_buff[25] = pulseCounter.byte[3];
        Cali_buff[26] = pulseCounter.byte[2];
        Cali_buff[27] = pulseCounter.byte[1];
        Cali_buff[28] = pulseCounter.byte[0];

        pulseCounter.float_data = pf;
        Cali_buff[29] = pulseCounter.byte[3];
        Cali_buff[30] = pulseCounter.byte[2];
        Cali_buff[31] = pulseCounter.byte[1];
        Cali_buff[32] = pulseCounter.byte[0];
       // WriteData_UART1(Cali_buff,33);
    
            send_data_on_RS485((char *)Cali_buff,33);
    
    }
    else if(Read_KWH_Value == 1)
    {
        Read_KWH_Value =0;

        Cali_buff[0]=0x8E;
        Cali_buff[1]=0x4C;
//	Cali_buff[2]=amr_id/256;
//	Cali_buff[3]=amr_id%256;
///////////////////
         pulseCounter.long_data=amr_id;
        Cali_buff[2]=pulseCounter.byte[0];
        Cali_buff[3]=pulseCounter.byte[1];
        Cali_buff[4]=pulseCounter.byte[2];
        Cali_buff[5]=pulseCounter.byte[3];

///////////////////
        Cali_buff[6] = 0x96;
        ////////////
//        pulseCounter.byte[0] = 0;//ReadByte_EEPROM(EE_KWH+0);
//        pulseCounter.byte[1] = 0;//ReadByte_EEPROM(EE_KWH+1);
//        pulseCounter.byte[2] = 0;//ReadByte_EEPROM(EE_KWH+2);
//        pulseCounter.byte[3] = 0;//ReadByte_EEPROM(EE_KWH+3);
//        pulses_r = pulseCounter.long_data;
        halCommonGetToken(&pulses_r,TOKEN_pulses_r);
        kwh=(((float)pulses_r*mf3));
        /////////////
        pulseCounter.float_data = kwh;
        Cali_buff[7] = pulseCounter.byte[3];
        Cali_buff[8] = pulseCounter.byte[2];
        Cali_buff[9] = pulseCounter.byte[1];
        Cali_buff[10] = pulseCounter.byte[0];
     //   WriteData_UART1(Cali_buff,11);
  
            send_data_on_RS485((char *)Cali_buff,11);
  
    }
/////////////////////////////
//  else if(Send_Gps_Statistic == 1)
//  {
//    temp_arr[0]='S';
//    temp_arr[1]='L';
//    temp_arr[2]=amr_id/256;
//    temp_arr[3]=amr_id%256;
//    temp_arr[4] = GPS_STATISTIC;
//
//    temp_arr[5] = GPS_Info[0].Status;		
//    temp_arr[6] = GPS_Info[0].Date;
//    temp_arr[7] = GPS_Info[0].Month;
//    temp_arr[8] = GPS_Info[0].Year;
//    temp_arr[9] = GPS_Info[0].hour;
//    temp_arr[10] = GPS_Info[0].Min;
//    temp_arr[11] = GPS_Info[0].Sec;
//    pulseCounter.float_data = GPS_Info[0].Latitude;				
//    temp_arr[12] = pulseCounter.byte[3];
//    temp_arr[13] = pulseCounter.byte[2];
//    temp_arr[14] = pulseCounter.byte[1];
//    temp_arr[15] = pulseCounter.byte[0];
//    pulseCounter.float_data = GPS_Info[0].Longitude;				
//    temp_arr[16] = pulseCounter.byte[3];
//    temp_arr[17] = pulseCounter.byte[2];
//    temp_arr[18] = pulseCounter.byte[1];
//    temp_arr[19] = pulseCounter.byte[0];
//    temp_arr[20] = GPS_Info[0].GPS_Data_Valid;
//    temp_arr[21] = GPS_Info[0].threeD_Fix_successfully;
//    temp_arr[22] = GPS_Info[0].Setalite_Visinity_Proper;
//    temp_arr[23] = GPS_Info[0].Setalite_reading_OK;
//    temp_arr[24] = GPS_Info[0].SBAS_Active;
//    temp_arr[25] = GPS_Info[0].gagan_Setalite_Id;
//    temp_arr[26] = GPS_Info[0].gagan_Setalite_RSSI;
//    temp_arr[27] = GPS_Info[0].HDOP_OK;
//
//    temp_arr[28] = GPS_Info[1].Status;		
//    temp_arr[29] = GPS_Info[1].Date;
//    temp_arr[30] = GPS_Info[1].Month;
//    temp_arr[31] = GPS_Info[1].Year;
//    temp_arr[32] = GPS_Info[1].hour;
//    temp_arr[33] = GPS_Info[1].Min;
//    temp_arr[34] = GPS_Info[1].Sec;
//    pulseCounter.float_data = GPS_Info[1].Latitude;				
//    temp_arr[35] = pulseCounter.byte[3];
//    temp_arr[36] = pulseCounter.byte[2];
//    temp_arr[37] = pulseCounter.byte[1];
//    temp_arr[38] = pulseCounter.byte[0];
//    pulseCounter.float_data = GPS_Info[1].Longitude;				
//    temp_arr[39] = pulseCounter.byte[3];
//    temp_arr[40] = pulseCounter.byte[2];
//    temp_arr[41] = pulseCounter.byte[1];
//    temp_arr[42] = pulseCounter.byte[0];
//    temp_arr[43] = GPS_Info[1].GPS_Data_Valid;
//    temp_arr[44] = GPS_Info[1].threeD_Fix_successfully;
//    temp_arr[45] = GPS_Info[1].Setalite_Visinity_Proper;
//    temp_arr[46] = GPS_Info[1].Setalite_reading_OK;
//    temp_arr[47] = GPS_Info[1].SBAS_Active;
//    temp_arr[48] = GPS_Info[1].gagan_Setalite_Id;
//    temp_arr[49] = GPS_Info[1].gagan_Setalite_RSSI;
//    temp_arr[50] = GPS_Info[1].HDOP_OK;
//
//    pulseCounter.int_data = averaging_counter;
//    temp_arr[51] = pulseCounter.byte[1];
//    temp_arr[52] = pulseCounter.byte[0];
//
//    temp_arr[53] = Packate_send_threshold_success;
//    temp_arr[54] = Packate_send_success;
//
//    temp_arr[55] = GPRMC_Received_counter;
//    temp_arr[56] = GPGSA_Received_counter;
//    temp_arr[57] = GPGGA_Received_counter;
//
//    pulseCounter.int_data = GPGSV_Received_counter;
//    temp_arr[58] = pulseCounter.byte[1];
//    temp_arr[59] = pulseCounter.byte[0];
//
//    Send_data_RF(temp_arr,60,1);
//    Send_Gps_Statistic = 0;
//    GPRMC_Received_counter = 0;
//    GPGSA_Received_counter = 0;
//    GPGGA_Received_counter = 0;
//    GPGSV_Received_counter = 0;
//  }
		else
		{

		}								
}

/*************************************************************************
Function Name: Send_data_RF
input: data pointer,length of message,only DCU.
Output: none.
Discription: send message to ember function.
*************************************************************************/

void Send_data_RF(unsigned char *data,unsigned char len,unsigned char only_DCU)
{
  unsigned char base=0,i=0;
  unsigned char sum=0;
  

  

if(amr_id >65535)
{
  if((data[0]!='I')&&(data[1]!='D'))
  {
    for(i=len;i>=0;i--)
    {
     data[i+6]=data[i];
     if(i == 0)
     {
       break;
     }
    }
    data[0]=SLC_ID_Buf[0];
    data[1]=SLC_ID_Buf[1];
    data[2]=SLC_ID_Buf[2];
    data[3]=SLC_ID_Buf[3];
    data[4]=SLC_ID_Buf[4];
    data[5]=SLC_ID_Buf[5];
    len =len+6;
  }
}
  if(Send_To_DCU == 1)
  {
    Send_To_DCU =2;
    received_short_id=0;
  }
  else if(LastGasp_unicast_status==1)
  {
    LastGasp_unicast_status=2;
    received_short_id=0;
  }
  else if (serverNodeId == received_short_id) //to avoid unnecessary response to OTA server
  {
    received_short_id =0;
  }
    
  if(only_DCU == 1)       // this flag is for indication of send message to co-ordinator or router.
  {
    received_short_id=0;
  }
  else
  {

  }
  send_data_frame(received_short_id,data,len,only_DCU);  // pass all message parameters to ember function.
}

/*************************************************************************
Function Name: Send_data_RF_idimmer
input: data pointer,length of message.
Output: none.
Discription: send message to ember function for Idimmer.
*************************************************************************/
void Send_data_RF_idimmer(unsigned char *data,unsigned char len)
{

}	


/*************************************************************************
Function Name: store_data_in_mem
input: none.
Output: none.
Discription: store event data in to ram. maximum 20 event we can store in
             RAM.
*************************************************************************/

void store_data_in_mem(void)
{
  unsigned char i=0,j=0,temp_break_flag = 0;
  
  for(i=0;i<20;i++)
  {
    if(temp_data_buff[i][0] == 0)					//store message if buff free
    {
      temp_break_flag = 1;
      break;
    }
  }

  if(temp_break_flag == 0)    // if buffer not free then delet last data from memory
  {
    for(i=delet_no;i<20;i++)
    {
      if((temp_data_buff[i][0] == 1))					//store message if buff free   && (temp_data_retrive_counter[i] == 3)
      {
        temp_data_retrive_counter[i] = 0;
        ///////////////
        if((temp_data_buff[i][6] == 25)||(temp_data_buff[i][6] == 26))
        {
          
        }
        if(temp_data_buff[i][6] == 27)
        {
          Lamp_Fail =0;

        }
        if(temp_data_buff[i][6] == 28)
        {
                    Ballast_Fail =0;
        }
        if(temp_data_buff[i][6] == 29)
        {
          Day_Burner =0;
        }
        ///////////////
        for(j=0;j<40;j++)
        {
          temp_data_buff[i][j] = 0;
        }
//        ///////////////
//        if((temp_data_buff[i][0] == 25)||(temp_data_buff[i][0] == 26))
//        {
//          
//        }
//        if(temp_data_buff[i][0] == 27)
//        {
//          Lamp_Fail =0;
//
//        }
//        if(temp_data_buff[i][0] == 28)
//        {
//                    Ballast_Fail =0;
//        }
//        if(temp_data_buff[i][0] == 29)
//        {
//          Day_Burner =0;
//        }
//        ///////////////
        
        no_of_pending_event--;
        delet_no = i+1;
        break;
      }
    }
    if(delet_no >= 20)
    {
      delet_no = 0;	
    }
  }
/////////////////////
  if((Auto_Send_Enable_Disable == 1)||((Auto_Send_Enable_Disable == 3)))
  {
    Frm_no =255;
    if((error_condition.bits.b0 != Lamp_ON_OFF ) &&(Configured_Event.bits.b0 == 1))
    {   
        Event_Detected =1;
        Event_Send_Cnt =0;
        Lamp_ON_OFF = error_condition.bits.b0;
        if(error_condition.bits.b0)
          Frm_no =25;
        else
           Frm_no =26;        
    }
    if(( (error_condition.bits.b3 ==1) && (Configured_Event.bits.b1 == 1))&&(Lamp_Fail == 0))
    {
        Event_Detected =1;
        Event_Send_Cnt =0;
        Lamp_Fail =1;
        Frm_no =27;      
    }
    if(( error_condition1.bits.b0 && (Configured_Event.bits.b2 == 1))&&(Ballast_Fail == 0))
    {
        Event_Detected =1;
        Event_Send_Cnt =0;
        Ballast_Fail =1;
        Frm_no =28;      
    }
    if((error_condition1.bits.b7 && (Configured_Event.bits.b3 == 1))&&(Day_Burner == 0))
    {
         Event_Detected =1;
        Event_Send_Cnt =0;
        Day_Burner =1;
        Frm_no =29;      
    }
  }  
  
/////////////////////
  for(i=0;i<20;i++)
  {
    if(temp_data_buff[i][0] == 0)					//store message if buff free
    {
      no_of_pending_event++;
      temp_data_buff[i][0] = 1;
      Rf_Track_Id++;
      temp_data_buff[i][1]='S';
      temp_data_buff[i][2]='L';
      temp_data_buff[i][3]=amr_id/256;
      temp_data_buff[i][4]=amr_id%256;
      temp_data_buff[i][5] = 'E';
      temp_data_buff[i][6] = Frm_no;
      temp_data_buff[i][7] = Date.Date;
      temp_data_buff[i][8] = Date.Month;
      temp_data_buff[i][9] = Date.Year;
      temp_data_buff[i][10] = Time.Hour;
      temp_data_buff[i][11] = Time.Min;
      temp_data_buff[i][12] = Time.Sec;

      temp_data_buff[i][13] = error_condition.Val;		//remove if we use above commented section.
      temp_data_buff[i][14] = error_condition1.Val;

      pulseCounter.float_data = vrdoub;
      temp_data_buff[i][15] = pulseCounter.byte[3];
      temp_data_buff[i][16] = pulseCounter.byte[2];
      temp_data_buff[i][17] = pulseCounter.byte[1];
      temp_data_buff[i][18] = pulseCounter.byte[0];

      pulseCounter.float_data = irdoub;
      temp_data_buff[i][19] = pulseCounter.byte[3];
      temp_data_buff[i][20] = pulseCounter.byte[2];
      temp_data_buff[i][21] = pulseCounter.byte[1];
      temp_data_buff[i][22] = pulseCounter.byte[0];

      pulseCounter.float_data = w1/1000.0;
      temp_data_buff[i][23] = pulseCounter.byte[3];
      temp_data_buff[i][24] = pulseCounter.byte[2];
      temp_data_buff[i][25] = pulseCounter.byte[1];
      temp_data_buff[i][26] = pulseCounter.byte[0];

      pulseCounter.float_data = kwh;
      temp_data_buff[i][27] = pulseCounter.byte[3];
      temp_data_buff[i][28] = pulseCounter.byte[2];
      temp_data_buff[i][29] = pulseCounter.byte[1];
      temp_data_buff[i][30] = pulseCounter.byte[0];

      pulseCounter.float_data = (float)lamp_burn_hour;
      temp_data_buff[i][31] = pulseCounter.byte[3];
      temp_data_buff[i][32] = pulseCounter.byte[2];
      temp_data_buff[i][33] = pulseCounter.byte[1];
      temp_data_buff[i][34] = pulseCounter.byte[0];

      pulseCounter.long_data = Rf_Track_Id;
      temp_data_buff[i][35] = pulseCounter.byte[3];
      temp_data_buff[i][36] = pulseCounter.byte[2];
      temp_data_buff[i][37] = pulseCounter.byte[1];
      temp_data_buff[i][38] = pulseCounter.byte[0];
      break;
    }
  }
}

/*************************************************************************
Function Name: get_data_from_mem_and_send
input: none.
Output: none.
Discription: get event data from RAM and send to DCU.
*************************************************************************/
void get_data_from_mem_and_send(void)
{
  unsigned char i=0,j=0;

  for(i=0;i<20;i++)
  {
    if((temp_data_buff[i][0] == 1))
    {
      Send_data_RF(&temp_data_buff[i][1],38,1);											// send event to only DCU
      for(j=0;j<40;j++)                                   // clear buffer after send event.
      {
        temp_data_buff[i][j] = 0;
      }
      no_of_pending_event--;
      break;
    }
  }
}

/*************************************************************************
Function Name: delet_data_from_mem
input: Track Id.
Output: none.
Discription: Delete data from memory if track id match with stored event
             track id
*************************************************************************/
void delet_data_from_mem(unsigned long int real_rf_track_id)
{
  unsigned char i=0,j=0;
  unsigned long int temp_Rf_Track_Id=0;

  for(i=0;i<20;i++)
  {
    if(temp_data_buff[i][0] == 1)
    {

      pulseCounter.byte[3] = temp_data_buff[i][35];
      pulseCounter.byte[2] = temp_data_buff[i][36];
      pulseCounter.byte[1] = temp_data_buff[i][37];
      pulseCounter.byte[0] = temp_data_buff[i][38];
      temp_Rf_Track_Id = pulseCounter.long_data;
      if(temp_Rf_Track_Id == real_rf_track_id)          // check if Track id match then delet message.
      {
        for(j=0;j<40;j++)
        {
          temp_data_buff[i][j] = 0;
        }
        temp_data_retrive_counter[i] = 0;
        no_of_pending_event--;                        // decrease no of pending event
        break;
      }
    }
  }
}
/////////////////////////////////////
void Auto_Event_delet_data_from_mem(unsigned long int real_rf_track_id);
void Auto_Event_delet_data_from_mem(unsigned long int real_rf_track_id)
{
  unsigned char i=0,j=0;
  unsigned long int temp_Rf_Track_Id=0;

  for(i=0;i<20;i++)
  {
    if(temp_data_buff[i][0] == 1)
    {

//      pulseCounter.byte[3] = temp_data_buff[i][35];
//      pulseCounter.byte[2] = temp_data_buff[i][36];
//      pulseCounter.byte[1] = temp_data_buff[i][37];
//      pulseCounter.byte[0] = temp_data_buff[i][38];
//      temp_Rf_Track_Id = pulseCounter.long_data;
      if(temp_data_buff[i][6] == real_rf_track_id)          // check if Track id match then delet message.
      {
        for(j=0;j<40;j++)
        {
          temp_data_buff[i][j] = 0;
        }
        temp_data_retrive_counter[i] = 0;
        no_of_pending_event--;                        // decrease no of pending event
        break;
      }
    }
  }
}

//////////////////////////////////////
/*************************************************************************
Function Name: delet_all_data
input: None.
Output: None.
Discription: Delete data all data from memory.
*************************************************************************/
void delet_all_data(void)      // delete all data either sent or unsent.
{
  unsigned char i=0,j=0;

  for(i=0;i<20;i++)
  {
    for(j=0;j<40;j++)
    {
      temp_data_buff[i][j] = 0;
    }
    temp_data_retrive_counter[i] = 0;		
  }
  no_of_pending_event = 0;	
}

/*************************************************************************
Function Name: send_all_event
input: None.
Output: None.
Discription: send all event one by one and if all send then clear event
             sending flag.
*************************************************************************/
void send_all_event(void)     // send all event one by one to DCU.
{
  unsigned char i=0,j=0;

  for(i=0;i<20;i++)
  {
    if(temp_data_buff[i][0] == 1)
    {
      Send_data_RF(&temp_data_buff[i][1],38,1);									// send event to only DCU
      break;	
    }
  }
  if(i >= 20)
  {
    read_event_flag = 0;
  }
}
////////////
void send_auto_event(void);     
void send_auto_event(void)     // send all event one by one to DCU.
{
  unsigned char i=0,j=0,H=0;

  for(i=0;i<20;i++)
  {
    if(temp_data_buff[i][0] == 1)
    {
      if((temp_data_buff[i][6] == 25)||(temp_data_buff[i][6] == 26)||
         (temp_data_buff[i][6] == 27)||(temp_data_buff[i][6] == 28)||(temp_data_buff[i][6] == 29))
      {
        Auto_ack_Rf_Track_Id =temp_data_buff[i][6];
        Send_data_RF(&temp_data_buff[i][1],38,1);									// send event to only DCU
        Event_Detected =1;
        break;	
      }
      else
      {
        Event_Detected =0;
      }
      
    }
    
  }
  
    
     
  
}

//////////////

void Write_NI_Para(unsigned int id)   // no need of this function in Ember
{

}

void DB_info_SLC(void)          // no need of this function in Ember
{

}

/*************************************************************************
Function Name: Configure_Slc_Mix_Mode_Sch
input: String pointer.
Output: None.
Discription: store Mix mode schedule in to NVM.
*************************************************************************/
void Configure_Slc_Mix_Mode_Sch(unsigned char *temp_str)
{
  unsigned char i=0;

  L_No = temp_str[0];    // read mix-mode schedule location from message and save buffer on that location.

  if(L_No == 0)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_1,&temp_str[1]);
  }
  else if(L_No == 1)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_2,&temp_str[1]);
  }
  else if(L_No == 2)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_3,&temp_str[1]);
  }
  else if(L_No == 3)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_4,&temp_str[1]);
  }
  else if(L_No == 4)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_5,&temp_str[1]);
  }
  else if(L_No == 5)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_6,&temp_str[1]);
  }
  else if(L_No == 6)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_7,&temp_str[1]);
  }
  else if(L_No == 7)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_8,&temp_str[1]);
  }
  else if(L_No == 8)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_9,&temp_str[1]);
  }
  else if(L_No == 9)
  {
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_10,&temp_str[1]);
  }
  else if(L_No == 10)
  {
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_11,&temp_str[1]);
  }
  else if(L_No == 11)
  {
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_12,&temp_str[1]);
  }
  else if(L_No == 12)
  {
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_13,&temp_str[1]);
  }
  else if(L_No == 13)
  {
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_14,&temp_str[1]);
  }
  else if(L_No == 14)
  {
//    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_15,&temp_str[1]);
  }
  else
  {
  }
  //////////////////
}	

/*************************************************************************
Function Name: unCompress_Mix_sch
input: schedule number.
Output: None.
Discription: read compress mix-mode value from NVM and uncompress to RAM.
*************************************************************************/
void unCompress_Mix_sch(unsigned char loc_no)
{
  BYTE_VAL temp_Mix_Mode_Byte;
  DWORD_VAL temp_Mix_Mode_Word[9];
  unsigned char i=0;

  if(loc_no == 0)        // uncompress message for mix-mode schedule checking.
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_1);
  }
  else if(loc_no == 1)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_2);
  }
  else if(loc_no == 2)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_3);
  }
  else if(loc_no == 3)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_4);
  }
  else if(loc_no == 4)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_5);
  }
  else  if(loc_no == 5)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_6);
  }
  else if(loc_no == 6)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_7);
  }
  else if(loc_no == 7)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_8);
  }
  else if(loc_no == 8)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_9);
  }
  else if(loc_no == 9)
  {
    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_10);
  }
  if(loc_no == 10)
  {
//    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_11);
  }
  else if(loc_no == 11)
  {
//    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_12);
  }
  else if(loc_no == 12)
  {
//    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_13);
  }
  else if(loc_no == 13)
  {
//    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_14);
  }
  else if(loc_no ==14)
  {
//    halCommonGetToken(&Slc_Comp_Mix_Mode_schedule[0],TOKEN_Slc_Mix_Mode_schedule_15);
  }


  Ember_Stack_run_Fun();              // call all ember tick function to increase stack timeing
  Slc_Mix_Mode_schedule[0].SLC_Mix_En = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_En;
  Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date;
  Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month;
  Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date;
  Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month;
  Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay;
  Slc_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override = Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override;

  for(i=0;i<9;i++)
  {
    temp_Mix_Mode_Word[i].v[0] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[i].compress1;
    temp_Mix_Mode_Word[i].v[1] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[i].compress2;
    temp_Mix_Mode_Word[i].v[2] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[i].compress3;
    temp_Mix_Mode_Word[i].v[3] = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[i].compress4;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b0;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].bSchFlag = temp_Mix_Mode_Byte.Val;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b1;
    temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b2;
    temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b3;
    temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b4;
    temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b5;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartHour = temp_Mix_Mode_Byte.Val;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b6;
    temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b7;
    temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b8;
    temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b9;
    temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b10;
    temp_Mix_Mode_Byte.bits.b5 = temp_Mix_Mode_Word[i].bits.b11;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartMin = temp_Mix_Mode_Byte.Val;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b12;
    temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b13;
    temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b14;
    temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b15;
    temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b16;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopHour = temp_Mix_Mode_Byte.Val;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b17;
    temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b18;
    temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b19;
    temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b20;
    temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b21;
    temp_Mix_Mode_Byte.bits.b5 = temp_Mix_Mode_Word[i].bits.b22;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopMin = temp_Mix_Mode_Byte.Val;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b23;
    temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b24;
    temp_Mix_Mode_Byte.bits.b2 = temp_Mix_Mode_Word[i].bits.b25;
    temp_Mix_Mode_Byte.bits.b3 = temp_Mix_Mode_Word[i].bits.b26;
    temp_Mix_Mode_Byte.bits.b4 = temp_Mix_Mode_Word[i].bits.b27;
    temp_Mix_Mode_Byte.bits.b5 = temp_Mix_Mode_Word[i].bits.b28;
    temp_Mix_Mode_Byte.bits.b6 = temp_Mix_Mode_Word[i].bits.b29;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].dimvalue = temp_Mix_Mode_Byte.Val;

    temp_Mix_Mode_Byte.Val = 0;
    temp_Mix_Mode_Byte.bits.b0 = temp_Mix_Mode_Word[i].bits.b30;
    temp_Mix_Mode_Byte.bits.b1 = temp_Mix_Mode_Word[i].bits.b31;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].Dim_Mode = temp_Mix_Mode_Byte.Val;

    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStartOff = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[i].compress5;
    Slc_Mix_Mode_schedule[0].Slc_Mix_Sch[i].cStopOff = Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[i].compress6;
  }
}

/*************************************************************************
Function Name: check_group
input: group information from message.
Output: if group match return 1 else 0.
Discription: compare message group to SLC group value.
*************************************************************************/
unsigned char check_group(unsigned char temp_G1)	
{
  BYTE_VAL temp_Group_val1;


  temp_Group_val1.Val = temp_G1;         // check SLC group from message, group info is in binary so LSB B0 represet 1st group ....MSB B7 represent 8th group.

  if(temp_Group_val1.Val == 0x00)
  {
    return 1;
  }
  else if((Group_val1.bits.b0 == temp_Group_val1.bits.b0) && (temp_Group_val1.bits.b0 == 1))
  {
    return 1;
  }
  else if((Group_val1.bits.b1 == temp_Group_val1.bits.b1) && (temp_Group_val1.bits.b1 == 1))
  {
    return 1;
  }
  else if((Group_val1.bits.b2 == temp_Group_val1.bits.b2) && (temp_Group_val1.bits.b2 == 1))
  {
    return 1;
  }
  else if((Group_val1.bits.b3 == temp_Group_val1.bits.b3) && (temp_Group_val1.bits.b3 == 1))
  {
    return 1;
  }
  else if((Group_val1.bits.b4 == temp_Group_val1.bits.b4) && (temp_Group_val1.bits.b4 == 1))
  {
    return 1;
  }
  else if((Group_val1.bits.b5 == temp_Group_val1.bits.b5) && (temp_Group_val1.bits.b5 == 1))
  {
    return 1;
  }
  else if((Group_val1.bits.b6 == temp_Group_val1.bits.b6) && (temp_Group_val1.bits.b6 == 1))
  {
    return 1;
  }	
  else if((Group_val1.bits.b7 == temp_Group_val1.bits.b7) && (temp_Group_val1.bits.b7 == 1))
  {
    return 1;
  }
  else
  {
    return 0;
  }		
}
/*****************************************************************************
Function Name::Expand the number of Group.Remaining logic would be same as having in that
**************************************************************************/

unsigned char check_32group(unsigned int temp_G1)	
{
  DWORD_VAL temp_Group_val1_32;


  temp_Group_val1_32.Val = temp_G1;         // check SLC group from message, group info is in binary so LSB B0 represet 1st group ....MSB B7 represent 8th group.

  if(temp_Group_val1_32.Val == 0x00)
  {
    return 1;
  }
  else if((Group_val1_32.bits.b0 == temp_Group_val1_32.bits.b0) && (temp_Group_val1_32.bits.b0 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b1 == temp_Group_val1_32.bits.b1) && (temp_Group_val1_32.bits.b1 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b2 == temp_Group_val1_32.bits.b2) && (temp_Group_val1_32.bits.b2 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b3 == temp_Group_val1_32.bits.b3) && (temp_Group_val1_32.bits.b3 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b4 == temp_Group_val1_32.bits.b4) && (temp_Group_val1_32.bits.b4 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b5 == temp_Group_val1_32.bits.b5) && (temp_Group_val1_32.bits.b5 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b6 == temp_Group_val1_32.bits.b6) && (temp_Group_val1_32.bits.b6 == 1))
  {
    return 1;
  }	
  else if((Group_val1_32.bits.b7 == temp_Group_val1_32.bits.b7) && (temp_Group_val1_32.bits.b7 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b8 == temp_Group_val1_32.bits.b8) && (temp_Group_val1_32.bits.b8 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b9 == temp_Group_val1_32.bits.b9) && (temp_Group_val1_32.bits.b9 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b10 == temp_Group_val1_32.bits.b10) && (temp_Group_val1_32.bits.b10 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b11 == temp_Group_val1_32.bits.b11) && (temp_Group_val1_32.bits.b11 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b12 == temp_Group_val1_32.bits.b12) && (temp_Group_val1_32.bits.b12 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b13 == temp_Group_val1_32.bits.b13) && (temp_Group_val1_32.bits.b13 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b14 == temp_Group_val1_32.bits.b14) && (temp_Group_val1_32.bits.b14 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b15 == temp_Group_val1_32.bits.b15) && (temp_Group_val1_32.bits.b15 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b16 == temp_Group_val1_32.bits.b16) && (temp_Group_val1_32.bits.b16 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b17 == temp_Group_val1_32.bits.b17) && (temp_Group_val1_32.bits.b17 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b18 == temp_Group_val1_32.bits.b18) && (temp_Group_val1_32.bits.b18 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b19 == temp_Group_val1_32.bits.b19) && (temp_Group_val1_32.bits.b19 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b20 == temp_Group_val1_32.bits.b20) && (temp_Group_val1_32.bits.b20 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b21 == temp_Group_val1_32.bits.b21) && (temp_Group_val1_32.bits.b21 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b22 == temp_Group_val1_32.bits.b22) && (temp_Group_val1_32.bits.b22 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b23 == temp_Group_val1_32.bits.b23) && (temp_Group_val1_32.bits.b23 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b24 == temp_Group_val1_32.bits.b24) && (temp_Group_val1_32.bits.b24 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b25 == temp_Group_val1_32.bits.b25) && (temp_Group_val1_32.bits.b25 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b26 == temp_Group_val1_32.bits.b26) && (temp_Group_val1_32.bits.b26 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b27 == temp_Group_val1_32.bits.b27) && (temp_Group_val1_32.bits.b27 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b28 == temp_Group_val1_32.bits.b28) && (temp_Group_val1_32.bits.b28 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b29 == temp_Group_val1_32.bits.b29) && (temp_Group_val1_32.bits.b29 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b30 == temp_Group_val1_32.bits.b30) && (temp_Group_val1_32.bits.b30 == 1))
  {
    return 1;
  }
  else if((Group_val1_32.bits.b31 == temp_Group_val1_32.bits.b31) && (temp_Group_val1_32.bits.b31 == 1))
  {
    return 1;
  }
  else
  {
    return 0;
  }		
}
/*************************************************************************
Function Name: check_multi_group_32
input: group information from message.
Output: if SLC in multiple group then return 1 else 0.
Discription: compare message group to SLC group value and if found SLC in to more
             groups then return 1 else return 0.
*************************************************************************/
unsigned char check_multi_group_32(unsigned int temp_G1)
{
  DWORD_VAL temp_Group_val1_32;
  unsigned char inc_group_counter = 0;	

  temp_Group_val1_32.Val = temp_G1;

  if(temp_Group_val1_32.bits.b0 == 1)      // increase counter if any bit field match in group.
  {
    inc_group_counter++;
  }

  if(temp_Group_val1_32.bits.b1 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1_32.bits.b2 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1_32.bits.b3 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1_32.bits.b4 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1_32.bits.b5 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1_32.bits.b6 == 1)
  {
    inc_group_counter++;
  }	

  if(temp_Group_val1_32.bits.b7 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b8 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b9 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b10 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b11 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b12 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b13 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b14 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b15 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b16 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b17 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b18 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b19 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b20 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b21 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b22 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b23 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b24 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b25 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b26 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b27 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b28 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b29 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b30 == 1)
  {
    inc_group_counter++;
  }
  if(temp_Group_val1_32.bits.b31 == 1)
  {
    inc_group_counter++;
  }

  if(inc_group_counter > 1)    // if counter is more then 1 then it indicates multiple group.
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/*************************************************************************
Function Name: check_multi_group
input: group information from message.
Output: if SLC in multiple group then return 1 else 0.
Discription: compare message group to SLC group value and if found SLC in to more
             groups then return 1 else return 0.
*************************************************************************/
unsigned char check_multi_group(unsigned char temp_G1)
{
  BYTE_VAL temp_Group_val1;
  unsigned char inc_group_counter = 0;	

  temp_Group_val1.Val = temp_G1;

  if(temp_Group_val1.bits.b0 == 1)      // increase counter if any bit field match in group.
  {
    inc_group_counter++;
  }

  if(temp_Group_val1.bits.b1 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1.bits.b2 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1.bits.b3 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1.bits.b4 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1.bits.b5 == 1)
  {
    inc_group_counter++;
  }

  if(temp_Group_val1.bits.b6 == 1)
  {
    inc_group_counter++;
  }	

  if(temp_Group_val1.bits.b7 == 1)
  {
    inc_group_counter++;
  }

  if(inc_group_counter > 1)    // if counter is more then 1 then it indicates multiple group.
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

void Response_Zero_For_Mix_Schedule_11_To_15(void)
{
   Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_En =0;
    Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Date=0;
    Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Start_Month=0;
    Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Date=0;
    Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Stop_Month=0;
    Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_WeekDay=0;
    Slc_Comp_Mix_Mode_schedule[0].SLC_Mix_Sch_Photocell_Override=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[0].compress6=0;


    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[1].compress6=0;


    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[2].compress6=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[3].compress6=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[4].compress6=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[5].compress6=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[6].compress6=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[7].compress6=0;

    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress1=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress2=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress3=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress4=0;			
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress5=0;
    Slc_Comp_Mix_Mode_schedule[0].Slc_Mix_Sch[8].compress6=0;
}

void Erase_Mix_Mode(void)
{
    Response_Zero_For_Mix_Schedule_11_To_15();
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_1,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_2,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_3,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_4,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_5,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_6,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_7,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_8,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_9,&Slc_Comp_Mix_Mode_schedule[0]);
    halCommonSetToken(TOKEN_Slc_Mix_Mode_schedule_10,&Slc_Comp_Mix_Mode_schedule[0]);
}

void Erase_Motion_perameter_And_Set_Default(void)
{
    Motion_pulse_rate = 1;
        halCommonSetToken(TOKEN_Motion_pulse_rate,&Motion_pulse_rate);
    Motion_dimming_percentage = 50;
        halCommonSetToken(TOKEN_Motion_dimming_percentage,&Motion_dimming_percentage);
    Motion_dimming_time = 120;
        halCommonSetToken(TOKEN_Motion_dimming_time,&Motion_dimming_time);
    dim_applay_time = 0;                      // Dimming_Apply_delay
        halCommonSetToken(TOKEN_dim_applay_time,&dim_applay_time);
    dim_inc_val	= 0;                         // gradually_dimming_time 																	
        halCommonSetToken(TOKEN_dim_inc_val,&dim_inc_val);
    Motion_normal_dimming_percentage = 100;
        halCommonSetToken(TOKEN_Motion_normal_dimming_percentage,&Motion_normal_dimming_percentage);
    Motion_Detect_Timeout = 5;
        halCommonSetToken(TOKEN_Motion_Detect_Timeout,&Motion_Detect_Timeout);
    Motion_Broadcast_Timeout= 5;
        halCommonSetToken(TOKEN_Motion_Broadcast_Timeout,&Motion_Broadcast_Timeout);
    Motion_Sensor_Type = 0;
        halCommonSetToken(TOKEN_Motion_Sensor_Type,&Motion_Sensor_Type);
////As discussed, no need to irage below perameter////////////////////////////////////////////
//    Motion_group_id = 0;
//    Group_val1.Val = Motion_group_id;
//        halCommonSetToken(TOKEN_Motion_group_id,&Motion_group_id);
//    Motion_group_id_32= 0;
//    Group_val1_32.Val = Motion_group_id_32;
//        halCommonSetToken(TOKEN_Motion_group_id_32,&Motion_group_id_32);
//    Motion_grp_Select_32 =1; //1 indicates it supports only 32 group..
//        halCommonSetToken(TOKEN_Motion_grp_Select_32,&Motion_grp_Select_32);

/////////////////////////////////////////////////////////////////

}