////////////////////

#ifndef RTC_DS1339
#define RTC_DS1339
#include "app/framework/include/af.h"
/***************************** Defines *****************************/
#define ACK 0
#define NACK 1
#if (defined (ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
	#define ADDRTC 0xa2 /* I2C slave address */
#else
	#define ADDRTC 0xd0 /* I2C slave address */
#endif
#define DS1339 /* compile directive, modify as required */
/************************* bit definitions *************************/
#define scl PORTC_PIN(7)//PORTBbits.RB10
#define sda PORTC_PIN(6)//PORTBbits.RB11
//sbit scl = P0^0; /* I2C pin definitions */
//sbit sda = P0^1;
//sbit sqw = P3^2; /* pin function depends upon device */
/* General Notes: Define one device to compile options for that device. */
/* Will not compile correctly if no device is defined. Not all options */
/* for each device are supported. There is no error checking for data */
/* entry. Defines may not remove all code that is not relevant to the */
/* device. This example was written for an 8051-type micro, the DS2250/ */
/* DS5000. The program must be modified to work properly on typical */
/* 8051 variants (i.e. assign I2C bus to unused port pins). This */
/* program is for example only and is not supported by Dallas Maxim */

typedef struct {
	      int8s Sec;     /* Second value - [0,59]           RTC_Sec*/
	      int8s Min;     /* Minute value - [0,59]           RTC_Min*/
	      int8s Hour;    /* Hour value - [0,23]             RTC_Hour*/
	} RTCTime;
	
typedef struct {
	     int8s Date;    /* Day of the month value - [1,31] RTC_Mday*/
	     int8s Month;   /* Month value - [1,12]            RTC_Mon*/
	     signed int Year;    /* Year value - [0,4095]           RTC_Year*/
	     signed char Week;    /* Day of week value - [0,6]       RTC_Wday*/
	    signed int  DofYear; /* Day of year value - [1,365]     RTC_Yday */
	} RTCDate;

//extern RTCTime Time;
//extern RTCDate Date;
extern RTCTime Time,PTime,Local_Time,GPS_Time;
extern RTCDate	Date,PDate,Local_Date,GPS_Date;
typedef unsigned char uchar;
void I2C_start();
void I2C_stop();
void I2C_write(unsigned char d);
uchar I2C_read(uchar);
void readbyte();
void writebyte();
void initialize();
void disp_clk_regs();
void burstramwrite(uchar);
void burstramread();
void alrm_int();
void alrm_read();
void tc_setup();
/////////////////////////
void init_ds1302(void);
void RTC_SET_TIME(RTCTime RTC_Time);
void RTC_SET_DATE(RTCDate RTC_Date);
void read_ds1302_bytes(void);
unsigned char week_day(unsigned int year,unsigned char date,unsigned char month);
void Find_Previous_date_Month(void);
//////////////////////////

#endif
