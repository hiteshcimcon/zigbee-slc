/****************************************************************************
Module: Energy meter

Project: Street Light controller
VERSION: 2.0.13

Module Details:

This module is use to read Energy meter data from the CS5463 IC.
****************************************************************************/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "generic.h"
#include "AMR_CS5463.h"
#include "protocol.h"
#include"DS1339.h"
#include "Application_logic.h"


#define EM_Delay 5 //300 * 20  5 =32 khz fre [300*20] = 50 hz fre
//extern unsigned char Send_Data_Save_Kwh;
extern unsigned int energy_meter_cnt ;
unsigned long int pulses_r,frequency;
float Calculated_Frequency=0.0;
float Calculated_Frequency_row =0.0;
float newkwh;
float diff;
unsigned long int Em_status_reg=0;
unsigned char datalow,datahigh,datamid;
unsigned char zempbyte;
unsigned long int zemplong;
unsigned long int reg_data_r[5];
unsigned char tempbyte;
unsigned char intesec;
float base;
float zempdoub;
float kbase;
float irdoub;
float vry;
float pf;
float syskw;
float syskva;
float va1;
float w1;
float var1;
float kwh;
float gunile1;
float mf1;
float mf2;
float mf3;
float mf4;
float vrdoub;
char temperature1 = 0;
unsigned char power_on_flag=0;
unsigned short int emt_int_count = 0,Kwh_count = 0;
unsigned char emt_pin_toggel = 0;
unsigned char check_day_burnar_flag = 0;
float temperature;

void Delay_EM(unsigned int i);

void CS_page_select(unsigned char page);
void CS_send_instruction(unsigned char instruction);
char CS_wait_and_clr_DRDY(void);
char CS_wait_for_DRDY(void);
void CS_clear_DRDY(void);
unsigned long int CS_read_register(unsigned char register_addr);
unsigned long int Raw_tempvalue;
float Get_Value_from_Raw_Data(uint32_t Rawdata, char Signed_num, float Weight);
/*************************************************************************
Function Name: init_cs5463
input: none.
Output: None.
Discription: This function is use to initialization.
*************************************************************************/
void init_cs5463()
{
  int i;
#if (defined (ISLC_3100_V7_7) ||defined(ISLC_3100_V7_9))
  
  halClearLed(sclk);//sclk=1;
  halClearLed(sdi);//sclk=1;
  for(i=0;i<3;i++)
  {
    Delay_EM(50000);
  }
  halSetLed(nreset);                     //nreset = 0;
  Delay_EM(1600);
  halClearLed(nreset);                   //nreset = 1;
  Delay_EM(100);
  
  CS_wait_and_clr_DRDY();
  CS_send_instruction(0x01);			//software reset
  CS_wait_and_clr_DRDY();	
  
  CS_wait_and_clr_DRDY();
  CS_send_instruction(0x03);			//wake up	
  CS_wait_and_clr_DRDY();	
        
  CS_page_select(0x10);
  write_to_all_regs(0x00,0x00,0x02,0x0a);
  
  CS_page_select(0);
  write_to_all_regs(0x05,0x00,0x00,0x12);			// PHASE COMPENSATION 
             
  CS_page_select(0x12);
  write_to_all_regs(0x1c,0xf3,0x5d,0x03);		// Page 0 ,pulse rate,  as per Cs5463 datasheet value for 305*20A * 12.5 p /sec
  
  CS_page_select(0);
  write_to_all_regs(0x09,0x00,0x03,0x00);		// Page 0 ,EPG1,EPG2 = Kwh, EPG3=Kvarh
  
    
  CS_page_select(0);
  write_to_all_regs(0x08,0x00,0x00,0x05);		// Page 0 ,EPG1,EPG2 = Kwh, EPG3=Kvarh
  
  
  CS_page_select(0);
  write_to_all_regs(0x01,0x70,0xe4,0x02);		// Page 0 ,EPG1,EPG3 on address 0 ,

  CS_page_select(0X10);
  write_to_all_regs(0x25,0x09,0x3B,0x00);		// Page 16 , config1 Register address 0 ,

  CS_wait_and_clr_DRDY();	
  CS_send_instruction(0x15);					// continoues conver
  CS_wait_and_clr_DRDY();  
#else
 
  halSetLed(nreset);                     //nreset = 0;
  Delay_EM(1600);
  halClearLed(nreset);                   //nreset = 1;
  Delay_EM(100);

  //	write_to_reg(0x01,0x40,0x04,0x00,PCB5);		//k=4   changes due to crystal change in new design
  write_to_reg(0x01,0x40,0x01,0x00,PCB5);		   //k=1

  /* set Mode Register */
  write_to_reg(0x01,0x64,0x61,0x02,0x00);     //AFC is set
  write_to_reg(0x01,0x4c,0x00,0x80,0x02);

  write_to_all_regs(0x4a,0xa0,0x0f,0x00);	    //For N=4000
  write_to_all_regs(0x5e,0xff,0xff,0xff);	    /* clear all bits in status reg. */
  write_to_all_regs(0x74,0x00,0x00,0x80);	    /* mask only for DRDY bit-all three */

  halSetLed(cs1);//cs1=0;
  transfer_byte(0xe8);		                      /* start continuous conversions..**/
  halClearLed(cs1);//cs1=1;
#endif
}

/*************************************************************************
Function Name: write_to_all_regs
input: command, Low byte of data, Middle Byte of Data, High Byte of Data.
Output: None.
Discription: This function is use to Write command to CS5463.
*************************************************************************/
#if (defined (ISLC_3100_V7_7) ||defined(ISLC_3100_V7_9))
void write_to_all_regs(unsigned char command,unsigned char high,unsigned char mid,unsigned char low)
{

  command = (0x40 | command);
  halSetLed(cs1);              //cs1=0;
  transfer_byte(command);
  transfer_byte(high);
  transfer_byte(mid);
  transfer_byte(low);
  halClearLed(cs1);            //cs1=1;
}
#else
void write_to_all_regs(unsigned char command,unsigned char low,unsigned char mid,unsigned char high)
{
  halSetLed(cs1);              //cs1=0;
  transfer_byte(command);
  transfer_byte(high);
  transfer_byte(mid);
  transfer_byte(low);
  halClearLed(cs1);            //cs1=1;
}
#endif
/*************************************************************************
Function Name: write_to_reg
input: port, Low byte of data, Middle Byte of Data, High Byte of Data.
Output: None.
Discription: This function is use to Write command to CS5463.
*************************************************************************/
void write_to_reg(unsigned char port,unsigned char command,unsigned char low,unsigned char mid,unsigned char high)
{
  switch(port)
  {
        case 0x01:
                halSetLed(cs1);         //cs1=0;
        break;
  } 											
  transfer_byte(command);
  transfer_byte(high);
  transfer_byte(mid);
  transfer_byte(low);
  Delay_EM(EM_Delay);
  halClearLed(cs1);                     //cs1=1;
}

/*************************************************************************
Function Name: read_a_reg
input: port, command.
Output: None.
Discription: This function is use to Read value from given command from CS5463.
*************************************************************************/
void read_a_reg(unsigned char port,unsigned char command)
{
  switch(port)
  {
  case 0x01:
    halSetLed(cs1);//cs1=0;;
    break;
  } 											  // END OF SWITCH CASE..
  transfer_byte(command);
  datahigh=receive_byte();
  datamid=receive_byte();
  datalow=receive_byte();
  Delay_EM(EM_Delay);
  halClearLed(cs1);//cs1=1;
}

/*************************************************************************
Function Name: transfer_byte
input: datum.
Output: None.
Discription: This function is use to transfer byte to CS5463.
*************************************************************************/
#if (defined (ISLC_3100_V7_7) ||defined(ISLC_3100_V7_9))
void transfer_byte(unsigned char datum)
{
	unsigned char count=0;

	halClearLed(sclk);//sclk=1;
	tempbyte=0x80;
	for(count=0;count<8;count++)
	{
		if(datum & tempbyte)
		{
			halClearLed(sdi);//sdi=1;
		}
		else
		{
			halSetLed(sdi);//sdi=0;
		}
		//Delay_EM(EM_Delay);
		halSetLed(sclk);//sclk=0;
		Delay_EM(EM_Delay);
		halClearLed(sclk);//sclk=1;
		Delay_EM(EM_Delay);
		tempbyte=tempbyte>>1;
	}
	Delay_EM(EM_Delay);
	halClearLed(sdi);//sdi=1;
	//Delay_EM(EM_Delay);
}

/*************************************************************************
Function Name: receive_byte
input: none.
Output: unsigned char data.
Discription: This function is use to Read byte from CS5463.
*************************************************************************/
unsigned char receive_byte()
{
	unsigned char count=0;
	halClearLed(sdi);//sdi=1;;
	datalow=0x80;
	tempbyte=0;
	
	for(count=0;count<8;count++)
	{
		//Delay_EM(EM_Delay);
		halSetLed(sclk);//sclk=0;
		Delay_EM(EM_Delay);
		halClearLed(sclk);//sclk=1;
		if(((GPIO_PAIN&PA1)>>PA1_BIT))//sdo)
		{
			tempbyte|=datalow;
		}
		Delay_EM(EM_Delay);
		datalow=datalow>>1;
//                              if(((GPIO_PAIN&PA1)>>PA1_BIT))//sdo)
//                              {
//                                      tempbyte|=datalow;
//                              }
//                              Delay_EM(EM_Delay);
//                              halSetLed(sclk);//sclk=0;
//                              Delay_EM(EM_Delay);
//                              halClearLed(sclk);//sclk=1;
//              
//                              Delay_EM(EM_Delay);
//                              datalow=datalow>>1;

	}
	return(tempbyte);
}
#else

void transfer_byte(unsigned char datum)
{
	unsigned char count=0;

	halSetLed(sclk);//sclk=0;
	tempbyte=0x80;
	for(count=0;count<8;count++)
	{
		if(datum & tempbyte)
		{
			halClearLed(sdi);//sdi=1;
		}
		else
		{
			halSetLed(sdi);//sdi=0;
		}
		Delay_EM(EM_Delay);
		halClearLed(sclk);//sclk=1;
		Delay_EM(EM_Delay);
		halSetLed(sclk);//sclk=0;
		Delay_EM(EM_Delay);
		tempbyte=tempbyte>>1;
	}
	Delay_EM(EM_Delay);
	halClearLed(sdi);//sdi=1;
	Delay_EM(EM_Delay);
}

/*************************************************************************
Function Name: receive_byte
input: none.
Output: unsigned char data.
Discription: This function is use to Read byte from CS5463.
*************************************************************************/
unsigned char receive_byte()
{
	unsigned char count=0;
	halClearLed(sdi);//sdi=1;;
	datalow=0x80;
	tempbyte=0;
	
	for(count=0;count<8;count++)
	{
		if(((GPIO_PAIN&PA1)>>PA1_BIT))//sdo)
		{
			tempbyte|=datalow;
		}
		Delay_EM(EM_Delay);
		halClearLed(sclk);//sclk=1;
		Delay_EM(EM_Delay);
		halSetLed(sclk);//sclk=0;
		Delay_EM(EM_Delay);
		datalow=datalow>>1;
	}
	return(tempbyte);
}
#endif
/*************************************************************************
Function Name: calculate_3p4w
input: none.
Output: none.
Discription: This function is use to calculate voltage, current, kw, pf, kwh
             from raw value read from cs5463.
*************************************************************************/
void calculate_3p4w(void)
{
		zempdoub=(1.0/16777216.0);
		irdoub=get_ieee(reg_data_r[1]);
		vrdoub=get_ieee(reg_data_r[2]);

		zempdoub=(2.0/16777216.0);
		
		zemplong=(reg_data_r[0] & 0x00800000);	
		if(zemplong)
		{
			reg_data_r[0]=(16777216-reg_data_r[0]);
		}
		zempdoub=get_ieee(reg_data_r[0]);
		vry=(vrdoub*irdoub);
		if(vry != 0.000000)
		{
			pf=(zempdoub/vry);
			pf=pf+0.02;
		}
		else
		{
			pf=0.99999;
		}

		if(pf>=1.0)
		 pf=0.9999;

		
//		vrdoub=vrdoub*(VRN/0.4456425);
  vrdoub=vrdoub*mf1;
		
		if(vrdoub<10.0)
		{
			vrdoub=0.0;
			pf=0.9999;
		}
//////////////////////////////
                if((vrdoub>85)&&(vrdoub<160))
		{
			mf3 = (PULSES_120/(float)KW_Cal_120);
		}
		else
		{
			mf3 = (PULSES/(float)KW_Cal);
		}
///////////////////////////////
		Calculated_Frequency = Calculated_Frequency * mf4;
		//vrdoub=vrdoub*pt_ratio;
		
//		irdoub=irdoub*(IR/0.2481445);
  irdoub=irdoub*mf2;
		
		if(irdoub<=CREEP_LIMIT)
		{
//			irdoub=0.0;
//			pf=0.9999;
                  #if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
                  #else
                          irdoub=0.0;
                          pf=0.9999;
                  #endif
		}
		
		base=(vrdoub*irdoub);
		
		va1 = base;
		
		w1=(base * pf);
		
		kbase=(base/1000.0);
		
		syskva=(syskva+kbase);
		
		kbase=(kbase*pf);
		
		syskw=(syskw+kbase);
		
		var1=(pf*pf);
		
		var1=(1.0-var1);
		
		var1=sqrt(var1);
		
		var1=(va1*var1);
	
		calculate_kwh();
}

/*************************************************************************
Function Name: get_ieee
input: raw data.
Output: float data.
Discription: This function is use to conver raw value in to ieee format.
*************************************************************************/
float get_ieee(unsigned long int rawdata)
{
	float value;
	unsigned int i;

base=zempdoub;
value=0.0;
for(i=0;i<24;i++){
	zemplong=0x00000001;
	if(i)zemplong=zemplong<<i;
	zemplong=(zemplong & rawdata);
	if(zemplong)value=value+base;
	base=(base*2.0);
}
return(value);
}

/*************************************************************************
Function Name: calculate_kwh
input: none.
Output: none.
Discription: This function is use to calculate Kwh from interrupt counter
*************************************************************************/
void calculate_kwh()
{
  unsigned char n=0;
	zempbyte=0;
//	if(irdoub > CREEP_LIMIT)
//	{
//		zempbyte=intesec;
//	}
        #if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
		zempbyte=intesec;
	#else
		if(irdoub > CREEP_LIMIT)
		{
			zempbyte=intesec;
		}		
	#endif
	intesec=0;
	pulses_r+=zempbyte;

	kwh=(((float)pulses_r*mf3));        // solve Kwh related Bug in new version
////////////////////////////
//        for(n =0;n<12;n++)
//        {
//            if(Time_Slot.Save_Energy_In_Slot[n] == 1)
//            {
//                Time_Slot.Slot_Pulses[n] =Time_Slot.Slot_Pulses[n] + zempbyte;
//
//               // sprintf(dispStr,"\r\n[Enrgy_Slot[%d]=%d] Shr=%02u::SMin=%02u::EHr=%02u::EMin=%02u P=%lu kwh=%lu***Hr=%d Min=%d",n,Time_Slot.Save_Energy_In_Slot[n],Time_Slot.Slot_Start_Hour[n],Time_Slot.Slot_Start_Min[n],Time_Slot.Slot_End_Hour[n],Time_Slot.Slot_End_Min[n],Time_Slot.Slot_Pulses[n],pulses_r,Time.Hour,Time.Min);
//               // WriteDebugMsg(dispStr);				
//            }	
//            Time_Slot.Save_Energy_In_Slot[n]=0;
//            Time_Slot.kwh_value[n] = ((float)Time_Slot.Slot_Pulses[n]*mf3);	
//        }
///////////////////////////
	
	diff = kwh - newkwh;

        if(diff >= 0.30)                   //V1.0.18.2  if diff is more then 0.30 then store in to NVM. //if((diff >= 0.01)||(Send_Data_Save_Kwh==1))
	{			
            //Send_Data_Save_Kwh =0;			
            newkwh = kwh;			
            pulseCounter.long_data = pulses_r;
            halCommonSetToken(TOKEN_pulses_r,&pulses_r);
           // halCommonSetToken(TOKEN_Slot_Pulses,&Time_Slot.Slot_Pulses[0]);
	}
}

/*************************************************************************
Function Name: calculate_kwh_LampOFF
input: none.
Output: none.
Discription: This function is use to store Kwh at diff of 0.1 at lamp off.
*************************************************************************/
void calculate_kwh_LampOFF(void)
{
  unsigned char n=0;

	zempbyte=0;
//	if(irdoub > CREEP_LIMIT)
//	{
//		zempbyte=intesec;
//	}
        #if (defined(ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
		zempbyte=intesec;
	#else
		if(irdoub > CREEP_LIMIT)
		{
			zempbyte=intesec;
		}		
	#endif
	intesec=0;
	pulses_r+=zempbyte;

	kwh=(((float)pulses_r*mf3));        // solve Kwh related Bug in new version
//    for(n =0;n<12;n++)
//    {
//        if(Time_Slot.Save_Energy_In_Slot[n] == 1)
//        {
//            Time_Slot.Slot_Pulses[n] =Time_Slot.Slot_Pulses[n] + zempbyte;
//            Time_Slot.kwh_value[n] = ((float)Time_Slot.Slot_Pulses[n]*mf3);
//
//        }		
//    }
	
	diff = kwh - newkwh;

 if(diff >= 0.10)                   // if difference is more then 0.10 then store in to NVM at a time of lamp off.
	{			
   newkwh = kwh;			
			pulseCounter.long_data = pulses_r;
   halCommonSetToken(TOKEN_pulses_r,&pulses_r);
   //halCommonSetToken(TOKEN_Slot_Pulses,&Time_Slot.Slot_Pulses[0]);
	}
}

/*************************************************************************
Function Name: Calculate_burnhour_Lampoff
input: none.
Output: none.
Discription: This function is use to store burn hour at diff of 15 min at lamp off.
*************************************************************************/
void Calculate_burnhour_Lampoff(void)
{
  unsigned long int Temp_burnhour;
  halCommonGetToken(&Temp_burnhour,TOKEN_lamp_burn_hour);  // Read last stored value of burn hour.
  if((lamp_burn_hour - Temp_burnhour) >= 15)  // if difference is more then 15 min then store in to NVM at time of Lamp OFF.
  {
        halCommonSetToken(TOKEN_lamp_burn_hour,&lamp_burn_hour);
  }
}

////////////////////////////////////////



unsigned char EM_OK =0;
void Read_EnergyMeter(void)
{

	unsigned char i,j,data=1;
	unsigned char *front_ptr;
#if (defined (ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))
  //unsigned char Test_count;
      //*********************************************************************************//
    if(energy_meter_cnt >=500)
    {
      energy_meter_cnt=0;
      
    CS_page_select(0);
    CS_read_register(0x17);

      //if ((datahigh&0xc0)&& (datahigh!=0xff))								//DRDY==1?
      if(datahigh == 0xc0) 
      {
          ////////////////////
          power_on_flag = 1;
          energy_time = 0;
          emt_int_count++;			
          energy_meter_ok = 0;
          check_day_burnar_flag =1; // 1.0.0013
          ////////////////////
          zempdoub=(1.0/16777216.0);
          
          CS_page_select(0x10);               // 300
          CS_read_register(0x07);         //rms voltage
          reg_data_r[2]= Raw_tempvalue;
          vrdoub=get_ieee(reg_data_r[2]);
      
          CS_page_select(0x10);
          CS_read_register(0x06);         //rms current
          reg_data_r[1]= Raw_tempvalue;
          irdoub=get_ieee(reg_data_r[1]);
          
          CS_page_select(0x10);              //added parvez
          CS_read_register(0x1B);         //energy meter temperature
          temperature=Get_Value_from_Raw_Data(Raw_tempvalue, 1, (256.0/16777216.0));
          temperature1 = temperature;
          
          zempdoub=(1.0/16777216.0);
              
          CS_page_select(0x10);
          CS_read_register(0x05);     //active power
          reg_data_r[0]= Raw_tempvalue;
          
          CS_page_select(0x10);
          CS_read_register(0x31);
          //Frequency=Get_Value_from_Raw_Data(Raw_tempvalue, 1, (2.0/16777216.0))*4000;
          frequency = Raw_tempvalue;
          Calculated_Frequency=get_ieee(frequency);
          Calculated_Frequency_row =Calculated_Frequency;
#else

	    i=0;
 //           data=(unsigned char)((GPIO_PBIN&PB6)>>PB6_BIT);  //Read a Port pin
            if(((GPIO_PBIN&PB6)>>PB6_BIT)){
              EM_OK =1;
            }
            if(!((GPIO_PBIN&PB6)>>PB6_BIT))
            {
             //   emberAfGuaranteedPrint("%p", "\nRead EM");
                power_on_flag = 1;
                if(EM_OK == 1){
                  EM_OK =0;
                energy_time = 0;
                }
                emt_int_count++;
                check_day_burnar_flag = 1;
                for(j=0x14;j<=0x18;j=j+2)
                {
                    read_a_reg(0x01,j);
                    front_ptr=(unsigned char *)&reg_data_r[i];
                    *front_ptr=datalow;
                    front_ptr++;
                    *front_ptr=datamid;
                    front_ptr++;
                    *front_ptr=datahigh;
                    front_ptr++;
                    *front_ptr=0x00;						
                    i++;
                }
                write_to_reg(0x01,0x5e,0x00,0x00,0x80);	 // clear status resistor flage of EM.

///////////////////////////////////////////////////////read temp from CS5463 //////////////
                i = 3;
                read_a_reg(0x01,0x26);
                pulseCounter.byte[0] = datalow;
                pulseCounter.byte[1] = datamid;
                pulseCounter.byte[2] = datahigh;
                pulseCounter.byte[3] = 0x00;
                temperature1 = pulseCounter.byte[2];
                //temperature2 = pulseCounter.byte[1];
                //temperature3 = pulseCounter.byte[0];

                read_a_reg(0x01,0x1A);
				i=0;
				front_ptr=(unsigned char *)&frequency;
						*front_ptr=datalow;
						front_ptr++;
						*front_ptr=datamid;
						front_ptr++;
						*front_ptr=datahigh;
////////////////////////////////////////////////////////read temp from CS5463 //////////////

                zempdoub=(1.0/16777216.0);
                irdoub=get_ieee(reg_data_r[1]);        // convert row value to needed value
                vrdoub=get_ieee(reg_data_r[2]);	   // convert row value to needed value
  //              temperature = get_ieee(reg_data_r[3]);	   // convert row value to needed value
                Calculated_Frequency=get_ieee(frequency);
                Calculated_Frequency_row =Calculated_Frequency;
                emt_pin_toggel = 0;
#endif				
/////////////////////calibration////////////
                if(calibmode == 1)
                {
                    if(Voltage_calibration == 1)
                    {
                        Voltage_calibration = 0;
                        Send_Voltage_calibration = 1;
                        Vr_Cal = vrdoub;
                        halCommonSetToken(TOKEN_Vr_Cal,&Vr_Cal);
                       // pulseCounter.float_data = Vr_Cal;
//                        WriteByte_EEPROM(EE_Vr_Cal + 0,pulseCounter.byte[3]);
//                        WriteByte_EEPROM(EE_Vr_Cal + 1,pulseCounter.byte[2]);
//                        WriteByte_EEPROM(EE_Vr_Cal + 2,pulseCounter.byte[1]);
//                        WriteByte_EEPROM(EE_Vr_Cal + 3,pulseCounter.byte[0]);	
                    }

                    if(Current_calibration == 1)
                    {
                        Current_calibration = 0;
                        Send_Current_calibration = 1;
                        Ir_Cal = irdoub;
                        halCommonSetToken(TOKEN_Ir_Cal,&Ir_Cal);
//                        pulseCounter.float_data = Ir_Cal;
//                        WriteByte_EEPROM(EE_Ir_Cal + 0,pulseCounter.byte[3]);
//                        WriteByte_EEPROM(EE_Ir_Cal + 1,pulseCounter.byte[2]);
//                        WriteByte_EEPROM(EE_Ir_Cal + 2,pulseCounter.byte[1]);
//                        WriteByte_EEPROM(EE_Ir_Cal + 3,pulseCounter.byte[0]);
                    }

                    if(KW_Calibration == 1)
                    {
                        KW_Calibration = 0;
                        Send_Kw_calibration = 1;
                        KW_Cal = (float)pulses_r;
                        halCommonSetToken(TOKEN_KW_Cal,&KW_Cal);
//                        pulseCounter.float_data = KW_Cal;
//                        WriteByte_EEPROM(EE_KW_Cal + 0,pulseCounter.byte[3]);
//                        WriteByte_EEPROM(EE_KW_Cal + 1,pulseCounter.byte[2]);
//                        WriteByte_EEPROM(EE_KW_Cal + 2,pulseCounter.byte[1]);
//                        WriteByte_EEPROM(EE_KW_Cal + 3,pulseCounter.byte[0]);	
                        pulses_r =0;
                        halCommonSetToken(TOKEN_pulses_r,&pulses_r);
//                        pulseCounter.long_data = 0;
//                        WriteByte_EEPROM(EE_KWH+0,pulseCounter.byte[0]);
//                        WriteByte_EEPROM(EE_KWH+1,pulseCounter.byte[1]);
//                        WriteByte_EEPROM(EE_KWH+2,pulseCounter.byte[2]);
//                        WriteByte_EEPROM(EE_KWH+3,pulseCounter.byte[3]);
                    }
                    ///////////////////////
                    if(KW_Calibration_120 == 1)
                    {
                        KW_Calibration_120 = 0;
                        Send_Kw_calibration_120 = 1;
                        KW_Cal_120 = (float)pulses_r;
                        halCommonSetToken(TOKEN_KW_Cal_120,&KW_Cal_120);

                        pulses_r =0;
                        halCommonSetToken(TOKEN_pulses_r,&pulses_r);
                    }
                    ///////////////////////
                }
//////////////////////////////////////////////////
            }
#if (defined (ISLC_3100_V7_7)||defined(ISLC_3100_V7_9))             
      }
#endif      
}

void calibration_Energy(void)
{
//	switch(CAL_page)
//	{	
//
//		case 1:
//					pulseCounter.float_data = vrdoub;
//					WriteByte_EEPROM(EE_VRDOUB+0,pulseCounter.byte[0]);
//					WriteByte_EEPROM(EE_VRDOUB+1,pulseCounter.byte[1]);
//					WriteByte_EEPROM(EE_VRDOUB+2,pulseCounter.byte[2]);
//					WriteByte_EEPROM(EE_VRDOUB+3,pulseCounter.byte[3]);
//               break;
//
//		case 2:
//					pulseCounter.float_data = irdoub;
//					WriteByte_EEPROM(EE_IRDOUB+0,pulseCounter.byte[0]);
//					WriteByte_EEPROM(EE_IRDOUB+1,pulseCounter.byte[1]);
//					WriteByte_EEPROM(EE_IRDOUB+2,pulseCounter.byte[2]);
//					WriteByte_EEPROM(EE_IRDOUB+3,pulseCounter.byte[3]);
//					pulses_r = 0;
//			   break;
//		case 3:
//					pulseCounter.long_data = pulses_r;
//					WriteByte_EEPROM(EE_PULSES_R+0,pulseCounter.byte[0]);
//					WriteByte_EEPROM(EE_PULSES_R+1,pulseCounter.byte[1]);
//					WriteByte_EEPROM(EE_PULSES_R+2,pulseCounter.byte[2]);
//					WriteByte_EEPROM(EE_PULSES_R+3,pulseCounter.byte[3]);
//			   break;	
//
//		default:
//			   break;
//	}

}

void set_mf_energy(void)
{
	mf1 = VRN / vrdoub;
	mf2 = IR / irdoub;
	mf3 = (PULSES/(float)pulses_r);
        mf4 = 8000.0;
//	mf1 = 506;
//	mf2 = 12;
//	mf3 = 0.000140;
}


void Delay_EM(unsigned int i)
{

	unsigned int j,k;
 for(j=0;j<i;j++)
	{
		for(k=0;k<10;k++)
		{

		}
	}
 halResetWatchdog();
 //Ember_Stack_run_Fun();   // call all ember tick function to increase stack timeing
}
void CS_page_select(unsigned char page)
{
    unsigned char passvalue;
    passvalue = (0x80| page);
    halSetLed(cs1);
    Delay_EM(EM_Delay);
    transfer_byte(passvalue);
    Delay_EM(EM_Delay);
    halClearLed(cs1);
}

void CS_send_instruction(unsigned char instruction)
{
    unsigned char passvalue;
    passvalue = (0xc0 | instruction);
    halSetLed(cs1);
    Delay_EM(EM_Delay);
    transfer_byte(passvalue);
    Delay_EM(EM_Delay);
    halClearLed(cs1);
}

char CS_wait_and_clr_DRDY(void)
{
    char  temp;
    temp = CS_wait_for_DRDY();
    if (temp)
    { return -1; }
    CS_clear_DRDY();
    return 0;
}

char CS_wait_for_DRDY(void)
{
    unsigned long int temp;
    unsigned char  loopCntr = 0;
    CS_page_select(0); //np ack require no return type.
    while (loopCntr < 30)
    {
        temp = CS_read_register(0x17);
        if (temp >> 24)				// if the read produced an error, ignore it.
        {
        }
        else
        {
            if (temp & 0x800000)	// if DRDY is set.
            {
                    return 0;			// exit with success.
                    break;
            }
        }
        loopCntr += 1;
        Delay_EM(2);
    }
    return -1;
}

void CS_clear_DRDY(void)
{
    CS_page_select(0);
    write_to_all_regs(0x17, 0x80,0x00,0x00);	// writing a 1 to the relevant bit clears it.
    Delay_EM(3);
}

unsigned long int CS_read_register(unsigned char register_addr)
{
    unsigned char *front_ptr;
    unsigned long int ret = 0;
    halSetLed(cs1);
    transfer_byte(register_addr);
    datahigh=receive_byte();
    datamid=receive_byte();
    datalow=receive_byte();
    Delay_EM(EM_Delay);
    halClearLed(cs1);//cs1=1;
    front_ptr=(unsigned char *)&Raw_tempvalue;
    *front_ptr=datalow;
    front_ptr++;
    *front_ptr=datamid;
    front_ptr++;
    *front_ptr=datahigh;
    front_ptr++;
    *front_ptr=0x00;
    ret=Raw_tempvalue;
    return ret;
}

float Get_Value_from_Raw_Data(uint32_t Rawdata, char Signed_num, float Weight)
{
    float value;
    if(Signed_num)
    {
        if(Rawdata & 0x00800000)
        {
            { Rawdata |= 0xFF000000; }
        }
        value = *((int32_t*)&Rawdata) * Weight;
    }
    else
    { value = Rawdata * Weight; }
    return(value);
}